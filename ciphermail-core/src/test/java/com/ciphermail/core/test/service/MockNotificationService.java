/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.test.service;

import com.ciphermail.core.common.notification.NotificationListener;
import com.ciphermail.core.common.notification.NotificationMessage;
import com.ciphermail.core.common.notification.NotificationService;
import com.ciphermail.core.common.notification.NotificationSeverity;

import java.util.LinkedList;
import java.util.List;

public class MockNotificationService implements NotificationService
{
    public static class MockNotification
    {
        private final String facility;
        private final NotificationSeverity severity;
        private final NotificationMessage message;

        MockNotification(String facility, NotificationSeverity severity, NotificationMessage message)
        {
            this.facility = facility;
            this.severity = severity;
            this.message = message;
        }

        public String getFacility() {
            return facility;
        }

        public NotificationSeverity getSeverity() {
            return severity;
        }

        public NotificationMessage getMessage() {
            return message;
        }
    }

    /*
     * The sent notifications
     */
    private final List<MockNotification> notifications = new LinkedList<>();

    /*
     * The registered listeners
     */
    private final List<NotificationListener> notificationListeners = new LinkedList<>();

    @Override
    public void sendNotification(String facility, NotificationSeverity severity, NotificationMessage message)
    {
        for (NotificationListener listener : notificationListeners) {
            listener.notificationEvent(facility, severity, message);
        }

        notifications.add(new MockNotification(facility, severity, message));
    }

    @Override
    public void registerNotificationListener(NotificationListener listener) {
        notificationListeners.add(listener);
    }

    public List<MockNotification> getNotifications() {
        return notifications;
    }
}
