/*
 * Copyright (c) 2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.test;

import org.slf4j.Logger;
import org.slf4j.Marker;

/**
 * Logger implementation that does nothing. Used for testing.
 */
public class NOPLogger implements Logger
{
    @Override
    public String getName() {
        return null;
    }

    @Override
    public boolean isTraceEnabled() {
        return false;
    }

    @Override
    public void trace(String paramString) {
    }

    @Override
    public void trace(String paramString, Object paramObject) {
    }

    @Override
    public void trace(String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void trace(String paramString, Object... paramVarArgs) {
    }

    @Override
    public void trace(String paramString, Throwable paramThrowable) {
    }

    @Override
    public boolean isTraceEnabled(Marker paramMarker) {
        return false;
    }

    @Override
    public void trace(Marker paramMarker, String paramString) {
    }

    @Override
    public void trace(Marker paramMarker, String paramString, Object paramObject) {
    }

    @Override
    public void trace(Marker paramMarker, String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void trace(Marker paramMarker, String paramString, Object... paramVarArgs) {
    }

    @Override
    public void trace(Marker paramMarker, String paramString, Throwable paramThrowable) {
    }

    @Override
    public boolean isDebugEnabled() {
        return false;
    }

    @Override
    public void debug(String paramString) {
    }

    @Override
    public void debug(String paramString, Object paramObject) {
    }

    @Override
    public void debug(String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void debug(String paramString, Object... paramVarArgs) {
    }

    @Override
    public void debug(String paramString, Throwable paramThrowable) {
    }

    @Override
    public boolean isDebugEnabled(Marker paramMarker) {
        return false;
    }

    @Override
    public void debug(Marker paramMarker, String paramString) {
    }

    @Override
    public void debug(Marker paramMarker, String paramString, Object paramObject) {
    }

    @Override
    public void debug(Marker paramMarker, String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void debug(Marker paramMarker, String paramString, Object... paramVarArgs) {
    }

    @Override
    public void debug(Marker paramMarker, String paramString, Throwable paramThrowable) {
    }

    @Override
    public boolean isInfoEnabled() {
        return false;
    }

    @Override
    public void info(String paramString) {
    }

    @Override
    public void info(String paramString, Object paramObject) {
    }

    @Override
    public void info(String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void info(String paramString, Object... paramVarArgs) {
    }

    @Override
    public void info(String paramString, Throwable paramThrowable) {
    }

    @Override
    public boolean isInfoEnabled(Marker paramMarker) {
        return false;
    }

    @Override
    public void info(Marker paramMarker, String paramString) {
    }

    @Override
    public void info(Marker paramMarker, String paramString, Object paramObject) {
    }

    @Override
    public void info(Marker paramMarker, String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void info(Marker paramMarker, String paramString, Object... paramVarArgs) {
    }

    @Override
    public void info(Marker paramMarker, String paramString, Throwable paramThrowable) {
    }

    @Override
    public boolean isWarnEnabled() {
        return false;
    }

    @Override
    public void warn(String paramString) {
    }

    @Override
    public void warn(String paramString, Object paramObject) {
    }

    @Override
    public void warn(String paramString, Object... paramVarArgs) {
    }

    @Override
    public void warn(String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void warn(String paramString, Throwable paramThrowable) {
    }

    @Override
    public boolean isWarnEnabled(Marker paramMarker) {
        return false;
    }

    @Override
    public void warn(Marker paramMarker, String paramString) {
    }

    @Override
    public void warn(Marker paramMarker, String paramString, Object paramObject) {
    }

    @Override
    public void warn(Marker paramMarker, String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void warn(Marker paramMarker, String paramString, Object... paramVarArgs) {
    }

    @Override
    public void warn(Marker paramMarker, String paramString, Throwable paramThrowable) {
    }

    @Override
    public boolean isErrorEnabled() {
        return false;
    }

    @Override
    public void error(String paramString) {
    }

    @Override
    public void error(String paramString, Object paramObject) {
    }

    @Override
    public void error(String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void error(String paramString, Object... paramVarArgs) {
    }

    @Override
    public void error(String paramString, Throwable paramThrowable) {
    }

    @Override
    public boolean isErrorEnabled(Marker paramMarker) {
        return false;
    }

    @Override
    public void error(Marker paramMarker, String paramString) {
    }

    @Override
    public void error(Marker paramMarker, String paramString, Object paramObject) {
    }

    @Override
    public void error(Marker paramMarker, String paramString, Object paramObject1, Object paramObject2) {
    }

    @Override
    public void error(Marker paramMarker, String paramString, Object... paramVarArgs) {
    }

    @Override
    public void error(Marker paramMarker, String paramString, Throwable paramThrowable) {
    }
}
