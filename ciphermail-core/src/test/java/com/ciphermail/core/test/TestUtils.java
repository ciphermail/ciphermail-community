/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.test;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.crl.CRLUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.UnhandledException;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

public class TestUtils
{
    // Location of all tests resources
    private static final File TEST_RESOURCES_BASE_DIR = new File("src/test/resources");

    // Location of all tests data
    private static final File TEST_DATA_DIR = new File(TEST_RESOURCES_BASE_DIR,"testdata");

    private TestUtils() {
        // Empty on purpose
    }

    /**
     * Returns the base dir where test resources are stored.
     */
    public static File getTestResourcesBaseDir() {
        return TEST_RESOURCES_BASE_DIR;
    }

    /**
     * Returns the base dir where test data is stored.
     */
    public static File getTestDataDir() {
        return TEST_DATA_DIR;
    }

    /**
     * Create a temp file with deleteOnExit set to true
     */
    public static File createTempFile(@Nonnull String suffix)
    throws IOException
    {
        File file = File.createTempFile("ciphermail-test", suffix);
        file.deleteOnExit();

        return file;
    }

    /**
     * Load a MIME message from the test resources dir
     * @param relativePath the relative path to the MIME encoded email to load
     * @return the MIME message
     */
    public static @Nonnull MimeMessage loadTestMessage(@Nonnull String relativePath)
    throws MessagingException, FileNotFoundException
    {
        return MailUtils.loadMessage(new File(TEST_DATA_DIR, relativePath));
    }

    /**
     * Calculates the number of mitm*.tmp files in the temp folder. This is used to detect if we have any
     * temp file leakage.
     */
    public static int getTempFileCount(String prefix, String postfix)
    {
        String[] files = SystemUtils.getJavaIoTmpDir().list((file, s) ->
            StringUtils.startsWith(s, prefix) && StringUtils.endsWith(s, postfix)
        );

        return files != null ? files.length : 0;
    }

    /**
     * Checks whether the two messages have the exact same content
     */
    public static boolean isEqual(@Nonnull MimeMessage message1, @Nonnull MimeMessage message2)
    {
        try {
            ByteArrayOutputStream bos1 = new ByteArrayOutputStream();

            MailUtils.writeMessage(message1, bos1);

            ByteArrayOutputStream bos2 = new ByteArrayOutputStream();

            MailUtils.writeMessage(message2, bos2);

            return Arrays.equals(bos2.toByteArray(), bos1.toByteArray());
        }
        catch (IOException | MessagingException e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * Returns the first certificate found in the provided file
     */
    public static @Nonnull X509Certificate loadCertificate(@Nonnull File file)
    {
        try {
            Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

            if (certificates != null && !certificates.isEmpty()) {
                return (X509Certificate)certificates.iterator().next();
            }

            throw new IllegalArgumentException("No X509Certificate found in file " + file);
        }
        catch (IOException | CertificateException | NoSuchProviderException  e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * Returns the first certificate found in the provided file
     */
    public static @Nonnull X509Certificate loadCertificate(@Nonnull String filename) {
        return loadCertificate(new File(filename));
    }

    /**
     * Returns the first CRL found in the provided file
     */
    public static @Nonnull X509CRL loadX509CRL(@Nonnull File file)
    {
        try {
            Collection<? extends CRL> crls = CRLUtils.readCRLs(file);

            if (!crls.isEmpty()) {
                return (X509CRL) crls.iterator().next();
            }

            throw new IllegalArgumentException("No X509CRL found in file " + file);
        }
        catch (IOException | CertificateException | NoSuchProviderException | CRLException  e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * Returns the first CRL found in the provided file
     */
    public static @Nonnull X509CRL loadX509CRL(@Nonnull InputStream input)
    {
        try {
            Collection<? extends CRL> crls = CRLUtils.readCRLs(input);

            if (!crls.isEmpty()) {
                return (X509CRL) crls.iterator().next();
            }

            throw new IllegalArgumentException("No X509CRL found in InputStream");
        }
        catch (CertificateException | NoSuchProviderException | CRLException  e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * Parses the input string into a Date. The input string should be formatted as "dd-MMM-yyyy HH:mm:ss z"
     */
    public static @Nonnull Date parseDate(@Nonnull String date)
    {
        try {
            return new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss z").parse(date);
        }
        catch (ParseException e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * Loads a PKCS12 KeyStore from the file.
     */
    public static @Nonnull KeyStore loadKeyStore(@Nonnull File file, @Nonnull String password)
    {
        try {
            KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

            try (FileInputStream input = new FileInputStream(file)) {
                keyStore.load(input, password.toCharArray());
            }

            return keyStore;
        }
        catch (NoSuchProviderException | NoSuchAlgorithmException | CertificateException | IOException |
                KeyStoreException e)
        {
            throw new UnhandledException(e);
        }
    }
}
