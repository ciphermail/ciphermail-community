/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Simple socket server just for testing purposes.
 *
 * @author Martijn Brinkers
 *
 */
public class SimpleSocketServer implements Runnable
{
    private final static Logger logger = LoggerFactory.getLogger(SimpleSocketServer.class);

    private final int port;
    private SocketAcceptEvent event;
    private final Object lock = new Object();

    private boolean running;

    public SimpleSocketServer(int port) {
        this.port = port;
    }

    @Override
    public void run()
    {
        try {
            try (ServerSocket serverSocket = new ServerSocket(port, 5, InetAddress.getByName("127.0.0.1")))
            {
                boolean localRunning;

                synchronized (lock) {
                    running = true;
                    localRunning = running;
                }

                logger.info("SimpleSocketServer running on port: {}", port);

                while (localRunning) {
                    try {
                        Socket socket = serverSocket.accept();

                        logger.info("Incoming connection.");

                        synchronized (lock) {
                            if (event != null) {
                                event.accept(socket);
                            }
                        }

                        synchronized (lock) {
                            localRunning = running;
                        }
                    }
                    catch (IOException e) {
                        logger.error("Socket error", e);
                    }
                }
            }
        }
        catch (IOException e) {
            logger.error("Error creating server socket", e);
        }
    }

    public void stop()
    {
        synchronized (lock) {
            running = false;
        }
    }

    public boolean isRunning()
    {
        synchronized (lock) {
            return running;
        }
    }

    public void setIncomingEvent(SocketAcceptEvent event)
    {
        synchronized (lock) {
            this.event = event;
        }
    }
}
