/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.test;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.StringLayout;
import org.apache.logging.log4j.core.appender.WriterAppender;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.junit.rules.ExternalResource;

import java.io.CharArrayWriter;

/**
 * Collects log output from Log4J2 to make it possible to check logs from a unit test.
 *
 * Usage:
 *
 * \@Rule
 * public LogAppenderResource memoryLogAppender = new LogAppenderResource(
 *      LogManager.getLogger(LogManager.getRootLogger().class));
 *
 * Replace LogManager.getRootLogger() by a specific logger to collect the logs from a specific logger
 *
 * To get the log output, use:
 *
 * memoryLogAppender.getLogOutput()
 *
 * Based on https://www.dontpanicblog.co.uk/2018/04/29/test-log4j2-with-junit/
 */
public class MemoryLogAppenderResource extends ExternalResource
{
    private static final String APPENDER_NAME = "MemoryLogAppender";

    private static final String DEFAULT_PATTERN = "\n%-5level %msg";

    // the log pattern
    private String pattern = DEFAULT_PATTERN;

    // the logger for which log output should be collected
    private final Logger logger;

    private Appender appender;

    private final CharArrayWriter memoryLogWriter = new CharArrayWriter();

    public MemoryLogAppenderResource(org.apache.logging.log4j.Logger logger) {
        this.logger = (org.apache.logging.log4j.core.Logger)logger;
    }

    @Override
    protected void before()
    {
        StringLayout layout = PatternLayout.newBuilder().withPattern(pattern).build();
        appender = WriterAppender.newBuilder()
                .setTarget(memoryLogWriter)
                .setLayout(layout)
                .setName(APPENDER_NAME)
                .build();
        appender.start();
        logger.addAppender(appender);
    }

    @Override
    protected void after() {
        logger.removeAppender(appender);
    }

    public String getLogOutput() {
        return memoryLogWriter.toString();
    }

    public void reset() {
        memoryLogWriter.reset();
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}