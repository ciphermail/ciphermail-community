/*
 * Copyright (c) 2010-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.common.mail.repository.MailRepository;
import com.ciphermail.core.common.mail.repository.MailRepositoryItem;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class QuarantineTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MailRepository quarantineMailRepository;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void before() {
        transactionOperations.executeWithoutResult(status -> quarantineMailRepository.deleteAll());
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private List<? extends MailRepositoryItem> getMailRepositoryItems()
    {
        return transactionOperations.execute(status ->
                quarantineMailRepository.getItems(0, Integer.MAX_VALUE));
    }

    private void assertMessage(UUID id, MimeMessage expected)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MailRepositoryItem item = quarantineMailRepository.getItem(id);

                assertTrue(TestUtils.isEqual(expected, item.getMimeMessage()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Quarantine createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Quarantine mailet = new Quarantine();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testQuarantine()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("errorProcessor", "error")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/html-alternative.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test1@example.com", "test2@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state(Mail.DEFAULT)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        List<? extends MailRepositoryItem> items = getMailRepositoryItems();

        assertNotNull(items);

        assertEquals(0, items.size());

        assertNull(CoreApplicationMailAttributes.getMailRepositoryID(mail));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Mail was quarantined"));

        items = getMailRepositoryItems();

        assertNotNull(items);

        assertEquals(1, items.size());

        assertMessage(items.get(0).getID(), sourceMessage);

        assertNotNull(CoreApplicationMailAttributes.getMailRepositoryID(mail));

        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        assertEquals("test1@example.com,test2@example.com",
                StringUtils.join(mail.getRecipients(), ","));
    }

    @Test
    public void testError()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("errorProcessor", "error")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/html-alternative.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test1@example.com", "test2@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state(Mail.DEFAULT)
                .build();

        Quarantine mailet = createMailet(mailetConfig);

        mailet.init(mailetConfig);

        // override the MailStorer with one that always fails
        mailet.setQuarantineMailStorer(m ->
        {
            throw new OutOfMemoryError("Expected error");
        });

        List<? extends MailRepositoryItem> items = getMailRepositoryItems();

        assertNotNull(items);

        assertEquals(0, items.size());

        assertNull(CoreApplicationMailAttributes.getMailRepositoryID(mail));

        assertTrue(TestUtils.isEqual(sourceMessage, mail.getMessage()));

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "OutOfMemoryError: Expected error"));

        items = getMailRepositoryItems();

        assertEquals(0, items.size());

        assertNull(CoreApplicationMailAttributes.getMailRepositoryID(mail));

        assertEquals("error", mail.getState());
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        assertEquals("test1@example.com,test2@example.com",
                StringUtils.join(mail.getRecipients(), ","));
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("errorProcessor", "error")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/html-alternative.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test1@example.com", "test2@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state(Mail.DEFAULT)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        Quarantine mailet = new Quarantine()
        {
            @Override
            protected void serviceMailTransacted(Mail mail)
            throws MessagingException, IOException
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
                super.serviceMailTransacted(mail);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        List<? extends MailRepositoryItem> items = getMailRepositoryItems();

        assertNotNull(items);

        assertEquals(0, items.size());

        assertNull(CoreApplicationMailAttributes.getMailRepositoryID(mail));

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Mail was quarantined"));

        assertTrue(constraintViolationThrown.booleanValue());

        items = getMailRepositoryItems();

        assertNotNull(items);

        assertEquals(1, items.size());

        assertMessage(items.get(0).getID(), sourceMessage);

        assertNotNull(CoreApplicationMailAttributes.getMailRepositoryID(mail));

        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());
        assertEquals("test1@example.com,test2@example.com",
                StringUtils.join(mail.getRecipients(), ","));
    }
}
