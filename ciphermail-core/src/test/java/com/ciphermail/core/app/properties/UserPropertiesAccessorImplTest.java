/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import com.ciphermail.core.common.properties.StandardHierarchicalProperties;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.util.BeanUtilsBeanBuilderImpl;
import com.ciphermail.core.common.util.ByteArray;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.annotation.Annotation;
import java.util.Date;
import java.util.Properties;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {UserPropertiesAccessorImplTest.LocalServices.class})
public class UserPropertiesAccessorImplTest
{
    @Component
    public static class LocalServices
    {
        @Bean
        UserPropertiesAccessor createUserPropertiesAccessor(ApplicationContext applicationContext)
        {
            return new UserPropertiesAccessorImpl(
                    new UserPropertyRegistryImpl(new MockEnvironment()).addClasses(mockUserPropertiesType,
                            TestProperties.class),
                    new BeanUtilsBeanBuilderImpl(),
                    new UserPropertiesFactoryRegistryImpl(applicationContext));
        }
    }

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private UserPropertiesAccessor userPropertiesAccessor;

    private static final UserPropertiesType mockUserPropertiesType = new UserPropertiesType()
    {
        @Override
        public String name() {
            return "mock";
        }

        @Override
        public String parent() {
            return "";
        }

        @Override
        public Type type() {
            return Type.PROPERTIES;
        }

        @Override
        public boolean visible() {
            return true;
        }

        @Override
        public String displayName() {
            return "";
        }

        @Override
        public String displayNameKey() {
            return "";
        }

        @Override
        public String descriptionKey() {
            return "";
        }

        @Override
        public int order() {
            return 10;
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return UserPropertiesType.class;
        }
    };

    public static class TestProperties extends DelegatedHierarchicalProperties
    {
        public TestProperties(HierarchicalProperties delegate) {
            super(delegate);
        }

        @UserProperty(
                name = "onlyProp",
                displayNameKey = UserProperty.BASE_KEY + "getOnlyProp" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "getOnlyProp" + UserProperty.DESCRIPTION_KEY
        )
        public String getOnlyProp()
        throws HierarchicalPropertiesException
        {
            return getProperty("getOnlyProp");
        }

        @UserProperty(
                name = "digest",
                displayNameKey = UserProperty.BASE_KEY + "digest" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "digest" + UserProperty.DESCRIPTION_KEY
        )
        public Digest getDigestTest()
        throws HierarchicalPropertiesException
        {
            return HierarchicalPropertiesUtils.getEnum(this, "digest", Digest.class);
        }

        @UserProperty(
                name = "digest",
                displayNameKey = UserProperty.BASE_KEY + "digest" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "digest" + UserProperty.DESCRIPTION_KEY
        )
        public void setDigestTest(Digest value)
        throws HierarchicalPropertiesException
        {
            HierarchicalPropertiesUtils.setEnum(this, "digest", value);
        }

        @UserProperty(
                name = "bool",
                displayNameKey = UserProperty.BASE_KEY + "bool" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "bool" + UserProperty.DESCRIPTION_KEY
        )
        public Boolean getBool()
        throws HierarchicalPropertiesException
        {
            return HierarchicalPropertiesUtils.getBoolean(this, "bool", false);
        }

        @UserProperty(
                name = "bool",
                displayNameKey = UserProperty.BASE_KEY + "bool" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "bool" + UserProperty.DESCRIPTION_KEY
        )
        public void setBool(Boolean value)
        throws HierarchicalPropertiesException
        {
            HierarchicalPropertiesUtils.setBoolean(this, "bool", value);
        }

        @UserProperty(
                name = "integer",
                displayNameKey = UserProperty.BASE_KEY + "integer" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "integer" + UserProperty.DESCRIPTION_KEY
        )
        public Integer getInteger()
        throws HierarchicalPropertiesException
        {
            return HierarchicalPropertiesUtils.getInteger(this, "integer", null);
        }

        @UserProperty(
                name = "integer",
                displayNameKey = UserProperty.BASE_KEY + "integer" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "integer" + UserProperty.DESCRIPTION_KEY
        )
        public void setInteger(Integer value)
        throws HierarchicalPropertiesException
        {
            HierarchicalPropertiesUtils.setInteger(this, "integer", value);
        }

        @UserProperty(
                name = "long",
                displayNameKey = UserProperty.BASE_KEY + "long" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "long" + UserProperty.DESCRIPTION_KEY
        )
        public Long getLong()
        throws HierarchicalPropertiesException
        {
            return HierarchicalPropertiesUtils.getLong(this, "long", null);
        }

        @UserProperty(
                name = "long",
                displayNameKey = UserProperty.BASE_KEY + "long" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "long" + UserProperty.DESCRIPTION_KEY
        )
        public void setLong(Long value)
        throws HierarchicalPropertiesException
        {
            HierarchicalPropertiesUtils.setLong(this, "long", value);
        }

        @UserProperty(
                name = "double",
                displayNameKey = UserProperty.BASE_KEY + "double" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "double" + UserProperty.DESCRIPTION_KEY
        )
        public Double getDouble()
        throws HierarchicalPropertiesException
        {
            return HierarchicalPropertiesUtils.getDouble(this, "double", null);
        }

        @UserProperty(
                name = "double",
                displayNameKey = UserProperty.BASE_KEY + "double" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "double" + UserProperty.DESCRIPTION_KEY
        )
        public void setDouble(Double value)
        throws HierarchicalPropertiesException
        {
            HierarchicalPropertiesUtils.setDouble(this, "double", value);
        }

        @UserProperty(
                name = "float",
                displayNameKey = UserProperty.BASE_KEY + "float" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "float" + UserProperty.DESCRIPTION_KEY
        )
        public Float getFloat()
        throws HierarchicalPropertiesException
        {
            return HierarchicalPropertiesUtils.getFloat(this, "float", null);
        }

        @UserProperty(
                name = "float",
                displayNameKey = UserProperty.BASE_KEY + "float" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "float" + UserProperty.DESCRIPTION_KEY
        )
        public void setFloat(Float value)
        throws HierarchicalPropertiesException
        {
            HierarchicalPropertiesUtils.setFloat(this, "float", value);
        }

        @UserProperty(
                name = "bytearray",
                displayNameKey = UserProperty.BASE_KEY + "bytearray" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "bytearray" + UserProperty.DESCRIPTION_KEY
        )
        public ByteArray getBytearray()
        throws HierarchicalPropertiesException
        {
            return HierarchicalPropertiesUtils.getByteArray(this, "bytearray", null);
        }

        @UserProperty(
                name = "bytearray",
                displayNameKey = UserProperty.BASE_KEY + "bytearray" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "bytearray" + UserProperty.DESCRIPTION_KEY
        )
        public void setBytearray(ByteArray value)
        throws HierarchicalPropertiesException
        {
            HierarchicalPropertiesUtils.setByteArray(this, "bytearray", value);
        }
    }

    /*
     * Date should not be supported because date conversion is error-prone. For storing dates in a property, the
     * date should be stored as a long timestamp
     */
    public static class DateShouldNotBeSupported
    {
        @UserProperty(
                name = "test",
                displayNameKey = UserProperty.BASE_KEY + "test" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "test" + UserProperty.DESCRIPTION_KEY
        )
        public Date getDate() {
            return null;
        }
    }

    private HierarchicalProperties properties;

    @Before
    public void setup()
    {
        // extend Properties to allow null property values
        properties = new StandardHierarchicalProperties(new Properties()
            {
                @Override
                public Object setProperty(String key, String value) {
                    return super.setProperty(key, ObjectUtils.defaultIfNull(value, "<null>"));
                }

                @Override
                public String getProperty(String key)
                {
                    String value = super.getProperty(key);

                    if ("<null>".equals(value)) {
                        value = null;
                    }

                    return value;
                }
            }
        );
    }

    @Test
    public void testEnum()
    throws Exception
    {
        userPropertiesAccessor.setProperty(properties, "digest", Digest.SHA512.name());
        assertEquals(Digest.SHA512.name(), userPropertiesAccessor.getProperty(properties, "digest"));

        userPropertiesAccessor.setProperty(properties, "digest", null);
        assertNull(userPropertiesAccessor.getProperty(properties, "digest"));
    }

    @Test
    public void testEnumInvalid()
    {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertiesAccessor.setProperty(properties, "digest", "invalid"));

        assertEquals("No enum constant com.ciphermail.core.common.security.digest.Digest.invalid",
                e.getMessage());
    }

    @Test
    public void testBoolean()
    throws Exception
    {
        userPropertiesAccessor.setProperty(properties, "bool",  "true");
        assertEquals("true", userPropertiesAccessor.getProperty(properties, "bool"));

        userPropertiesAccessor.setProperty(properties, "bool",  "false");
        assertEquals("false", userPropertiesAccessor.getProperty(properties, "bool"));

        userPropertiesAccessor.setProperty(properties, "bool",  null);
        assertEquals("false", userPropertiesAccessor.getProperty(properties, "bool"));

        userPropertiesAccessor.setProperty(properties, "bool",  "");
        assertEquals("false", userPropertiesAccessor.getProperty(properties, "bool"));

        // BooleanConverter:
        // By default, any object whose string representation is one of the values {"yes", "y", "true", "on", "1"}
        // is converted to Boolean.TRUE, and string representations {"no", "n", "false", "off", "0"} are converted to Boolean.FALSE.
        // https://commons.apache.org/proper/commons-beanutils/javadocs/v1.9.4/apidocs/index.html

        userPropertiesAccessor.setProperty(properties, "bool",  "yes");
        assertEquals("true", userPropertiesAccessor.getProperty(properties, "bool"));

        userPropertiesAccessor.setProperty(properties, "bool",  "no");
        assertEquals("false", userPropertiesAccessor.getProperty(properties, "bool"));
    }

    @Test
    public void testBooleanInvalid()
    {
        ConversionException e = assertThrows(ConversionException.class,
                () -> userPropertiesAccessor.setProperty(properties, "bool",  "invalid"));

        Assert.assertEquals("Can't convert value 'invalid' to type class java.lang.Boolean", e.getMessage());
    }

    @Test
    public void testInteger()
    throws Exception
    {
        userPropertiesAccessor.setProperty(properties, "integer", "123");
        assertEquals("123", userPropertiesAccessor.getProperty(properties, "integer"));

        userPropertiesAccessor.setProperty(properties, "integer", "");
        assertNull(userPropertiesAccessor.getProperty(properties, "integer"));

        userPropertiesAccessor.setProperty(properties, "integer", null);
        assertNull(userPropertiesAccessor.getProperty(properties, "integer"));
    }

    @Test
    public void testIntegerInvalid()
    {
        ConversionException e = assertThrows(ConversionException.class,
                () -> userPropertiesAccessor.setProperty(properties, "integer",  "invalid"));

        Assert.assertEquals("Error converting from 'String' to 'Integer' For input string: \"invalid\"",
                e.getMessage());
    }

    @Test
    public void testLong()
    throws Exception
    {
        userPropertiesAccessor.setProperty(properties, "long", "123");
        assertEquals("123", userPropertiesAccessor.getProperty(properties, "long"));

        userPropertiesAccessor.setProperty(properties, "long", "");
        assertNull(userPropertiesAccessor.getProperty(properties, "long"));

        userPropertiesAccessor.setProperty(properties, "long", null);
        assertNull(userPropertiesAccessor.getProperty(properties, "long"));
    }

    @Test
    public void testLongInvalid()
    {
        ConversionException e = assertThrows(ConversionException.class,
                () -> userPropertiesAccessor.setProperty(properties, "long",  "invalid"));

        Assert.assertEquals("Error converting from 'String' to 'Long' For input string: \"invalid\"",
                e.getMessage());
    }

    @Test
    public void testDouble()
    throws Exception
    {
        userPropertiesAccessor.setProperty(properties, "double", "123.456");
        assertEquals("123.456", userPropertiesAccessor.getProperty(properties, "double"));

        userPropertiesAccessor.setProperty(properties, "double", "");
        assertNull(userPropertiesAccessor.getProperty(properties, "double"));

        userPropertiesAccessor.setProperty(properties, "double", null);
        assertNull(userPropertiesAccessor.getProperty(properties, "double"));
    }

    @Test
    public void testDoubleInvalid()
    {
        ConversionException e = assertThrows(ConversionException.class,
                () -> userPropertiesAccessor.setProperty(properties, "double",  "invalid"));

        Assert.assertEquals("Error converting from 'String' to 'Double' For input string: \"invalid\"",
                e.getMessage());
    }

    @Test
    public void testFloat()
    throws Exception
    {
        userPropertiesAccessor.setProperty(properties, "float", "123.456");
        assertEquals("123.456", userPropertiesAccessor.getProperty(properties, "float"));

        userPropertiesAccessor.setProperty(properties, "float", "");
        assertNull(userPropertiesAccessor.getProperty(properties, "float"));

        userPropertiesAccessor.setProperty(properties, "float", null);
        assertNull(userPropertiesAccessor.getProperty(properties, "float"));
    }

    @Test
    public void testFloatInvalid()
    {
        ConversionException e = assertThrows(ConversionException.class,
                () -> userPropertiesAccessor.setProperty(properties, "float",  "invalid"));

        Assert.assertEquals("Error converting from 'String' to 'Float' For input string: \"invalid\"",
                e.getMessage());
    }

    @Test
    public void testDateShouldNotBeSupported()
    {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                        new UserPropertiesAccessorImpl(
                                new UserPropertyRegistryImpl(new MockEnvironment()).addClasses(mockUserPropertiesType,
                                        DateShouldNotBeSupported.class),
                                new BeanUtilsBeanBuilderImpl(),
                                new UserPropertiesFactoryRegistryImpl(applicationContext))
                );

        Assert.assertEquals("There is no converter for class class java.util.Date", e.getMessage());
    }

    @Test
    public void testByteArray()
    throws Exception
    {
        userPropertiesAccessor.setProperty(properties, "bytearray", "AQID");
        assertEquals("AQID", userPropertiesAccessor.getProperty(properties, "bytearray"));
        userPropertiesAccessor.setProperty(properties, "bytearray", null);
        assertNull(userPropertiesAccessor.getProperty(properties, "bytearray"));
    }
}