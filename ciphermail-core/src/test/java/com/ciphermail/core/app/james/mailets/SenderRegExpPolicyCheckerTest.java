/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.dlp.PolicyPatternManager;
import com.ciphermail.core.app.dlp.PolicyPatternNode;
import com.ciphermail.core.app.dlp.UserPreferencesPolicyPatternManager;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.dlp.PolicyViolation;
import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.core.common.dlp.Validator;
import com.ciphermail.core.common.dlp.ValidatorRegistry;
import com.ciphermail.core.common.dlp.impl.PolicyPatternImpl;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SenderRegExpPolicyCheckerTest
{
    /*
     * Test Validator which returns false is the input matches a certain string
     */
    static class DoNotValidateOnStaticStringValidator implements Validator
    {
        private final String name;
        private final String value;

        DoNotValidateOnStaticStringValidator(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getDescription() {
            return null;
        }

        @Override
        public boolean isValid(String input) {
            return !StringUtils.equals(input, value);
        }
    }

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private NamedBlobManager namedBlobManager;

    @Autowired
    private PolicyPatternManager policyPatternManager;

    @Autowired
    private UserPreferencesPolicyPatternManager userPreferencesPolicyPatternManager;

    @Autowired
    private ValidatorRegistry validatorRegistry;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                namedBlobManager.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        validatorRegistry.addValidator(new DoNotValidateOnStaticStringValidator("do-not-match-on-test", "test"));

        addPattern("WARN", "test", PolicyViolationAction.WARN);
        addPattern("MUST_ENCRYPT", "test", PolicyViolationAction.MUST_ENCRYPT);
        addPattern("QUARANTINE", "test", PolicyViolationAction.QUARANTINE);
        addPattern("BLOCK", "test", PolicyViolationAction.BLOCK);
        addPattern("TES", "tes", PolicyViolationAction.WARN);
        addPattern("WARN_MATCH_ALL", ".*", PolicyViolationAction.WARN);
        addPattern("WORD", "detected-mime-type: application/x-tika-msoffice", PolicyViolationAction.QUARANTINE);

        addGroup("GROUP");
        addChild("GROUP", "CHILD", ".*", PolicyViolationAction.WARN);

        addGroup("RECURSIVE1");
        addGroup("RECURSIVE2");
        addChild("RECURSIVE1", "RECURSIVE2");
        addChild("RECURSIVE2", "RECURSIVE1");
        addChild("RECURSIVE2", "CHILD2", ".*", PolicyViolationAction.WARN);

        addPattern("QUARANTINE_DELAY", "test", PolicyViolationAction.QUARANTINE, true, null);

        addPattern("TEST_WITH_VALIDATOR", "test", PolicyViolationAction.WARN, false, "do-not-match-on-test");
        addPattern("TES_WITH_VALIDATOR", "tes", PolicyViolationAction.WARN, false, "do-not-match-on-test");
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void addPattern(String name, String regExp, PolicyViolationAction priority) {
        addPattern(name, regExp, priority, false, null);
    }

    private void addPattern(String name, String regExp, PolicyViolationAction priority, boolean delayEvaluation,
        String validatorName)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            PolicyPatternNode node = policyPatternManager.createPattern(name);

            PolicyPatternImpl pattern = new PolicyPatternImpl(name, Pattern.compile(regExp));

            pattern.setAction(priority);
            pattern.setDelayEvaluation(delayEvaluation);
            pattern.setValidatorName(validatorName);

            node.setPolicyPattern(pattern);
        });
    }

    private void addGroup(String name)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            policyPatternManager.createPattern(name);
        });
    }

    private void addChild(String parentName, String childName, String regExp, PolicyViolationAction priority)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            PolicyPatternNode parent = policyPatternManager.getPattern(parentName);

            PolicyPatternImpl pattern = new PolicyPatternImpl(childName, Pattern.compile(regExp));

            pattern.setAction(priority);

            PolicyPatternNode childNode = policyPatternManager.createPattern(childName);

            childNode.setPolicyPattern(pattern);

            parent.getChilds().add(childNode);
        });
    }

    private void addChild(String parentName, String childName)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            PolicyPatternNode child = policyPatternManager.getPattern(childName);
            PolicyPatternNode parent = policyPatternManager.getPattern(parentName);

            parent.getChilds().add(child);
        });
    }

    private void addUserPatterns(String email, String... patternNames)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                userWorkflow.makePersistent(user);

                Collection<PolicyPatternNode> nodes = userPreferencesPolicyPatternManager.getPatterns(
                        user.getUserPreferences());

                for (String patternName : patternNames) {
                    nodes.add(policyPatternManager.getPattern(patternName));
                }

                userPreferencesPolicyPatternManager.setPatterns(user.getUserPreferences(), nodes);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private SenderRegExpPolicyChecker createDefaultSenderRegExpPolicyChecker()
    throws MessagingException
    {
        return createDefaultSenderRegExpPolicyChecker(null);
    }

    private SenderRegExpPolicyChecker createDefaultSenderRegExpPolicyChecker(Map<String, String> additionalProperties)
    throws MessagingException
    {
        SenderRegExpPolicyChecker mailet = new SenderRegExpPolicyChecker();

        FakeMailetConfig.Builder mailetConfigBuilder = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("warnProcessor", "warnProcessor")
                .setProperty("mustEncryptProcessor", "mustEncryptProcessor")
                .setProperty("quarantineProcessor", "quarantineProcessor")
                .setProperty("blockProcessor", "blockProcessor")
                .setProperty("errorProcessor", "errorProcessor")
                .setProperty("delayEvaluationProcessor", "delayEvaluationProcessor");

        if (additionalProperties != null)
        {
            for (Map.Entry<String, String> property : additionalProperties.entrySet()) {
                mailetConfigBuilder.setProperty(property.getKey(), property.getValue());
            }
        }

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfigBuilder.build());

        return mailet;
    }

    public Mail testViolation(String... policyNames)
    throws Exception
    {
        return testViolation(null, policyNames);
    }

    public Mail testViolation(Map<String, String> additionalProperties, String... policyNames)
    throws Exception
    {
        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test2@example.com")
                .sender("test@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/normal-message-with-attach.eml"))
                .state(Mail.DEFAULT)
                .build();

        for (String policyName : policyNames) {
            addUserPatterns("test@example.com", policyName);
        }

        SenderRegExpPolicyChecker mailet = createDefaultSenderRegExpPolicyChecker(additionalProperties);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        return mail;
    }

    @Test
    public void testViolateWARN()
    throws Exception
    {
        Mail mail = testViolation("WARN");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: WARN, Priority: WARN, Delayed Evaluation: false"));

        assertEquals("warnProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        assertEquals("Policy: RegExp, Rule: WARN, Priority: WARN, Match: test, test, test, test, test, " +
                "test, test, test, test, test, test", violation.toString());

        assertEquals(CoreApplicationMailAttributes.getMainPolicyViolation(mail).toString(), violation.toString());
    }

    @Test
    public void testViolateWARNMatchAll()
    throws Exception
    {
        Mail mail = testViolation("WARN_MATCH_ALL");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: WARN_MATCH_ALL, Priority: WARN, Delayed Evaluation: false"));

        assertEquals("warnProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        // Somehow the end of line seems to be matched separately
        assertEquals("Policy: RegExp, Rule: WARN_MATCH_ALL, Priority: WARN, Match: to: <test@example.com> subject: " +
                "normal message attachment dat..., , \"content-type: text/plain; charset=\"\"windows-1252\"\" " +
                "content-tran...\", , test , , detected-mime-type: text/plain detected-mime-type: applicatio..., , \"" +
                "content-type: text/html; charset=\"\"windows-1252\"\" content-trans...\", , test , , " +
                "detected-mime-type: text/html detected-mime-type: text/plain ..., , \"" +
                "content-type: application/octet-stream; name=\"\"cityinfo.zip\"\" c...\", , " +
                "detected-mime-type: application/zip detected-mime-type: appli..., ", violation.toString());
    }

    @Test
    public void testViolateMUST_ENCRYPT()
    throws Exception
    {
        Mail mail = testViolation("MUST_ENCRYPT");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: MUST_ENCRYPT, Priority: MUST_ENCRYPT, Delayed Evaluation: false"));

        assertEquals("mustEncryptProcessor", mail.getState());

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        assertEquals("Policy: RegExp, Rule: MUST_ENCRYPT, Priority: MUST_ENCRYPT, Match: test, test, test, test, " +
                "test, test, test, test, test, test, test", violation.toString());
    }

    @Test
    public void testViolateQUARANTINE()
    throws Exception
    {
        Mail mail = testViolation("QUARANTINE");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: QUARANTINE, Priority: QUARANTINE, Delayed Evaluation: false"));

        assertEquals("quarantineProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        assertEquals("Policy: RegExp, Rule: QUARANTINE, Priority: QUARANTINE, Match: test, test, test, test, test, " +
                "test, test, test, test, test, test", violation.toString());
    }

    @Test
    public void testViolateBLOCK()
    throws Exception
    {
        addUserPatterns("test@example.com", "WARN");

        Mail mail = testViolation("BLOCK");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: BLOCK, Priority: BLOCK, Delayed Evaluation: false"));

        assertEquals("blockProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(2, violations.size());

        Iterator<PolicyViolation> it = violations.iterator();

        PolicyViolation violation = it.next();

        assertEquals("Policy: RegExp, Rule: BLOCK, Priority: BLOCK, Match: test, test, test, test, test, test, " +
                "test, test, test, test, test", violation.toString());

        violation = it.next();

        assertEquals("Policy: RegExp, Rule: WARN, Priority: WARN, Match: test, test, test, test, test, test, " +
                "test, test, test, test, test", violation.toString());
    }

    @Test
    public void testMaxStringLength()
    throws Exception
    {
        Mail mail = testViolation(Map.of("maxStringLength", "3"), "TES");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: TES, Priority: WARN, Delayed Evaluation: false"));

        assertEquals("warnProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        // test is matched multiple times because of partial and overlap
        assertEquals("Policy: RegExp, Rule: TES, Priority: WARN, Match: tes, tes, tes, tes, tes, tes, tes, tes, tes, " +
                     "tes, tes, tes", violation.toString());
    }

    @Test
    public void testChildPattern()
    throws Exception
    {
        Mail mail = testViolation("GROUP");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: CHILD, Priority: WARN, Delayed Evaluation: false"));

        assertEquals("warnProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        assertEquals("Policy: RegExp, Rule: CHILD, Priority: WARN, Match: to: <test@example.com> subject: " +
                "normal message attachment dat..., , \"content-type: text/plain; charset=\"\"windows-1252\"\" " +
                "content-tran...\", , test , , detected-mime-type: text/plain detected-mime-type: applicatio..., , \"" +
                "content-type: text/html; charset=\"\"windows-1252\"\" content-trans...\", , test , , " +
                "detected-mime-type: text/html detected-mime-type: text/plain ..., , \"" +
                "content-type: application/octet-stream; name=\"\"cityinfo.zip\"\" c...\", , " +
                "detected-mime-type: application/zip detected-mime-type: appli..., ", violation.toString());
    }

    @Test
    public void testRecursive()
    throws Exception
    {
        Mail mail = testViolation("RECURSIVE1");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: CHILD2, Priority: WARN, Delayed Evaluation: false"));

        assertEquals("warnProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        assertEquals("Policy: RegExp, Rule: CHILD2, Priority: WARN, Match: to: <test@example.com> subject: " +
                "normal message attachment dat..., , \"content-type: text/plain; charset=\"\"windows-1252\"\" " +
                "content-tran...\", , test , , detected-mime-type: text/plain detected-mime-type: applicatio..., , \"" +
                "content-type: text/html; charset=\"\"windows-1252\"\" content-trans...\", , test , , " +
                "detected-mime-type: text/html detected-mime-type: text/plain ..., , \"" +
                "content-type: application/octet-stream; name=\"\"cityinfo.zip\"\" c...\", , " +
                "detected-mime-type: application/zip detected-mime-type: appli..., ", violation.toString());
    }

    @Test
    public void testWordDoc()
    throws Exception
    {
        SenderRegExpPolicyChecker mailet = createDefaultSenderRegExpPolicyChecker();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test2@example.com")
                .sender("martijn@djigzo.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/test_word_doc_attached.eml"))
                .state(Mail.DEFAULT)
                .build();

        addUserPatterns("martijn@djigzo.com", "WORD");

        memoryLogAppender.reset();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: WORD, Priority: QUARANTINE, Delayed Evaluation: false"));

        assertEquals("quarantineProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        assertEquals("Policy: RegExp, Rule: WORD, Priority: QUARANTINE, Match: detected-mime-type: " +
                     "application/x-tika-msoffice", violation.toString());
    }

    @Test
    public void testPriorities()
    throws Exception
    {
        Mail mail = testViolation("WARN", "QUARANTINE");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: QUARANTINE, Priority: QUARANTINE, Delayed Evaluation: false"));

        assertEquals("quarantineProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        List<PolicyViolation> violations = new ArrayList<>(CoreApplicationMailAttributes.getPolicyViolations(mail));

        assertEquals(2, violations.size());

        PolicyViolation violation = violations.get(0);

        assertEquals("Policy: RegExp, Rule: QUARANTINE, Priority: QUARANTINE, Match: test, test, test, test, test, " +
                "test, test, test, test, test, test", violation.toString());

        assertEquals(violation.toString(), CoreApplicationMailAttributes.getMainPolicyViolation(mail).toString());

        violation = violations.get(1);

        assertEquals("Policy: RegExp, Rule: WARN, Priority: WARN, Match: test, test, test, test, test, test, " +
                "test, test, test, test, test", violation.toString());

        assertFalse(violation.isDelayEvaluation());
    }

    @Test
    public void testDelay()
    throws Exception
    {
        Mail mail = testViolation("QUARANTINE_DELAY");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: QUARANTINE_DELAY, Priority: QUARANTINE, Delayed Evaluation: true"));

        assertEquals("delayEvaluationProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        List<PolicyViolation> violations = new ArrayList<>(CoreApplicationMailAttributes.getPolicyViolations(mail));

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.get(0);

        assertEquals("Policy: RegExp, Rule: QUARANTINE_DELAY, Priority: QUARANTINE, Match: test, test, test, test, "
            + "test, test, test, test, test, test, test", violation.toString());

        assertTrue(violation.isDelayEvaluation());
    }

    @Test
    public void testDelayMultipleViolationsSamePriority()
    throws Exception
    {
        Mail mail = testViolation("QUARANTINE", "QUARANTINE_DELAY");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: QUARANTINE, Priority: QUARANTINE, Delayed Evaluation: false"));

        assertEquals("quarantineProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        List<PolicyViolation> violations = new ArrayList<>(CoreApplicationMailAttributes.getPolicyViolations(mail));

        assertEquals(2, violations.size());

        PolicyViolation violation = violations.get(0);

        assertEquals("Policy: RegExp, Rule: QUARANTINE, Priority: QUARANTINE, Match: test, test, test, test, "
                + "test, test, test, test, test, test, test", violation.toString());

        assertEquals(violation.toString(), CoreApplicationMailAttributes.getMainPolicyViolation(mail).toString());

        violation = violations.get(1);

        assertEquals("Policy: RegExp, Rule: QUARANTINE_DELAY, Priority: QUARANTINE, Match: test, test, test, test, "
                + "test, test, test, test, test, test, test", violation.toString());

        assertFalse(CoreApplicationMailAttributes.getMainPolicyViolation(mail).isDelayEvaluation());
    }

    @Test
    public void testDelayMultipleViolationsDifferentPriority()
    throws Exception
    {
        Mail mail = testViolation("QUARANTINE_DELAY", "WARN");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: QUARANTINE_DELAY, Priority: QUARANTINE, Delayed Evaluation: true"));

        assertEquals("delayEvaluationProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        List<PolicyViolation> violations = new ArrayList<>(CoreApplicationMailAttributes.getPolicyViolations(mail));

        assertEquals(2, violations.size());

        PolicyViolation violation = violations.get(0);

        assertEquals("Policy: RegExp, Rule: QUARANTINE_DELAY, Priority: QUARANTINE, Match: test, test, test, test, "
            + "test, test, test, test, test, test, test", violation.toString());

        assertEquals(violation.toString(), CoreApplicationMailAttributes.getMainPolicyViolation(mail).toString());

        violation = violations.get(1);

        assertEquals("Policy: RegExp, Rule: WARN, Priority: WARN, Match: test, test, test, test, test, test, " +
                "test, test, test, test, test", violation.toString());

        assertTrue(CoreApplicationMailAttributes.getMainPolicyViolation(mail).isDelayEvaluation());
    }

    @Test
    public void testWithValidateNoMatch()
    throws Exception
    {
        Mail mail = testViolation("TEST_WITH_VALIDATOR");

        checkLogsForErrorsOrExceptions();

        assertEquals(Mail.DEFAULT, mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        assertNull(CoreApplicationMailAttributes.getPolicyViolations(mail));
    }

    @Test
    public void testWithValidateMatch()
    throws Exception
    {
        Mail mail = testViolation("TES_WITH_VALIDATOR");

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: TES_WITH_VALIDATOR, Priority: WARN, Delayed Evaluation: false"));

        assertEquals("warnProcessor", mail.getState());

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        assertEquals("Policy: RegExp, Rule: TES_WITH_VALIDATOR, Priority: WARN, Match: tes, tes, tes, tes, tes, tes, "
            + "tes, tes, tes, tes, tes", violation.toString());

        assertEquals(CoreApplicationMailAttributes.getMainPolicyViolation(mail).toString(), violation.toString());
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        MutableBoolean constraintViolationThrown = new MutableBoolean();

        SenderRegExpPolicyChecker mailet = new SenderRegExpPolicyChecker()
        {
            @Override
            protected void onHandleUserEvent(User user)
            {
                /*
                 * throw ConstraintViolationException for first time a user is handled
                 */
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
                super.onHandleUserEvent(user);
            }
        };

        FakeMailetConfig.Builder mailetConfigBuilder = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("warnProcessor", "warnProcessor")
                .setProperty("mustEncryptProcessor", "mustEncryptProcessor")
                .setProperty("quarantineProcessor", "quarantineProcessor")
                .setProperty("blockProcessor", "blockProcessor")
                .setProperty("errorProcessor", "errorProcessor")
                .setProperty("delayEvaluationProcessor", "delayEvaluationProcessor");

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfigBuilder.build());

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test2@example.com")
                .sender("martijn@djigzo.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/normal-message-with-attach.eml"))
                .state(Mail.DEFAULT)
                .build();

        addUserPatterns("test@example.com", "WARN");

        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Policy was violated. Rule: WARN, Priority: WARN, Delayed Evaluation: false"));

        assertTrue(constraintViolationThrown.booleanValue());

        assertEquals("warnProcessor", mail.getState());

        assertNull(CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        Collection<PolicyViolation> violations = CoreApplicationMailAttributes.getPolicyViolations(mail);

        assertEquals(1, violations.size());

        PolicyViolation violation = violations.iterator().next();

        assertEquals("Policy: RegExp, Rule: WARN, Priority: WARN, Match: test, test, test, test, test, " +
                "test, test, test, test, test, test", violation.toString());

        assertEquals(CoreApplicationMailAttributes.getMainPolicyViolation(mail).toString(), violation.toString());
    }

    @Test
    public void testErrorHandling()
    throws Exception
    {
        SenderRegExpPolicyChecker mailet = createDefaultSenderRegExpPolicyChecker();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("test@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/normal-message-with-attach.eml"))
                .state(Mail.DEFAULT)
                .build();

        addUserPatterns("test@example.com", "WARN");

        memoryLogAppender.reset();

        // override TextNormalizer to always throw an exception
        mailet.setTextNormalizer((input, output) ->
        {
            throw new IOException("Thrown on purpose");
        });

        mailet.service(mail);

        assertEquals("errorProcessor", mail.getState());
        assertEquals("IOException: Thrown on purpose",
                CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Error extracting text from email. Message: IOException: Thrown on purpose"));
    }
}
