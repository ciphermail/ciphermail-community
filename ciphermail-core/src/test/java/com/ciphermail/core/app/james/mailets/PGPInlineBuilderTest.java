/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PGPPublicKeys;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporterImpl;
import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.common.security.openpgp.PGPTestUtils;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListEntry;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListException;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.james.server.core.MimeMessageInputStreamSource;
import org.apache.james.server.core.MimeMessageWrapper;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPInlineBuilderTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPTrustList trustList;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    @Before
    public void before()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
                trustList.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void importKeyRing(File file, String password)
    {
        transactionOperations.execute(status ->
        {
            try {
                return new PGPKeyRingImporterImpl(keyRing).importKeyRing(FileUtils.openInputStream(file),
                        new StaticPasswordProvider(password)).size();
            }
            catch (IOException | PGPException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void trustKey(long keyID) {
        trustKey(keyID, false);
    }

    private void trustKey(long keyID, boolean includeSubKeys)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry keyEntry = CloseableIteratorUtils.toList(keyRing.getByKeyID(keyID)).get(0);

                PGPTrustListEntry trustEntry = trustList.createEntry(keyEntry.getPublicKey());

                trustEntry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(trustEntry, includeSubKeys);
            }
            catch (IOException | PGPTrustListException | CloseableIteratorException | PGPException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new PGPInlineBuilder();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testPGPInlineBuilderSign()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(), "pgp/test@example.com.gpg.key"),
                "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attach-encoded-filename.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed. Hash algorithm: SHA256"));

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        assertNull(mail.getState());

        assertEquals("Partitioned", newMessage.getHeader("X-PGP-Encoding-Format", ","));

        List<Part> parts = PGPTestUtils.getAllParts(newMessage);

        assertEquals(4, parts.size());

        Writer output = new StringWriter();
        Writer error = new StringWriter();

        Part part = parts.get(0);

        assertEquals("text/plain; charset=us-ascii", part.getContentType());

        assertTrue(PGPTestUtils.verify(part, output, error));
        assertThat(error.toString(), containsString("armor: BEGIN PGP SIGNED MESSAGE"));
        assertThat(error.toString(), containsString("Hash: SHA256"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo " +
                "(this is a test key) <test@example.com>\""));
        assertThat(error.toString(), containsString("textmode signature, digest algorithm SHA256"));

        part = parts.get(1);

        assertEquals("application/octet-stream; name=\"PGPexch.htm.asc\"", part.getContentType());
        assertEquals("text/html; charset=UTF-8", StringUtils.join(part.getHeader(
                "X-Content-PGP-Universal-Saved-Content-Type"), ","));
        assertEquals("alternative", StringUtils.join(part.getHeader(
                "X-PGP-MIME-Structure"), ","));

        output = new StringWriter();
        error = new StringWriter();

        assertTrue(PGPTestUtils.verify(part, output, error));

        part = parts.get(2);

        assertEquals("application/octet-stream;\r\n\tname=\"cityinfo.zip\"", part.getContentType());
        assertEquals("attachment", part.getDisposition());
        assertEquals("=?ISO-8859-1?Q?=E4=F6=FC_=C4=D6=DC?=.zip", part.getFileName());

        part = parts.get(3);

        assertEquals("application/octet-stream; \r\n\tname=\"=?UTF-8?Q?=C3=A4=C3=B6=C3=BC_=C3=84=C3=96=C3=" +
        		"9C.zip.sig?=\"", part.getContentType());
        assertEquals("attachment", part.getDisposition());
        assertEquals("=?UTF-8?Q?=C3=A4=C3=B6=C3=BC_=C3=84=C3=96=C3=9C.zip.sig?=", part.getFileName());

        output = new StringWriter();
        error = new StringWriter();

        // Verify the detached signature
        assertTrue(PGPTestUtils.verifyDetached(parts.get(2), false, parts.get(3), output, error));

        assertThat(error.toString(), containsString("armor: BEGIN PGP SIGNATURE"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo " +
                "(this is a test key) <test@example.com>\""));
        assertThat(error.toString(), containsString("binary signature, digest algorithm SHA256"));
    }

    @Test
    public void testPGPInlineBuilderSignSetProcessor()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(), "pgp/test@example.com.gpg.key"),
                "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signedProcessor", "signed")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attach-encoded-filename.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed. Hash algorithm: SHA256"));

        assertEquals("signed", mail.getState());
    }

    @Test
    public void testPGPInlineBuilderEncrypt()
    throws Exception
    {
        List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(
                new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc")));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attach-encoded-filename.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setPGPPublicKeys(mail, new PGPPublicKeys(publicKeys));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE encrypted. Encryption algorithm: AES-128; " +
                "Compression algorithm: ZLIB; Add integrity packet: true"));

        assertNull(mail.getState());

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        assertNull(mail.getState());

        assertTrue(newMessage.isMimeType("multipart/mixed"));
        assertEquals("Partitioned", newMessage.getHeader("X-PGP-Encoding-Format", ","));

        List<Part> parts = PGPTestUtils.getAllParts(newMessage);

        assertEquals(3, parts.size());

        Writer output = new StringWriter();
        Writer error = new StringWriter();

        Part part = parts.get(0);

        assertEquals("text/plain; charset=us-ascii", part.getContentType());

        assertTrue(PGPTestUtils.decrypt(part, "test", output, error));

        assertEquals("Test", output.toString());
        assertThat(error.toString(), containsString("armor: BEGIN PGP MESSAGE"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("public key encrypted data: good DEK"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertThat(error.toString(), containsString("AES encrypted data"));
        assertThat(error.toString(), containsString("original file name=''"));
        assertThat(error.toString(), containsString("decryption okay"));

        part = parts.get(1);

        assertEquals("application/octet-stream; name=\"PGPexch.htm.pgp\"", part.getContentType());
        assertEquals("text/html; charset=UTF-8", StringUtils.join(part.getHeader(
                "X-Content-PGP-Universal-Saved-Content-Type"), ","));
        assertEquals("alternative", StringUtils.join(part.getHeader(
                "X-PGP-MIME-Structure"), ","));

        output = new StringWriter();
        error = new StringWriter();

        assertTrue(PGPTestUtils.decrypt(part, "test", output, error));

        assertThat(output.toString(), containsString("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">"));
        assertThat(error.toString(), containsString("armor: BEGIN PGP MESSAGE"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("public key encrypted data: good DEK"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertThat(error.toString(), containsString("AES encrypted data"));
        assertThat(error.toString(), containsString("original file name=''"));
        assertThat(error.toString(), containsString("decryption okay"));

        part = parts.get(2);

        assertEquals("application/octet-stream; \r\n\tname=\"=?UTF-8?Q?=C3=A4=C3=B6=C3=BC_=C3=84=C3=96=C3=" +
                "9C.zip.pgp?=\"", part.getContentType());
        assertEquals("attachment", part.getDisposition());
        assertEquals("=?UTF-8?Q?=C3=A4=C3=B6=C3=BC_=C3=84=C3=96=C3=9C.zip.pgp?=", part.getFileName());

        output = new StringWriter();
        error = new StringWriter();

        assertTrue(PGPTestUtils.decrypt(part, "test", output, error));

        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("public key encrypted data: good DEK"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertThat(error.toString(), containsString("compressed packet: algo=2"));
        assertThat(error.toString(), containsString("AES encrypted data"));
        assertThat(error.toString(), containsString("original file name='äöü ÄÖÜ.zip'"));
        assertThat(error.toString(), containsString("decryption okay"));
    }

    @Test
    public void testPGPInlineBuilderEncryptSetProcessor()
    throws Exception
    {
        List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(
                new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc")));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("encryptedProcessor", "encrypted")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attach-encoded-filename.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setPGPPublicKeys(mail, new PGPPublicKeys(publicKeys));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE encrypted. Encryption algorithm: AES-128; " +
                "Compression algorithm: ZLIB; Add integrity packet: true"));

        assertEquals("encrypted", mail.getState());
    }

    @Test
    public void testPGPInlineBuilderSignEncryptSetProcessor()
    throws Exception
    {
        List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(
                new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc")));

        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("encryptedProcessor", "encrypted")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attach-encoded-filename.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setPGPPublicKeys(mail, new PGPPublicKeys(publicKeys));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed and encrypted. Hash algorithm: SHA256; " +
                "Encryption algorithm: AES-128; Compression algorithm: ZLIB"));

        assertEquals("encrypted", mail.getState());

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        assertTrue(newMessage.isMimeType("multipart/mixed"));
        assertEquals("Partitioned", newMessage.getHeader("X-PGP-Encoding-Format", ","));

        List<Part> parts = PGPTestUtils.getAllParts(newMessage);

        assertEquals(3, parts.size());

        Writer output = new StringWriter();
        Writer error = new StringWriter();

        Part part = parts.get(0);

        assertEquals("text/plain; charset=us-ascii", part.getContentType());

        assertTrue(PGPTestUtils.decrypt(part, "test", output, error));

        assertEquals("Test", output.toString());
        assertThat(error.toString(), containsString("armor: BEGIN PGP MESSAGE"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("public key encrypted data: good DEK"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertThat(error.toString(), containsString("AES encrypted data"));
        assertThat(error.toString(), containsString("compressed packet: algo=2"));
        assertThat(error.toString(), containsString("onepass_sig packet: keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("signature packet: algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo (this is a test key) " +
                                             "<test@example.com>\""));
        assertThat(error.toString(), containsString("textmode signature, digest algorithm SHA256"));
        assertThat(error.toString(), containsString("decryption okay"));

        part = parts.get(1);

        assertEquals("application/octet-stream; name=\"PGPexch.htm.pgp\"", part.getContentType());
        assertEquals("text/html; charset=UTF-8", StringUtils.join(part.getHeader(
                "X-Content-PGP-Universal-Saved-Content-Type"), ","));
        assertEquals("alternative", StringUtils.join(part.getHeader(
                "X-PGP-MIME-Structure"), ","));

        output = new StringWriter();
        error = new StringWriter();

        assertTrue(PGPTestUtils.decrypt(part, "test", output, error));

        assertThat(output.toString(), containsString("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">"));
        assertThat(error.toString(), containsString("armor: BEGIN PGP MESSAGE"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("public key encrypted data: good DEK"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertThat(error.toString(), containsString("AES encrypted data"));
        assertThat(error.toString(), containsString("compressed packet: algo=2"));
        assertThat(error.toString(), containsString("onepass_sig packet: keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("signature packet: algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo (this is a test key) " +
                                             "<test@example.com>\""));
        assertThat(error.toString(), containsString("textmode signature, digest algorithm SHA256"));
        assertThat(error.toString(), containsString("decryption okay"));

        part = parts.get(2);

        assertEquals("application/octet-stream; \r\n\tname=\"=?UTF-8?Q?=C3=A4=C3=B6=C3=BC_=C3=84=C3=96=C3=" +
                     "9C.zip.pgp?=\"", part.getContentType());
        assertEquals("attachment", part.getDisposition());
        assertEquals("=?UTF-8?Q?=C3=A4=C3=B6=C3=BC_=C3=84=C3=96=C3=9C.zip.pgp?=", part.getFileName());

        output = new StringWriter();
        error = new StringWriter();

        assertTrue(PGPTestUtils.decrypt(part, "test", output, error));

        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("public key encrypted data: good DEK"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertThat(error.toString(), containsString("compressed packet: algo=2"));
        assertThat(error.toString(), containsString("AES encrypted data"));
        assertThat(error.toString(), containsString("original file name='äöü ÄÖÜ.zip'"));
        assertThat(error.toString(), containsString("onepass_sig packet: keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("signature packet: algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo (this is a test key) " +
                                             "<test@example.com>\""));
        assertThat(error.toString(), containsString("binary signature, digest algorithm SHA256"));
        assertThat(error.toString(), containsString("decryption okay"));
        assertThat(error.toString(), not(containsString("message was not integrity protected")));
    }

    @Test
    public void testPGPInlineHTMLOnlySign()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/html-only.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed. Hash algorithm: SHA256"));

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        assertNull(mail.getState());

        List<Part> parts = PGPTestUtils.getAllParts(newMessage);

        assertEquals(2, parts.size());

        Writer output = new StringWriter();
        Writer error = new StringWriter();

        Part part = parts.get(0);

        assertEquals("text/plain; charset=UTF-8", part.getContentType());

        assertTrue(PGPTestUtils.verify(part, output, error));

        part = parts.get(1);

        assertEquals("application/octet-stream; name=\"PGPexch.htm.asc\"", part.getContentType());

        assertTrue(PGPTestUtils.verify(part, output, error));
    }

    @Test
    public void testPGPInlineHTMLOnlySignAndEncrypt()
    throws Exception
    {
        List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(
                new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc")));

        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signingAlgorithmAttribute", "non-existent")
                .setProperty("encryptionAlgorithmAttribute", "non-existent")
                .setProperty("compressionAlgorithmAttribute", "non-existent")
                .setProperty("convertHTMLToTextAttribute", "non-existent")
                .setProperty("addIntegrityPacketAttribute", "non-existent")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/html-only.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setPGPPublicKeys(mail, new PGPPublicKeys(publicKeys));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed and encrypted. Hash algorithm: SHA256; " +
                "Encryption algorithm: AES-128; Compression algorithm: ZLIB"));

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        assertNull(mail.getState());

        List<Part> parts = PGPTestUtils.getAllParts(newMessage);

        assertEquals(2, parts.size());

        Writer output = new StringWriter();
        Writer error = new StringWriter();

        Part part = parts.get(0);

        assertEquals("text/plain; charset=us-ascii", part.getContentType());

        assertTrue(PGPTestUtils.decrypt(part, "test", output, error));

        // Check Unicode chars
        assertThat(output.toString(), containsString("Türkiyenin En Büyük"));

        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("public key encrypted data: good DEK"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertThat(error.toString(), containsString("AES encrypted data"));
        assertThat(error.toString(), containsString("compressed packet: algo=2"));
        assertThat(error.toString(), containsString("onepass_sig packet: keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("signature packet: algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo (this is a test key) " +
                "<test@example.com>\""));
        assertThat(error.toString(), containsString("textmode signature, digest algorithm SHA256"));
        assertThat(error.toString(), containsString("decryption okay"));

        part = parts.get(1);

        assertEquals("application/octet-stream; name=\"PGPexch.htm.pgp\"", part.getContentType());

        assertTrue(PGPTestUtils.decrypt(part, "test", output, error));

        /*
         * Check Unicode chars
         */
        assertThat(output.toString(), containsString("Türkiyenin En Büyük"));

        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("public key encrypted data: good DEK"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertThat(error.toString(), containsString("AES encrypted data"));
        assertThat(error.toString(), containsString("compressed packet: algo=2"));
        assertThat(error.toString(), containsString("onepass_sig packet: keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("signature packet: algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo (this is a test key) " +
                "<test@example.com>\""));
        assertThat(error.toString(), containsString("textmode signature, digest algorithm SHA256"));
        assertThat(error.toString(), containsString("decryption okay"));
    }

    @Test
    public void testPGPMailAttributes()
    throws Exception
    {
        List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(
                new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc")));

        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signingAlgorithmAttribute", "attr.signingAlgorithm")
                .setProperty("encryptionAlgorithmAttribute", "attr.encryptionAlgorithm")
                .setProperty("compressionAlgorithmAttribute", "attr.compressionAlgorithm")
                .setProperty("convertHTMLToTextAttribute", "attr.convertHTMLToText")
                .setProperty("addIntegrityPacketAttribute", "attr.addIntegrityPacket")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/html-only.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setPGPPublicKeys(mail, new PGPPublicKeys(publicKeys));

        mail.setAttribute(Attribute.convertToAttribute("attr.signingAlgorithm", "SHA512"));
        mail.setAttribute(Attribute.convertToAttribute("attr.encryptionAlgorithm", "Twofish"));
        mail.setAttribute(Attribute.convertToAttribute("attr.compressionAlgorithm", "ZIP"));
        mail.setAttribute(Attribute.convertToAttribute("attr.convertHTMLToText", "false"));
        mail.setAttribute(Attribute.convertToAttribute("attr.addIntegrityPacket", "false"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed and encrypted. Hash algorithm: SHA512; " +
                "Encryption algorithm: Twofish; Compression algorithm: ZIP"));

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        assertNull(mail.getState());

        List<Part> parts = PGPTestUtils.getAllParts(newMessage);

        assertEquals(1, parts.size());

        Writer output = new StringWriter();
        Writer error = new StringWriter();

        Part part = parts.get(0);

        assertTrue(PGPTestUtils.decrypt(part, "test", output, error, true /* ignore-mdc-error */));

        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("public key encrypted data: good DEK"));
        assertThat(error.toString(), containsString("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertThat(error.toString(), containsString("TWOFISH encrypted data"));
        assertThat(error.toString(), containsString("compressed packet: algo=1"));
        assertThat(error.toString(), containsString("onepass_sig packet: keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("signature packet: algo 1, keyid 4EC4E8813E11A9A0"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo (this is a test key) " +
                                                    "<test@example.com>\""));
        assertThat(error.toString(), containsString("textmode signature, digest algorithm SHA512"));
        assertThat(error.toString(), containsString("decryption okay"));
        assertThat(error.toString(), containsString("WARNING: message was not integrity protected"));
    }

    @Test
    public void testPGPInlineHTMLAttachmentSign()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attached-html.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed. Hash algorithm: SHA256"));

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        assertNull(mail.getState());

        List<Part> parts = PGPTestUtils.getAllParts(newMessage);

        assertEquals(3, parts.size());

        Writer output = new StringWriter();
        Writer error = new StringWriter();

        Part part = parts.get(0);

        // first part should be the converted HTML to text
        assertEquals("text/plain; charset=UTF-8", part.getContentType());

        assertTrue(PGPTestUtils.verify(part, output, error));

        part = parts.get(1);

        assertEquals("text/plain; charset=us-ascii", part.getContentType());

        assertTrue(PGPTestUtils.verify(part, output, error));

        part = parts.get(2);

        assertEquals("application/octet-stream; name=test.htm", part.getContentType());

        assertTrue(PGPTestUtils.verify(part, output, error));
    }

    @Test
    public void testPGPInlineBuilderMultipartReport()
    throws Exception
    {
        List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(
                new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc")));

        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("encryptedProcessor", "encrypted")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/multipart-report.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setPGPPublicKeys(mail, new PGPPublicKeys(publicKeys));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed and encrypted. Hash algorithm: SHA256; " +
                "Encryption algorithm: AES-128; Compression algorithm: ZLIB"));

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        assertEquals("encrypted", mail.getState());
        assertEquals("Partitioned", newMessage.getHeader("X-PGP-Encoding-Format", ","));
    }

    /*
     * Message with invalid content-transfer-encoding cannot be signed
     */
    @Test
    public void testPGPInlineBuilderSignInvalidContentTransferEncoding()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("encryptedProcessor", "encrypted")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/unknown-content-transfer-encoding.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Unknown encoding: xxx. Retrying (retry count: 1)"));

        assertEquals(MailUtils.partToMimeString(sourceMessage), MailUtils.partToMimeString(mail.getMessage()));
    }

    @Test
    public void testPGPInlineBuilderSign8BitMultipart()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("encryptedProcessor", "encrypted")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/8bit-multipart.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed. Hash algorithm: SHA256"));

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        String mime = MailUtils.partToMimeString(newMessage);

        assertEquals("Partitioned", newMessage.getHeader("X-PGP-Encoding-Format", ","));

        List<Part> parts = PGPTestUtils.getAllParts(newMessage);

        assertEquals(5, parts.size());

        Writer output = new StringWriter();
        Writer error = new StringWriter();

        Part part = parts.get(0);

        assertEquals("from 8bit to 7bit by CipherMail", StringUtils.join(
                part.getHeader("X-MIME-Autoconverted")));
        assertEquals("quoted-printable", StringUtils.join(
                part.getHeader("Content-Transfer-Encoding")));
        assertEquals("text/plain; charset=UTF-8", part.getContentType());
        assertTrue(mime.contains("This is a test with unlauts: Sch=C3=B6n"));

        assertTrue(PGPTestUtils.verify(part, output, error));
        assertThat(error.toString(), containsString("armor: BEGIN PGP SIGNED MESSAGE"));
        assertThat(error.toString(), containsString("Hash: SHA256"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo (this is a test key) " +
                                                    "<test@example.com>\""));
        assertThat(error.toString(), containsString("textmode signature, digest algorithm SHA256"));

        part = parts.get(1);

        assertEquals("from 8bit to 7bit by CipherMail", StringUtils.join(
                part.getHeader("X-MIME-Autoconverted")));
        assertEquals("base64", StringUtils.join(
                part.getHeader("Content-Transfer-Encoding")));

        assertTrue(mime.contains("VGhpcyBpcyBhIHRlc3Qgd2l0aCB1bmxhdXRzOiBTY2jDtm4KCg=="));

        part = parts.get(3);

        assertEquals("from 8bit to 7bit by CipherMail", StringUtils.join(
                part.getHeader("X-MIME-Autoconverted")));
        assertEquals("7bit", StringUtils.join(
                part.getHeader("Content-Transfer-Encoding")));

        assertTrue(mime.contains("no conversion is needed because everything is 7bit"));
    }

    @Test
    public void testMimeMessageWrapper()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("encryptedProcessor", "encrypted")
                .build();

        MimeMessage sourceMessage = new MimeMessageWrapper(MimeMessageInputStreamSource.create(
                MailImpl.getId(), new FileInputStream(new File(TestUtils.getTestDataDir(),
                "mail/8bit-multipart.eml"))));

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/INLINE signed. Hash algorithm: SHA256"));

        // Need to clone the message to make sure that Part#getinputStream works
        MimeMessage newMessage = MailUtils.cloneMessage(mail.getMessage());

        String mime = MailUtils.partToMimeString(newMessage);

        assertEquals("Partitioned", newMessage.getHeader("X-PGP-Encoding-Format", ","));

        List<Part> parts = PGPTestUtils.getAllParts(newMessage);

        assertEquals(5, parts.size());

        Writer output = new StringWriter();
        Writer error = new StringWriter();

        Part part = parts.get(0);

        assertEquals("from 8bit to 7bit by CipherMail", StringUtils.join(
                part.getHeader("X-MIME-Autoconverted")));
        assertEquals("quoted-printable", StringUtils.join(
                part.getHeader("Content-Transfer-Encoding")));
        assertEquals("text/plain; charset=UTF-8", part.getContentType());
        assertTrue(mime.contains("This is a test with unlauts: Sch=C3=B6n"));

        assertTrue(PGPTestUtils.verify(part, output, error));
        assertThat(error.toString(), containsString("armor: BEGIN PGP SIGNED MESSAGE"));
        assertThat(error.toString(), containsString("Hash: SHA256"));
        assertThat(error.toString(), containsString("Good signature from \"test key djigzo (this is a test key) " +
                "<test@example.com>\""));
        assertThat(error.toString(), containsString("textmode signature, digest algorithm SHA256"));

        part = parts.get(1);

        assertEquals("from 8bit to 7bit by CipherMail", StringUtils.join(
                part.getHeader("X-MIME-Autoconverted")));
        assertEquals("base64", StringUtils.join(
                part.getHeader("Content-Transfer-Encoding")));

        assertTrue(mime.contains("VGhpcyBpcyBhIHRlc3Qgd2l0aCB1bmxhdXRzOiBTY2jDtm4KCg=="));

        part = parts.get(3);

        assertEquals("from 8bit to 7bit by CipherMail", StringUtils.join(
                part.getHeader("X-MIME-Autoconverted")));
        assertEquals("7bit", StringUtils.join(
                part.getHeader("Content-Transfer-Encoding")));

        assertTrue(mime.contains("no conversion is needed because everything is 7bit"));
    }

    @Test
    public void testPGPInlineBuilderSignRetry()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        PGPInlineBuilder mailet = new PGPInlineBuilder()
        {
            @Override
            protected void serviceMailTransacted(Mail mail)
            throws MessagingException, IOException
            {
                super.serviceMailTransacted(mail);

                /*
                 * throw ConstraintViolationException for first time a user is handled
                 */
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertTrue(constraintViolationThrown.booleanValue());

        MimeMessage newMessage = mail.getMessage();

        String mime = MailUtils.partToMimeString(newMessage);

        // * The message should be signed only once
        assertEquals(1, StringUtils.countMatches(mime, "-----BEGIN PGP SIGNATURE-----"));
    }
}
