/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.smtp;

import com.ciphermail.core.test.MemoryLogAppenderResource;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.io.FileHandler;
import org.apache.james.core.MaybeSender;
import org.apache.james.protocols.smtp.hook.HookResult;
import org.apache.james.queue.api.MailQueue;
import org.apache.james.queue.api.MailQueueFactory;
import org.apache.james.queue.api.MailQueueName;
import org.apache.james.queue.api.ManageableMailQueue;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.mailet.Mail;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.StringReader;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ThrottlingMailHookTest.LocalServices.class})
public class ThrottlingMailHookTest
{
    static class MockMailQueue implements ManageableMailQueue
    {
        private final MailQueueName mailQueueName;
        private long size;

        MockMailQueue(MailQueueName name) {
            this.mailQueueName = name;
        }

        @Override
        public long getSize() {
            return size;
        }

        public void setSize(long size) {
            this.size = size;
        }

        @Override
        public long flush() {
            return 0;
        }

        @Override
        public long clear()
        {
            size = 0;
            return 0;
        }

        @Override
        public long remove(Type type, String value) {
            return 0;
        }

        @Override
        public MailQueueIterator browse() {
            return null;
        }

        @Override
        public MailQueueName getName() {
            return mailQueueName;
        }

        @Override
        public void enQueue(Mail mail, Duration delay) {
            // empty on purpose
        }

        @Override
        public void enQueue(Mail mail) {
            // empty on purpose
        }

        @Override
        public Publisher<Void> enqueueReactive(Mail mail) {
            return null;
        }

        @Override
        public Publisher<MailQueueItem> deQueue() {
            return null;
        }

        @Override
        public void close() {
            // empty on purpose
        }
    }

    static class MockMailQueueFactory implements MailQueueFactory<MailQueue>
    {
        private final Map<MailQueueName, MailQueue> mailQueues = new HashMap<>();

        @Override
        public Optional<MailQueue> getQueue(MailQueueName name, PrefetchCount count) {
             return Optional.of(mailQueues.computeIfAbsent(name, mailQueueName -> new MockMailQueue(name)));
        }

        @Override
        public MailQueue createQueue(MailQueueName name, PrefetchCount count) {
            return null;
        }

        @Override
        public Set<MailQueueName> listCreatedMailQueues() {
            return mailQueues.values().stream().map(MailQueue::getName).collect(Collectors.toSet());
        }
    }

    @Autowired
    private AbstractApplicationContext applicationContext;

    @BeforeClass
    public static void setUpBeforeClass() {
        Configurator.setLevel(ThrottlingMailHook.class, Level.DEBUG);
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    /*
     * For this test we must create some local beans
     */
    public static class LocalServices
    {
        @Bean
        // make sure we always get a newly created MailQueueFactory
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public MailQueueFactory<?> createMailQueueFactory() {
            return new MockMailQueueFactory();
        }
    }

    private ThrottlingMailHook createThrottlingMailHook(String xmlConfig)
    throws Exception
    {
        XMLConfiguration configuration = new XMLConfiguration();

        FileHandler fh = new FileHandler(configuration);
        fh.load(new StringReader(xmlConfig));

        ThrottlingMailHook mailHook = new ThrottlingMailHook();

        // auto wire mailHook
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailHook);

        mailHook.init(configuration);

        return mailHook;
    }

    /*
     * Max setting is required
     */
    @Test
    public void testMaxRequired()
    {
        String xmlConfig = """
                <handler>
                    <checkInterval>25</checkInterval>
                    <throttling>
                        <limit>50</limit>
                        <delay>200</delay>
                    </throttling>
                </handler>
                """;

        NoSuchElementException e = assertThrows(
                NoSuchElementException.class, () -> createThrottlingMailHook(xmlConfig));

        assertEquals("Key 'max' does not map to an existing object!", e.getMessage());
    }

    /*
     * Max setting is required
     */
    @Test
    public void testCheckIntervalRequired()
    {
        String xmlConfig = """
                <handler>
                    <max>25</max>
                    <throttling>
                        <limit>50</limit>
                        <delay>200</delay>
                    </throttling>
                </handler>
                """;

        NoSuchElementException e = assertThrows(
                NoSuchElementException.class, () -> createThrottlingMailHook(xmlConfig));

        assertEquals("Key 'checkInterval' does not map to an existing object!", e.getMessage());
    }

    /*
     * limit should not decrease
     */
    @Test
    public void testIncreasingLimit()
    {
        String xmlConfig = """
                <handler>
                    <max>600</max>
                    <checkInterval>25</checkInterval>
                    <throttling>
                        <limit>50</limit>
                        <delay>200</delay>
                    </throttling>
                    <throttling>
                        <limit>49</limit>
                        <delay>300</delay>
                    </throttling>
                </handler>
                """;

        IllegalArgumentException e = assertThrows(
                IllegalArgumentException.class, () -> createThrottlingMailHook(xmlConfig));

        assertEquals("Limit cannot decrease: 49", e.getMessage());
    }

    /*
     * delay should not decrease
     */
    @Test
    public void testIncreasingDelay()
    {
        String xmlConfig = """
                <handler>
                    <max>600</max>
                    <checkInterval>25</checkInterval>
                    <throttling>
                        <limit>50</limit>
                        <delay>200</delay>
                    </throttling>
                    <throttling>
                        <limit>50</limit>
                        <delay>199</delay>
                    </throttling>
                </handler>
                """;

        IllegalArgumentException e = assertThrows(
                IllegalArgumentException.class, () -> createThrottlingMailHook(xmlConfig));

        assertEquals("Delay cannot decrease: 199", e.getMessage());
    }

    @Test
    public void testNoThrottling()
    throws Exception
    {
        String xmlConfig = """
                <handler>
                    <max>600</max>
                    <checkInterval>25</checkInterval>
                </handler>
                """;

        ThrottlingMailHook mailHook = createThrottlingMailHook(xmlConfig);
        assertEquals(0, mailHook.getThrottlings().size());
    }

    @Test
    public void testSettings()
    throws Exception
    {
        String xmlConfig = """
                <handler>
                    <max>600</max>
                    <checkInterval>25</checkInterval>
                    <throttling>
                        <limit>1</limit>
                        <delay>10</delay>
                    </throttling>
                    <throttling>
                        <limit>2</limit>
                        <delay>20</delay>
                    </throttling>
                </handler>
                """;

        ThrottlingMailHook mailHook = createThrottlingMailHook(xmlConfig);
        assertEquals(600, mailHook.getMaxQeueuSize());
        assertEquals(25, mailHook.getCheckInterval());
        assertEquals(2, mailHook.getThrottlings().size());
        assertEquals(1, mailHook.getThrottlings().get(0).limit);
        assertEquals(10, mailHook.getThrottlings().get(0).delay);
        assertEquals(2, mailHook.getThrottlings().get(1).limit);
        assertEquals(20, mailHook.getThrottlings().get(1).delay);
    }

    @Test
    public void testMaxReached()
    throws Exception
    {
        String xmlConfig = """
                <handler>
                    <max>1000</max>
                    <checkInterval>2</checkInterval>
                    <throttling>
                        <limit>1</limit>
                        <delay>10</delay>
                    </throttling>
                    <throttling>
                        <limit>2</limit>
                        <delay>20</delay>
                    </throttling>
                </handler>
                """;

        ThrottlingMailHook mailHook = createThrottlingMailHook(xmlConfig);

        MockMailQueue mailQueue = (MockMailQueue) mailHook.getMailQueueFactory().getQueue(
                MailQueueName.of("spool")).orElseThrow();

        mailQueue.setSize(0);

        memoryLogAppender.reset();

        HookResult hookResult = mailHook.doMail(null, MaybeSender.nullSender());

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Checking queue MailQueueName{value=spool}"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Queue MailQueueName{value=spool} size 0"));

        assertEquals(HookResult.DECLINED, hookResult);
        assertEquals(0, mailHook.getCachedDelay().get());
        assertEquals(0, mailHook.getCachedQueueSize().get());

        mailQueue.setSize(1001);

        memoryLogAppender.reset();

        hookResult = mailHook.doMail(null, MaybeSender.nullSender());

        // previous value was cached
        assertEquals(HookResult.DECLINED, hookResult);
        assertEquals(0, mailHook.getCachedDelay().get());
        assertEquals(0, mailHook.getCachedQueueSize().get());

        memoryLogAppender.reset();

        // try again
        hookResult = mailHook.doMail(null, MaybeSender.nullSender());

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Queue MailQueueName{value=spool} size 1001"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Maximum queue size reached. Maximum: 1000, Current: 1001"));

        // delay was set
        assertEquals(20, mailHook.getCachedDelay().get());
        // queue size was set
        assertEquals(1001, mailHook.getCachedQueueSize().get());
        // now maximum should have been reached
        assertEquals(HookResult.DENYSOFT.getResult(), hookResult.getResult());
        assertEquals(HookResult.DENYSOFT.getSmtpRetCode(), hookResult.getSmtpRetCode());
        assertEquals("Queue size 1001 exceeds the maximum size 1000", hookResult.getSmtpDescription());

        memoryLogAppender.reset();

        // try again. max should have been cached
        hookResult = mailHook.doMail(null, MaybeSender.nullSender());

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Queue MailQueueName{value=spool} size 1001"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Maximum queue size reached. Maximum: 1000, Current: 1001"));

        // now maximum should have been reached
        assertEquals(HookResult.DENYSOFT.getResult(), hookResult.getResult());
        assertEquals(HookResult.DENYSOFT.getSmtpRetCode(), hookResult.getSmtpRetCode());
        assertEquals("Queue size 1001 exceeds the maximum size 1000", hookResult.getSmtpDescription());
        // delay was set
        assertEquals(20, mailHook.getCachedDelay().get());

        // set new size to max
        mailQueue.setSize(1000);

        memoryLogAppender.reset();

        // try again
        hookResult = mailHook.doMail(null, MaybeSender.nullSender());

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Queue MailQueueName{value=spool} size 1000"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Sleep for 20 msec"));

        // delay was set
        assertEquals(20, mailHook.getCachedDelay().get());
        assertEquals(1000, mailHook.getCachedQueueSize().get());
        assertEquals(HookResult.DECLINED, hookResult);
   }

    @Test
    public void testMultipleQueues()
    throws Exception
    {
        String xmlConfig = """
                <handler>
                    <max>1000</max>
                    <checkInterval>2</checkInterval>
                    <throttling>
                        <limit>1</limit>
                        <delay>10</delay>
                    </throttling>
                    <throttling>
                        <limit>2</limit>
                        <delay>20</delay>
                    </throttling>
                </handler>
                """;

        ThrottlingMailHook mailHook = createThrottlingMailHook(xmlConfig);

        MockMailQueue spoolQueue = (MockMailQueue) mailHook.getMailQueueFactory().getQueue(
                MailQueueName.of("spool")).orElseThrow();

        MockMailQueue outgoingQueue = (MockMailQueue) mailHook.getMailQueueFactory().getQueue(
                MailQueueName.of("outgoing")).orElseThrow();

        spoolQueue.setSize(10);
        outgoingQueue.setSize(20);

        mailHook.doMail(null, MaybeSender.nullSender());
        assertEquals(20, mailHook.getCachedQueueSize().get());

        // force queue check
        mailHook.getMailCounter().set(1);

        spoolQueue.setSize(30);
        mailHook.doMail(null, MaybeSender.nullSender());
        assertEquals(30, mailHook.getCachedQueueSize().get());
    }
}