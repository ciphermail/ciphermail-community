/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PhoneNumbers;
import com.ciphermail.core.app.properties.SMSProperties;
import com.ciphermail.core.app.properties.SMSPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.SQLException;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class HasPhoneNumberTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @BeforeClass
    public static void setUpBeforeClass() {
        Configurator.setLevel(HasPhoneNumber.class, Level.DEBUG);
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                addUserWithPhoneNumber("test1@example.com", "+31123456788");
                addUserWithPhoneNumber("test2@example.com", "+31123456789");
                addUserWithPhoneNumber("test3@example.com", null);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private SMSProperties createSMSProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMSPropertiesImpl.class)
                .createInstance(properties);
    }

    private void addUserWithPhoneNumber(String email, String phoneNumber)
    throws Exception
    {
        User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

        userWorkflow.makePersistent(user);

        createSMSProperties(user.getUserPreferences().getProperties()).setSMSPhoneNumber(phoneNumber);
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new HasPhoneNumber();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testSingleMatchCaseInsensitive()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test1@EXAMPLE.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();

        mail.setMessage(message);

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test1@EXAMPLE.com")));

        PhoneNumbers phoneNumbers = CoreApplicationMailAttributes.getPhoneNumbers(mail);

        assertNotNull(phoneNumbers);
        assertEquals(1, phoneNumbers.size());
        assertNotNull(phoneNumbers.get("test1@example.com"));
        assertEquals("+31123456788", phoneNumbers.get("test1@example.com").getNumber());
    }

    @Test
    public void testMultipleMatch()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test1@example.com", "test2@example.com", "test3@example.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();

        mail.setMessage(message);

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("test1@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));

        PhoneNumbers phoneNumbers = CoreApplicationMailAttributes.getPhoneNumbers(mail);

        assertNotNull(phoneNumbers);
        assertEquals(2, phoneNumbers.size());
        assertNotNull(phoneNumbers.get("test1@example.com"));
        assertEquals("+31123456788", phoneNumbers.get("test1@example.com").getNumber());
        assertNotNull(phoneNumbers.get("test2@example.com"));
        assertEquals("+31123456789", phoneNumbers.get("test2@example.com").getNumber());
    }

    @Test
    public void testNoMatchMissingPhoneNumber()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test3@example.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();

        mail.setMessage(message);

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "User test3@example.com has no sms phoneNumber"));

        assertEquals(0, result.size());

        PhoneNumbers phoneNumbers = CoreApplicationMailAttributes.getPhoneNumbers(mail);

        assertNotNull(phoneNumbers);
        assertEquals(0, phoneNumbers.size());
    }

    @Test
    public void testNoMatchUnknownUser()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("unknown@example.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();

        mail.setMessage(message);

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "User unknown@example.com has no sms phoneNumber"));

        assertEquals(0, result.size());

        PhoneNumbers phoneNumbers = CoreApplicationMailAttributes.getPhoneNumbers(mail);

        assertNotNull(phoneNumbers);
        assertEquals(0, phoneNumbers.size());
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        HasPhoneNumber matcher = new HasPhoneNumber()
        {
            @Override
            protected boolean hasMatch(@Nonnull User user)
            throws MessagingException
            {
                boolean match = super.hasMatch(user);

                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                return match;
            }
        };

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test1@example.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();

        mail.setMessage(message);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertTrue(constraintViolationThrown.booleanValue());

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test1@EXAMPLE.com")));

        PhoneNumbers phoneNumbers = CoreApplicationMailAttributes.getPhoneNumbers(mail);

        assertNotNull(phoneNumbers);
        assertEquals(1, phoneNumbers.size());
        assertNotNull(phoneNumbers.get("test1@example.com"));
        assertEquals("+31123456788", phoneNumbers.get("test1@example.com").getNumber());
    }
}
