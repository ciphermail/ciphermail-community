/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.properties.PDFProperties;
import com.ciphermail.core.app.properties.PDFPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.SQLException;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class HasValidPasswordTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @BeforeClass
    public static void setUpBeforeClass()
    {
        Configurator.setLevel(HasValidPassword.class, Level.DEBUG);
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
                addUserWithPassword("test1@example.com", "test1");
                addUserWithPassword("test2@example.com", "test2");
                addUserWithPassword("null@example.com", null);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void addUserWithPassword(String email, String password)
    throws Exception
    {
        User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

        PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                .createInstance(user.getUserPreferences().getProperties());

        pdfProperties.setPassword(password);

        userWorkflow.makePersistent(user);
    }

    private Mail createMail(String from, String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject("test");
        message.setFrom(new InternetAddress(from));
        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .mimeMessage(message)
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new HasValidPassword();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testCaseInsensitive()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("from@example.com", "TEst1@EXAMPLE.COM");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertThat(result, hasItem(new MailAddress("TEST1@example.COM")));

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);
        assertNotNull(passwords.get("test1@example.com"));
        assertEquals("test1", passwords.get("test1@example.com").getPassword());
        assertNull(passwords.get("test1@example.com").getPasswordID());
    }

    @Test
    public void testMultipleMatches()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("from@example.com", "test2@example.com", "test1@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertThat(result, allOf(hasItem(new MailAddress("test1@example.com")),
                hasItem(new MailAddress("test2@example.com"))));

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);
        assertNotNull(passwords.get("test1@example.com"));
        assertEquals("test1", passwords.get("test1@example.com").getPassword());

        assertNotNull(passwords.get("test2@example.com"));
        assertEquals("test2", passwords.get("test2@example.com").getPassword());
    }

    @Test
    public void testUnknownUser()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("from@example.com", "unknown@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testNullPassword()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("from@example.com", "null@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    /*
     * Test if existing passwords attached to a mail item are not overwritten but are merged
     */
    @Test
    public void testExistingPasswords()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("from@example.com", "test1@example.com", "test2@example.com");

        Passwords existingPasswords = new Passwords();

        PasswordContainer passwordContainer1 = new PasswordContainer();
        passwordContainer1.setPassword("newpassword1");
        passwordContainer1.setPasswordID("newid1");

        PasswordContainer passwordContainer2 = new PasswordContainer();
        passwordContainer2.setPassword("newpassword2");
        passwordContainer2.setPasswordID("newid2");

        PasswordContainer passwordContainer3 = new PasswordContainer();
        passwordContainer3.setPassword("newpasswordfortest2");
        passwordContainer3.setPasswordID("newidfortest2");

        existingPasswords.put("new1@example.com", passwordContainer1);
        existingPasswords.put("new2@example.com", passwordContainer2);
        existingPasswords.put("test2@example.com", passwordContainer3);

        CoreApplicationMailAttributes.setPasswords(mail, existingPasswords);

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Existing passwords found. Merge with new passwords"));

        assertEquals(2, result.size());

        assertThat(result, allOf(hasItem(new MailAddress("test1@example.com")),
                hasItem(new MailAddress("test2@example.com"))));

        Passwords newPasswords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(newPasswords);
        assertEquals(4, newPasswords.size());

        assertNotNull(newPasswords.get("test1@example.com"));
        assertEquals("test1", newPasswords.get("test1@example.com").getPassword());
        assertNull(newPasswords.get("test1@example.com").getPasswordID());

        assertNotNull(newPasswords.get("test2@example.com"));
        assertEquals("newpasswordfortest2", newPasswords.get("test2@example.com").getPassword());
        assertEquals("newidfortest2", newPasswords.get("test2@example.com").getPasswordID());

        assertNotNull(newPasswords.get("new1@example.com"));
        assertEquals("newpassword1", newPasswords.get("new1@example.com").getPassword());
        assertEquals("newid1", newPasswords.get("new1@example.com").getPasswordID());

        assertNotNull(newPasswords.get("new2@example.com"));
        assertEquals("newpassword2", newPasswords.get("new2@example.com").getPassword());
        assertEquals("newid2", newPasswords.get("new2@example.com").getPasswordID());
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        HasValidPassword matcher = new HasValidPassword()
        {
            @Override
            protected boolean hasMatch(@Nonnull User user)
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    // add some fake data to check whether the passwords are cleared on retry
                    HasValidPasswordActivationContext hasValidPasswordContext = getActivationContext().get(
                            ACTIVATION_CONTEXT_KEY,
                            HasValidPasswordActivationContext.class);

                    hasValidPasswordContext.getPasswords().put("random@example.com", new PasswordContainer());

                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                return super.hasMatch(user);
            }
        };

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        Mail mail = createMail("from@example.com", "test1@example.com", "test2@example.com");

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        Passwords existingPasswords = new Passwords();

        PasswordContainer passwordContainer1 = new PasswordContainer();
        passwordContainer1.setPassword("newpassword1");
        passwordContainer1.setPasswordID("newid1");

        PasswordContainer passwordContainer2 = new PasswordContainer();
        passwordContainer2.setPassword("newpassword2");
        passwordContainer2.setPasswordID("newid2");

        PasswordContainer passwordContainer3 = new PasswordContainer();
        passwordContainer3.setPassword("newpasswordfortest2");
        passwordContainer3.setPasswordID("newidfortest2");

        existingPasswords.put("new1@example.com", passwordContainer1);
        existingPasswords.put("new2@example.com", passwordContainer2);
        existingPasswords.put("test2@example.com", passwordContainer3);

        CoreApplicationMailAttributes.setPasswords(mail, existingPasswords);

        Collection<MailAddress> result = matcher.match(mail);

        assertEquals(2, result.size());

        assertThat(result, allOf(hasItem(new MailAddress("test1@example.com")),
                hasItem(new MailAddress("test2@example.com"))));

        Passwords newPasswords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(newPasswords);
        assertEquals(4, newPasswords.size());

        assertNotNull(newPasswords.get("test1@example.com"));
        assertEquals("test1", newPasswords.get("test1@example.com").getPassword());
        assertNull(newPasswords.get("test1@example.com").getPasswordID());

        assertNotNull(newPasswords.get("test2@example.com"));
        assertEquals("newpasswordfortest2", newPasswords.get("test2@example.com").getPassword());
        assertEquals("newidfortest2", newPasswords.get("test2@example.com").getPasswordID());

        assertNotNull(newPasswords.get("new1@example.com"));
        assertEquals("newpassword1", newPasswords.get("new1@example.com").getPassword());
        assertEquals("newid1", newPasswords.get("new1@example.com").getPasswordID());

        assertNotNull(newPasswords.get("new2@example.com"));
        assertEquals("newpassword2", newPasswords.get("new2@example.com").getPassword());
        assertEquals("newid2", newPasswords.get("new2@example.com").getPasswordID());
    }
}
