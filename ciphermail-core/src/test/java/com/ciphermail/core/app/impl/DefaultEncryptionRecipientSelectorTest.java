/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.ctl.CTL;
import com.ciphermail.core.common.security.ctl.CTLEntry;
import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import com.ciphermail.core.common.security.ctl.CTLException;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.AddressException;
import java.io.File;
import java.security.KeyStore;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class DefaultEncryptionRecipientSelectorTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt certStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    private X509CRLStoreExt crlStore;

    @Autowired
    private PKISecurityServices pKISecurityServices;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private CTLManager ctlManager;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            deleteAllUsers();

            try {
                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();
                crlStore.removeAllEntries();
                ctlManager.getCTL(CTLManager.DEFAULT_CTL).deleteAll();
            }
            catch (CertStoreException | CRLStoreException | CTLException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void deleteAllUsers()
    {
        Set<User> users;

        try {
            users = userWorkflow.getUsers(null, null, SortDirection.ASC);
        }
        catch (AddressException | CloseableIteratorException | HierarchicalPropertiesException e) {
            throw new UnhandledException(e);
        }

        for (User user : users) {
            userWorkflow.deleteUser(user);
        }
    }

    private void addUser(String email, Collection<X509Certificate> certificates)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserPreferences preferences = user.getUserPreferences();

                preferences.getCertificates().addAll(certificates);

                userWorkflow.makePersistent(user);
            }
            catch (HierarchicalPropertiesException | AddressException  e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Set<User> select(DefaultEncryptionRecipientSelector selector, Collection<X509Certificate> certificates,
            String... emails)
    {
        return transactionOperations.execute(status ->
        {
            try {
                Set<User> users = new HashSet<>();

                for (String email : emails)
                {
                    User user = userWorkflow.getUser(email, UserNotExistResult.NULL_IF_NOT_EXIST);

                    if (user == null)
                    {
                        user = userWorkflow.addUser(email);

                        userWorkflow.makePersistent(user);
                    }

                    users.add(user);
                }

                return selector.select(users, certificates);
            }
            catch (HierarchicalPropertiesException | AddressException  e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCertificates(X509CertStoreExt store, Collection<X509Certificate> certificates)  {
        addCertificates(store, certificates.toArray(new X509Certificate[] {}));
    }

    private void addCertificates(X509CertStoreExt store, X509Certificate ...certificates)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                for (X509Certificate certificate : certificates)
                {
                    if (!store.contains(certificate)) {
                        store.addCertificate(certificate);
                    }
                }
            }
            catch (CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addToCTL(String thumbprint, CTLEntryStatus ctlStatus, boolean allowExpired)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL(CTLManager.DEFAULT_CTL);

                CTLEntry ctlEntry = ctl.createEntry(thumbprint);

                ctlEntry.setStatus(ctlStatus);
                ctlEntry.setAllowExpired(allowExpired);

                ctl.addEntry(ctlEntry);
            }
            catch (CTLException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectOneUserAutoSelectWhiteListedExpired()
    throws Exception
    {
        KeyStore keyStore = TestUtils.loadKeyStore(new File(TEST_BASE, "keys/testCertificates.p12"), "test");

        X509Certificate rootCert = (X509Certificate) keyStore.getCertificate("root");

        assertNotNull(rootCert);

        addCertificates(rootStore, rootCert);

        X509Certificate caCert = (X509Certificate) keyStore.getCertificate("ca");

        assertNotNull(caCert);

        addCertificates(certStore, caCert);

        X509Certificate expiredCert = (X509Certificate) keyStore.getCertificate("NotYetValid");

        assertNotNull(expiredCert);

        assertTrue(X509CertificateInspector.isExpired(expiredCert));

        assertEquals("test@example.com,test@example.com", StringUtils.join(
                new X509CertificateInspector(expiredCert).getEmail(), ","));

        addCertificates(certStore, expiredCert);

        Set<X509Certificate> certificates = new HashSet<>();

        DefaultEncryptionRecipientSelector selector = new DefaultEncryptionRecipientSelector(pKISecurityServices,
                true /* check validity */);

        Set<User> users = select(selector, certificates, "test@example.com");

        assertNotNull(users);

        assertEquals(0, users.size());
        assertEquals(0, certificates.size());

        // white-list the certificate
        addToCTL(X509CertificateInspector.getThumbprint(expiredCert), CTLEntryStatus.WHITELISTED,
                true /* allow expired */);

        users = select(selector, certificates, "test@example.com");

        // Although the certificate was white-listed it is not automatically selected when expired
        assertEquals(0, users.size());
        assertEquals(0, certificates.size());

        addUser("test@example.com", Collections.singleton(expiredCert));

        users = select(selector, certificates, "test@example.com");

        // The certificate is now explicitly added to the user so it's now used because it's white-listed
        assertEquals(1, users.size());
        assertEquals(1, certificates.size());
        assertEquals(expiredCert, certificates.iterator().next());
    }

    @Test
    public void testSelectOneUserAutoSelect()
    throws Exception
    {
        addCertificates(certStore, CertificateUtils.readX509Certificates(
                new File(TEST_BASE, "certificates/testCertificates.p7b")));
        addCertificates(certStore, TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/mitm-test-ca.cer")));

        addCertificates(rootStore, TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/mitm-test-root.cer")));

        Set<X509Certificate> certificates = new HashSet<>();

        DefaultEncryptionRecipientSelector selector = new DefaultEncryptionRecipientSelector(pKISecurityServices,
                true /* check validity */);

        Set<User> users = select(selector, certificates, "test@example.com");

        assertEquals(1, users.size());
        assertEquals(13, certificates.size());

        certificates.clear();

        selector = new DefaultEncryptionRecipientSelector(pKISecurityServices,
                false /* do not check validity */);

        users = select(selector, certificates, "test@example.com");

        assertEquals(1, users.size());
        assertEquals(13, certificates.size());
    }

    @Test
    public void testSelectOneUserManualSelect()
    throws Exception
    {
        Collection<X509Certificate> manualCertificates = CertificateUtils.readX509Certificates(
                new File(TEST_BASE, "certificates/testCertificates.p7b"));

        manualCertificates.add(TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/thawte-freemail-valid-till-091108.cer")));

        assertEquals(21, manualCertificates.size());

        addCertificates(certStore, TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/mitm-test-ca.cer")));

        addCertificates(rootStore, TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/mitm-test-root.cer")));

        addCertificates(certStore, manualCertificates);

        addUser("someoneelse@example.com", manualCertificates);

        DefaultEncryptionRecipientSelector selector = new DefaultEncryptionRecipientSelector(pKISecurityServices,
                true /* check validity */);

        Set<X509Certificate> certificates = new HashSet<>();

        Set<User> users = select(selector, certificates, "someoneelse@example.com");

        assertEquals(1, users.size());
        assertEquals(15, certificates.size());
    }

    @Test
    public void testSelectOneUserNoCheck()
    throws Exception
    {
        Collection<X509Certificate> manualCertificates = CertificateUtils.readX509Certificates(
                new File(TEST_BASE, "certificates/testCertificates.p7b"));

        manualCertificates.add(TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/thawte-freemail-valid-till-091108.cer")));

        assertEquals(21, manualCertificates.size());

        addCertificates(certStore, manualCertificates);

        addUser("someoneelse@example.com", manualCertificates);

        DefaultEncryptionRecipientSelector selector = new DefaultEncryptionRecipientSelector(pKISecurityServices,
                false /* no check validity */);

        Set<X509Certificate> certificates = new HashSet<>();

        Set<User> users = select(selector, certificates, "someoneelse@example.com");

        assertEquals(1, users.size());
        assertEquals(21, certificates.size());
    }
}
