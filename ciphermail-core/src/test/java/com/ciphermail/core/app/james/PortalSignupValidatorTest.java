/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.ciphermail.core.app.PortalEmailSigner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PortalSignupValidatorTest
{
    @Test
    void testDefaultSettings()
    throws Exception
    {
        PortalEmailSigner emailSigner = new PortalEmailSigner("test@example.com", 123L,
                PortalEmailSigner.PORTAL_SIGNUP);

        String mac = emailSigner.calculateMAC("key");

        Assertions.assertEquals("za6cmsudulgipjpcw7cs4ax4j7znn5fraj73ifd5vjig2sc2kdda", mac);

        mac = emailSigner.calculateMAC("key2");

        Assertions.assertEquals("nqu7yawsjltvvybsemyya4lcewrusf6jndgtthlogqgf2f7xqdua", mac);
    }

    @Test
    void testTimestamp()
    throws Exception
    {
        PortalEmailSigner emailSigner = new PortalEmailSigner("test@example.com", 1377812402999L,
                PortalEmailSigner.PORTAL_SIGNUP);

        String mac1 = emailSigner.calculateMAC("key");

        emailSigner = new PortalEmailSigner("test@example.com", 2377812402999L,
                PortalEmailSigner.PORTAL_SIGNUP);

        String mac2 = emailSigner.calculateMAC("key");

        Assertions.assertNotEquals(mac1, mac2);
    }

    @Test
    void testEmailNormalization()
    throws Exception
    {
        PortalEmailSigner emailSigner = new PortalEmailSigner("tEst@EXamPLE.coM", 123L,
                PortalEmailSigner.PORTAL_SIGNUP);

        String mac = emailSigner.calculateMAC("key");

        Assertions.assertEquals("za6cmsudulgipjpcw7cs4ax4j7znn5fraj73ifd5vjig2sc2kdda", mac);
    }

    @Test
    void testDifferentID()
    throws Exception
    {
        PortalEmailSigner emailSigner = new PortalEmailSigner("test@example.com", 123L,
                new byte[]{1,2,3});

        String mac = emailSigner.calculateMAC("key");

        Assertions.assertEquals("dcydvy7im63f44odmy2nbnsa5icbae3juu7u5nqgqrh6vutc5iuq", mac);
    }
}
