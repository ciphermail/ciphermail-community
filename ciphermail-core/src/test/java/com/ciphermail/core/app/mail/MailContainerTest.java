/*
 * Copyright (c) 2010-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.mail;

import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.junit.Test;

import javax.mail.internet.MimeMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MailContainerTest
{
    @Test
    public void testSerialize()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/html-alternative.eml");

        Mail mail = MailImpl.builder().name("test")
                .sender("sender@example.com")
                .addRecipients("test1@example.com", "test2@example.com")
                .mimeMessage(message).build();

        mail.setAttribute(Attribute.convertToAttribute("test1", "1234"));
        mail.setAttribute(Attribute.convertToAttribute("test2", "5678"));

        MailContainer container = new MailContainer(mail);

        assertNotNull(container.getMail().getMessage());

        byte[] serialized = SerializationUtils.serialize(container);

        MailContainer deserialized = (MailContainer) SerializationUtils.deserialize(serialized);

        assertEquals(mail.getRecipients(), deserialized.getMail().getRecipients());
        assertEquals(mail.getMaybeSender(), deserialized.getMail().getMaybeSender());
        assertEquals(mail.getState(), deserialized.getMail().getState());
        assertEquals(mail.getErrorMessage(), deserialized.getMail().getErrorMessage());
        assertEquals(mail.getName(), deserialized.getMail().getName());
        assertEquals(mail.getRemoteHost(), deserialized.getMail().getRemoteHost());
        assertEquals(mail.getRemoteAddr(), deserialized.getMail().getRemoteAddr());
        assertEquals("1234", MailAttributesUtils.getManagedAttributeValue(deserialized.getMail(),
                "test1", String.class).get());
        assertEquals("5678", MailAttributesUtils.getManagedAttributeValue(deserialized.getMail(),
                "test2", String.class).get());
        // the message should not be serialized
        assertNull(deserialized.getMail().getMessage());
    }
}
