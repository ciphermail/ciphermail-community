/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.UserPreferenceMerger;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesCategory;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UserPreferenceMergerTest
{
    @Test
    public void testAddNew()
    {
        Set<UserPreferences> targetPreferences = new LinkedHashSet<>();

        targetPreferences.add(new MockupUserPreferences("prefs1", UserPreferencesCategory.USER.name()));
        targetPreferences.add(new MockupUserPreferences("prefs2", UserPreferencesCategory.UNKNOWN.name()));
        targetPreferences.add(new MockupUserPreferences("prefs3", "otherCategory"));

        Set<UserPreferences> newPreferences = new LinkedHashSet<>();

        newPreferences.add(new MockupUserPreferences("prefs4", UserPreferencesCategory.GLOBAL.name()));
        newPreferences.add(new MockupUserPreferences("prefs6", UserPreferencesCategory.DOMAIN.name()));
        newPreferences.add(new MockupUserPreferences("prefs7", UserPreferencesCategory.CUSTOM.name()));

        UserPreferenceMerger merger = new DefaultUserPreferenceMerger();

        Set<UserPreferences> mergedPreferences = merger.merge(targetPreferences, newPreferences);

        assertEquals(6, mergedPreferences.size());

        List<UserPreferences> list = new LinkedList<>(mergedPreferences);

        assertEquals("prefs4", list.get(0).getName());;
        assertEquals("prefs6", list.get(1).getName());;
        assertEquals("prefs7", list.get(2).getName());;
        assertEquals("prefs2", list.get(3).getName());;
        assertEquals("prefs3", list.get(4).getName());;
        assertEquals("prefs1", list.get(5).getName());;
    }

    @Test
    public void testAddExisting()
    {
        Set<UserPreferences> targetPreferences = new LinkedHashSet<>();

        targetPreferences.add(new MockupUserPreferences("prefs1", UserPreferencesCategory.USER.name()));
        targetPreferences.add(new MockupUserPreferences("prefs2", UserPreferencesCategory.UNKNOWN.name()));
        targetPreferences.add(new MockupUserPreferences("prefs3", "otherCategory"));
        targetPreferences.add(new MockupUserPreferences("prefs4", UserPreferencesCategory.USER.name()));

        Set<UserPreferences> newPreferences = new LinkedHashSet<>();

        newPreferences.add(new MockupUserPreferences("prefs5", UserPreferencesCategory.GLOBAL.name()));
        newPreferences.add(new MockupUserPreferences("prefs7", UserPreferencesCategory.DOMAIN.name()));
        newPreferences.add(new MockupUserPreferences("prefs8", UserPreferencesCategory.CUSTOM.name()));

        UserPreferenceMerger merger = new DefaultUserPreferenceMerger();

        Set<UserPreferences> mergedPreferences = merger.merge(targetPreferences, newPreferences);

        assertEquals(7, mergedPreferences.size());

        List<UserPreferences> list = new LinkedList<>(mergedPreferences);

        assertEquals("prefs5", list.get(0).getName());;
        assertEquals("prefs7", list.get(1).getName());;
        assertEquals("prefs8", list.get(2).getName());;
        assertEquals("prefs2", list.get(3).getName());;
        assertEquals("prefs3", list.get(4).getName());;
        assertEquals("prefs1", list.get(5).getName());;
    }

    @Test
    public void testNoMerge()
    {
        Set<UserPreferences> targetPreferences = new LinkedHashSet<>();

        targetPreferences.add(new MockupUserPreferences("prefs1", UserPreferencesCategory.USER.name()));
        targetPreferences.add(new MockupUserPreferences("prefs2", UserPreferencesCategory.UNKNOWN.name()));

        Set<UserPreferences> newPreferences = new LinkedHashSet<>();

        newPreferences.add(new MockupUserPreferences("prefs1", UserPreferencesCategory.USER.name()));
        newPreferences.add(new MockupUserPreferences("prefs2", UserPreferencesCategory.UNKNOWN.name()));

        UserPreferenceMerger merger = new DefaultUserPreferenceMerger();

        Set<UserPreferences> mergedPreferences = merger.merge(targetPreferences, newPreferences);

        assertNull(mergedPreferences);
    }
}
