/*
 * Copyright (c) 2010-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.ca.CA;
import com.ciphermail.core.common.security.ca.CACertificateSignatureAlgorithm;
import com.ciphermail.core.common.security.ca.CAImpl;
import com.ciphermail.core.common.security.ca.CAProperties;
import com.ciphermail.core.common.security.ca.CAPropertiesProvider;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.CertificateRequestHandler;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistry;
import com.ciphermail.core.common.security.ca.CertificateRequestStore;
import com.ciphermail.core.common.security.ca.SMIMEKeyAndCertificateIssuer;
import com.ciphermail.core.common.security.ca.handlers.BuiltInCertificateRequestHandler;
import com.ciphermail.core.common.security.ca.hibernate.CertificateRequestEntity;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang.time.DateUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class, RequestSenderCertificateTest.LocalServices.class})
public class RequestSenderCertificateTest
{
    private static final int INITIAL_KEY_STORE_SIZE = 2;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private CertificateRequestHandlerRegistry certificateRequestHandlerRegistry;

    @Autowired
    private CertificateRequestStore certificateRequestStore;

    @Autowired
    private SMIMEKeyAndCertificateIssuer sMIMEKeyAndCertificateIssuer;

    @Autowired
    private CA ca;

    @Autowired
    private CAPropertiesProvider caPropertiesProvider;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private AbstractApplicationContext applicationContext;

    public static class LocalServices
    {
        // For this test we must override the CA bean to make it a prototype bean so we can better handle the background
        // thread
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        @Bean(CipherMailSystemServices.CA_SERVICE_NAME)
        public CA caService(
                CertificateRequestStore certificateRequestStore,
                CertificateRequestHandlerRegistry certificateRequestHandlerRegistry,
                @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME) KeyAndCertStore keyAndCertStore,
                TransactionOperations transactionOperations,
                SessionManager sessionManager)
        {
            CAImpl ca = new CAImpl(certificateRequestStore, certificateRequestHandlerRegistry, keyAndCertStore,
                    transactionOperations, sessionManager, 5 /* in seconds */, 2592000, new int[] {5});

            // For the tests, the background should run immediately when started
            ca.setThreadStartupDelay(0);

            return ca;
        }
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                // Clean key/root store
                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();

                // Remove pending requests
                List<? extends CertificateRequest> all = certificateRequestStore.getAllRequests(null, null);

                for (CertificateRequest request : all) {
                    certificateRequestStore.deleteRequest(request.getID());
                }

                // Load CA
                KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
                keyStore.load(new FileInputStream(new File(TestUtils.getTestDataDir(),
                        "keys/testCA.p12")), "test".toCharArray());

                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                        MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);

                // Import root
                rootStore.addCertificate(TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-root.cer")));

                 // Configure CA properties
                CAProperties properties = caPropertiesProvider.getProperties();

                properties.setDefaultCommonName("test CN");
                properties.setCertificateValidityDays(365);
                properties.setKeyLength(2048);
                properties.setSignatureAlgorithm(CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION);
                properties.setCRLDistributionPoint(null);
                properties.setDefaultCertificateRequestHandler(null);

                // use test CA for the signer
                properties.setIssuerThumbprint("D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844F3C" +
                                             "9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        CertificateRequestHandler builtInhandler = new BuiltInCertificateRequestHandler(
                keyAndCertStore,
                caPropertiesProvider,
                sMIMEKeyAndCertificateIssuer);

        certificateRequestHandlerRegistry.registerHandler(builtInhandler);

        ((CAImpl) ca).start();
    }

    @After
    public void setUpAfter()
    throws Exception
    {
        // Wait for background thread to stop
        ((CAImpl) ca).stop(DateUtils.MILLIS_PER_SECOND * 30);
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void addCertificateRequest(String email)
    {
        transactionOperations.executeWithoutResult(stats ->
        {
            CertificateRequestEntity request = new CertificateRequestEntity("non existing handler");

            request.setSubject(null);
            request.setEmail(email);
            request.setNextUpdate(DateUtils.addDays(new Date(), 1));

            certificateRequestStore.addRequest(request);
        });
    }

    private long getCertificateRequestStoreSize() {
        return transactionOperations.execute(status -> certificateRequestStore.getSize()).longValue();
    }

    private long getKeyAndCertStoreSize() {
        return transactionOperations.execute(status -> keyAndCertStore.size()).longValue();
    }

    private long getUserCount()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return userWorkflow.getUserCount();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }).longValue();
    }

    private boolean isUser(String email)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(status ->
        {
            try {
                return userWorkflow.getUser(email, UserNotExistResult.NULL_IF_NOT_EXIST) != null;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }));
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new RequestSenderCertificate();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testOverrideCertificateRequestHandler()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                // specify an unknown request handler so we can check the queue
                .setProperty("certificateRequestHandler", "unknown")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Requesting a certificate for user from@example.com using handler unknown"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "CertificateRequestHandler with name unknown not available"));

        assertEquals(0, getCertificateRequestStoreSize());
        assertEquals(INITIAL_KEY_STORE_SIZE, getKeyAndCertStoreSize());
    }

    /*
     * Do not request a new certificate because a valid certificate is available
     */
    @Test
    public void testSigningCertAvailable()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        keyStore.load(new FileInputStream(new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12")), "test".toCharArray());

        transactionOperations.executeWithoutResult(stats ->
        {
            try {
                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore, MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        assertEquals(22, getKeyAndCertStoreSize());

        assertEquals(0, getUserCount());

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, getUserCount());
        assertEquals(22, getKeyAndCertStoreSize());
    }

    /*
     * Tests whether a new certificate is requested if there is already a pending request but skipIfAvailable is false
     */
    @Test
    public void testPendingSkipIfAvailableFalse()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("skipIfAvailable", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        String from = "from@example.com";

        addCertificateRequest(from);

        sourceMessage.setFrom(new InternetAddress(from));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals(0, getUserCount());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Requesting a certificate for user from@example.com using handler Local"));

        assertEquals(INITIAL_KEY_STORE_SIZE + 1L, getKeyAndCertStoreSize());
        assertEquals(1, getUserCount());
        assertTrue(isUser(from));
    }

    /*
     * Tests whether a new certificate is not requested when there is already a pending request for the sender
     */
    @Test
    public void testPendingAvailable()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        String from = "from@example.com";

        addCertificateRequest(from);

        sourceMessage.setFrom(new InternetAddress(from));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals(0, getUserCount());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(INITIAL_KEY_STORE_SIZE, getKeyAndCertStoreSize());
        assertEquals(0, getUserCount());
    }

    @Test
    public void testNotAddUser()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("addUser", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals(0, getUserCount());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Requesting a certificate for user from@example.com using handler Local"));

        assertEquals(INITIAL_KEY_STORE_SIZE + 1, getKeyAndCertStoreSize());
        assertEquals(0, getUserCount());
    }

    @Test
    public void testRequestCertificateDefaultSettings()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        String from = "from@example.com";

        sourceMessage.setFrom(new InternetAddress(from));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals(0, getUserCount());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Requesting a certificate for user from@example.com using handler Local"));

        assertEquals(INITIAL_KEY_STORE_SIZE + 1, getKeyAndCertStoreSize());
        assertEquals(1, getUserCount());
        assertTrue(isUser(from));
    }

    @Test
    public void testInvalidOriginator()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        String from = "...";

        sourceMessage.setFrom(new InternetAddress(from));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals(0, getUserCount());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(INITIAL_KEY_STORE_SIZE, getKeyAndCertStoreSize());
        assertEquals(0, getUserCount());
    }

    /*
     * Tests whether multiple threads requesting a certificate for the same email address does not result in
     * multiple certificates for that email address
     */
    @Test
    public void testRequestCertificateMultiThreaded()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage message1 = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        message1.setFrom(new InternetAddress("from1@example.com"));

        MimeMessage message2 = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        message2.setFrom(new InternetAddress("from2@example.com"));

        int executionCount = 20;

        AtomicInteger executionCounter = new AtomicInteger();
        CountDownLatch countDownLatch = new CountDownLatch(executionCount);

        Callable<Null> callable = new Callable<Null>()
        {
            @Override
            public Null call()
            throws Exception
            {
                try {
                    FakeMail mail1 = FakeMail.builder().name(MailImpl.getId())
                            .recipients("recipient@example.com")
                            .sender("sender1@example.com")
                            .mimeMessage(message1)
                            .build();

                    mailet.service(mail1);

                    FakeMail mail2 = FakeMail.builder().name(MailImpl.getId())
                            .recipients("recipient@example.com")
                            .sender("sender2@example.com")
                            .mimeMessage(message2)
                            .build();

                    mailet.service(mail2);

                    System.out.println("Count " + executionCounter.incrementAndGet() + ". Thread: " +
                                       Thread.currentThread().getName());

                    return null;
                }
                finally {
                    countDownLatch.countDown();
                }
            }
        };

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (int i = 0; i < executionCount; i++) {
                executorService.submit(callable);
            }

            assertTrue("Timeout", countDownLatch.await(30, TimeUnit.SECONDS));
            assertEquals(executionCount, executionCounter.get());
        }
        finally {
            executorService.shutdown();
        }

        assertEquals(INITIAL_KEY_STORE_SIZE + 2L, getKeyAndCertStoreSize());
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        String from = "from@example.com";

        sourceMessage.setFrom(new InternetAddress(from));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        RequestSenderCertificate mailet = new RequestSenderCertificate()
        {
            @Override
            protected void onHandleUserEvent(User user)
            throws MessagingException
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
                super.onHandleUserEvent(user);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        assertEquals(0, getUserCount());

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Requesting a certificate for user from@example.com using handler Local"));

        assertTrue(constraintViolationThrown.booleanValue());

        assertEquals(INITIAL_KEY_STORE_SIZE + 1, getKeyAndCertStoreSize());
        assertEquals(1, getUserCount());
        assertTrue(isUser(from));
    }
}
