/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesSelector;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class DefaultUserPreferencesSelectorTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private UserPreferencesSelector userPreferencesSelector;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                domainManager.deleteAllDomains();
            }
            catch (CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences userPreferences = domainManager.addDomain("exAMPle.com");

                assertNotNull(userPreferences);

                Set<UserPreferences> selected = userPreferencesSelector.select(
                        new MockupUser("test@example.com", null, null));

                assertEquals(1, selected.size());

                assertTrue(selected.contains(userPreferences));
            }
            catch (CloseableIteratorException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectWildcardDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences userPreferences = domainManager.addDomain("*.exAMPle.com");

                assertNotNull(userPreferences);

                Set<UserPreferences> selected = userPreferencesSelector.select(
                        new MockupUser("test@example.com", null, null));

                assertEquals(1, selected.size());

                assertTrue(selected.contains(userPreferences));
            }
            catch (CloseableIteratorException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectSubWildcardDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences userPreferences = domainManager.addDomain("*.exAMPle.com");

                assertNotNull(userPreferences);

                Set<UserPreferences> selected = userPreferencesSelector.select(
                        new MockupUser("test@sub.example.com", null, null));

                assertEquals(1, selected.size());

                assertTrue(selected.contains(userPreferences));
            }
            catch (CloseableIteratorException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectMultipleDomains()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences userPreferences = domainManager.addDomain("exAMPle.com");

                assertNotNull(userPreferences);

                UserPreferences wildcardPreferences = domainManager.addDomain("*.exAMPle.com");

                assertNotNull(wildcardPreferences);

                Set<UserPreferences> selected = userPreferencesSelector.select(
                        new MockupUser("test@example.com", null, null));

                assertEquals(2, selected.size());

                assertTrue(selected.contains(userPreferences));
                assertTrue(selected.contains(wildcardPreferences));

                UserPreferences[] selectedArray = new UserPreferences[2];

                selected.toArray(selectedArray);

                assertEquals(wildcardPreferences, selectedArray[0]);
                assertEquals(userPreferences, selectedArray[1]);
            }
            catch (CloseableIteratorException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectMultipleWildcardDomains()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences subWildcard = domainManager.addDomain("*.sub.exAMPle.com");

                assertNotNull(subWildcard);

                UserPreferences domain = domainManager.addDomain("exAMPle.com");

                assertNotNull(domain);

                UserPreferences wildcard = domainManager.addDomain("*.exAMPle.com");

                assertNotNull(wildcard);

                UserPreferences subDomain = domainManager.addDomain("sub.example.COM");

                assertNotNull(subDomain);

                Set<UserPreferences> selected = userPreferencesSelector.select(
                        new MockupUser("test@sub.example.com", null, null));

                assertEquals(2, selected.size());

                UserPreferences[] selectedArray = new UserPreferences[2];

                selected.toArray(selectedArray);

                assertEquals(subWildcard, selectedArray[0]);
                assertEquals(subDomain, selectedArray[1]);
            }
            catch (CloseableIteratorException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }
}
