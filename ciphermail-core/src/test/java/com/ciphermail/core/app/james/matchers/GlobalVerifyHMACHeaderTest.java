/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.UserPreferencesCategoryManager;
import com.ciphermail.core.app.impl.DefaultGlobalPreferencesManager;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.MimeMessage;
import java.sql.SQLException;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class GlobalVerifyHMACHeaderTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Autowired
    private UserPreferencesCategoryManager userPreferencesCategoryManager;

    @Before
    public void setup() {
        deleteGlobalProperties();
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void deleteGlobalProperties()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                userPreferencesCategoryManager.getUserPreferencesManager(UserPreferencesCategory.GLOBAL.name()).
                        deleteUserPreferences(new DefaultGlobalPreferencesManager(userPreferencesCategoryManager,
                                null).getGlobalUserPreferences());
            }
            catch (HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setGlobalProperty(String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences preferences = globalPreferencesManager.getGlobalUserPreferences();

                UserProperties properties = preferences.getProperties();

                properties.setProperty(propertyName, value);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new GlobalVerifyHMACHeader();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testCorrectSecret()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false, X-HMAC, test, secret.prop, true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/hmac-header.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        setGlobalProperty("secret.prop", "123456");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("recipient@example.com")));
    }

    @Test
    public void testIncorrectSecret()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false, X-HMAC, test, secret.prop, true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/hmac-header.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        setGlobalProperty("secret.prop", "not-correct");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "HMAC mismatch"));

        assertEquals(0, result.size());
    }

    @Test
    public void testMissingProperty()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false, X-HMAC, test, secret.prop, true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/hmac-header.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Secret is not set"));

        assertEquals(0, result.size());
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false, X-HMAC, test, secret.prop, true")
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        Matcher matcher = new GlobalVerifyHMACHeader()
        {
            @Override
            protected byte[] getSecretTransacted(Mail mail)
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                return super.getSecretTransacted(mail);
            }
        };

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/hmac-header.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        setGlobalProperty("secret.prop", "123456");

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertTrue(constraintViolationThrown.booleanValue());

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("recipient@example.com")));
    }
}
