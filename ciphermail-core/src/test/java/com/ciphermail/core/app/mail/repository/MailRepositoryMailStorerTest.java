/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.mail.repository;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.repository.MailRepository;
import com.ciphermail.core.common.mail.repository.MailRepositoryItem;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Mail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class MailRepositoryMailStorerTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(),"mail/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MailRepository mailRepository;

    @Autowired
    private MailRepositoryMailStorer mailStorer;

    private void storeMail(Mail mail)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                mailStorer.store(mail);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private List<? extends MailRepositoryItem> getMailStoreItems()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return mailRepository.getItems(0, Integer.MAX_VALUE);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private String getRecipients(UUID id)
    {
        return transactionOperations.execute(status ->
        {
            try {
                MailRepositoryItem item = mailRepository.getItem(id);

                return StringUtils.join(item.getRecipients(), ",");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void assertMessage(UUID id, MimeMessage expected)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MailRepositoryItem item = mailRepository.getItem(id);

                assertTrue(TestUtils.isEqual(expected, item.getMimeMessage()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testStoreMail()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "html-alternative.eml"));

        Mail mail = MailImpl.builder().name("test")
                .sender("sender@example.com")
                .addRecipients("test1@example.com", "test2@example.com")
                .mimeMessage(message).build();

        assertEquals(0, getMailStoreItems().size());

        assertNull(CoreApplicationMailAttributes.getMailRepositoryID(mail));

        storeMail(mail);

        List<? extends MailRepositoryItem> items = getMailStoreItems();

        assertEquals(1, items.size());

        MailRepositoryItem item = items.get(0);

        assertMessage(item.getID(), message);

        // check if the id was added to the Mail attributes
        assertNotNull(CoreApplicationMailAttributes.getMailRepositoryID(mail));
        assertEquals(item.getID(), CoreApplicationMailAttributes.getMailRepositoryID(mail));

        assertEquals("\"Open Source Group Members\" <group-digests@linkedin.com>", item.getFromHeader());
        assertEquals("test1@example.com,test2@example.com", getRecipients(item.getID()));
        assertEquals("sender@example.com", item.getSender().getAddress());
        assertEquals("group-digests@linkedin.com", item.getOriginator().getAddress());
        assertEquals("127.0.0.1", item.getRemoteAddress());
    }
}
