/*
 * Copyright (c) 2015-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class MailAttributesSenderSubjectTriggerTest
{
    @Autowired
    private AbstractApplicationContext applicationContext;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mail createMail(String subject, String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject(subject);
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new MailAttributesSenderSubjectTrigger();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testNoParametersSet()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false, {}")
                .build();

        Mail mail = createMail("test no match", "recipient@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
        assertEquals("test no match", mail.getMessage().getSubject());
    }

    @Test
    public void testDefaultSubjectTriggerDefaultOther()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'subjectTriggerDefault':'encrypt'}")
                .build();

        Mail mail = createMail("test enCrypt", "recipient1@example.com", "recipient2@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("recipient1@example.com")));
        assertTrue(result.contains(new MailAddress("recipient2@example.com")));
        assertEquals("test enCrypt", mail.getMessage().getSubject());
    }

    @Test
    public void testSubjectTriggerAttributeDefaultRegExpRemoveDefault()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'subjectTriggerAttribute':'subject-trigger', 'regExprDefault':'true', "
                           + "'removePatternDefault':'true'}")
                .build();

        Mail mail = createMail("test [ enCrypt ]", "recipient1@example.com", "recipient2@example.com");

        mail.setAttribute(Attribute.convertToAttribute("subject-trigger", "(?i)\\[\\s*encrypt\\s*\\]"));

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("recipient1@example.com")));
        assertTrue(result.contains(new MailAddress("recipient2@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
    }

    @Test
    public void testAttributes()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'subjectTriggerAttribute':'subject-trigger', " +
                           "'regExprAttribute':'reg-expr', 'removePatternAttribute':'remove-pattern'}")
                .build();

        Mail mail = createMail("test [ enCrypt ]", "recipient1@example.com", "recipient2@example.com");

        mail.setAttribute(Attribute.convertToAttribute("subject-trigger", "(?i)\\[\\s*encrypt\\s*\\]"));
        mail.setAttribute(Attribute.convertToAttribute("reg-expr", "true"));
        mail.setAttribute(Attribute.convertToAttribute("remove-pattern", "true"));

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("recipient1@example.com")));
        assertTrue(result.contains(new MailAddress("recipient2@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
    }

    @Test
    public void testAttributesOverrideDefaults()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'subjectTriggerDefault':'abc', " +
                           "'subjectTriggerAttribute':'subject-trigger', " +
                           "'regExprAttribute':'reg-expr', " +
                           "'removePatternAttribute':'remove-pattern', " +
                           "'regExprDefault':'true', " +
                           "'removePatternDefault':'true'}")
                .build();

        Mail mail = createMail("test enCrypt", "recipient1@example.com", "recipient2@example.com");

        mail.setAttribute(Attribute.convertToAttribute("subject-trigger", "encrypt"));
        mail.setAttribute(Attribute.convertToAttribute("reg-expr", "false"));
        mail.setAttribute(Attribute.convertToAttribute("remove-pattern", "false"));

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("recipient1@example.com")));
        assertTrue(result.contains(new MailAddress("recipient2@example.com")));
        assertEquals("test enCrypt", mail.getMessage().getSubject());
    }

    @Test
    public void testIllegalRegExp()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'subjectTriggerAttribute':'subject-trigger', 'regExprDefault':'true'}")
                .build();

        Mail mail = createMail("test [ enCrypt ]", "recipient1@example.com", "recipient2@example.com");

        mail.setAttribute(Attribute.convertToAttribute("subject-trigger", "["));

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "The trigger '[' is not a valid pattern. Matcher will not match."));

        assertEquals(0, result.size());
     }
}
