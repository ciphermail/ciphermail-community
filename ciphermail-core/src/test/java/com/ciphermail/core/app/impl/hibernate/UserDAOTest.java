/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserDAOTest
{
    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup()
    {
        UserDAO dao = createDAO();

        // Delete all users
        for (UserEntity entity : dao.findAll(UserEntity.class)) {
            dao.delete(entity);
        }
    }

    private UserDAO createDAO() {
        return UserDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    private UserEntity addUser(String email) {
        return createDAO().addUser(email, true);
    }

    @Test
    public void testGetUser()
    {
        UserDAO dao = createDAO();

        assertNull(dao.getUser("test@example.com"));

        addUser("testX@example.com");
        addUser("test@example.com");

        UserEntity user = dao.getUser("test@example.com");

        assertNotNull(user);
        assertEquals("test@example.com", user.getEmail());

        user = dao.getUser("TEST@example.com");

        // Email address should be canonicalized
        assertEquals("test@example.com", user.getEmail());
    }

    @Test
    public void testDeleteUser()
    {
        UserDAO dao = createDAO();

        assertNull(dao.getUser("test@example.com"));

        addUser("test@example.com");

        assertEquals(1, dao.getUserCount());

        UserEntity user = dao.getUser("test@example.com");

        assertNotNull(user);

        dao.deleteUser(user);
        assertEquals(0, dao.getUserCount());

        user = dao.getUser("test@example.com");
        assertNull(user);
    }

    @Test
    public void testEquals()
    {
        UserDAO dao = createDAO();

        UserEntity user1 = addUser("test1@example.com");
        UserEntity user2 = addUser("test2@example.com");

        assertNotNull(user1);
        assertNotNull(user2);

        assertEquals(user1, dao.getUser("test1@example.com"));
        assertNotEquals(user1, user2);
        assertEquals(user1.hashCode(), dao.getUser("test1@example.com").hashCode());
        assertEquals("test1@example.com", user1.toString());
    }

    @Test
    public void testGetEmailIterator()
    throws CloseableIteratorException
    {
        UserDAO dao = createDAO();

        addUser("test1@example.com");
        addUser("test2@example.com");
        addUser("test3@example.com");

        List<String> emails;

        emails = CloseableIteratorUtils.toList(dao.getEmailIterator(null, null, SortDirection.ASC));

        assertEquals(3, emails.size());
        assertEquals("test1@example.com", emails.get(0));
        assertEquals("test2@example.com", emails.get(1));
        assertEquals("test3@example.com", emails.get(2));

        emails = CloseableIteratorUtils.toList(dao.getEmailIterator(null, null, null));

        assertEquals(3, emails.size());
        assertEquals("test1@example.com", emails.get(0));
        assertEquals("test2@example.com", emails.get(1));
        assertEquals("test3@example.com", emails.get(2));

        emails = CloseableIteratorUtils.toList(dao.getEmailIterator(null, null, SortDirection.DESC));

        assertEquals(3, emails.size());
        assertEquals("test1@example.com", emails.get(2));
        assertEquals("test2@example.com", emails.get(1));
        assertEquals("test3@example.com", emails.get(0));

        emails = CloseableIteratorUtils.toList(dao.getEmailIterator(0, 0, SortDirection.ASC));

        assertEquals(0, emails.size());

        emails = CloseableIteratorUtils.toList(dao.getEmailIterator(0, 1, SortDirection.ASC));

        assertEquals(1, emails.size());
        assertEquals("test1@example.com", emails.get(0));

        emails = CloseableIteratorUtils.toList(dao.getEmailIterator(0, 1, SortDirection.DESC));

        assertEquals(1, emails.size());
        assertEquals("test3@example.com", emails.get(0));

        emails = CloseableIteratorUtils.toList(dao.getEmailIterator(1, 2, SortDirection.ASC));

        assertEquals(2, emails.size());
        assertEquals("test2@example.com", emails.get(0));
        assertEquals("test3@example.com", emails.get(1));

        emails = CloseableIteratorUtils.toList(dao.getEmailIterator(0, null, SortDirection.ASC));
        assertEquals(3, emails.size());

        emails = CloseableIteratorUtils.toList(dao.getEmailIterator(null, 1, SortDirection.ASC));
        assertEquals(1, emails.size());
    }

    @Test
    public void testSearchEmail()
    throws CloseableIteratorException
    {
        UserDAO dao = createDAO();

        addUser("test1@example.com");
        addUser("test2@example.com");
        addUser("test3@example.com");

        List<String> emails = CloseableIteratorUtils.toList(dao.searchEmail("test2@example.com",
                null, null, SortDirection.ASC));

        assertEquals(1, emails.size());
        assertEquals("test2@example.com", emails.get(0));

        emails = CloseableIteratorUtils.toList(dao.searchEmail("test2@example.com",
                null, null, SortDirection.DESC));

        assertEquals(1, emails.size());
        assertEquals("test2@example.com", emails.get(0));

        emails = CloseableIteratorUtils.toList(dao.searchEmail("%@example.com",
                null, null, SortDirection.ASC));

        assertEquals(3, emails.size());
        assertEquals("test1@example.com", emails.get(0));
        assertEquals("test2@example.com", emails.get(1));
        assertEquals("test3@example.com", emails.get(2));

        emails = CloseableIteratorUtils.toList(dao.searchEmail("%@example.com",
                null, null, null));

        assertEquals(3, emails.size());
        assertEquals("test1@example.com", emails.get(0));
        assertEquals("test2@example.com", emails.get(1));
        assertEquals("test3@example.com", emails.get(2));

        emails = CloseableIteratorUtils.toList(dao.searchEmail("%@example.com",
                null, null, SortDirection.DESC));

        assertEquals(3, emails.size());
        assertEquals("test1@example.com", emails.get(2));
        assertEquals("test2@example.com", emails.get(1));
        assertEquals("test3@example.com", emails.get(0));

        emails = CloseableIteratorUtils.toList(dao.searchEmail("%@example.com",
                1, Integer.MAX_VALUE, SortDirection.ASC));

        assertEquals(2, emails.size());
        assertEquals("test2@example.com", emails.get(0));
        assertEquals("test3@example.com", emails.get(1));

        emails = CloseableIteratorUtils.toList(dao.searchEmail("%@example.com",
                1, 2, SortDirection.DESC));

        assertEquals(2, emails.size());
        assertEquals("test2@example.com", emails.get(0));
        assertEquals("test1@example.com", emails.get(1));

        emails = CloseableIteratorUtils.toList(dao.searchEmail("no-match",
                null, null, SortDirection.ASC));

        assertEquals(0, emails.size());

        emails = CloseableIteratorUtils.toList(dao.searchEmail("%@example.com",
                1, null, SortDirection.ASC));
        assertEquals(2, emails.size());

        emails = CloseableIteratorUtils.toList(dao.searchEmail("%@example.com",
                null, 2, SortDirection.ASC));
        assertEquals(2, emails.size());
    }

    @Test
    public void testSearchCount()
    {
        UserDAO dao = createDAO();

        addUser("test1@example.com");
        addUser("test2@example.com");
        addUser("test3@example.com");

        assertEquals(3, dao.getSearchEmailCount("%"));
        assertEquals(0, dao.getSearchEmailCount("no-match"));
        assertEquals(1, dao.getSearchEmailCount("test3@example.com"));
    }

    @Test
    public void testGetUserCount()
    {
        UserDAO dao = createDAO();

        assertEquals(0, dao.getUserCount());

        addUser("test1@example.com");

        assertEquals(1, dao.getUserCount());

        addUser("test2@example.com");

        assertEquals(2, dao.getUserCount());
    }
}
