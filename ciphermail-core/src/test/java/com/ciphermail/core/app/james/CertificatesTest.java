/*
 * Copyright (c) 2010-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class CertificatesTest
{
    private static final String JSON_ENCODED_CERTS =
        "[\"MIIDVjCCAr+gAwIBAgIOUtwAAQACsrt0dJQB9HUwDQYJKoZIhvcNAQEEBQAwgbwxCzAJBgNVBAYTAkRFMRAwDgYDVQQIEwdIYW1idXJn" +
        "MRAwDgYDVQQHEwdIYW1idXJnMTowOAYDVQQKEzFUQyBUcnVzdENlbnRlciBmb3IgU2VjdXJpdHkgaW4gRGF0YSBOZXR3b3JrcyBHbWJIMSI" +
        "wIAYDVQQLExlUQyBUcnVzdENlbnRlciBDbGFzcyAxIENBMSkwJwYJKoZIhvcNAQkBFhpjZXJ0aWZpY2F0ZUB0cnVzdGNlbnRlci5kZTAeFw" +
        "0wNTA5MjQyMDE5NDNaFw0wNjA5MjQyMDE5NDNaME0xCzAJBgNVBAYTAk5MMRkwFwYDVQQDExBNYXJ0aWpuIEJyaW5rZXJzMSMwIQYJKoZIh" +
        "vcNAQkBFhRtLmJyaW5rZXJzQHBvYm94LmNvbTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEA2F1WbWOnUMPJ4X+zCtS0LGT6rUqRKgln" +
        "b4SVNVVX4RbGWN2p3QrhO2HrwGaPBWWvGF2mYL3S2B1T5/8naMHZZrkrlQPZe4EwYo3LBIySAJFQdZnCDs68xBwXJilqRoCfNHeV86hVQkU" +
        "4qN3g6CgdlrDyoUaFPzALyRv79P/f2CsCAwEAAaOByDCBxTAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIF4DAzBglghkgBhvhCAQgEJh" +
        "YkaHR0cDovL3d3dy50cnVzdGNlbnRlci5kZS9ndWlkZWxpbmVzMBEGCWCGSAGG+EIBAQQEAwIFoDBdBglghkgBhvhCAQMEUBZOaHR0cHM6L" +
        "y93d3cudHJ1c3RjZW50ZXIuZGUvY2dpLWJpbi9jaGVjay1yZXYuY2dpLzUyREMwMDAxMDAwMkIyQkI3NDc0OTQwMUY0NzU/MA0GCSqGSIb3" +
        "DQEBBAUAA4GBAIsb6tL1nA0rZzEff+lQpkhIyte0dGmFSrq6t34T92Ql87XW63EeMLrJZl0/17B1jancthAbSzV/h82XfYmjI2I95J86J1Z" +
        "yF6Hk84N37QA7mAghv/HlhjL5x8kQ7KgOQ6KmbcKfK2sbYmRgZ3q7/6EDKeXLIu9/c4CswqKzWdlb\"]";

    @Test
    public void testSerialize()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
            "certificates/testcertificate.cer"));

        Certificates certificates = new Certificates(Collections.singleton(certificate));

        assertEquals(1, certificates.getCertificates().size());

        String json = Certificates.toJSON(certificates);

        Certificates deserialized = Certificates.fromJSON(json);

        assertEquals(certificates.getCertificates(), deserialized.getCertificates());
    }

    @Test
    public void testDeserialize()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
            "certificates/testcertificate.cer"));

        Certificates certificates = Certificates.fromJSON(JSON_ENCODED_CERTS);

        assertEquals(1, certificates.getCertificates().size());
        assertEquals(certificate, certificates.getCertificates().iterator().next());
    }
}
