/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.common.dkim.DKIMUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang.mutable.MutableObject;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SenderPropertyDKIMSignTest
{
    private static final String KEY_PAIR_PEM =
            """
            -----BEGIN RSA PRIVATE KEY-----
            MIICXgIBAAKBgQCrUoJ16657mK99O8WYddgMFJPmXDIO49SaJzOp1/sohNd1T6Xh
            WWZVGHEfioxz+HmUFLnekyw51NE64O9cpaiVoZTga9fn5WjtmQNglvcbnhWedeUZ
            PgTkuhbUHgZNtkwvtKyRnTlh7ZxthF6W33q2Bj1CETuS2WdRs/4AA4CNCwIDAQAB
            AoGAaOu3ChC0Yu03TDL26FADaCKSEVoVLhlJcr7fXPzwy/fPHAETTdc6XJMDdJWd
            PsjFbHLlAfKP+zriiHSJIuwxObFocxleeTDKil3QlqXMjqFfF4PUP1XIiZREgDlE
            Gq8nqhFgHeUfV8U7kBejvuhF6r4FfWO5fjgLBbh8u0dTZkECQQDaMPdbNV84daV6
            5nkzYUgWBRn0NjRuIsVY+ugyhNgUexmreGFlpcWrDGMQDU2b2xSR2FEAILM/pxag
            NzxrTFu3AkEAyQJryIY/tl3OD1jvAJLJmDQT0kf0AUcbrL4bL0rWABlH4dk2db6Y
            MyGi88HcJNLkoadBHqJWIBiFWXaGvrzBTQJBAJbgYUt6vpuGDqXLlWfID1batDXA
            /cRi2uBKsCGu5tRSw09k8MSfOu6qpB3HdTEe7zxivrA97HVJj0W+rFLt/EUCQQCy
            2w6gzKOgV3NkwJNZhUMPxTbl4tRA1s7PNBDoUcR9LgGB+k61EjRHOuTN1G9X7Lc3
            B6Wv5m6P/IGbCxX2Xen5AkEAs/mUhmBH3T7Xd1PyeGk6ycIGdu4t+mZc0BhhKfN8
            z3KElieQ+Jy4cXrI5cdPJZwdxDdD0BQMsGnMVNSxz9j8zg==
            -----END RSA PRIVATE KEY-----
            """;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup() {
        setGlobalProperty("dkim-key-pair", KEY_PAIR_PEM);
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void setGlobalProperty(String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences preferences = globalPreferencesManager.getGlobalUserPreferences();

                UserProperties properties = preferences.getProperties();

                properties.setProperty(propertyName, value);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private boolean verify(MimeMessage message)
    throws Exception
    {
        return verify(message, DKIMUtils.DEFAULT_DKIM_HEADER);
    }

    private boolean verify(MimeMessage message, String dkimHeader)
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("publicKey", KEY_PAIR_PEM)
                .setProperty("resultAttribute", "ciphermail.dkim.result")
                .setProperty("dkimHeader", dkimHeader)
                .build();

        Mailet mailet = new StaticKeyDKIMVerify();

        mailet.init(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();

        mailet.service(mail);

        return "verified".equals(MailAttributesUtils.getManagedAttributeValue(mail,
                "ciphermail.dkim.result", String.class).orElse(null));
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SenderPropertyDKIMSign();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testSign()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                                                  "h=from:to; a=rsa-sha256; bh=; b=;")
                .setProperty("foldSignature", "true")
                .setProperty("keyProperty", "dkim-key-pair")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was DKIM signed"));

        assertTrue(verify(mail.getMessage()));
    }

    @Test
    public void testMissingFrom()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                                                  "h=from:to; a=rsa-sha256; bh=; b=;")
                .setProperty("foldSignature", "true")
                .setProperty("keyProperty", "dkim-key-pair")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        //sourceMessage.setFrom(new InternetAddress("from@example.com"));
        sourceMessage.removeHeader("From");
        sourceMessage.saveChanges();

        assertNull(sourceMessage.getHeader("From"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was DKIM signed"));

        MimeMessage signedMessage = mail.getMessage();

        assertTrue(verify(signedMessage));

        // changing from should result in DKIM failure
        signedMessage.setFrom(new InternetAddress("from@example.com"));
        signedMessage.saveChanges();

        assertFalse(verify(signedMessage));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "DKIM verification failed."));
    }

    @Test
    public void testCustomHeader()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                                                  "h=from:to; a=rsa-sha256; bh=; b=;")
                .setProperty("foldSignature", "true")
                .setProperty("keyProperty", "dkim-key-pair")
                .setProperty("dkimHeader", "X-X")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertNull(mail.getMessage().getHeader("X-X"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was DKIM signed"));

        assertNotNull(mail.getMessage().getHeader("X-X"));

        assertTrue(verify(mail.getMessage(), "X-X"));
    }

    @Test
    public void testFailBecauseOfMissingKey()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                                                  "h=from:to; a=rsa-sha256; bh=; b=;")
                .setProperty("foldSignature", "true")
                .setProperty("keyProperty", "no-key")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "The message could not be DKIM signed. There is no private key"));

        memoryLogAppender.reset();

        assertFalse(verify(mail.getMessage()));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "DKIM verification failed due to an error"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "No DKIM signature found"));
    }

    /*
     * The DKIM template is required to contain from
     */
    @Test
    public void testMissingFromInTemplate()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=djigzo; " +
                                                  "h=X-Unknown-Header; a=rsa-sha256; bh=; b=;")
                .setProperty("foldSignature", "true")
                .setProperty("keyProperty", "dkim-key-pair")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "DKIM signing failed"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Invalid signature template"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "From field not signed"));
    }

    @Test
    public void testSpeed()
    throws Exception
    {
        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        int repeat = 1000;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                    .setProperty("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                                                      "h=from:to; a=rsa-sha256; bh=; b=;")
                    .setProperty("foldSignature", "true")
                    .setProperty("keyProperty", "dkim-key-pair")
                    .build();

            FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                    .recipients("test@example.com")
                    .sender("sender@example.com")
                    .mimeMessage(sourceMessage)
                    .build();

            Mailet mailet = createMailet(mailetConfig);

            mailet.service(mail);

            checkLogsForErrorsOrExceptions();

            assertThat(memoryLogAppender.getLogOutput(), containsString(
                    "Message was DKIM signed"));

            assertTrue(verify(mail.getMessage()));
        }

        double signsPerSec = repeat * 1000.0 / (System.currentTimeMillis() - start);

        System.out.println("Signs/second: " + signsPerSec);

        double expected = 100 * TestProperties.getTestPerformanceFactor();

        assertTrue("can fail in slower systems.", signsPerSec > expected);
    }

    /*
     * Test if parsing a PEM keypair is fast enough
     */
    @Test
    public void testSpeedParsePEM()
    throws Exception
    {
        int repeat = 10000;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++) {
            assertNotNull(StaticKeyDKIMSign.parseKey(KEY_PAIR_PEM));
        }

        double perSec = repeat * 1000.0 / (System.currentTimeMillis() - start);

        System.out.println("parse/second: " + perSec);

        double expected = 1000 * TestProperties.getTestPerformanceFactor();

        assertTrue("can fail in slower systems.", perSec > expected);
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                                                  "h=from:to; a=rsa-sha256; bh=; b=;")
                .setProperty("foldSignature", "true")
                .setProperty("keyProperty", "dkim-key-pair")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        Mailet mailet = new SenderPropertyDKIMSign()
        {
            @Override
            protected void onHandleUserEvent(User user, MutableObject pemKeyPairHolder)
            throws MessagingException
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
                super.onHandleUserEvent(user, pemKeyPairHolder);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was DKIM signed"));

        assertTrue(verify(mail.getMessage()));
    }

    @Test
    public void testInvalidKey()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("signatureTemplate", "v=1; c=relaxed/relaxed; s=selector; d=example.com; " +
                                                  "h=from:to; a=rsa-sha256; bh=; b=;")
                .setProperty("foldSignature", "true")
                .setProperty("keyProperty", "dkim-key-pair")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // configure an invalid DKIM signing key
        setGlobalProperty("dkim-key-pair", "invalid-key");

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "DKIM signing failed"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "The PEM encoded blob did not return any object"));
    }
}
