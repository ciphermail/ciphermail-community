/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class HadValidSMIMESignatureTest
{
    @Autowired
    private AbstractApplicationContext applicationContext;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mail createMail(String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject("test");
        message.setFrom(new InternetAddress("test@example.com"));
        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new HadValidSMIMESignature();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testValidSignatureFirstLayer()
    throws Exception
    {
        Mail mail = createMail("test1@example.com", "test2@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[0]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());

        assertTrue(result.contains(new MailAddress("test1@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));
    }

    @Test
    public void testValidSignatureFirstLayerCheckSenderMismatchEnabled()
    throws Exception
    {
        Mail mail = createMail("test1@example.com", "test2@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-0", "test1@example.com");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-0", "test@example.com");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-0", "test2@example.com");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[0]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());

        assertTrue(result.contains(new MailAddress("test1@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));
    }

    @Test
    public void testValidSignatureFirstLayerCheckSenderMismatchEnabledMismatch()
    throws Exception
    {
        Mail mail = createMail("test1@example.com", "test2@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-0", "test1@example.com");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-0", "test2@example.com");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-0", "test3@example.com");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[0], checkSenderMismatch:true}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testValidSignatureFirstLayerCheckSenderMismatchEnabledMissingSigners()
    throws Exception
    {
        Mail mail = createMail("test1@example.com", "test2@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[0], checkSenderMismatch:true}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testValidSignatureWrongLayer()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[1]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testValidSignatureWrongLayerMismatchCheckEnabled()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-0", "test@example.com");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[1], checkSenderMismatch:true}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testNoSignatures()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[0,1,2,3]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testTwoLayersValid()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Verified-0-2", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-2", "True");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[0,1,2]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());

        assertTrue(result.contains(new MailAddress("recipient@example.com")));
    }

    @Test
    public void testTwoLayersValidMismatchEnabled()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-0", "testX@example.com");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-0", "test@example.com");
        message.setHeader("X-CipherMail-Info-Signer-Verified-0-2", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-2", "True");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-2", "test@example.com");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-2", "testY@example.com");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[0,1,2], checkSenderMismatch:true}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());

        assertTrue(result.contains(new MailAddress("recipient@example.com")));
    }

    @Test
    public void testMultipleLayersOneInvalid()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Verified-0-2", "False");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-2", "True");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[0,1,2]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testNotTrusted()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-1", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-1", "False");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[1]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testNotTrustedMismatchCheckEnabled()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-1", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-1", "False");
        message.addHeader("X-CipherMail-Info-Signer-Email-0-1", "test@example.com");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[1], checkSenderMismatch:true}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testNotVerified()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-1", "XXX");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-1", "False");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[1]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testHeaderMismatch1()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-1", "True");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[1]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "trusted/verified header mismatch"));

        assertEquals(0, result.size());
    }

    @Test
    public void testHeaderMismatch2()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-1", "True");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[1]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "trusted/verified header mismatch"));

        assertEquals(0, result.size());
    }

    @Test
    public void testHeaderMismatch3()
    throws Exception
    {
        Mail mail = createMail("recipient@example.com");

        MimeMessage message = mail.getMessage();

        message.setHeader("X-CipherMail-Info-Signer-Verified-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-0", "True");
        message.setHeader("X-CipherMail-Info-Signer-Trusted-0-2", "True");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{layers:[0,1,2]}")
                .build();

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "trusted/verified header mismatch"));

        assertEquals(0, result.size());
    }
}
