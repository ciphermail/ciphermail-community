/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PGPPublicKeysTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static final String JSON_ENCODED_PUBLIC_KEYS =
            "[\"mQENBFJVA8UBCACkaxAP5qHVuFhg5BNoqRcXOg2oDex4Rn53LHQb8aRXZAQeUhBv9OlpStwYNiPhy+7L7Qh1RORAbRn0+HWhY754R" +
            "A3HpNKLN2InY1r4a2PRIn8EHiIQlB2dnszPr8CBSIRSjvuDC/SAz8EVC77TR36fra2lF3CXUyJmJUGa258I+UV+kqPLcrPspufOVn27" +
            "/Q7Vke0XNmVu0E8+8L1Rf4Dlt9l4pjeP9Y7Fbu10bN8tg9cU5tBeRdjUOH5RvmEDRBJT/u7cb7VFgHuErtPh/0cg2yzy4LK1HoRZWBR" +
            "uwvCuW0hzKyxK2ChyJ83obAV3YrRAGj+4iaNxyqzMjOncz0HhABEBAAG0N3Rlc3Qga2V5IGRqaWd6byAodGhpcyBpcyBhIHRlc3Qga2" +
            "V5KSA8dGVzdEBleGFtcGxlLmNvbT6JATgEEwECACIFAlJVA8UCGwMGCwkIBwMCBhUIAgkKCwQWAgMBAh4BAheAAAoJEE7E6IE+EamgN" +
            "NMH/14kdP8kJQOUOqal/y6+rnZLTfYJffykYVmN2sONZa+k1Xd0STli7bZ0kYPPHlrtpc4hwoHJxu17RgdUjxnBhmFuyPVji9kyzuP+" +
            "V+KtKHXSsqGt9ko29BjnZjDrix9RvjuQVbzf6mvrO3Tak+9g+XfqmqonoIgHmDpxXx/WKiJeBSCM8tO5hKrBayoEGyG2zrRRIJc/cXu" +
            "jIbmV3wBjJD2uCYOdb3BEtGg5o2+zApK5ffRW5kgA1UjAadTeOlMX6nWtpadS8sm0OSr6w8xk7JkMK4JP0sz8bfaIaHqaJwjvJFvr3/" +
            "C8bBsx/wY5mmFdkrKcNafuZMlZ0maRhKg+cZg=\",\"uQENBFJVA8UBCAC0ZN8OC0ahsewvdXgJWJq1VFr8ZOLYXL2A8bvBw5nvOX49q" +
            "O3IVjsYNvnxe59hgAAhSktAuFAMjI14f+AFcXorBWniK/2oKTQQ2/ShmTuKO438K6fajsnZsXwWJBV0dFEFa0wRoL9H4ymkw140JsLZ" +
            "SkkGMhGZl5gaEhTblc8eRIkVTvly0zVyDkfwxpBgGEad549IK7Hm+2hqBTzyjzP+36L4YldF8JtOnKJQCccc+djpsDe6uzVaxZsofEs" +
            "fDQPk5US1IcGcz4mX6tDWI+7txbPy1n/krKX40ZmNk/bBew160GeifSO69Jh4zr7LEMr1HkxrUk2LquxSLFe/9WVVABEBAAGJAR8EGA" +
            "ECAAkFAlJVA8UCGwwACgkQTsTogT4RqaBuBAf/eWo7fgdnJR8FCf5C6u9Vqhmb9/6paqQMFJ5cdRrODI4RyzNnnEAdi5vTrreJ1iFZl" +
            "VXLLinvXTmwsamzrFgFri9IYnwcvvDSZ09dWilEpnO+f/5VcNgVgYFWtx0TtYwdekEgc5KgsyCWmI6lEiRKHAG0TV9RoCHDkfvlpy51" +
            "fXbnujAGh7ZIace+QCEa6X4EfB6H3Qc3Ddgqy9WvyXkafVrr9mfilrtSj1zGd5f2CTIKEar/OVnLmZ+q5bP7Hf7phjex7Q5FgZ25ULS" +
            "1cf4GDBmivJh7sD3lrG92MiOqBx7vfohxa/doyf8iRZvwYq3JoqJEbktjePYqehxTtqFQIg==\"]";

    @Test
    public void testSerialize()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKeys container = new PGPPublicKeys(keys);

        assertEquals(keys.size(), container.getPublicKeys().size());

        PGPPublicKeys deserialized = PGPPublicKeys.fromJSON(PGPPublicKeys.toJSON(container));

        assertEquals(keys.size(), deserialized.getPublicKeys().size());

        for (int i = 0; i < keys.size(); i++) {
            assertTrue(PGPKeyUtils.isEquals(keys.get(i), deserialized.getPublicKeys().get(i)));
        }
    }

    @Test
    public void testDeserialize()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKeys deserialized = PGPPublicKeys.fromJSON(JSON_ENCODED_PUBLIC_KEYS);

        assertEquals(keys.size(), deserialized.getPublicKeys().size());

        for (int i = 0; i < keys.size(); i++) {
            assertTrue(PGPKeyUtils.isEquals(keys.get(i), deserialized.getPublicKeys().get(i)));
        }
    }
}
