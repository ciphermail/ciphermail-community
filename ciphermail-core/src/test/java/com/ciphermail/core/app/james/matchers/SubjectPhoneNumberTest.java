/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.properties.SMSProperties;
import com.ciphermail.core.app.properties.SMSPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SubjectPhoneNumberTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private SMSProperties createSMSProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMSPropertiesImpl.class)
                .createInstance(properties);
    }

    private void assertPhoneNumber(String email, String phoneNumber)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.NULL_IF_NOT_EXIST);

                assertNotNull(user);

                assertEquals(phoneNumber, createSMSProperties(user.getUserPreferences().getProperties())
                        .getSMSPhoneNumber());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setUserProperty(String email, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                user.getUserPreferences().getProperties().setProperty(propertyName, value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private boolean isUser(String email)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(status ->
        {
            try {
                return userWorkflow.getUser(email, UserNotExistResult.NULL_IF_NOT_EXIST) != null;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }));
    }

    private Mail createMail(String from, String subject, String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject(subject);
        message.setFrom(from != null ? new InternetAddress(from) : null);
        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new SubjectPhoneNumber();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testNumberNotAtEnd()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "+31123456 some text", "test2@example.com");

        assertFalse(isUser("test2@example.com"));

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
        assertEquals("+31123456 some text", mail.getMessage().getSubject());
        assertFalse(isUser("test2@example.com"));
    }

    @Test
    public void testMultipleRecipientsMatch()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "test 316123456", "test@example.com",
                "test2@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Found 2 recipients but only one is supported."));

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        assertFalse(isUser("test@example.com"));
        assertFalse(isUser("test2@example.com"));
    }

    @Test
    public void testMultipleRecipientsNoMatch()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "test", "test@example.com",
                "test2@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
        assertEquals("test", mail.getMessage().getSubject());
        assertFalse(isUser("test@example.com"));
        assertFalse(isUser("test2@example.com"));
    }

    @Test
    public void testOnlyNumber()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "+31123456", "test2@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("", mail.getMessage().getSubject());
        assertPhoneNumber("test2@example.com", "+31123456");
        assertTrue(isUser("test2@example.com"));
    }

    @Test
    public void testOnlyNumberNoPlus()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "31123456", "test2@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("", mail.getMessage().getSubject());
        assertPhoneNumber("test2@example.com", "+31123456");
        assertTrue(isUser("test2@example.com"));
    }

    @Test
    public void testNoNumber()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "test test", "test@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
        assertEquals("test test", mail.getMessage().getSubject());
    }

    @Test
    public void testNullSubject()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", null, "test@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testNullSender()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail(null, "test 31123456", "test@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        assertPhoneNumber("test@example.com", "+31123456");
    }

    @Test
    public void testNullDefaultCountryCode()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "test 06123456", "test@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Phone number is missing."));

        assertEquals(0, result.size());
    }

    @Test
    public void testDefaultCountryCode()
    throws Exception
    {
        setUserProperty("test@example.com", "sms-default-country-code", "31");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "test 06123456", "test@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        assertPhoneNumber("test@example.com", "+316123456");
    }

    @Test
    public void testSubjectPhoneNumber()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "test +31123456", "test@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        assertPhoneNumber("test@example.com", "+31123456");

        mail = createMail("test@example.com", "test 31 1 23456", "test@example.com");

        result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        assertPhoneNumber("test@example.com", "+31123456");

        mail = createMail("test@example.com", "test 31-1 (2)3456", "test@example.com");

        result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        assertPhoneNumber("test@example.com", "+31123456");
    }

    @Test
    public void testAltRegExpression()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,\\d{6,25}")
                .build();

        Mail mail = createMail("test@example.com", "test 316123456", "test@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertEquals("test", mail.getMessage().getSubject());
        assertPhoneNumber("test@example.com", "+316123456");

        mail = createMail("test@example.com", "test 3161 23456", "test@example.com");

        result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
        assertEquals("test 3161 23456", mail.getMessage().getSubject());
    }

    @Test
    public void testMutipleNumbers()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "+31123456 test +31123456",
                "test2@example.com");

        assertFalse(isUser("test2@example.com"));

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("+31123456 test", mail.getMessage().getSubject());
        assertPhoneNumber("test2@example.com", "+31123456");
        assertTrue(isUser("test2@example.com"));
    }

    @Test
    public void testOriginalNumberInMailAttributes()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "test 123 31123456", "test2@example.com");

        assertFalse(isUser("test2@example.com"));

        CoreApplicationMailAttributes.setOriginalSubject(mail, "test 123 [encrypt] 31123456");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertEquals("test 123", mail.getMessage().getSubject());
        assertPhoneNumber("test2@example.com", "+31123456");
        assertTrue(isUser("test2@example.com"));
    }
}
