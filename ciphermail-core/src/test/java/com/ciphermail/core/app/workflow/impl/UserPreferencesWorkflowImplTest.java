/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow.impl;

import com.ciphermail.core.app.NamedCertificate;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.impl.NamedCertificateImpl;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.UserPreferencesWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.KeyAndCertificateImpl;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserPreferencesWorkflowImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserPreferencesWorkflow userPreferencesWorkflow;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private NamedBlobManager namedBlobManager;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                // Clean key/root store
                keyAndCertStore.removeAllEntries();

                namedBlobManager.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void checkNotReferencedFromNamedCertificates(X509Certificate certificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<UserPreferences> prefs = userPreferencesWorkflow.getReferencingFromNamedCertificates(
                        certificate, null, null);

                assertEquals(0, prefs.size());

                assertEquals(0, userPreferencesWorkflow.getReferencingFromNamedCertificatesCount(certificate));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addUsersWithNamedCertificate(X509Certificate certificate, String... emails)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                for (String email : emails)
                {
                    User user = userWorkflow.addUser(email);

                    userWorkflow.makePersistent(user);

                    NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

                    user.getUserPreferences().getNamedCertificates().add(namedCertificate);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void checkReferencedFromNamedCertificates(X509Certificate certificate, Integer firstResult,
            Integer maxResults, String... names)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<UserPreferences> prefs = userPreferencesWorkflow.getReferencingFromNamedCertificates(
                        certificate, firstResult, maxResults);

                assertEquals(names.length, prefs.size());

                Set<String> foundNames = new HashSet<>();

                for (UserPreferences pref : prefs) {
                    foundNames.add(pref.getName());
                }

                for (String name : names) {
                    assertTrue(foundNames.contains(name));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void checkNotReferencedFromCertificates(X509Certificate certificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<UserPreferences> prefs = userPreferencesWorkflow.getReferencingFromCertificates(certificate, null,
                        null);

                assertEquals(0, prefs.size());

                assertEquals(0, userPreferencesWorkflow.getReferencingFromCertificatesCount(certificate));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addUsersWithCertificate(X509Certificate certificate, String... emails)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                for (String email : emails)
                {
                    User user = userWorkflow.addUser(email);

                    userWorkflow.makePersistent(user);

                    user.getUserPreferences().getCertificates().add(certificate);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void checkReferencedFromCertificates(X509Certificate certificate, Integer firstResult,
            Integer maxResults, String... names)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<UserPreferences> prefs = userPreferencesWorkflow.getReferencingFromCertificates(certificate,
                        firstResult, maxResults);

                assertEquals(names.length, prefs.size());

                Set<String> foundNames = new HashSet<>();

                for (UserPreferences pref : prefs) {
                    foundNames.add(pref.getName());
                }

                for (String name : names) {
                    assertTrue(foundNames.contains(name));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void checkNotReferencedFromKeyAndCertificate(X509Certificate certificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<UserPreferences> prefs = userPreferencesWorkflow.getReferencingFromKeyAndCertificate(certificate,
                        null, null);

                assertEquals(0, prefs.size());

                assertEquals(0, userPreferencesWorkflow.getReferencingFromKeyAndCertificateCount(certificate));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }


    private void addUsersWithSigningCertificate(KeyAndCertificate keyAndCertificate, String... emails)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                for (String email : emails)
                {
                    User user = userWorkflow.addUser(email);

                    userWorkflow.makePersistent(user);

                    user.getUserPreferences().setKeyAndCertificate(keyAndCertificate);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void checkReferencedFromKeyAndCertificate(X509Certificate certificate, Integer firstResult,
            Integer maxResults, String... names)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<UserPreferences> prefs = userPreferencesWorkflow.getReferencingFromKeyAndCertificate(certificate,
                        firstResult, maxResults);

                assertEquals(names.length, prefs.size());

                Set<String> foundNames = new HashSet<>();

                for (UserPreferences pref : prefs) {
                    foundNames.add(pref.getName());
                }

                for (String name : names) {
                    assertTrue(foundNames.contains(name));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void createNamedBlob(String category, String name) {
        transactionOperations.execute(status -> namedBlobManager.createNamedBlob(category, name));
    }

    private void addUsersWithNamedBlob(String category, String name, String... emails)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                NamedBlob namedBlob = Objects.requireNonNull(namedBlobManager.getNamedBlob(category, name));

                for (String email : emails)
                {
                    User user = userWorkflow.addUser(email);

                    userWorkflow.makePersistent(user);

                    user.getUserPreferences().getNamedBlobs().add(namedBlob);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private long getReferencingFromNamedBlobsCount(String category, String name)
    {
        Long count = transactionOperations.execute(status ->
        {
            try {
                return userPreferencesWorkflow.getReferencingFromNamedBlobsCount(
                        namedBlobManager.getNamedBlob(category, name));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        return count != null ? count : 0L;
    }

    private List<UserPreferences> getReferencingFromNamedBlobs(String category, String name, Integer firstResult,
            Integer maxResults)
    {
        return transactionOperations.execute(status ->
        {

            NamedBlob namedBlob = Objects.requireNonNull(namedBlobManager.getNamedBlob(category, name));

            List<UserPreferences> prefs = userPreferencesWorkflow.getReferencingFromNamedBlobs(
                    namedBlob, firstResult, maxResults);

            // copy to force them to be loaded (except the binary blob and childs)
            List<UserPreferences> result = new LinkedList<>(prefs);

            // Sort to make sure the order is deterministic
            result.sort(Comparator.comparing(UserPreferences::getName));

            return result;
        });
    }

    private void deleteUser(String email)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.NULL_IF_NOT_EXIST);

                if (user != null) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testInUseNamedCertificates()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/hash-starts-with-0.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        assertNotNull(certificate);

        assertTrue(keyAndCertificateWorkflow.addCertificateTransacted(certificate));

        String[] emails = new String[]{"test@example.com", "test2@example.com"};

        checkNotReferencedFromNamedCertificates(certificate);

        addUsersWithNamedCertificate(certificate, emails);

        checkReferencedFromNamedCertificates(certificate, null, null, emails);
    }

    @Test
    public void testInUseCertificates()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/hash-starts-with-0.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        assertNotNull(certificate);

        assertTrue(keyAndCertificateWorkflow.addCertificateTransacted(certificate));

        String[] emails = new String[]{"test@example.com", "test2@example.com"};

        checkNotReferencedFromCertificates(certificate);

        addUsersWithCertificate(certificate, emails);

        checkReferencedFromCertificates(certificate, null, null, emails);
    }

    @Test
    public void testInUseKeyAndCertificate()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/hash-starts-with-0.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        assertNotNull(certificate);

        assertTrue(keyAndCertificateWorkflow.addCertificateTransacted(certificate));

        String[] emails = new String[]{"test@example.com", "test2@example.com"};

        checkNotReferencedFromKeyAndCertificate(certificate);

        addUsersWithSigningCertificate(new KeyAndCertificateImpl(null, certificate), emails);

        checkReferencedFromKeyAndCertificate(certificate, null, null, emails);
    }

    @Test
    public void testReferencingFromNamedBlobs()
    {
        String category = "category";
        String name = "name";

        createNamedBlob(category, name);

        addUsersWithNamedBlob(category, name, "test1@example.com", "test2@example.com");

        assertEquals(2, getReferencingFromNamedBlobsCount(category, name));

        List<UserPreferences> prefs = getReferencingFromNamedBlobs(category, name, null, null);

        assertEquals(2, prefs.size());

        UserPreferences pref = prefs.get(0);

        assertEquals("test1@example.com", pref.getName());
        assertEquals("USER", pref.getCategory());

        pref = prefs.get(1);

        assertEquals("test2@example.com", pref.getName());
        assertEquals("USER", pref.getCategory());

        // Check whether first and max result work
        //
        // Since we do not know what order getReferencingFromNamedBlobs will returns the results, we need to make sure
        // that all items are found irrespective of order
        Set<String> shouldFind = new HashSet<>();
        shouldFind.add("test1@example.com");
        shouldFind.add("test2@example.com");

        assertEquals(2, shouldFind.size());

        prefs = getReferencingFromNamedBlobs(category, name, 1, 1);

        assertEquals(1, prefs.size());

        pref = prefs.get(0);

        shouldFind.remove(pref.getName());

        assertEquals(1, shouldFind.size());

        prefs = getReferencingFromNamedBlobs(category, name, 0, 1);

        assertEquals(1, prefs.size());

        pref = prefs.get(0);

        shouldFind.remove(pref.getName());

        assertEquals(0, shouldFind.size());

        deleteUser("test1@example.com");

        assertEquals(1, getReferencingFromNamedBlobsCount(category, name));

        prefs = getReferencingFromNamedBlobs(category, name, null, null);

        assertEquals(1, prefs.size());

        pref = prefs.get(0);

        assertEquals("test2@example.com", pref.getName());
        assertEquals("USER", pref.getCategory());

        deleteUser("test2@example.com");

        assertEquals(0, getReferencingFromNamedBlobsCount(category, name));

        prefs = getReferencingFromNamedBlobs(category, name, null, null);

        assertEquals(0, prefs.size());
    }
}
