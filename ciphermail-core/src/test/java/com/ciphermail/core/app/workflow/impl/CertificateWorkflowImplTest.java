/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow.impl;

import com.ciphermail.core.app.NamedCertificate;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.impl.NamedCertificateImpl;
import com.ciphermail.core.app.workflow.CertificateWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.KeyAndCertificateImpl;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CertificateWorkflowImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt certStore;

    private CertificateWorkflow certificateWorkflow;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                // Clean key/root store
                certStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        certificateWorkflow = new CertificateWorkflowImpl(certStore, sessionManager, transactionOperations);
    }

    private boolean isInUse(X509Certificate certificate)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(status ->
        {
            try {
                return certificateWorkflow.isInUse(certificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }));
    }

    private void addUserWithCertificate(String email, X509Certificate certificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.addUser(email);

                userWorkflow.makePersistent(user);

                user.getUserPreferences().getCertificates().add(certificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addUserWithNamedCertificate(String email, X509Certificate certificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.addUser(email);

                userWorkflow.makePersistent(user);

                NamedCertificate namedCertificate = new NamedCertificateImpl("name", certificate);

                user.getUserPreferences().getNamedCertificates().add(namedCertificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addUserWithSigningCertificate(String email, KeyAndCertificate keyAndCertificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.addUser(email);

                userWorkflow.makePersistent(user);

                user.getUserPreferences().setKeyAndCertificate(keyAndCertificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private long getCertStoreSize()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return certStore.size();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }).longValue();
    }

    @Test
    public void testInUseNamedCertificate()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/hash-starts-with-0.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        assertTrue(certificateWorkflow.addCertificateTransacted(certificate));

        assertFalse(isInUse(certificate));

        assertFalse(isInUse(certificate));

        addUserWithNamedCertificate("test@example.com", certificate);

        assertTrue(isInUse(certificate));
    }

    @Test
    public void testInUseKeyAndCertificate()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/hash-starts-with-0.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        assertTrue(certificateWorkflow.addCertificateTransacted(certificate));

        assertFalse(isInUse(certificate));

        addUserWithSigningCertificate("test@example.com",
                new KeyAndCertificateImpl(null, certificate));

        assertTrue(isInUse(certificate));
    }

    @Test
    public void testInUse()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/hash-starts-with-0.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        assertTrue(certificateWorkflow.addCertificateTransacted(certificate));

        assertFalse(isInUse(certificate));

        addUserWithCertificate("test@example.com", certificate);

        assertTrue(isInUse(certificate));
    }

    @Test
    public void testNotInUse()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/hash-starts-with-0.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        assertTrue(certificateWorkflow.addCertificateTransacted(certificate));

        assertFalse(isInUse(certificate));
    }

    @Test
    public void testAddCertificate()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/hash-starts-with-0.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        assertTrue(certificateWorkflow.addCertificateTransacted(certificate));
    }

    @Test
    public void testAddCertificatesMultipleThreads()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/mozilla-trusted-roots.pem");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(file);

        Collection<Future<?>> futures = new LinkedList<>();

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (X509Certificate certificate : certificates)
            {
                Callable<Null> callable = () ->
                {
                    certificateWorkflow.addCertificateTransacted(certificate);

                    return null;
                };

                // Schedule twice to "force" ConstraintViolations
                futures.add(executorService.submit(callable));
                futures.add(executorService.submit(callable));
            }

            // Wait for results
            for (Future<?> future : futures) {
                future.get();
            }
        }
        finally {
            executorService.shutdown();
        }

        assertEquals(certificates.size(), getCertStoreSize());
    }
}
