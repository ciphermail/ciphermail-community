/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.SecurityInfoTags;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.notification.NotificationListener;
import com.ciphermail.core.common.notification.NotificationMessage;
import com.ciphermail.core.common.notification.NotificationService;
import com.ciphermail.core.common.notification.NotificationSeverity;
import com.ciphermail.core.common.notification.StandardNotificationFacilities;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.digest.Digests;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporterImpl;
import com.ciphermail.core.common.security.openpgp.PGPRecursiveValidatingMIMEHandler;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListEntry;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListException;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.core.common.security.openpgp.validator.ExpirationValidator;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.FileConstants;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.bouncycastle.openpgp.PGPException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.ContentDisposition;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.ParameterList;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPHandlerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPTrustList trustList;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Autowired
    private ExpirationValidator expirationValidator;

    // Keeps track of the number of temp files so we can check whether all temp files are cleared
    private int tempFileCount;

    private NotificationListenerImpl notificationListener;

    static class NotificationListenerImpl implements NotificationListener
    {
        private final List<String> facilities = new LinkedList<>();
        private final List<NotificationSeverity> severities = new LinkedList<>();
        private final List<NotificationMessage> messages = new LinkedList<>();

        @Override
        public String getName() {
            return "dummy NotificationListener";
        }

        @Override
        public void notificationEvent(String facility, NotificationSeverity severity, NotificationMessage message)
        {
            facilities.add(facility);
            severities.add(severity);
            messages.add(message);
        }
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    @Before
    public void before()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
                trustList.deleteAll();
            }
            catch (IOException | PGPTrustListException e) {
                throw new UnhandledException(e);
            }
        });

        notificationListener = new NotificationListenerImpl();

        notificationService.registerNotificationListener(notificationListener);

        //  get the current nr of temp files
        tempFileCount = TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp");

        // set the current date to a fixed date so keys will not expire
        expirationValidator.setCurrentDate(TestUtils.parseDate("01-Oct-2023 11:38:35 GMT"));
    }

    @After
    public void after()
    {
        // check if we have any temp file leakage
        assertEquals(tempFileCount, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));
    }

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void importKeyRing(InputStream input, String password)
    {
        transactionOperations.execute(status ->
        {
            try {
                return new PGPKeyRingImporterImpl(keyRing).importKeyRing(input,
                        new StaticPasswordProvider(password)).size();
            }
            catch (IOException | PGPException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private long getKeyRingSize()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyRing.getSize();
            }
            catch (IOException e) {
                throw new UnhandledException(e);
            }
        }).longValue();
    }

    private void trustKey(long keyID) {
        trustKey(keyID, false);
    }

    private void trustKey(long keyID, boolean includeSubKeys)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry keyEntry = CloseableIteratorUtils.toList(keyRing.getByKeyID(keyID)).get(0);

                PGPTrustListEntry trustEntry = trustList.createEntry(keyEntry.getPublicKey());

                trustEntry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(trustEntry, includeSubKeys);
            }
            catch (IOException | PGPTrustListException | CloseableIteratorException | PGPException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new PGPHandler();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testDecryptInline()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("Message has been PGP decrypted; " +
                "MailID: null; Recipients: [test@example.com]"));

        assertEquals("handled", mail.getState());

        MimeMessage handled = mail.getMessage();

        assertNotSame(sourceMessage, handled);

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertNull(handled.getHeader(PGPRecursiveValidatingMIMEHandler.PGP_DECRYPTION_KEY_NOT_FOUND));

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdf = mp.getBodyPart(1);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check MD5 HASH of PDF to check whether the attachment was correctly decrypted
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    /**
     * Message is PGP/INLINE encrypted. However, the default value for inlinePGPEnabled is false.
     */
    @Test
    public void testDecryptPGPInlineNotEnabledByDefault()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertSame(sourceMessage, mail.getMessage());
    }

    /**
     * Tests whether a message which is not PGP is handled without errors
     */
    @Test
    public void testInlineEnabledNoPGP()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/multiple-attachments.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertSame(sourceMessage, mail.getMessage());
    }

    /**
     * Tests a PGP/INLINE for which there is no decryption key
     */
    @Test
    public void testDecryptInlineMissingDecryptionKey()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("PGP decryption key not found; " +
                "Recipient Key IDs: [9DB59E3B674B8F6E, 8AAA6718AADEAF7F]"));

        MimeMessage newMessage = mail.getMessage();

        // Because a header has been added and the message was therefore cloned, the message is no longer the same
        // instance
        assertNotSame(sourceMessage, newMessage);

        assertEquals("True", newMessage.getHeader(
                "X-CipherMail-Info-PGP-Decryption-Key-Not-Found", ","));
        assertEquals("True", newMessage.getHeader(
                PGPRecursiveValidatingMIMEHandler.PGP_DECRYPTION_KEY_NOT_FOUND, ","));

        newMessage.removeHeader(PGPRecursiveValidatingMIMEHandler.PGP_DECRYPTION_KEY_NOT_FOUND);

        // The MIME should be the same if we remove the PGP_DECRYPTION_KEY_NOT_FOUND header
        ByteArrayOutputStream originalMIME = new ByteArrayOutputStream();
        ByteArrayOutputStream newMIME = new ByteArrayOutputStream();

        MailUtils.writeMessage(sourceMessage, originalMIME);
        MailUtils.writeMessage(newMessage, newMIME);

        assertArrayEquals(originalMIME.toByteArray(), newMIME.toByteArray());
    }

    /**
     * Tests what happens if a PGP message is decompressed exceeding the max size
     */
    @Test
    public void testMaxDecompressionExceeded()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("decompressionUpperlimit", "10")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Maximum bytes read have been reached."));

        MimeMessage newMessage = mail.getMessage();

        assertSame(sourceMessage, newMessage);
    }

    /**
     * If the number of PGP objects read exceeds the maximum, a PGP exception should be thrown.
     */
    @Test
    public void testDecryptMaxObjectsExceeded()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("maxObjects", "2")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Maximum number of PGP objects read"));

        MimeMessage newMessage = mail.getMessage();

        assertSame(sourceMessage, newMessage);
    }

    /**
     * Tests whether an exception is thrown when the max MIME depth is reached
     */
    @Test
    public void testDecryptMaxMimeDepthExceeded()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("maxMimeDepth", "0")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Maximum depth 0 reached. " +
                "Retrying (retry count: 1)"));

        MimeMessage newMessage = mail.getMessage();

        assertSame(sourceMessage, newMessage);
    }

    @Test
    public void testDecryptPGPMIME()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-MIME-sign&encrypt.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        // key is not trusted
        assertThat(memoryLogAppender.getLogOutput(), containsString("PGP/MIME signature was not valid; " +
                "Failure message: Signer's key with key ID 4605970C271AD23B not found."));

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        assertEquals("PGP/MIME", newMessage.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertEquals("AES-256", newMessage.getHeader(
                "X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", newMessage.getHeader(
                "X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("False", newMessage.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("Signer's key with key ID\r\n 4605970C271AD23B not found.",
                newMessage.getHeader("X-CipherMail-Info-PGP-Signature-Failure", ","));
        assertNull(newMessage.getHeader(PGPRecursiveValidatingMIMEHandler.PGP_DECRYPTION_KEY_NOT_FOUND));

        Multipart mp = (Multipart) newMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdf = mp.getBodyPart(1);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check MD5 HASH of PDF to check whether the attachment was correctly decrypted
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    /**
     * Tests whether security info is added to the subject
     */
    @Test
    public void testDecryptPGPMIMEAddSubjectInfo()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("subjectTemplate", "%2$s %1$s")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-MIME-sign&encrypt.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals("test PGP/MIME sign&encrypt", sourceMessage.getSubject());

        // set the security info tags which should be added to the subject
        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        // check whether the message-id was retained
        assertEquals(sourceMessage.getMessageID(), newMessage.getMessageID());

        assertEquals("[invalid] [decrypted] test PGP/MIME sign&encrypt", newMessage.getSubject());

        Multipart mp = (Multipart) newMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdf = mp.getBodyPart(1);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check MD5 HASH of PDF to check whether the attachment was correctly decrypted
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    @Test
    public void testDecryptPGPMIMEDoNotRetainMessageID()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("retainMessageID", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-MIME-sign&encrypt.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        // the message-id should not be retained
        assertNotEquals(sourceMessage.getMessageID(), newMessage.getMessageID());

        assertThat(memoryLogAppender.getLogOutput(), containsString("Message has been PGP decrypted"));
    }

    @Test
    public void testPGPMIMEDecryptionKeyNotFound()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("retainMessageID", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-MIME-sign&encrypt.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("PGP decryption key not found; " +
                "Recipient Key IDs: [9DB59E3B674B8F6E, 8AAA6718AADEAF7F]"));

        MimeMessage newMessage = mail.getMessage();

        // because headers are added, message is cloned
        assertNotSame(sourceMessage, newMessage);

        assertEquals("True", mail.getMessage().getHeader(
                "X-CipherMail-Info-PGP-Decryption-Key-Not-Found", ","));
        assertEquals("True", mail.getMessage().getHeader(
                PGPRecursiveValidatingMIMEHandler.PGP_DECRYPTION_KEY_NOT_FOUND, ","));
    }

    /**
     * Test PGP/MIME message which is not compressed
     */
    @Test
    public void testDecryptPGPMIMEWihoutCompression()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                // set decompressionUpperlimit to low value so we will get an exception if the message was compressed
                .setProperty("decompressionUpperlimit", "1")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-MIME-no-compression.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        Multipart mp = (Multipart) mail.getMessage().getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertTrue(body.isMimeType("multipart/alternative"));

        BodyPart zipPart = mp.getBodyPart(1);

        assertEquals("application/octet-stream;\r\n\tname=\"cityinfo.zip\"", zipPart.getContentType());
    }

    /**
     * Test whether the signature layer is not removed if removeSignature is set to true
     */
    @Test
    public void testPGPMIMESignedNotRemoveSignature()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("removeSignature", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-MIME-signed.eml");

        assertEquals("<5279482E.8000504@djigzo.com>", sourceMessage.getMessageID());
        assertTrue(sourceMessage.isMimeType("multipart/signed"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals("PGP/MIME signed", sourceMessage.getSubject());

        // set the security info tags which should be added to the subject
        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        // check whether the message-id was retained
        assertEquals(sourceMessage.getMessageID(), newMessage.getMessageID());

        assertTrue(newMessage.isMimeType("multipart/signed"));

        assertEquals("PGP/MIME signed [invalid]", newMessage.getSubject());
        assertEquals("PGP/MIME", newMessage.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertNull(newMessage.getHeader("X-CipherMail-Info-PGP-Encrypted"));
        assertEquals("4605970C271AD23B", newMessage.getHeader(
                "X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("False", newMessage.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("Signer's key with key ID\r\n 4605970C271AD23B not found.",
                newMessage.getHeader("X-CipherMail-Info-PGP-Signature-Failure", ","));
    }

    @Test
    public void testPGPMIMESignedRemoveSignatureRetainMessageID()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-MIME-signed.eml");

        assertTrue(sourceMessage.isMimeType("multipart/signed"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        // check whether the message-id was retained
        assertEquals(sourceMessage.getMessageID(), newMessage.getMessageID());

        assertTrue(newMessage.isMimeType("text/plain"));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-PGP-Signed", ","));
    }

    @Test
    public void testPGPMIMESignedRemoveSignatureNotRetainMessageID()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("retainMessageID", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-MIME-signed.eml");

        assertTrue(sourceMessage.isMimeType("multipart/signed"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        // check whether the message-id was not retained
        assertNotEquals(sourceMessage.getMessageID(), newMessage.getMessageID());

        assertTrue(newMessage.isMimeType("text/plain"));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-PGP-Signed", ","));
    }

    /**
     * Test a PGP/MIME signed email created with Mutt
     */
    @Test
    public void testPGPMIMEMuttSigned()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP_MIME_mutt_plain_text_signed.eml");

        assertTrue(sourceMessage.isMimeType("multipart/signed"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        assertEquals(sourceMessage.getMessageID(), newMessage.getMessageID());

        assertTrue(newMessage.isMimeType("text/plain"));
        assertEquals("PGP/MIME", newMessage.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("6BEFD4D6B459AB58", newMessage.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("False", newMessage.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("Signer's key with key ID\r\n 6BEFD4D6B459AB58 not found.",
                newMessage.getHeader("X-CipherMail-Info-PGP-Signature-Failure", ","));
    }

    /**
     * Tests a PGP message which is encoded with a special partitioned format generated by PGP Universal.
     * PGP universal uses a special format (partitioned) when PGP/INLINE encrypting an email with an HTML part.
     */
    @Test
    public void testDecryptInlinePartitionedHTMLAltWithAttachment()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-inline-partitioned-signed-encrypted-html-alt-attachment.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        assertEquals(sourceMessage.getMessageID(), newMessage.getMessageID());

        assertEquals("PGP/INLINE", newMessage.getHeader("X-CipherMail-Info-PGP-Encoding", ","));

        assertTrue(newMessage.isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) newMessage.getContent();

        assertEquals(3, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);

        assertTrue(bp.isMimeType("text/plain"));
        assertTrue(((String)bp.getContent()).contains("Test"));

        bp = mp.getBodyPart(1);

        assertEquals("application/octet-stream; name=PGPexch.htm", bp.getContentType());
        assertEquals("alternative", StringUtils.join(bp.getHeader("X-PGP-MIME-Structure"), ","));

        assertTrue(IOUtils.toString(bp.getInputStream(), StandardCharsets.UTF_8).contains(
                "FONT face=Arial color=#ff0000 size=2>Test</FONT>"));

        bp = mp.getBodyPart(2);

        assertEquals("application/octet-stream; name=cityinfo.zip", bp.getContentType());
        assertEquals("application/octet-stream;\r\n\tname=\"cityinfo.zip\"",
                StringUtils.join(bp.getHeader("X-Content-PGP-Universal-Saved-Content-Type"), ","));
    }

    /**
     * Tests whether an attached PGP key is imported
     */
    @Test
    public void testPGPSignedImportKey()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("importKeys", "true")
                .setProperty("removeKeys", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-inline-signed-attached-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Replacing PGP keys with replacement text"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Key with key id 4605970C271AD23B added"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Key with key id 9DB59E3B674B8F6E added"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "New PGP keys have been imported"));

        assertEquals(2, getKeyRingSize());

        MimeMessage newMessage = mail.getMessage();

        assertTrue(newMessage.isMimeType("multipart/mixed"));
        assertEquals("PGP/INLINE", newMessage.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("4605970C271AD23B", newMessage.getHeader(
                "X-CipherMail-Info-PGP-Signer-KeyID", ","));

        Multipart mp = (Multipart) newMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", bp.getContentType());

        bp = mp.getBodyPart(1);

        assertEquals("text/plain; charset=us-ascii", bp.getContentType());
        assertEquals("*** Attached PGP keys have been removed ***", bp.getContent());

        assertEquals(1, notificationListener.messages.size());
        assertEquals(StandardNotificationFacilities.PGP_KEY_IMPORT, notificationListener.facilities.get(0));
        assertEquals(NotificationSeverity.INFORMATIONAL, notificationListener.severities.get(0));
        assertEquals("A keyring with master key ID 4605970C271AD23B was imported.",
                notificationListener.messages.get(0).getMessage());
    }

    /**
     * Tests whether the extracted key replacement text can be configured
     */
    @Test
    public void testPGPSignedImportKeySetReplacementText()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("importKeys", "true")
                .setProperty("removeKeys", "true")
                .setProperty("keyReplacementText", "123")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-inline-signed-attached-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Replacing PGP keys with replacement text"));

        assertEquals(2, getKeyRingSize());

        MimeMessage newMessage = mail.getMessage();

        Multipart mp = (Multipart) newMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(1);

        assertEquals("text/plain; charset=us-ascii", bp.getContentType());
        assertEquals("123", bp.getContent());
    }

    /**
     * Tests whether attached keys are not removed when removeKeys is false
     */
    @Test
    public void testPGPSignedImportKeyNoRemoveKeys()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("importKeys", "true")
                .setProperty("removeKeys", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-inline-signed-attached-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, getKeyRingSize());

        MimeMessage newMessage = mail.getMessage();

        Multipart mp = (Multipart) newMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(1);

        assertEquals("application/pgp-keys;\r\n name=\"0x271AD23B.asc\"", bp.getContentType());
    }

    /**
     * Tests whether keys are removed even if import keys is disabled
     */
    @Test
    public void testPGPSignedNoImportKeyRemove()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("importKeys", "false")
                .setProperty("removeKeys", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-inline-signed-attached-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Replacing PGP keys with replacement text"));

        assertEquals(0, getKeyRingSize());

        MimeMessage newMessage = mail.getMessage();

        Multipart mp = (Multipart) newMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(1);

        assertEquals("text/plain; charset=us-ascii", bp.getContentType());
        assertEquals("*** Attached PGP keys have been removed ***", bp.getContent());
    }

    /**
     * Tests whether PGP keys attached to an email which is not a PGP email, are imported
     */
    @Test
    public void testImportAttachKeysNoPGPMessage()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("importKeys", "true")
                .setProperty("removeKeys", "true")
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-attached-key.eml");

        assertTrue(sourceMessage.isMimeType("multipart/mixed"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Replacing PGP keys with replacement text"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Key with key id 4605970C271AD23B added"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Key with key id 9DB59E3B674B8F6E added"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "New PGP keys have been imported"));

        assertEquals(2, getKeyRingSize());

        MimeMessage newMessage = mail.getMessage();

        assertEquals(sourceMessage.getMessageID(), newMessage.getMessageID());

        Multipart mp = (Multipart) newMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(1);

        assertEquals("text/plain; charset=us-ascii", bp.getContentType());
        assertEquals("*** Attached PGP keys have been removed ***", bp.getContent());

        assertEquals("handled", mail.getState());
    }

    /**
     * Tests whether multiple attached key MIME parts are supported
     */
    @Test
    public void testImportKeyMultipleAttachedKeys()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("importKeys", "true")
                .setProperty("removeKeys", "true")
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-attached-multiple-keys.eml");

        assertTrue(sourceMessage.isMimeType("multipart/mixed"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Replacing PGP keys with replacement text"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Key with key id 94CEE89F0C345BCC added"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Key with key id 4605970C271AD23B added"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Key with key id 9DB59E3B674B8F6E added"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "New PGP keys have been imported"));

        assertEquals(3, getKeyRingSize());

        MimeMessage newMessage = mail.getMessage();

        Multipart mp = (Multipart) newMessage.getContent();

        assertEquals(3, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", bp.getContentType());

        bp = mp.getBodyPart(1);

        assertEquals("text/plain; charset=us-ascii", bp.getContentType());
        assertEquals("*** Attached PGP keys have been removed ***", bp.getContent());

        bp = mp.getBodyPart(2);

        assertEquals("text/plain; charset=us-ascii", bp.getContentType());
        assertEquals("*** Attached PGP keys have been removed ***", bp.getContent());

        assertEquals("handled", mail.getState());
    }

    /**
     * Tests whether multiple master keys from a single keyring gets imported
     */
    @Test
    public void testImportMultipleMasterKeysSingleKeyRing()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("importKeys", "true")
                .setProperty("removeKeys", "true")
                .setProperty("retainMessageID", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-attached-multiple-keys-one-keyring.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(11, getKeyRingSize());

        assertNotEquals(sourceMessage.getMessageID(), mail.getMessage().getMessageID());
    }

    /**
     * Tests whether an attached key is skipped if the size of the key (in bytes) exceeds the max
     *
     * Note: if a PGP blob contains keys, those keys are not checked for size and can be imported.
     */
    @Test
    public void testImportKeyAttachKeysMaxKeySizeExceededInlineDisabled()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "false")
                .setProperty("importKeys", "true")
                .setProperty("removeKeys", "true")
                .setProperty("maxKeySize", "10")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-attached-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Maximum bytes read have been reached"));

        assertEquals(0, getKeyRingSize());
    }

    /**
     * If PGP/INLINE is enabled, the key blob is treated as PGP/INLINE and the keys are therefore imported even if
     * they exceed the maxKeySize.
     *
     * TODO check whether we can limit the key size if the key is imported from a PGP blob
     */
    @Test
    public void testImportKeyAttachKeysMaxKeySizeExceededInlineEnabled()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("importKeys", "true")
                .setProperty("removeKeys", "true")
                .setProperty("maxKeySize", "10")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-attached-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        // the key importer refused the import the keys because of the size
        // however, the keys are imported anyway because they are handled as PGP/INLINE
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Maximum bytes read have been reached"));

        // the keys are imported even though maxKeySize was set to a low value
        // this is because the PGP keys are imported as a PGP/INLINE blob
        assertEquals(2, getKeyRingSize());
    }

    /**
     * Tests whether a notification is sent when a key gets imported
     */
    @Test
    public void testImportKeyNotification()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("importKeys", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-attached-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, getKeyRingSize());

        assertEquals(1, notificationListener.messages.size());
        assertEquals(StandardNotificationFacilities.PGP_KEY_IMPORT, notificationListener.facilities.get(0));
        assertEquals(NotificationSeverity.INFORMATIONAL, notificationListener.severities.get(0));
        assertEquals("A keyring with master key ID 4605970C271AD23B was imported.",
                notificationListener.messages.get(0).getMessage());
    }

    /**
     * Tests whether a notification is not sent when a key gets imported and notification is disabled
     */
    @Test
    public void testImportKeyNotificationDisabled()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("importKeys", "true")
                .setProperty("sendImportNotification" , "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-attached-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, getKeyRingSize());

        assertEquals(0, notificationListener.messages.size());
    }

    /**
     * Test whether a mesage which is not PGP is sent as-is
     */
    @Test
    public void testNoPGP()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("importKeys", "true")
                .setProperty("removeKeys" , "true")
                .setProperty("handledProcessor" , "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/test-5K.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertSame(sourceMessage, mail.getMessage());

        assertEquals("initial", mail.getState());
    }

    /**
     * PGP/INLINE messsage with some parts which are not signed, should be flagged as "mixed"
     */
    @Test
    public void testPGInlineSignedMixedContent()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/10C01C5A2F6059E7.asc")), null);

        trustKey(PGPUtils.getKeyIDFromHex("10C01C5A2F6059E7"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-inline-signed-mixed-content.eml");

        assertEquals("[Full-disclosure] [SECURITY] CVE-2013-4590 Information disclosure via XXE when " +
                "running untrusted web applications", sourceMessage.getSubject());

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/INLINE signature was valid"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/INLINE signed message contained mixed content"));

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        assertEquals("PGP/INLINE", newMessage.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("10C01C5A2F6059E7", newMessage.getHeader(
                "X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-PGP-Mixed-Content", ","));

        assertEquals("[Full-disclosure] [SECURITY] CVE-2013-4590 Information disclosure via XXE when " +
                "running untrusted web applications [signed] [mixed content]", newMessage.getSubject());
    }

    /**
     * A messaage which is signed with a key with a different email address then the sender should get the signed-by
     * tag
     */
    @Test
    public void testPGInlineSignedSenderSignerMismatch()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/10C01C5A2F6059E7.asc")), null);

        trustKey(PGPUtils.getKeyIDFromHex("10C01C5A2F6059E7"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-inline-signed-mixed-content.eml");

        sourceMessage.setFrom(new InternetAddress("mismatch@example.com"));

        assertEquals("[Full-disclosure] [SECURITY] CVE-2013-4590 Information disclosure via XXE when " +
                "running untrusted web applications", sourceMessage.getSubject());

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/INLINE signature was valid"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/INLINE signed message contained mixed content"));

        MimeMessage newMessage = mail.getMessage();

        assertNotSame(sourceMessage, newMessage);

        assertEquals("[Full-disclosure] [SECURITY] CVE-2013-4590 Information disclosure via XXE when running " +
                "untrusted web applications [signed by: markt@apache.org] [mixed content]", newMessage.getSubject());
    }

    /**
     * Tests all configurable Mail attributes supported by the PGPHandler mailet
     */
    @Test
    public void testMailAttributes()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabledAttribute", "test.inlinePGPEnabledAttribute")
                .setProperty("scanHTMLForPGPAttribute", "test.scanHTMLForPGPAttribute")
                .setProperty("skipNonPGPExtensionsAttribute", "test.skipNonPGPExtensionsAttribute")
                .setProperty("importKeysAttribute", "test.importKeysAttribute")
                .setProperty("removeKeysAttribute", "test.removeKeysAttribute")
                .setProperty("decryptAttribute", "test.decryptAttribute")
                .setProperty("removeSignatureAttribute", "test.removeSignatureAttribute")
                .build();

        //
        // Test inlinePGPEnabledAttribute & scanHTMLForPGPAttribute
        //

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/html-symantec-encryption-desktop.eml"))
                .build();

        // override the default values
        mail.setAttribute(Attribute.convertToAttribute("test.inlinePGPEnabledAttribute", true));
        mail.setAttribute(Attribute.convertToAttribute("test.scanHTMLForPGPAttribute", false));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        String mime = MailUtils.partToMimeString(mail.getMessage());

        // PGP encrypted HTML should not be decrypted
        assertThat(mime, containsString("-----BEGIN PGP MESSAGE-----"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message has been PGP decrypted"));

        checkLogsForErrorsOrExceptions();

        //
        // Test skipNonPGPExtensionsAttribute
        //

        // reset log output since we are only interested in the logs produced while handling this email
        memoryLogAppender.reset();

        mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(TestUtils.loadTestMessage(
                        "mail/PGP-signed-and-encrypted-filename-different.eml"))
                .build();

        // override the default values
        mail.setAttribute(Attribute.convertToAttribute("test.inlinePGPEnabledAttribute", true));
        mail.setAttribute(Attribute.convertToAttribute("test.skipNonPGPExtensionsAttribute", false));

        mailet.service(mail);

        mime = MailUtils.partToMimeString(mail.getMessage());

        // the attachment should have been decrypted
        assertThat(mime, containsString("Content-Type: application/octet-stream; name=djigzo-whitepaper.pdf"));

        checkLogsForErrorsOrExceptions();

        //
        // Test importKeysAttribute
        //

        // reset log output since we are only interested in the logs produced while handling this email
        memoryLogAppender.reset();

        mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/PGP-inline-signed-attached-key.eml"))
                .build();

        // override the default values
        mail.setAttribute(Attribute.convertToAttribute("test.importKeysAttribute", true));

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "New PGP keys have been imported"));

        checkLogsForErrorsOrExceptions();

        //
        // Test removeKeysAttribute
        //

        // reset log output since we are only interested in the logs produced while handling this email
        memoryLogAppender.reset();

        mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/PGP-inline-signed-attached-key.eml"))
                .build();

        // override the default values
        mail.setAttribute(Attribute.convertToAttribute("test.removeKeysAttribute", true));

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Replacing PGP keys with replacement text"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP keys were removed from the message"));

        checkLogsForErrorsOrExceptions();

        //
        // Test decryptAttribute
        //

        // reset log output since we are only interested in the logs produced while handling this email
        memoryLogAppender.reset();

        mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/PGP-MIME-sign&encrypt.eml"))
                .build();

        // override the default values
        mail.setAttribute(Attribute.convertToAttribute("test.decryptAttribute", false));

        mailet.service(mail);

        // message should not be decrypted
        assertTrue(mail.getMessage().isMimeType("multipart/encrypted"));

        // log should be empty
        assertEquals("", StringUtils.trimToEmpty(memoryLogAppender.getLogOutput()));

        checkLogsForErrorsOrExceptions();

        //
        // Test removeSignatureAttribute
        //

        // reset log output since we are only interested in the logs produced while handling this email
        memoryLogAppender.reset();

        mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/PGP-MIME-signed.eml"))
                .build();

        // override the default values
        mail.setAttribute(Attribute.convertToAttribute("test.removeSignatureAttribute", false));

        mailet.service(mail);

        // message should be signed
        assertTrue(mail.getMessage().isMimeType("multipart/signed"));

        checkLogsForErrorsOrExceptions();
    }

    /**
     * Test a message which was signed with a sub-key
     */
    @Test
    public void testPGPInlineSignedWithSubKeyMixed()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/carnil@cpan.org.789D6F057FD863FE.asc")), null);

        trustKey(PGPUtils.getKeyIDFromHex("789D6F057FD863FE"), true);

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-inline-signed-subkey-mixed-carnil@cpan.org.789D6F057FD863FE.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/INLINE signature was not valid; Failure message: " +
                        "Public key expired on Sun Sep 18 16:29:56 CEST 2016"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/INLINE signed message contained mixed content"));

        assertEquals("PGP/INLINE", mail.getMessage().getHeader(
                "X-CipherMail-Info-PGP-Encoding", ","));
    }

    @Test
    public void testInlineSignedUnicode()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/martijn@djigzo.com_0x271AD23B.asc")), null);

        trustKey(PGPUtils.getKeyIDFromHex("4605970C271AD23B"), true);

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-inline-signed-unicode.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/INLINE signature was valid"));

        String body = (String) mail.getMessage().getContent();

        assertThat(body, containsString("ὕαλον ϕαγεῖν"));
    }

    /*
     * Corrupt compressed file which results in an endless loop on GnuPG.
     *
     * This problem does not affect BC
     */
    @Test
    public void testPGPInlineCorruptCompressed_CVE_2014_4617()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("removeSignature", "true")
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-inline-corrupt-compressed-CVE-2014-4617.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state(Mail.DEFAULT)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Unexpected end of ZIP input stream"));

        assertEquals(Mail.DEFAULT, mail.getState());
    }

    @Test
    public void testPGPUniversalMIMESignedEncrypted()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/martijn@djigzo.com_0x271AD23B.asc")), null);

        trustKey(PGPUtils.getKeyIDFromHex("4605970C271AD23B"), true);

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-universal-MIME-signed-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message has been PGP decrypted"));

        MimeMessage handled = mail.getMessage();

        assertTrue(handled.isMimeType("multipart/mixed"));
        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
    }

    @Test
    public void testPGPUniversalWorkaroundDisabled()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/martijn@djigzo.com_0x271AD23B.asc")), null);

        trustKey(PGPUtils.getKeyIDFromHex("4605970C271AD23B"), true);

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enablePGPUniversalWorkaround", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-universal-MIME-signed-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(MailUtils.partToMimeString(sourceMessage), MailUtils.partToMimeString(mail.getMessage()));
    }

    @Test
    public void testPGPUniversalMIMESigned()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-universal-MIME-signed.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertTrue(handled.isMimeType("text/plain"));
        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("71E39DD73143B8AF", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
    }

    @Test
    public void testPGPMIMEDecryptionDisabled()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("decrypt", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-MIME-sign&encrypt.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(MailUtils.partToMimeString(sourceMessage), MailUtils.partToMimeString(mail.getMessage()));
    }

    /**
     * Tests an email which is first PGP/INLINE encrypted and then PGP/INLINE signed.
     */
    @Test
    public void testPGPInlineEncryptedThenInlineSigned()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/PGP-inline-encrypted-inline-signed.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertTrue(((String)handled.getContent()).contains("test"));
        assertFalse(((String)handled.getContent()).contains("-----BEGIN PGP MESSAGE-----"));
    }

    /**
     * Test whether an email encrypted with PGP Desktop can be decrypted
     */
    @Test
    public void testInlineHTMLSymantecEncrypted()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"), true);

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/html-symantec-encryption-desktop.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertTrue(handled.isMimeType("multipart/mixed"));
        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4EC4E8813E11A9A0", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("Test HTML Symantec Encryption Desktop ", handled.getSubject());
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signer-Mismatch", ","));
        assertEquals("test@example.com", handled.getHeader("X-CipherMail-Info-PGP-Signer-Email", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content"));

        Multipart outerMP = (Multipart) handled.getContent();

        assertEquals(2, outerMP.getCount());

        BodyPart body = outerMP.getBodyPart(0);

        assertTrue(body.isMimeType("multipart/alternative"));

        // After decryption of text and html part, both parts of the alternative part are text parts because the
        // html part actually contains text only
        Multipart alternativeMP = (Multipart) body.getContent();

        assertEquals(2, alternativeMP.getCount());

        body = alternativeMP.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertEquals("Test red",  ((String) body.getContent()).trim());

        // The other (alternative part which was originally HTML) is similar
        body = alternativeMP.getBodyPart(1);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertEquals("Test red",  ((String) body.getContent()).trim());

        // And then we we have an HTML part (was PGP partitioned)
        body = outerMP.getBodyPart(1);

        assertEquals("application/octet-stream; name=PGPexch.htm", body.getContentType());
        assertTrue(IOUtils.toString(body.getInputStream(), StandardCharsets.UTF_8).contains(
                "<p class=MsoNormal>Test <span style='color:red'>red</span><o:p></o:p></p>"));
    }

    @Test
    public void testInlineHTMLSymantecEncryptedDefaultNoScanHTMLForPGP()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"), true);

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("scanHTMLForPGP", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/html-symantec-encryption-desktop.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertTrue(handled.isMimeType("multipart/mixed"));
        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4EC4E8813E11A9A0", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signer-Mismatch", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content", ","));
        assertEquals("test@example.com", handled.getHeader("X-CipherMail-Info-PGP-Signer-Email", ","));

        Multipart outerMP = (Multipart) handled.getContent();

        assertEquals(2, outerMP.getCount());

        BodyPart body = outerMP.getBodyPart(0);

        assertTrue(body.isMimeType("multipart/alternative"));

        // After decryption of text and html part, both parts of the alternative part are text parts because the
        // html part actually contains text only
        Multipart alternativeMP = (Multipart) body.getContent();

        assertEquals(2, alternativeMP.getCount());

        body = alternativeMP.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertEquals("Test red",  ((String) body.getContent()).trim());

        // The other alternative part is not decrypted (still HTML)
        body = alternativeMP.getBodyPart(1);

        assertEquals("text/html; charset=\"us-ascii\"", body.getContentType());
        assertTrue(((String) body.getContent()).contains("<div>-----BEGIN PGP MESSAGE-----</div>"));

        // And then we we have an HTML part (was PGP partitioned)
        body = outerMP.getBodyPart(1);

        assertEquals("application/octet-stream; name=PGPexch.htm", body.getContentType());
        assertTrue(IOUtils.toString(body.getInputStream(), StandardCharsets.UTF_8).contains(
                "<p class=MsoNormal>Test <span style='color:red'>red</span><o:p></o:p></p>"));
    }

    /*
     * Test if non-standard charset works with HTML part
     */
    @Test
    public void testInlineHTMLHTMLISO_8859_2()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/test-HTML-PGP-ISO-8859-2.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertTrue(handled.isMimeType("multipart/alternative"));
        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));

        Multipart outerMP = (Multipart) handled.getContent();

        assertEquals(2, outerMP.getCount());

        BodyPart body = outerMP.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-2", body.getContentType());
        assertTrue(((String) body.getContent()).contains("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg"));

        // The other alternative part the same. It was HTML but now it's text
        body = outerMP.getBodyPart(1);

        assertEquals("text/plain; charset=ISO-8859-2", body.getContentType());
        assertTrue(((String) body.getContent()).contains("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg"));
    }

    /*
     * Test if non-standard charset works with HTML part. This time the content type specifies a different char set
     * that the encrypted PGP blob
     */
    @Test
    public void testInlineHTMLHTMLISO_8859_2_with_content_type_ascii()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/test-HTML-PGP-outer-ascii-inner-ISO-8859-2.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertTrue(handled.isMimeType("multipart/alternative"));
        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));

        Multipart outerMP = (Multipart) handled.getContent();

        assertEquals(2, outerMP.getCount());

        BodyPart body = outerMP.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-2", body.getContentType());
        assertTrue(((String) body.getContent()).contains("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg"));

        // The other alternative part the same. It was HTML but now it's text
        body = outerMP.getBodyPart(1);

        assertEquals("text/plain; charset=ISO-8859-2", body.getContentType());
        assertTrue(((String) body.getContent()).contains("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg"));
    }

    @Test
    public void testInlineSymantecDesktopEncryptedHTMLInlineImageAndAttachmentWithUnicodeFilename()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"), true);

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-desktop-partitioned-HTML-with-inline-image-and-attachment-with-unicode-filename.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertEquals("multipart/mixed;\r\n\tboundary=\"_007_5011D045B6C5A947B21BAF12DDBF724464B3BD60A1WINUQ50EVO2Q9_\"",
                handled.getContentType());

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(3, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);

        assertEquals("multipart/related;\r\n\tboundary=\"_006_5011D045B6C5A947B21BAF12DDBF724464B3BD60A1WINUQ50EVO2Q9_"
            + "\";\r\n\ttype=\"multipart/alternative\"", bp.getContentType());

        Multipart inner = (Multipart) bp.getContent();

        assertEquals(2, inner.getCount());

        bp = inner.getBodyPart(0);

        assertEquals("multipart/alternative;\r\n\tboundary=\"_000_5011D045B6C5A947B21BAF12DDBF724464B3BD60A1WINUQ50"
            + "EVO2Q9_\"", bp.getContentType());

        Multipart alternative = (Multipart) bp.getContent();

        assertEquals(2, alternative.getCount());

        bp = alternative.getBodyPart(0);

        assertEquals("text/plain; charset=iso-8859-1", bp.getContentType());
        assertEquals("CipherMail Logo", ((String)bp.getContent()).trim());

        bp = alternative.getBodyPart(1);

        assertEquals("text/plain; charset=iso-8859-1", bp.getContentType());
        assertEquals("CipherMail Logo", ((String)bp.getContent()).trim());

        bp = inner.getBodyPart(1);
        assertEquals("image/png; name=image001.png", bp.getContentType());
        assertEquals("<image001.png@01D0663D.64D84E40>", StringUtils.join(bp.getHeader("Content-ID")));

        bp = mp.getBodyPart(1);

        assertEquals("application/octet-stream; \r\n\tname=\"=?UTF-8?B?w6TDtsO8IMOEw5bDnC56aXA=?=\"",
                bp.getContentType());
        assertEquals("=?UTF-8?B?w6TDtsO8IMOEw5bDnC56aXA=?=", bp.getFileName());

        bp = mp.getBodyPart(2);

        assertEquals("application/octet-stream; name=PGPexch.htm", bp.getContentType());
        assertEquals("PGPexch.htm", bp.getFileName());

        assertTrue(IOUtils.toString(bp.getInputStream(), StandardCharsets.UTF_8).contains(
                "<img width=285 height=61 id=\"Picture_x0020_1\" src=\"cid:image001.png@01D0663D.64D84E40\">"));
    }

    @Test
    public void testDecryptInlineHTMLOnly()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-Inline-signed&encrypted-HTML-only.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));

        // The body has test two times. First the HTML contains test and a PGP/INLINE message which itself
        // contain test again.
        assertEquals("test \r\n\r\ntest", handled.getContent());
    }

    @Test
    public void testSkipNonPGPExtensionsDisabled()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .setProperty("skipNonPGPExtensions", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-signed-and-encrypted-filename-different.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdf = mp.getBodyPart(1);

        assertEquals("djigzo-whitepaper.pdf", pdf.getFileName());
        assertEquals("attachment", pdf.getDisposition());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    /*
     * Test that checks whether content-disposition parameters filename*0* and filename*1* are removed because they
     * still contain the old filename (with .pgp extension)
     */
    @Test
    public void testContentDispositionFilename()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com-expiration-2030-12-31.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP inline UTF-8 filename attachment.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(3, mp.getCount());

        BodyPart body = mp.getBodyPart(2);

        ContentDisposition cd = new ContentDisposition(body.getHeader("Content-Disposition")[0]);

        ParameterList pl = cd.getParameterList();

        assertEquals(1, pl.size());
        assertEquals("äöü ÄÖÜ.zip",  HeaderUtils.decodeTextQuietly(pl.get("filename")));
    }

    @Test
    public void testDecryptInlineMultipleKeysNotAllPartsDecryped()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-encrypted-multiple-parts-different-keys.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP decryption key not found; Recipient Key IDs: [C939AA447303FD41]"));

        MimeMessage handled = mail.getMessage();

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader(PGPRecursiveValidatingMIMEHandler.PGP_DECRYPTION_KEY_NOT_FOUND, ","));

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("Version: GnuPG v1"));

        BodyPart pdf = mp.getBodyPart(1);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    @Test
    public void testAddSecurityInfoToSubjectSkipTag()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key")), "test");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("inlinePGPEnabled", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-MIME-sign&encrypt.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "<skip>", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertEquals(sourceMessage.getMessageID(), handled.getMessageID());

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));

        assertEquals("test PGP/MIME sign&encrypt [invalid]", handled.getSubject());

        // Test skip invalid tag
        mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "<skip>", "[mixed content]"));

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        handled = mail.getMessage();

        assertEquals(sourceMessage.getMessageID(), handled.getMessageID());

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));

        assertEquals("test PGP/MIME sign&encrypt [decrypted]", handled.getSubject());
    }

    @Test
    public void testDecryptPGPMIMECurve25519()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/curve-25519@example.com.key")), "test");
        trustKey(PGPUtils.getKeyIDFromHex("971322AF21733471"), true);

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/curve-25519@example.com-signed-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage handled = mail.getMessage();

        assertEquals(sourceMessage.getMessageID(), handled.getMessageID());

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertEquals("AES-128", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("971322AF21733471", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));

        // Looks like Thunderbird created a multipart/mixed email with only one part. Weird.....
        assertTrue(handled.isMimeType("multipart/mixed"));

        String mime = MailUtils.partToMimeString(handled);

        assertTrue(mime.contains("""
                --l4NcVmvFiuZ78V32a6rmGglEOIIyCsHha\r
                Content-Type: text/plain; charset=utf-8\r
                Content-Language: en-US\r
                Content-Transfer-Encoding: quoted-printable\r
                \r
                test\r
                \r
                \r
                \r
                \r
                \r
                \r
                \r
                --l4NcVmvFiuZ78V32a6rmGglEOIIyCsHha--\r
                """));
    }

    @Test
    public void testPGPMIMESignedRemoveSignatureRetainMessageIDRetry()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        PGPHandler mailet = new PGPHandler()
        {
            @Override
            protected void serviceMailTransacted(Mail mail)
            throws MessagingException
            {
                super.serviceMailTransacted(mail);

                /*
                 * throw ConstraintViolationException for first time a user is handled
                 */
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-MIME-signed.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertTrue(constraintViolationThrown.booleanValue());

        MimeMessage handled = mail.getMessage();

        assertEquals("<5279482E.8000504@djigzo.com>", handled.getMessageID());
        assertTrue(handled.isMimeType("text/plain"));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Encrypted"));
    }
}
