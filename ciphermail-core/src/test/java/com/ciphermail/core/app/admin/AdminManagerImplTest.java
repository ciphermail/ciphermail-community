/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.admin;

import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class AdminManagerImplTest
{
    @Autowired
    private AdminManager adminManager;

    @Autowired
    private RoleManager roleManager;

    @Autowired
    private TransactionOperations transactionOperations;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            adminManager.deleteAll();
            roleManager.deleteAllRoles();
        });
    }

    private boolean hasRoles(String username, String... roles)
    {
        Admin admin = adminManager.getAdmin(username);

        assertNotNull(admin);

        Set<Role> authorities = admin.getRoles();

        Set<String> hasRoles = new HashSet<>();

        for (Role authority : authorities) {
            hasRoles.add(authority.getName());
        }

        for (String role : roles)
        {
            if (!hasRoles.contains(role)) {
                return false;
            }
        }

        return roles.length == authorities.size();
    }

    private void addRole(String username, String role)
    {
        Role authority = roleManager.getRole(role);

        assertNotNull(authority);

        Admin admin = adminManager.getAdmin(username);

        assertNotNull(admin);

        admin.getRoles().add(authority);
    }

    @Test
    public void testAddAuthorities()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            adminManager.persistAdmin(adminManager.createAdmin("admin1"));
            adminManager.persistAdmin(adminManager.createAdmin("admin2"));

            roleManager.persistRole(roleManager.createRole("TEST"));

            addRole("admin1", "TEST");
            addRole("admin2", "TEST");

            hasRoles("admin1", "TEST");
            hasRoles("admin2", "TEST");
        });
    }

    @Test
    public void testAddAdmin()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            adminManager.persistAdmin(adminManager.createAdmin("admin1"));

            Admin admin = adminManager.getAdmin("admin1");

            assertEquals("admin1", admin.getName());
            assertNull(admin.getPassword());
            assertTrue(hasRoles("admin1"));


            admin.setPassword("test");

            assertEquals("test", admin.getPassword());

            adminManager.persistAdmin(adminManager.createAdmin("admin2"));

            admin = adminManager.getAdmin("admin2");

            assertEquals("admin2", admin.getName());
            assertNull(admin.getPassword());
            assertTrue(hasRoles("admin1"));

             List<? extends Admin> admins = adminManager.getAdmins(null, null, SortDirection.ASC);

            assertEquals(2, admins.size());
            assertEquals("admin1", admins.get(0).toString());
            assertEquals("admin2", admins.get(1).toString());
        });
    }

    @Test
    public void testDeleteAdmin()
    {
        transactionOperations.executeWithoutResult(status -> {
            adminManager.persistAdmin(adminManager.createAdmin("admin"));
        });

        transactionOperations.executeWithoutResult(status ->
        {
            assertEquals(1, adminManager.getAdminCount());
            adminManager.deleteAdmin("admin");
            assertEquals(0, adminManager.getAdminCount());
        });
    }

    @Test
    public void testCascadeAuthorities()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            Admin admin = adminManager.createAdmin("admin");
            admin.getRoles().add(new Role("test"));
            adminManager.persistAdmin(admin);
        });

        transactionOperations.executeWithoutResult(status ->
        {
            Admin admin = adminManager.getAdmin("admin");
            Set<Role> authorities = admin.getRoles();
            assertEquals(1, authorities.size());
        });
    }

    @Test
    public void testGetEffectivePermissions()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            Admin admin = adminManager.createAdmin("admin");

            Role role1 = new Role("r1");
            role1.getPermissions().add("p1");
            role1.getPermissions().add("p2");

            Role role2 = new Role("r2");
            role2.getPermissions().add("p3");
            role2.getPermissions().add("p4");

            Role subRole1 = new Role("s1");
            subRole1.getPermissions().add("ps1");
            subRole1.getPermissions().add("ps2");

            Role subRole2 = new Role("s2");
            subRole2.getPermissions().add("ps3");
            subRole2.getPermissions().add("ps4");

            Role subSubRole1 = new Role("ss1");
            subSubRole1.getPermissions().add("ps5");
            subSubRole1.getPermissions().add("ps6");

            role1.getRoles().add(subRole1);
            role1.getRoles().add(subRole2);
            subRole2.getRoles().add(subSubRole1);

            admin.getRoles().add(role1);
            admin.getRoles().add(role2);

            adminManager.persistAdmin(admin);
        });

        transactionOperations.executeWithoutResult(status ->
        {
            Set<String> permissions = adminManager.getEffectivePermissions("admin");
            assertEquals(10, permissions.size());
            assertTrue(permissions.contains("p1"));
            assertTrue(permissions.contains("p2"));
            assertTrue(permissions.contains("p3"));
            assertTrue(permissions.contains("p4"));
            assertTrue(permissions.contains("ps1"));
            assertTrue(permissions.contains("ps2"));
            assertTrue(permissions.contains("ps3"));
            assertTrue(permissions.contains("ps4"));
            assertTrue(permissions.contains("ps5"));
            assertTrue(permissions.contains("ps6"));
        });
    }

    @Test
    public void testGetEffectivePermissionsCycle()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            Admin admin = adminManager.createAdmin("admin");

            Role role1 = new Role("r1");

            Role subRole1 = new Role("r2");
            subRole1.getPermissions().add("p1");

            Role subRole2 = new Role("r3");
            subRole2.getPermissions().add("p2");

            role1.getRoles().add(subRole1);
            subRole1.getRoles().add(subRole2);
            subRole2.getRoles().add(role1);

            admin.getRoles().add(role1);

            adminManager.persistAdmin(admin);
        });

        transactionOperations.executeWithoutResult(status ->
        {
            Set<String> permissions = adminManager.getEffectivePermissions("admin");
            assertEquals(2, permissions.size());
            assertTrue(permissions.contains("p1"));
            assertTrue(permissions.contains("p2"));
        });
    }
}
