/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.UserPreferencesPGPSigningKeySelector;
import com.ciphermail.core.app.properties.PGPProperties;
import com.ciphermail.core.app.properties.PGPPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.openpgp.PGPKeyFlagChecker;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporter;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.SigningKeyValidator;
import com.ciphermail.core.common.security.password.PasswordProvider;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class,
        UserPreferencesPGPSigningKeySelectorImplTest.LocalServices.class})
public class UserPreferencesPGPSigningKeySelectorImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyFlagChecker keyFlagChecker;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPKeyRingImporter keyRingImporter;

    @Autowired
    private PasswordProvider passwordProvider;

    @Autowired
    private UserPreferencesPGPSigningKeySelector userPreferencesPGPSigningKeySelector;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    public static class LocalServices
    {
        // For this test we must override the pgpSigningKeyValidator bean
        @Bean(CipherMailSystemServices.PGP_SIGNING_KEY_VALIDATOR_SERVICE_NAME)
        public PGPPublicKeyValidator pGPSigningKeyValidatorService(PGPKeyFlagChecker keyFlagChecker)
        {
            return new SigningKeyValidator(keyFlagChecker);
        }
    }

    @Before
    public void before()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();

                assertEquals(0, keyRing.getSize());

                keyRingImporter.importKeyRing(new FileInputStream(
                        new File(TEST_BASE, "test-multiple-sub-keys.gpg.key")),
                        passwordProvider);

                keyRingImporter.importKeyRing(new FileInputStream(
                        new File(TEST_BASE, "test@example.com.gpg.asc")),
                        passwordProvider);

                assertEquals(8, keyRing.getSize());
            }
            catch (IOException | PGPException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private boolean isValidForSigning(PGPKeyRingEntry entry)
    throws PGPException, IOException
    {
        PGPPublicKey publicKey = entry.getPublicKey();

        if (publicKey.isMasterKey()) {
            return keyFlagChecker.isMasterKeyValidForSigning(publicKey);
        }
        else {
            return keyFlagChecker.isSubKeyValidForSigning(entry.getParentKey().getPublicKey(), publicKey);
        }
    }

    private PGPKeyRingEntry getByKeyID(long keyID)
    throws PGPException, IOException, CloseableIteratorException
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getByKeyID(keyID, MissingKeyAlias.ALLOWED);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        // Assume we only have one hit for keyID
        assertTrue(list.size() <= 1);

        return !list.isEmpty() ? list.get(0) : null;
    }

    private void addEmailAddresses(long keyID, String... emails)
    throws PGPException, IOException, CloseableIteratorException
    {
        PGPKeyRingEntry entry = getByKeyID(keyID);

        assertNotNull(entry);

        Set<String> email = entry.getEmail();

        email.addAll(Arrays.asList(emails));

        entry.setEmail(email);
    }

    private void removeEmailAddresses(long keyID, String... emails)
    throws PGPException, IOException, CloseableIteratorException
    {
        PGPKeyRingEntry entry = getByKeyID(keyID);

        assertNotNull(entry);

        Set<String> email = entry.getEmail();

        email.removeAll(Arrays.asList(emails));

        entry.setEmail(email);
    }

    private PGPProperties createPGPProperties(@Nonnull HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(PGPPropertiesImpl.class)
                .createInstance(properties);
    }

    @Test
    public void testKeyNotSetMacthingEmailAddress()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                        UserPreferencesCategory.USER.toString());

                PGPKeyRingEntry signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                assertNotNull(signingKey);
                assertTrue(isValidForSigning(signingKey));
                assertNotNull(signingKey.getPrivateKey());
            }
            catch (IOException | PGPException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testKeyNotSetNoMatchingAddressMatch()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences userPreferences = new MockupUserPreferences("non-existing-user@example.com",
                        UserPreferencesCategory.USER.toString());

                PGPKeyRingEntry signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                assertNull(signingKey);
            }
            catch (IOException | PGPException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDomainMatch()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences userPreferences = new MockupUserPreferences("non-existing-user@example.com",
                        UserPreferencesCategory.USER.toString());

                addEmailAddresses(getByKeyID(PGPUtils.getKeyIDFromHex("D80D1572D0486F55")).getKeyID(),
                        "example.com");

                PGPKeyRingEntry signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                assertNotNull(signingKey);
                assertTrue(isValidForSigning(signingKey));
                assertNotNull(signingKey.getPrivateKey());
                assertEquals("D80D1572D0486F55", PGPUtils.getKeyIDHex(signingKey.getKeyID()));
            }
            catch (IOException | PGPException | HierarchicalPropertiesException | CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testRemoveEmailAddress()
    {
        String email = "someuser@example.com";

        UserPreferences userPreferences = new MockupUserPreferences(email,
                UserPreferencesCategory.USER.toString());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry key = getByKeyID(PGPUtils.getKeyIDFromHex("61FEA9640551AAEE"));

                assertNotNull(key);

                addEmailAddresses(key.getKeyID(), email);

                PGPKeyRingEntry signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                assertNotNull(signingKey);
                assertEquals(key, signingKey);
                assertTrue(isValidForSigning(signingKey));
                assertNotNull(signingKey.getPrivateKey());
                assertEquals(key.getSHA256Fingerprint(),createPGPProperties(userPreferences.getProperties())
                        .getPGPSigningKey());

            }
            catch (IOException | PGPException | HierarchicalPropertiesException | CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry key = getByKeyID(PGPUtils.getKeyIDFromHex("61FEA9640551AAEE"));

                // now remove the email address, the signing cert should no longer be valid
                removeEmailAddresses(key.getKeyID(), email);

                PGPKeyRingEntry signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                assertNull(signingKey);
                assertNull(createPGPProperties(userPreferences.getProperties()).getPGPSigningKey());
            }
            catch (IOException | PGPException | HierarchicalPropertiesException | CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDeleteKeyNoOtherKeyAvailable()
    {
        String email = "someuser@example.com";

        UserPreferences userPreferences = new MockupUserPreferences(email, UserPreferencesCategory.USER.toString());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry key = getByKeyID(PGPUtils.getKeyIDFromHex("80C4F95A9FE00D79"));

                assertNotNull(key);

                addEmailAddresses(key.getKeyID(), email);

                PGPKeyRingEntry signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                assertNotNull(signingKey);
                assertEquals(key, signingKey);
                assertTrue(isValidForSigning(signingKey));
                assertNotNull(signingKey.getPrivateKey());
                assertEquals(key.getSHA256Fingerprint(), createPGPProperties(userPreferences.getProperties())
                        .getPGPSigningKey());
            }
            catch (IOException | PGPException | HierarchicalPropertiesException | CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry key = getByKeyID(PGPUtils.getKeyIDFromHex("80C4F95A9FE00D79"));

                assertNotNull(key);

                // now remove the key entry completely
                keyRing.delete(key.getID());
                assertEquals(7, keyRing.getSize());

                PGPKeyRingEntry signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                assertNull(signingKey);
                assertNull(createPGPProperties(userPreferences.getProperties()).getPGPSigningKey());
            }
            catch (IOException | PGPException | HierarchicalPropertiesException | CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDeleteKeyOtherKeyAvailable()
    {
        String email = "test@example.com";

        UserPreferences userPreferences = new MockupUserPreferences(email, UserPreferencesCategory.USER.toString());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                createPGPProperties(userPreferences.getProperties()).setPGPSigningKey(
                        "2FC04AA5353543E07710EC99EC53D26020D683AE9F4DDFF8747F41F1138F5B3F");

                PGPKeyRingEntry signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                assertNotNull(signingKey);
                assertEquals("80C4F95A9FE00D79", PGPUtils.getKeyIDHex(signingKey.getKeyID()));
                assertTrue(isValidForSigning(signingKey));
                assertNotNull(signingKey.getPrivateKey());
                assertEquals("2FC04AA5353543E07710EC99EC53D26020D683AE9F4DDFF8747F41F1138F5B3F",
                        createPGPProperties(userPreferences.getProperties()).getPGPSigningKey());
            }
            catch (IOException | PGPException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                // now remove the key entry completely
                keyRing.delete(signingKey.getID());
                assertEquals(7, keyRing.getSize());

                signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

                assertNotNull(signingKey);
                assertNotNull(createPGPProperties(userPreferences.getProperties()).getPGPSigningKey());
                assertNotEquals("80C4F95A9FE00D79", PGPUtils.getKeyIDHex(signingKey.getKeyID()));
            }
            catch (IOException | PGPException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }
}
