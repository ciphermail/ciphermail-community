/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import org.apache.commons.lang.NotImplementedException;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class MockupUserWorkflow implements UserWorkflow
{
    private final Map<String, User> users = new LinkedHashMap<>();

    @Override
    public User getUser(@Nonnull String email, UserNotExistResult userNotExistResult)
    throws AddressException, HierarchicalPropertiesException
    {
        User user = users.get(email);

        if (user == null && userNotExistResult == UserNotExistResult.DUMMY_IF_NOT_EXIST)
        {
            user = new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                    null);
        }

        return user;
    }

    @Override
    public Set<User> getUsers(Integer firstResult, Integer maxResults, SortDirection sortDirection)
    throws AddressException, CloseableIteratorException, HierarchicalPropertiesException
    {
        throw new NotImplementedException("MockupUserWorkflow#getUsers");
    }

    @Override
    public long getUserCount() {
        return users.size();
    }

    @Override
    public User addUser(@Nonnull String email)
    throws AddressException, HierarchicalPropertiesException
    {
        return new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                null);
    }

    @Override
    public boolean isInUse(@Nonnull String email) {
        return false;
    }

    @Override
    public boolean deleteUser(@Nonnull User user) {
        return users.remove(user.getEmail()) != null;
    }

    @Override
    public User makePersistent(@Nonnull User user)
    {
        users.put(user.getEmail(), user);

        return user;
    }

    @Override
    public boolean isPersistent(@Nonnull User user) {
        return users.containsKey(user.getEmail());
    }

    @Override
    public CloseableIterator<String> getEmailIterator()
    {
        Iterator<String> it = users.keySet().iterator();

        return new CloseableIterator<String>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public String next() {
                return it.next();
            }

            @Override
            public void close() {
                // empty on purpose
            }

            @Override
            public boolean isClosed() {
                return false;
            }
        };
    }

    @Override
    public CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
        SortDirection sortDirection)
    {
        throw new NotImplementedException("MockupUserWorkflow#getEmailIterator");
    }

    @Override
    public CloseableIterator<String> searchEmail(@Nonnull String search, Integer firstResult, Integer maxResults,
        SortDirection sortDirection)
    {
        throw new NotImplementedException("MockupUserWorkflow#searchEmail");
    }

    @Override
    public long getSearchEmailCount(@Nonnull String search) {
        throw new NotImplementedException("MockupUserWorkflow#getSearchEmailCount");
    }
}
