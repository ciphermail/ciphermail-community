/*
 * Copyright (c) 2009-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.OutputStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


public class RemoveHeadersTest
{
    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new RemoveHeaders();

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testInvalidContentEncoding()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("pattern", "(?i)^Subject$")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/unknown-content-transfer-encoding.eml");

        sourceMessage.setFrom(new InternetAddress("sender@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals("test simple message", sourceMessage.getSubject());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        // subject should be removed
        assertNull(mail.getMessage().getSubject());
    }

    @Test
    public void testRemoveHeader()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("pattern", "(?i)^X-Evolution-.*")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/unicode.eml");

        sourceMessage.setFrom(new InternetAddress("sender@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        String sourceMessageID = mail.getMessage().getMessageID();

        assertNotNull(mail.getMessage().getHeader("X-Evolution-Account"));
        assertNotNull(mail.getMessage().getHeader("X-Evolution-Format"));
        assertNotNull(mail.getMessage().getHeader("X-Mailer"));

        mailet.service(mail);

        assertNull(mail.getMessage().getHeader("X-Evolution-Account"));
        assertNull(mail.getMessage().getHeader("X-Evolution-Format"));
        assertNotNull(mail.getMessage().getHeader("X-Mailer"));
        assertEquals(sourceMessageID, mail.getMessage().getMessageID());
        assertEquals("initial", mail.getState());
    }

    @Test
    public void testErrorProcessor()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("pattern", "(?i)^X-Evolution-.*")
                .setProperty("errorProcessor", "error")
                .build();

        MimeMessage sourceMessage = new MimeMessage(TestUtils.loadTestMessage("mail/unicode.eml"))
        {
            @Override
            public void writeTo(OutputStream os) throws IOException {
                throw new IOException("failed on purpose");
            }
        };

        sourceMessage.setFrom(new InternetAddress("sender@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertNotNull(mail.getMessage().getHeader("X-Evolution-Account"));

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message with message-id <1223111568.9369.2370.camel@ubuntu> and MailID null is not a " +
                "valid MIME message. The header(s) can therefore not be removed."));

        assertEquals("error", mail.getState());
        assertEquals(MailUtils.partToMimeString(sourceMessage), MailUtils.partToMimeString(mail.getMessage()));
    }
}
