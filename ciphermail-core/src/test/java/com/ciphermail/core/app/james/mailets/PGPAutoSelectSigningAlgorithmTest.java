/*
 * Copyright (c) 2020-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.common.security.openpgp.PGPHashAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporterImpl;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListEntry;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListException;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.AttributeName;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.bouncycastle.openpgp.PGPException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPAutoSelectSigningAlgorithmTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPTrustList trustList;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    @Before
    public void before()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
                trustList.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void importKeyRing(File file, String password)
    {
        transactionOperations.execute(status ->
        {
            try {
                return new PGPKeyRingImporterImpl(keyRing).importKeyRing(FileUtils.openInputStream(file),
                        new StaticPasswordProvider(password)).size();
            }
            catch (IOException | PGPException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void trustKey(long keyID) {
        trustKey(keyID, false);
    }

    private void trustKey(long keyID, boolean includeSubKeys)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry keyEntry = CloseableIteratorUtils.toList(keyRing.getByKeyID(keyID)).get(0);

                PGPTrustListEntry trustEntry = trustList.createEntry(keyEntry.getPublicKey());

                trustEntry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(trustEntry, includeSubKeys);
            }
            catch (IOException | PGPTrustListException | CloseableIteratorException | PGPException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new PGPAutoSelectSigningAlgorithm();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testAutoSelectBrainpool512()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),"pgp/brainpool-p-512@example.com.key"),
                "test");

        trustKey(PGPUtils.getKeyIDFromHex("F969CD61623B6BFC"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signingAlgorithmAttribute", "runtime.pgp.signingAlgorithm")
                .build();

        MimeMessage message = new MimeMessage((Session)null);
        message.setText("test");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("brainpool-p-512@example.com")
                .mimeMessage(message)
                .state(Mail.DEFAULT)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(PGPHashAlgorithm.SHA512, PGPHashAlgorithm.fromName(
                MailAttributesUtils.getManagedAttributeValue(mail,
                        "runtime.pgp.signingAlgorithm", String.class).get()));
        assertEquals(Mail.DEFAULT, mail.getState());
    }

    @Test
    public void testAutoSelectBrainpool512WithProcessor()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(), "pgp/brainpool-p-512@example.com.key"),
                "test");

        trustKey(PGPUtils.getKeyIDFromHex("F969CD61623B6BFC"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signingAlgorithmAttribute", "runtime.pgp.signingAlgorithm")
                .setProperty("processor", "some-processor")
                .build();

        MimeMessage message = new MimeMessage((Session)null);
        message.setText("test");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("brainpool-p-512@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(PGPHashAlgorithm.SHA512, PGPHashAlgorithm.fromName(
                MailAttributesUtils.getManagedAttributeValue(mail,
                        "runtime.pgp.signingAlgorithm", String.class).get()));

        assertEquals("some-processor", mail.getState());
    }

    @Test
    public void testAutoSelectNoKey()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signingAlgorithmAttribute", "runtime.pgp.signingAlgorithm")
                .setProperty("processor", "some-processor")
                .build();

        MimeMessage message = new MimeMessage((Session)null);
        message.setText("test");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("brainpool-p-512@example.com")
                .mimeMessage(message)
                .state("original")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(Optional.empty(), mail.getAttribute(AttributeName.of("runtime.pgp.signingAlgorithm")));

        assertEquals("original", mail.getState());
    }

    @Test
    public void testAutoSelectRetry()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(), "pgp/brainpool-p-512@example.com.key"),
                "test");

        trustKey(PGPUtils.getKeyIDFromHex("F969CD61623B6BFC"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signingAlgorithmAttribute", "runtime.pgp.signingAlgorithm")
                .build();

        MimeMessage message = new MimeMessage((Session)null);
        message.setText("test");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("brainpool-p-512@example.com")
                .mimeMessage(message)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        PGPAutoSelectSigningAlgorithm mailet = new PGPAutoSelectSigningAlgorithm()
        {
            @Override
            protected void serviceMailTransacted(Mail mail)
            throws MessagingException, IOException
            {
                super.serviceMailTransacted(mail);

                /*
                 * throw ConstraintViolationException for first time a user is handled
                 */
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 1)"));

        assertTrue(constraintViolationThrown.booleanValue());
        assertEquals(PGPHashAlgorithm.SHA512, PGPHashAlgorithm.fromName(
                MailAttributesUtils.getManagedAttributeValue(mail,
                        "runtime.pgp.signingAlgorithm", String.class).get()));
    }
}
