/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PGPPublicKeys;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporterImpl;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListEntry;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class RecipientHasPGPPublicKeysTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPTrustList trustList;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void before()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
                trustList.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private int importKeyRing(InputStream input, String password)
    {
        return transactionOperations.execute(status ->
        {
            try {
                return new PGPKeyRingImporterImpl(keyRing).importKeyRing(input,
                        new StaticPasswordProvider(password)).size();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }).intValue();
    }

    private void trustKey(long keyID) {
        trustKey(keyID, false);
    }

    private void trustKey(long keyID, boolean includeSubKeys)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry keyEntry = CloseableIteratorUtils.toList(keyRing.getByKeyID(keyID)).get(0);

                PGPTrustListEntry trustEntry = trustList.createEntry(keyEntry.getPublicKey());

                trustEntry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(trustEntry, includeSubKeys);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    public void addEmailAddressesToExistingKey(long keyID, String... emails)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CloseableIterator<PGPKeyRingEntry> keyRingIterator = keyRing.getByKeyID(keyID,
                        MissingKeyAlias.ALLOWED);

                List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(keyRingIterator);

                PGPKeyRingEntry entry = list.get(0);

                Set<String> email = entry.getEmail();

                email.addAll(Arrays.asList(emails));

                entry.setEmail(email);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mail createMail(String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject("test");
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new RecipientHasPGPPublicKeys();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testSingleRecipient()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc")), null);

        trustKey(PGPUtils.getKeyIDFromHex("8AAA6718AADEAF7F"));

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@ExamplE.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@ExamplE.com")));

        PGPPublicKeys publicKeys = CoreApplicationMailAttributes.getPGPPublicKeys(mail);

        assertNotNull(publicKeys);

        assertEquals(1, publicKeys.getPublicKeys().size());

        assertEquals("8AAA6718AADEAF7F", PGPUtils.getKeyIDHex(publicKeys.getPublicKeys().get(0).getKeyID()));
    }

    @Test
    public void testMultipleRecipients()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc")), null);
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/zimbra_security.asc")), null);

        trustKey(PGPUtils.getKeyIDFromHex("8AAA6718AADEAF7F"));
        trustKey(PGPUtils.getKeyIDFromHex("33F215C0B0EC29E0"));

        // associate info@ciphermail.com with key 8AAA6718AADEAF7F
        addEmailAddressesToExistingKey(PGPUtils.getKeyIDFromHex("8AAA6718AADEAF7F"), "info@ciphermail.com");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("TEST@EXAMPLE.COM", "security@zimbra.com", "info@ciphermail.com",
                "test3@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(3, result.size());
        assertTrue(result.contains(new MailAddress("teST@EXAmple.com")));
        assertTrue(result.contains(new MailAddress("security@zimbra.com")));
        assertTrue(result.contains(new MailAddress("info@ciphermail.com")));

        PGPPublicKeys publicKeys = CoreApplicationMailAttributes.getPGPPublicKeys(mail);

        assertNotNull(publicKeys);

        assertEquals(2, publicKeys.getPublicKeys().size());

        Set<String> keyIDs = new HashSet<>();

        for (PGPPublicKey publicKey : publicKeys.getPublicKeys()) {
            keyIDs.add(PGPUtils.getKeyIDHex(publicKey.getKeyID()));
        }

        // There are two keys since one key is used for both test@example.com and info@ciphermail.com
        assertTrue(keyIDs.contains("8AAA6718AADEAF7F"));
        assertTrue(keyIDs.contains("33F215C0B0EC29E0"));
    }

    @Test
    public void testNoKeys()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc")), null);
        importKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                "pgp/zimbra_security.asc")), null);

        trustKey(PGPUtils.getKeyIDFromHex("8AAA6718AADEAF7F"));
        trustKey(PGPUtils.getKeyIDFromHex("33F215C0B0EC29E0"));

        // associate info@ciphermail.com with key 8AAA6718AADEAF7F
        addEmailAddressesToExistingKey(PGPUtils.getKeyIDFromHex("8AAA6718AADEAF7F"), "info@ciphermail.com");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        RecipientHasPGPPublicKeys matcher = new RecipientHasPGPPublicKeys()
        {
            @Override
            protected boolean hasMatch(@Nonnull User user)
            throws MessagingException
            {
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                return super.hasMatch(user);
            }
        };

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        Mail mail = createMail("TEST@EXAMPLE.COM", "security@zimbra.com", "info@ciphermail.com",
                "test3@example.com");

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertTrue(constraintViolationThrown.booleanValue());

        assertEquals(3, result.size());
        assertTrue(result.contains(new MailAddress("teST@EXAmple.com")));
        assertTrue(result.contains(new MailAddress("security@zimbra.com")));
        assertTrue(result.contains(new MailAddress("info@ciphermail.com")));

        PGPPublicKeys publicKeys = CoreApplicationMailAttributes.getPGPPublicKeys(mail);

        assertNotNull(publicKeys);

        assertEquals(2, publicKeys.getPublicKeys().size());

        Set<String> keyIDs = new HashSet<>();

        for (PGPPublicKey publicKey : publicKeys.getPublicKeys()) {
            keyIDs.add(PGPUtils.getKeyIDHex(publicKey.getKeyID()));
        }

        // There are two keys since one key is used for both test@example.com and info@ciphermail.com
        assertTrue(keyIDs.contains("8AAA6718AADEAF7F"));
        assertTrue(keyIDs.contains("33F215C0B0EC29E0"));
    }
}
