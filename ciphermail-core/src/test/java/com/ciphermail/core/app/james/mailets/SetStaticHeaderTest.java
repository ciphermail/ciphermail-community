/*
 * Copyright (c) 2014-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.Header;
import javax.mail.internet.MimeMessage;
import java.util.Enumeration;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SetStaticHeaderTest
{
    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SetStaticHeader();

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testSetStaticHeaderInvalidEncoding()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("name", "X-Some-Header")
                .setProperty("value", "123")
                .build();

        MimeMessage sourceMessage =  TestUtils.loadTestMessage(
                "mail/message-with-attach-invalid-encoding.eml");

        // we need to return MailImpl because SetSender requires that in order to change the sender
        Mail mail = MailImpl.builder().name(MailImpl.getId())
                .addRecipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        assertNull(sourceMessage.getHeader("X-Some-Header", ","));
        assertEquals("<111>", sourceMessage.getMessageID());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals("123", newMessage.getHeader("X-Some-Header", ","));
        assertEquals("<111>", newMessage.getMessageID());
    }

    @Test
    public void testSetStaticHeaderSimpleMessage()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("name", "X-Some-Header")
                .setProperty("value", "123")
                .build();

        MimeMessage sourceMessage =  TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        // we need to return MailImpl because SetSender requires that in order to change the sender
        Mail mail = MailImpl.builder().name(MailImpl.getId())
                .addRecipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        assertNull(sourceMessage.getHeader("X-Some-Header", ","));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals("123", newMessage.getHeader("X-Some-Header", ","));
    }

    @Test
    public void testSetStaticHeaderFromAttribute()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("attribute", "some.attribute")
                .build();

        MimeMessage sourceMessage =  TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        // we need to return MailImpl because SetSender requires that in order to change the sender
        Mail mail = MailImpl.builder().name(MailImpl.getId())
                .addRecipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        assertNull(sourceMessage.getHeader("X-Some-Header", ","));

        mail.setAttribute(Attribute.convertToAttribute("some.attribute", "X-Some-Header:   123"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals("123", newMessage.getHeader("X-Some-Header", ","));
    }

    @Test
    public void testSetStaticHeaderFromAttributeUnicode()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("attribute", "some.attribute")
                .build();

        MimeMessage sourceMessage =  TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        // we need to return MailImpl because SetSender requires that in order to change the sender
        Mail mail = MailImpl.builder().name(MailImpl.getId())
                .addRecipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        assertNull(sourceMessage.getHeader("X-Some-Header", ","));

        mail.setAttribute(Attribute.convertToAttribute("some.attribute", "X-Schön:   Schön"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals("=?UTF-8?Q?Sch=C3=B6n?=", newMessage.getHeader(
                "=?UTF-8?Q?X-Sch=C3=B6n?=", ","));
    }

    @Test
    public void testSetStaticHeaderAttributeValueNotSet()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("attribute", "some.attribute")
                .build();

        MimeMessage sourceMessage =  TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        // we need to return MailImpl because SetSender requires that in order to change the sender
        Mail mail = MailImpl.builder().name(MailImpl.getId())
                .addRecipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        assertNull(sourceMessage.getHeader("X-Some-Header", ","));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        @SuppressWarnings("unchecked")
        Enumeration<Header> headers = newMessage.getAllHeaders();

        int i = 0;

        while (headers.hasMoreElements())
        {
            headers.nextElement();

            i++;
        }

        // Note. MIME-Version and Message-ID headers get added
        assertEquals(6, i);
    }
}
