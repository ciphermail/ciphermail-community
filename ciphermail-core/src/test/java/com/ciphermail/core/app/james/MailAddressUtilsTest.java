/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import org.apache.james.core.MailAddress;
import org.apache.james.core.MaybeSender;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Mail;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.ParseException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MailAddressUtilsTest
{
    @Test
    public void testGetRecipients()
    throws ParseException
    {
        Collection<MailAddress> recipients = new LinkedList<>();

        recipients.add(new MailAddress("test1@example.com"));
        recipients.add(new MailAddress("test2@example.com"));

        Mail mail = MailImpl.builder().name ("test").addRecipients(recipients).build();

        Collection<MailAddress> result = MailAddressUtils.getRecipients(mail);

        assertEquals(result, recipients);
    }

    @Test
    public void testFromAddressArrayToMailAddressList()
    throws ParseException
    {
        List<MailAddress> addresses = MailAddressUtils.fromAddressArrayToMailAddressList(
                new InternetAddress("test1@example.com"),
                new InternetAddress("test2@example.com"));

        assertEquals(2, addresses.size());
        assertEquals("test1@example.com", addresses.get(0).toString());
        assertEquals("test2@example.com", addresses.get(1).toString());
    }

    @Test
    public void testToMailAddressList()
    throws ParseException
    {
        Set<String> emails = new LinkedHashSet<>();

        emails.add("test1@example.com");
        emails.add("test2@example.com");

        List<MailAddress> addresses = MailAddressUtils.toMailAddressList(emails);

        assertEquals(2, addresses.size());
        assertEquals("test1@example.com", addresses.get(0).toString());
        assertEquals("test2@example.com", addresses.get(1).toString());
    }

    @Test
    public void testToMailAddressListArray()
    throws ParseException
    {
        String[] emails = new String[]{"test1@example.com", "test2@example.com"};

        List<MailAddress> addresses = MailAddressUtils.toMailAddressList(emails);

        assertEquals(2, addresses.size());
        assertEquals("test1@example.com", addresses.get(0).toString());
        assertEquals("test2@example.com", addresses.get(1).toString());
    }

    @Test
    public void testToInternetAddress()
    throws ParseException
    {
        assertEquals(new InternetAddress("test@example.com"), MailAddressUtils.toInternetAddress(
                new MailAddress("test@example.com")));
        assertNull(MailAddressUtils.toInternetAddress(MailAddress.nullSender()));

        assertEquals(new InternetAddress("test@example.com"), MailAddressUtils.toInternetAddress(
                MaybeSender.getMailSender("test@example.com")));
        assertNull(MailAddressUtils.toInternetAddress(MaybeSender.getMailSender("invalid")));
        assertNull(MailAddressUtils.toInternetAddress(MaybeSender.nullSender()));
    }
}
