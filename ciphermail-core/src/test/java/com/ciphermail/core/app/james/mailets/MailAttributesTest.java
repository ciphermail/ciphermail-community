/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.AttributeName;
import org.apache.mailet.AttributeValue;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class MailAttributesTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup() {
        deleteAllUsers();
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void deleteAllUsers()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setUserProperty(String email, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                userProperties.setProperty(propertyName, value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new MailAttributes();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testDelete()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("delete", "[ \"todelete\", \"anothertodelete\", \"nonexistingtodelete\"]")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mail.setAttribute(Attribute.convertToAttribute("todelete", "abc"));
        mail.setAttribute(Attribute.convertToAttribute("anothertodelete", true));
        mail.setAttribute(Attribute.convertToAttribute("nottodelete", 123));

        assertEquals("abc", MailAttributesUtils.getManagedAttributeValue(mail,
                "todelete", String.class).orElseThrow());

        assertEquals(true, MailAttributesUtils.getManagedAttributeValue(mail,
                "anothertodelete", Boolean.class).orElseThrow());

        assertEquals(123, (long) MailAttributesUtils.getManagedAttributeValue(mail,
                "nottodelete", Integer.class).orElseThrow());

        assertEquals(3, mail.attributeNames().count());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mail.attributeNames().count());

        assertEquals(Optional.empty(), MailAttributesUtils.getManagedAttributeValue(mail,
                "todelete", String.class));

        assertEquals(Optional.empty(), MailAttributesUtils.getManagedAttributeValue(mail,
                "anothertodelete", Boolean.class));

        assertEquals(123, (long) MailAttributesUtils.getManagedAttributeValue(mail,
                "nottodelete", Integer.class).orElseThrow());
    }

    @Test
    public void testAddSingleValue()
    throws Exception
    {
        setUserProperty("from@example.com", "p1", "property value");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("add",
                        "[" +
                                "{\"name\":\"k1\", \"type\":\"STRING\",\"value\":\"v1\"}," +
                                "{\"name\":\"k2\", \"type\":\"INT\",\"value\":\"2\"}," +
                                "{\"name\":\"k3\", \"type\":\"STRING\",\"value\":\"#{p1}\"}," +
                                "{\"name\":\"k4\", \"type\":\"STRING\",\"value\":\"${ORIGINATOR}\"}" +
                        "]")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        message.setFrom(new InternetAddress("from@example.com"));

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test@example.com")
                .mimeMessage(message)
                .state("original")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals(0, mail.attributeNames().count());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals("original", mail.getState());

        assertEquals(4, mail.attributeNames().count());

        assertEquals("v1", MailAttributesUtils.getManagedAttributeValue(mail,
                "k1", String.class).orElseThrow());

        assertEquals(2, (int) MailAttributesUtils.getManagedAttributeValue(mail,
                "k2", Integer.class).orElseThrow());

        assertEquals("property value", MailAttributesUtils.getManagedAttributeValue(mail,
                "k3", String.class).orElseThrow());

        assertEquals("from@example.com", MailAttributesUtils.getManagedAttributeValue(mail,
                "k4", String.class).orElseThrow());
    }

    @Test
    public void testSupportedTypes()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("add",
                        "[" +
                            "{\"name\":\"boolean\", \"type\":\"BOOLEAN\",\"value\":\"true\"}," +
                            "{\"name\":\"string\", \"type\":\"STRING\",\"value\":\"test\"}," +
                            "{\"name\":\"bytes\", \"type\":\"BYTES\",\"value\":\"dGVzdA==\"}," +
                            "{\"name\":\"int\", \"type\":\"INT\",\"value\":\"55\"}," +
                            "{\"name\":\"long\", \"type\":\"LONG\",\"value\":\"12345\"}," +
                            "{\"name\":\"float\", \"type\":\"FLOAT\",\"value\":\"3.14\"}," +
                            "{\"name\":\"date\", \"type\":\"DATE\",\"value\":\"2023-07-25T16:17:07.88046146+02:00\"}," +
                            "{\"name\":\"double\", \"type\":\"DOUBLE\",\"value\":\"1.2\"}" +
                        "]")
                .build();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .state("original")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals(0, mail.attributeNames().count());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals("original", mail.getState());

        assertEquals(8, mail.attributeNames().count());

        assertEquals(true, MailAttributesUtils.getManagedAttributeValue(mail,
                "boolean", Boolean.class).orElseThrow());

        assertEquals("test", MailAttributesUtils.getManagedAttributeValue(mail,
                "string", String.class).orElseThrow());

        assertEquals( "test", new String(MailAttributesUtils.getManagedAttributeValue(mail,
                "bytes", byte[].class).orElseThrow(), StandardCharsets.UTF_8));

        assertEquals(55, (int) MailAttributesUtils.getManagedAttributeValue(mail,
                "int", Integer.class).orElseThrow());

        assertEquals(12345L, (long) MailAttributesUtils.getManagedAttributeValue(mail,
                "long", Long.class).orElseThrow());

        assertEquals(3.14, (float) MailAttributesUtils.getManagedAttributeValue(mail,
                "float", Float.class).orElseThrow(), 0.001);

        assertEquals(ZonedDateTime.parse("2023-07-25T16:17:07.88046146+02:00"),
                MailAttributesUtils.getManagedAttributeValue(mail,"date", ZonedDateTime.class).orElseThrow());

        assertEquals(1.2, (double) MailAttributesUtils.getManagedAttributeValue(mail,
                "double", Double.class).orElseThrow(), 0.001);
    }

    @Test
    public void testAddCollectionValues()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("add",
                        "[" +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"v1\"}," +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"t1@example.com\"}," +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"t2@example.com\"}," +
                            "{\"name\": \"k2\", \"type\":\"INT\",\"value\":\"10\"}" +
                        "]")
                .setProperty("processor", "next")
                .build();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .state("original")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mail.setAttribute(Attribute.convertToAttribute("k2", "string value for k2"));

        assertEquals("original", mail.getState());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals("next", mail.getState());

        List<AttributeValue<?>> values = MailAttributesUtils.toAttributeValueList(
                MailAttributesUtils.getManagedAttributeValue(mail,
                "k1", List.class));

        assertEquals(3, values.size());

        assertEquals("v1", values.get(0).valueAs(String.class).orElseThrow());
        assertEquals("t1@example.com", values.get(1).valueAs(String.class).orElseThrow());
        assertEquals("t2@example.com", values.get(2).valueAs(String.class).orElseThrow());

        values = MailAttributesUtils.toAttributeValueList(MailAttributesUtils.getManagedAttributeValue(mail,
                "k2", List.class));

        assertEquals(2, values.size());

        assertEquals("string value for k2", values.get(0).valueAs(String.class).orElseThrow());
        assertEquals(10, (int) values.get(1).valueAs(Integer.class).orElseThrow());
    }

    @Test
    public void testAddSpecialVariables()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("add",
                        "[" +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"${originator}\"}," +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"${replyto}\"}," +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"${sender}\"}," +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"${from}\"}," +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"${RECIPIENTS}\"}," +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"${subject}\"}," +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"${messageid}\"}" +
                        "]")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        message.setHeader("Message-Id", "fixed-message-id");
        message.setFrom(new InternetAddress("from@example.com"));
        message.setReplyTo(new Address[]{new InternetAddress("reply@example.com")});

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com", "r2@example.com", "r3@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        List<AttributeValue<?>> values = MailAttributesUtils.toAttributeValueList(
                MailAttributesUtils.getManagedAttributeValue(mail,
                "k1", List.class));

        assertEquals(9, values.size());

        assertEquals("from@example.com", values.get(0).valueAs(String.class).orElseThrow());
        assertEquals("reply@example.com", values.get(1).valueAs(String.class).orElseThrow());
        assertEquals("sender@example.com", values.get(2).valueAs(String.class).orElseThrow());
        assertEquals("from@example.com", values.get(3).valueAs(String.class).orElseThrow());
        assertEquals("r1@example.com", values.get(4).valueAs(String.class).orElseThrow());
        assertEquals("r2@example.com", values.get(5).valueAs(String.class).orElseThrow());
        assertEquals("r3@example.com", values.get(6).valueAs(String.class).orElseThrow());
        assertEquals("test simple message", values.get(7).valueAs(String.class).orElseThrow());
        assertEquals("fixed-message-id", values.get(8).valueAs(String.class).orElseThrow());
    }

    @Test
    public void testMissingSpecialVariable()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("add",
                        "[" +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"${messageid}\"}" +
                        "]")
                .build();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertTrue(mail.getAttribute(AttributeName.of("k1")).isEmpty());
    }

    @Test
    public void testMissingUserProperty()
    throws Exception
    {
        setUserProperty("from@example.com", "p2", "prop value 2");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("add",
                        "[" +
                            "{\"name\": \"k1\", \"type\":\"STRING\",\"value\":\"#{p1}\"}," +
                            "{\"name\": \"k2\", \"type\":\"STRING\",\"value\":\"#{p2}\"}" +
                        "]")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        message.setFrom(new InternetAddress("from@example.com"));

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertTrue(mail.getAttribute(AttributeName.of("k1")).isEmpty());
        assertEquals("prop value 2", MailAttributesUtils.getManagedAttributeValue(mail,
                "k2", String.class).orElseThrow());
    }

    @Test
    public void testInvalidAddresses()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("add",
                        "[" +
                            "{\"name\":\"p1\", \"type\":\"STRING\",\"value\":\"${replyTo}\"}," +
                            "{\"name\":\"p2\", \"type\":\"STRING\",\"value\":\"${from}\"}," +
                            "{\"name\":\"p3\", \"type\":\"STRING\",\"value\":\"${subject}\"}" +
                        "]")
                .build();

        String mime = "Content-Type: text/plain\n" +
                "Subject: =?UTF-8?Q?=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E?= =?UTF-8?Q?=0F=10=11=12=13=14=15=16="
                + "17=18=19=1A=1B=1C=1D=1E?= =?UTF-8?Q?=1F_!\"#$%&'()*+,-./0123456789:;<=3D>?= =?UTF-8?Q?=3F@"
                + "ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^?= =?UTF-8?Q?=5F`abcdefghijklmnopqrstuvwxyz{|}~?=\n" +
                "From: \"=?UTF-8?Q?=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E=3D?=\" <from@example.com>\n" +
                "Reply-To: \"=?UTF-8?Q?=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E=3D?=\" <replyto@example.com>\n" +
                "\n" +
                "test\n";

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test@example.com")
                .mimeMessage(new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8)))
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(3, mail.attributeNames().count());

        assertEquals("=?UTF-8?Q?=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E=3D?= <replyto@example.com>",
                MailAttributesUtils.getManagedAttributeValue(mail,"p1", String.class).orElseThrow());

        assertEquals("=?UTF-8?Q?=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E=3D?= <from@example.com>",
                MailAttributesUtils.getManagedAttributeValue(mail,"p2", String.class).orElseThrow());

        assertEquals("\t\n\r !\"#$%&'()*+,-./0123456789:;<=>?@" +
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
                MailAttributesUtils.getManagedAttributeValue(mail,"p3", String.class).orElseThrow());
    }

    @Test
    public void testRetryOnConstraintViolation()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("add",
                        "[" +
                            "{\"name\":\"p1\", \"type\":\"STRING\",\"value\":\"1\"}," +
                            "{\"name\":\"p2\", \"type\":\"STRING\",\"value\":\"2\"}," +
                            "{\"name\":\"p3\", \"type\":\"STRING\",\"value\":\"3\"}" +
                        "]")
                .setProperty("processor", "next")
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        Mailet mailet = new MailAttributes()
        {
            @Override
            protected void serviceMailTransacted(Mail mail)
            throws MessagingException
            {
                /*
                 * throw ConstraintViolationException for first time a user is handled
                 */
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                super.serviceMailTransacted(mail);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 1)"));

        assertTrue(constraintViolationThrown.booleanValue());

        assertEquals("next", mail.getState());
        assertEquals(3, mail.attributeNames().count());

        assertEquals("1", MailAttributesUtils.getManagedAttributeValue(mail,"p1",
                String.class).orElseThrow());
        assertEquals("2", MailAttributesUtils.getManagedAttributeValue(mail,"p2",
                String.class).orElseThrow());
        assertEquals("3", MailAttributesUtils.getManagedAttributeValue(mail,"p3",
                String.class).orElseThrow());
    }
}
