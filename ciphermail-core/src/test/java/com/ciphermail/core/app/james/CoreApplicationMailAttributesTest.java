/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.ciphermail.core.common.dlp.PolicyViolation;
import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Mail;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

class CoreApplicationMailAttributesTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    void testSecurityInfoTags()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        Assertions.assertNull(CoreApplicationMailAttributes.getSecurityInfoTags(mail));

        SecurityInfoTags securityInfoTags = new SecurityInfoTags("decrypted", "signed",
                "signedBy", "signedInvalid", "mixed");

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, securityInfoTags);

        // Test whether the attributes are actually serialized/deserialized
        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        SecurityInfoTags deserializedSecurityInfoTags = CoreApplicationMailAttributes.getSecurityInfoTags(
                deserializedMail);

        Assertions.assertNotSame(securityInfoTags, deserializedSecurityInfoTags);

        Assertions.assertEquals("decrypted", deserializedSecurityInfoTags.getDecryptedTag());
        Assertions.assertEquals("signed", deserializedSecurityInfoTags.getSignedValidTag());
        Assertions.assertEquals("signedBy", deserializedSecurityInfoTags.getSignedByValidTag());
        Assertions.assertEquals("signedInvalid", deserializedSecurityInfoTags.getSignedInvalidTag());
        Assertions.assertEquals("mixed", deserializedSecurityInfoTags.getMixedContentTag());
    }

    @Test
    void testPortalInvitation()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        Assertions.assertNull(CoreApplicationMailAttributes.getPortalSignups(mail));

        PortalSignups portalSignups = new PortalSignups();

        // test empty set
        CoreApplicationMailAttributes.setPortalSignups(mail, portalSignups);

        // Test whether the attributes are actually serialized/deserialized
        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        PortalSignups deserializedPortalSignups = CoreApplicationMailAttributes.getPortalSignups(
                deserializedMail);

        Assertions.assertNotSame(portalSignups, deserializedPortalSignups);

        Assertions.assertEquals(0, deserializedPortalSignups.size());

        portalSignups.put("test1@example.com", new PortalSignupDetails("test1@example.com",
                1, "m1"));
        portalSignups.put("test2@example.com", new PortalSignupDetails("test2@example.com",
                Long.MAX_VALUE, "m2"));

        CoreApplicationMailAttributes.setPortalSignups(mail, portalSignups);

        // Test whether the attributes are actually serialized/deserialized
        deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        deserializedPortalSignups = CoreApplicationMailAttributes.getPortalSignups(deserializedMail);

        Assertions.assertEquals(2, deserializedPortalSignups.size());
        Assertions.assertEquals(1, deserializedPortalSignups.get("test1@example.com").getTimestamp());
        Assertions.assertEquals("m1", deserializedPortalSignups.get("test1@example.com").getMac());
        Assertions.assertEquals(Long.MAX_VALUE, deserializedPortalSignups.get("test2@example.com").getTimestamp());
        Assertions.assertEquals("m2", deserializedPortalSignups.get("test2@example.com").getMac());
    }

    @Test
    void testPasswords()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        Assertions.assertNull(CoreApplicationMailAttributes.getPasswords(mail));

        Passwords passwords = new Passwords();

        // test empty set
        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        // Test whether the attributes are actually serialized/deserialized
        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        Passwords deserializedPasswords = CoreApplicationMailAttributes.getPasswords(deserializedMail);

        Assertions.assertNotSame(passwords, deserializedPasswords);

        Assertions.assertEquals(0, deserializedPasswords.size());

        PasswordContainer passwordContainer = new PasswordContainer();

        passwordContainer.setPassword("p1");
        passwordContainer.setPasswordID("1");
        passwordContainer.setPasswordLength(1);

        passwords.put("test1@example.com", passwordContainer);

        passwordContainer = new PasswordContainer();

        passwordContainer.setPassword("p2");
        passwordContainer.setPasswordID("2");
        passwordContainer.setPasswordLength(2);

        passwords.put("test2@example.com", passwordContainer);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        deserializedPasswords = CoreApplicationMailAttributes.getPasswords(deserializedMail);

        Assertions.assertEquals(2, deserializedPasswords.size());
        Assertions.assertEquals("p1", deserializedPasswords.get("test1@example.com").getPassword());
        Assertions.assertEquals("1", deserializedPasswords.get("test1@example.com").getPasswordID());
        Assertions.assertEquals(1, (long) deserializedPasswords.get("test1@example.com").getPasswordLength());
        Assertions.assertEquals("p2", deserializedPasswords.get("test2@example.com").getPassword());
        Assertions.assertEquals("2", deserializedPasswords.get("test2@example.com").getPasswordID());
        Assertions.assertEquals(2, (long) deserializedPasswords.get("test2@example.com").getPasswordLength());
    }

    @Test
    void testCertificates()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        Assertions.assertNull(CoreApplicationMailAttributes.getCertificates(mail));

        Certificates certificates = new Certificates();

        // test empty set
        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        // Test whether the attributes are actually serialized/deserialized
        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        Certificates deserializedCertificates = CoreApplicationMailAttributes.getCertificates(deserializedMail);

        Assertions.assertNotSame(certificates, deserializedCertificates);

        Assertions.assertEquals(0, deserializedCertificates.getCertificates().size());

        X509Certificate certificate1 = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/testcertificate.cer"));
        X509Certificate certificate2 = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/mitm-test-root.cer"));

        certificates.setCertificates(Set.of(Objects.requireNonNull(certificate1),
                Objects.requireNonNull(certificate2)));

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        deserializedCertificates = CoreApplicationMailAttributes.getCertificates(deserializedMail);

        Assertions.assertEquals(2, deserializedCertificates.getCertificates().size());
        Assertions.assertTrue(deserializedCertificates.getCertificates().contains(certificate1));
        Assertions.assertTrue(deserializedCertificates.getCertificates().contains(certificate2));
    }

    @Test
    void testPolicyViolations()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        Assertions.assertNull(CoreApplicationMailAttributes.getPolicyViolations(mail));

        List<PolicyViolation> policyViolations = new LinkedList<>();

        policyViolations.add(new PolicyViolation("policy1", "rule1", "match1",
                PolicyViolationAction.BLOCK, false));
        policyViolations.add(new PolicyViolation("policy2", "rule2", "match2",
                PolicyViolationAction.MUST_ENCRYPT, true));

        CoreApplicationMailAttributes.setPolicyViolations(mail, policyViolations);

        // Test whether the attributes are actually serialized/deserialized
        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        List<PolicyViolation> deserializedPolicyViolations = CoreApplicationMailAttributes.getPolicyViolations(
                deserializedMail);

        Assertions.assertEquals(2, deserializedPolicyViolations.size());

        PolicyViolation policyViolation = deserializedPolicyViolations.get(0);
        Assertions.assertEquals("policy1", policyViolation.getPolicy());
        Assertions.assertEquals("rule1", policyViolation.getRule());
        Assertions.assertEquals("match1", policyViolation.getMatch());
        Assertions.assertEquals(PolicyViolationAction.BLOCK, policyViolation.getPriority());
        Assertions.assertFalse(policyViolation.isDelayEvaluation());

        policyViolation = deserializedPolicyViolations.get(1);
        Assertions.assertEquals("policy2", policyViolation.getPolicy());
        Assertions.assertEquals("rule2", policyViolation.getRule());
        Assertions.assertEquals("match2", policyViolation.getMatch());
        Assertions.assertEquals(PolicyViolationAction.MUST_ENCRYPT, policyViolation.getPriority());
        Assertions.assertTrue(policyViolation.isDelayEvaluation());
    }

    @Test
    void testPhoneNumbers()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        Assertions.assertNull(CoreApplicationMailAttributes.getPhoneNumbers(mail));

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        // test empty set
        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        // Test whether the attributes are actually serialized/deserialized
        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        PhoneNumbers deserializedPhoneNumbers = CoreApplicationMailAttributes.getPhoneNumbers(deserializedMail);

        Assertions.assertNotSame(phoneNumbers, deserializedPhoneNumbers);

        Assertions.assertEquals(0, deserializedPhoneNumbers.size());

        phoneNumbers.put("test1@example.com", new PhoneNumber("1234"));
        phoneNumbers.put("test2@example.com", new PhoneNumber("5678"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        deserializedPhoneNumbers = CoreApplicationMailAttributes.getPhoneNumbers(deserializedMail);

        Assertions.assertEquals(2, deserializedPhoneNumbers.size());
        Assertions.assertEquals("1234", deserializedPhoneNumbers.get("test1@example.com").getNumber());
        Assertions.assertEquals("5678", deserializedPhoneNumbers.get("test2@example.com").getNumber());
    }
}