/*
 * Copyright (c) 2021-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SenderSubjectPasswordTriggerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void addUserWithTrigger(String email, String trigger, Boolean enabled)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                String basePropertyName = "b";

                User user = userWorkflow.addUser(email);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                if (trigger != null) {
                    userProperties.setProperty(basePropertyName + ".trigger", trigger);
                }

                if (enabled != null)
                {
                    userProperties.setProperty(basePropertyName + ".enabled",
                            BooleanUtils.toStringTrueFalse(enabled));
                }

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mail createMail(String from, String subject, String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject(subject);
        message.setFrom(from != null ? new InternetAddress(from) : null);
        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new SenderSubjectPasswordTrigger();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testOneRecipient()
    throws Exception
    {
        addUserWithTrigger("test@example.com", "(?i)pw\\s*=\\s*(?<pw>\\S+)", true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger','enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt pw = 123$#",
                "SOMEUSER@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());

        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test encrypt", mail.getMessage().getSubject());

        // Check if password container exists and contains the password
        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);
        assertEquals(1, passwords.size());
        assertNotNull(passwords.get("someuser@example.com"));
        assertEquals("123$#", passwords.get("someuser@example.com").getPassword());
        assertNull(passwords.get("someuser@example.com").getPasswordID());
    }

    @Test
    public void testMultipleRecipients()
    throws Exception
    {
        addUserWithTrigger("test@example.com", "(?i)pw\\s*=\\s*(?<pw>\\S+)", true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger','enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "pw = 123$# test encrypt", "test1@example.com",
                "test2@example.com", "test3@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(3, result.size());

        assertTrue(result.contains(new MailAddress("test1@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));
        assertTrue(result.contains(new MailAddress("test3@example.com")));
        assertEquals("test encrypt", mail.getMessage().getSubject());

        // Check if password container exists and contains the password
        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);
        assertEquals(3, passwords.size());

        assertNotNull(passwords.get("test1@example.com"));
        assertEquals("123$#", passwords.get("test2@example.com").getPassword());
        assertNotNull(passwords.get("test2@example.com"));
        assertEquals("123$#", passwords.get("test2@example.com").getPassword());
        assertNotNull(passwords.get("test3@example.com"));
        assertEquals("123$#", passwords.get("test2@example.com").getPassword());
    }

    @Test
    public void testDisabled()
    throws Exception
    {
        addUserWithTrigger("test@example.com", "(?i)pw\\s*=\\s*(?<pw>\\S+)", false);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger','enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt pw = 123$#",
                "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());

        assertEquals("test encrypt pw = 123$#", mail.getMessage().getSubject());
        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNull(passwords);
    }

    @Test
    public void testMissingGroup()
    throws Exception
    {
        addUserWithTrigger("test@example.com", "(?i)pw\\s*=\\s*(\\S+)", true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger','enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt pw = 123$#",
                "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "There is no group with name pw"));

        assertEquals(1, result.size());

        assertEquals("test encrypt", mail.getMessage().getSubject());

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);

        // there should be no password
        assertTrue(passwords.isEmpty());
    }

    @Test
    public void testMissingPassword()
    throws Exception
    {
        addUserWithTrigger("test@example.com", "(?i)pw\\s*=\\s*(?<pw>\\S*)", true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger','enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt pw =",
                "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "No password found in subject header"));

        assertEquals(1, result.size());

        assertEquals("test encrypt", mail.getMessage().getSubject());

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);

        // there should be no password
        assertTrue(passwords.isEmpty());
    }

    @Test
    public void testChangeGroupName()
    throws Exception
    {
        addUserWithTrigger("test@example.com", "(?i)pw\\s*=\\s*(?<other>\\S+)", true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger',"
                           + "'enabledProperty':'b.enabled', 'groupName':'other'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt pw = 123$#",
                "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());

        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test encrypt", mail.getMessage().getSubject());
    }
}
