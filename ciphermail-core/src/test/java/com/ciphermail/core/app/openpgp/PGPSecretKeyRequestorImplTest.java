/*
 * Copyright (c) 2014-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.openpgp;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClient;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientSettingsImpl;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServers;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServersImpl;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPSecretKeyRequestorImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private KeyServerClient keyServerClient;

    @Autowired
    private PGPSecretKeyRequestor secretKeyRequestor;

    /*
     * Need this to restore the original HKPKeyServers in after
     */
    private HKPKeyServers originalHKPKeyServers;

    @Before
    public void setup() {
        originalHKPKeyServers = getKeyServers();
    }

    @After
    public void after() {
        setKeyServers(originalHKPKeyServers);
    }

    private void setGlobalProperty(String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences preferences = globalPreferencesManager.getGlobalUserPreferences();

                UserProperties properties = preferences.getProperties();

                properties.setProperty(propertyName, value);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private PGPSecretKeyRequestorResult requestSecretKey(String email)
    {
        return transactionOperations.execute(status ->
        {
            try {
                return secretKeyRequestor.requestSecretKey(email);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setKeyServers(HKPKeyServers servers)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyServerClient.setServers(servers);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private HKPKeyServers getKeyServers()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyServerClient.getServers();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testRequestSecretKeyEmailOnly()
    {
        setGlobalProperty("pgp-auto-publish-keys", "true");

        PGPSecretKeyRequestorResult result = requestSecretKey("test@example.com");

        assertNotNull(result.getKeyServerClientSubmitResult());

        setGlobalProperty("pgp-auto-publish-keys", "false");

        result = requestSecretKey("test@example.com");

        assertNull(result.getKeyServerClientSubmitResult());
    }

    @Test
    public void testRequestSecretKeyInvalidKeyServer()
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl("http://non.existing.domain.example.com"));

        setKeyServers(servers);

        setGlobalProperty("pgp-auto-publish-keys", "true");

        PGPSecretKeyRequestorResult result = requestSecretKey("test@example.com");

        assertNotNull(result.getKeyServerClientSubmitResult());
        assertEquals(0, result.getKeyServerClientSubmitResult().getURLs().size());
        assertEquals(1, result.getKeyServerClientSubmitResult().getErrors().size());
        assertTrue(result.getKeyServerClientSubmitResult().getErrors().get(0).contains(
                "UnknownHostException: non.existing.domain.example.com"));
    }
}
