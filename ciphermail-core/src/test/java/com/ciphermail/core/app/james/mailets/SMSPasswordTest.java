/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.james.PhoneNumber;
import com.ciphermail.core.app.james.PhoneNumbers;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.sms.DummySMSTransport;
import com.ciphermail.core.common.sms.SMSGateway;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.time.DateUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SMSPasswordTest
{
    @Autowired
    private SMSGateway smsGateway;

    @Autowired
    private DummySMSTransport smsTransport;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    {
        smsTransport.reset();
        smsGateway.start();
    }

    @After
    public void setUpAfter()
    throws Exception
    {
        // Wait for background thread to stop
        smsGateway.stop(DateUtils.MILLIS_PER_SECOND * 30);

        smsGateway.deleteAll();
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SMSPassword();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test(timeout=80000)
    public void testInvalidFrom()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("template", "sms.ftl")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/invalid-from-to-cc-reply-to.eml");

        String recipient = "recipient@example.com";

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients(recipient)
                .sender("sender@example.com")
                .mimeMessage(new MimeMessage(sourceMessage))
                .state(Mail.DEFAULT)
                .build();

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("ID");

        passwords.put(recipient, container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        phoneNumbers.put(recipient, new PhoneNumber("1234567890"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        // wait some time for the SMS messages to be transported
        while (smsTransport.getPhoneNumbers().size() < 1) {
            ThreadUtils.sleepQuietly(100);
        }

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '1234567890'"));

        assertEquals(1, smsTransport.getPhoneNumbers().size());
        assertTrue(smsTransport.getPhoneNumbers().contains("1234567890"));

        String sms = smsTransport.getMessages().get(0);

        assertEquals("A message from \"%8787\" <&^&^&^&^&^&)\"(&@^%## was sent to you by email. "
                     + "The pdf password is test the message id is ID", sms);

        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(MailUtils.partToMimeString(mail.getMessage()), MailUtils.partToMimeString(sourceMessage));
    }

    @Test(timeout=10000)
    public void testMissingFrom()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("template", "sms.ftl")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/normal-message-with-attach.eml");

        sourceMessage.setFrom(null);

        String recipient = "recipient@example.com";

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients(recipient)
                .sender("sender@example.com")
                .mimeMessage(new MimeMessage(sourceMessage))
                .state(Mail.DEFAULT)
                .build();

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("ID");

        passwords.put(recipient, container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        phoneNumbers.put(recipient, new PhoneNumber("1234567890"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        // wait some time for the SMS messages to be transported
        while (smsTransport.getPhoneNumbers().size() < 1) {
            ThreadUtils.sleepQuietly(100);
        }

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '1234567890'"));

        assertEquals(1, smsTransport.getPhoneNumbers().size());
        assertTrue(smsTransport.getPhoneNumbers().contains("1234567890"));

        String sms = smsTransport.getMessages().get(0);

        assertEquals("A message was sent to you by email. The pdf password is test the message id is ID", sms);

        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(MailUtils.partToMimeString(mail.getMessage()), MailUtils.partToMimeString(sourceMessage));
    }

    @Test(timeout=10000)
    public void testVeryLongFromAndRecipient()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("template", "sms.ftl")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/normal-message-with-attach.eml");

        String recipient = "abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789@example.com";

        sourceMessage.setFrom(new InternetAddress(recipient, "SOME LONG PERSONAL NAME"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients(recipient)
                .sender("sender@example.com")
                .mimeMessage(new MimeMessage(sourceMessage))
                .state(Mail.DEFAULT)
                .build();

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("ID");

        passwords.put(recipient, container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        phoneNumbers.put(recipient, new PhoneNumber("1234567890"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        // wait some time for the SMS messages to be transported
        while (smsTransport.getPhoneNumbers().size() < 1) {
            ThreadUtils.sleepQuietly(100);
        }

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '1234567890'"));

        assertEquals(1, smsTransport.getPhoneNumbers().size());
        assertTrue(smsTransport.getPhoneNumbers().contains("1234567890"));

        String sms = smsTransport.getMessages().get(0);

        assertEquals("A message from abcdefghijklmnopqrstuvwxyz0123456789abcdefghi[...] was sent to you " +
                     "by email. The pdf password is test the message id is ID", sms);

        assertEquals(Mail.DEFAULT, mail.getState());
        assertEquals(MailUtils.partToMimeString(mail.getMessage()), MailUtils.partToMimeString(sourceMessage));
    }

    @Test(timeout=10000)
    public void testMultipleSMS()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("template", "sms.ftl")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/normal-message-with-attach.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient1@example.com", "recipient2@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state(Mail.DEFAULT)
                .build();

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test 1");
        container.setPasswordID("ID 1");
        passwords.put("recipient1@example.com", container);

        container = new PasswordContainer();
        container.setPassword("test 2");
        container.setPasswordID("ID 2");
        passwords.put("recipient2@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        phoneNumbers.put("recipient1@example.com", new PhoneNumber("1234567890"));
        phoneNumbers.put("recipient2@example.com", new PhoneNumber("0987654321"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        // wait some time for the SMS messages to be transported
        while (smsTransport.getPhoneNumbers().size() < 2) {
            ThreadUtils.sleepQuietly(100);
        }

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '1234567890'"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '0987654321'"));

        assertEquals(2, smsTransport.getPhoneNumbers().size());
        assertTrue(smsTransport.getPhoneNumbers().contains("1234567890"));
        assertTrue(smsTransport.getPhoneNumbers().contains("0987654321"));

        assertThat(smsTransport.getMessages(), hasItems(
                "A message from test@example.com was sent to you by email. " +
                "The pdf password is test 1 the message id is ID 1",
                "A message from test@example.com was sent to you by email. " +
                "The pdf password is test 2 the message id is ID 2"));
    }

    @Test(timeout=10000)
    public void testCaseInsensitive()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("template", "sms.ftl")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/normal-message-with-attach.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("RECIPIENT@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state(Mail.DEFAULT)
                .build();

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("ID");
        passwords.put("recipient@EXAMPLE.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        phoneNumbers.put("recipient@example.COM", new PhoneNumber("1234567890"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        // wait some time for the SMS messages to be transported
        while (smsTransport.getPhoneNumbers().size() < 1) {
            ThreadUtils.sleepQuietly(100);
        }

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '1234567890'"));

        assertEquals(1, smsTransport.getPhoneNumbers().size());
        assertTrue(smsTransport.getPhoneNumbers().contains("1234567890"));

        assertThat(smsTransport.getMessages(), hasItems(
                "A message from test@example.com was sent to you by email. " +
                "The pdf password is test the message id is ID"));
    }

    @Test(timeout=10000)
    public void testSendSMSNonMatchingRecipient()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("template", "sms.ftl")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/normal-message-with-attach.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient1@example.com", "recipient2@example.com", "recipient3@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state(Mail.DEFAULT)
                .build();

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test 1");
        container.setPasswordID("ID 1");
        passwords.put("recipient1@example.com", container);

        container = new PasswordContainer();
        container.setPassword("test 2");
        container.setPasswordID("ID 2");
        passwords.put("recipient2@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        phoneNumbers.put("recipient1@example.com", new PhoneNumber("1234567890"));
        phoneNumbers.put("recipient2@example.com", new PhoneNumber("0987654321"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        // wait some time for the SMS messages to be transported
        while (smsTransport.getPhoneNumbers().size() < 2) {
            ThreadUtils.sleepQuietly(100);
        }

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Password for recipient recipient3@example.com is missing."));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '1234567890'"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '0987654321'"));

        assertEquals(2, smsTransport.getPhoneNumbers().size());
        assertTrue(smsTransport.getPhoneNumbers().contains("1234567890"));
        assertTrue(smsTransport.getPhoneNumbers().contains("0987654321"));

        assertThat(smsTransport.getMessages(), hasItems(
                "A message from test@example.com was sent to you by email. " +
                "The pdf password is test 1 the message id is ID 1",
                "A message from test@example.com was sent to you by email. " +
                "The pdf password is test 2 the message id is ID 2"));
    }

    @Test(timeout=10000)
    public void testPasswordPhoneNumberMismatch()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("template", "sms.ftl")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/normal-message-with-attach.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("RECIPIENT@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state(Mail.DEFAULT)
                .build();

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("ID");
        passwords.put("recipient@EXAMPLE.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        phoneNumbers.put("recipient@example.COM", new PhoneNumber("1234567890"));
        phoneNumbers.put("other@example.COM", new PhoneNumber("32323232323"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        // wait some time for the SMS messages to be transported
        ThreadUtils.sleepQuietly(100);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Passwords and PhoneNumbers do not match up"));

        assertEquals(0, smsTransport.getPhoneNumbers().size());
    }
}
