/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.dlp;

import com.ciphermail.core.common.dlp.MatchFilter;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.dlp.impl.PolicyPatternImpl;
import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PolicyPatternManagerImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private NamedBlobManager namedBlobManager;

    @Autowired
    private PolicyPatternManager policyPatternManager;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status -> namedBlobManager.deleteAll());
    }

    private PolicyPatternNode createPattern(String name) {
        return transactionOperations.execute(status -> policyPatternManager.createPattern(name));
    }

    private PolicyPatternNode getPattern(String name) {
        return transactionOperations.execute(status -> policyPatternManager.getPattern(name));
    }

    private void deletePattern(String name) {
        transactionOperations.executeWithoutResult(status -> policyPatternManager.deletePattern(name));
    }

    private void assertChildSize(int size, String name)
    {
        assertEquals(size, transactionOperations.execute(status -> policyPatternManager.
                getPattern(name).getChilds().size()).intValue());
    }

    private void addChild(String parent, String child)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            PolicyPatternNode node = policyPatternManager.getPattern(parent);

            PolicyPatternNode childNode = policyPatternManager.getPattern(child);

            node.getChilds().add(childNode);
        });
    }

    private void addChildren()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            PolicyPatternNode node1 = policyPatternManager.createPattern("node1");
            PolicyPatternNode node2 = policyPatternManager.createPattern("node2");
            PolicyPatternNode node3 = policyPatternManager.createPattern("node3");
            PolicyPatternNode node4 = policyPatternManager.createPattern("node4");

            PolicyPatternImpl pattern = new PolicyPatternImpl("node2", Pattern.compile(".*"));
            pattern.setMatchFilterName("Mask");

            node2.setPolicyPattern(pattern);
            node3.setPolicyPattern(new PolicyPatternImpl("node3", Pattern.compile(".*")));
            node4.setPolicyPattern(new PolicyPatternImpl("node4", Pattern.compile(".*")));

            Set<PolicyPatternNode> childs = node1.getChilds();
            childs.add(node2);
            childs.add(node3);

            node1.getChilds().add(node4);
        });
    }

    private boolean containsChild(String name, String childName)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(status ->
        {
            PolicyPatternNode node = policyPatternManager.getPattern(name);

            assertNotNull(node);

            PolicyPatternNode childNode = policyPatternManager.getPattern(childName);

            assertNotNull(childNode);

            Set<PolicyPatternNode> childs = node.getChilds();

            return childs.contains(childNode);
        }));
    }

    private void setPolicyPattern(String name, PolicyPattern pattern)
    {
        transactionOperations.executeWithoutResult(status -> policyPatternManager.
                getPattern(name).setPolicyPattern(pattern));
    }

    private PolicyPattern getPolicyPattern(String name) {
        return transactionOperations.execute(status -> policyPatternManager.getPattern(name).getPolicyPattern());
    }

    private MatchFilter getMatchFilter(String name) {
        return transactionOperations.execute(status -> policyPatternManager.getPattern(name).getPolicyPattern().
                getMatchFilter());
    }

    private boolean isInUse(String name) {
        return Boolean.TRUE.equals(transactionOperations.execute(status -> policyPatternManager.isInUse(name)));
    }

    private List<String> getInUseInfo(String name) {
        return transactionOperations.execute(status -> policyPatternManager.getInUseInfo(name));
    }

    @Test
    public void testCreateNew()
    {
        assertNull(getPattern("test"));

        PolicyPatternNode node = createPattern("test");

        assertNotNull(node);

        assertEquals("test", node.getName());
        assertNull(node.getPolicyPattern());
        assertEquals(0, node.getChilds().size());

        node = getPattern("test");

        assertNotNull(node);

        assertEquals("test", node.getName());
        assertNull(node.getPolicyPattern());
        assertChildSize(0, "test");
    }

    @Test
    public void testAddChildren()
    {
        assertNotNull(createPattern("other"));
        addChildren();
        assertChildSize(3, "node1");

        assertTrue(containsChild("node1", "node2"));
        assertTrue(containsChild("node1", "node3"));
        assertTrue(containsChild("node1", "node4"));
        assertFalse(containsChild("node1", "other"));

        transactionOperations.executeWithoutResult(status ->
        {
            policyPatternManager.getPattern("node1").getChilds().remove(
                    policyPatternManager.getPattern("node3"));
        });

        assertChildSize(2, "node1");

        assertTrue(containsChild("node1", "node2"));
        assertTrue(containsChild("node1", "node4"));
    }

    @Test
    public void testSetPattern()
    {
        assertNotNull(createPattern("test"));

        PolicyPatternImpl pattern = new PolicyPatternImpl("name", Pattern.compile(".*"));

        pattern.setDescription("description");
        pattern.setThreshold(12);

        setPolicyPattern("test", pattern);

        PolicyPattern stored = getPolicyPattern("test");
        assertEquals("name", stored.getName());
        assertEquals(".*", stored.getPattern().pattern());
        assertEquals("description", stored.getDescription());
        assertEquals(12, stored.getThreshold());
    }

    @Test
    public void testDelete()
    {
        assertNull(getPattern("test"));

        assertNotNull(createPattern("test"));

        assertNotNull(getPattern("test"));

        deletePattern("test");

        assertNull(getPattern("test"));
    }

    @Test
    public void testCreateExisting()
    {
        assertNotNull(createPattern("test"));

        DataIntegrityViolationException e = assertThrows(DataIntegrityViolationException.class,
                () -> createPattern("test"));
    }

    @Test
    public void testInUseGroupPattern()
    {
        createPattern("parent1");
        createPattern("parent2");
        createPattern("child1");
        createPattern("child2");
        createPattern("child3");

        addChild("parent1", "child1");
        addChild("parent1", "child2");

        addChild("parent2", "child1");
        addChild("parent1", "parent2");

        assertTrue(isInUse("child1"));
        assertTrue(isInUse("child2"));
        assertTrue(isInUse("parent2"));
        assertFalse(isInUse("child3"));
        assertFalse(isInUse("parent1"));

        assertEquals(getInUseInfo("child1").size(), 2);
        assertThat(getInUseInfo("child1"), hasItems("pattern,group,parent1", "pattern,group,parent2"));
        assertEquals("[pattern,group,parent1]",
                getInUseInfo("child2").toString());
        assertEquals("[pattern,group,parent1]",
                getInUseInfo("parent2").toString());
        assertEquals("[]",
                getInUseInfo("child3").toString());
        assertEquals("[]",
                getInUseInfo("parent1").toString());
    }

    @Test
    public void testGetPatterns()
    {
        createPattern("p1");
        createPattern("p2");
        createPattern("p4");
        createPattern("p3");

        transactionOperations.executeWithoutResult(status ->
        {
            List<PolicyPatternNode> patterns = policyPatternManager.getPatterns(null, null);

            assertEquals(4, patterns.size());
            assertEquals("p1", patterns.get(0).getName());
            assertEquals("p2", patterns.get(1).getName());
            assertEquals("p3", patterns.get(2).getName());
            assertEquals("p4", patterns.get(3).getName());
        });
    }
}
