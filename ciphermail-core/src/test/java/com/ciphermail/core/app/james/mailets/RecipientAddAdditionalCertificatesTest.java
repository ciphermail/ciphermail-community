/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.NamedCertificateCategory;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.impl.NamedCertificateUtils;
import com.ciphermail.core.app.james.Certificates;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class RecipientAddAdditionalCertificatesTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void before()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                domainManager.deleteAllDomains();

                // Clear additional certs for global settings
                UserPreferences preferences = globalPreferencesManager.getGlobalUserPreferences();

                NamedCertificateUtils.replaceNamedCertificates(preferences.getNamedCertificates(),
                        NamedCertificateCategory.ADDITIONAL.name(), null);

                // Deletes certs and keys
                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void importCertificates(X509CertStoreExt certStore, File certificateFile)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);

                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate) {
                        certStore.addCertificate((X509Certificate) certificate);
                    }
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setAdditionalCertificatesForUser(String email, Collection<X509Certificate> certificates)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                NamedCertificateUtils.replaceNamedCertificates(user.getUserPreferences().getNamedCertificates(),
                        NamedCertificateCategory.ADDITIONAL.name(), certificates);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setAdditionalCertificatesForDomain(String domain, Collection<X509Certificate> certificates)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences domainPrefs = domainManager.getDomainPreferences(domain);

                if (domainPrefs == null) {
                    domainPrefs = domainManager.addDomain(domain);
                }

                NamedCertificateUtils.replaceNamedCertificates(domainPrefs.getNamedCertificates(),
                        NamedCertificateCategory.ADDITIONAL.name(), certificates);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new RecipientAddAdditionalCertificates();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testAddCertificate()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("validateCertificates", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("sender@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("teST1@EXAMple.com", "test2@example.com", "test3@example.com", "test4@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        Certificates certificates = new Certificates(new HashSet<>(
                CertificateUtils.readX509Certificates(
                        new File(TestUtils.getTestDataDir(),"certificates/testcertificate.cer"))));

        assertEquals(1, certificates.getCertificates().size());

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Collection<X509Certificate> additionalCerts = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/multipleemail.cer"));

        assertEquals(1, additionalCerts.size());

        setAdditionalCertificatesForUser("test1@example.com", additionalCerts);

        additionalCerts = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/rim.cer"));

        assertEquals(1, additionalCerts.size());

        setAdditionalCertificatesForUser("test2@example.com", additionalCerts);
        setAdditionalCertificatesForUser("test3@example.com", additionalCerts);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        certificates = CoreApplicationMailAttributes.getCertificates(mail);

        assertEquals(3, certificates.getCertificates().size());
    }

    @Test
    public void testAddCertificateDomain()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("validateCertificates", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("sender@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("teST@EXAMple.com", "teST@other.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        Certificates certificates = new Certificates(new HashSet<>(
                CertificateUtils.readX509Certificates(
                        new File(TestUtils.getTestDataDir(), "certificates/testcertificate.cer"))));

        assertEquals(1, certificates.getCertificates().size());

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Collection<X509Certificate> additionalCerts = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/multipleemail.cer"));

        assertEquals(1, additionalCerts.size());

        setAdditionalCertificatesForDomain("*.other.com", additionalCerts);

        additionalCerts = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/rim.cer"));

        assertEquals(1, additionalCerts.size());

        setAdditionalCertificatesForUser("test@example.com", additionalCerts);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        certificates = CoreApplicationMailAttributes.getCertificates(mail);

        assertEquals(3, certificates.getCertificates().size());
    }

    @Test
    public void testValidateCertificate()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("sender@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test1@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        Collection<X509Certificate> additionalCerts = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/valid_certificate_mitm_test_ca.cer"));

        assertEquals(1, additionalCerts.size());

        setAdditionalCertificatesForUser("test1@example.com", additionalCerts);

        Certificates certificates = new Certificates(new HashSet<>(
                CertificateUtils.readX509Certificates(
                        new File(TestUtils.getTestDataDir(), "certificates/multipleemail.cer"))));

        assertEquals(1, certificates.getCertificates().size());

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        certificates = CoreApplicationMailAttributes.getCertificates(mail);

        // the additional cert is not trusted
        assertEquals(1, certificates.getCertificates().size());

        // import chain to make cert trusted and try again
        importCertificates(rootStore, new File(
                TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"));
        importCertificates(keyAndCertStore, new File(
                TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        certificates = CoreApplicationMailAttributes.getCertificates(mail);

        // the additional cert is not trusted
        assertEquals(2, certificates.getCertificates().size());
    }

    @Test
    public void testRetry()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("sender@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test1@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        RecipientAddAdditionalCertificates mailet = new RecipientAddAdditionalCertificates()
        {
            @Override
            protected void onHandleUserEvent(@Nonnull User user)
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                super.onHandleUserEvent(user);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        Collection<X509Certificate> additionalCerts = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/valid_certificate_mitm_test_ca.cer"));

        assertEquals(1, additionalCerts.size());

        setAdditionalCertificatesForUser("test1@example.com", additionalCerts);

        Certificates certificates = new Certificates(new HashSet<>(
                CertificateUtils.readX509Certificates(new File(
                        TestUtils.getTestDataDir(), "certificates/multipleemail.cer"))));

        assertEquals(1, certificates.getCertificates().size());

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        // import chain to make cert trusted and try again
        importCertificates(rootStore, new File(
                TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"));
        importCertificates(keyAndCertStore, new File(
                TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"));

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertTrue(constraintViolationThrown.booleanValue());

        certificates = CoreApplicationMailAttributes.getCertificates(mail);

        // the additional cert is not trusted
        assertEquals(2, certificates.getCertificates().size());
    }
}
