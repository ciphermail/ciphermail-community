/*
 * Copyright (c) 2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.password.validator;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.impl.MockupUserWorkflow;
import com.ciphermail.core.app.properties.PortalProperties;
import com.ciphermail.core.app.properties.PortalPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.passay.PasswordData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserPropertyNOutOfMRuleTest
{
    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    private PasswordData createPasswordData(String email, String password)
    {
        PasswordData passwordData = new PasswordData(password);

        passwordData.setUsername(email);

        return passwordData;
    }

    private PortalProperties createPortalProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class).createInstance(
                properties);
    }

    @Test
    public void testUser()
    throws Exception
    {
        MockupUserWorkflow workflow = new MockupUserWorkflow();

        User user = workflow.addUser("test@example.com");

        workflow.makePersistent(user);

        createPortalProperties(user.getUserPreferences().getProperties())
                .setPasswordPolicy("{r:[{p:'^.{8,}$'}]}");

        UserPropertyNOutOfMRule rule = new UserPropertyNOutOfMRule(workflow, "portal-password-policy");

        assertTrue(rule.validate(createPasswordData("test@example.com", "12345678")).isValid());
        assertFalse(rule.validate(createPasswordData("test@example.com", "1234567")).isValid());

        createPortalProperties(user.getUserPreferences().getProperties())
                .setPasswordPolicy("{r:[{p:'^.{7,}$'}]}");
        assertTrue(rule.validate(createPasswordData("test@example.com", "12345678")).isValid());
        assertTrue(rule.validate(createPasswordData("test@example.com", "1234567")).isValid());
    }

    @Test
    public void testUnknownUser()
    {
        MockupUserWorkflow workflow = new MockupUserWorkflow();

        UserPropertyNOutOfMRule rule = new UserPropertyNOutOfMRule(workflow, "portal-password-policy");

        assertFalse(rule.validate(createPasswordData("test@example.com", "123456789")).isValid());
    }

    @Test
    public void testInvalidEmailAddress()
    throws Exception
    {
        MockupUserWorkflow workflow = new MockupUserWorkflow();

        User user = workflow.addUser("test@example.com");

        workflow.makePersistent(user);

        createPortalProperties(user.getUserPreferences().getProperties())
                .setPasswordPolicy("{r:[{p:'^.{8,}$'}]}");

        UserPropertyNOutOfMRule rule = new UserPropertyNOutOfMRule(workflow, "portal-password-policy");

        assertTrue(rule.validate(createPasswordData("test@example.com", "12345678")).isValid());
        assertFalse(rule.validate(createPasswordData("test", "12345678")).isValid());
    }
}
