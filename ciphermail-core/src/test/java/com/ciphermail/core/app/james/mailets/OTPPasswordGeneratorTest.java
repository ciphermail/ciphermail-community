/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.OTPPasswordContainer;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.HMACSecretKeyDeriverImpl;
import com.ciphermail.core.common.security.otp.HMACOTPGenerator;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class OTPPasswordGeneratorTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    {
        deleteAllUsers();

        addUser("test1@example.com");
        setUserProperty("test1@example.com", "client-secret", "secret1");

        addUser("test2@example.com");
        setUserProperty("test2@example.com", "client-secret", "mbofjehkghgzt4u4c4l6yd3yae");
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void deleteAllUsers()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setUserProperty(String email, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                userProperties.setProperty(propertyName, value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addUser(String email)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                userWorkflow.makePersistent(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST));
            }
            catch (AddressException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new OTPPasswordGenerator();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testUserProperties()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("teST1@EXAMple.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .state(Mail.DEFAULT)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(Mail.DEFAULT, mail.getState());

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);

        OTPPasswordContainer passwordContainer = (OTPPasswordContainer) passwords.get("test1@example.com");

        assertNotNull(passwordContainer);
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());
        assertNotNull(passwordContainer.getPasswordLength());
        assertEquals(16, (int) passwordContainer.getPasswordLength());
        assertEquals("mzotxni", passwordContainer.getClientSecretID());

        // check if a new password is generated when it's handled again
        mailet.service(mail);

        Passwords newPasswords = CoreApplicationMailAttributes.getPasswords(mail);

        OTPPasswordContainer newPasswordContainer = (OTPPasswordContainer) newPasswords.get("test1@example.com");

        assertNotEquals(newPasswordContainer.getPassword(), passwordContainer.getPassword());
        assertEquals(newPasswordContainer.getClientSecretID(), passwordContainer.getClientSecretID());
    }

    @Test
    public void testUserPropertiesNonDefaultPasswordLength()
    throws Exception
    {
        setUserProperty("test1@example.com", "pdf-password-length", "100");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);

        OTPPasswordContainer passwordContainer = (OTPPasswordContainer) passwords.get("test1@example.com");

        assertNotNull(passwordContainer);
        assertNotNull(passwordContainer.getPassword());
        assertNotNull(passwordContainer.getPasswordID());
        assertNotNull(passwordContainer.getPasswordLength());
        assertEquals(100, (int) passwordContainer.getPasswordLength());
    }

    @Test
    public void testMissingSecret()
    throws Exception
    {
        setUserProperty("nosecret@example.com", "client-secret", "");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("nosecret@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        MailUtils.validateMessage(mail.getMessage());

        assertNull(CoreApplicationMailAttributes.getPasswords(mail));

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "No OTP secret key found for user nosecret@example.com. Retrying (retry count: 1)"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "No OTP secret key found for user nosecret@example.com"));
    }

    /*
     * Deterministic test to see whether the password is generated consistently
     */
    @Test
    public void testPassword()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test2@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        OTPPasswordGenerator mailet = new OTPPasswordGenerator()
        {
            // Override generatePasswordID so we have a deterministic test, i.e., the generated password
            // will always be the same
            @Override
            protected synchronized String generatePasswordID() {
                return "123";
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);

        assertEquals(1, passwords.size());

        OTPPasswordContainer passwordContainer = (OTPPasswordContainer) passwords.get("test2@example.com");

        assertNotNull(passwordContainer);

        // check if the generated password is the expected value
        String expected = new HMACOTPGenerator("HmacSHA256").generate(
                new HMACSecretKeyDeriverImpl().deriveKey("mbofjehkghgzt4u4c4l6yd3yae",
                                "USER:test2@example.com", "otp").getBytes(StandardCharsets.UTF_8),
                "123".getBytes(StandardCharsets.UTF_8), 16);

        assertEquals("m2g24nxmsox7itnpoco5b5rvz4", passwordContainer.getPassword());
        assertEquals(expected, passwordContainer.getPassword());
        assertEquals("123", passwordContainer.getPasswordID());
        assertNotNull(passwordContainer.getPasswordLength());
        assertEquals(16, (int) passwordContainer.getPasswordLength());
        assertEquals("fx5ekrq", passwordContainer.getClientSecretID());
    }

    /**
     * Tests whether generating a retry is executed when a ConstraintViolationException occurs
     */
    @Test
    public void testRetry()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test2@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .state("some state")
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        OTPPasswordGenerator mailet = new OTPPasswordGenerator()
        {
            // Override generatePasswordID so we have a deterministic test, i.e., the generated password
            // will always be the same
            @Override
            protected synchronized String generatePasswordID() {
                return "123";
            }

            // Overwrite generatePassword so we can force a ConstraintViolationException
            @Override
            protected void generatePassword(@Nonnull User user, @Nonnull PasswordContainer passwordContainer)
            throws MessagingException
            {
                super.generatePassword(user, passwordContainer);

                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    ActivationContext passwordActivationContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                            ActivationContext.class);

                    PasswordContainer newPasswordContainer = new PasswordContainer();
                    newPasswordContainer.setPassword("dummy");
                    newPasswordContainer.setPasswordID("dummy");

                    passwordActivationContext.getPasswords().put("dummy@example.com", newPasswordContainer);

                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        mailet.service(mail);

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("some state", mail.getState());
        assertTrue(constraintViolationThrown.booleanValue());

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 1)"));

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNotNull(passwords);

        assertEquals(1, passwords.size());

        OTPPasswordContainer passwordContainer = (OTPPasswordContainer) passwords.get("test2@example.com");

        assertNotNull(passwordContainer);
        assertEquals("m2g24nxmsox7itnpoco5b5rvz4", passwordContainer.getPassword());
        assertEquals("123", passwordContainer.getPasswordID());
        assertNotNull(passwordContainer.getPasswordLength());
        assertEquals(16, (int) passwordContainer.getPasswordLength());
    }

    @Test
    public void testPasswordRetryExhaustRetries()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        Mail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test2@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .state("some state")
                .build();

        OTPPasswordGenerator mailet = new OTPPasswordGenerator()
        {
            // Overwrite generatePassword so we can force a ConstraintViolationException
            @Override
            protected void generatePassword(@Nonnull User user, @Nonnull PasswordContainer passwordContainer)
            {
                throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                        "fake constraint");
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        mailet.service(mail);

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("some state", mail.getState());

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 1)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 2)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 3)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "org.hibernate.exception.ConstraintViolationException: Forced exception"));

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        assertNull(passwords);
    }
}
