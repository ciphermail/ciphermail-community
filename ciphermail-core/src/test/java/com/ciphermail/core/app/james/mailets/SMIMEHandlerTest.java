/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.SecurityInfoTags;
import com.ciphermail.core.app.properties.SMIMEProperties;
import com.ciphermail.core.app.properties.SMIMEPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.notification.NotificationListener;
import com.ciphermail.core.common.notification.NotificationMessage;
import com.ciphermail.core.common.notification.NotificationService;
import com.ciphermail.core.common.notification.NotificationSeverity;
import com.ciphermail.core.common.notification.StandardNotificationFacilities;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.SecurityConstants;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.ctl.CTL;
import com.ciphermail.core.common.security.ctl.CTLDAO;
import com.ciphermail.core.common.security.ctl.CTLEntry;
import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import com.ciphermail.core.common.security.ctl.CTLException;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.core.common.security.keystore.MockBasicKeyStore;
import com.ciphermail.core.common.security.smime.SMIMEInspector;
import com.ciphermail.core.common.security.smime.SMIMEInspectorImpl;
import com.ciphermail.core.common.security.smime.SMIMESecurityInfoHeader;
import com.ciphermail.core.common.security.smime.SMIMEType;
import com.ciphermail.core.common.util.FileConstants;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SMIMEHandlerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private CTLManager ctlManager;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private AbstractApplicationContext applicationContext;

    private int tempFileCount;

    private NotificationListenerImpl notificationListener = new NotificationListenerImpl();

    static class NotificationListenerImpl implements NotificationListener
    {
        private List<String> facilities;
        private List<NotificationSeverity> severities;
        private List<NotificationMessage> messages;

        NotificationListenerImpl() {
            reset();
        }

        private void reset()
        {
            facilities = new LinkedList<>();
            severities = new LinkedList<>();
            messages = new LinkedList<>();
        }

        @Override
        public String getName() {
            return "dummy NotificationListener";
        }

        @Override
        public void notificationEvent(String facility, NotificationSeverity severity, NotificationMessage message)
        {
            facilities.add(facility);
            severities.add(severity);
            messages.add(message);
        }
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                domainManager.deleteAllDomains();

                // Clean key/root store
                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();

                // Clean all CTLs
                CTLDAO.deleteAllEntries(SessionAdapterFactory.create(sessionManager.getSession()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        notificationListener = new NotificationListenerImpl();

        notificationService.registerNotificationListener(notificationListener);

        // get the current nr of temp files
        tempFileCount = TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp");

        importKeyStore(keyAndCertificateWorkflow, new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12"),"test");

        importCertificates(rootStore, new File(TestUtils.getTestDataDir(),
                "certificates/mitm-test-root.cer"));
    }

    @After
    public void after()
    {
        // check if we have any temp file leakage
        assertEquals(tempFileCount, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile, String password)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
                keyStore.load(new FileInputStream(pfxFile), password.toCharArray());

                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                        MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void importCertificates(X509CertStoreExt certStore, File certificateFile)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);

                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate) {
                        certStore.addCertificate((X509Certificate) certificate);
                    }
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private long getKeyAndCertStoreSize() {
        return transactionOperations.execute(status -> keyAndCertStore.size()).longValue();
    }

    private void addUserCertificate(String email, X509Certificate certificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                user.getUserPreferences().getCertificates().add(certificate);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addDomainCertificate(String domain, X509Certificate certificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences domainPrefs = domainManager.getDomainPreferences(domain);

                if (domainPrefs == null) {
                    domainPrefs = domainManager.addDomain(domain);
                }

                domainPrefs.getCertificates().add(certificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addToCTL(String thumbprint, CTLEntryStatus ctlStatus, boolean allowExpired)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL(CTLManager.DEFAULT_CTL);

                CTLEntry ctlEntry = ctl.createEntry(thumbprint);

                ctlEntry.setStatus(ctlStatus);
                ctlEntry.setAllowExpired(allowExpired);

                ctl.addEntry(ctlEntry);
            }
            catch (CTLException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setAutoImportCertificatesFromMail(String email, boolean value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                createSMIMEProperties(user.getUserPreferences().getProperties())
                        .setAutoImportCertificatesFromMail(value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setAutoImportCertificatesSkipUntrusted(String email, boolean value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                createSMIMEProperties(user.getUserPreferences().getProperties())
                        .setAutoImportCertificatesSkipUntrusted(value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void clearCertStore(X509CertStoreExt store)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                store.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void removeCertificate(X509CertStoreExt store, X509Certificate certificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                store.removeCertificate(certificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * Because the order of Mail's received can be different (because of HashSet's HashMap's etc.) we will find the
     * correct Mail based on the recipients
     */
    private static FakeMailContext.SentMail getSentMailWithRecipients(FakeMailContext mailContext, String... recipents)
    throws Exception
    {
        Collection<MailAddress> addresses = new LinkedList<>();

        for (String recipient : recipents) {
            addresses.add(new MailAddress(recipient));
        }

        for (FakeMailContext.SentMail sentMail : mailContext.getSentMails())
        {
            if (sentMail.getRecipients().size() == addresses.size() && sentMail.getRecipients().containsAll(addresses)) {
                return sentMail;
            }
        }

        return null;
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SMIMEHandler();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    private SMIMEProperties createSMIMEProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMIMEPropertiesImpl.class)
                .createInstance(properties);
    }

    @Test
    public void testDecompress()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("threshold", "0")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-body-compressed.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.COMPRESSED, inspector.getSMIMEType());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void testNoDecompress()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("threshold", "0")
                .setProperty("decompress", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-body-compressed.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.COMPRESSED, inspector.getSMIMEType());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, mailContext.getSentMails().size());

        inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.COMPRESSED, inspector.getSMIMEType());

        assertEquals("initial", mail.getState());
    }

    @Test
    public void testDefaultSettings()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        assertEquals("<602167900.17.1194204767299.JavaMail.martijn@ubuntu>", sourceMessage.getMessageID());

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(22, getKeyAndCertStoreSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("<602167900.17.1194204767299.JavaMail.martijn@ubuntu>", sentMail.getMsg().getMessageID());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // 3 certs should have been added
        assertEquals(25, getKeyAndCertStoreSize());
    }

    @Test
    public void testDoNotRetainMessageId()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("retainMessageID", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        assertEquals("<602167900.17.1194204767299.JavaMail.martijn@ubuntu>", sourceMessage.getMessageID());

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertNotEquals("<602167900.17.1194204767299.JavaMail.martijn@ubuntu>",
                sentMail.getMsg().getMessageID());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void testRemoveSignature()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("removeSignature", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals(sourceMessage.getMessageID(), sentMail.getMsg().getMessageID());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void testRemoveSignatureMailAttribute()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("removeSignatureAttribute", "attr.removeSignature")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mail.setAttribute(Attribute.convertToAttribute("attr.removeSignature", true));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals(sourceMessage.getMessageID(), sentMail.getMsg().getMessageID());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void testNoDecrypt()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("decrypt", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, mailContext.getSentMails().size());

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals(sourceMessage.getMessageID(), mail.getMessage().getMessageID());

        assertEquals("initial", mail.getState());
    }

    /*
     * Checks for missing handledProcessor
     */
    @Test
    public void testMissingHandledProcessor()
    {
        NullPointerException e = assertThrows(NullPointerException.class, () ->
            createMailet(FakeMailetConfig.builder()
                    .mailetContext(FakeMailContext.defaultContext())
                    .build()));

        assertEquals("handledProcessor parameter is missing", e.getMessage());
    }

    @Test
    public void testDecryptOL2010WithMissingSKIInSubjectWorkaroundEnabled()
    throws Exception
    {
        boolean skiWorkaroundEnabled = SecurityConstants.isOutlook2010SKIWorkaroundEnabled();

        try {
            SecurityConstants.setOutlook2010SKIWorkaroundEnabled(true);

            FakeMailContext mailContext = FakeMailContext.defaultContext();

            FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                    .mailetContext(mailContext)
                    .setProperty("handledProcessor", "handled")
                    .build();

            MimeMessage sourceMessage = TestUtils.loadTestMessage(
                    "mail/outlook2010_cert_missing_subjkeyid.eml");

            FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                    .recipients("m.brinkers@pobox.com")
                    .sender("test@example.com")
                    .mimeMessage(sourceMessage)
                    .build();

            importKeyStore(keyAndCertificateWorkflow, new File(TestUtils.getTestDataDir(),
                    "keys/outlook2010_cert_missing_subjkeyid.p12"), "");

            Mailet mailet = createMailet(mailetConfig);

            mailet.service(mail);

            checkLogsForErrorsOrExceptions();

            assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));

            assertEquals(1, mailContext.getSentMails().size());

            FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

            SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                    SecurityFactoryBouncyCastle.PROVIDER_NAME);

            assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

            assertEquals("handled", sentMail.getState());
            assertEquals(Mail.GHOST, mail.getState());
        }
        finally {
            SecurityConstants.setOutlook2010SKIWorkaroundEnabled(skiWorkaroundEnabled);
        }
    }

    @Test
    public void testStrictMultipleRecipients()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("threshold", "10")
                .setProperty("strict", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/encrypt-15-recipients.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com", "test2@example.com", "test3@example.com", "test4@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted. MailID: null; Recipients: [test@example.com]"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted. MailID: null; Recipients: [test2@example.com]"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted. MailID: null; Recipients: [test3@example.com]"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME decryption key not found"));

        // two new emails should be created, one decrypted, and one still encrypted for test4@example.com
        assertEquals(2, mailContext.getSentMails().size());

        // this email should have been decrypted and sent to test, test2 and test3
        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals(sourceMessage.getMessageID(), sentMail.getMsg().getMessageID());

        assertEquals("handled", sentMail.getState());
        assertEquals(3, sentMail.getRecipients().size());

        assertThat(sentMail.getRecipients(), allOf(
                hasItem(new MailAddress("test@example.com")),
                hasItem(new MailAddress("test2@example.com")),
                hasItem(new MailAddress("test3@example.com"))));

        // this email should not have been decrypted because for test4 there was not decryption key
        sentMail = mailContext.getSentMails().get(1);

        inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(1, sentMail.getRecipients().size());

        assertThat(sentMail.getRecipients(), hasItem(new MailAddress("test4@example.com")));

        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void testStrictSignedCorrupt()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("threshold", "10")
                .setProperty("strict", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-opaque-corrupt.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com", "test2@example.com", "test3@example.com", "test4@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Error checking for S/MIME for user test@example.com"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Error checking for S/MIME for user test2@example.com"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Error checking for S/MIME for user test3@example.com"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Error checking for S/MIME for user test4@example.com"));

        assertEquals(0, mailContext.getSentMails().size());

        assertEquals(4, mail.getRecipients().size());

        assertThat(mail.getRecipients(), allOf(
                hasItem(new MailAddress("test@example.com")),
                hasItem(new MailAddress("test2@example.com")),
                hasItem(new MailAddress("test3@example.com")),
                hasItem(new MailAddress("test4@example.com"))));

        assertEquals("initial", mail.getState());
    }

    @Test
    public void testStrictMailAttribute()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("threshold", "10")
                .setProperty("strictAttribute", "attr.strict")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/encrypt-15-recipients.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com", "test2@example.com", "test3@example.com", "test4@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mail.setAttribute(Attribute.convertToAttribute("attr.strict", "true"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted. MailID: null; Recipients: [test@example.com]"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted. MailID: null; Recipients: [test2@example.com]"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted. MailID: null; Recipients: [test3@example.com]"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME decryption key not found"));

        // two new emails should be created, one decrypted, and one still encrypted for test4@example.com
        assertEquals(2, mailContext.getSentMails().size());

        // this email should have been decrypted and sent to test, test2 and test3
        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals(sourceMessage.getMessageID(), sentMail.getMsg().getMessageID());

        assertEquals("handled", sentMail.getState());
        assertEquals(3, sentMail.getRecipients().size());

        assertThat(sentMail.getRecipients(), allOf(
                hasItem(new MailAddress("test@example.com")),
                hasItem(new MailAddress("test2@example.com")),
                hasItem(new MailAddress("test3@example.com"))));

        // this email should not have been decrypted because for test4 there was not decryption key
        sentMail = mailContext.getSentMails().get(1);

        inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(1, sentMail.getRecipients().size());

        assertThat(sentMail.getRecipients(), hasItem(new MailAddress("test4@example.com")));

        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void test3Layers()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/3-layer-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com", "test2@example.com", "test3@example.com", "test4@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mail.setAttribute(Attribute.convertToAttribute("attr.strict", "true"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted. MailID: null; Recipients: " +
                "[test@example.com, test2@example.com, test3@example.com, test4@example.com]"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals(sourceMessage.getMessageID(), sentMail.getMsg().getMessageID());

        assertEquals("handled", sentMail.getState());
        assertEquals(4, sentMail.getRecipients().size());

        assertThat(sentMail.getRecipients(), allOf(
                hasItem(new MailAddress("test@example.com")),
                hasItem(new MailAddress("test2@example.com")),
                hasItem(new MailAddress("test3@example.com")),
                hasItem(new MailAddress("test4@example.com"))));

        sentMail = mailContext.getSentMails().get(0);

        inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(4, sentMail.getRecipients().size());

        assertThat(sentMail.getRecipients(), allOf(
                hasItem(new MailAddress("test@example.com")),
                hasItem(new MailAddress("test2@example.com")),
                hasItem(new MailAddress("test3@example.com")),
                hasItem(new MailAddress("test4@example.com"))));

        assertEquals("1.2.840.113549.3.2", sentMail.getMsg().getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("DES_EDE3_CBC, Key size: 168", sentMail.getMsg().getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-1", ","));
        assertEquals("AES128_CBC, Key size: 128", sentMail.getMsg().getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-2", ","));
        assertEquals("True", sentMail.getMsg().getHeader(
                "X-CipherMail-Info-SMIME-Encrypted", ","));

        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void test3LayersStrict()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("threshold", "10")
                .setProperty("strict", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/3-layer-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com", "test2@example.com", "test3@example.com", "test4@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(3, mailContext.getSentMails().size());

        //
        // message 1
        //
        // Because the order of Mail's received can be different (because of HashSet's HashMap's etc.) we will find the
        // correct Mail based on the recipients
        FakeMailContext.SentMail newMail = getSentMailWithRecipients(mailContext, "test@example.com");

        MimeMessage newMessage = newMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the last layer could not be decrypted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        // Because of strict mode, the decryption key could not be found
        assertEquals("True", newMessage.getHeader(
                "X-CipherMail-Info-SMIME-Decryption-Key-Not-Found", ","));

        // Only test@example.com can decrypt the first two layers
        assertEquals(1, newMail.getRecipients().size());
        assertTrue(newMail.getRecipients().contains(new MailAddress("test@example.com")));
        assertEquals("handled", newMail.getState());

        assertEquals("1.2.840.113549.3.2", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("DES_EDE3_CBC, Key size: 168", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-1", ","));
        assertEquals("AES128_CBC, Key size: 128", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-2", ","));

        //
        // message 2
        //
        newMail = getSentMailWithRecipients(mailContext,"test2@example.com", "test3@example.com");

        newMessage = newMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the last layer could not be decrypted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        // Because of strict mode, the decryption key could not be found
        assertEquals("True", newMessage.getHeader(
                "X-CipherMail-Info-SMIME-Decryption-Key-Not-Found", ","));

        // test2@example.com and test3@example.com can only decrypt the first layers
        assertEquals(2, newMail.getRecipients().size());
        assertTrue(newMail.getRecipients().contains(new MailAddress("test2@example.com")));
        assertTrue(newMail.getRecipients().contains(new MailAddress("test3@example.com")));
        assertEquals("handled", newMail.getState());

        assertEquals("1.2.840.113549.3.2", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("DES_EDE3_CBC, Key size: 168", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-1", ","));
        assertNull(newMessage.getHeader("X-CipherMail-Info-Encryption-Algorithm-2"));

        //
        // message 3
        //
        newMail = getSentMailWithRecipients(mailContext,"test4@example.com");

        newMessage = newMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the last message could not be decrypted because of strict mode
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        // Because of strict mode, the decryption key could not be found
        assertEquals("True", newMessage.getHeader(
                "X-CipherMail-Info-SMIME-Decryption-Key-Not-Found", ","));

        assertEquals(1, newMail.getRecipients().size());
        assertTrue(newMail.getRecipients().contains(new MailAddress("test4@example.com")));
        assertEquals("handled", newMail.getState());
    }

    @Test
    public void test3LayersStrictWithExplicitCertificate()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("threshold", "10")
                .setProperty("strict", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/3-layer-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com", "test2@example.com", "test3@example.com", "test4@example.com",
                        "sub@subdomain.example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        keyStore.load(new FileInputStream(new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12")), "test".toCharArray());

        // Make sure sub@subdomain.example.com can decrypt all
        addUserCertificate("sub@subdomain.example.com",
                (X509Certificate) keyStore.getCertificate("NoEmail"));
        addUserCertificate("sub@subdomain.example.com",
                (X509Certificate) keyStore.getCertificate("ValidCertificate"));
        addUserCertificate("sub@subdomain.example.com",
                (X509Certificate) keyStore.getCertificate("multipleEmail"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(4, mailContext.getSentMails().size());

        //
        // message 1
        //
        // Because the order of Mail's received can be different (because of HashSet's HashMap's etc.) we will find the
        // correct Mail based on the recipients
        FakeMailContext.SentMail newMail = getSentMailWithRecipients(mailContext, "test@example.com");

        MimeMessage newMessage = newMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the last layer could not be decrypted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        // Because of strict mode, the decryption key could not be found
        assertEquals("True", newMessage.getHeader(
                "X-CipherMail-Info-SMIME-Decryption-Key-Not-Found", ","));

        // Only test@example.com can decrypt the first two layers
        assertEquals(1, newMail.getRecipients().size());
        assertTrue(newMail.getRecipients().contains(new MailAddress("test@example.com")));
        assertEquals("handled", newMail.getState());

        assertEquals("1.2.840.113549.3.2", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("DES_EDE3_CBC, Key size: 168", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-1", ","));
        assertEquals("AES128_CBC, Key size: 128", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-2", ","));

        //
        // message 2
        //
        newMail = getSentMailWithRecipients(mailContext, "test2@example.com", "test3@example.com");

        newMessage = newMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the last layer could not be decrypted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        // Because of strict mode, the decryption key could not be found
        assertEquals("True", newMessage.getHeader(
                "X-CipherMail-Info-SMIME-Decryption-Key-Not-Found", ","));

        // test2@example.com and test3@example.com can only decrypt the first layers
        assertEquals(2, newMail.getRecipients().size());
        assertTrue(newMail.getRecipients().contains(new MailAddress("test2@example.com")));
        assertTrue(newMail.getRecipients().contains(new MailAddress("test3@example.com")));
        assertEquals("handled", newMail.getState());

        assertEquals("1.2.840.113549.3.2", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("DES_EDE3_CBC, Key size: 168", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-1", ","));
        assertNull(newMessage.getHeader("X-CipherMail-Info-Encryption-Algorithm-2"));

        //
        // message 3
        //
        newMail = getSentMailWithRecipients(mailContext, "test4@example.com");

        newMessage = newMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the last message could not be decrypted because of strict mode
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        // Because of strict mode, the decryption key could not be found
        assertEquals("True", newMessage.getHeader(
                "X-CipherMail-Info-SMIME-Decryption-Key-Not-Found", ","));

        assertEquals(1, newMail.getRecipients().size());
        assertTrue(newMail.getRecipients().contains(new MailAddress("test4@example.com")));
        assertEquals("handled", newMail.getState());

        //
        // message 4
        //
        newMail = getSentMailWithRecipients(mailContext,"sub@subdomain.example.com");

        newMessage = newMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the last layer was also decrypted
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));


        // sub@subdomain.example.com can decrypt all layers
        assertEquals(1, newMail.getRecipients().size());
        assertTrue(newMail.getRecipients().contains(new MailAddress("sub@subdomain.example.com")));
        assertEquals("handled", newMail.getState());

        assertEquals("1.2.840.113549.3.2", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("DES_EDE3_CBC, Key size: 168", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-1", ","));
        assertEquals("AES128_CBC, Key size: 128", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-2", ","));
    }

    @Test
    public void test3LayersStrictDomainCerts()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("threshold", "10")
                .setProperty("strict", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/3-layer-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@sub.example.com", "test2@sub.example.com", "test3@sub.example.com",
                        "test4@sub.example.com", "test5@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        keyStore.load(new FileInputStream(new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12")), "test".toCharArray());

        // Make sure sub@subdomain.example.com can decrypt all
        addDomainCertificate("sub.example.com",
                (X509Certificate) keyStore.getCertificate("NoEmail"));
        addDomainCertificate("sub.example.com",
                (X509Certificate) keyStore.getCertificate("ValidCertificate"));
        addDomainCertificate("sub.example.com",
                (X509Certificate) keyStore.getCertificate("multipleEmail"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, mailContext.getSentMails().size());

        //
        // message 1
        //
        // Because the order of Mail's received can be different (because of HashSet's HashMap's etc.) we will find the
        // correct Mail based on the recipients
        FakeMailContext.SentMail newMail = getSentMailWithRecipients(mailContext, "test5@example.com");

        MimeMessage newMessage = newMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals("1.2.840.113549.3.2", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));

        assertEquals(1, newMail.getRecipients().size());
        assertTrue(newMail.getRecipients().contains(new MailAddress("test5@example.com")));

        //
        // message 2
        //
        newMail = getSentMailWithRecipients(mailContext, "test@sub.example.com", "test2@sub.example.com",
                "test3@sub.example.com",
                "test4@sub.example.com");

        newMessage = newMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals(4, newMail.getRecipients().size());

        assertThat(newMail.getRecipients(), allOf(
                hasItem(new MailAddress("test@sub.example.com")),
                hasItem(new MailAddress("test2@sub.example.com")),
                hasItem(new MailAddress("test3@sub.example.com")),
                hasItem(new MailAddress("test4@sub.example.com"))));

        assertEquals("handled", newMail.getState());

        assertEquals("1.2.840.113549.3.2", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("DES_EDE3_CBC, Key size: 168", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-1", ","));
        assertEquals("AES128_CBC, Key size: 128", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-2", ","));
    }

    @Test
    public void test3LayersStrictBlacklistedCert()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("threshold", "10")
                .setProperty("strict", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/3-layer-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com", "test2@example.com", "test3@example.com", "test4@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        keyStore.load(new FileInputStream(new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12")), "test".toCharArray());

        // Blacklist the ValidCertificate
        addToCTL(X509CertificateInspector.getThumbprint(keyStore.getCertificate("ValidCertificate")),
                CTLEntryStatus.BLACKLISTED, false);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        // message to test4@example.com could not be decrypted because it was black-listed and strict mode is enabled
        assertEquals(2, mailContext.getSentMails().size());

        //
        // message 1
        //
        // Because the order of Mail's received can be different (because of HashSet's HashMap's etc.) we will find the
        // correct Mail based on the recipients
        FakeMailContext.SentMail newMail = getSentMailWithRecipients(mailContext, "test@example.com",
                "test2@example.com", "test3@example.com");

        MimeMessage newMessage = newMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals("1.2.840.113549.3.2", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));

        // test@example.com, test2@example.com and test3@example.com can decrypt the first two layers
        assertEquals(3, newMail.getRecipients().size());
        assertThat(newMail.getRecipients(), allOf(
                hasItem(new MailAddress("test@example.com")),
                hasItem(new MailAddress("test2@example.com")),
                hasItem(new MailAddress("test3@example.com"))));

        //
        // message 2
        //
        newMail = getSentMailWithRecipients(mailContext, "test4@example.com");

        newMessage = newMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // not decrypted by test4@example.com because of strict mode
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals(1, newMail.getRecipients().size());
        assertTrue(newMail.getRecipients().contains(new MailAddress("test4@example.com")));

        assertEquals("1.2.840.113549.3.2", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));

        // test4@example.com is not allowed to decrypt the message
        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void testNoDecryptionKey()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/encrypted-no-decryption-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "A suitable decryption key could not be found. CMS Recipients: " +
                "CN=UTN-USERFirst-Client Authentication and Email, OU=http://www.usertrust.com, " +
                "O=The USERTRUST Network, L=Salt Lake City, ST=UT, " +
                "C=US/D7B834E01BB40A3EDA40A897F3787BAE//1.2.840.113549.1.1.1"));

        // The message is always handled if it's an S/MIME message because the security info is added
        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail newMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = newMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the message could not be decrypted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        // Security information should be added
        assertEquals("DES_EDE3_CBC, Key size: 168", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));

        assertEquals("handled", newMail.getState());
        assertEquals(Mail.GHOST, mail.getState());
    }

    /*
     * Test for bug:
     *
     * In strict mode, S/MIME signed messages are only handled by the S/MIME handler if the recipient has a private key.
     * If a digitally signed message is received and the recipient does not have a private key and is using strict mode
     * (not the default), then the message is not handled by the S/MIME handler and send as-is. The certificate is
     * therefore not extracted and if "remove digital signature" is enabled, the signature is not removed.
     */
    @Test
    public void testSignedRecipientNoKeyStrict()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("strict", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(22, getKeyAndCertStoreSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was valid"));

        // Signing cert should have been added
        assertEquals(25, getKeyAndCertStoreSize());

        // The message is always handled if it's an S/MIME message because the security info is added
        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail newMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = newMail.getMsg();

        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the message is still signed
        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", newMail.getState());
        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void testStaticNoImportCertificates()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("importCertificates", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(22, getKeyAndCertStoreSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // No certs should have been added
        assertEquals(22, getKeyAndCertStoreSize());
    }

    @Test
    public void testAddSecurityInfoToSubject()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("recipient@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[%s decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
        assertEquals("normal message with attachment [%s decrypted] [signed]", newMessage.getSubject());
    }

    @Test
    public void testAddSecurityInfoToSubject3Layers()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/3-layer-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("test simple message [decrypted] [decrypted] [decrypted]", newMessage.getSubject());
    }

    @Test
    public void testAddSecurityInfoToSubject3NoDecryptionKey()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/encrypted-no-decryption-key.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals("RE: Test ", sourceMessage.getSubject());

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME decryption key not found"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the message could not be decrypted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // Security information should be added
        assertEquals("DES_EDE3_CBC, Key size: 168", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));

        assertEquals("RE: Test ", newMessage.getSubject());

        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND, ","));
    }

    @Test
    public void testAddSecurityInfoToSubjectClearSigned()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-body-clear-signed.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("test simple message [signed]", newMessage.getSubject());
    }

    @Test
    public void testAddSecurityInfoToSubjectTampered()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-tampered.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Signature could not be verified. Message: " +
                "org.bouncycastle.cms.CMSSignerDigestMismatchException: message-digest attribute value does not " +
                "match calculated value"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was not valid; Signer IDs: CN=christine " +
                "intermediate/121536A24CCCA5EC01785DAE8830910/"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("ik ook wipjam? [invalid]", newMessage.getSubject());
    }

    @Test
    public void testAddSecurityInfoToSubjectInvalid()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-sign-missing-smime-ext-key-usage.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was not valid; Signer IDs: EMAILADDRESS=ca@example.com, " +
                "CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/115FD035BA042503BCC6CA44680F9F8"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("test simple message [invalid]", newMessage.getSubject());
    }

    @Test
    public void testAddSecurityInfoToSubjectSubjectTemplate()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("subjectTemplate", "%2$s ABC %1$s")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("[signed] ABC [decrypted] ABC normal message with attachment", newMessage.getSubject());
    }

    @Test
    public void testAddSecurityInfoToSubjectSignerMismatch()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-body-clear-signed.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("test@example.com", newMessage.getHeader("X-CipherMail-Info-Signer-Email-0-0", ","));
        assertEquals("test simple message [signed by: test@example.com]", newMessage.getSubject());
    }

    @Test
    public void testDisableAutoImportCertificateFromMailSetting()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(22, getKeyAndCertStoreSize());

        setAutoImportCertificatesFromMail("test@example.com", false);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // No certs should have been added
        assertEquals(22, getKeyAndCertStoreSize());
    }

    @Test
    public void testEnableImportUntrustedCertificates()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // Remove roots to see whether import of certs from email works for untrusted certs
        clearCertStore(rootStore);

        assertEquals(22, getKeyAndCertStoreSize());

        setAutoImportCertificatesFromMail("test@example.com", true);
        setAutoImportCertificatesSkipUntrusted("test@example.com", false);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was not valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // 3 certs should have been added even though the certs were not trusted
        assertEquals(25, getKeyAndCertStoreSize());
    }

    @Test
    public void testDisableImportUntrustedCertificates()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // Remove roots to see whether import of certs from email works for untrusted certs
        clearCertStore(rootStore);

        assertEquals(22, getKeyAndCertStoreSize());

        setAutoImportCertificatesFromMail("test@example.com", true);
        setAutoImportCertificatesSkipUntrusted("test@example.com", true);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was not valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // no new certs should have been added
        assertEquals(22, getKeyAndCertStoreSize());
    }

    /*
     * Test whether the intermediate from the email will immediately be used for validation even though the
     * certificate is not yet in the store
     */
    @Test
    public void testImportCertificatesIntermediateNotYetImported()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // Remove intermediate to see whether import of certs from email works even if the intermediate is not
        // available in the store (i.e., the certs from the email should be used for validation without adding them
        // first)
        removeCertificate(keyAndCertStore, TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/mitm-test-ca.cer")));

        assertEquals(21, getKeyAndCertStoreSize());

        setAutoImportCertificatesFromMail("test@example.com", true);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // new certs should have been added
        assertEquals(24, getKeyAndCertStoreSize());
    }

    /*
     * Test whether the intermediate from the CMS blob will immediately be used for validation even though the
     * certificate is not imported and import is disabled.
     */
    @Test
    public void testIntermediateNotInStoreImportDisabled()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // Remove intermediate to see whether import of certs from email works even if the intermediate is not
        // available in the store (i.e., the certs from the email should be used for validation without adding them)
        removeCertificate(keyAndCertStore, TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/mitm-test-ca.cer")));

        assertEquals(21, getKeyAndCertStoreSize());

        setAutoImportCertificatesFromMail("test@example.com", false);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // no new certs should have been added
        assertEquals(21, getKeyAndCertStoreSize());
    }

    /*
     * Test whether the intermediate from the CMS blob will not be used for validation because the limit is set to 1
     */
    @Test
    public void testMaxCMScertificates()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("maxCMSCertificates", "1")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // Remove intermediate to see whether import of certs from email works even if the intermediate is not
        // available in the store (i.e., the certs from the email should be used for validation without adding them)
        removeCertificate(keyAndCertStore, TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/mitm-test-ca.cer")));

        assertEquals(21, getKeyAndCertStoreSize());

        setAutoImportCertificatesFromMail("test@example.com", false);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Max number 1 of additional certificates reached"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was not valid; Signer IDs: EMAILADDRESS=ca@example.com, CN=MITM Test CA, " +
                "L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // no new certs should have been added
        assertEquals(21, getKeyAndCertStoreSize());
    }

    /*
     * Test whether the intermediate from the CMS blob will not be used for validation because the limit is set to 1
     */
    @Test
    public void testMaxCMScertificatesStrictMode()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("strict", "true")
                .setProperty("maxCMSCertificates", "1")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // Remove intermediate to see whether import of certs from email works even if the intermediate is not
        // available in the store (i.e., the certs from the email should be used for validation without adding them)
        removeCertificate(keyAndCertStore, TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/mitm-test-ca.cer")));

        assertEquals(21, getKeyAndCertStoreSize());

        setAutoImportCertificatesFromMail("test@example.com", false);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Max number 1 of additional certificates reached"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was not valid; Signer IDs: EMAILADDRESS=ca@example.com, CN=MITM Test CA, " +
                "L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // no new certs should have been added
        assertEquals(21, getKeyAndCertStoreSize());
    }

    /*
     * Test whether the intermediate from the CMS blob will not be used for validation because the limit is set to 1
     */
    @Test
    public void testDisableAddingCMScertificates()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("addCMSCertificates", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // Remove intermediate to see whether import of certs from email works even if the intermediate is not
        // available in the store (i.e., the certs from the email should be used for validation without adding them)
        removeCertificate(keyAndCertStore, TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/mitm-test-ca.cer")));

        assertEquals(21, getKeyAndCertStoreSize());

        setAutoImportCertificatesFromMail("test@example.com", false);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was not valid; Signer IDs: EMAILADDRESS=ca@example.com, CN=MITM Test CA, " +
                "L=Amsterdam, ST=NH, C=NL/115FCD741088707366E9727452C9770"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // no new certs should have been added
        assertEquals(21, getKeyAndCertStoreSize());
    }

    @Test
    public void testImportNewCertificatesNotification()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(22, getKeyAndCertStoreSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // new certs should have been added
        assertEquals(25, getKeyAndCertStoreSize());

        assertEquals(3, notificationListener.messages.size());

        assertEquals(StandardNotificationFacilities.CERTIFICATE_IMPORT, notificationListener.facilities.get(0));
        assertEquals(NotificationSeverity.INFORMATIONAL, notificationListener.severities.get(0));
        assertEquals("A certificate with subject EMAILADDRESS=test@example.com, CN=Valid certificate, L=Amsterdam, " +
        		"ST=NH, C=NL and serial number 115FCD741088707366E9727452C9770 was imported.",
                notificationListener.messages.get(0).getMessage());

        assertEquals(StandardNotificationFacilities.CERTIFICATE_IMPORT, notificationListener.facilities.get(1));
        assertEquals(NotificationSeverity.INFORMATIONAL, notificationListener.severities.get(1));
        assertEquals("A certificate with subject EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
        		"C=NL and serial number 115FCAD6B536FD8D49E72922CD1F0DA was imported.",
                notificationListener.messages.get(1).getMessage());

        assertEquals(StandardNotificationFacilities.CERTIFICATE_IMPORT, notificationListener.facilities.get(2));
        assertEquals(NotificationSeverity.INFORMATIONAL, notificationListener.severities.get(2));
        assertEquals("A certificate with subject EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, " +
        		"C=NL and serial number 115FCAC409FB2022B7D06920A00FE42 was imported.",
                notificationListener.messages.get(2).getMessage());

        notificationListener.reset();

        // Same email again
        mailet.service(mail);

        // No certs should have been imported
        assertEquals(0, notificationListener.messages.size());
    }

    @Test
    public void testImportNewCertificatesNotificationDisabled()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("sendImportNotification", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(22, getKeyAndCertStoreSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // new certs should have been added
        assertEquals(25, getKeyAndCertStoreSize());

        assertEquals(0, notificationListener.messages.size());
    }

    @Test
    public void testDecryptSubjectKeyId()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/encrypted-subjectkeyid-recipientid-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals(22, getKeyAndCertStoreSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertTrue(newMessage.isMimeType("multipart/mixed"));

        assertEquals("DES_EDE3_CBC, Key size: 168", newMessage.getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("//9C82DE52944798337B05036A187DED41632EFBFF/1.2.840.113549.1.1.1",
                newMessage.getHeader("X-CipherMail-Info-Encryption-Recipient-0-0", ","));
    }

    @Test
    public void testClearSignedCertWithInvalidEmailAddresses()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/clear-signed-cert-invalid-email-addresses.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("signed with cert containing invalid email addresses [signed]", newMessage.getSubject());

        assertEquals("test@example.com,aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                     "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                     "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@example.com",
                newMessage.getHeader("X-CipherMail-Info-Signer-Email-0-0", ","));
    }

    @Test
    public void testClearSignedAndEncryptedCertWithInvalidEmailAddresses()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/encrypted-cert-invalid-email-addresses.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[decrypted]", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("signed and encrypted with cert containing invalid email addresses [decrypted] [signed]",
                newMessage.getSubject());

        assertEquals("test@example.com,aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                     "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                     "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@example.com",
                newMessage.getHeader("X-CipherMail-Info-Signer-Email-0-1", ","));
    }

    @Test
    public void testNoSMIMEMessage()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/test-5K.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, mailContext.getSentMails().size());

        assertEquals("initial", mail.getState());
    }

    @Test
    public void testEFAILHeaderDetect8bit()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("checkInvalid7BitChars", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/efail-SMIME-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Illegal characters detected in decrypted message"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // because the message headers were tampered, it is no longer signed
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));
    }

    @Test
    public void testEFAILHeaderDetect8bitAttribute()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("checkInvalid7BitCharsAttribute", "test.checkInvalid7BitChars")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/efail-SMIME-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // because the message headers were tampered, it is no longer signed
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND));

        // set attribute true and resent
        mail.setAttribute(Attribute.convertToAttribute("test.checkInvalid7BitChars", true));

        memoryLogAppender.reset();
        mailContext.resetSentMails();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Illegal characters detected in decrypted message"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        sentMail = mailContext.getSentMails().get(0);

        newMessage = sentMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // because the message headers were tampered, it is no longer signed
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));

        // set attribute false and resent
        mail.setAttribute(Attribute.convertToAttribute("test.checkInvalid7BitChars", false));

        memoryLogAppender.reset();
        mailContext.resetSentMails();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        sentMail = mailContext.getSentMails().get(0);

        newMessage = sentMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // because the message headers were tampered, it is no longer signed
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND));
    }

    @Test
    public void testEFAILBodyDetect8bit()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("checkInvalid7BitChars", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/efail-SMIME-encrypted-body-changed.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Illegal characters detected in decrypted message"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // because the message headers were tampered, it is no longer signed
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));

        String body = (String) newMessage.getContent();

        // The following text was inserted into the encrypted content
        assertTrue(body.contains("<img ignore=\""));
    }

    @Test
    public void testEFAILHeaderDetect8bitAbortDecryption()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("checkInvalid7BitChars", "true")
                .setProperty("abortDecryptionOnInvalid7BitChars", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/efail-SMIME-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Illegal characters detected in decrypted message"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Decryption aborted. Illegal characters detected in decrypted message"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // decryption should have been be aborted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));
    }

    @Test
    public void testEFAILHeaderDetect8bitAbortDecryptionAttribute()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("checkInvalid7BitChars", "true")
                .setProperty("abortDecryptionOnInvalid7BitCharsAttribute",
                        "test.abortDecryptionOnInvalid7BitCharsAttribute")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/efail-SMIME-encrypted.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        mail.setAttribute(Attribute.convertToAttribute(
                "test.abortDecryptionOnInvalid7BitCharsAttribute", true));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Illegal characters detected in decrypted message"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Decryption aborted. Illegal characters detected in decrypted message"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // decryption should have been be aborted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));
    }

    @Test
    public void testEFAILHeaderDetect8bitAttached()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("checkInvalid7BitChars", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/efail-SMIME-encrypted-body-changed-attached.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Illegal characters detected in decrypted message"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // because the S/MIME message is attached, the outer mail is not S/MIME
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));

        // now check the attached message
        MimeMessage attachedMessage = BodyPartUtils.searchForRFC822(newMessage);

        assertNotNull(attachedMessage);

        inspector = new SMIMEInspectorImpl(attachedMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the attached mail should be decrypted but not signed because it was corrupted because of injection
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertNull(attachedMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", attachedMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));
    }

    @Test
    public void testEFAILHeaderDetect8bitAttachedAbortDecryption()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("checkInvalid7BitChars", "true")
                .setProperty("abortDecryptionOnInvalid7BitChars", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/efail-SMIME-encrypted-body-changed-attached.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Illegal characters detected in decrypted message"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Decryption aborted. Illegal characters detected in decrypted message"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // because the S/MIME message is attached, the outer mail is not S/MIME
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));

        // now check the attached message
        MimeMessage attachedMessage = BodyPartUtils.searchForRFC822(newMessage);

        assertNotNull(attachedMessage);

        inspector = new SMIMEInspectorImpl(attachedMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // decryption should have been be aborted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertNull(attachedMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", attachedMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));
    }

    @Test
    public void testEFAILHeaderDetect8bitAttachedStrict()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("checkInvalid7BitChars", "true")
                .setProperty("strict", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/efail-SMIME-encrypted-body-changed-attached.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com", "other@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Illegal characters detected in decrypted message"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME message has been decrypted"));

        // there should be two emails, one decrypted and one not decrypted because of strict mode and mismatch of
        // email address
        assertEquals(2, mailContext.getSentMails().size());

        //
        // message 1
        //
        // Because the order of Mail's received can be different (because of HashSet's HashMap's etc.) we will find the
        // correct Mail based on the recipients
        FakeMailContext.SentMail sentMail = getSentMailWithRecipients(mailContext, "test@example.com");

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // because the S/MIME message is attached, the outer mail is not S/MIME
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertNull(newMessage.getHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));

        // now check the attached message
        MimeMessage attachedMessage = BodyPartUtils.searchForRFC822(newMessage);

        assertNotNull(attachedMessage);

        inspector = new SMIMEInspectorImpl(attachedMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // the attached mail should be decrypted but not signed because it was corrupted because of injection
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertNull(attachedMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND));
        assertEquals("True", attachedMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, ","));

        //
        // message 2
        //
        // Because the order of Mail's received can be different (because of HashSet's HashMap's etc.) we will find the
        // correct Mail based on the recipients
        sentMail = getSentMailWithRecipients(mailContext, "other@example.com");

        newMessage = sentMail.getMsg();

        inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // Because the S/MIME message is attached, the outer mail is not S/MIME
        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
        assertEquals("handled", sentMail.getState());

        assertEquals("True", newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND, ","));
        // Because email was not decrypted, no illegal chars were detected from decrypted message
        assertNull(newMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND));

        // Now check the attached message to see whether that is still encrypted
        attachedMessage = BodyPartUtils.searchForRFC822(newMessage);

        inspector = new SMIMEInspectorImpl(attachedMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // The attached mail should not be decrypted
        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        assertEquals("True", attachedMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND, ","));
         // Because email was not decrypted, no illegal chars were detected from decrypted message
        assertNull(attachedMessage.getHeader(
                SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND));
    }

    @Test
    public void testAddSecurityInfoToSubjectSkipTag()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("recipient@example.com")
                .mimeMessage(sourceMessage)
                .build();

        //
        // test skip decrytion tag
        //

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "<skip>", "[signed]", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
        assertEquals("normal message with attachment [signed]", newMessage.getSubject());

        //
        // test skip signed tag
        //
        mailContext.resetSentMails();
        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[encrypted]", "<skip>", "[signed by: %s]",
                "[invalid]", "[mixed content]"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        sentMail = mailContext.getSentMails().get(0);

        newMessage = sentMail.getMsg();

        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
        assertEquals("normal message with attachment [encrypted]", newMessage.getSubject());

        //
        // test skip signed by tag
        //
        mailContext.resetSentMails();
        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[encrypted]", "[signed]", "<skip>",
                "[invalid]", "[mixed content]"));

        sourceMessage.setFrom(new InternetAddress("other@example.com"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME signature was valid"));

        assertEquals(1, mailContext.getSentMails().size());

        sentMail = mailContext.getSentMails().get(0);

        newMessage = sentMail.getMsg();

        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
        assertEquals("normal message with attachment [encrypted]", newMessage.getSubject());
    }

    @Test
    public void testInvalidSignedByTag()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("recipient@example.com")
                .mimeMessage(sourceMessage)
                .build();

        sourceMessage.setFrom(new InternetAddress("other@example.com"));

        CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                "[encrypted]", "[signed]", "[signed by: %xx]",
                "[invalid]", "[mixed content]"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("Invalid format string"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage newMessage = sentMail.getMsg();

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
        assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
        assertEquals("normal message with attachment [encrypted] <invalid format string>",
                newMessage.getSubject());
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/signed-encrypt-validcertificate.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("recipient@example.com")
                .mimeMessage(sourceMessage)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        SMIMEHandler mailet = new SMIMEHandler()
        {
            @Override
            protected Messages handleMessageTransactedNonStrict(Mail mail)
            throws MessagingException
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                return super.handleMessageTransactedNonStrict(mail);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        assertEquals(22, getKeyAndCertStoreSize());

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was valid"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "S/MIME signature was valid"));

        assertTrue(constraintViolationThrown.booleanValue());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("<602167900.17.1194204767299.JavaMail.martijn@ubuntu>", sentMail.getMsg().getMessageID());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());

        // 3 certs should have been added
        assertEquals(25, getKeyAndCertStoreSize());
    }

    @Test
    public void testAES256_GCM_AOEP_512()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/smime-aes256-gcm-oaep-sha512.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        String header = HeaderUtils.decodeHeaderValue(sentMail.getMsg().getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("AES256_GCM, Key size: 256", header);

        header = HeaderUtils.decodeHeaderValue(sentMail.getMsg().getHeader("X-CipherMail-Info-Encryption-Recipient-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                     "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters", header);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void testAES256_GCM_signedRemoveSignature()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("handledProcessor", "handled")
                .setProperty("removeSignature", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/smime-encrypted-gcm-256-signed.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("S/MIME message has been decrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        String header = HeaderUtils.decodeHeaderValue(sentMail.getMsg().getHeader(
                "X-CipherMail-Info-Encryption-Algorithm-0", ","));
        assertEquals("AES256_GCM, Key size: 256", header);

        header = HeaderUtils.decodeHeaderValue(sentMail.getMsg().getHeader("X-CipherMail-Info-Encryption-Recipient-0-0", ","));
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                     "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

        SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("This is a test\r\n", sentMail.getMsg().getContent());

        assertEquals("handled", sentMail.getState());
        assertEquals(Mail.GHOST, mail.getState());
    }

    @Test
    public void testMissingKey()
    throws Exception
    {
        boolean skiWorkaroundEnabled = SecurityConstants.isOutlook2010SKIWorkaroundEnabled();

        try {
            SecurityConstants.setOutlook2010SKIWorkaroundEnabled(true);

            FakeMailContext mailContext = FakeMailContext.defaultContext();

            FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                    .mailetContext(mailContext)
                    .setProperty("handledProcessor", "handled")
                    .build();

            MimeMessage sourceMessage = TestUtils.loadTestMessage(
                    "mail/outlook2010_cert_missing_subjkeyid.eml");

            FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                    .recipients("m.brinkers@pobox.com")
                    .sender("test@example.com")
                    .mimeMessage(sourceMessage)
                    .build();

            Mailet mailet = createMailet(mailetConfig);

            mailet.service(mail);

            checkLogsForErrorsOrExceptions();

            assertThat(memoryLogAppender.getLogOutput(), containsString(
                    "S/MIME decryption key not found; MailID: null; " +
                    "Message: A suitable decryption key could not be found. " +
                    "CMS Recipients: //2219E504D5750B37D20CC930B14129E1A2E583B1/1.2.840.113549.1.1.1"));

            assertEquals(1, mailContext.getSentMails().size());

            FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

            SMIMEInspector inspector = new SMIMEInspectorImpl(sentMail.getMsg(), new MockBasicKeyStore(),
                    SecurityFactoryBouncyCastle.PROVIDER_NAME);

            assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

            assertEquals("handled", sentMail.getState());
            assertEquals(Mail.GHOST, mail.getState());
        }
        finally {
            SecurityConstants.setOutlook2010SKIWorkaroundEnabled(skiWorkaroundEnabled);
        }
    }
}
