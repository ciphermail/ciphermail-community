/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang.mutable.MutableObject;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SenderPropertyDKIMVerifyTest
{
    private static final String KEY_PAIR_PEM =
            """
            -----BEGIN RSA PRIVATE KEY-----
            MIICXgIBAAKBgQCrUoJ16657mK99O8WYddgMFJPmXDIO49SaJzOp1/sohNd1T6Xh
            WWZVGHEfioxz+HmUFLnekyw51NE64O9cpaiVoZTga9fn5WjtmQNglvcbnhWedeUZ
            PgTkuhbUHgZNtkwvtKyRnTlh7ZxthF6W33q2Bj1CETuS2WdRs/4AA4CNCwIDAQAB
            AoGAaOu3ChC0Yu03TDL26FADaCKSEVoVLhlJcr7fXPzwy/fPHAETTdc6XJMDdJWd
            PsjFbHLlAfKP+zriiHSJIuwxObFocxleeTDKil3QlqXMjqFfF4PUP1XIiZREgDlE
            Gq8nqhFgHeUfV8U7kBejvuhF6r4FfWO5fjgLBbh8u0dTZkECQQDaMPdbNV84daV6
            5nkzYUgWBRn0NjRuIsVY+ugyhNgUexmreGFlpcWrDGMQDU2b2xSR2FEAILM/pxag
            NzxrTFu3AkEAyQJryIY/tl3OD1jvAJLJmDQT0kf0AUcbrL4bL0rWABlH4dk2db6Y
            MyGi88HcJNLkoadBHqJWIBiFWXaGvrzBTQJBAJbgYUt6vpuGDqXLlWfID1batDXA
            /cRi2uBKsCGu5tRSw09k8MSfOu6qpB3HdTEe7zxivrA97HVJj0W+rFLt/EUCQQCy
            2w6gzKOgV3NkwJNZhUMPxTbl4tRA1s7PNBDoUcR9LgGB+k61EjRHOuTN1G9X7Lc3
            B6Wv5m6P/IGbCxX2Xen5AkEAs/mUhmBH3T7Xd1PyeGk6ycIGdu4t+mZc0BhhKfN8
            z3KElieQ+Jy4cXrI5cdPJZwdxDdD0BQMsGnMVNSxz9j8zg==
            -----END RSA PRIVATE KEY-----
            """;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup() {
        setGlobalProperty("dkim-key-pair", KEY_PAIR_PEM);
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void setGlobalProperty(String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences preferences = globalPreferencesManager.getGlobalUserPreferences();

                UserProperties properties = preferences.getProperties();

                properties.setProperty(propertyName, value);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SenderPropertyDKIMVerify();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testRelaxedRelaxedTextMessage()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("resultAttribute", "ciphermail.dkim.result")
                .setProperty("keyProperty", "dkim-key-pair")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/dkim-relaxed-relaxed-text-plain.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals("verified", MailAttributesUtils.getManagedAttributeValue(mail,
                "ciphermail.dkim.result", String.class).get());
    }

    @Test
    public void testRelaxedRelaxedWrongKey()
    throws Exception
    {
        String otherKey =
                """
                -----BEGIN RSA PRIVATE KEY-----
                MIICXAIBAAKBgQDMyeGqS+rYHbGv31jeaN5jDdTgfeMqeCVLIfaOWZ96CIPu0w5j
                Qup4wf5Cdhz4uRmLv+YRFzovyoYbsvCDKojkGRtIDPoihwwzSzF0MbGtpTN/AP/i
                M1pNv2SflqEsXYdrTrUY+SR/RY3qgdCkD+B5Jc1vWcFQ/neImUPiWvgF+QIDAQAB
                AoGAG7KycvYRrWlWvxLWUj6c1YjpYfOk5fmaEa9mSZqVl0vPjF2/dG14iRyz5wlS
                odFnQx+RXn5lwFEEEnsBTLxUB0fo4HGE8jsOK6UKUxZsNNlOqXi2DwfLHxPhGtC6
                A1SztOTPIRtzIyxTHNsHKWsdzIj4+8CIVSMIue9YzfJwEIECQQDq5xjWr27koSq4
                mBNEqx6bOLDWTWqd5NP0vM5Z8rnTyxYMOeO5hE19RQytpSNHPs7bh+7Ogn+Bewzs
                cwI6KzzXAkEA3y5l5qfvtOPomG8X5LpzJ3PLLmhO+0nXKddzYQwiogX9rZIkTsCU
                r5yz1Ppkh7rcAC1WN/NVUZx0gq+u0k0prwJBAOciudgVC0LAKu80BFGfJyCI6cgU
                qQHwNXctiMYNBjiWLn2dQKw8uJq4pL8ALPRfot90o9Bjq97WG2NVzy05ekUCQFyd
                KHQ7JGHwYck/8K5eIQMyLhKn/n6Q+dTHL34KRyTtT4QDrUVw7UKiyI2NCsK4bCs2
                pRy6waEIR+EjfmyO0MkCQD6yCgN3KO/903aRA/axPVMk/bDRvukRsY1685OJAbaI
                fkcrtGKoND9Q/w93+/OQ4YkEMG3ddjWSnS3Dyz1TwJM=
                -----END RSA PRIVATE KEY-----
                """;

        setGlobalProperty("dkim-key-pair", otherKey);

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("resultAttribute", "ciphermail.dkim.result")
                .setProperty("keyProperty", "dkim-key-pair")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/dkim-relaxed-relaxed-text-plain.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "DKIM verification failed"));

        assertEquals("failed", MailAttributesUtils.getManagedAttributeValue(mail,
                "ciphermail.dkim.result", String.class).get());
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("resultAttribute", "ciphermail.dkim.result")
                .setProperty("keyProperty", "dkim-key-pair")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/dkim-relaxed-relaxed-text-plain.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        Mailet mailet = new SenderPropertyDKIMVerify()
        {
            @Override
            protected void onHandleUserEvent(User user, MutableObject pemKeyPairHolder)
            throws MessagingException
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
                super.onHandleUserEvent(user, pemKeyPairHolder);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertEquals("verified", MailAttributesUtils.getManagedAttributeValue(mail,
                "ciphermail.dkim.result", String.class).get());;
    }
}
