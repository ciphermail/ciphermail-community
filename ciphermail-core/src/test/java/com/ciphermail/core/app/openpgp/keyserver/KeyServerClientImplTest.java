/*
 * Copyright (c) 2014-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.openpgp.keyserver;

import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClient;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientSettings;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientSettingsImpl;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServers;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServersImpl;
import com.ciphermail.core.common.security.openpgp.keyserver.KeyServerKeyInfo;
import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPPublicKeyRingCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class KeyServerClientImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private KeyServerClient keyServerClient;

    @Autowired
    private HKPKeyServerClient hkpKeyServerClient;

    /*
     * Need this to restore the original HKPKeyServers in after
     */
    private HKPKeyServers originalHKPKeyServers;

    @Before
    public void setup()
    throws Exception
    {
        originalHKPKeyServers = getKeyServers();

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                HKPKeyServerClientSettings settings = new HKPKeyServerClientSettingsImpl(TestProperties.getPGPKeyserverURL1());

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "martijn@djigzo.com_0x271AD23B.asc")))));

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "0xb91671444a3c41eb.asc")))));

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "0x4ec4e8813e11a9a0.asc")))));

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "0x94cee89f0c345bcc.asc")))));

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "0xd382c0ec9e8ca3cf.asc")))));

                // Use PGP key server 2
                settings = new HKPKeyServerClientSettingsImpl(TestProperties.getPGPKeyserverURL2());

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "martijn@djigzo.com_0x271AD23B.asc")))));

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "0xb91671444a3c41eb.asc")))));

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "0x4ec4e8813e11a9a0.asc")))));

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "0x94cee89f0c345bcc.asc")))));

                hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(TEST_BASE, "0xd382c0ec9e8ca3cf.asc")))));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @After
    public void after() {
        setKeyServers(originalHKPKeyServers);
    }

    private void setKeyServers(HKPKeyServers servers)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyServerClient.setServers(servers);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private HKPKeyServers getKeyServers()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyServerClient.getServers();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private KeyServerClientSearchResult searchKeys(String query, boolean exact, Integer maxKeys,
            boolean skipDuplicateKeys)
    {
        return transactionOperations.execute(status ->
        {
            try {
                keyServerClient.setSkipDuplicateKeys(skipDuplicateKeys);
                return keyServerClient.searchKeys(query, exact, maxKeys);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private PGPPublicKeyRingCollection getKeys(String keyID)
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyServerClient.getKeys(keyID);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private KeyServerClientSubmitResult submitKeys(PGPPublicKeyRingCollection keyRingCollection)
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyServerClient.submitKeys(keyRingCollection);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchKeysSingleServer()
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        setKeyServers(servers);

        KeyServerClientSearchResult result = searchKeys("0xDC368B248911C140EF6564764605970C271AD23B", true,
                null, true);

        assertNotNull(result);
        assertEquals(0, result.getErrors().size());

        List<KeyServerKeyInfo> keyInfos = result.getKeys();

        assertEquals(1, keyInfos.size());

        KeyServerKeyInfo keyInfo = keyInfos.get(0);

        String keyID = keyInfo.getKeyID();

        assertTrue("DC368B248911C140EF6564764605970C271AD23B".equals(keyID) || "271AD23B".equals(keyID));
        assertEquals("RSA", keyInfo.getAlgorithm().toString());
        assertEquals(2048, keyInfo.getKeyLength());
        assertEquals("Thu Oct 03 22:29:58 CEST 2013", keyInfo.getCreationDate().toString());
        assertEquals("Wed Oct 04 17:32:40 CEST 2023", keyInfo.getExpirationDate().toString());
        assertNull(keyInfo.getFlags());
        assertEquals(2, keyInfo.getUserIDs().size());

        // Some key servers return HTML encoded content
        assertEquals("Martijn Brinkers <martijn@djigzo.com>", StringEscapeUtils.unescapeHtml(
                keyInfo.getUserIDs().get(0)));
        assertEquals("Martijn Brinkers <martijn@ciphermail.com>", StringEscapeUtils.unescapeHtml(
                keyInfo.getUserIDs().get(1)));
    }

    @Test
    public void testSearchKeysMultipleServers()
    {
        HKPKeyServers servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL2()));

        setKeyServers(servers);

        // Check if we can read the server back
        HKPKeyServers read = getKeyServers();

        assertEquals(2, read.getClientSettings().size());

        KeyServerClientSearchResult result = searchKeys("0xDC368B248911C140EF6564764605970C271AD23B", true,
                null, true);

        assertNotNull(result);

        assertEquals(0, result.getErrors().size());

        List<KeyServerKeyInfo> keyInfos = result.getKeys();

        assertEquals(1, keyInfos.size());

        String keyID = keyInfos.get(0).getKeyID();

        assertTrue("DC368B248911C140EF6564764605970C271AD23B".equals(keyID) || "271AD23B".equals(keyID));
    }

    @Test
    public void testSearchKeysMaxSet()
    {
        HKPKeyServers servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        setKeyServers(servers);

        KeyServerClientSearchResult result = searchKeys("djigzo.com", false,
                null, true);

        assertNotNull(result);
        assertEquals(4, result.getKeys().size());
        assertEquals(0, result.getErrors().size());

        result = searchKeys("djigzo.com", true, 1, true);

        assertNotNull(result);
        assertEquals(1, result.getKeys().size());
        assertEquals(0, result.getErrors().size());
    }

    @Test
    public void testSearchKeysMultipleServersAddDuplicate()
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL2()));

        setKeyServers(servers);

        KeyServerClientSearchResult result = searchKeys("0xDC368B248911C140EF6564764605970C271AD23B",
                true,null, false /* include duplicates */);

        assertNotNull(result);
        // Note: this sometimes fails. Some servers from the pool can return errors
        assertEquals(0, result.getErrors().size());

        List<KeyServerKeyInfo> keyInfos = result.getKeys();

        // Duplicate results are now included
        // Note: this sometimes fails. Some servers from the pool can return errors
        assertEquals(2, keyInfos.size());

        // Some key servers return short key IDs and some long
        assertTrue("271AD23B".equals(keyInfos.get(0).getKeyID()) ||
                "DC368B248911C140EF6564764605970C271AD23B".equals(keyInfos.get(0).getKeyID()));

        assertTrue("271AD23B".equals(keyInfos.get(1).getKeyID()) ||
                "DC368B248911C140EF6564764605970C271AD23B".equals(keyInfos.get(1).getKeyID()));
    }

    @Test
    public void testSearchKeysError()
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                "http://non.existing.domain.example.com"));

        setKeyServers(servers);

        KeyServerClientSearchResult result = searchKeys("0xDC368B248911C140EF6564764605970C271AD23B", true,
                null,
                true);

        assertNotNull(result);
        assertEquals(0, result.getKeys().size());
        assertEquals(1, result.getErrors().size());

        assertEquals("UnknownHostException: non.existing.domain.example.com (http://non.existing.domain.example.com)",
                result.getErrors().get(0));
    }

    @Test
    public void testSearchKeysErrorMultipleServers()
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl("http://blabla.example.com"));
        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        setKeyServers(servers);

        KeyServerClientSearchResult result = searchKeys("0xDC368B248911C140EF6564764605970C271AD23B", true,
                null, true);

        assertNotNull(result);
        assertEquals(1, result.getKeys().size());
        assertEquals(1, result.getErrors().size());

        assertEquals("UnknownHostException: blabla.example.com: Name or service not known (http://blabla.example.com)",
                result.getErrors().get(0));

        String keyID = result.getKeys().get(0).getKeyID();

        assertTrue("DC368B248911C140EF6564764605970C271AD23B".equals(keyID) || "271AD23B".equals(keyID));
    }

    @Test
    public void testGetKeysSingleServer()
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        setKeyServers(servers);

        PGPPublicKeyRingCollection keyRingCollection = getKeys("0xDC368B248911C140EF6564764605970C271AD23B");

        assertNotNull(keyRingCollection);

        PGPPublicKeyRing keyRing = keyRingCollection.getKeyRings().next();

        assertEquals("271AD23B", PGPUtils.getShortKeyIDHex(keyRing.getPublicKey().getKeyID()));
    }

    @Test
    public void testGetKeysFirstServerError()
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl("http://non.existing.domain.example.com"));
        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        setKeyServers(servers);

        PGPPublicKeyRingCollection keyRingCollection = getKeys("0xDC368B248911C140EF6564764605970C271AD23B");

        assertNotNull(keyRingCollection);

        PGPPublicKeyRing keyRing = keyRingCollection.getKeyRings().next();

        assertEquals("271AD23B", PGPUtils.getShortKeyIDHex(keyRing.getPublicKey().getKeyID()));
    }

    @Test
    public void testGetKeysNoMatch()
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl("http://non.existing.domain.example.com"));
        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        setKeyServers(servers);

        try {
            getKeys("0xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

            fail();
        }
        catch (Exception e)
        {
            String message = e.getMessage();

            // Result depends on which key server is accessed
            assertTrue(StringUtils.containsIgnoreCase(message, "No results found") ||
                    StringUtils.containsIgnoreCase(message, "0 keys found"));
            assertTrue("Found " + message, StringUtils.containsIgnoreCase(message, "404 Not found"));
        }
    }

    @Test
    public void testSubmitKeys()
    throws Exception
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        setKeyServers(servers);

        PGPPublicKeyRingCollection keyRingCollection = new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, "martijn@djigzo.com_0x271AD23B.asc"))));

        KeyServerClientSubmitResult result = submitKeys(keyRingCollection);

        assertEquals(1, result.getURLs().size());
        assertEquals(0, result.getErrors().size());

        assertEquals(TestProperties.getPGPKeyserverURL1(), result.getURLs().get(0));
    }

    @Test
    public void testSubmitKeysWithFaultyServer()
    throws Exception
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl("http://non.existing.domain.example.com"));
        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        setKeyServers(servers);

        PGPPublicKeyRingCollection keyRingCollection = new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, "martijn@djigzo.com_0x271AD23B.asc"))));

        KeyServerClientSubmitResult result = submitKeys(keyRingCollection);

        assertEquals(1, result.getURLs().size());
        assertEquals(1, result.getErrors().size());

        assertEquals(TestProperties.getPGPKeyserverURL1(), result.getURLs().get(0));
    }

    @Test
    public void testSubmitKeysFail()
    throws Exception
    {
        HKPKeyServersImpl servers = new HKPKeyServersImpl();

        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl("http://non.existing.domain.example.com"));
        servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                TestProperties.getPGPKeyserverURL1()));

        setKeyServers(servers);

        // Create a PGPPublicKeyRingCollection which returns a bogus encoded key ring
        PGPPublicKeyRingCollection keyRingCollection = new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, "martijn@djigzo.com_0x271AD23B.asc"))))
        {
            @Override
            public byte[] getEncoded() {
                return "invalid key ring".getBytes();
            }
        };

        KeyServerClientSubmitResult result = submitKeys(keyRingCollection);

        assertEquals(0, result.getURLs().size());
        assertEquals(2, result.getErrors().size());

        assertEquals("UnknownHostException: non.existing.domain.example.com (http://non.existing.domain.example.com)",
                result.getErrors().get(0));
    }
}
