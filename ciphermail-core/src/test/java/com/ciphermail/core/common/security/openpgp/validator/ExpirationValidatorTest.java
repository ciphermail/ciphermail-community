/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.security.openpgp.PGPExpirationCheckerImpl;
import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.common.security.openpgp.PGPSignatureValidatorImpl;
import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ExpirationValidatorTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Test
    public void testIsExpired()
    throws Exception
    {
        PGPPublicKeyValidator validator = new ExpirationValidator(
                new PGPExpirationCheckerImpl(new PGPSignatureValidatorImpl()));

        PGPPublicKeyValidatorResult result = validator.validate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "test@example.com-expiration-2030-12-31.gpg.asc")), null);

        assertTrue(result.isValid());
        assertNull(result.getFailureMessage());

        result = validator.validate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "expired.asc")), null);

        assertFalse(result.isValid());
        assertEquals("Public key expired on Fri Jan 11 08:02:10 CET 2013", result.getFailureMessage());

        result = validator.validate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "test@example.com.gpg.asc")), null);

        assertTrue(result.isValid());
        assertNull(result.getFailureMessage());
    }
}
