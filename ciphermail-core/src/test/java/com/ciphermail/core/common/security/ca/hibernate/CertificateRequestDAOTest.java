/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.hibernate;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CertificateRequestDAOTest
{
    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup()
    {
        CertificateRequestDAO dao = createDAO();

        for (CertificateRequestEntity entity : dao.findAll(CertificateRequestEntity.class)) {
            dao.delete(entity);
        }
    }

    private CertificateRequestDAO createDAO() {
        return CertificateRequestDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    @Test
    public void testDeleteRequest()
    {
        CertificateRequestDAO dao = createDAO();

        assertEquals(0, dao.findAll(CertificateRequestEntity.class).size());

        CertificateRequestEntity entity = dao.addRequest(new CertificateRequestEntity("h1"));

        assertEquals(1, dao.findAll(CertificateRequestEntity.class).size());

        dao.delete(entity);

        assertEquals(0, dao.findAll(CertificateRequestEntity.class).size());
    }

    @Test
    public void testGetNextRequest()
    {
        CertificateRequestDAO dao = createDAO();

        Date now = new Date();

        CertificateRequestEntity entity;

        entity = dao.getNextRequest(now);
        assertNull(entity);

        entity = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -1)));
        entity.setEmail("test1@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -3)));
        entity.setEmail("test2@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("hx", DateUtils.addDays(now, -2)));
        entity.setEmail("test3@example.com");

        assertEquals(3, dao.findAll(CertificateRequestEntity.class).size());

        entity = dao.getNextRequest(now);
        assertEquals("test2@example.com", entity.getEmail());
        dao.delete(entity);

        entity = dao.getNextRequest(now);
        assertEquals("test3@example.com", entity.getEmail());
        dao.delete(entity);

        entity = dao.getNextRequest(now);
        assertEquals("test1@example.com", entity.getEmail());
        dao.delete(entity);

        entity = dao.getNextRequest(now);
        assertNull(entity);

        CertificateRequestEntity entity1 = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -1)));
        entity1.setEmail("test1@example.com");

        CertificateRequestEntity entity2 = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -3)));
        entity2.setEmail("test2@example.com");

        CertificateRequestEntity entity3 = dao.addRequest(new CertificateRequestEntity("hx", DateUtils.addDays(now, -2)));
        entity3.setEmail("test3@example.com");

        entity = dao.getNextRequest(now);
        assertEquals("test2@example.com", entity.getEmail());

        entity = dao.getNextRequest(DateUtils.addDays(now, -4));
        assertEquals("test2@example.com", entity.getEmail());

        entity2.setNextUpdate(DateUtils.addDays(now, 3));

        entity = dao.getNextRequest(now);
        assertEquals("test3@example.com", entity.getEmail());

        entity = dao.getNextRequest(DateUtils.addDays(now, -4));
        assertEquals("test3@example.com", entity.getEmail());

        entity3.setNextUpdate(DateUtils.addDays(now, 5));

        entity = dao.getNextRequest(now);
        assertEquals("test1@example.com", entity.getEmail());

        entity1.setNextUpdate(DateUtils.addDays(now, 1));

        entity = dao.getNextRequest(DateUtils.addDays(now, 10));
        assertEquals("test1@example.com", entity.getEmail());

        entity2.setNextUpdate(now);

        entity = dao.getNextRequest(DateUtils.addDays(now, 10));
        assertEquals("test2@example.com", entity.getEmail());

        entity2.setNextUpdate(DateUtils.addDays(now, 3));

        assertEquals(3, dao.findAll(CertificateRequestEntity.class).size());

        entity = dao.getNextRequest(DateUtils.addDays(now, -4));
        assertNull(entity);

        entity = dao.getNextRequest(DateUtils.addDays(now, 2));
        assertEquals("test1@example.com", entity.getEmail());
        dao.delete(entity);

        entity = dao.getNextRequest(DateUtils.addDays(now, 2));
        assertNull(entity);

        entity = dao.getNextRequest(DateUtils.addDays(now, 4));
        assertEquals("test2@example.com", entity.getEmail());
        dao.delete(entity);

        entity = dao.getNextRequest(DateUtils.addDays(now, 4));
        assertNull(entity);

        entity = dao.getNextRequest(DateUtils.addDays(now, 6));
        assertEquals("test3@example.com", entity.getEmail());
    }

    @Test
    public void testGetSize()
    {
        CertificateRequestDAO dao = createDAO();

        assertEquals(0, dao.getSize());

        dao.addRequest(new CertificateRequestEntity("h1"));

        assertEquals(1, dao.getSize());

        dao.addRequest(new CertificateRequestEntity("h1"));

        assertEquals(2, dao.getSize());
    }

    @Test
    public void testGetSizeByEmail()
    {
        CertificateRequestDAO dao = createDAO();

        CertificateRequestEntity entity;

        entity = dao.addRequest(new CertificateRequestEntity("h1"));
        entity.setEmail("test1@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("h1"));
        entity.setEmail("test2@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("hx"));
        entity.setEmail("test1@example.com");

        assertEquals(0, dao.getSizeByEmail("testX@example.com", Match.EXACT));
        assertEquals(2, dao.getSizeByEmail("test1@example.com", Match.EXACT));
        assertEquals(1, dao.getSizeByEmail("test2@example.com", Match.EXACT));

        assertEquals(3, dao.getSizeByEmail("%@example.com", Match.LIKE));
        assertEquals(3, dao.getSizeByEmail("%@EXAMPLE.com", Match.LIKE));
        assertEquals(0, dao.getSizeByEmail("bla@example.com", Match.LIKE));
        assertEquals(0, dao.getSizeByEmail("bla@example.com", Match.LIKE));
    }

    @Test
    public void testGetAllRequests()
    {
        CertificateRequestDAO dao = createDAO();

        List<CertificateRequestEntity> entities = dao.getAllRequests(null, null);
        assertEquals(0, entities.size());

        CertificateRequestEntity entity;

        Date now = new Date();

        entity = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -1)));
        entity.setEmail("test1@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -3)));
        entity.setEmail("test2@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("hx", DateUtils.addDays(now, -2)));
        entity.setEmail("test3@example.com");

        entities = dao.getAllRequests(null, null);
        assertEquals(3, entities.size());

        assertEquals("test2@example.com", entities.get(0).getEmail());
        assertEquals("test3@example.com", entities.get(1).getEmail());
        assertEquals("test1@example.com", entities.get(2).getEmail());

        entities = dao.getAllRequests(1, 1);
        assertEquals(1, entities.size());
        assertEquals("test3@example.com", entities.get(0).getEmail());

        entities = dao.getAllRequests(2, 2);
        assertEquals(1, entities.size());
        assertEquals("test1@example.com", entities.get(0).getEmail());
    }

    @Test
    public void testGetAllRequestsIterator()
    throws CloseableIteratorException
    {
        CertificateRequestDAO dao = createDAO();

        List<CertificateRequestEntity> entities = CloseableIteratorUtils.toList(dao.getAllRequests());
        assertEquals(0, entities.size());

        CertificateRequestEntity entity;

        Date now = new Date();

        entity = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -1)));
        entity.setEmail("test1@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -3)));
        entity.setEmail("test2@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("hx", DateUtils.addDays(now, -2)));
        entity.setEmail("test3@example.com");

        entities = CloseableIteratorUtils.toList(dao.getAllRequests());
        assertEquals(3, entities.size());

        assertEquals("test2@example.com", entities.get(0).getEmail());
        assertEquals("test3@example.com", entities.get(1).getEmail());
        assertEquals("test1@example.com", entities.get(2).getEmail());
    }

    @Test
    public void testGetRequestsByEmail()
    {
        CertificateRequestDAO dao = createDAO();

        List<CertificateRequestEntity> entities;

        entities = dao.getRequestsByEmail("test1@example.com", Match.EXACT, null, null);
        assertEquals(0, entities.size());

        CertificateRequestEntity entity;

        Date now = new Date();

        entity = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -2)));
        entity.setEmail("test1@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("h1", DateUtils.addDays(now, -1)));
        entity.setEmail("test2@example.com");

        entity = dao.addRequest(new CertificateRequestEntity("hx", DateUtils.addDays(now, -3)));
        entity.setEmail("test1@example.com");

        entities = dao.getRequestsByEmail("test1@example.com", Match.EXACT, null, null);
        assertEquals(2, entities.size());

        entities = dao.getRequestsByEmail("test1@example.com", Match.EXACT, 1, null);
        assertEquals(1, entities.size());

        entities = dao.getRequestsByEmail("test1@example.com", Match.EXACT, 0, 1);
        assertEquals(1, entities.size());

        entities = dao.getRequestsByEmail("%@example.com", Match.LIKE, null, null);
        assertEquals(3, entities.size());
        assertEquals("test1@example.com", entities.get(0).getEmail());
        assertEquals("hx", entities.get(0).getCertificateHandlerName());
        assertEquals("test1@example.com", entities.get(1).getEmail());
        assertEquals("h1", entities.get(1).getCertificateHandlerName());
        assertEquals("test2@example.com", entities.get(2).getEmail());
        assertEquals("h1", entities.get(2).getCertificateHandlerName());

        entities = dao.getRequestsByEmail("%@example.com", Match.LIKE, 1, null);
        assertEquals(2, entities.size());

        entities = dao.getRequestsByEmail("%@example.com", Match.LIKE, null, 1);
        assertEquals(1, entities.size());
    }
}
