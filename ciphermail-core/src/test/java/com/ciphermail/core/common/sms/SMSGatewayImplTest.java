/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.sms.impl.SMSGatewayImpl;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SMSGatewayImplTest
{
    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private SMSTransportFactoryRegistry smsTransportFactoryRegistry;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private DummySMSTransport smsTransport;

    private static SMS expired;

    private final SMSExpiredListenerImpl smsExpiredListener = new SMSExpiredListenerImpl();

    private static class SMSExpiredListenerImpl implements SMSExpiredListener
    {
        @Override
        public void expired(SMS sms) {
            expired = sms;
        }
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Before
    public void setUpBefore() {
        smsTransport.reset();
    }

    private SMSGatewayImpl createSMSGateway()
    {
        SMSGatewayImpl smsGateway = new SMSGatewayImpl(
                smsTransportFactoryRegistry,
                userPropertiesFactoryRegistry,
                globalPreferencesManager,
                sessionManager,
                transactionOperations,
                smsExpiredListener);

        smsGateway.setSleepTime(100);
        smsGateway.setSleepTimeOnError(100);
        smsGateway.setExpirationTime(DateUtils.MILLIS_PER_SECOND * 5);

        return smsGateway;
    }

    @Test
    public void testGateway()
    throws Exception
    {
        SMSGatewayImpl gateway = createSMSGateway();

        gateway.deleteAll();
        assertEquals(0, gateway.getCount());

        // test delete
        SMS sms = gateway.sendSMS("123", "test 1");

        assertNotNull(sms);
        assertEquals(1, gateway.getCount());
        gateway.delete(sms.getID());
        assertEquals(0, gateway.getCount());

        gateway.setStartupDelayTime(0);
        gateway.start();

        try {
            assertEquals(0, smsTransport.getPhoneNumbers().size());

            gateway.sendSMS("123", "test 1");
            gateway.sendSMS("456", "test 2");

            // wait some time for the message to be sent
            ThreadUtils.sleepQuietly(1000);

            assertEquals(2, smsTransport.getPhoneNumbers().size());

            smsTransport.reset();

            assertNull(expired);

            gateway.sendSMS("123", "test 1");
            gateway.sendSMS("456", "test 2");
            gateway.sendSMS("789", "fail");

            // wait some time for the message to be sent
            ThreadUtils.sleepQuietly(1000);

            assertEquals(2, smsTransport.getPhoneNumbers().size());

            // wait some time for the sms to expire
            ThreadUtils.sleepQuietly(5000);

            assertNotNull(expired);
            assertEquals("789", expired.getPhoneNumber());
            assertEquals("fail", expired.getMessage());
        }
        finally {
            // Wait for background thread to stop
            gateway.stop(DateUtils.MILLIS_PER_SECOND * 30);
        }
    }

    /*
     * Test which checks if there are multiple threads that try send the same SMS message. This check is added to
     * test what happens in a system with a clustered database. If two systems have access to the same SMS queue,
     * an SMS message should not be sent twice. When an SMS entry is accessed, it should be locked.
     */
    @Test
    public void testConcurrentAccess()
    {
        SMSGatewayImpl gateway1 = createSMSGateway();

        gateway1.deleteAll();

        gateway1.setStartupDelayTime(0);
        gateway1.setExpirationTime(DateUtils.MILLIS_PER_SECOND * 30);
        gateway1.setUpdateInterval(1000);
        gateway1.setSleepTime(10);
        gateway1.setSleepTimeOnError(0);
        gateway1.setThreadName("#SMS1#");

        SMSGatewayImpl gateway2 = createSMSGateway();

        gateway2.deleteAll();

        gateway2.setStartupDelayTime(0);
        gateway2.setExpirationTime(DateUtils.MILLIS_PER_SECOND * 30);
        gateway2.setUpdateInterval(1000);
        gateway2.setSleepTime(10);
        gateway2.setSleepTimeOnError(0);
        gateway2.setThreadName("#SMS2#");

        int nrOfSMS = 100;

        for (int i = 0; i < nrOfSMS; i++)
        {
            gateway1.sendSMS(gateway1.getThreadName() + Integer.toString(i), "test");
            gateway1.sendSMS(gateway2.getThreadName() + Integer.toString(i), "test");
        }

        try {
            gateway1.start();
            gateway2.start();

            ThreadUtils.sleepQuietly(DateUtils.MILLIS_PER_SECOND * 10);

            assertEquals(nrOfSMS * 2L, smsTransport.getMessages().size());

            Set<String> phoneNumbers = new HashSet<>(smsTransport.getPhoneNumbers());

            assertEquals(nrOfSMS * 2L, phoneNumbers.size());

            for (int i = 0; i < nrOfSMS; i++)
            {
                assertTrue(phoneNumbers.contains(gateway1.getThreadName() + Integer.toString(i)));
                assertTrue(phoneNumbers.contains(gateway2.getThreadName() + Integer.toString(i)));
            }
        }
        finally
        {
            try {
                gateway1.stop(DateUtils.MILLIS_PER_SECOND * 30);
            }
            catch (Exception e) {
                // ignore
            }
            try {
                gateway2.stop(DateUtils.MILLIS_PER_SECOND * 30);
            }
            catch (Exception e) {
                // ignore
            }
        }
    }
}
