/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class MimeMessageWithIDTest
{
    @Test
    public void testSetMessageID()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-evolution.eml");

        assertEquals("<1195030544.6663.27.camel@ubuntu>", message.getMessageID());

        message.saveChanges();

        assertNotEquals("<1195030544.6663.27.camel@ubuntu>", message.getMessageID());

        message = TestUtils.loadTestMessage("mail/clear-signed-evolution.eml");

        assertEquals("<1195030544.6663.27.camel@ubuntu>", message.getMessageID());

        MimeMessage clone = new MimeMessage(message);

        assertEquals("<1195030544.6663.27.camel@ubuntu>", clone.getMessageID());

        clone.saveChanges();

        assertNotEquals("<1195030544.6663.27.camel@ubuntu>", clone.getMessageID());

        MimeMessage cloneWithID = new MimeMessageWithID(message, "abc");

        assertEquals("abc", cloneWithID.getMessageID());

        cloneWithID.saveChanges();

        assertEquals("abc", cloneWithID.getMessageID());
    }

    @Test
    public void testSetMessageIDInputStream()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-evolution.eml");

        assertEquals("<1195030544.6663.27.camel@ubuntu>", message.getMessageID());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        message.writeTo(bos);

        MimeMessage clone = new MimeMessage(MailSession.getDefaultSession(),
                new ByteArrayInputStream(bos.toByteArray()));

        assertEquals("<1195030544.6663.27.camel@ubuntu>", clone.getMessageID());

        clone.saveChanges();

        assertNotEquals("<1195030544.6663.27.camel@ubuntu>", clone.getMessageID());

        MimeMessage cloneWithID = new MimeMessageWithID(MailSession.getDefaultSession(),
                new ByteArrayInputStream(bos.toByteArray()), "123");

        assertEquals("123", cloneWithID.getMessageID());

        cloneWithID.saveChanges();

        assertEquals("123", cloneWithID.getMessageID());
    }

    @Test
    public void testSetMessageIDNull()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-evolution.eml");

        assertEquals("<1195030544.6663.27.camel@ubuntu>", message.getMessageID());

        MimeMessage cloneWithID = new MimeMessageWithID(message, null);

        assertEquals("<1195030544.6663.27.camel@ubuntu>", cloneWithID.getMessageID());

        cloneWithID.saveChanges();

        assertNotNull(cloneWithID.getMessageID());
        assertNotEquals("<1195030544.6663.27.camel@ubuntu>", cloneWithID.getMessageID());
    }

    @Test
    public void testSetMessageIDNullStream()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-evolution.eml");

        assertEquals("<1195030544.6663.27.camel@ubuntu>", message.getMessageID());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        message.writeTo(bos);

        MimeMessage cloneWithID = new MimeMessageWithID(MailSession.getDefaultSession(),
                new ByteArrayInputStream(bos.toByteArray()), null);

        assertEquals("<1195030544.6663.27.camel@ubuntu>", cloneWithID.getMessageID());

        cloneWithID.saveChanges();

        assertNotNull(cloneWithID.getMessageID());
        assertNotEquals("<1195030544.6663.27.camel@ubuntu>", cloneWithID.getMessageID());
    }

    @Test
    public void testWithSession()
    throws Exception
    {
        MimeMessage message = new MimeMessageWithID(MailSession.getDefaultSession(), "123");

        message.setText("test");

        message.saveChanges();

        assertEquals("123", message.getMessageID());
    }
}
