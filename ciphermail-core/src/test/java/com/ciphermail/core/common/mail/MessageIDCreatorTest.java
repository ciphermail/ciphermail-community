/*
 * Copyright (c) 2011-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.test.TestProperties;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class MessageIDCreatorTest
{
    @Test
    public void testCreateMessageID()
    {
       String messageID = MessageIDCreator.getInstance().createUniqueMessageID();

       System.out.println(messageID);

       assertTrue(messageID.contains("@"));
    }

    @Test
    public void testSpeed()
    {
        int repeat = 100000;

        long start = System.currentTimeMillis();

        String prevMessageID = null;

        for (int i = 0; i < repeat; i++)
        {
            String messageID = MessageIDCreator.getInstance().createUniqueMessageID();

            assertNotEquals(messageID, prevMessageID);

            prevMessageID = messageID;
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("create/sec: " + perSecond);

        double expected = 10000 * TestProperties.getTestPerformanceFactor();

        // NOTE: !!! can fail on a slower system
        assertTrue("nr create/sec too slow. !!! this can fail on a slower system !!!", perSecond > expected);
    }
}
