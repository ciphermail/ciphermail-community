/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.openpgp.PGPKeyRingParser.KeyRingEventListener;
import com.ciphermail.core.common.security.password.PasswordProvider;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;

class PGPKeyRingParserTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(),"pgp/");

    @Test
    void testParseLargeKeyRing()
    throws Exception
    {
        final MutableInt publicKeyRingCount = new MutableInt();

        KeyRingEventListener keyRingEventListener = new KeyRingEventListener()
        {
            @Override
            public void handleSecretKeyRing(PGPSecretKeyRing secretKeyRing, PasswordProvider passwordProvider) {
                // ignore
            }

            @Override
            public void handlePublicKeyRing(PGPPublicKeyRing publicKeyRing) {
                publicKeyRingCount.increment();
            }
        };

        PGPKeyRingParser parser = new PGPKeyRingParser();

        parser.setKeyRingEventListener(keyRingEventListener);
        parser.parseKeyRing(new FileInputStream(new File(TEST_BASE, "sks-dump-0235.pgp")), null);

        // There are more keyrings but some fail due to UTF-16 conversion errors
        Assertions.assertEquals(8565, publicKeyRingCount.intValue());
    }
}
