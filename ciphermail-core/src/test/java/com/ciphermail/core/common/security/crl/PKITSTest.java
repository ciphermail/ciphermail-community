/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilder;
import com.ciphermail.core.common.security.certpath.SMIMEExtendedKeyUsageCertPathChecker;
import com.ciphermail.core.common.security.certpath.TrustAnchorBuilder;
import com.ciphermail.core.common.security.certpath.TrustAnchorBuilderPKIXCertificatePathBuilder;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.certstore.jce.X509CertStoreParameters;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.math.BigInteger;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Some test cases for the CRL tests of PKITS
 * @author Martijn Brinkers
 *
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PKITSTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt x509CertStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt x509RootStore;

    @Autowired
    private X509CRLStoreExt x509CRLStore;

    @Autowired
    private TrustAnchorBuilder trustAnchorBuilder;

    private static Date testDate;

    private CertStore certStore;
    private X509CertStoreParameters certStoreParams;
    private X509CertStoreParameters rootStoreParams;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        testDate = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");
    }

    @Before
    public void setup()
    throws Exception
    {
        certStoreParams = new X509CertStoreParameters(x509CertStore, x509CRLStore);

        certStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, certStoreParams,
                CipherMailProvider.PROVIDER);

        rootStoreParams = new X509CertStoreParameters(x509RootStore);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStoreParams.getCertStore().removeAllEntries();
                rootStoreParams.getCertStore().removeAllEntries();
                certStoreParams.getCRLStore().removeAllEntries();

                addCertificates(new File(TestUtils.getTestDataDir(),
                        "PKITS/certs/TrustAnchorRootCertificate.crt"),
                        rootStoreParams.getCertStore());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCRL(File crlFile, X509CRLStoreExt crlStore)
    throws Exception
    {
        crlStore.addCRL(TestUtils.loadX509CRL(crlFile));
    }

    private void addCertificates(File file, X509CertStoreExt certStore)
    throws Exception
    {
        for (Certificate certificate : CertificateUtils.readCertificates(file))
        {
            if (certificate instanceof X509Certificate && !certStore.contains((X509Certificate) certificate)) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    private PKIXCertPathBuilderResult getCertPathBuilderResult(X509CertSelector selector)
    throws Exception
    {
        CertificatePathBuilder builder = new TrustAnchorBuilderPKIXCertificatePathBuilder(trustAnchorBuilder);

        builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
        builder.addCertStore(certStore);
        builder.setRevocationEnabled(false);

        // Since the certificates under test expired on 19/04/2011 we need to set
        // the date explicitly
        builder.setDate(TestUtils.parseDate("24-Mar-2008 16:38:35 GMT"));

        CertPathBuilderResult result = builder.buildPath(selector);

        assertTrue(result instanceof PKIXCertPathBuilderResult);

        return (PKIXCertPathBuilderResult) result;
    }

    @Test
    public void test_4_4_1_Missing_CRL_Test1()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/NoCRLCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidMissingCRLTest1EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(new BigInteger("1"));
                selector.setIssuer(new X500Principal("CN=No CRL CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_2_Invalid_Revoked_CA_Test2()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/GoodCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/RevokedsubCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidRevokedCATest2EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/GoodCACRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/RevokedsubCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(new BigInteger("1"));
                selector.setIssuer(new X500Principal("CN=Revoked subCA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 3);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.REVOKED, detail[1].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[2].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_3_Invalid_Revoked_EE_Test3()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/GoodCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidRevokedEETest3EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/GoodCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("F"));
                selector.setIssuer(new X500Principal("CN=Good CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_4_Invalid_Bad_CRL_Signature_Test4()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/BadCRLSignatureCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidBadCRLSignatureTest4EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/BadCRLSignatureCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=Bad CRL Signature CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                // unknown because the CRLs signature was invalid and therefore not included in the search
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_5_Invalid_Bad_CRL_Issuer_Name_Test5()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/BadCRLIssuerNameCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidBadCRLIssuerNameTest5EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/BadCRLIssuerNameCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=Bad CRL Issuer Name CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_6_Invalid_Wrong_CRL_Test6()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/WrongCRLCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidWrongCRLTest6EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                // This test wants us to add "WrongCRLCACRL.crl" but it's the exact same crl as "TrustAnchorRootCRL.crl"
                // so we will not add it again because we can only add a crl just once (thumbprint must be unique)

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=Wrong CRL CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_7_Valid_Two_CRLs_Test7()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/TwoCRLsCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValidTwoCRLsTest7EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TwoCRLsCAGoodCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TwoCRLsCABadCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=Two CRLs CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_8_Invalid_Unknown_CRL_Entry_Extension_Test8()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/UnknownCRLEntryExtensionCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidUnknownCRLEntryExtensionTest8EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/UnknownCRLEntryExtensionCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=Unknown CRL Entry Extension CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_9_Invalid_Unknown_CRL_Extension_Test9()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/UnknownCRLExtensionCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidUnknownCRLExtensionTest9EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/UnknownCRLExtensionCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=Unknown CRL Extension CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_10_Invalid_Unknown_CRL_Extension_Test10()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/UnknownCRLExtensionCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidUnknownCRLExtensionTest10EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/UnknownCRLExtensionCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("2"));
                selector.setIssuer(new X500Principal("CN=Unknown CRL Extension CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNSUPPORTED_CRITICAL_EXTENSION, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNSUPPORTED_CRITICAL_EXTENSION, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_11_Invalid_Old_CRL_nextUpdate_Test11()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/OldCRLnextUpdateCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidOldCRLnextUpdateTest11EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/OldCRLnextUpdateCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=Old CRL nextUpdate CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("02-Jan-2002 16:38:35 GMT");

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.EXPIRED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.EXPIRED, detail[0].getStatus());
                assertTrue(DateUtils.addDays(detail[0].getNextUpdate(),2).after(now));
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_12_Invalid_pre2000_CRL_nextUpdate_Test12()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/pre2000CRLnextUpdateCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/Invalidpre2000CRLnextUpdateTest12EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/pre2000CRLnextUpdateCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=pre2000 CRL nextUpdate CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.EXPIRED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.EXPIRED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_13_Valid_GeneralizedTime_CRL_nextUpdate_Test13()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/GeneralizedTimeCRLnextUpdateCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValidGeneralizedTimeCRLnextUpdateTest13EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/GeneralizedTimeCRLnextUpdateCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=GenerizedTime CRL nextUpdate CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_14_Valid_Negative_Serial_Number_Test14()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/NegativeSerialNumberCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValidNegativeSerialNumberTest14EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/NegativeSerialNumberCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("FF"));
                selector.setIssuer(new X500Principal("CN=Negative Serial Number CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_15_Invalid_Negative_Serial_Number_Test15()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/NegativeSerialNumberCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidNegativeSerialNumberTest15EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/NegativeSerialNumberCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(new BigInteger("-1"));
                selector.setIssuer(new X500Principal("CN=Negative Serial Number CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_16_Valid_Long_Serial_Number_Test16()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/LongSerialNumberCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValidLongSerialNumberTest16EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/LongSerialNumberCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("7F0102030405060708090A0B0C0D0E0F10111212"));
                selector.setIssuer(new X500Principal("CN=Long Serial Number CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertEquals(null, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_17_Valid_Long_Serial_Number_Test17()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/LongSerialNumberCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValidLongSerialNumberTest17EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/LongSerialNumberCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("7E0102030405060708090A0B0C0D0E0F10111213"));
                selector.setIssuer(new X500Principal("CN=Long Serial Number CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_4_18_Invalid_Long_Serial_Number_Test18()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/LongSerialNumberCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidLongSerialNumberTest18EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/LongSerialNumberCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("7F0102030405060708090A0B0C0D0E0F10111213"));
                selector.setIssuer(new X500Principal("CN=Long Serial Number CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    // TODO: Add support for multiple signing keys for tests 4.4.19 - 4.4.21.


    @Test
    public void test_4_7_4_Invalid_keyUsage_Critical_cRLSign_False_Test4()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/keyUsageCriticalcRLSignFalseCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidkeyUsageCriticalcRLSignFalseTest4EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/keyUsageCriticalcRLSignFalseCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal(
                        "CN=keyUsage Critical cRLSign False CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }


    @Test
    public void test_4_7_5_Invalid_keyUsage_Not_Critical_cRLSign_False_Test5()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/keyUsageNotCriticalcRLSignFalseCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidkeyUsageNotCriticalcRLSignFalseTest5EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/keyUsageNotCriticalcRLSignFalseCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal(
                        "CN=keyUsage Not Critical cRLSign False CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_15_1_Invalid_deltaCRLIndicator_No_Base_Test1()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/deltaCRLIndicatorNoBaseCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddeltaCRLIndicatorNoBaseTest1EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLIndicatorNoBaseCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal(
                        "CN=deltaCRLIndicator No Base CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_15_2_Valid_delta_CRL_Test2()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/deltaCRLCA1Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValiddeltaCRLTest2EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1CRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1deltaCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=deltaCRL CA1, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_15_3_Invalid_delta_CRL_Test3()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/deltaCRLCA1Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddeltaCRLTest3EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1CRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1deltaCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("2"));
                selector.setIssuer(new X500Principal("CN=deltaCRL CA1, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_15_4_Invalid_delta_CRL_Test4()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/deltaCRLCA1Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddeltaCRLTest4EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1CRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1deltaCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("3"));
                selector.setIssuer(new X500Principal("CN=deltaCRL CA1, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    // TODO: Support 4.15.5 Valid delta-CRL Test5. Whoever came up with the stupid idea of onHold and removed
    // from CRL probably never built any systems him/her-self.

    @Test
    public void test_4_15_6_Invalid_delta_CRL_Test6()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/deltaCRLCA1Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddeltaCRLTest6EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1CRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1deltaCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("5"));
                selector.setIssuer(new X500Principal("CN=deltaCRL CA1, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    // TODO: Support 4.15.7 Valid delta-CRL Test7

    @Test
    public void test_4_15_8_Valid_delta_CRL_Test8()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/deltaCRLCA2Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValiddeltaCRLTest8EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA2CRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA2deltaCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=deltaCRL CA2, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_15_9_Invalid_delta_CRL_Test9()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/deltaCRLCA2Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddeltaCRLTest9EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA2CRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA2deltaCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("2"));
                selector.setIssuer(new X500Principal("CN=deltaCRL CA2, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_15_10_Invalid_delta_CRL_Test10()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/deltaCRLCA3Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddeltaCRLTest10EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA3CRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA3deltaCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=deltaCRL CA3, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.EXPIRED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.EXPIRED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_1_Valid_distributionPoint_Test1()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/distributionPoint1CACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValiddistributionPointTest1EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint1CACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("OU=distributionPoint1 CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_2_Invalid_distributionPoint_Test2()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/distributionPoint1CACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddistributionPointTest2EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint1CACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("2"));
                selector.setIssuer(new X500Principal("OU=distributionPoint1 CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_3_Invalid_distributionPoint_Test3()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/distributionPoint1CACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddistributionPointTest3EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint1CACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("3"));
                selector.setIssuer(new X500Principal("OU=distributionPoint1 CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());


                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_4_Valid_distributionPoint_Test4()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/distributionPoint1CACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValiddistributionPointTest4EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint1CACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("4"));
                selector.setIssuer(new X500Principal("OU=distributionPoint1 CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_5_Valid_distributionPoint_Test5()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/distributionPoint2CACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValiddistributionPointTest5EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint2CACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("OU=distributionPoint2 CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_6_Invalid_distributionPoint_Test6()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/distributionPoint2CACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddistributionPointTest6EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint2CACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("2"));
                selector.setIssuer(new X500Principal("OU=distributionPoint2 CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_7_Valid_distributionPoint_Test7()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/distributionPoint2CACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValiddistributionPointTest7EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint2CACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("3"));
                selector.setIssuer(new X500Principal("OU=distributionPoint2 CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_8_Invalid_distributionPoint_Test8()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/distributionPoint2CACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddistributionPointTest8EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint2CACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("4"));
                selector.setIssuer(new X500Principal("OU=distributionPoint2 CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_9_Invalid_distributionPoint_Test9()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/distributionPoint2CACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvaliddistributionPointTest9EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint2CACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("5"));
                selector.setIssuer(new X500Principal("OU=distributionPoint2 CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_10_Valid_No_issuingDistributionPoint_Test10()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/NoissuingDistributionPointCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValidNoissuingDistributionPointTest10EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/NoissuingDistributionPointCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("OU=No issuingDistributionPoint CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }


    @Test
    public void test_4_14_11_Invalid_onlyContainsUserCerts_CRL_Test11()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlyContainsUserCertsCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidonlyContainsUserCertsTest11EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlyContainsUserCertsCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=onlyContainsUserCerts CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_12_Invalid_onlyContainsCACerts_CRL_Test12()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlyContainsCACertsCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidonlyContainsCACertsTest12EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlyContainsCACertsCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=onlyContainsCACerts CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_13_Valid_onlyContainsCACerts_CRL_Test13()
    {
        transactionOperations.executeWithoutResult(status ->

        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlyContainsCACertsCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValidonlyContainsCACertsTest13EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlyContainsCACertsCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("2"));
                selector.setIssuer(new X500Principal("CN=onlyContainsCACerts CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_14_Invalid_onlyContainsAttributeCerts_Test14()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlyContainsAttributeCertsCACert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidonlyContainsAttributeCertsTest14EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlyContainsAttributeCertsCACRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=onlyContainsAttributeCerts CA, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);

                assertEquals("CN=Trust Anchor, O=Test Certificates, C=US",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_15_Invalid_onlySomeReasons_Test15()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlySomeReasonsCA1Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidonlySomeReasonsTest15EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA1compromiseCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA1otherreasonsCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=onlySomeReasons CA1, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_16_Invalid_onlySomeReasons_Test16()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlySomeReasonsCA1Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidonlySomeReasonsTest16EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA1compromiseCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA1otherreasonsCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("2"));
                selector.setIssuer(new X500Principal("CN=onlySomeReasons CA1, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.CERTIFICATE_HOLD, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_17_Invalid_onlySomeReasons_Test17()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlySomeReasonsCA2Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidonlySomeReasonsTest17EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA2CRL1.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA2CRL2.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("CN=onlySomeReasons CA2, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.UNKNOWN, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.UNKNOWN, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_18_Valid_onlySomeReasons_Test18()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlySomeReasonsCA3Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValidonlySomeReasonsTest18EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA3compromiseCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA3otherreasonsCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("OU=onlySomeReasons CA3, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_19_Valid_onlySomeReasons_Test19()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlySomeReasonsCA4Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/ValidonlySomeReasonsTest19EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA4compromiseCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA4otherreasonsCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("1"));
                selector.setIssuer(new X500Principal("OU=onlySomeReasons CA4, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationResult.getStatus());
                assertNull(revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.NOT_REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.NOT_REVOKED, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_20_Invalid_onlySomeReasons_Test20()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlySomeReasonsCA4Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidonlySomeReasonsTest20EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA4compromiseCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA4otherreasonsCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("2"));
                selector.setIssuer(new X500Principal("OU=onlySomeReasons CA4, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void test_4_14_21_Invalid_onlySomeReasons_Test21()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/onlySomeReasonsCA4Cert.crt"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TestUtils.getTestDataDir(), "PKITS/certs/InvalidonlySomeReasonsTest21EE.crt"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/TrustAnchorRootCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA4compromiseCRL.crl"),
                        certStoreParams.getCRLStore());
                addCRL(new File(TestUtils.getTestDataDir(), "PKITS/crls/onlySomeReasonsCA4otherreasonsCRL.crl"),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("3"));
                selector.setIssuer(new X500Principal("OU=onlySomeReasons CA4, O=Test Certificates, C=US"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath, trustAnchor,
                        testDate);

                assertEquals(RevocationStatus.REVOKED, revocationResult.getStatus());
                assertEquals(RevocationReason.AFFILIATION_CHANGED, revocationResult.getReason());

                RevocationDetail[] detail = revocationResult.getDetails();

                assertEquals(detail.length, 2);
                assertEquals(RevocationStatus.REVOKED, detail[0].getStatus());
                assertEquals(RevocationStatus.UNKNOWN, detail[1].getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
    // TODO: support indirect CRLs
}
