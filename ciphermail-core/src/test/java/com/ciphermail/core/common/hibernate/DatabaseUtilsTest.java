/*
 * Copyright (c) 2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

public class DatabaseUtilsTest
{
    @Test
    public void testLimitDateMaxNotReached()
    {
        Date now = new Date();

        Date result = DatabaseUtils.limitDate(now);

        assertEquals(now, result);
        assertSame(now, result);
    }

    @Test
    public void testLimitDateMaxReached()
    throws ParseException
    {
        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("10000-01-01 01:01:01");

        Date result = DatabaseUtils.limitDate(date);

        assertNotSame(date, result);
        assertNotEquals(date, result);
        assertEquals(DatabaseUtils.MYSQL_MAX_DATE, result);

        date = new Date(Long.MAX_VALUE);

        result = DatabaseUtils.limitDate(date);

        assertNotSame(date, result);
        assertNotEquals(date, result);
        assertEquals(DatabaseUtils.MYSQL_MAX_DATE, result);
    }

    @Test
    public void testLimitDateMinReached()
    {
        Date date = DateUtils.addSeconds(DatabaseUtils.MYSQL_MIN_DATE, -1);

        Date result = DatabaseUtils.limitDate(date);

        assertNotSame(date, result);
        assertNotEquals(date, result);
        assertEquals(DatabaseUtils.MYSQL_MIN_DATE, result);

        date = DateUtils.addYears(DatabaseUtils.MYSQL_MIN_DATE, -1000);

        result = DatabaseUtils.limitDate(date);

        assertNotSame(date, result);
        assertNotEquals(date, result);
        assertEquals(DatabaseUtils.MYSQL_MIN_DATE, result);
    }

    @Test
    public void testLimitDateNull() {
        assertNull(DatabaseUtils.limitDate(null));
    }
}
