/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crlstore.hibernate;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreEntry;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.CRL;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLSelector;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class X509CRLStoreExtHibernateTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    private X509CRLStoreExt crlStore;
    private X509CRLStoreExt otherCRLStore;

    @Before
    public void setup()
    {
        crlStore = new X509CRLStoreExtHibernate("crlStore", sessionManager);

        // Create other store to check whether the CRL stores do not interact
        otherCRLStore = new X509CRLStoreExtHibernate("otherCRLStore", sessionManager);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                crlStore.removeAllEntries();
                otherCRLStore.removeAllEntries();

                // Hibernate inserts before delete so we need to flush before adding new CRLs to make sure that
                // we do not get a constrain violation. Alternatively we can add the CRLs in a new transaction.
                sessionManager.getSession().flush();

                addTestCRLs(otherCRLStore);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private static void addCRL(String filename, X509CRLStoreExt crlStore)
    throws Exception
    {
        crlStore.addCRL(TestUtils.loadX509CRL(new File(TestUtils.getTestDataDir(), "crls/" + filename)));
    }

    private static void addTestCRLs(X509CRLStoreExt crlStore)
    throws Exception
    {
        addCRL("intel-basic-enterprise-issuing-CA.crl", crlStore);
        addCRL("test-ca-no-next-update.crl", crlStore);
        addCRL("itrus.com.cn.crl", crlStore);
        addCRL("test-ca.crl", crlStore);
        addCRL("ThawteSGCCA.crl", crlStore);
    }

    @Test
    public void testSize()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCRLs(crlStore);

                assertEquals(5, crlStore.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSizeOtherStore()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(5, otherCRLStore.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testRemoveCRL()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCRLs(crlStore);

                Collection<X509CRL> crls = crlStore.getCRLs(null);

                assertEquals(5, crls.size());

                X509CRL crl = crls.iterator().next();

                assertTrue(crlStore.contains(crl));

                crlStore.remove(crl);

                assertFalse(crlStore.contains(crl));

                crls = crlStore.getCRLs(null);

                assertEquals(4, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUniqueConstraint()
    {
        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    addCRL("itrus.com.cn.crl", crlStore);
                    addCRL("itrus.com.cn.crl", crlStore);
                }
                catch (Exception e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch (DataIntegrityViolationException e) {
            // expected
        }
    }

    @Test
    public void testGetAllCRLsNullSelector()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCRLs(crlStore);

                Collection<? extends CRL> crls = crlStore.getCRLs(null);

                assertEquals(5, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetAllCRLs()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCRLs(crlStore);

                X509CRLSelector selector = new X509CRLSelector();

                Collection<? extends CRL> crls = crlStore.getCRLs(selector);

                assertEquals(5, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCRLs(crlStore);

                X509CRLSelector selector = new X509CRLSelector();

                CloseableIterator<? extends CRL> iterator = crlStore.getCRLIterator(selector);

                assertFalse(iterator.isClosed());

                List<? extends CRL> crls = CloseableIteratorUtils.toList(iterator);

                assertTrue(iterator.isClosed());

                assertEquals(5, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLIteratorNulSelector()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCRLs(crlStore);

                CloseableIterator<? extends CRL> iterator = crlStore.getCRLIterator(null);

                assertFalse(iterator.isClosed());

                List<? extends CRL> crls = CloseableIteratorUtils.toList(iterator);

                assertTrue(iterator.isClosed());

                assertEquals(5, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLIteratorNoCRLs()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CloseableIterator<? extends CRL> iterator = crlStore.getCRLIterator(null);

                assertFalse(iterator.isClosed());

                List<? extends CRL> crls = CloseableIteratorUtils.toList(iterator);

                assertTrue(iterator.isClosed());

                assertEquals(0, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testRemoveAll()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Collection<? extends CRL> crls = crlStore.getCRLs(null);

                assertEquals(0, crls.size());

                addTestCRLs(crlStore);

                crls = crlStore.getCRLs(null);

                assertEquals(5, crls.size());

                crlStore.removeAllEntries();

                crls = crlStore.getCRLs(null);

                assertEquals(0, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLEntryIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCRLs(crlStore);

                X509CRLSelector selector = new X509CRLSelector();

                CloseableIterator<? extends X509CRLStoreEntry> iterator = crlStore.getCRLStoreIterator(selector);

                assertFalse(iterator.isClosed());

                List<? extends X509CRLStoreEntry> crls = CloseableIteratorUtils.toList(iterator);

                assertTrue(iterator.isClosed());

                assertEquals(5, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLEntryIteratorNullSelector()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCRLs(crlStore);

                CloseableIterator<? extends X509CRLStoreEntry> iterator = crlStore.getCRLStoreIterator(null);

                assertFalse(iterator.isClosed());

                List<? extends X509CRLStoreEntry> crls = CloseableIteratorUtils.toList(iterator);

                assertTrue(iterator.isClosed());

                assertEquals(5, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLEntryIteratorNoCRLs()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CRLSelector selector = new X509CRLSelector();

                CloseableIterator<? extends X509CRLStoreEntry> iterator = crlStore.getCRLStoreIterator(selector);

                assertFalse(iterator.isClosed());

                List<? extends X509CRLStoreEntry> crls = CloseableIteratorUtils.toList(iterator);

                assertTrue(iterator.isClosed());

                assertEquals(0, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * The CRL has a nextUpdate value which exceeds the maximum date value accepted by MySQL which resulted in an
     * exception.
     *
     * '10000-01-01 00:59:59'
     *
     * This CRL has been found "in the wild". We will maximize the date values to make sure the CRL can be imported
     *
     */
    @Test
    public void testCRLNextUpdateTooLargeForMySQL()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCRL("max-next-update-time.crl", crlStore);
                assertEquals(1, crlStore.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
