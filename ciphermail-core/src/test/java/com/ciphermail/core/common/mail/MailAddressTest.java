/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import org.apache.commons.lang.SerializationUtils;
import org.apache.james.core.MailAddress;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.ParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Test for James MailAddress. Strictly speaking this test doesn't belong here because it's not the correct package.
 * However because we have modified MailAddress to make it accept more email addresses we decided to put the test in
 * this package and not in the James source.
 *
 * @author Martijn Brinkers
 *
 */
public class MailAddressTest
{
    @Test
    public void testValid()
    throws Exception
    {
        MailAddress address = new MailAddress("test@example.com");
        assertEquals("test", address.getLocalPart());
        assertEquals("example.com", address.getDomain().asString());

        address = new MailAddress("  test@example.com  ");
        assertEquals("test", address.getLocalPart());
        assertEquals("example.com", address.getDomain().asString());

        address = new MailAddress("\"test.\"@example.com  ");
        assertEquals("\"test.\"", address.getLocalPart());
        assertEquals("example.com", address.getDomain().asString());

        // dot num ([])
        address = new MailAddress("test@[1.2.3.4]");
        assertEquals("test", address.getLocalPart());
        assertEquals("1.2.3.4", address.getDomain().asString());

        assertEquals("test@exa-mple.com", new InternetAddress("test@exa-mple.com").getAddress());
    }

    @Test
    public void testToInternetAddress()
    throws Exception
    {
        MailAddress address = new MailAddress("test@example.com");
        assertEquals("test@example.com", address.toInternetAddress().get().toString());
    }

    private void shouldFail(String address)
    {
        try {
            new MailAddress(address);

            fail();
        }
        catch(ParseException e) {
            // expected
        }
    }

    @Test
    public void testInvalid()
    throws Exception
    {
        shouldFail("test@-example.com");
        shouldFail("test.@example.com");
    }

    @Test
    public void testSerialize()
    throws Exception
    {
        MailAddress address = new MailAddress("test@example.com");

        MailAddress copy = (MailAddress) SerializationUtils.deserialize(SerializationUtils.serialize(address));

        assertEquals(address, copy);
    }
}
