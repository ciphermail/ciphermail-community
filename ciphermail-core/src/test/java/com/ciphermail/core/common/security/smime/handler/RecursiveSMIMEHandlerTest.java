/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.handler;

import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.smime.SMIMEHeader;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class RecursiveSMIMEHandlerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore certStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    private PKISecurityServices pkiSecurityServices;

    private RecursiveSMIMEHandler recursiveSMIMEHandler;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Clean key/root/crl store
                certStore.removeAllEntries();
                rootStore.removeAllEntries();

                importKeyStore(keyAndCertificateWorkflow, new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12"), "test");

                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-root.cer"), rootStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-ca.cer"), certStore);

                recursiveSMIMEHandler = new RecursiveSMIMEHandler(pkiSecurityServices);
                recursiveSMIMEHandler.setCertificatesEventListener(createCertificatesEventListener());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private static void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile,
            String password)
    throws Exception
    {
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        keyStore.load(new FileInputStream(pfxFile), password.toCharArray());

        keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
    }

    private static void addCertificates(File file, X509CertStoreExt certStore)
    throws Exception
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        addCertificates(certificates, certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore)
    throws Exception
    {
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate && !certStore.contains((X509Certificate) certificate)) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    private CertificateCollectionEvent createCertificatesEventListener()
    {
        return certificates ->
        {
            for (X509Certificate certificate : certificates)
            {
                try {
                    if (!certStore.contains(certificate)) {
                        certStore.addCertificate(certificate);
                    }
                }
                catch (CertStoreException e) {
                    throw new CertificateException(e);
                }
            }
        };
    }

    @Test
    public void testEmbeddedClearSigned()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/embedded-clear-signed.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSignedKeyUsageNotForSigning()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/clear-signed-key-usage-not-for-signing.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/1178C38151374D6C4B29F891B9B4A77//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("False", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Signer-Trusted-Info-0-0", ","));

                assertTrue(header.startsWith("Signing certificate not trusted. Message: Key usage does not "
                    + "allow DIGITALSIGNATURE or NONREPUDIATION."));

                assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSignedMissingExtSMIMEKeyUsage()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/clear-sign-missing-smime-ext-key-usage.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FD035BA042503BCC6CA44680F9F8//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("False", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Signer-Trusted-Info-0-0", ","));

                assertTrue(header.startsWith("Signing certificate not trusted. Message: Extended key usage does not "
                    + "allow EMAILPROTECTION."));

                assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSignedEncryptEmptyBody()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-signed-encrypted-rfc822-empty-body.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-1", ","));
                assertEquals("True", header);

                assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSignedEncryptEmptyBodyRemoveSignature()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-signed-encrypted-rfc822-empty-body.eml"));

                recursiveSMIMEHandler.setRemoveSignature(true);

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-1", ","));
                assertEquals("True", header);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedRFC822()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-rfc822.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));

                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAttachedEncryptedRFC822()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-encrypted-rfc822.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                Multipart mp = (Multipart) newMessage.getContent();

                assertEquals(2, mp.getCount());

                MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);

                assertTrue(bp.isMimeType("multipart/alternative"));

                Multipart mp2 = (Multipart) bp.getContent();

                assertEquals(2, mp2.getCount());

                bp = (MimeBodyPart) mp.getBodyPart(1);

                assertTrue(bp.isMimeType("message/rfc822"));

                newMessage = (MimeMessage) bp.getContent();

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));

                assertEquals("AES128_CBC, Key size: 128", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-123", ","));
                assertEquals("456", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAttachedEncryptedCorrupt()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-corrupt-encrypted.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNull(newMessage);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedNonStandardContentType()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-non-standard-smime-headers.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                // message cannot be decrypted because there is no private key
                assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAttachedEncryptedNonStandardContentType()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-encrypted-non-standard-content-type.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                Multipart mp = (Multipart) newMessage.getContent();

                assertEquals(2, mp.getCount());

                MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);

                assertTrue(bp.isMimeType("multipart/alternative"));

                Multipart mp2 = (Multipart) bp.getContent();

                assertEquals(2, mp2.getCount());

                bp = (MimeBodyPart) mp.getBodyPart(1);

                assertTrue(bp.isMimeType("message/rfc822"));

                newMessage = (MimeMessage) bp.getContent();

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));

                assertEquals("AES128_CBC, Key size: 128", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAttachedMultipleParts()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-signed-encrypted-multiple-parts.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                Multipart mp = (Multipart) newMessage.getContent();

                assertEquals(3, mp.getCount());

                MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);

                assertTrue(bp.isMimeType("multipart/alternative"));

                Multipart mp2 = (Multipart) bp.getContent();

                assertEquals(2, mp2.getCount());

                bp = (MimeBodyPart) mp.getBodyPart(1);

                assertTrue(bp.isMimeType("message/rfc822"));

                newMessage = (MimeMessage) bp.getContent();

                assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));

                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-1", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);


                // get the decompressed part
                bp = (MimeBodyPart) mp.getBodyPart(2);

                assertTrue(bp.isMimeType("message/rfc822"));

                newMessage = (MimeMessage) bp.getContent();

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Compressed-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAttachedSignedEncrypted()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-signed-encrypted.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                Multipart mp = (Multipart) newMessage.getContent();

                assertEquals(2, mp.getCount());

                MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);

                assertTrue(bp.isMimeType("multipart/alternative"));

                Multipart mp2 = (Multipart) bp.getContent();

                assertEquals(2, mp2.getCount());

                bp = (MimeBodyPart) mp.getBodyPart(1);

                assertTrue(bp.isMimeType("message/rfc822"));

                newMessage = (MimeMessage) bp.getContent();

                assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));

                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-1", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAttachedSignedEncryptedRemoveSignature()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-signed-encrypted.eml"));

                recursiveSMIMEHandler.setRemoveSignature(true);

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                Multipart mp = (Multipart) newMessage.getContent();

                assertEquals(2, mp.getCount());

                MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);

                assertTrue(bp.isMimeType("multipart/alternative"));

                Multipart mp2 = (Multipart) bp.getContent();

                assertEquals(2, mp2.getCount());

                bp = (MimeBodyPart) mp.getBodyPart(1);

                assertTrue(bp.isMimeType("message/rfc822"));

                newMessage = (MimeMessage) bp.getContent();

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));

                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-1", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAttachedSignedRemoveSignature()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-signed.eml"));

                recursiveSMIMEHandler.setRemoveSignature(true);

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                Multipart mp = (Multipart) newMessage.getContent();

                assertEquals(2, mp.getCount());

                MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);

                assertTrue(bp.isMimeType("multipart/alternative"));

                Multipart mp2 = (Multipart) bp.getContent();

                assertEquals(2, mp2.getCount());

                bp = (MimeBodyPart) mp.getBodyPart(1);

                assertTrue(bp.isMimeType("message/rfc822"));

                newMessage = (MimeMessage) bp.getContent();

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAttachedSigned()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-signed.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                Multipart mp = (Multipart) newMessage.getContent();

                assertEquals(2, mp.getCount());

                MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);

                assertTrue(bp.isMimeType("multipart/alternative"));

                Multipart mp2 = (Multipart) bp.getContent();

                assertEquals(2, mp2.getCount());

                bp = (MimeBodyPart) mp.getBodyPart(1);

                assertTrue(bp.isMimeType("message/rfc822"));

                newMessage = (MimeMessage) bp.getContent();

                assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAttachedEncrypted()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/attached-encrypted.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                Multipart mp = (Multipart) newMessage.getContent();

                assertEquals(2, mp.getCount());

                MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(0);

                assertTrue(bp.isMimeType("multipart/alternative"));

                Multipart mp2 = (Multipart) bp.getContent();

                assertEquals(2, mp2.getCount());

                bp = (MimeBodyPart) mp.getBodyPart(1);

                assertTrue(bp.isMimeType("message/rfc822"));

                newMessage = (MimeMessage) bp.getContent();

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));

                assertEquals("AES128_CBC, Key size: 128", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptSigned()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypt-signed-validcertificate.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-1", ","));
                assertNull(header);

                assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSignedEncryptSigned()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/signed-encrypt-signed-validcertificate.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-1", ","));

                assertNull(header);

                assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSignedEncryptSignedRemoveSignature()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/signed-encrypt-signed-validcertificate.eml"));

                recursiveSMIMEHandler.setRemoveSignature(true);

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Signer-ID-0-0", ","));

                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-1", ","));

                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-2", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-2", ","));
                assertEquals("True", header);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptSignedEncryptRemoveSignature()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypt-signed-encrypt-validcertificate.eml"));

                recursiveSMIMEHandler.setRemoveSignature(true);

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-1", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));

                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-2", ","));
                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSignedEncryptRemoveSignature()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/signed-encrypt-validcertificate.eml"));

                recursiveSMIMEHandler.setRemoveSignature(true);

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-1", ","));
                assertEquals("True", header);

                assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSignedEncrypt()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/signed-encrypt-validcertificate.eml"));

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-1", ","));
                assertEquals("True", header);

                assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-Test", ","));
                assertEquals("test 1,test 2", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testMaxRecursion()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypt-signed-encrypt-validcertificate.eml"));

                recursiveSMIMEHandler.setMaxRecursion(2);
                recursiveSMIMEHandler.setRemoveSignature(true);

                MimeMessage newMessage = recursiveSMIMEHandler.handlePart(message);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-1", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));

                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
