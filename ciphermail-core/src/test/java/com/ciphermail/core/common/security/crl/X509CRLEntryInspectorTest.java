/*
 * Copyright (c) 2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class X509CRLEntryInspectorTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(),"crls/");

    @Test
    public void testReason()
    throws Exception
    {
        X509CRL crl = Objects.requireNonNull(TestUtils.loadX509CRL(
                new File(TEST_BASE, "verisign-critical-crl-bug.crl")));

        X509CRLEntry crlEntry = crl.getRevokedCertificate(BigIntegerUtils.hexDecode(
                "0914677B95541EE51873D2EA49D7CC62"));

        X509CRLEntryInspector inspector = new X509CRLEntryInspector(crlEntry);

        assertEquals(RevocationReason.KEY_COMPROMISE, inspector.getReason());
        assertEquals((Integer)1, inspector.getReasonCode());
    }

    @Test
    public void testNoReason()
    throws Exception
    {
        X509CRL crl = Objects.requireNonNull(TestUtils.loadX509CRL(
                new File(TEST_BASE, "ThawteSGCCA.crl")));

        X509CRLEntry crlEntry = crl.getRevokedCertificate(BigIntegerUtils.hexDecode(
                "4B4CDC90F8F4D58C89CB5EC922567440"));

        X509CRLEntryInspector inspector = new X509CRLEntryInspector(crlEntry);

        assertNull(inspector.getReason());
        assertNull(inspector.getReasonCode());
    }

    @Test
    public void testSerialNumber()
    {
        X509CRL crl = Objects.requireNonNull(TestUtils.loadX509CRL(
                new File(TEST_BASE, "verisign-critical-crl-bug.crl")));

        X509CRLEntry crlEntry = crl.getRevokedCertificate(BigIntegerUtils.hexDecode(
                "914677B95541EE51873D2EA49D7CC62"));

        X509CRLEntryInspector inspector = new X509CRLEntryInspector(crlEntry);

        assertEquals("914677B95541EE51873D2EA49D7CC62", inspector.getSerialNumberHex());
    }

    @Test
    public void testToString()
    {
        X509CRL crl = Objects.requireNonNull(TestUtils.loadX509CRL(
                new File(TEST_BASE, "verisign-critical-crl-bug.crl")));

        X509CRLEntry crlEntry = crl.getRevokedCertificate(BigIntegerUtils.hexDecode(
                "914677B95541EE51873D2EA49D7CC62"));

        assertEquals("914677B95541EE51873D2EA49D7CC62\t2011-04-14\tKEY_COMPROMISE",
                X509CRLEntryInspector.toString(crlEntry));
    }
}
