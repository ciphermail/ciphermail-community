/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LFInputStreamTest
{
    private String filter(String input)
    throws UnsupportedEncodingException, IOException
    {
        return IOUtils.toString(new LFInputStream(new ByteArrayInputStream(
                input.getBytes(StandardCharsets.US_ASCII))), StandardCharsets.US_ASCII);
    }

    @Test
    public void testLFInput()
    throws Exception
    {
        assertEquals("test\n123\n456\n\n\n\n\n", filter("test\n123\r456\n\r\n\n\r\r"));
        assertEquals("", filter(""));
        assertEquals("test", filter("test"));
        assertEquals("\n\n\n\n\n\n\n\n\n\n", filter("\n\n\n\n\n\n\n\n\n\n"));
        assertEquals("\n\n\n\n\n\n\n\n\n\n", filter("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n"));
        assertEquals("\n\n\n\n\n\n\n\n\n\n", filter("\r\r\r\r\r\r\r\r\r\r"));
    }

    @Test
    public void testClose()
    throws Exception
    {
        MutableBoolean closed = new MutableBoolean();

        InputStream delegate = new ByteArrayInputStream("test".getBytes()) {
            @Override
            public void close() {
                closed.setValue(true);
            }
        };

        LFInputStream input = new LFInputStream(delegate);

        assertFalse(closed.booleanValue());
        input.close();
        assertTrue(closed.booleanValue());
    }
}
