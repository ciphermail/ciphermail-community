/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CRLLocatorTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private X509CRLStoreExt crlStore;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                crlStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private X509CRL addCRL(File crlFile, X509CRLStoreExt crlStore)
    throws Exception
    {
        X509CRL crl = TestUtils.loadX509CRL(crlFile);

        crlStore.addCRL(crl);

        return crl;
    }

    @Test
    public void testMultipleCRLs()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TEST_BASE, "crls/ThawteSGCCA.crl");

                X509CRL crl = addCRL(crlFile, crlStore);

                assertTrue(crlStore.contains(crl));

                crlFile = new File(TEST_BASE, "crls/ThawteSGCCA-thisupdate-211207.crl");

                X509CRL otherCRL = addCRL(crlFile, crlStore);

                CRLLocator locator = new CRLLocator(crlStore);

                X509Certificate issuer = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/ThawteSGCCA.cer"));

                assertNotNull(issuer);

                List<X509CRL> crls = locator.findCRLs(issuer);

                assertEquals(2, crls.size());

                assertTrue(crls.contains(crl));
                assertTrue(crls.contains(otherCRL));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testNoCRLsFound()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TEST_BASE, "crls/ThawteSGCCA.crl");

                addCRL(crlFile, crlStore);

                crlFile = new File(TEST_BASE, "crls/ThawteSGCCA-thisupdate-211207.crl");

                addCRL(crlFile, crlStore);

                CRLLocator locator = new CRLLocator(crlStore);

                X509Certificate issuer = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/testcertificate.cer"));

                assertNotNull(issuer);

                List<X509CRL> crls = locator.findCRLs(issuer);

                assertEquals(0, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEmptyStore()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CRLLocator locator = new CRLLocator(crlStore);

                X509Certificate issuer = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/ThawteSGCCA.cer"));

                assertNotNull(issuer);

                List<X509CRL> crls = locator.findCRLs(issuer);

                assertEquals(0, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
