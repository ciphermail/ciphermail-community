/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.openpgp.validator.EncryptionKeyValidator;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPPublicKeySelectorImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private PGPKeyRingEntry getByKeyID(long keyID, MissingKeyAlias missingKeyAlias)
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getByKeyID(keyID, missingKeyAlias);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        // Assume we only have one hit for keyID
        assertTrue(list.size() <= 1);

        return !list.isEmpty() ? list.get(0) : null;
    }

    private void addEmailAddresses(long keyID, String... emails)
    throws Exception
    {
        PGPKeyRingEntry entry = Objects.requireNonNull(getByKeyID(keyID, MissingKeyAlias.ALLOWED));

        Set<String> email = entry.getEmail();

        email.addAll(Arrays.asList(emails));

        entry.setEmail(email);
    }

    private Set<PGPKeyRingEntry> select(String email)
    throws Exception
    {
        PGPKeySelector selector = new PGPPublicKeySelectorImpl(keyRing,
                new EncryptionKeyValidator(new PGPKeyFlagCheckerImpl(new PGPSignatureValidatorImpl(),
                        new PGPUserIDValidatorImpl(new PGPSignatureValidatorImpl()))));

        return selector.select(email);
    }

    @Test
    public void testSelectEmail()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                for (PGPPublicKey key : keys)
                {
                    PGPKeyRingEntry entry = keyRing.addPGPPublicKey(key);

                    addEmailAddresses(entry.getKeyID(), "test@example.com");
                }

                Set<PGPKeyRingEntry> selected = select("test@EXAMPLE.com");

                assertEquals(2, selected.size());

                Set<String> keyIds = new HashSet<>();

                for (PGPKeyRingEntry key : selected) {
                    keyIds.add(PGPUtils.getKeyIDHex(key.getKeyID()));
                }

                assertTrue(keyIds.contains("2CF5B3CA80EF9A79"));
                assertTrue(keyIds.contains("916D59C0B1A63D43"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                for (PGPPublicKey key : keys)
                {
                    PGPKeyRingEntry entry = keyRing.addPGPPublicKey(key);

                    addEmailAddresses(entry.getKeyID(), "example.com");
                }

                Set<PGPKeyRingEntry> selected = select("test@EXAMPLE.com");

                assertEquals(2, selected.size());

                Set<String> keyIds = new HashSet<>();

                for (PGPKeyRingEntry key : selected) {
                    keyIds.add(PGPUtils.getKeyIDHex(key.getKeyID()));
                }

                assertTrue(keyIds.contains("2CF5B3CA80EF9A79"));
                assertTrue(keyIds.contains("916D59C0B1A63D43"));

                selected = select("exAmple.com");

                assertEquals(2, selected.size());

                keyIds = new HashSet<>();

                for (PGPKeyRingEntry key : selected) {
                    keyIds.add(PGPUtils.getKeyIDHex(key.getKeyID()));
                }

                assertTrue(keyIds.contains("2CF5B3CA80EF9A79"));
                assertTrue(keyIds.contains("916D59C0B1A63D43"));

                selected = select("xAmple.com");

                assertEquals(0, selected.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectDomainAndEmail()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                for (PGPPublicKey key : keys)
                {
                    PGPKeyRingEntry entry = keyRing.addPGPPublicKey(key);

                    /*
                     * Test whether it does not match duplicate keys
                     */
                    addEmailAddresses(entry.getKeyID(), "test@example.com");
                    addEmailAddresses(entry.getKeyID(), "example.com");
                }

                Set<PGPKeyRingEntry> selected = select("test@example.com");

                assertEquals(2, selected.size());

                Set<String> keyIds = new HashSet<>();

                for (PGPKeyRingEntry key : selected) {
                    keyIds.add(PGPUtils.getKeyIDHex(key.getKeyID()));
                }

                assertTrue(keyIds.contains("2CF5B3CA80EF9A79"));
                assertTrue(keyIds.contains("916D59C0B1A63D43"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectNoMatch()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                for (PGPPublicKey key : keys)
                {
                    PGPKeyRingEntry entry = keyRing.addPGPPublicKey(key);

                    addEmailAddresses(entry.getKeyID(), "test@example.com");
                }

                Set<PGPKeyRingEntry> selected = select("test2@example.com");

                assertEquals(0, selected.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectNoMatchNoEmailAddresses()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                for (PGPPublicKey key : keys) {
                    keyRing.addPGPPublicKey(key);
                }

                Set<PGPKeyRingEntry> selected = select("example.com");

                assertEquals(0, selected.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
