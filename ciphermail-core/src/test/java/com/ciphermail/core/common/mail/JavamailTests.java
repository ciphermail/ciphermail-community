/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/*
 * Test cases to test certain aspects of Javamail to learn how it actually works.
 */
public class JavamailTests
{
    @BeforeClass
    public static void setUpBeforeClass()
    {
        System.setProperty("mail.mime.parameters.strict", "false");
    }

    @Test
    public void testUnfolding()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/folded-header.eml");

        String header = message.getHeader("X-Folded-Header", ",");

        assertEquals("Test of a \r\n\tfolded header", header);

        String unfolded = MimeUtility.unfold(header);

        assertEquals("Test of a  folded header", unfolded);
    }

    @Test
    public void testFolding()
    throws Exception
    {
        String unfolded =
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa ";

        assertEquals(-1, unfolded.indexOf('\n'));

        String folded = MimeUtility.fold(0, unfolded);

        assertNotEquals(-1, folded.indexOf('\n'));
    }

    @Test
    public void testFoldingMessage()
    throws Exception
    {
        String unfolded =
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
               "aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa ";

        MimeMessage message = TestUtils.loadTestMessage("mail/folded-header.eml");

        message.setHeader("X-Test", unfolded);

        String mime = MailUtils.partToMimeString(message);

        assertThat(mime, containsString("X-Test: aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
                                        "aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
                                        "aaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaa " +
                                        "aaaaaaaaaaaaa"));

        MimeMessage savedMessage = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals(unfolded, savedMessage.getHeader("X-Test", ","));
    }

    @Test
    public void testNonASCII()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/chinese-encoded-from.eml");

        String header = message.getHeader("from", ",");

        // this decoding works
        String decoded = MimeUtility.decodeWord("=?big5?q?=AFQ=C0s=B8=E9@=BD=DE=C0Y=20j?=");

        assertEquals("烏龍賊@豬頭 j", decoded);

        // this decoding only works when the following system property is set
        // mail.mime.decodetext.strict=false.
        // the header is non-standard encoded so it only works when strict is off
        // see MimeUtility for more info
        decoded = MimeUtility.decodeText(header);
    }

    @Test
    public void testEncoding()
    throws Exception
    {
        String header = "test";

        header = MimeUtility.encodeText(header);

        assertEquals("test", header);

        header = MimeUtility.encodeText("烏龍賊@豬頭 j");

        assertEquals("烏龍賊@豬頭 j", MimeUtility.decodeText(header));
    }

    @Test
    public void testSetNullHeaderValue()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/folded-header.eml");

        assertNotNull(message.getSubject());

        message.setHeader("subject", null);

        assertEquals("null", message.getHeader("subject", ","));

        message.removeHeader("subject");
        assertNull(message.getHeader("subject"));
    }

    @Test
    @Ignore("test ignored because it requires the system property mail.mime.parameters.strict to be false")
    // Note: test ignored because it requires the system property mail.mime.parameters.strict to be false
    // setting this property only works when set for this test class but now when set on the main
    // AllTest class. Somehow the system property is not set on the forked java process during unit testing
    public void testAttachmentFilenameSpacesNotQuoted()
    throws Exception
    {
        // Apple sometimes adds a filename with spaces without quotes. Setting the system property
        // mail.mime.parameters.strict to true allows Javamail to read the filename correctly (workaround)
        MimeMessage message = TestUtils.loadTestMessage("mail/message-attach-filename-with-no-quotes.eml");

        String filename = message.getFileName();

        assertEquals("city info.zip", filename);
    }
}
