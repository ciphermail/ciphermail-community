/*
 * Copyright (c) 2015, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certificate.impl.StandardSerialNumberGenerator;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SelfSignedSMIMEKeyAndCertificateIssuerTest
{
    @Test
    public void testIssueSelfSignedCertificate()
    throws Exception
    {
        RequestParameters request = new RequestParametersImpl();

        X500PrincipalBuilder subjectBuilder = X500PrincipalBuilder.getInstance();

        subjectBuilder.setCommonName("testCN");
        subjectBuilder.setEmail("subject@example.com");

        request.setEmail("test@example.com");
        request.setKeyLength(2048);
        request.setSignatureAlgorithm("SHA256WithRSAEncryption");
        request.setValidity(3650);
        request.setSubject(subjectBuilder.buildPrincipal());

        KeyAndCertificateIssuer issuer = new SelfSignedSMIMEKeyAndCertificateIssuer(new StandardSerialNumberGenerator());

        KeyAndCertificate issued = issuer.issueKeyAndCertificate(request, null);

        assertNotNull(issued);
        assertNotNull(issued.getCertificate());
        assertNotNull(issued.getPrivateKey());

        X509CertificateInspector inspector = new X509CertificateInspector(issued.getCertificate());

        assertEquals(2, inspector.getEmail().size());
        assertTrue(inspector.getEmail().contains("test@example.com"));
        assertTrue(inspector.getEmail().contains("subject@example.com"));

        assertEquals(1, inspector.getEmailFromDN().size());
        assertEquals(1, inspector.getEmailFromAltNames().size());

        assertEquals("subject@example.com", StringUtils.join(inspector.getEmailFromDN(), ","));
        assertEquals("test@example.com", StringUtils.join(inspector.getEmailFromAltNames(), ","));
        assertEquals(issued.getCertificate().getSubjectX500Principal(),
                issued.getCertificate().getIssuerX500Principal());
        issued.getCertificate().verify(issued.getCertificate().getPublicKey());

        assertNotNull(issued.getCertificate().getEncoded());
    }
}
