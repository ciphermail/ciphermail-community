/*
 * Copyright (c) 2011-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


public class FreshestCertificateComparatorTest
{
    private final static File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testCompare()
    {
        X509Certificate may_18_2006 = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/dod-mega-crl.cer"));

        X509Certificate nov_1_2007 = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/valid_certificate_mitm_test_ca.cer"));

        assertTrue(new FreshestCertificateComparator().compare(may_18_2006, nov_1_2007) < 0);
        assertTrue(new FreshestCertificateComparator().compare(nov_1_2007, may_18_2006) > 0);
        assertEquals(0, new FreshestCertificateComparator().compare(nov_1_2007, nov_1_2007));
        assertEquals(0, new FreshestCertificateComparator().compare(may_18_2006, may_18_2006));
        assertTrue(new FreshestCertificateComparator().compare(null, may_18_2006) < 0);
        assertTrue(new FreshestCertificateComparator().compare(may_18_2006, null) > 0);
        assertEquals(0, new FreshestCertificateComparator().compare(null, null));
    }

    @Test
    public void testSort()
    {
        List<X509Certificate> certs = new LinkedList<X509Certificate>();

        X509Certificate may_18_2006 = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/dod-mega-crl.cer"));

        X509Certificate nov_1_2007 = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/valid_certificate_mitm_test_ca.cer"));

        certs.add(nov_1_2007);
        certs.add(may_18_2006);
        certs.add(null);

        assertEquals(nov_1_2007, certs.get(0));
        assertEquals(may_18_2006, certs.get(1));
        assertNull(certs.get(2));

        certs.sort(new FreshestCertificateComparator());

        assertNull(certs.get(0));
        assertEquals(may_18_2006, certs.get(1));
        assertEquals(nov_1_2007, certs.get(2));
    }
}
