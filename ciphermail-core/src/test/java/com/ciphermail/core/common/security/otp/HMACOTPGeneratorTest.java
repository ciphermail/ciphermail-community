/*
 * Copyright (c) 2011-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.otp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

class HMACOTPGeneratorTest
{
    @Test
    void testGenerateOTP()
    throws Exception
    {
        OTPGenerator generator = new HMACOTPGenerator("HmacSHA1");

        String result = generator.generate(new byte[]{1, 2}, new byte[]{3,4}, 100);

        Assertions.assertNotNull(result);

        String result2 = generator.generate(new byte[]{1, 2}, new byte[]{3,4}, 200);

        Assertions.assertEquals(result, result2);

        String result3 = generator.generate(new byte[]{1}, new byte[]{3,4}, 100);

        Assertions.assertNotEquals(result, result3);

        result2 = generator.generate(new byte[]{1, 2}, new byte[]{3,4}, 32);

        Assertions.assertEquals(result, result2);
    }

    @Test
    void testGenerateOTPCheckValue()
    throws Exception
    {
        OTPGenerator generator = new HMACOTPGenerator("HmacSHA256");

        String result = generator.generate(new byte[]{1, 2}, new byte[]{3,4}, 100);

        Assertions.assertEquals("clenctrhwib7gbzqnqgdbtok72nvbn7ynelvxmwyrke5pqd7xlza", result);
    }

    @Test
    void testGenerateOTPASCII()
    throws Exception
    {
        OTPGenerator generator = new HMACOTPGenerator("HmacSHA256");

        String result = generator.generate("secret1".getBytes(StandardCharsets.US_ASCII),
                "3608011".getBytes(StandardCharsets.US_ASCII), 8);

        // The hex value was determined with a HmacSHA256 JavaScript library
        Assertions.assertEquals("datg6q54oqrt4", result);
    }
}
