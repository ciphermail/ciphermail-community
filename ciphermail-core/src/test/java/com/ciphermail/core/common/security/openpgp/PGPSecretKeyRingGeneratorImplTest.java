/*
 * Copyright (c) 2014-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.apache.commons.collections4.IteratorUtils;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import org.bouncycastle.bcpg.ECDHPublicBCPGKey;
import org.bouncycastle.bcpg.ECDSAPublicBCPGKey;
import org.bouncycastle.bcpg.PublicKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

class PGPSecretKeyRingGeneratorImplTest
{
    void testGenerateRSA(PGPKeyGenerationType type, int expectedStrength)
    throws Exception
    {
        PGPSecretKeyRingGeneratorImpl generator = new PGPSecretKeyRingGeneratorImpl();

        PrivateKeysAndPublicKeyRing keyRing = generator.generateKeyRing(
                type, "test key <test@example.com>");

        PGPPublicKeyRing publicKeyRing = keyRing.getPublicKeyRing();

        Object[] publicKeys = IteratorUtils.toArray(publicKeyRing.getPublicKeys());

        System.out.println(publicKeys.getClass());

        Assertions.assertEquals(2, publicKeys.length);

        PGPPublicKey key = (PGPPublicKey) publicKeys[0];
        Assertions.assertEquals(PublicKeyAlgorithmTags.RSA_GENERAL, key.getAlgorithm());
        Assertions.assertEquals(expectedStrength, key.getBitStrength());

        PGPPrivateKey privateKey = keyRing.getPrivateKey(key);
        Assertions.assertNotNull(privateKey);
        Assertions.assertEquals(PublicKeyAlgorithmTags.RSA_GENERAL, privateKey.getPublicKeyPacket().getAlgorithm());

        key = (PGPPublicKey) publicKeys[1];
        Assertions.assertEquals(PublicKeyAlgorithmTags.RSA_GENERAL, key.getAlgorithm());
        Assertions.assertEquals(expectedStrength, key.getBitStrength());

        privateKey = keyRing.getPrivateKey(key);
        Assertions.assertNotNull(privateKey);
        Assertions.assertEquals(PublicKeyAlgorithmTags.RSA_GENERAL, privateKey.getPublicKeyPacket().getAlgorithm());
    }

    @Test
    void testGenerateRSA()
    throws Exception
    {
        testGenerateRSA(PGPKeyGenerationType.RSA_1024, 1024);
        testGenerateRSA(PGPKeyGenerationType.RSA_2048, 2048);
        testGenerateRSA(PGPKeyGenerationType.RSA_3072, 3072);
        testGenerateRSA(PGPKeyGenerationType.RSA_4096, 4096);
    }

    public void testGenerateECCKeys(PGPKeyGenerationType type, int expectedStrength, String expectedName)
    throws Exception
    {
        PGPSecretKeyRingGeneratorImpl generator = new PGPSecretKeyRingGeneratorImpl();

        PrivateKeysAndPublicKeyRing keyRing = generator.generateKeyRing(
                type, "test key <test@example.com>");

        PGPPublicKeyRing publicKeyRing = keyRing.getPublicKeyRing();

        Object[] publicKeys = IteratorUtils.toArray(publicKeyRing.getPublicKeys());

        Assertions.assertEquals(2, publicKeys.length);

        PGPPublicKey key = (PGPPublicKey) publicKeys[0];
        Assertions.assertEquals(PublicKeyAlgorithmTags.ECDSA, key.getAlgorithm());
        Assertions.assertEquals(expectedStrength, key.getBitStrength());

        ECDSAPublicBCPGKey dsaECKey = (ECDSAPublicBCPGKey) key.getPublicKeyPacket().getKey();
        Assertions.assertEquals(expectedName, ECNamedCurveTable.getName(dsaECKey.getCurveOID()));

        PGPPrivateKey privateKey = keyRing.getPrivateKey(key);
        Assertions.assertNotNull(privateKey);
        Assertions.assertEquals(PublicKeyAlgorithmTags.ECDSA, privateKey.getPublicKeyPacket().getAlgorithm());

        key = (PGPPublicKey) publicKeys[1];
        Assertions.assertEquals(PublicKeyAlgorithmTags.ECDH, key.getAlgorithm());
        Assertions.assertEquals(expectedStrength, key.getBitStrength());

        ECDHPublicBCPGKey dhECKey = (ECDHPublicBCPGKey) key.getPublicKeyPacket().getKey();
        Assertions.assertEquals(expectedName, ECNamedCurveTable.getName(dhECKey.getCurveOID()));

        privateKey = keyRing.getPrivateKey(key);
        Assertions.assertNotNull(privateKey);
        Assertions.assertEquals(PublicKeyAlgorithmTags.ECDH, privateKey.getPublicKeyPacket().getAlgorithm());
    }

    @Test
    void testGenerateNISTECC()
    throws Exception
    {
        testGenerateECCKeys(PGPKeyGenerationType.ECC_NIST_P_256, 256, "prime256v1");
        testGenerateECCKeys(PGPKeyGenerationType.ECC_NIST_P_384, 384, "secp384r1");
        testGenerateECCKeys(PGPKeyGenerationType.ECC_NIST_P_521, 521, "secp521r1");
    }

    @Test
    void testGenerateBrainpool()
    throws Exception
    {
        testGenerateECCKeys(PGPKeyGenerationType.ECC_BRAINPOOL_P_256, 256, "brainpoolP256r1");
        testGenerateECCKeys(PGPKeyGenerationType.ECC_BRAINPOOL_P_384, 384, "brainpoolP384r1");
        testGenerateECCKeys(PGPKeyGenerationType.ECC_BRAINPOOL_P_512, 512, "brainpoolP512r1");
    }

    @Test
    void generateHSMSafeKeyRing()
    throws Exception
    {
        PGPSecretKeyRingGeneratorImpl generator = new PGPSecretKeyRingGeneratorImpl();

        PrivateKeysAndPublicKeyRing privateKeysAndPublicKeyRing = generator.generateKeyRing(
                PGPKeyGenerationType.RSA_2048, "test key <test@example.com>");

        Iterator<PGPPublicKey> it = privateKeysAndPublicKeyRing.getPublicKeyRing().getPublicKeys();

        int i = 0;

        while (it.hasNext())
        {
            PGPPublicKey publicKey = it.next();

            Assertions.assertNotNull(publicKey);

            i++;

            PGPPrivateKey privateKey = privateKeysAndPublicKeyRing.getPrivateKey(publicKey);

            Assertions.assertNotNull(privateKey);
        }

        Assertions.assertEquals(2, i);
    }
}
