/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

public class X500PrincipalInspectorTest
{
    public static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testUnicode()
    {
        File file = new File(TEST_BASE, "certificates/chinesechars.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        X500Principal subject = Objects.requireNonNull(certificate).getSubjectX500Principal();

        X500PrincipalInspector inspector = new X500PrincipalInspector(subject);

        List<String> cn = inspector.getCommonName();

        assertEquals("[刘书洪]", cn.toString());
    }

    @Test
    public void testGetMultipleEmail()
    {
        // Subject:
        // EMAILADDRESS=antoinecailliau@netcourrier.com, EMAILADDRESS=antoine@media-box.net,
        // EMAILADDRESS=antoine.cailliau@student.uclouvain.be, EMAILADDRESS=antoinecailliau@gmail.com,
        // EMAILADDRESS=a.cailliau@ac-graphic.net, CN=Antoine Cailliau, GIVENNAME=Antoine, SURNAME=Cailliau
        File file = new File(TEST_BASE, "certificates/multipleemail.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        X500Principal subject = Objects.requireNonNull(certificate).getSubjectX500Principal();

        X500PrincipalInspector inspector = new X500PrincipalInspector(subject);

        List<String> emails = inspector.getEmail();
        List<String> organisation = inspector.getOrganisation();
        List<String> organisationalUnit = inspector.getOrganisationalUnit();
        List<String> countryCode = inspector.getCountryCode();
        List<String> locality = inspector.getLocality();
        List<String> cn = inspector.getCommonName();
        List<String> givenName = inspector.getGivenName();
        List<String> surname = inspector.getSurname();

        assertEquals(
                "[a.cailliau@ac-graphic.net, antoinecailliau@gmail.com, " +
                "antoine.cailliau@student.uclouvain.be, antoine@media-box.net, " +
                "antoinecailliau@netcourrier.com]", emails.toString());

        assertEquals("[]", organisation.toString());
        assertEquals("[]", organisationalUnit.toString());
        assertEquals("[]", countryCode.toString());
        assertEquals("[]", locality.toString());
        assertEquals("[Antoine Cailliau]", cn.toString());
        assertEquals("[Antoine]", givenName.toString());
        assertEquals("[Cailliau]", surname.toString());
    }

    @Test
    public void testInspector()
    {
        // Subject:
        // CN=KEENER.PAUL.BRENDAN.1114986220, OU=USMC, OU=PKI, OU=DoD, O=U.S. Government, C=US

        File file = new File(TEST_BASE, "certificates/dod-mega-crl.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        X500Principal subject = Objects.requireNonNull(certificate).getSubjectX500Principal();

        X500PrincipalInspector inspector = new X500PrincipalInspector(subject);

        List<String> emails = inspector.getEmail();
        List<String> organisation = inspector.getOrganisation();
        List<String> organisationalUnit = inspector.getOrganisationalUnit();
        List<String> countryCode = inspector.getCountryCode();
        List<String> state = inspector.getState();
        List<String> locality = inspector.getLocality();
        List<String> cn = inspector.getCommonName();
        List<String> givenName = inspector.getGivenName();
        List<String> surname = inspector.getSurname();

        assertEquals("[]", emails.toString());
        assertEquals("[U.S. Government]", organisation.toString());
        assertEquals("[DoD, PKI, USMC]", organisationalUnit.toString());
        assertEquals("[US]", countryCode.toString());
        assertEquals("[]", state.toString());
        assertEquals("[]", locality.toString());
        assertEquals("[KEENER.PAUL.BRENDAN.1114986220]", cn.toString());
        assertEquals("[]", givenName.toString());
        assertEquals("[]", surname.toString());
    }

    @Test
    public void testState()
    {
        // CN=Intel Corporation Basic Enterprise Issuing CA 1, OU=Information Technology Enterprise Business Computing,
        //      O=Intel Corporation, L=Folsom, ST=CA, C=US, EMAILADDRESS=pki@intel.com

        File file = new File(TEST_BASE, "certificates/intel-corp-basic.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        X500Principal issuer = Objects.requireNonNull(certificate).getIssuerX500Principal();

        X500PrincipalInspector inspector = new X500PrincipalInspector(issuer);

        List<String> emails = inspector.getEmail();
        List<String> organisation = inspector.getOrganisation();
        List<String> organisationalUnit = inspector.getOrganisationalUnit();
        List<String> countryCode = inspector.getCountryCode();
        List<String> state = inspector.getState();
        List<String> locality = inspector.getLocality();
        List<String> cn = inspector.getCommonName();
        List<String> givenName = inspector.getGivenName();
        List<String> surname = inspector.getSurname();

        assertEquals("[pki@intel.com]", emails.toString());
        assertEquals("[Intel Corporation]", organisation.toString());
        assertEquals("[Information Technology Enterprise Business Computing]", organisationalUnit.toString());
        assertEquals("[US]", countryCode.toString());
        assertEquals("[CA]", state.toString());
        assertEquals("[Folsom]", locality.toString());
        assertEquals("[Intel Corporation Basic Enterprise Issuing CA 1]", cn.toString());
        assertEquals("[]", givenName.toString());
        assertEquals("[]", surname.toString());
    }
}
