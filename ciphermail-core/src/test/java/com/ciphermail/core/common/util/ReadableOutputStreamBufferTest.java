/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ReadableOutputStreamBufferTest
{
    private static int tempFileCount;

    @Before
    public void before() {
        // get the current nr of temp files
        tempFileCount = TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp");
    }

    @After
    public void after()
    throws IOException
    {
        // check if we have any temp file leakage
        assertEquals(tempFileCount, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));
    }

    private void testReadWrite(int threshold)
    throws IOException
    {
        ReadableOutputStreamBuffer buffer = new ReadableOutputStreamBuffer(threshold);

        byte[] b = new byte[1024];

        for (int i = 0; i < b.length; i++)
        {
            b[i] = (byte) i;

            buffer.write((byte)i);
        }

        buffer.write(b);

        InputStream input = buffer.getInputStream();

        if (threshold <= b.length * 2)
        {
            assertTrue(input instanceof BufferedInputStream);

            // there should be an extra temp file
            assertEquals(tempFileCount + 1, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));
        }
        else {
            assertTrue(input instanceof ByteArrayInputStream);
        }

        assertEquals(2 * b.length, input.available());

        byte[] br = IOUtils.toByteArray(input);

        assertEquals(2 * b.length, br.length);

        for (int i = 0; i < br.length; i++) {
            assertEquals(b[i % b.length], br[i]);
        }

        buffer.close();
    }

    @Test
    public void testReadWriteMem()
    throws IOException
    {
        testReadWrite(SizeUtils.MB);
    }

    @Test
    public void testReadWriteDisk()
    throws IOException
    {
        testReadWrite(0);
    }

    private void testWriteAfterGettingInput(int threshold)
    throws IOException
    {
        ReadableOutputStreamBuffer buffer = new ReadableOutputStreamBuffer(threshold);

        byte[] b = new byte[1024];

        assertEquals(tempFileCount, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));

        buffer.write(b);

        if (threshold <= b.length)
        {
            // there should be an extra temp file
            assertEquals(tempFileCount + 1, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));
        }

        buffer.getInputStream();

        try {
            buffer.write(b);

            fail("IOException was excpected");
        }
        catch(IOException e) {
            // expected
        }

        buffer.close();
    }

    @Test
    public void testWriteAfterGettingInputMem()
    throws IOException
    {
        testWriteAfterGettingInput(SizeUtils.MB);
    }

    @Test
    public void testWriteAfterGettingInputDisk()
    throws IOException
    {
        testWriteAfterGettingInput(0);
    }
}
