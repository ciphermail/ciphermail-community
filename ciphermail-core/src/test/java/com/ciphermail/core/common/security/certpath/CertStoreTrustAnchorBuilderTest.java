/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certpath;

import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CertStoreTrustAnchorBuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                rootStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void importCertificate(String filename, X509CertStoreExt certStore)
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/" + filename);

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    @Test
    public void testInitialBuild()
    {
        // The root certificate must be imported in a separate transaction because the trust anchor builder uses
        // a stateless session to get all certs and because of that the imported cert is not yet available
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                importCertificate("rim.cer", rootStore);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(rootStore,
                        10 * DateUtils.MILLIS_PER_SECOND);

                Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                assertEquals(1, trustAnchors.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEmptyStore()
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(rootStore, 10 * DateUtils.MILLIS_PER_SECOND);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                assertEquals(0, trustAnchors.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddCertificateSeperateTransactions()
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(rootStore, 0L);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                assertEquals(0, trustAnchors.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // The root certificate must be imported in a separate transaction because the trust anchor builder uses
        // a stateless session to get all certs and because of that the imported cert is not yet available
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                importCertificate("rim.cer", rootStore);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                assertEquals(1, trustAnchors.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddCertificateOneTransaction()
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(rootStore, 0L);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, rootStore.size());

                Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                assertEquals(0, trustAnchors.size());

                importCertificate("rim.cer", rootStore);

                assertEquals(1, rootStore.size());

                trustAnchors = builder.getTrustAnchors();

                assertEquals(1, trustAnchors.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddExtraCertificate()
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(rootStore, 0L);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File file = new File(TEST_BASE, "certificates/gmail-ssl.cer");

                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

                builder.addCertificates(certificates);

                Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                assertEquals(1, trustAnchors.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // The root certificate must be imported in a separate transaction because the trust anchor builder uses
        // a stateless session to get all certs and because of that the imported cert is not yet available
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                importCertificate("rim.cer", rootStore);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Set<TrustAnchor>  trustAnchors = builder.getTrustAnchors();

                assertEquals(2, trustAnchors.size());

                File file = new File(TEST_BASE, "certificates/chinesechars.cer");

                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

                builder.addCertificates(certificates);

                trustAnchors = builder.getTrustAnchors();

                assertEquals(3, trustAnchors.size());

            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUnmodifiableCollection()
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(rootStore, 0L);

        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                    trustAnchors.add(null);
                }
                catch (Exception e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch (UnhandledException e) {
            assertTrue(e.getCause() instanceof UnsupportedOperationException);
        }
    }

    @Test
    public void testNoChange()
    {
        TrustAnchorBuilder builder = new CertStoreTrustAnchorBuilder(rootStore, 999999 * DateUtils.MILLIS_PER_SECOND);

        // The root certificate must be imported in a separate transaction because the trust anchor builder uses
        // a stateless session to get all certs and because of that the imported cert is not yet available
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                importCertificate("rim.cer", rootStore);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Set<TrustAnchor> trustAnchors1 = builder.getTrustAnchors();
                Set<TrustAnchor> trustAnchors2 = builder.getTrustAnchors();

                assertSame(trustAnchors1, trustAnchors2);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
