/*
 * Copyright (c) 2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import com.ciphermail.core.common.security.certificate.CertificateUtils;

public class KeyAndCertificateWithAdditonalCertsImplTest
{
    private PrivateKey privateKey;
    private X509Certificate certificate;
    private Collection<X509Certificate> additionalCertificates;

    @Before
    public void setup()
    throws Exception
    {
        KeyStore keyStore = KeyStore.getInstance("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream("src/test/resources/testdata/keys/testCertificates.p12"), "test".toCharArray());

        certificate = (X509Certificate) keyStore.getCertificate("validcertificate");
        privateKey = (PrivateKey) keyStore.getKey("validcertificate", "test".toCharArray());
        additionalCertificates = CertificateUtils.readX509Certificates(
                new File("src/test/resources/testdata/certificates/testCertificates.p7b"));
    }

    @Test
    public void testKeyAndCertificate()
    {
        KeyAndCertificateWithAdditonalCerts keyAndCerts = new KeyAndCertificateWithAdditonalCertsImpl(privateKey,
                certificate);

        assertEquals(privateKey, keyAndCerts.getPrivateKey());
        assertEquals(certificate, keyAndCerts.getCertificate());
        assertNotNull(keyAndCerts.getAdditionalCertificates());
        assertEquals(0, keyAndCerts.getAdditionalCertificates().size());
    }

    @Test
    public void testKeyAndCertificateWithAdditonalCerts()
    {
        KeyAndCertificateWithAdditonalCerts keyAndCerts = new KeyAndCertificateWithAdditonalCertsImpl(privateKey,
                certificate, additionalCertificates);

        assertEquals(privateKey, keyAndCerts.getPrivateKey());
        assertEquals(certificate, keyAndCerts.getCertificate());
        assertEquals(20, keyAndCerts.getAdditionalCertificates().size());
    }
}
