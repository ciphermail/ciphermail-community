/*
 * Copyright (c) 2009-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ctl;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CTLImplTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private CTLManager ctlManager;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete ALL entries from ALL CTLs
                CTLDAO.deleteAllEntries(SessionAdapterFactory.create(sessionManager.getSession()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddEntry()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL("global");

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/testcertificate.cer"));

                CTLEntry entry = ctl.createEntry(certificate);

                ctl.addEntry(entry);

                CTLEntry added = ctl.getEntry(certificate);

                assertEquals(entry, added);
                assertEquals(1, ctl.size());

                // default is white listed
                assertEquals(CTLEntryStatus.WHITELISTED, added.getStatus());

                // default is false
                assertFalse(added.isAllowExpired());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddDuplicateEntry()
    {
        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    CTL ctl = ctlManager.getCTL("global");

                    X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                            "certificates/testcertificate.cer"));

                    CTLEntry entry = ctl.createEntry(certificate);

                    ctl.addEntry(entry);

                    CTLEntry duplicate = ctl.createEntry(certificate);

                    ctl.addEntry(duplicate);

                    assertEquals(1, ctl.size());
                }
                catch (Exception e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch (UnhandledException e) {
            assertEquals(1, ExceptionUtils.indexOfThrowable(e, ConstraintViolationException.class));
        }
    }

    @Test
    public void testAddSameCertDifferentCTL()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl1 = ctlManager.getCTL("global1");
                CTL ctl2 = ctlManager.getCTL("global2");

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/testcertificate.cer"));

                CTLEntry entry1 = ctl1.createEntry(certificate);

                ctl1.addEntry(entry1);

                CTLEntry entry2 = ctl2.createEntry(certificate);

                ctl2.addEntry(entry2);

                assertEquals(1, ctl1.size());
                assertEquals(1, ctl2.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDeleteAll()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl1 = ctlManager.getCTL("global1");
                CTL ctl2 = ctlManager.getCTL("global2");

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/testcertificate.cer"));

                CTLEntry entry1 = ctl1.createEntry(certificate);

                ctl1.addEntry(entry1);

                CTLEntry entry2 = ctl2.createEntry(certificate);

                ctl2.addEntry(entry2);

                assertEquals(1, ctl1.size());
                assertEquals(1, ctl2.size());

                ctl1.deleteAll();

                assertEquals(0, ctl1.size());
                assertEquals(1, ctl2.size());
                assertNull(ctl1.getEntry(certificate));
                assertEquals(entry2, ctl2.getEntry(certificate));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddMulitpleEntries()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL("global");

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/testcertificate.cer"));

                CTLEntry entry1 = ctl.createEntry(certificate);

                ctl.addEntry(entry1);

                CTLEntry entry2 = ctl.createEntry("abc");

                ctl.addEntry(entry2);

                CTLEntry entry3 = ctl.createEntry("def");

                ctl.addEntry(entry3);

                assertEquals(3, ctl.size());
                assertEquals(entry1, ctl.getEntry(certificate));
                assertEquals(entry2, ctl.getEntry("abc"));
                assertEquals(entry3, ctl.getEntry("def"));

                List<? extends CTLEntry> entries = ctl.getEntries(0, Integer.MAX_VALUE);

                assertEquals(3, entries.size());
                assertTrue(entries.contains(entry1));
                assertTrue(entries.contains(entry2));
                assertTrue(entries.contains(entry3));

                entries = ctl.getEntries(0, 1);
                assertEquals(1, entries.size());
                assertTrue(entries.contains(entry1));

                entries = ctl.getEntries(1, 1);
                assertEquals(1, entries.size());
                assertTrue(entries.contains(entry2));

                entries = ctl.getEntries(10, 1);
                assertEquals(0, entries.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateWhiteListed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL("global");

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/valid_certificate_mitm_test_ca.cer"));

                CTLEntry entry = ctl.createEntry(certificate);

                entry.setStatus(CTLEntryStatus.WHITELISTED);
                entry.setAllowExpired(false);

                ctl.addEntry(entry);

                assertEquals(CTLEntryStatus.WHITELISTED, ctl.getEntry(certificate).getStatus());

                CTLValidityResult result = ctl.checkValidity(certificate);

                assertEquals(CTLValidity.VALID, result.getValidity());
                assertEquals(CTLImpl.WHITELISTED_MESSAGE, result.getMessage());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateWhiteListedButExpired()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL("global");

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/thawte-freemail-valid-till-091108.cer"));

                CTLEntry entry = ctl.createEntry(certificate);

                entry.setStatus(CTLEntryStatus.WHITELISTED);
                entry.setAllowExpired(false);

                ctl.addEntry(entry);

                assertEquals(CTLEntryStatus.WHITELISTED, ctl.getEntry(certificate).getStatus());

                CTLValidityResult result = ctl.checkValidity(certificate);

                assertEquals(CTLValidity.INVALID, result.getValidity());
                assertEquals(CTLImpl.EXPIRED_MESSAGE, result.getMessage());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateWhiteListedExpiredButAllowed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL("global");

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/thawte-freemail-valid-till-091108.cer"));

                CTLEntry entry = ctl.createEntry(certificate);

                entry.setStatus(CTLEntryStatus.WHITELISTED);
                entry.setAllowExpired(true);

                ctl.addEntry(entry);

                assertEquals(CTLEntryStatus.WHITELISTED, ctl.getEntry(certificate).getStatus());

                CTLValidityResult result = ctl.checkValidity(certificate);

                assertEquals(CTLValidity.VALID, result.getValidity());
                assertEquals(CTLImpl.WHITELISTED_MESSAGE, result.getMessage());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateBlackListed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL("global");

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/valid_certificate_mitm_test_ca.cer"));

                CTLEntry entry = ctl.createEntry(certificate);

                entry.setStatus(CTLEntryStatus.BLACKLISTED);

                ctl.addEntry(entry);

                assertEquals(CTLEntryStatus.BLACKLISTED, ctl.getEntry(certificate).getStatus());

                CTLValidityResult result = ctl.checkValidity(certificate);

                assertEquals(CTLValidity.INVALID, result.getValidity());
                assertEquals(CTLImpl.BLACKLISTED_MESSAGE, result.getMessage());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateNotListed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL("global");

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/valid_certificate_mitm_test_ca.cer"));

                CTLValidityResult result = ctl.checkValidity(certificate);

                assertEquals(CTLValidity.NOT_LISTED, result.getValidity());
                assertEquals(CTLImpl.NOT_LISTED_MESSAGE, result.getMessage());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateNullCertificate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            CTL ctl = ctlManager.getCTL("global");

            NullPointerException e = assertThrows(NullPointerException.class, () ->
                    ctl.checkValidity(null));
        });
    }
}
