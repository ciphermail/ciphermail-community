/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.impl.StandardSerialNumberGenerator;
import com.ciphermail.core.common.util.FixedWidthReader;
import org.apache.commons.codec.binary.Hex;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import static org.junit.Assert.assertNotNull;

/**
 * Test class used to create public/private key pairs and write out their hex
 * representations. We will need this for setting up test cases because we do
 * not want to generate new key pairs for some tests (tests need to be deterministic)
 *
 * @author Martijn Brinkers
 *
 */
public class GenerateKeyPairsTest
{
    private static final int WIDTH = 60;

    private static KeyPairGenerator keyPairGenerator1024;
    private static KeyPairGenerator keyPairGenerator2048;
    private static StandardSerialNumberGenerator serialNumberGenerator;

    @BeforeClass
    public static void setUpBeforeClass()
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();
        SecureRandom randomSource = securityFactory.createSecureRandom();

        keyPairGenerator1024 = securityFactory.createKeyPairGenerator("RSA");
        keyPairGenerator1024.initialize(1024, randomSource);

        keyPairGenerator2048 = securityFactory.createKeyPairGenerator("RSA");
        keyPairGenerator2048.initialize(2048, randomSource);

        serialNumberGenerator = new StandardSerialNumberGenerator();
    }

    private String bigIntToString(BigInteger bigInt)
    throws IOException
    {
        return FixedWidthReader.fixedWidth(bigInt.toString(16), WIDTH);
    }

    private String bytesToHex(byte[] bytes)
    throws IOException
    {
        String hex = String.copyValueOf(Hex.encodeHex(bytes));

        return FixedWidthReader.fixedWidth(hex, WIDTH);
    }

    private void writeKeyPair(KeyPair keyPair)
    throws IOException
    {
        System.out.println("Keypair:");

        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();

        System.out.println("Modulus:");
        System.out.println(bigIntToString(privateKey.getModulus()));

        System.out.println();
        System.out.println("Private exponent:");
        System.out.println(bigIntToString(privateKey.getPrivateExponent()));

        System.out.println();
        System.out.println("Public exponent:");
        System.out.println(bigIntToString(publicKey.getPublicExponent()));

        System.out.println();
        System.out.println("Encoded public key:");
        System.out.println(bytesToHex(keyPair.getPublic().getEncoded()));

        System.out.println();
        System.out.println("Encoded private key:");
        System.out.println(bytesToHex(keyPair.getPrivate().getEncoded()));

        System.out.println();
        System.out.println("Serial number:");
        System.out.println(bigIntToString(serialNumberGenerator.generate()));
    }


    @Test
    public void generateKeyPair1024()
    throws IOException
    {
        KeyPair keyPair = keyPairGenerator1024.generateKeyPair();

        System.out.println("Keypair 1024 bits");

        writeKeyPair(keyPair);

        System.out.println();

        assertNotNull(keyPair.getPrivate());
        assertNotNull(keyPair.getPublic());
    }

    @Test
    public void generateKeyPair2048()
    throws IOException
    {
        KeyPair keyPair = keyPairGenerator2048.generateKeyPair();

        System.out.println("Keypair 2048 bits");

        writeKeyPair(keyPair);

        System.out.println();

        assertNotNull(keyPair.getPrivate());
        assertNotNull(keyPair.getPublic());
    }
}
