/*
 * Copyright (c) 2008-2016, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PhoneNumberUtilsTest
{
    @Test
    public void testGetPhoneNumber()
    {
        assertEquals("+311234567", PhoneNumberUtils.normalizeAndValidatePhoneNumber("311234567"));
        assertEquals("+311234567", PhoneNumberUtils.normalizeAndValidatePhoneNumber("+311234567"));
        assertEquals("+311234567", PhoneNumberUtils.normalizeAndValidatePhoneNumber("   +311234567  "));
        assertEquals("+311234567", PhoneNumberUtils.normalizeAndValidatePhoneNumber("   +31  1234567  "));
    }

    @Test
    public void testGetPhoneNumberNoMatch()
    {
        assertNull(PhoneNumberUtils.normalizeAndValidatePhoneNumber("a123"));
        assertNull(PhoneNumberUtils.normalizeAndValidatePhoneNumber("  123a"));
        assertNull(PhoneNumberUtils.normalizeAndValidatePhoneNumber("++123445555"));
        assertNull(PhoneNumberUtils.normalizeAndValidatePhoneNumber("00123445555"));
        assertNull(PhoneNumberUtils.normalizeAndValidatePhoneNumber("1234455550000000000000000000008888888"));
    }

    @Test
    public void testIsValidPhoneNumber()
    {
        assertTrue(PhoneNumberUtils.isValidPhoneNumber("311234567"));
        assertTrue(PhoneNumberUtils.isValidPhoneNumber("+311234567"));
        assertTrue(PhoneNumberUtils.isValidPhoneNumber("   +311234567  "));
        assertTrue(PhoneNumberUtils.isValidPhoneNumber("   +31  1234567  "));

        assertFalse(PhoneNumberUtils.isValidPhoneNumber("a123"));
        assertFalse(PhoneNumberUtils.isValidPhoneNumber("  123a"));
        assertFalse(PhoneNumberUtils.isValidPhoneNumber("++123445555"));
        assertFalse(PhoneNumberUtils.isValidPhoneNumber("00123445555"));
        assertFalse(PhoneNumberUtils.isValidPhoneNumber("1234455550000000000000000000008888888"));
    }

    @Test
    public void testAddCountryCode()
    {
        assertEquals("316123456", PhoneNumberUtils.addCountryCode("06123456", "31"));
        assertEquals("6123456", PhoneNumberUtils.addCountryCode("6123456", "31"));
        assertEquals("06123456", PhoneNumberUtils.addCountryCode("06123456", null));
        assertEquals("31", PhoneNumberUtils.addCountryCode("0", "31"));
    }

    @Test
    public void testMaskPhoneNumber()
    {
        assertEquals("#######890", PhoneNumberUtils.maskPhoneNumber("1234567890", 3));
        assertEquals("##########", PhoneNumberUtils.maskPhoneNumber("1234567890", 0));
        assertEquals("1234567890", PhoneNumberUtils.maskPhoneNumber("1234567890", 10));
        assertEquals("12", PhoneNumberUtils.maskPhoneNumber("12", 3));
        assertEquals("", PhoneNumberUtils.maskPhoneNumber("", 3));
        assertNull(PhoneNumberUtils.maskPhoneNumber(null, 3));
    }
}
