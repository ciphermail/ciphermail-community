/*
 * Copyright (c) 2021-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.keyserver;

import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HTTPClientProxyProvider;
import com.ciphermail.core.common.http.HTTPClientStaticProxyProvider;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPPublicKeyRingCollection;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class HKPKeyServerClientImplTest
{
    private static final File testBase = new File(TestUtils.getTestDataDir(), "pgp/");

    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        HKPKeyServerClientSettings settings = new HKPKeyServerClientSettingsImpl(TestProperties.getPGPKeyserverURL1());

        client.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(testBase, "martijn@djigzo.com_0x271AD23B.asc")))));

        client.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(testBase, "0xb91671444a3c41eb.asc")))));

        client.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(testBase, "0x4ec4e8813e11a9a0.asc")))));

        client.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(testBase, "0x94cee89f0c345bcc.asc")))));

        client.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(testBase, "0xd382c0ec9e8ca3cf.asc")))));

        client.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(testBase, "0xd382c0ec9e8ca3cf.asc")))));

        client.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(testBase, "0xec4b033c70096ad1.asc")))));

        client.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(testBase, "0x37e1c17570096ad1.asc")))));
    }

    @Before
    public void before()
    {
        // Clear interrupted status
        Thread.interrupted();
    }

    private static HKPKeyServerClientImpl createHKPKeyServerClient() {
        return createHKPKeyServerClient(null, "test", "test");
    }

    private static HKPKeyServerClientImpl createHKPKeyServerClient(HTTPClientProxyProvider proxyProvider,
            String username, String password)
    {
        CloseableHttpAsyncClientFactoryImpl httpClientFactory = new CloseableHttpAsyncClientFactoryImpl(proxyProvider);

        BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();

        credentialsProvider.setCredentials(new AuthScope(null, -1),
                new UsernamePasswordCredentials(username, password.toCharArray()));

        HttpClientContextFactoryImpl httpClientContextFactory = new HttpClientContextFactoryImpl(
                credentialsProvider);

        return new HKPKeyServerClientImpl(httpClientFactory, httpClientContextFactory);
    }

    @Test
    public void testSearchWithKeyID()
    throws Exception
    {
        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverURL1());

            List<KeyServerKeyInfo> results = client.searchKeys(clientSettings, "0x271AD23B", true);

            assertEquals(1, results.size());

            KeyServerKeyInfo keyInfo = results.get(0);

            assertEquals("DC368B248911C140EF6564764605970C271AD23B", keyInfo.getKeyID());
            assertEquals("RSA", keyInfo.getAlgorithm().toString());
            assertEquals(2048, keyInfo.getKeyLength());
            assertEquals("Thu Oct 03 22:29:58 CEST 2013", keyInfo.getCreationDate().toString());
            assertEquals("Wed Oct 04 17:32:40 CEST 2023", keyInfo.getExpirationDate().toString());
            assertNull(keyInfo.getFlags());
            assertEquals(2, keyInfo.getUserIDs().size());
            // Some key server escape html
            assertEquals("Martijn Brinkers <martijn@djigzo.com>", StringEscapeUtils.unescapeHtml(
                    keyInfo.getUserIDs().get(0)));
            assertEquals("Martijn Brinkers <martijn@ciphermail.com>", StringEscapeUtils.unescapeHtml(
                    keyInfo.getUserIDs().get(1)));
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testSearchWithFingerprint()
    throws Exception
    {
        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverURL1());

            List<KeyServerKeyInfo> results = client.searchKeys(clientSettings,
                    "0xDC368B248911C140EF6564764605970C271AD23B", true);

            assertEquals(1, results.size());

            KeyServerKeyInfo keyInfo = results.get(0);

            assertEquals("DC368B248911C140EF6564764605970C271AD23B", keyInfo.getKeyID());
            assertEquals("RSA", keyInfo.getAlgorithm().toString());
            assertEquals(2048, keyInfo.getKeyLength());
            assertEquals("Thu Oct 03 22:29:58 CEST 2013", keyInfo.getCreationDate().toString());
            assertEquals("Wed Oct 04 17:32:40 CEST 2023", keyInfo.getExpirationDate().toString());
            assertNull(keyInfo.getFlags());
            assertEquals(2, keyInfo.getUserIDs().size());
            // Some key server escape html
            assertEquals("Martijn Brinkers <martijn@djigzo.com>", StringEscapeUtils.unescapeHtml(
                    keyInfo.getUserIDs().get(0)));
            assertEquals("Martijn Brinkers <martijn@ciphermail.com>", StringEscapeUtils.unescapeHtml(
                    keyInfo.getUserIDs().get(1)));
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testUTF8()
    throws Exception
    {
        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverURL1());

            List<KeyServerKeyInfo> results = client.searchKeys(clientSettings, "0x9E8CA3CF", false);

            assertEquals(1, results.size());

            KeyServerKeyInfo keyInfo = results.get(0);

            assertEquals("BAA0273749B5CCE25D32E2ADD382C0EC9E8CA3CF", keyInfo.getKeyID());
            assertEquals("DSA", keyInfo.getAlgorithm().toString());
            assertEquals(1024, keyInfo.getKeyLength());
            assertEquals("Wed Mar 16 03:42:12 CET 2005", keyInfo.getCreationDate().toString());
            assertEquals("Sat Mar 15 03:42:12 CET 2008", keyInfo.getExpirationDate().toString());
            assertNull(keyInfo.getFlags());
            assertEquals(3, keyInfo.getUserIDs().size());
            // Some key server escape html
            assertEquals("Jörg Behr <joerg80@gmx.net>", StringEscapeUtils.unescapeHtml(
                    keyInfo.getUserIDs().get(0)));
            assertEquals("Jörg Behr <jbehr@physnet.uni-hamburg.de>", StringEscapeUtils.unescapeHtml(
                    keyInfo.getUserIDs().get(1)));
            assertEquals("Jörg Behr <joerg.behr@physnet.uni-hamburg.de>", StringEscapeUtils.unescapeHtml(
                    keyInfo.getUserIDs().get(2)));
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testErrorMessage()
    throws Exception
    {
        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverURL1());

            try {
                client.searchKeys(clientSettings, "non-existing-key", false);
            }
            catch (IOException e) {
                assertThat(e.getMessage(), containsString("No results found: No keys found (404 Not found)"));
            }
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testGetKeys()
    throws Exception
    {
        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverURL1());

            PGPPublicKeyRingCollection keyRingCollection = client.getKeys(clientSettings, "0x271AD23B");

            assertNotNull(keyRingCollection);
            assertEquals(1, keyRingCollection.size());

            PGPPublicKeyRing keyRing = keyRingCollection.getKeyRings().next();

            assertEquals("271AD23B", PGPUtils.getShortKeyIDHex(keyRing.getPublicKey().getKeyID()));
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testGetKeysPerformance()
    throws Exception
    {
        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverURL1());

            int repeat = 5000;

            long start = System.currentTimeMillis();

            for (int i = 0; i < repeat; i++)
            {
                System.out.println(i);

                PGPPublicKeyRingCollection keyRingCollection = client.getKeys(clientSettings, "0x271AD23B");

                assertNotNull(keyRingCollection);
                assertEquals(1, keyRingCollection.size());
            }

            long diff = System.currentTimeMillis() - start;

            double perSecond = repeat * 1000.0 / diff;

            System.out.println("downloads/sec: " + perSecond);

            double expected = 100 * TestProperties.getTestPerformanceFactor();

            // NOTE: !!! can fail on a slower system
            assertTrue("Requests per second too slow. Expected > " + expected + " but was " + perSecond +
                    " !!! this can fail on a slower system !!!", perSecond > expected);
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testSearchWithMaxSet()
    throws Exception
    {
        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverURL1());

            List<KeyServerKeyInfo> results = client.searchKeys(clientSettings, "djigzo.com", false);

            assertEquals(4, results.size());

            results = client.searchKeys(clientSettings, "djigzo.com", false, 1);

            assertEquals(1, results.size());
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testGetKeysShortKeyCollision()
    throws Exception
    {
        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverURL1());

            PGPPublicKeyRingCollection keyRingCollection = client.getKeys(clientSettings, "0x70096AD1");

            assertNotNull(keyRingCollection);
            assertEquals(2, keyRingCollection.size());
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testSearchKeysTimeout()
    throws Exception
    {
        String localIP = "127.0.0.3";

        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            client.setTotalTimeout(2000);

            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl("http://" + localIP + ":11371");

            ServerSocket serverSocket = new ServerSocket();

            // Create a socket which does nothing on 127.0.0.3:11371 so we can test the timeouts
            serverSocket.bind(new InetSocketAddress(localIP, 11371));

            try {
                client.searchKeys(clientSettings, "something", false);

                fail();
            }
            catch (IOException e) {
                assertEquals("A timeout has occurred connecting to: http://127.0.0.3:11371/pks/lookup", e.getMessage());
            }
            finally {
                serverSocket.close();
            }
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testGetKeysTimeout()
    throws Exception
    {
        String localIP = "127.0.0.3";

        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            client.setTotalTimeout(2000);

            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl("http://" + localIP + ":11371");

            ServerSocket serverSocket = new ServerSocket();

            // Create a socket which does nothing on 127.0.0.3:11371 so we can test the timeouts
            serverSocket.bind(new InetSocketAddress(localIP, 11371));

            try {
                client.getKeys(clientSettings, "something");

                fail();
            }
            catch (IOException e) {
                assertEquals("A timeout has occurred connecting to: http://127.0.0.3:11371/pks/lookup", e.getMessage());
            }
            finally {
                serverSocket.close();
            }
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testSubmitKeysTimeout()
    throws Exception
    {
        String localIP = "127.0.0.3";

        HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            client.setTotalTimeout(2000);

            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl("http://" + localIP + ":11371");

            ServerSocket serverSocket = new ServerSocket();

            // Create a socket which does nothing on 127.0.0.3:11371 so we can test the timeouts
            serverSocket.bind(new InetSocketAddress(localIP, 11371));

            try {
                client.submitKeys(clientSettings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                        new FileInputStream(new File(testBase, "0x94cee89f0c345bcc.asc")))));

                fail();
            }
            catch (IOException e) {
                assertEquals("A timeout has occurred connecting to: http://127.0.0.3:11371/pks/add", e.getMessage());
            }
            finally {
                serverSocket.close();
            }
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testSearchViaProxy()
    throws Exception
    {
        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        HKPKeyServerClientImpl client = createHKPKeyServerClient(proxyProvider, "test", "test");

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverProxyURL1());

            List<KeyServerKeyInfo> results = client.searchKeys(clientSettings, "0x271AD23B", true);

            assertEquals(1, results.size());
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testSearchViaProxyInvalidCredentials()
    throws Exception
    {
        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        HKPKeyServerClientImpl client = createHKPKeyServerClient(proxyProvider, "test", "invalid");

        try {
            HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverProxyURL1());

            try {
                client.searchKeys(clientSettings, "0x271AD23B", true);

                fail();
            }
            catch (IOException e) {
                assertTrue(e.getMessage(), e.getMessage().startsWith("Cache Access Denied."));
            }
        }
        finally {
            client.shutdown();
        }
    }

    @Test
    public void testSearchMultithreaded()
    throws Exception
    {
        final HKPKeyServerClientImpl client = createHKPKeyServerClient();

        try {
            final HKPKeyServerClientSettings clientSettings = new HKPKeyServerClientSettingsImpl(
                    TestProperties.getPGPKeyserverURL1());

            int nrOfDownloads = 10000;

            final AtomicInteger downloadCounter = new AtomicInteger();

            final CountDownLatch countDownLatch = new CountDownLatch(nrOfDownloads);

            final Thread mainThread = Thread.currentThread();

            ExecutorService executorService = Executors.newFixedThreadPool(10);

            try {
                for (int i = 0; i < nrOfDownloads; i++)
                {
                    executorService.execute(() ->
                    {
                        try {
                            System.out.println("Key : " + downloadCounter.incrementAndGet() + ". Thread: " +
                                    Thread.currentThread().getName());

                            PGPPublicKeyRingCollection keyRingCollection = client.getKeys(clientSettings,
                                    "0x271AD23B");

                            assertNotNull(keyRingCollection);
                            assertEquals(1, keyRingCollection.size());
                        }
                        catch (IOException | URISyntaxException e) {
                            // Stop main thread from waiting
                            mainThread.interrupt();

                            throw new UnhandledException(e);
                        }
                        finally {
                            countDownLatch.countDown();
                        }
                    });
                }

                assertTrue("Timeout", countDownLatch.await(30, TimeUnit.SECONDS));
                assertEquals(nrOfDownloads, downloadCounter.get());
            }
            finally {
                executorService.shutdown();
            }
        }
        finally {
            client.shutdown();
        }
    }
}
