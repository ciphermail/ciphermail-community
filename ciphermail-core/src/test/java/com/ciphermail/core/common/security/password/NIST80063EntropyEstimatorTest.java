/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password;

import static org.junit.Assert.*;

import org.junit.Test;

public class NIST80063EntropyEstimatorTest {

    @Test
    public void testEstimateEntropy()
    {
        assertEquals(0, NIST80063EntropyEstimator.estimateEntropy(null), 0);
        assertEquals(0, NIST80063EntropyEstimator.estimateEntropy(""), 0);
        assertEquals(10, NIST80063EntropyEstimator.estimateEntropy("test"), 0);
        assertEquals(12, NIST80063EntropyEstimator.estimateEntropy("testA"), 0);
        assertEquals(12, NIST80063EntropyEstimator.estimateEntropy("test^"), 0);
        assertEquals(14, NIST80063EntropyEstimator.estimateEntropy("testa^"), 0);
        assertEquals(20, NIST80063EntropyEstimator.estimateEntropy("testA^"), 0);
        assertEquals(16, NIST80063EntropyEstimator.estimateEntropy("0123456"), 0);
        assertEquals(18, NIST80063EntropyEstimator.estimateEntropy("01234567"), 0);
        assertEquals(19.5, NIST80063EntropyEstimator.estimateEntropy("012345678"), 0);
        assertEquals(21, NIST80063EntropyEstimator.estimateEntropy("0123456789"), 0);
        assertEquals(34.5, NIST80063EntropyEstimator.estimateEntropy("0123456789012345678"), 0);
        assertEquals(36, NIST80063EntropyEstimator.estimateEntropy("01234567890123456789"), 0);
        assertEquals(37, NIST80063EntropyEstimator.estimateEntropy("012345678901234567890"), 0);
        assertEquals(40, NIST80063EntropyEstimator.estimateEntropy("012345678901234567890123"), 0);
    }
}
