/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class XTextEncoderTest
{
    @Test
    public void testEncode()
    {
        String encoded = XTextEncoder.encode("test 1234");
        assertEquals("test+201234", encoded);

        encoded = XTextEncoder.encode("+test");
        assertEquals("+2Btest", encoded);

        encoded = XTextEncoder.encode("test+");
        assertEquals("test+2B", encoded);

        encoded = XTextEncoder.encode("x=x");
        assertEquals("x+3Dx", encoded);

        encoded = XTextEncoder.encode(null);
        assertNull(encoded);

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 128; i++) {
            sb.append((char)i);
        }

        encoded = XTextEncoder.encode(sb.toString());
        assertEquals("+00+01+02+03+04+05+06+07+08+09+0A+0B+0C+0D+0E+0F+10+11+12+13+14+15+16+17+18+19+1A+1B" +
                     "+1C+1D+1E+1F+20!\"#$%&'()*+2B,-./0123456789:;<+3D>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`" +
                     "abcdefghijklmnopqrstuvwxyz{|}~+7F", encoded);
    }

    @Test
    public void testDecode()
    {
        String decoded = XTextEncoder.decode("");
        assertEquals("", decoded);

        decoded = XTextEncoder.decode("test+201234");
        assertEquals("test 1234", decoded);

        decoded = XTextEncoder.decode("+2Btest");
        assertEquals("+test", decoded);

        decoded = XTextEncoder.decode("test+2B");
        assertEquals("test+", decoded);

        decoded = XTextEncoder.decode("aaa+3Daaa");
        assertEquals("aaa=aaa", decoded);

        decoded = XTextEncoder.decode(null);
        assertEquals(null, decoded);

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 128; i++) {
            sb.append((char)i);
        }

        decoded = XTextEncoder.decode ("+00+01+02+03+04+05+06+07+08+09+0A+0B+0C+0D+0E+0F+10+11+12+13+14+15+16+17+18+19+1A+1B" +
                     "+1C+1D+1E+1F+20!\"#$%&'()*+2B,-./0123456789:;<+3D>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`" +
                     "abcdefghijklmnopqrstuvwxyz{|}~+7F");

        assertEquals(sb.toString(), decoded);
    }

    @Test
    public void testDecodeInvalid()
    {
        String decoded = XTextEncoder.decode("test+");
        assertEquals("test+", decoded);
    }
}
