/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ResourceLocatorTest
{
    public static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testLoadPropertiesFromFile()
    throws IOException
    {
        ResourceLocator loader = new ResourceLocator();

        Properties properties = new Properties();

        properties.load(loader.getResourceAsStream(TEST_BASE + "/other/test.properties"));

        assertNotNull(properties);
    }

    @Test
    public void testLoadPropertiesSystemProperty()
    throws IOException
    {
        final String systemPropName = "testLoadPropertiesSystemProperty";

        ResourceLocator loader = new ResourceLocator();

        System.setProperty(systemPropName, TEST_BASE + "/other");

        loader.setSystemProperty(systemPropName);

        Properties properties = new Properties();

        properties.load(loader.getResourceAsStream("test.properties"));

        assertNotNull(properties);
        assertEquals("123", properties.getProperty("test"));
    }

    @Test
    public void testLoadPropertiesNonExistingSystemProperty()
    throws IOException
    {
        final String systemPropName = "XXX";

        ResourceLocator loader = new ResourceLocator();

        loader.setSystemProperty(systemPropName);

        Properties properties = new Properties();

        properties.load(loader.getResourceAsStream(TEST_BASE + "/other/test.properties"));

        assertNotNull(properties);
        assertEquals("123", properties.getProperty("test"));
    }

    @Test
    public void testLoadPropertiesBaseDir()
    throws IOException
    {
        ResourceLocator loader = new ResourceLocator();

        loader.setBaseDir(TEST_BASE + "/other");

        Properties properties = new Properties();

        properties.load(loader.getResourceAsStream("test.properties"));

        assertNotNull(properties);
    }

    @Test
    public void testLoadNonExistingProperties()
    throws FileNotFoundException
    {
        ResourceLocator loader = new ResourceLocator();


        InputStream input = loader.getResourceAsStream("xxx");

        assertNull(input);
    }

    /*
     * tries to load a property from a jar file. For this test to work javamail jar should be on the
     * classpath.
     */
    @Test
    public void testLoadPropertiesFromJar()
    throws IOException
    {
        ResourceLocator loader = new ResourceLocator();

        Properties properties = new Properties();

        properties.load(loader.getResourceAsStream("META-INF/javamail.default.providers"));

        assertNotNull(properties);
    }
}
