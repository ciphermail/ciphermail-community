/*
 * Copyright (c) 2009-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.template;

import com.ciphermail.core.test.TestUtils;
import freemarker.template.SimpleHash;
import freemarker.template.TemplateNotFoundException;
import org.apache.commons.lang.SystemUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class TemplateBuilderTest
{
    private static TemplateBuilder templateBuilder;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        templateBuilder = new TemplateBuilderImpl(new File(SystemUtils.USER_DIR), "/templates");
    }

    @Test
    public void testAbbreviate()
    throws TemplateBuilderException
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        properties.put("value", "qwerty");

        String result = templateBuilder.buildTemplate("${abbreviate(value, 5)}", properties);

        assertEquals("qw...", result);

        result = templateBuilder.buildTemplate("${abbreviate(unknown!\"\",5)}", properties);

        assertEquals("", result);
    }

    @Test(expected = TemplateBuilderException.class)
    public void testAbbreviateNotEnoughParams()
    throws TemplateBuilderException
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        properties.put("value", "qwerty");

        templateBuilder.buildTemplate("${abbreviate(value)}", properties);
    }

    @Test
    public void testAbbreviateNonStringValues()
    throws Exception
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        properties.put("number", 123456);
        properties.put("boolean", true);
        properties.put("date", TestUtils.parseDate("31-Aug-2017 07:38:35 GMT"));
        properties.put("null", null);

        assertEquals("12...", templateBuilder.buildTemplate("${abbreviate(number, 5)}", properties));
        assertEquals("...", templateBuilder.buildTemplate("${abbreviate(boolean, 3)}", properties));
        assertEquals("31-Aug-2017...", templateBuilder.buildTemplate("${abbreviate(date, 14)}", properties));
        assertEquals("", templateBuilder.buildTemplate("${abbreviate(null, 14)}", properties));
    }

    @Test
    public void testBasic()
    throws TemplateBuilderException
    {
        SimpleHash properties = new TemplateHashModelBuilderImpl().buildSimpleHash();

        properties.put("value", "123");

        String result = templateBuilder.buildTemplate("name=${value}", properties);

        assertEquals("name=123", result);
    }

    @Test(expected = TemplateBuilderException.class)
    public void testQuotedPrintableNotEnoughParams()
    throws TemplateBuilderException
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        templateBuilder.buildTemplate("${qp()}", properties);
    }

    @Test(expected = TemplateBuilderException.class)
    public void testQuotedPrintableTooManyParams()
    throws TemplateBuilderException
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        templateBuilder.buildTemplate("${qp(1,2,3)}", properties);
    }

    @Test
    public void testQuotedPrintable()
    throws Exception
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        properties.put("value", "=");
        properties.put("boolean", true);
        properties.put("date", TestUtils.parseDate("31-Aug-2017 07:38:35 GMT"));
        properties.put("null", null);

        assertEquals("=3D", templateBuilder.buildTemplate("${qp(value)}", properties));
        assertEquals("=3D", templateBuilder.buildTemplate("${qp(value, 'US-ASCII')}", properties));
        assertEquals("=C3=A4=C3=B6=C3=BC=C3=84=C3=96=C3=9C", templateBuilder.buildTemplate("${qp('äöüÄÖÜ', 'UTF-8')}",
                properties));
        assertEquals("??????", templateBuilder.buildTemplate("${qp('äöüÄÖÜ', 'US-ASCII')}", properties));
        assertEquals("true", templateBuilder.buildTemplate("${qp(boolean)}", properties));
        // The timezone is changed by Freemarker therefore we cannot just compare the strings directly
        assertEquals(TestUtils.parseDate("31-Aug-2017 07:38:35 GMT").getTime(),
                TestUtils.parseDate(templateBuilder.buildTemplate("${qp(date)}", properties)).getTime());
        assertEquals("", templateBuilder.buildTemplate("${qp(null)}", properties));
    }

    @Test
    public void testQuotedPrintableUnknownVar()
    throws TemplateBuilderException
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        assertEquals("", templateBuilder.buildTemplate("${qp(value!\"\")}", properties));
    }

    @Test
    public void testUnicode()
    throws TemplateBuilderException
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        properties.put("value", "äöüÄÖÜ");

        String result = templateBuilder.buildTemplate("${value} äöüÄÖÜ", properties);

        assertEquals("äöüÄÖÜ äöüÄÖÜ", result);
    }

    @Test
    public void testFormatString()
    throws TemplateBuilderException
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        properties.put("value", "test one %s, two %s");
        properties.put("otherValue", "test abc");

        assertEquals("test one 1, two 2", templateBuilder.buildTemplate("${format(value, '1', '2')}", properties));
        assertEquals("test one %s, two %s", templateBuilder.buildTemplate("${format(value, '1')}", properties));
        assertEquals("test one , two ", templateBuilder.buildTemplate("${format(value, b!, a!\"\")}", properties));
        assertEquals("test abc", templateBuilder.buildTemplate("${format(otherValue, '1', '2')}", properties));
        assertEquals("test one 1, two 2", templateBuilder.buildTemplate("${format(value, 1, 2)}", properties));
        assertEquals("test one true, two false", templateBuilder.buildTemplate("${format(value, true, false)}", properties));

        // String#format fails. The formatter method then returns the full format string
        assertEquals("test one %s, two %s", templateBuilder.buildTemplate("${format(value, '1')}", properties));
    }

    @Test
    public void testFormatStringNotEnoughParameters()
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        assertThrows(TemplateBuilderException.class, () ->
                templateBuilder.buildTemplate("${format()}", properties));
    }

    @Test
    public void testMimeEncodeHeader()
    throws Exception
    {
        SimpleHash properties = TemplateObjectsFactory.createSimpleHash();

        properties.put("noencodingneeded", "test123");
        properties.put("encodingneeded", "Fortroligt du må ikke læse denne email.");

        assertEquals("test123", templateBuilder.buildTemplate("${mimeEncodeHeader(noencodingneeded)}", properties));
        assertEquals("=?UTF-8?Q?Fortroligt_du_m=C3=A5_ikke_l=C3=A6se_denne_email.?=", templateBuilder.buildTemplate("${mimeEncodeHeader(encodingneeded)}", properties));
    }

    @Test
    public void testClasspathTemplate()
    throws IOException
    {
        assertNotNull(templateBuilder.getTemplateByName("macros.ftl"));
    }

    @Test
    public void testClasspathTemplateNotFound()
    {
        TemplateNotFoundException e = assertThrows(TemplateNotFoundException.class, () ->
                templateBuilder.getTemplateByName("../templates/macros.ftl"));

        assertTrue(e.getMessage(), e.getMessage().startsWith("Template not found for name \"../templates/macros.ftl\".\n" +
                     "Reason given: Backing out from the root directory is not allowed."));
    }
}