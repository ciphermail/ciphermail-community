/*
 * Copyright (c) 2012-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

public class ColorUtilsTest
{
    @Test
    public void testToColor()
    {
        Color color = ColorUtils.toColor("1,2,3");

        assertEquals(1, color.getRed());
        assertEquals(2, color.getGreen());
        assertEquals(3, color.getBlue());

        color = ColorUtils.toColor(" 01 , 000  ,200 ");

        assertEquals(1, color.getRed());
        assertEquals(0, color.getGreen());
        assertEquals(200, color.getBlue());
    }

    @Test
    public void testToColorFail()
    {
        try {
            ColorUtils.toColor("abc");
            fail("IllegalArgumentException was expected");
        }
        catch (IllegalArgumentException e) {
            // expected
        }

        try {
            ColorUtils.toColor("1,2,300");
            fail("IllegalArgumentException was expected");
        }
        catch (IllegalArgumentException e) {
            // expected
        }

        try {
            ColorUtils.toColor("1,2,");
            fail("IllegalArgumentException was expected");
        }
        catch (IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    public void testToColorEmpty()
    {
        assertNull(ColorUtils.toColor(null));
        assertNull(ColorUtils.toColor(""));
        assertNull(ColorUtils.toColor("  "));
    }

    @Test
    public void testToString()
    {
        assertEquals("1,2,3", ColorUtils.toString(new Color(1,2,3)));

        assertNull(ColorUtils.toString(null));
    }
}
