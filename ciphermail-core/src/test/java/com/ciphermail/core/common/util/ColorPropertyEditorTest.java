/*
 * Copyright (c) 2012-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Test;

public class ColorPropertyEditorTest
{
    @Test
    public void testSetAsText()
    {
        ColorPropertyEditor pe = new ColorPropertyEditor();

        pe.setAsText("1,2,3");

        Color color = (Color) pe.getValue();

        assertEquals(1, color.getRed());
        assertEquals(2, color.getGreen());
        assertEquals(3, color.getBlue());

        pe.setAsText(" 255 ,  255 ,  0    ");

        color = (Color) pe.getValue();

        assertEquals(255, color.getRed());
        assertEquals(255, color.getGreen());
        assertEquals(0, color.getBlue());

        pe.setAsText(" 001 ,  002,  003    ");

        color = (Color) pe.getValue();

        assertEquals(1, color.getRed());
        assertEquals(2, color.getGreen());
        assertEquals(3, color.getBlue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailNoMatch()
    {
        ColorPropertyEditor pe = new ColorPropertyEditor();

        pe.setAsText("1,2,x");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailOutOfBounds()
    {
        ColorPropertyEditor pe = new ColorPropertyEditor();

        pe.setAsText("1,2,256");
    }
}
