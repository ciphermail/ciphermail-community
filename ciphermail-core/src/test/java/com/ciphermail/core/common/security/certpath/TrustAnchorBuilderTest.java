/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certpath;

import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.certstore.jce.X509CertStoreParameters;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class TrustAnchorBuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt x509RootStore;

    private CertStore rootStore;

    @Before
    public void setup()
    throws Exception
    {
        X509CertStoreParameters rootStoreParams = new X509CertStoreParameters(x509RootStore);

        rootStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, rootStoreParams,
                CipherMailProvider.PROVIDER);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                rootStoreParams.getCertStore().removeAllEntries();

                importCertificate("windows-xp-all-roots.p7b", rootStoreParams.getCertStore());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void importCertificate(String filename, X509CertStoreExt certStore)
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/" + filename);

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    @Test
    public void testloadTrustAnchorsCollection()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                TrustAnchorBuilder builder = new SimpleTrustAnchorBuilder();

                X509CertSelector selector = new X509CertSelector();

                selector.setIssuer(new X500Principal(
                        "OU=VeriSign Trust Network, OU=\"(c) 1998 VeriSign, Inc. - For authorized use only\", " +
                        "OU=Class 1 Public Primary Certification Authority - G2, O=\"VeriSign, Inc.\", C=US"));

                Collection<? extends Certificate> certificates = rootStore.getCertificates(selector);

                builder.addCertificates(certificates);

                Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                assertEquals(2, trustAnchors.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testloadTrustAnchorsCollectionTiming()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                long start = System.currentTimeMillis();

                final int tries = 100;

                for (int i = 0; i < tries; i++)
                {
                    TrustAnchorBuilder builder = new SimpleTrustAnchorBuilder();

                    X509CertSelector selector = new X509CertSelector();

                    selector.setIssuer(new X500Principal(
                            "OU=VeriSign Trust Network, OU=\"(c) 1998 VeriSign, Inc. - For authorized use only\", " +
                            "OU=Class 1 Public Primary Certification Authority - G2, O=\"VeriSign, Inc.\", C=US"));

                    Collection<? extends Certificate> certificates = rootStore.getCertificates(selector);

                    builder.addCertificates(certificates);

                    Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                    assertEquals(2, trustAnchors.size());
                }

                double secondsPerBuild = (System.currentTimeMillis() - start) * 0.001 / tries;

                System.out.println("testloadTrustAnchorsCollectionTiming. Seconds / try: " + secondsPerBuild);

                double expected = 0.4;

                assertTrue("Too slow. Expected " + expected + " but got" + secondsPerBuild,
                        secondsPerBuild < expected);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testloadTrustAnchors()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                long start = System.currentTimeMillis();

                final int tries = 100;

                for (int i = 0; i < tries; i++)
                {
                    TrustAnchorBuilder builder = new SimpleTrustAnchorBuilder();

                    builder.addCertificates(rootStore);

                    Set<TrustAnchor> trustAnchors = builder.getTrustAnchors();

                    assertEquals(246, trustAnchors.size());
                }

                double secondsPerBuild = (System.currentTimeMillis() - start) * 0.001 / tries;

                System.out.println("testloadTrustAnchors. Seconds / try: " + secondsPerBuild);

                double expected = 0.4;

                assertTrue("Too slow. Expected " + expected + " but got" + secondsPerBuild,
                        secondsPerBuild < expected);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
