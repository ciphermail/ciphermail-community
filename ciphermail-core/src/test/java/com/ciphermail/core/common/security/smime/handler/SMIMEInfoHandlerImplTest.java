/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.handler;

import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.smime.SMIMEInspector;
import com.ciphermail.core.common.security.smime.SMIMEInspectorImpl;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SMIMEInfoHandlerImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore certStore;

    @Autowired
    private PKISecurityServices pkiSecurityServices;

    private static SecurityFactory securityFactory;

    @BeforeClass
    public static void setUpBeforeClass() {
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Clean key/root/crl store
                certStore.removeAllEntries();
                rootStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addTestCAs()
    throws Exception
    {
        addCertificates(new File(TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"), rootStore);
        addCertificates(new File(TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"), certStore);
    }

    private static void addCertificates(File file, X509CertStoreExt certStore)
    throws Exception
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        addCertificates(certificates, certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore)
    throws Exception
    {
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate && !certStore.contains((X509Certificate) certificate)) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    @Test
    public void testClearSigned()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCAs();

                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/clear-signed-validcertificate.eml"));

                SMIMEInspector inspector = new SMIMEInspectorImpl(message, certStore,
                        securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

                // add Certificates from CMS blob
                addCertificates(inspector.getSignedInspector().getCertificates(), certStore);

                message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 0, false);

                MailUtils.validateMessage(message);

                String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testClearSignedPathNotFound()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/clear-signed-validcertificate.eml"));

                SMIMEInspector inspector = new SMIMEInspectorImpl(message, certStore,
                        securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

                message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 123, false);

                MailUtils.validateMessage(message);

                String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-ID-0-123", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-Verified-0-123", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-Trusted-0-123", ","));
                assertEquals("False", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-Email-0-123", ","));
                assertEquals("test@example.com", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testClearSignedNoCertificates()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/signed-no-certificates.eml"));

                SMIMEInspector inspector = new SMIMEInspectorImpl(message, certStore,
                        securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

                message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 0, false);

                MailUtils.validateMessage(message);

                String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
                assertEquals("False", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader(
                        "X-CipherMail-Info-Signer-Verification-Info-0-0", ","));
                assertEquals("Signing certificate could not be found.", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testClearSignedNoCertificatesButInStore()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCAs();

                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/signed-no-certificates.eml"));

                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/valid_certificate_mitm_test_ca.cer"), certStore);

                SMIMEInspector inspector = new SMIMEInspectorImpl(message, certStore,
                        securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

                SMIMEInfoHandlerImpl handler = new SMIMEInfoHandlerImpl(pkiSecurityServices);

                handler.setAddCMSCertificates(true);

                message = handler.handle(message, inspector, 0, false);

                MailUtils.validateMessage(message);

                String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEnveloped()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCAs();

                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-validcertificate.eml"));

                SMIMEInspector inspector = new SMIMEInspectorImpl(message, certStore,
                        securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

                message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 1, false);

                MailUtils.validateMessage(message);

                String header = HeaderUtils.decodeHeaderValue(message.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-1", ","));
                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(message.getHeader(
                        "X-CipherMail-Info-Encryption-Recipient-0-1", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL/" +
                        "115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEnvelopedMultipleRecipients()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCAs();

                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypt-15-recipients.eml"));

                SMIMEInspector inspector = new SMIMEInspectorImpl(message, certStore,
                        securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

                message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 1, false);

                MailUtils.validateMessage(message);

                String header = HeaderUtils.decodeHeaderValue(message.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-1", ","));
                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                final int recipients = 15;

                for (int i = 0; i < recipients; i++)
                {
                    header = HeaderUtils.decodeHeaderValue(message.getHeader(
                            "X-CipherMail-Info-Encryption-Recipient-" + i + "-1",
                            ","));
                    assertNotNull(header);
                }

                header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Encryption-Recipient-" +
                        (recipients + 1) + "-1", ","));

                assertNull(header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testCompressed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCAs();

                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/compressed.eml"));

                SMIMEInspector inspector = new SMIMEInspectorImpl(message, certStore,
                        securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

                message = new SMIMEInfoHandlerImpl(pkiSecurityServices).handle(message, inspector, 1, false);

                MailUtils.validateMessage(message);

                String header = HeaderUtils.decodeHeaderValue(message.getHeader("X-CipherMail-Info-Compressed-1", ","));
                assertEquals("True", header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testMaxRecipients()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addTestCAs();

                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypt-15-recipients.eml"));

                SMIMEInspector inspector = new SMIMEInspectorImpl(message, certStore,
                        securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

                SMIMEInfoHandlerImpl handler = new SMIMEInfoHandlerImpl(pkiSecurityServices);

                int maxRecipients = 3;

                handler.setMaxRecipients(maxRecipients);

                message = handler.handle(message, inspector, 1, false);

                MailUtils.validateMessage(message);

                String header = HeaderUtils.decodeHeaderValue(message.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-1", ","));
                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                for (int i = 0; i < maxRecipients; i++)
                {
                    header = HeaderUtils.decodeHeaderValue(message.getHeader(
                            "X-CipherMail-Info-Encryption-Recipient-" + i + "-1",
                            ","));
                    assertNotNull(header);
                }

                for (int i = maxRecipients; i < 15; i++)
                {
                    header = HeaderUtils.decodeHeaderValue(message.getHeader(
                            "X-CipherMail-Info-Encryption-Recipient-" + i + "-1",
                            ","));
                    assertNull(header);
                }

                assertNull(header);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
