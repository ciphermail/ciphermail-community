/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.pdf;

import com.ciphermail.core.common.security.SecurityFactoryFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

class PDFURLBuilderTest
{
    @Test
    void testHttpURLBuilder()
    throws Exception
    {
        PDFURLBuilder builder = new PDFURLBuilder();

        builder.setBaseURL("http://example.com/");

        builder.addParameter("param", "one");

        String url = builder.buildURL();

        Assertions.assertEquals("http://example.com?param=one", url);
    }

    @Test
    void testHttpURLBuilderSpaceInName()
    throws Exception
    {
        PDFURLBuilder builder = new PDFURLBuilder();

        builder.setBaseURL("http://example.com/");

        builder.addParameter("param ", "one");

        String url = builder.buildURL();

        Assertions.assertEquals("http://example.com?param=one", url);
    }

    @Test
    void testHttpURLBuilderSpace()
    throws Exception
    {
        PDFURLBuilder builder = new PDFURLBuilder();

        builder.setBaseURL("http://example.com/");

        builder.addParameter("param", "one two");

        String url = builder.buildURL();

        Assertions.assertEquals("http://example.com?param=one+two", url);
    }

    @Test
    void testHttpURLBuilderNoParameters()
    throws Exception
    {
        PDFURLBuilder builder = new PDFURLBuilder();

        builder.setBaseURL("http://example.com/");

        String url = builder.buildURL();

        Assertions.assertEquals("http://example.com", url);
    }

    @Test
    void testHttpURLBuilderHMac()
    throws Exception
    {
        PDFURLBuilder builder = new PDFURLBuilder();

        builder.setBaseURL("http://example.com");

        builder.addParameter("param1", "one");
        builder.addParameter("param2", "two");
        builder.addParameter("param3", "*&/%?");

        Mac mac = SecurityFactoryFactory.getSecurityFactory().createMAC("HmacSHA256");

        Assertions.assertNotNull(mac);

        SecretKeySpec key = new SecretKeySpec(new byte[]{1, 2, 3}, "hmac");

        mac.init(key);

        builder.addHMAC("mac", mac);

        String url = builder.buildURL();

        Assertions.assertEquals("http://example.com?param1=one&param2=two&param3=*%26%2F%25%3F&mac=xi3htpn4j3efs3msgl4avqopuxjqp6vz2l4dt4sfqj75moiqzmpa", url);
    }

    @Test
    void testHttpURLBuilderBuildQuery()
    throws Exception
    {
        PDFURLBuilder builder = new PDFURLBuilder();

        builder.setBaseURL("http://example.com");

        builder.addParameter("param1", "one");
        builder.addParameter("param2", "two");
        builder.addParameter("param3", "*&/%?");

        Mac mac = SecurityFactoryFactory.getSecurityFactory().createMAC("HmacSHA256");

        Assertions.assertNotNull(mac);

        SecretKeySpec key = new SecretKeySpec(new byte[]{1, 2, 3}, "hmac");

        mac.init(key);

        builder.addHMAC("mac", mac);

        String query = builder.buildQueury();

        Assertions.assertEquals("param1=one&param2=two&param3=*%26%2F%25%3F&mac=xi3htpn4j3efs3msgl4avqopuxjqp6vz2l4dt4sfqj75moiqzmpa", query);
    }
}
