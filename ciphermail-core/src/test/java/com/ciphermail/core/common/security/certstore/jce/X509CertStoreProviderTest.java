/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore.jce;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import com.ciphermail.core.common.security.cms.SignerIdentifier;
import com.ciphermail.core.common.security.cms.SignerIdentifierImpl;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class X509CertStoreProviderTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    private CertStore certStore;
    private X509CertStoreParameters certStoreParams;
    private X509CertStoreParameters caCertStoreParams;
    private KeyStore keyStore;

    @Before
    public void setup()
    throws Exception
    {
        certStoreParams = new X509CertStoreParameters(new X509CertStoreExtHibernate("certificates", sessionManager));

        certStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, certStoreParams,
                CipherMailProvider.PROVIDER);

        caCertStoreParams = new X509CertStoreParameters(new X509CertStoreExtHibernate("ca", sessionManager));

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStoreParams.getCertStore().removeAllEntries();
                caCertStoreParams.getCertStore().removeAllEntries();

                keyStore = loadKeyStore(new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12"), "test");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");
                assertNotNull(certificate);
                caCertStoreParams.getCertStore().addCertificate(certificate);

                certificate = (X509Certificate) keyStore.getCertificate("ca");
                assertNotNull(certificate);
                caCertStoreParams.getCertStore().addCertificate(certificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private KeyStore loadKeyStore(File file, String password)
    throws Exception
    {
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());

        Enumeration<String> aliases = keyStore.aliases();

        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();

            Certificate certificate = keyStore.getCertificate(alias);

            if (!(certificate instanceof X509Certificate)) {
                // only X509Certificates are supported
                continue;
            }

            boolean hasPrivateKey = keyStore.isKeyEntry(alias) &&
                    (keyStore.getKey(alias, null) instanceof PrivateKey);

            alias = hasPrivateKey ? alias : null;

            certStoreParams.getCertStore().addCertificate((X509Certificate) certificate, alias);
        }

        return keyStore;
    }

    @Test
    public void testGetCertificatesIssuerSerial()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                selector.setIssuer(certificate.getIssuerX500Principal());
                selector.setSerialNumber(certificate.getSerialNumber());

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesSignerIdentifier()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                SignerIdentifier identifier = new SignerIdentifierImpl(certificate);

                CertSelector selector = identifier.getSelector();

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesSubjectKeyIdentifier()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                SignerIdentifier identifier = new SignerIdentifierImpl(null, null,
                        X509CertificateInspector.getSubjectKeyIdentifier(certificate));

                CertSelector selector = identifier.getSelector();

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesUnoptimized()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                X509CertSelector selector = new X509CertSelector();

                selector.setKeyUsage(certificate.getKeyUsage());

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(17, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesNoMatch()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(new BigInteger("123", 10));

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(0, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesValidOnly()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setCertificateValid(new Date());

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(21, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesByCertificate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                selector.setCertificate(certificate);

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());

                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesNoRestriction()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(22, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesSerial()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCAC409FB2022B7D06920A00FE42"));

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesIssuer()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                // should find root and ca certificate
                assertEquals(2, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesSubject()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setSubject(new X500Principal(
                        "EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                // should find root
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesEmailAltNameUsingSelector()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.addSubjectAlternativeName(1, "TEST3@exAmple.com");

                Collection<? extends Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
