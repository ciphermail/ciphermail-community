/*
 * Copyright (c) 2012-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.GeneralName;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


public class X509ExtensionInspectorTest
{
    public static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testSubjectKeyIdentifier()
    {
        X509Certificate certificate = Objects.requireNonNull(TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/subjectkeyident.cer")));

        assertEquals("256E864B45E603AF649B577F5CDD63B0850A513E",
                HexUtils.hexEncode(X509ExtensionInspector.getSubjectKeyIdentifier(certificate)));

        assertEquals("256E864B45E603AF649B577F5CDD63B0850A513E",
                X509ExtensionInspector.getSubjectKeyIdentifierHex(certificate));

        // Now with no SKI
        certificate = Objects.requireNonNull(TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/outlook2010_cert_missing_subjkeyid.pem")));

        assertNull(HexUtils.hexEncode(X509ExtensionInspector.getSubjectKeyIdentifier(certificate)));
        assertNull(X509ExtensionInspector.getSubjectKeyIdentifierHex(certificate));
    }

    @Test
    public void testAuthoritykeyIdentifier()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/mitm-test-ca.cer"));

        AuthorityKeyIdentifier authorityKeyIdentifier = X509CertificateInspector.getAuthorityKeyIdentifier(certificate);

        assertNotNull(authorityKeyIdentifier);
        assertEquals(1, authorityKeyIdentifier.getAuthorityCertIssuer().getNames().length);

        GeneralName name = authorityKeyIdentifier.getAuthorityCertIssuer().getNames()[0];
        assertEquals(GeneralName.directoryName, name.getTagNo());
        assertEquals("C=NL,ST=NH,L=Amsterdam,CN=MITM Test Root,E=root@example.com", X500Name.getInstance(
                name.getName()).toString());
        assertEquals("115FCAC409FB2022B7D06920A00FE42",
                BigIntegerUtils.hexEncode(authorityKeyIdentifier.getAuthorityCertSerialNumber()));

        // another cert
        certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/ldap-crl.cer"));

        authorityKeyIdentifier = X509CertificateInspector.getAuthorityKeyIdentifier(certificate);

        assertNotNull(authorityKeyIdentifier);
        assertEquals("37509F5DEF72162D12C7D46C408B1F65F550A8F9", HexUtils.hexEncode(
                authorityKeyIdentifier.getKeyIdentifier()));
    }

    @Test
    public void testBasicConstraint()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/mitm-test-ca.cer"));

        BasicConstraints constraints = X509CertificateInspector.getBasicConstraints(certificate);

        assertTrue(constraints.isCA());
        assertNull(constraints.getPathLenConstraint());

        certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/ldap-crl.cer"));

        assertNull(X509CertificateInspector.getBasicConstraints(certificate));
    }

}
