/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.repository;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.repository.hibernate.MailRepositoryImpl;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.ThreadUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class MailRepositoryMaintainerTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "mail/");

    private static final String DEFAULT_REPOSITORY = "test-quarantine";

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    private static MimeMessage message;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        message = MailUtils.loadMessage(new File(TEST_BASE, "html-alternative.eml"));
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                getRepository(DEFAULT_REPOSITORY).deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private static class MailRepositoryEventListenerTester implements MailRepositoryEventListener
    {
        List<MailRepositoryItem> expired = new LinkedList<>();

        @Override
        public void onDeleted(String repository, MailRepositoryItem item) {
            fail();
        }

        @Override
        public void onReleased(String repository, MailRepositoryItem item) {
            fail();
        }

        @Override
        public void onExpired(String repository, MailRepositoryItem item) {
            expired.add(item);
        }
    }

    private MailRepository getRepository(String repositoryName) {
        return new MailRepositoryImpl(sessionManager, repositoryName);
    }

    private void addItemWithDate(String repositoryName, MimeMessage message, Date created)
    {
        transactionOperations.execute(status ->
        {
            try {
                MailRepository repository = getRepository(repositoryName);

                MailRepositoryItem item = ((MailRepositoryImpl) repository).createItem(message, created);

                repository.addItem(item);

                return item;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private long getItemCount(String repositoryName)
    {
        return Optional.ofNullable(transactionOperations.execute(status ->
        {
            try {
                return getRepository(repositoryName).getItemCount();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        })).orElse(0L);
    }

    @Test
    public void testExpire()
    throws Exception
    {
        Date expiredDate = DateUtils.addDays(new Date(), -6);

        addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
        addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
        addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
        addItemWithDate(DEFAULT_REPOSITORY, message, new Date());

        assertEquals(4, getItemCount(DEFAULT_REPOSITORY));

        MailRepositoryEventListenerTester listener = new MailRepositoryEventListenerTester();

        MailRepositoryMaintainer maintainer = new MailRepositoryMaintainer(Collections.singleton(
            getRepository(DEFAULT_REPOSITORY)), listener, transactionOperations);

        maintainer.setSettleTime(0);
        maintainer.setSleepTime(10);
        maintainer.start();

        try {
            ThreadUtils.sleep(Duration.of(2, ChronoUnit.SECONDS));

            assertEquals(1, getItemCount(DEFAULT_REPOSITORY));

            assertEquals(3, listener.expired.size());
        }
        finally {
            maintainer.stop();
        }
    }

    @Test
    public void testBatchSize()
    throws Exception
    {
        Date expiredDate = DateUtils.addDays(new Date(), -6);

        int nrToAdd = 500;

        for (int i = 0; i < nrToAdd; i++)
        {
            addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
            addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
            addItemWithDate(DEFAULT_REPOSITORY, message, expiredDate);
            addItemWithDate(DEFAULT_REPOSITORY, message, new Date());
        }

        assertEquals(nrToAdd * 4L, getItemCount(DEFAULT_REPOSITORY));

        MailRepositoryEventListenerTester listener = new MailRepositoryEventListenerTester();

        MailRepositoryMaintainer maintainer = new MailRepositoryMaintainer(Collections.singleton(
            getRepository(DEFAULT_REPOSITORY)), listener, transactionOperations);

        maintainer.setSettleTime(0);
        maintainer.setSleepTime(10);

        try {
            maintainer.start();

            do {
                ThreadUtils.sleep(Duration.of(1, ChronoUnit.SECONDS));
            }
            while (getItemCount(DEFAULT_REPOSITORY) != nrToAdd);

            ThreadUtils.sleep(Duration.of(2, ChronoUnit.SECONDS));

            assertEquals(nrToAdd, getItemCount(DEFAULT_REPOSITORY));

            assertEquals(nrToAdd * 3L, listener.expired.size());
        }
        finally {
            maintainer.stop();
        }
    }
}
