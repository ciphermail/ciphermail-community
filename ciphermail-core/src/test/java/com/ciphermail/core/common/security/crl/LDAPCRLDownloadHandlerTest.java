/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.SimpleSocketServer;
import com.ciphermail.core.test.SocketAcceptEvent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.security.cert.CRL;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class LDAPCRLDownloadHandlerTest
{
    private static final int SERVER_PORT = 61111;

    private static SimpleSocketServer server;

    private static class RunawaySocketHandler implements Runnable
    {
        @Override
        public void run() {
            ThreadUtils.sleepQuietly(Integer.MAX_VALUE);
        }
    }

    @BeforeClass
    public static void setUpBeforeClass() {
        startServer();
    }

    @AfterClass
    public static void setUpAfterClass()
    {
        server.stop();
    }

    private static void startServer()
    {
        server = new SimpleSocketServer(SERVER_PORT);

        Thread serverThread = new Thread(server);
        serverThread.setDaemon(true);

        serverThread.start();

        while(!server.isRunning()) {
            ThreadUtils.sleepQuietly(100);
        }
    }

    @Test(expected = IOException.class)
    public void testLDAPCRLDownloadHandlerTimeoutTest()
    throws Exception
    {
        SocketAcceptEvent event = socket ->
        {
            Runnable runnable = new RunawaySocketHandler();
            new Thread(runnable).start();
        };

        server.setIncomingEvent(event);

        LDAPCRLDownloadHandler handler = new LDAPCRLDownloadHandler();

        handler.setTotalTimeout(1000);

        URI uri = new URI("ldap://127.0.0.1:" + SERVER_PORT);

        Collection<? extends CRL> crls = handler.downloadCRLs(uri);

        assertEquals(0, crls.size());
    }

    /*
     * Note: This test sometimes fails because of connection problems
     *
     * TODO replace with LDAP to localhost docker
     */
    @Test
    public void testLDAPCRLDownloadHandler()
    throws Exception
    {
        CRLDownloadHandler handler = new LDAPCRLDownloadHandler();

        // alternative
        URI uri = new URI("ldap://directory.d-trust.net/CN=D-TRUST%20Test%20CA%202003%201:PN,O=D-Trust%20GmbH,C=DE");

        Collection<? extends CRL> crls = handler.downloadCRLs(uri);

        assertEquals(1, crls.size());
    }

    @Test(expected = IOException.class)
    public void testLDAPCRLDownloadHandlerNoMatch()
    throws Exception
    {
        CRLDownloadHandler handler = new LDAPCRLDownloadHandler();

        URI uri = new URI("ldap:///CN=ukspkca01,CN=AIA,CN=Public%20Key%20Services,CN=Services,CN=Configuration," +
            "DC=hds,DC=com?cACertificate?base?objectClass=certificationAuthority");

        handler.downloadCRLs(uri);
    }
}
