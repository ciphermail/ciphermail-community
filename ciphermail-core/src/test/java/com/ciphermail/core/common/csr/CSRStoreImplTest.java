/*
 * Copyright (c) 2015-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption pro.
 */
package com.ciphermail.core.common.csr;

import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.common.security.PublicKeyInspector;
import com.ciphermail.core.common.security.certificate.CSRBuilder;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.X500PrincipalInspector;
import com.ciphermail.core.common.security.certificate.X500PrincipalUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateBuilder;
import com.ciphermail.core.common.security.certificate.impl.StandardX509CertificateBuilder;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CSRStoreImplTest
{
    private static final File TEST_BASE_CORE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private CSRStore csrStore;

    @Autowired
    private NamedBlobManager namedBlobManager;

    @Before
    public void before()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                namedBlobManager.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGenerateCSR()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, csrStore.getCSRs().size());

                X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

                builder.setCommonName("john doe");
                builder.setEmail("test@example.com");
                builder.setCountryCode("NL");
                builder.setState("NH");
                builder.setGivenName("john");
                builder.setSurname("Doe");
                builder.setLocality("Amsterdam");
                builder.setOrganisation("CipherMail");

                CSRStoreEntry csr = csrStore.generateCSR(List.of("example.com"), builder.buildName(),
                        CSRBuilder.Algorithm.RSA_2048);

                assertNotNull(csr);

                assertEquals(1, csrStore.getCSRs().size());

                assertNotNull(csr.getPKCS10Request());

                X500Name subject = csr.getPKCS10Request().getSubject();

                assertEquals("1.2.840.113549.1.9.1=test@example.com,givenName=john,sn=Doe,cn=john doe,o=CipherMail,"
                    + "l=Amsterdam,st=NH,c=NL", subject.toString());

                X500PrincipalInspector inspector = new X500PrincipalInspector(subject);

                assertEquals("john doe", StringUtils.join(inspector.getCommonName(), ","));
                assertEquals("test@example.com", StringUtils.join(inspector.getEmail(), ","));

                assertNotNull(csr.getKeyPair());
                assertNotNull(csr.getKeyPair().getPrivate());
                assertNotNull(csr.getKeyPair().getPublic());
                assertNull(csr.getCertificateChain());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGenerateMultipleCSRs()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, csrStore.getCSRs().size());

                X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

                builder.setCommonName("john doe 1");
                builder.setEmail("test@example.com");

                CSRStoreEntry csr1 = csrStore.generateCSR(List.of("example.com"), builder.buildName(),
                        CSRBuilder.Algorithm.RSA_2048);

                assertNotNull(csr1);

                assertEquals(2048, PublicKeyInspector.getKeyLength(csr1.getKeyPair().getPublic()));

                assertEquals(1, csrStore.getCSRs().size());

                builder.setCommonName("john doe 2");

                CSRStoreEntry csr2 = csrStore.generateCSR(List.of("example.com"), builder.buildName(),
                        CSRBuilder.Algorithm.RSA_2048);

                assertNotNull(csr2);

                assertEquals(2048, PublicKeyInspector.getKeyLength(csr2.getKeyPair().getPublic()));

                assertEquals(2, csrStore.getCSRs().size());

                builder.setCommonName("john doe 3");

                CSRStoreEntry csr3 = csrStore.generateCSR(List.of("example.com"), builder.buildName(),
                        CSRBuilder.Algorithm.RSA_4096);

                assertNotNull(csr3);

                assertEquals(4096, PublicKeyInspector.getKeyLength(csr3.getKeyPair().getPublic()));

                assertEquals(3, csrStore.getCSRs().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGenerateCSRImportCertificate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

                builder.setCommonName("john doe");
                builder.setEmail("test@example.com");

                X500Name subject = builder.buildName();

                CSRStoreEntry csr = csrStore.generateCSR(List.of("example.com"), subject,
                        CSRBuilder.Algorithm.RSA_2048);

                X509CertificateBuilder certificateBuilder = new StandardX509CertificateBuilder("BC", "BC");

                certificateBuilder.setSubject(X500PrincipalUtils.fromX500Name(subject));
                certificateBuilder.setIssuer(X500PrincipalUtils.fromX500Name(subject));
                certificateBuilder.setNotBefore(new Date());
                certificateBuilder.setNotAfter(DateUtils.addDays(new Date(), 1));
                certificateBuilder.setPublicKey(csr.getKeyPair().getPublic());
                certificateBuilder.setSerialNumber(BigInteger.valueOf(1L));
                certificateBuilder.setSignatureAlgorithm("SHA256WithRSA");

                X509Certificate certificate = certificateBuilder.generateCertificate(csr.getKeyPair().getPrivate(), null);

                // to test whether the chain is stored, we will use some random certs
                X509Certificate[] fakeChain = CollectionUtils.toArray(CertificateUtils.readX509Certificates(
                        new File(TEST_BASE_CORE, "certificates/testCertificates.p7b")), X509Certificate.class);

                csrStore.importCertificate(ArrayUtils.addAll(new X509Certificate[]{certificate}, fakeChain));

                csr = csrStore.getCSR(csr.getID());

                assertEquals(certificate, csr.getCertificateChain()[0]);

                for (int i = 0; i < fakeChain.length; i++) {
                    assertEquals(fakeChain[i], csr.getCertificateChain()[i+1]);
                }

                // Import again
                csrStore.importCertificate(ArrayUtils.addAll(new X509Certificate[]{certificate}, fakeChain));

                csr = csrStore.getCSR(csr.getID());

                assertEquals(certificate, csr.getCertificateChain()[0]);

                for (int i = 0; i < fakeChain.length; i++) {
                    assertEquals(fakeChain[i], csr.getCertificateChain()[i+1]);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGenerateCSRImportCertificateNoMatch()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

                builder.setCommonName("john doe");
                builder.setEmail("test@example.com");

                X500Name subject = builder.buildName();

                csrStore.generateCSR(List.of("example.com"), subject, CSRBuilder.Algorithm.RSA_2048);

                X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_CORE,
                        "certificates/testcertificate.cer"));

                CSRStoreEntry entry = csrStore.importCertificate(certificate);

                assertNull(entry);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDeleteCSR()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, csrStore.getCSRs().size());

                X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

                builder.setCommonName("john doe");
                builder.setEmail("test@example.com");

                CSRStoreEntry csr = csrStore.generateCSR(List.of("example.com"), builder.buildName(),
                        CSRBuilder.Algorithm.RSA_2048);

                assertEquals(1, csrStore.getCSRs().size());

                csrStore.deleteCSR(csr.getID());

                assertEquals(0, csrStore.getCSRs().size());

                csr = csrStore.generateCSR(List.of("example.com"), builder.buildName(),
                        CSRBuilder.Algorithm.RSA_2048);

                assertEquals(1, csrStore.getCSRs().size());

                csrStore.deleteCSR("non existing");

                assertEquals(1, csrStore.getCSRs().size());

                csrStore.deleteCSR(csr.getID());

                assertEquals(0, csrStore.getCSRs().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
