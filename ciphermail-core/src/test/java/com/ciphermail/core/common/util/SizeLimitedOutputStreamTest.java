/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.mutable.MutableBoolean;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SizeLimitedOutputStreamTest
{
    @Test
    public void testLargeBuff()
    throws IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        SizeLimitedOutputStream slo = new SizeLimitedOutputStream(bos, 12345);

        byte[] buf = new byte[12345];

        for (int i = 0; i < buf.length; i++) {
            buf[i] = (byte) i;
        }

        slo.write(buf, 0, buf.length);

        byte[] result = bos.toByteArray();

        assertEquals(12345, result.length);

        for (int i = 0; i < buf.length; i++) {
            assertEquals(buf[i], result[i]);
        }
    }

    @Test(expected = LimitReachedException.class)
    public void testOverTheLimit()
    throws IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        SizeLimitedOutputStream slo = new SizeLimitedOutputStream(bos, 3, true, true);

        byte[] buf = {1, 2, 3, 4};

        slo.write(buf, 0, buf.length);
    }

    @Test
    public void testOverTheLimitBitsinkMode()
    throws IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        SizeLimitedOutputStream slo = new SizeLimitedOutputStream(bos, 3, false /* bitsink mode */, true);

        byte[] buf = {1, 2, 3, 4};

        slo.write(buf, 0, buf.length);

        byte[] result = bos.toByteArray();

        assertEquals(3, result.length);
        assertEquals(1, result[0]);
        assertEquals(2, result[1]);
        assertEquals(3, result[2]);
    }

    @Test
    public void testOverInLimit()
    throws IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        SizeLimitedOutputStream slo = new SizeLimitedOutputStream(bos, 30);

        byte[] buf = {1, 2, 3, 4};

        slo.write(buf, 0, buf.length);

        byte[] result = bos.toByteArray();

        assertEquals(4, result.length);
    }

    @Test
    public void testClose()
    throws IOException
    {
        final MutableBoolean closeCalled = new MutableBoolean();

        ByteArrayOutputStream bos = new ByteArrayOutputStream() {
            @Override
            public void close() {
                closeCalled.setValue(true);
            }
        };

        SizeLimitedOutputStream slo = new SizeLimitedOutputStream(bos, 100);

        assertFalse(closeCalled.booleanValue());

        slo.close();

        assertTrue(closeCalled.booleanValue());
    }

    @Test
    public void testFlush()
    throws IOException
    {
        final MutableBoolean flushCalled = new MutableBoolean();

        ByteArrayOutputStream bos = new ByteArrayOutputStream() {
            @Override
            public void flush() {
                flushCalled.setValue(true);
            }
        };

        SizeLimitedOutputStream slo = new SizeLimitedOutputStream(bos, 100);

        assertFalse(flushCalled.booleanValue());

        slo.flush();

        assertTrue(flushCalled.booleanValue());
    }
}
