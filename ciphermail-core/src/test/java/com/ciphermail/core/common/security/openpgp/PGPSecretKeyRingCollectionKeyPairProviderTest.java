/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPSecretKeyRingCollection;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PGPSecretKeyRingCollectionKeyPairProviderTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Test
    public void testPGPSecretKeyRingCollectionKeyPairProvider()
    throws Exception
    {
        PGPSecretKeyRingCollection keyRingCollection = new JcaPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, "test-multiple-sub-keys.gpg.key"))));

        PGPKeyPairProvider keyPairProvider = new PGPSecretKeyRingCollectionKeyPairProvider(keyRingCollection,
                new StaticPasswordProvider("test"));

        List<PGPKeyRingPair> pairs = keyPairProvider.getByKeyID(PGPUtils.getKeyIDFromHex("D80D1572D0486F55"));

        assertEquals(1, pairs.size());
        assertEquals("D80D1572D0486F55", PGPUtils.getKeyIDHex(pairs.get(0).getPublicKey().getKeyID()));
        assertNotNull(pairs.get(0).getPrivateKey());

        pairs = keyPairProvider.getByKeyID(PGPUtils.getKeyIDFromHex("639BCE48E8891CC7"));

        assertEquals(1, pairs.size());
        assertEquals("639BCE48E8891CC7", PGPUtils.getKeyIDHex(pairs.get(0).getPublicKey().getKeyID()));
        assertNotNull(pairs.get(0).getPrivateKey());

        pairs = keyPairProvider.getByKeyID(PGPUtils.getKeyIDFromHex("FF"));
        assertEquals(0, pairs.size());
    }
}
