/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.tools.GPG;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.cryptlib.CryptlibObjectIdentifiers;
import org.bouncycastle.asn1.sec.SECObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.bcpg.ECDHPublicBCPGKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPSecretKeyRingCollection;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PGPMIMEEncryptionBuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static List<PGPPublicKey> publicKeys;

    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        publicKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "pgp/test@example.com.gpg.asc"));
    }

    private void decryptGPG(String password, File file, StringBuilder output, StringBuilder error)
    throws Exception
    {
        GPG gpg = new GPG();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ByteArrayOutputStream errorStream = new ByteArrayOutputStream();

        TeeOutputStream outputTee = new TeeOutputStream(outputStream, System.out);
        TeeOutputStream errorTee = new TeeOutputStream(errorStream, System.err);

        try {
            assertTrue(gpg.decrypt(file, password, outputTee, errorTee));
        }
        finally
        {
            if (output != null) {
                output.append(MiscStringUtils.toStringFromASCIIBytes(outputStream.toByteArray()));
            }

            if (error != null) {
                error.append(MiscStringUtils.toStringFromASCIIBytes(errorStream.toByteArray()));
            }
        }
    }

    private MimeMessage decryptCM(MimeMessage encryptedMessage, String secretKeyFile, String password)
    throws Exception
    {
        // Clone message to make sure it has been written and additional CR/LF pairs have been removed
        encryptedMessage = MailUtils.cloneMessage(encryptedMessage);

        assertTrue(encryptedMessage.isMimeType("multipart/encrypted;"));

        PGPSecretKeyRingCollection keyRingCollection = new JcaPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, secretKeyFile))));

        PGPKeyPairProvider keyPairProvider = new PGPSecretKeyRingCollectionKeyPairProvider(keyRingCollection,
                new StaticPasswordProvider(password));

        PGPMIMEHandler handler = new PGPMIMEHandler(keyPairProvider);

        return handler.handleMessage(encryptedMessage);
    }

    @Test
    public void testPGPEncryptedMultipart()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        MimeMessage encrypted = builder.encrypt(message, publicKeys);

        BodyPart encryptedBlob = ((Multipart)encrypted.getContent()).getBodyPart(1);

        assertEquals("inline; filename=\"encrypted.asc\"", StringUtils.join(
                encryptedBlob.getHeader("Content-Disposition")));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain;"));
        assertTrue(output.contains("Test"));
        assertTrue(output.contains("Content-Type: text/html;"));
        assertTrue(output.contains("<BODY bgColor=3D#ffffff>"));
        assertTrue(output.contains("Content-Type: application/octet-stream;"));
        assertTrue(output.contains("cityinfo.zip"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains("pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertTrue(error.contains("pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/test@example.com.gpg.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("multipart/mixed;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain;"));
        assertTrue(mime.contains("Test"));
        assertTrue(mime.contains("Content-Type: text/html;"));
        assertTrue(mime.contains("<BODY bgColor=3D#ffffff>"));
        assertTrue(mime.contains("Content-Type: application/octet-stream;"));
        assertTrue(mime.contains("cityinfo.zip"));
    }

    @Test
    public void testPGPEncryptedTextOnlyAESDefault()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        assertEquals(2, publicKeys.size());
        assertEquals("4EC4E8813E11A9A0", PGPUtils.getKeyIDHex(publicKeys.get(0).getKeyID()));
        assertEquals("8AAA6718AADEAF7F", PGPUtils.getKeyIDHex(publicKeys.get(1).getKeyID()));

        MimeMessage encrypted = builder.encrypt(message, publicKeys);

        BodyPart encryptedBlob = ((Multipart)encrypted.getContent()).getBodyPart(1);

        assertEquals("inline; filename=\"encrypted.asc\"", StringUtils.join(
                encryptedBlob.getHeader("Content-Disposition")));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 1, keyid 4EC4E8813E11A9A0"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 1, keyid 8AAA6718AADEAF7F"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/test@example.com.gpg.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptedTextOnlyAES128()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_128);

        MimeMessage encrypted = builder.encrypt(message, publicKeys);

        BodyPart encryptedBlob = ((Multipart)encrypted.getContent()).getBodyPart(1);

        assertEquals("inline; filename=\"encrypted.asc\"", StringUtils.join(
                encryptedBlob.getHeader("Content-Disposition")));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/test@example.com.gpg.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptedTextOnlyAES192()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_192);

        MimeMessage encrypted = builder.encrypt(message, publicKeys);

        BodyPart encryptedBlob = ((Multipart)encrypted.getContent()).getBodyPart(1);

        assertEquals("inline; filename=\"encrypted.asc\"", StringUtils.join(
                encryptedBlob.getHeader("Content-Disposition")));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES192 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/test@example.com.gpg.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptedTextOnlyAES256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        MimeMessage encrypted = builder.encrypt(message, publicKeys);

        BodyPart encryptedBlob = ((Multipart)encrypted.getContent()).getBodyPart(1);

        assertEquals("inline; filename=\"encrypted.asc\"", StringUtils.join(
                encryptedBlob.getHeader("Content-Disposition")));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/test@example.com.gpg.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptedTextOnlyContentDispositionAttachment()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        builder.setDispositionInline(false);

        MimeMessage encrypted = builder.encrypt(message, publicKeys);

        BodyPart encryptedBlob = ((Multipart)encrypted.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"encrypted.asc\"", StringUtils.join(
                encryptedBlob.getHeader("Content-Disposition")));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/test@example.com.gpg.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptSigned()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        MimeMessage encrypted = builder.encrypt(message, publicKeys);

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/test@example.com.gpg.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("multipart/signed;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
        assertTrue(mime.contains("-----BEGIN PGP SIGNATURE-----"));
    }

    @Test
    public void testPGPEncryptNISTP256AES128()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_128);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/nist-p-256@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(SECObjectIdentifiers.secp256r1,
                (((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID()));

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid AF8B30CEB2B736DE"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/nist-p-256@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptNISTP256AES256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        // ECDH requires AES256 ?? Or is it because of some padding? https://dev.gnupg.org/T3763
        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/nist-p-256@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(SECObjectIdentifiers.secp256r1,
                (((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID()));

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid AF8B30CEB2B736DE"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/nist-p-256@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptNISTP384AES256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/nist-p-384@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(SECObjectIdentifiers.secp384r1,
                (((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID()));

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid EFD63D644076AA44"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/nist-p-384@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptNISTP521AES256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/nist-p-521@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(SECObjectIdentifiers.secp521r1,
                (((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID()));

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid 5C93EB074ECFED6E"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/nist-p-521@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptCurve25519AES128()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        // ECDH requires AES256 ?? Or is it because of some padding? https://dev.gnupg.org/T3763
        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_128);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/curve-25519@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(CryptlibObjectIdentifiers.curvey25519,
                ((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID());

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid A396424B7E0C3D5B"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/curve-25519@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptCurve25519AES256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        // ECDH requires AES256 ?? Or is it because of some padding? https://dev.gnupg.org/T3763
        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/curve-25519@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(CryptlibObjectIdentifiers.curvey25519,
                ((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID());

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid A396424B7E0C3D5B"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/curve-25519@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptBrainpoolP256AES128()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        // ECDH requires AES256 ?? Or is it because of some padding? https://dev.gnupg.org/T3763
        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_128);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/brainpool-p-256@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(TeleTrusTObjectIdentifiers.brainpoolP256r1,
                (((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID()));

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid CA9DD1D7A12E2513"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/brainpool-p-256@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptBrainpoolP256AES256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        // ECDH requires AES256 ?? Or is it because of some padding? https://dev.gnupg.org/T3763
        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/brainpool-p-256@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(TeleTrusTObjectIdentifiers.brainpoolP256r1,
                (((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID()));

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid CA9DD1D7A12E2513"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/brainpool-p-256@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptBrainpoolP384AES256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        // ECDH requires AES256 ?? Or is it because of some padding? https://dev.gnupg.org/T3763
        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/brainpool-p-384@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(TeleTrusTObjectIdentifiers.brainpoolP384r1,
                (((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID()));

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid B4338BDA88A2BA0E"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/brainpool-p-384@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptBrainpoolP512AES256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        // ECDH requires AES256 ?? Or is it because of some padding? https://dev.gnupg.org/T3763
        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/brainpool-p-512@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(TeleTrusTObjectIdentifiers.brainpoolP512r1,
                (((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID()));

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid 13635FC461FAB8E4"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/brainpool-p-512@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
    }

    @Test
    public void testPGPEncryptSECP256k1keyAES256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMEEncryptionBuilder builder = new PGPMIMEEncryptionBuilder();

        // ECDH requires AES256 ?? Or is it because of some padding? https://dev.gnupg.org/T3763
        builder.setEncryptionAlgorithm(PGPEncryptionAlgorithm.AES_256);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/secp256k1@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPPublicKey publicKey = ecKeys.get(1).getPublicKey();

        assertEquals(SECObjectIdentifiers.secp256k1,
                (((ECDHPublicBCPGKey)publicKey.getPublicKeyPacket().getKey()).getCurveOID()));

        assertFalse(publicKey.isMasterKey());
        assertEquals(PGPPublicKeyAlgorithm.ECDH, PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm()));
        assertTrue(publicKey.isEncryptionKey());

        MimeMessage encrypted = builder.encrypt(message, Collections.singletonList(publicKey));

        File encryptedFile = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(encrypted, encryptedFile);

        StringBuilder outputBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();

        decryptGPG("test", encryptedFile, outputBuilder, errorBuilder);

        String output = outputBuilder.toString();
        String error = errorBuilder.toString();

        assertTrue(output.contains("Content-Type: text/plain"));
        assertTrue(output.contains("test"));

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains(":pubkey enc packet: version 3, algo 18, keyid ECBCD7754520A2C0"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES256 encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));

        MimeMessage decrypted = decryptCM(encrypted, "pgp/secp256k1@example.com.key", "test");

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("text/plain;"));

        ByteArrayOutputStream mimeOutputStream = new ByteArrayOutputStream();

        decrypted.writeTo(mimeOutputStream);

        String mime = mimeOutputStream.toString(StandardCharsets.UTF_8);

        assertTrue(mime.contains("Content-Type: text/plain"));
        assertTrue(mime.contains("test"));
   }
}
