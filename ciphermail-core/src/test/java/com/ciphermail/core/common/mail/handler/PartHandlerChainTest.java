/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.handler;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.HeaderNameMatcher;
import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PartHandlerChainTest
{
    @Test
    public void testChainUnknownContentType()
    throws MessagingException, IOException
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-type.eml");

        HeaderMatcher matcher = new HeaderNameMatcher("date", "SUBJECT");

        PartHandlerChain decorator = new PartHandlerChain(new RemoveHeadersHandler(matcher));

        decorator.addMessageHandlers(new AddHeadersHandler(new Header("test", "123"),
                new Header("test", "456")));

        message = BodyPartUtils.toMessage(decorator.handlePart(message));

        MailUtils.validateMessage(message);

        assertNull(message.getSubject());
        assertNull(message.getHeader("datE"));
        assertEquals("123,456", message.getHeader("test", ","));

        MailUtils.validateMessage(message);
    }

    @Test
    public void testChainOrderTest()
    throws MessagingException, IOException
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-type.eml");

        HeaderMatcher matcher = new HeaderNameMatcher("test");

        PartHandlerChain decorator = new PartHandlerChain();

        decorator.addMessageHandlers(new AddHeadersHandler(new Header("test", "123"),
                new Header("test", "456")));

        // this handler should remove the headers that were just added
        decorator.addMessageHandlers(new RemoveHeadersHandler(matcher));

        message = BodyPartUtils.toMessage(decorator.handlePart(message));

        MailUtils.validateMessage(message);

        assertNull(message.getHeader("test", ","));

        MailUtils.validateMessage(message);
    }

    @Test
    public void testChainOrderTestOtherOrder()
    throws MessagingException, IOException
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-type.eml");

        HeaderMatcher matcher = new HeaderNameMatcher("test");

        PartHandlerChain decorator = new PartHandlerChain();

        decorator.addMessageHandlers(new RemoveHeadersHandler(matcher));
        decorator.addMessageHandlers(new AddHeadersHandler(new Header("test", "123"),
                new Header("test", "456")));

        message = BodyPartUtils.toMessage(decorator.handlePart(message));

        MailUtils.validateMessage(message);

        assertEquals("123,456", message.getHeader("test", ","));

        MailUtils.validateMessage(message);
    }
}
