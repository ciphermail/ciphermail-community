/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.common.security.openpgp.PGPSignatureValidator;
import com.ciphermail.core.common.security.openpgp.PGPSignatureValidatorImpl;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RevocationValidatorTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static final PGPSignatureValidator signatureValidator = new PGPSignatureValidatorImpl();

    @Test
    public void testRevoked()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test-multiple-sub-keys.gpg.asc"));

        assertEquals(6, keys.size());

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        Context context = new ContextImpl();

        context.set(StandardValidatorContextObjects.MASTER_KEY, master);

        for (PGPPublicKey key : keys)
        {
            PGPPublicKeyValidator validator = new RevocationValidator(signatureValidator);

            PGPPublicKeyValidatorResult result = validator.validate(key, context);

            if (PGPUtils.getKeyIDHex(key.getKeyID()).equals("80C4F95A9FE00D79")) {
                assertFalse(result.isValid());
            }
            else {
                assertTrue(result.isValid());
            }
        }
    }

    @Test
    public void testMasterKeyNotInContext()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test-multiple-sub-keys.gpg.asc"));

        assertEquals(6, keys.size());

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        Context context = new ContextImpl();

        for (PGPPublicKey key : keys)
        {
            PGPPublicKeyValidator validator = new RevocationValidator(signatureValidator);

            PGPPublicKeyValidatorResult result = validator.validate(key, context);

            if (PGPUtils.getKeyIDHex(key.getKeyID()).equals("D80D1572D0486F55")) {
                assertTrue(result.isValid());
            }
            else {
                assertFalse(result.isValid());

                assertEquals("Master key not available. Unable to check revocation status.",
                        result.getFailureMessage());
                assertEquals(PGPPublicKeyValidatorFailureSeverity.MAJOR, result.getFailureSeverity());
            }
        }
    }

    @Test
    public void testRevokedDecodedKey()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test-multiple-sub-keys.gpg.asc"));

        assertEquals(6, keys.size());

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        PGPPublicKey revokedKey = null;

        for (PGPPublicKey key : keys)
        {
            if (PGPUtils.getKeyIDHex(key.getKeyID()).equals("80C4F95A9FE00D79")) {
                revokedKey = key;
            }
        }

        Context context = new ContextImpl();

        context.set(StandardValidatorContextObjects.MASTER_KEY, master);

        PGPPublicKeyValidator validator = new RevocationValidator(signatureValidator);

        PGPPublicKeyValidatorResult result = validator.validate(revokedKey, context);
        assertFalse(result.isValid());

        // Now encode and decode to see whether the signature is kept
        revokedKey = PGPKeyUtils.decodePublicKey(revokedKey.getEncoded());

        result = validator.validate(revokedKey, context);
        assertFalse(result.isValid());
    }

    @Test
    public void testRevokedMasterKey()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "revoked-master-key.asc"));

        assertEquals(2, keys.size());

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        Context context = new ContextImpl();

        context.set(StandardValidatorContextObjects.MASTER_KEY, master);

        for (PGPPublicKey key : keys)
        {
            PGPPublicKeyValidator validator = new RevocationValidator(signatureValidator);

            PGPPublicKeyValidatorResult result = validator.validate(key, context);

            // Since the master is revoked, the sub keys are also revoked
            assertFalse(result.isValid());
        }
    }
}
