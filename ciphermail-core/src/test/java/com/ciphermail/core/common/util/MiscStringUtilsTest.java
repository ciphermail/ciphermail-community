/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.junit.Test;

import java.util.Collection;
import java.util.LinkedList;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class MiscStringUtilsTest
{
    @Test
    public void testIsPrintableAscii()
    {
        assertTrue(MiscStringUtils.isPrintableAscii(""));
        assertTrue(MiscStringUtils.isPrintableAscii(" 12 zZ"));
        assertTrue(MiscStringUtils.isPrintableAscii(null));
        assertTrue(MiscStringUtils.isPrintableAscii("\n\r\t"));
        assertFalse(MiscStringUtils.isPrintableAscii("\0007"));
        assertFalse(MiscStringUtils.isPrintableAscii("öäüßÖÄÜ"));
    }

    @Test
    public void testToAscii()
    {
        assertEquals("123", MiscStringUtils.convertToASCII("123"));
        assertNull(MiscStringUtils.convertToASCII(null));
        assertEquals("???", MiscStringUtils.convertToASCII("刘书洪"));
    }

    @Test
    public void testToAsciiBytes()
    {
        byte[] bytes = MiscStringUtils.getBytesASCII("ABC\n");

        assertArrayEquals(new byte[]{ 'A', 'B', 'C', '\n'}, bytes);

        assertNull(MiscStringUtils.getBytesASCII(null));
    }

    @Test
    public void testToAsciiString()
    {
        String s = MiscStringUtils.toStringFromASCIIBytes(new byte[]{ 'A', 'B', 'C', '\n'});

        assertEquals("ABC\n", s);

        assertNull(MiscStringUtils.toStringFromASCIIBytes((byte[])null));
    }

    @Test
    public void testToUTF8String()
    {
        String s = MiscStringUtils.toStringFromUTF8Bytes(new byte[]{ 'A', 'B', 'C', '\n'});

        assertEquals("ABC\n", s);

        assertNull(MiscStringUtils.toStringFromUTF8Bytes(null));
    }

    @Test
    public void testRemoveControlChars()
    {
        assertNull(MiscStringUtils.removeControlChars(null));
        assertEquals("123", MiscStringUtils.removeControlChars("123"));
        assertEquals(" 123 ", MiscStringUtils.removeControlChars(" 123 "));
        assertEquals("123", MiscStringUtils.removeControlChars("\00\r1\n2\t3\00"));
        assertNull(MiscStringUtils.removeControlChars(null));
    }

    @Test
    public void testUnquote()
    {
        assertEquals("test@example.com", MiscStringUtils.unquote("test@example.com", '"'));
        assertEquals("test@example.com", MiscStringUtils.unquote("'test@example.com'", '\''));
        assertEquals("test@example.com", MiscStringUtils.unquote("\"test@example.com\"", '"'));
        assertEquals("test@example.com", MiscStringUtils.unquote("#test@example.com#", '#'));
        assertEquals("\"test@example.com", MiscStringUtils.unquote("\"test@example.com", '"'));
        assertEquals("", MiscStringUtils.unquote("", '"'));
        assertNull(MiscStringUtils.unquote(null, '"'));
    }

    @Test
    public void testRestrictLength()
    {
        String s = MiscStringUtils.restrictLength("123", 3);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("123", 4);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("123", 1);
        assertEquals("1", s);

        s = MiscStringUtils.restrictLength("123", 0);
        assertEquals("", s);

        s = MiscStringUtils.restrictLength(null, 0);
        assertNull(s);

        s = MiscStringUtils.restrictLength("123", -1);
        assertEquals("", s);

        assertNull(MiscStringUtils.restrictLength(null, 1));
    }

    @Test
    public void testRestrictLengthDots()
    {
        String s = MiscStringUtils.restrictLength("123", 3, true);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("123", 3, false);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("1234", 3, true);
        assertEquals("...", s);

        s = MiscStringUtils.restrictLength("1234", 3, false);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("12345678", 5, true);
        assertEquals("12...", s);

        s = MiscStringUtils.restrictLength("12", 1, true);
        assertEquals("...", s);
    }

    @Test
    public void testRestrictLengthDotsSetDots()
    {
        String s = MiscStringUtils.restrictLength("1234", 3, true, "***");
        assertEquals("***", s);

        s = MiscStringUtils.restrictLength("12345678", 5, true, "**");
        assertEquals("123**", s);

        s = MiscStringUtils.restrictLength("12", 1, true, "long string");
        assertEquals("long string", s);

        s = MiscStringUtils.restrictLength("1234", 3, true, null);
        assertEquals("123", s);

        s = MiscStringUtils.restrictLength("123456789", 7, true, "******");
        assertEquals("1******", s);
    }

    @Test
    public void testEnsureEndsWith()
    {
        assertEquals("test/", MiscStringUtils.ensureEndsWith("test/", "/"));
        assertEquals("test/", MiscStringUtils.ensureEndsWith("test", "/"));
        assertEquals("123", MiscStringUtils.ensureEndsWith(null, "123"));
    }

    @Test
    public void testReplaceLastChars()
    {
        String s = MiscStringUtils.replaceLastChars("abc123", 3, "mn");
        assertEquals("abcmnmnmn", s);

        s = MiscStringUtils.replaceLastChars("abc123", 4, "*");
        assertEquals("ab****", s);

        s = MiscStringUtils.replaceLastChars("abc123", 100, "*");
        assertEquals("******", s);

        s = MiscStringUtils.replaceLastChars("abc123", 1, null);
        assertEquals("abc12", s);

        s = MiscStringUtils.replaceLastChars(null, 1, null);
        assertEquals("", s);

        s = MiscStringUtils.replaceLastChars("abc123", 0, "**");
        assertEquals("abc123", s);

        s = MiscStringUtils.replaceLastChars("abc123", -100, "**");
        assertEquals("abc123", s);

        s = MiscStringUtils.replaceLastChars("", 100, "**");
        assertEquals("", s);
    }

    @Test
    public void testContains()
    {
        Collection<String> lines = new LinkedList<String>();

        lines.add("test");
        lines.add("test");
        lines.add("match");

        assertTrue(MiscStringUtils.contains("test", lines));
        assertTrue(MiscStringUtils.contains("match", lines));
        assertFalse(MiscStringUtils.contains("ppp", lines));
        assertFalse(MiscStringUtils.contains(null, (Collection<String>)null));

        assertTrue(MiscStringUtils.contains("test", new String[]{"a", "test"}));
        assertFalse(MiscStringUtils.contains("test", new String[]{}));
        assertFalse(MiscStringUtils.contains(null, (String[])null));

        assertEquals(0, MiscStringUtils.containsIndex("test", lines));
        assertEquals(2, MiscStringUtils.containsIndex("match", lines));
        assertEquals(-1, MiscStringUtils.containsIndex("ppp", lines));
        assertEquals(-1, MiscStringUtils.containsIndex(null, (Collection<String>)null));

        assertEquals(1, MiscStringUtils.containsIndex("test", new String[]{"a", "test"}));
        assertEquals(-1, MiscStringUtils.containsIndex("test", new String[]{}));
        assertEquals(-1, MiscStringUtils.containsIndex(null, (String[])null));
    }

    @Test
    public void testContainsRegEx()
    {
        Collection<String> lines = new LinkedList<String>();

        lines.add("test 1234 abc 567");
        lines.add("678");
        lines.add("no match");

        assertTrue(MiscStringUtils.containsRegEx("\\d{3}", lines));
        assertTrue(MiscStringUtils.containsRegEx("^\\d{3}$", lines));
        assertEquals(0, MiscStringUtils.containsRegExIndex("\\d{3}", lines));
        assertEquals(1, MiscStringUtils.containsRegExIndex("^\\d{3}$", lines));
        assertFalse(MiscStringUtils.containsRegEx("\\d{5}", lines));
        assertFalse(MiscStringUtils.containsRegEx("\\d{5}", (Collection<String>)null));

        assertTrue(MiscStringUtils.containsRegEx("\\d{3}", new String[]{"123"}));
        assertFalse(MiscStringUtils.containsRegEx("\\d{4}", new String[]{"123"}));
        assertFalse(MiscStringUtils.containsRegEx("\\d{5}", (String[])null));
    }

    @Test
    public void testSplitStringIntoFixedSizeChunks()
    {
        assertEquals("123\r\n456\r\n7", MiscStringUtils.splitStringIntoFixedSizeChunks("1234567", 3, "\r\n"));
        assertEquals("12\r\r\n\n34\r\n5", MiscStringUtils.splitStringIntoFixedSizeChunks("12\r\n345", 3, "\r\n"));
        assertEquals("123\r\n456\r\n7", MiscStringUtils.splitStringIntoFixedSizeChunks("1234567", 3, "\r\n"));
        assertEquals("123#456#7", MiscStringUtils.splitStringIntoFixedSizeChunks("1234567", 3, "#"));
        assertEquals("1234567", MiscStringUtils.splitStringIntoFixedSizeChunks("1234567", 3, null));
        assertEquals("123", MiscStringUtils.splitStringIntoFixedSizeChunks("123", 3, "\r\n"));
        assertNull(MiscStringUtils.splitStringIntoFixedSizeChunks(null, 3, "\r\n"));
        assertEquals("1#2#3", MiscStringUtils.splitStringIntoFixedSizeChunks("123", 1, "#"));
        assertEquals("1\n2\n3", MiscStringUtils.splitStringIntoFixedSizeChunks("123", 1));
    }
}
