/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.junit.Test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ProcessRunnerTest
{
    @Test
    public void testRunSuccess()
    throws Exception
    {
        ProcessRunner runner = new ProcessRunner();

        List<String> cmd = new LinkedList<>();

        cmd.add("ls");

        assertEquals(0, runner.run(cmd));
    }

    @Test
    public void testRunFailure()
    throws Exception
    {
        ProcessRunner runner = new ProcessRunner();

        List<String> cmd = new LinkedList<>();

        cmd.add("ls");
        cmd.add("--non-existing-property");

        try {
            runner.run(cmd);

            fail();
        }
        catch(ProcessException e) {
            // expected
        }
    }

    @Test
    public void testRunFailureNoException()
    throws Exception
    {
        ProcessRunner runner = new ProcessRunner();

        runner.setThrowExceptionOnErrorExitCode(false);

        List<String> cmd = new LinkedList<>();

        cmd.add("ls");
        cmd.add("--non-existing-property");

        assertEquals(2, runner.run(cmd));
    }

    @Test
    public void testHangingProcess()
    {
        ProcessRunner runner = new ProcessRunner();

        long timeout = 2000;

        runner.setTimeout(timeout);

        List<String> cmd = new LinkedList<>();

        cmd.add("sleep");
        cmd.add("infinity");

        long startTime = System.currentTimeMillis();

        try {
            runner.run(cmd);

            fail();
        }
        catch(IOException e)
        {
            assertTrue(System.currentTimeMillis() - startTime >= timeout);

            // expected
            assertEquals("Timeout running [sleep,infinity]", e.getMessage());
        }
    }

    @Test
    public void testProcessRunnerBulk()
    throws Exception
    {
        for (int i = 0; i < 100; i++)
        {
            ProcessRunner runner = new ProcessRunner();

            List<String> cmd = new LinkedList<>();

            cmd.add("date");

            runner.setOutput(System.out);
            runner.setError(System.err);

            assertEquals(0, runner.run(cmd));
        }
    }

    @Test
    public void testSplitCommandLine()
    {
        List<String> cmd = ProcessRunner.splitCommandLine("ls -l");
        assertEquals(2, cmd.size());
        assertEquals("ls", cmd.get(0));
        assertEquals("-l", cmd.get(1));

        cmd = ProcessRunner.splitCommandLine("ls -1 -2 -3 -4");
        assertEquals(5, cmd.size());
        assertEquals("ls", cmd.get(0));
        assertEquals("-1", cmd.get(1));
        assertEquals("-2", cmd.get(2));
        assertEquals("-3", cmd.get(3));
        assertEquals("-4", cmd.get(4));

        cmd = ProcessRunner.splitCommandLine("ls ''");
        assertEquals(2, cmd.size());
        assertEquals("ls", cmd.get(0));
        assertEquals("", cmd.get(1));

        cmd = ProcessRunner.splitCommandLine("ls '-l'");
        assertEquals(2, cmd.size());
        assertEquals("ls", cmd.get(0));
        assertEquals("-l", cmd.get(1));

        cmd = ProcessRunner.splitCommandLine("echo 'some string'");
        assertEquals(2, cmd.size());
        assertEquals("echo", cmd.get(0));
        assertEquals("some string", cmd.get(1));

        cmd = ProcessRunner.splitCommandLine("echo \"some string\"");
        assertEquals(2, cmd.size());
        assertEquals("echo", cmd.get(0));
        assertEquals("some string", cmd.get(1));
    }
}
