/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.test.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

class SMIMEUtilsTest
{
    @Test
    void testDissectSigned()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-validcertificate.eml");

        MimeMultipart multipart = (MimeMultipart) message.getContent();

        BodyPart[] parts = SMIMEUtils.dissectSigned(multipart);

        Assertions.assertEquals(2, parts.length);
        Assertions.assertNotNull(parts[0]);
        Assertions.assertNotNull(parts[1]);
        Assertions.assertTrue(parts[0].isMimeType("multipart/mixed"));
        Assertions.assertTrue(parts[1].isMimeType("application/pkcs7-signature"));
    }

    @Test
    void testDissectSignedSwitched()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-validcertificate.eml");

        MimeMultipart multipart = (MimeMultipart) message.getContent();

        BodyPart part0 = multipart.getBodyPart(0);
        BodyPart part1 = multipart.getBodyPart(1);

        Assertions.assertTrue(part0.isMimeType("multipart/mixed"));
        Assertions.assertTrue(part1.isMimeType("application/pkcs7-signature"));

        MimeMultipart multipartSwitched = new MimeMultipart();

        multipartSwitched.addBodyPart(part1);
        multipartSwitched.addBodyPart(part0);

        part0 = multipartSwitched.getBodyPart(0);
        part1 = multipartSwitched.getBodyPart(1);

        Assertions.assertTrue(part0.isMimeType("application/pkcs7-signature"));
        Assertions.assertTrue(part1.isMimeType("multipart/mixed"));

        BodyPart[] parts = SMIMEUtils.dissectSigned(multipartSwitched);

        Assertions.assertEquals(2, parts.length);
        Assertions.assertNotNull(parts[0]);
        Assertions.assertNotNull(parts[1]);
        Assertions.assertTrue(parts[0].isMimeType("multipart/mixed"));
        Assertions.assertTrue(parts[1].isMimeType("application/pkcs7-signature"));
    }

    @Test
    void testDissectNull()
    throws Exception
    {
        Assertions.assertNull(SMIMEUtils.dissectSigned(null));
    }

    @Test
    void testDissectNoMatch()
    throws Exception
    {
        MimeMultipart multipart = new MimeMultipart();

        multipart.addBodyPart(new MimeBodyPart());
        multipart.addBodyPart(new MimeBodyPart());

        Assertions.assertNull(SMIMEUtils.dissectSigned(multipart));
    }

    @Test
    void testDissectOnePart()
    throws Exception
    {
        MimeMultipart multipart = new MimeMultipart();

        multipart.addBodyPart(new MimeBodyPart());

        Assertions.assertNull(SMIMEUtils.dissectSigned(multipart));
    }

    @Test
    void testDissectThreeParts()
    throws Exception
    {
        MimeMultipart multipart = new MimeMultipart();

        multipart.addBodyPart(new MimeBodyPart());
        multipart.addBodyPart(new MimeBodyPart());
        multipart.addBodyPart(new MimeBodyPart());

        Assertions.assertNull(SMIMEUtils.dissectSigned(multipart));
    }

    @Test
    void testGetSMIMEType()
    throws Exception
    {
        SMIMEType type = SMIMEUtils.getSMIMEType(TestUtils.loadTestMessage("mail/smime-encrypted-gcm-256.eml"));

        Assertions.assertEquals(SMIMEType.AUTH_ENCRYPTED, type);
    }
}
