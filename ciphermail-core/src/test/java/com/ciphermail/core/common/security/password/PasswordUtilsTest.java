/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.bouncycastle.util.Arrays;
import org.junit.Test;

import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.test.TestProperties;


public class PasswordUtilsTest
{
    @Test
    public void testIsEqualDigest()
    throws Exception
    {
        assertTrue(PasswordUtils.isEqualDigest("a", "a", Digest.SHA1));
        assertTrue(PasswordUtils.isEqualDigest("a", "a", Digest.SHA256));
        assertTrue(PasswordUtils.isEqualDigest(null, null, Digest.SHA1));
        assertTrue(PasswordUtils.isEqualDigest("", "", Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest("a", null, Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest(null, "a", Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest(null, "", Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest("", null, Digest.SHA1));
        assertFalse(PasswordUtils.isEqualDigest("qwerty", "qwerty ", Digest.SHA1));
    }

    @Test
    public void testIsEqual()
    throws Exception
    {
        assertTrue(PasswordUtils.isEqual("a", "a"));
        assertTrue(PasswordUtils.isEqual("a", "a"));
        assertTrue(PasswordUtils.isEqual(null, null));
        assertTrue(PasswordUtils.isEqual("", ""));
        assertFalse(PasswordUtils.isEqual("a", null));
        assertFalse(PasswordUtils.isEqual(null, "a"));
        assertFalse(PasswordUtils.isEqual(null, ""));
        assertFalse(PasswordUtils.isEqual("", null));
        assertFalse(PasswordUtils.isEqual("qwerty", "qwerty "));
    }

    @Test
    public void testIsEqualByteForByte()
    throws Exception
    {
        assertTrue(PasswordUtils.isEqualByteForByte("a", "a"));
        assertTrue(PasswordUtils.isEqualByteForByte("a", "a"));
        assertTrue(PasswordUtils.isEqualByteForByte(null, null));
        assertTrue(PasswordUtils.isEqualByteForByte("", ""));
        assertFalse(PasswordUtils.isEqualByteForByte("a", null));
        assertFalse(PasswordUtils.isEqualByteForByte(null, "a"));
        assertFalse(PasswordUtils.isEqualByteForByte(null, ""));
        assertFalse(PasswordUtils.isEqualByteForByte("", null));
        assertFalse(PasswordUtils.isEqualByteForByte("qwerty", "qwerty "));
    }

    @Test
    public void testIsEqualDigestSpeedTest()
    throws Exception
    {
        final int repeat = 100000;

        final long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            String password = "testqwertyu" + Integer.toString(i);

            assertTrue(PasswordUtils.isEqualDigest(password, password, Digest.SHA256));
        }

        final long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println(perSecond);

        double expected = 10000 * TestProperties.getTestPerformanceFactor();

        assertTrue("Can be slower on slower systems", perSecond > expected);
    }

    @Test
    public void testClearPassword()
    {
        char[] password = "123".toCharArray();

        assertEquals(3, password.length);
        assertTrue(Arrays.areEqual(new char[]{'1', '2', '3'}, password));

        PasswordUtils.clearPassword(password);

        assertEquals(3, password.length);
        assertTrue(Arrays.areEqual(new char[]{0, 0, 0}, password));

        password = new char[]{};
        assertEquals(0, password.length);
        PasswordUtils.clearPassword(password);
        assertEquals(0, password.length);

        PasswordUtils.clearPassword(null);
    }
}
