/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.TestUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BasicCertStoreKeyStoreTest
{
    private static SecurityFactory securityFactory;
    private static X509BasicCertStore certStore;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        certStore = new BasicCertStoreKeyStore(loadKeyStore(new File(
                TestUtils.getTestDataDir(), "keys/testCertificates.p12"), "test"));
    }

    private static KeyStore loadKeyStore(File file, String password)
    throws KeyStoreException
    {
        try {
            KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

            // initialize key store
            keyStore.load(new FileInputStream(file), password.toCharArray());

            return keyStore;
        }
        catch (NoSuchProviderException | NoSuchAlgorithmException | CertificateException | IOException e) {
            throw new KeyStoreException(e);
        }
    }

    @Test
    public void testCertificateIterator()
    throws CertStoreException, CloseableIteratorException
    {
        X509CertSelector selector = new X509CertSelector();

        CloseableIterator<X509Certificate> iterator = certStore.getCertificateIterator(selector);

        List<X509Certificate> certificates = CloseableIteratorUtils.toList(iterator);

        assertEquals(22, certificates.size());
    }

    @Test
    public void testCertificateIteratorNullSelector()
    throws CertStoreException, CloseableIteratorException
    {
        CloseableIterator<X509Certificate> iterator = certStore.getCertificateIterator(null);

        List<X509Certificate> certificates = CloseableIteratorUtils.toList(iterator);

        assertEquals(22, certificates.size());
    }

    @Test
    public void testCertificateIteratorNoMatch()
    throws CertStoreException, CloseableIteratorException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(new BigInteger("123", 10));

        CloseableIterator<X509Certificate> iterator = certStore.getCertificateIterator(selector);

        List<X509Certificate> certificates = CloseableIteratorUtils.toList(iterator);

        assertEquals(0, certificates.size());
    }

    @Test
    public void testCertificateIteratorSerialMatch()
    throws CertStoreException, CloseableIteratorException
    {
        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCAC409FB2022B7D06920A00FE42"));

        CloseableIterator<X509Certificate> iterator = certStore.getCertificateIterator(selector);

        List<X509Certificate> certificates = CloseableIteratorUtils.toList(iterator);

        assertEquals(1, certificates.size());
    }
}
