/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms;

import com.ciphermail.core.common.util.ThreadUtils;
import org.apache.commons.lang.time.DateUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Test SMSTransport implementation.
 *
 * @author Martijn Brinkers
 *
 */
public class DummySMSTransport implements SMSTransport
{
    private final List<String> phoneNumbers = Collections.synchronizedList(new LinkedList<>());
    private final List<String> messages = Collections.synchronizedList(new LinkedList<>());

    private long sleep = DateUtils.MILLIS_PER_SECOND * 5;

    /**
     * Dummy sendSMS. If message equals 'fail' an  IOException will be thrown. If message equals 'delay',
     * a sleep will suspend the thread for some time.
     */
    @Override
    public void sendSMS(String phoneNumber, String message)
    throws IOException
    {
        if (message.equals("fail")) {
            throw new IOException("Failed on purpose");
        }

        phoneNumbers.add(phoneNumber);
        messages.add(message);

        if (message.equals("delay")) {
            ThreadUtils.sleepQuietly(sleep);
        }
    }

    @Override
    public String getName() {
        return "DummySMSTransport";
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public List<String> getMessages() {
        return messages;
    }

    public long getSleep() {
        return sleep;
    }

    public void setSleep(long sleep) {
        this.sleep = sleep;
    }

    public void reset()
    {
        phoneNumbers.clear();
        messages.clear();
    }
}
