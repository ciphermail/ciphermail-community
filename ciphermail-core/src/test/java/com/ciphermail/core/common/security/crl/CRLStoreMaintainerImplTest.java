/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.SignatureException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CRLStoreMaintainerImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt certStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    private X509CRLStoreExt crlStore;

    @Autowired
    private PKISecurityServices pKISecurityServices;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStore.removeAllEntries();
                rootStore.removeAllEntries();
                crlStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private X509CRL addCRL(File crlFile, X509CRLStoreExt crlStore)
    throws Exception
    {
        X509CRL crl = TestUtils.loadX509CRL(crlFile);

        crlStore.addCRL(crl);

        return crl;
    }

    /*
     * Test that adds two CRLS having the same issuer subject but the CRLs are issued
     * by two different CRLs having similar subjects. The new CRL should not overwrite
     * the 'old' CRL
     */
    @Test
    public void testAddSameSubjectNotSameIssuer()
    throws Exception
    {
        CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                pKISecurityServices.getCRLPathBuilderFactory());

        X509Certificate ca1 = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/ca-same-subject-1.cer"));

        X509Certificate root1 = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/root-same-subject-1.cer"));

        ca1.verify(root1.getPublicKey());

        X509Certificate ca2 = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/ca-same-subject-2.cer"));

        X509Certificate root2 = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/root-same-subject-2.cer"));

        ca2.verify(root2.getPublicKey());

        assertNotEquals(ca1, ca2);

        X509CRL crl1 = TestUtils.loadX509CRL(new File(TestUtils.getTestDataDir(),
                "crls/crl-ca-same-subject-1.crl"));

        crl1.verify(ca1.getPublicKey());

        try {
            crl1.verify(ca2.getPublicKey());

            fail();
        }
        catch(SignatureException e) {
            // expected
        }

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStore.addCertificate(ca1);
                certStore.addCertificate(ca2);

                rootStore.addCertificate(root1);
                rootStore.addCertificate(root2);

                assertTrue(crlStoreMaintainer.addCRL(crl1));

                assertTrue(crlStore.contains(crl1));

                assertEquals(1, crlStore.size());
                assertEquals(1, crlStore.getCRLs(null).size());

                X509CRL crl2 = TestUtils.loadX509CRL(new File(TestUtils.getTestDataDir(),
                        "crls/crl-ca-same-subject-2.crl"));

                crl2.verify(ca2.getPublicKey());

                try {
                    crl2.verify(ca1.getPublicKey());

                    fail();
                }
                catch(SignatureException e) {
                    // expected
                }

                assertNotEquals(crl1, crl2);

                assertTrue(crlStoreMaintainer.addCRL(crl2));

                assertTrue(crlStore.contains(crl2));

                assertEquals(2, crlStore.size());
                assertEquals(2, crlStore.getCRLs(null).size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testCRLNotTrusted()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TestUtils.getTestDataDir(), "crls/crl-ca-same-subject-1.crl");

                X509CRL crl = TestUtils.loadX509CRL(crlFile);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                        pKISecurityServices.getCRLPathBuilderFactory());

                crlStoreMaintainer.addCRL(crl);

                assertEquals(0, crlStore.getCRLs(null).size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddDuplicate()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TestUtils.getTestDataDir(), "crls/ThawteSGCCA.crl");

                X509CRL crl = TestUtils.loadX509CRL(crlFile);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                        pKISecurityServices.getCRLPathBuilderFactory(), false);

                crlStoreMaintainer.addCRL(crl);

                assertTrue(crlStore.contains(crl));

                assertEquals(1, crlStore.getCRLs(null).size());

                crlStoreMaintainer.addCRL(crl);

                assertTrue(crlStore.contains(crl));

                assertEquals(1, crlStore.getCRLs(null).size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUpdateNewerFullCRLNoCRLNumber()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TestUtils.getTestDataDir(), "crls/ThawteSGCCA.crl");

                X509CRL crl = addCRL(crlFile, crlStore);

                assertTrue(crlStore.contains(crl));

                // load a newer CRL that replaces the other CRL
                crlFile = new File(TestUtils.getTestDataDir(), "crls/ThawteSGCCA-thisupdate-211207.crl");

                X509CRL newerCRL = TestUtils.loadX509CRL(crlFile);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                        pKISecurityServices.getCRLPathBuilderFactory(), false);

                crlStoreMaintainer.addCRL(newerCRL);

                assertFalse(crlStore.contains(crl));

                assertTrue(crlStore.contains(newerCRL));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUpdateOlderFullCRLNoCRLNumber()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TestUtils.getTestDataDir(), "crls/ThawteSGCCA-thisupdate-211207.crl");

                X509CRL crl = addCRL(crlFile, crlStore);

                assertTrue(crlStore.contains(crl));

                // load an older CRL that should not replace the other CRL
                crlFile = new File(TestUtils.getTestDataDir(), "crls/ThawteSGCCA.crl");

                X509CRL olderCRL = TestUtils.loadX509CRL(crlFile);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                        pKISecurityServices.getCRLPathBuilderFactory(), false);

                crlStoreMaintainer.addCRL(olderCRL);

                assertTrue(crlStore.contains(crl));

                assertFalse(crlStore.contains(olderCRL));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUpdateNewerFullCRLCRLNumber()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TestUtils.getTestDataDir(),
                        "crls/UTN-USERFirst-ClientAuthenticationandEmail.crl");

                X509CRL crl = addCRL(crlFile, crlStore);

                assertTrue(crlStore.contains(crl));

                // load a newer CRL that replaces the other CRL
                crlFile = new File(TestUtils.getTestDataDir(),
                        "crls/UTN-USERFirst-ClientAuthenticationandEmail-211207.crl");

                X509CRL newerCRL = TestUtils.loadX509CRL(crlFile);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                        pKISecurityServices.getCRLPathBuilderFactory(), false);

                crlStoreMaintainer.addCRL(newerCRL);

                assertFalse(crlStore.contains(crl));

                assertTrue(crlStore.contains(newerCRL));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUpdateOlderFullCRLCRLNumber()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TestUtils.getTestDataDir(),
                        "crls/UTN-USERFirst-ClientAuthenticationandEmail-211207.crl");

                X509CRL crl = addCRL(crlFile, crlStore);

                assertTrue(crlStore.contains(crl));

                // load an older CRL that should not replace the other CRL
                crlFile = new File(TestUtils.getTestDataDir(),
                        "crls/UTN-USERFirst-ClientAuthenticationandEmail.crl");

                X509CRL olderCRL = TestUtils.loadX509CRL(crlFile);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                        pKISecurityServices.getCRLPathBuilderFactory(), false);

                crlStoreMaintainer.addCRL(olderCRL);

                assertTrue(crlStore.contains(crl));

                assertFalse(crlStore.contains(olderCRL));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUpdateDifferentReasons()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TestUtils.getTestDataDir(),
                        "PKITS/crls/onlySomeReasonsCA1compromiseCRL.crl");

                X509CRL crl = addCRL(crlFile, crlStore);

                assertTrue(crlStore.contains(crl));

                crlFile = new File(TestUtils.getTestDataDir(),
                        "PKITS/crls/onlySomeReasonsCA1otherreasonsCRL.crl");

                X509CRL newerCRL = TestUtils.loadX509CRL(crlFile);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                        pKISecurityServices.getCRLPathBuilderFactory(), false);

                crlStoreMaintainer.addCRL(newerCRL);

                // they must both be in the store because they have a different scope
                assertTrue(crlStore.contains(crl));
                assertTrue(crlStore.contains(newerCRL));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUpdateDeltaAndBase()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // first add delta
                File crlFile = new File(TestUtils.getTestDataDir(),
                        "PKITS/crls/deltaCRLCA1deltaCRL.crl");

                X509CRL crl = addCRL(crlFile, crlStore);

                assertTrue(crlStore.contains(crl));

                crlFile = new File(TestUtils.getTestDataDir(),
                        "PKITS/crls/deltaCRLCA1CRL.crl");

                // now add the older base
                X509CRL olderCRL = TestUtils.loadX509CRL(crlFile);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                        pKISecurityServices.getCRLPathBuilderFactory(), false);

                crlStoreMaintainer.addCRL(olderCRL);

                // they must both be in the store because one is the base and the other the delta
                assertTrue(crlStore.contains(crl));
                assertTrue(crlStore.contains(olderCRL));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUpdateBaseAndDelta()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add base
                File crlFile = new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1CRL.crl");

                X509CRL crl = addCRL(crlFile, crlStore);

                assertTrue(crlStore.contains(crl));

                // add delta
                crlFile = new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1deltaCRL.crl");

                X509CRL deltaCRL = TestUtils.loadX509CRL(crlFile);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(crlStore,
                        pKISecurityServices.getCRLPathBuilderFactory(), false);

                crlStoreMaintainer.addCRL(deltaCRL);

                // they must both be in the store because one is the base and the other the delta
                assertTrue(crlStore.contains(crl));
                assertTrue(crlStore.contains(deltaCRL));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
