/*
 * Copyright (c) 2010-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 *
 * @author Martijn Brinkers
 *
 */
public class StringIteratorTest
{
    @Test
    public void testUTF8()
    throws IOException
    {
        byte[] b = "Test 123äöüÄÖÜ".getBytes(StandardCharsets.UTF_8);

        StringIterator si = new StringIterator(new ByteArrayInputStream(b), 2, "UTF-8");

        assertEquals("Te", si.getNext());
        assertEquals("st", si.getNext());
        assertEquals(" 1", si.getNext());
        assertEquals("23", si.getNext());
        assertEquals("äö", si.getNext());
        assertEquals("üÄ", si.getNext());
        assertEquals("ÖÜ", si.getNext());
        assertNull(si.getNext());
    }

    @Test
    public void testASCII()
    throws IOException
    {
        byte[] b = "Test 123äöüÄÖÜ".getBytes(StandardCharsets.US_ASCII);

        StringIterator si = new StringIterator(new ByteArrayInputStream(b), 100, "US-ASCII");

        assertEquals("Test 123??????", si.getNext());
        assertNull(si.getNext());
    }

    @Test
    public void testMaxInitBuffer()
    throws IOException
    {
        byte[] b = "Test 123".getBytes(StandardCharsets.US_ASCII);

        StringIterator si = new StringIterator(new ByteArrayInputStream(b), 100, "US-ASCII", 1);

        assertEquals("Test 123", si.getNext());
        assertNull(si.getNext());
    }
}
