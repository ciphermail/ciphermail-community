/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certpath;

import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableInt;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CertPathBuilderSpeedTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt certStore;

    @Autowired
    private X509CRLStoreExt crlStore;

    @Autowired
    private CertificatePathBuilderFactory certificatePathBuilderFactory;

    @Before
    public void setup()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                rootStore.removeAllEntries();
                certStore.removeAllEntries();

                importCRL("test-ca.crl", crlStore);
                importCRL("test-root-ca-not-revoked.crl", crlStore);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // add roots
        importCertificatesPerTransaction("windows-xp-all-roots.p7b", rootStore);
        importCertificatesPerTransaction("mitm-test-root.cer", rootStore);

        long start = System.currentTimeMillis();

        int imported = importCertificatesPerTransaction("random-self-signed-1000.p7b", certStore);

        assertEquals(1000, imported);

        long diff = System.currentTimeMillis() - start;

        double timePerCertificate = diff * 0.001 / imported;

        System.out.println("Import seconds/certificate : " + timePerCertificate);

        double expected = 0.02 / TestProperties.getTestPerformanceFactor();

        if (timePerCertificate > expected)
        {
            // Note: This might fail on slower systems!!
            fail("Import seconds/certificate too slow. Expected: " + expected + " but was: " +
            timePerCertificate + ". Note: This might fail on slower systems!!!");
        }

        importCertificatesPerTransaction("mitm-test-ca.cer", certStore);
        importCertificatesPerTransaction("testCertificates.p7b", certStore);
    }

    /*
     * When importing a large number of certificates, using a transaction per certificate is faster than one large
     * transaction
     */
    private int importCertificatesPerTransaction(String certificateFilename, X509CertStoreExt store)
    throws Exception
    {
        MutableInt imported = new MutableInt();

        File file = new File(TestUtils.getTestDataDir(), "certificates/" + certificateFilename);

        for (Certificate certificate : CertificateUtils.readCertificates(file))
        {
            if (certificate instanceof X509Certificate)
            {
                transactionOperations.executeWithoutResult(status ->
                {
                    try {
                        if (!store.contains((X509Certificate) certificate))
                        {
                            store.addCertificate((X509Certificate) certificate);

                            imported.increment();
                        }
                    }
                    catch (Exception e) {
                        throw new UnhandledException(e);
                    }
                });
            }
        }

        return imported.intValue();
    }

    private void importCRL(String crlFilename, X509CRLStoreExt crlStore)
    throws Exception
    {
        File crlFile = new File(TestUtils.getTestDataDir(), "crls/" + crlFilename);

        X509CRL crl = TestUtils.loadX509CRL(crlFile);

        if (!crlStore.contains(crl)) {
            crlStore.addCRL(crl);
        }
    }

    @Test
    public void testLoadCertificates()
    {
        long start = System.currentTimeMillis();

        X509CertSelector selector = new X509CertSelector();

        MutableInt i = new MutableInt();

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CloseableIterator<X509Certificate> it = certStore.getCertificateIterator(selector);

                while (it.hasNext())
                {
                    X509Certificate certificate = it.next();

                    assertNotNull(certificate);

                    i.increment();

                    if (i.intValue() == 10000) {
                        break;
                    }
                }
                // close it again (should not be a problem)
                it.close();

                assertTrue(it.isClosed());

                // close it again (should not be a problem)
                it.close();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        double secondsPerCertificate = ((System.currentTimeMillis() - start) * 0.001 / i.intValue());

        System.out.println("Total certificates: " + i + ". seconds / certificate: " + secondsPerCertificate);

        double expected = 9E-4 / TestProperties.getTestPerformanceFactor();

        if (secondsPerCertificate > expected)
        {
            // Note: This might fail on slower systems!!
            fail("Seconds / certificate too slow. Expected: " + expected + " but was: " + secondsPerCertificate +
                    ". Note: This might fail on slower systems!!!");
        }
    }

    /*
     * Test path building for a certificate with critical extended key usage containing EMAILPROTECTION when
     * SMIMEExtendedKeyUsageCertPathChecker is added to the path checkers
     */
    @Test
    public void testBuildPathManyCertificates()
    throws Exception
    {
        int tries = 10;

        long start = System.currentTimeMillis();

        for (int i = 0; i < tries; i++)
        {
            X509CertSelector selector = new X509CertSelector();

            selector.setSerialNumber(BigIntegerUtils.hexDecode("116A448F117FF69FE4F2D4D38F689D7"));
            selector.setIssuer(new X500Principal(
                    "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

            CertificatePathBuilder pathBuilder = certificatePathBuilderFactory.createCertificatePathBuilder();


            CertPathBuilderResult  result =
                transactionOperations.execute(status ->
                {
                    try {
                        return pathBuilder.buildPath(selector);
                    }
                    catch (Exception e) {
                        throw new UnhandledException(e);
                    }
                });

            assertEquals(2, result.getCertPath().getCertificates().size());
        }

        long diff = System.currentTimeMillis() - start;

        double secondsPerBuild = diff * 0.001 / tries;

        System.out.println("Seconds / build: " + secondsPerBuild);

        double expected = 0.05 / TestProperties.getTestPerformanceFactor();

        if (secondsPerBuild > expected)
        {
            // Note: This might fail on slower systems!!
            fail("Seconds / build too slow. Expected: " + expected + " but was: " + secondsPerBuild +
                    ". Note: This might fail on slower systems!!!");
        }
    }
}
