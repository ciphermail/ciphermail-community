/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPException;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PGPContentSignatureVerifierImplTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testValidate()
    throws Exception
    {
        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/martijn@djigzo.com_0x271AD23B.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed.eml"));

        BodyPart[] parts = PGPMIMEUtils.dissectPGPMIMESigned((Multipart) message.getContent());

        PGPSignatureVerifierResult result = verifier.verify(
                new ByteArrayInputStream(MailUtils.partToByteArray(parts[0])),
                new ByteArrayInputStream(MailUtils.partToByteArray(parts[1])));

        assertTrue(result.isValid());
        assertEquals("4605970C271AD23B", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testValidateTextOnly()
    throws Exception
    {
        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/martijn@djigzo.com_0x271AD23B.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed-with-attachment.eml"));

        BodyPart[] parts = PGPMIMEUtils.dissectPGPMIMESigned((Multipart) message.getContent());

        PGPSignatureVerifierResult result = verifier.verify(
                new ByteArrayInputStream(MailUtils.partToByteArray(parts[0])),
                new ByteArrayInputStream(MailUtils.partToByteArray(parts[1])));

        assertTrue(result.isValid());
        assertEquals("4605970C271AD23B", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testValidateKeyNotFound()
    throws Exception
    {
        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/test@example.com.gpg.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed.eml"));

        BodyPart[] parts = PGPMIMEUtils.dissectPGPMIMESigned((Multipart) message.getContent());

        try {
            verifier.verify(
                    new ByteArrayInputStream(MailUtils.partToByteArray(parts[0])),
                    new ByteArrayInputStream(MailUtils.partToByteArray(parts[1])));
        }
        catch(PGPException e) {
            assertEquals("Signer's key with key ID 4605970C271AD23B not found.", e.getMessage());
        }
    }

    @Test
    public void testTampered()
    throws Exception
    {
        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/martijn@djigzo.com_0x271AD23B.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed-tampered.eml"));

        BodyPart[] parts = PGPMIMEUtils.dissectPGPMIMESigned((Multipart) message.getContent());

        PGPSignatureVerifierResult result = verifier.verify(
                new ByteArrayInputStream(MailUtils.partToByteArray(parts[0])),
                new ByteArrayInputStream(MailUtils.partToByteArray(parts[1])));

        assertFalse(result.isValid());
        assertEquals("4605970C271AD23B", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }
}
