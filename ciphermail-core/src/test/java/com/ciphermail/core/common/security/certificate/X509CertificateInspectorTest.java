/*
 * Copyright (c) 2008-2016, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class X509CertificateInspectorTest
{
    private static final File TEST_BASE_DIR = new File(TestUtils.getTestDataDir(), "certificates/");

    @Test
    public void testEmailTest()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "multipleemail.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        assertEquals(10, inspector.getEmail().size());
        assertEquals(5, inspector.getEmailFromDN().size());
        assertEquals(5, inspector.getEmailFromAltNames().size());
    }

    @Test
    public void testSHA1Thumbprint()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "testcertificate.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        assertEquals("1A6C675F9D8AF742219D9DBE7C65003944A7766B", inspector.getThumbprint(Digest.SHA1));
    }

    @Test
    public void testSHA1HashStarts0()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "hash-starts-with-0.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        assertEquals("05818393AAE1765AD32F1D78C157220147469091E9B1BA92E6C94E687A679BEB5FA85CC10585FC5090283602EEAD6595A91C993A3D4ED99D6F517EF2ED7DC73E",
                inspector.getThumbprint());
    }


    @Test
    public void testGetSerialNumber()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "dod-mega-crl.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        assertEquals("30929E", inspector.getSerialNumberHex());
    }

    @Test
    public void testGetSerialNumber2()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "testcertificate.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        assertEquals("52DC00010002B2BB74749401F475", inspector.getSerialNumberHex());
    }

    @Test
    public void testSubjectKeyIdentifier()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "testcertificate.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        assertNull(inspector.getSubjectKeyIdentifierHex());

        certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "subjectkeyident.cer"));

        inspector = new X509CertificateInspector(certificate);

        assertEquals("256E864B45E603AF649B577F5CDD63B0850A513E", inspector.getSubjectKeyIdentifierHex());

        certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "equifax.cer"));

        inspector = new X509CertificateInspector(certificate);

        assertEquals("48E668F92BD2B295D747D82320104F3398909FD4", inspector.getSubjectKeyIdentifierHex());
    }

    @Test
    public void testKeyUsageEmpty()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR,
                "No_key_usage_MITM_Test_CA.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        Set<KeyUsageType> keyUsage = inspector.getKeyUsage();

        assertNull(keyUsage);
    }

    @Test
    public void testKeyUsage2()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "chinesechars.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        Set<KeyUsageType> keyUsage = inspector.getKeyUsage();

        assertEquals(2, keyUsage.size());

        assertTrue(keyUsage.contains(KeyUsageType.DIGITALSIGNATURE));
        assertTrue(keyUsage.contains(KeyUsageType.KEYENCIPHERMENT));
    }

    @Test
    public void testExtendedKeyUsage()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "mitm-test-ca.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        Set<ExtendedKeyUsageType> keyUsage = inspector.getExtendedKeyUsage();

        assertEquals(2, keyUsage.size());

        assertTrue(keyUsage.contains(ExtendedKeyUsageType.EMAILPROTECTION));
        assertTrue(keyUsage.contains(ExtendedKeyUsageType.OCSPSIGNING));
    }

    @Test
    public void testExtendedKeyUsageEmpty()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "rim.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        Set<ExtendedKeyUsageType> keyUsage = inspector.getExtendedKeyUsage();

        assertNull(keyUsage);
    }

    @Test
    public void testExpired()
    throws Exception
    {
        X509Certificate certificate = Objects.requireNonNull(TestUtils.loadCertificate(
                new File(TEST_BASE_DIR, "mitm-test-ca.cer")));

        assertFalse(X509CertificateInspector.isExpired(certificate));

        certificate = Objects.requireNonNull(TestUtils.loadCertificate(new File(TEST_BASE_DIR,
                "Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer")));

        assertTrue(X509CertificateInspector.isExpired(certificate));
    }

    @Test
    public void testCalculateSubjectKeyIdentifier()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "subjectkeyident.cer"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        assertEquals("256E864B45E603AF649B577F5CDD63B0850A513E", inspector.getSubjectKeyIdentifierHex());

        SubjectKeyIdentifier subjectKeyIdentifier = inspector.calculateSubjectKeyIdentifier();

        String skiHex = HexUtils.hexEncode(subjectKeyIdentifier.getKeyIdentifier());

        assertEquals("256E864B45E603AF649B577F5CDD63B0850A513E", skiHex);

        certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR, "AC_MINEFI_DPMA.cer"));

        inspector = new X509CertificateInspector(certificate);

        subjectKeyIdentifier = inspector.calculateSubjectKeyIdentifier();

        skiHex = HexUtils.hexEncode(subjectKeyIdentifier.getKeyIdentifier());

        assertEquals(skiHex, inspector.getSubjectKeyIdentifierHex());
    }

    /*
     * tests the Microsoft invented alternative way to calculate the subjectKeyIdentifier
     */
    @Test
    public void testCalculateEncodedSubjectKeyIdentifier2()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR,
                "outlook2010_cert_missing_subjkeyid.pem"));

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        byte[] subjectKeyIdentifier = inspector.calculateSubjectKeyIdentifierMicrosoft();

        String skiHex = HexUtils.hexEncode(subjectKeyIdentifier);

        assertEquals("2219E504D5750B37D20CC930B14129E1A2E583B1", skiHex);
    }

    /*
     * tests getting the issuerAlternativeNames which fails on OpenJDK 7.
     *
     * The certificate contains invalid data (at least invalid according to RFC 5280). The invalid data was silently
     * ignored with OpenJDK 6 but OpenJDK 7 seems to be more strict (the Virtual Appliance by default uses OpenJDK 6).
     *
     * Details:
     *
     * The IssuerAltName extension is defined in RFC 5280 as:
     *
     * IssuerAltName ::= GeneralNames
     *
     * GeneralNames ::= SEQUENCE SIZE (1..MAX) OF GeneralName
     *
     * So there should be at least one GeneralName if the IssuerAltName extension is defined. The certificate in
     * question however contains an empty IssuerAltName sequence. This is not allowed. In Java 6, this was silently
     * discarded but Java 7 seems to be more strict.
     *
     * For a similar report see https://bugzilla.redhat.com/show_bug.cgi?format=multiple&id=441801.
     *
     * Note: This test fails on Java 7 because on Java 7, an exception is thrown
     */
    @Test
    public void testIssuerAlternativeNamesOpenJDKFailure()
    throws Exception
    {
        X509Certificate certificate = Objects.requireNonNull(TestUtils.loadCertificate(new File(TEST_BASE_DIR,
                "issuer-alt-names-fail-openjdk7.cer")));

        assertNull(certificate.getIssuerAlternativeNames());
    }

    @Test
    public void testIsSelfSigned()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR,
                "equifax.cer"));

        assertTrue(X509CertificateInspector.isSelfSigned(certificate));
        assertTrue(new X509CertificateInspector(certificate).isSelfSigned());

        certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR,
                "mitm-test-root.cer"));

        assertTrue(X509CertificateInspector.isSelfSigned(certificate));
        assertTrue(new X509CertificateInspector(certificate).isSelfSigned());

        certificate = TestUtils.loadCertificate(new File(TEST_BASE_DIR,
                "testcertificate.cer"));

        assertFalse(X509CertificateInspector.isSelfSigned(certificate));
        assertFalse(new X509CertificateInspector(certificate).isSelfSigned());
    }
}
