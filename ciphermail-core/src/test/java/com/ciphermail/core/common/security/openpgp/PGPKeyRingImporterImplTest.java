/*
 * Copyright (c) 2014-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPKeyRingImporterImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    private PGPKeyRingImporterImpl importer;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        importer = new PGPKeyRingImporterImpl(keyRing);
    }

    private List<PGPKeyRingEntry> importPublicKeyRing(File file)
    throws Exception
    {
        return importer.importKeyRing(new FileInputStream(file), null);
    }

    private List<PGPKeyRingEntry> importKeyRing(InputStream input, String importPassword)
    throws Exception
    {
        return importer.importKeyRing(input, new StaticPasswordProvider(importPassword));
    }

    private List<PGPKeyRingEntry> importKeyRing(InputStream input, String importPassword,
            boolean ignoreParsingErrors)
    throws Exception
    {
        return importer.importKeyRing(input, new StaticPasswordProvider(importPassword), ignoreParsingErrors);
    }

    private List<PGPKeyRingEntry> getAllEntries()
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getIterator(new PGPSearchParameters(),
                null, null);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        return list;
    }

    private PGPKeyRingEntry getByKeyID(long keyID)
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getByKeyID(keyID, MissingKeyAlias.ALLOWED);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        // Assume we only have one hit for keyID
        assertTrue(list.size() <= 1);

        return !list.isEmpty() ? list.get(0) : null;
    }

    @Test
    public void testImportSecretKeyRing()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TestUtils.getTestDataDir(),
                        "pgp/test-multiple-sub-keys.gpg.key");

                List<PGPKeyRingEntry> imported = importKeyRing(new FileInputStream(file), "test");

                assertEquals(6, imported.size());

                assertEquals(6, keyRing.getSize());

                List<PGPKeyRingEntry> entries = getAllEntries();

                assertEquals(6, entries.size());

                for (PGPKeyRingEntry entry : entries)
                {
                    assertNotNull(entry.getPublicKey());
                    assertNotNull(entry.getPrivateKey());
                    assertTrue(imported.contains(entry));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testImportPublicKeyRing()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TestUtils.getTestDataDir(), "pgp/test-multiple-sub-keys.gpg.asc");

                List<PGPKeyRingEntry> imported = importer.importKeyRing(new FileInputStream(file), null);

                assertEquals(6, imported.size());

                assertEquals(6, keyRing.getSize());

                List<PGPKeyRingEntry> entries = getAllEntries();

                assertEquals(6, entries.size());

                for (PGPKeyRingEntry entry : entries) {
                    assertTrue(imported.contains(entry));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testImportPublicKeyRingTwice()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TestUtils.getTestDataDir(), "pgp/test-multiple-sub-keys.gpg.asc");

                List<PGPKeyRingEntry> imported = importer.importKeyRing(new FileInputStream(file), null);

                assertEquals(6, imported.size());

                imported = importer.importKeyRing(new FileInputStream(file), null);

                assertEquals(0, imported.size());

                assertEquals(6, keyRing.getSize());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testImportUpdatedKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TestUtils.getTestDataDir(), "pgp/test@example.com.gpg.key");

                List<PGPKeyRingEntry> imported = importKeyRing(new FileInputStream(file), "test");

                assertEquals(2, imported.size());
                assertEquals(2, keyRing.getSize());

                file = new File(TestUtils.getTestDataDir(), "pgp/test@example.com-additional-userid.gpg.asc");

                imported = importPublicKeyRing(file);

                assertEquals(1, imported.size());
                assertEquals(2, keyRing.getSize());

                file = new File(TestUtils.getTestDataDir(), "pgp/test@example.com-revoked.gpg.key");

                imported = importPublicKeyRing(file);

                assertEquals(1, imported.size());
                assertEquals(2, keyRing.getSize());

                assertNotNull(getByKeyID(imported.get(0).getKeyID()).getPrivateKey());
                assertNotNull(imported.get(0).getPrivateKey());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testImportPublicKeysAndSecretKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-export-from-enigmail-with-key.asc");

                List<PGPKeyRingEntry> imported = importKeyRing(new FileInputStream(file), "test");

                assertEquals(4, imported.size());

                List<PGPKeyRingEntry> entries = getAllEntries();

                assertEquals(2, entries.size());

                for (PGPKeyRingEntry entry : entries)
                {
                    assertNotNull(entry.getPublicKey());
                    assertNotNull(entry.getPrivateKey());
                    assertTrue(imported.contains(entry));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSubkeyBindingSigOnMaster()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                // The master key contains a subkey binding signature on itself
                File file = new File(TestUtils.getTestDataDir(), "pgp/subkey-binding-sig-on-master.asc");

                List<PGPKeyRingEntry> imported = importKeyRing(new FileInputStream(file), "test");

                assertEquals(2, imported.size());

                List<PGPKeyRingEntry> entries = getAllEntries();

                assertEquals(2, entries.size());

                for (PGPKeyRingEntry entry : entries)
                {
                    // Check if parent does not point to self (due to the sub key binding sig to self)
                    if (entry.isMasterKey()) {
                        assertNull(entry.getParentKey());
                    }
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * Test that checks if PGP key with invalid UTF User ID gets imported (failed with old BC version)
     */
    @Test
    public void testImportPublicKeyRingInvalidUTF16()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TestUtils.getTestDataDir(), "pgp/invalid_UTF16_codepoint.asc");

                List<PGPKeyRingEntry> imported = importPublicKeyRing(file);

                assertEquals(2, imported.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testImportPrivateKeysAndPublicKeyRing()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                PGPSecretKeyRingGeneratorImpl generator = new PGPSecretKeyRingGeneratorImpl();

                PrivateKeysAndPublicKeyRing privateKeysAndPublicKeyRing = generator.generateKeyRing(
                        PGPKeyGenerationType.RSA_2048, "test key <test@example.com>");

                List<PGPKeyRingEntry> imported = importer.importKeyRing(privateKeysAndPublicKeyRing);

                assertEquals(2, imported.size());

                List<PGPKeyRingEntry> entries = getAllEntries();

                assertEquals(2, entries.size());

                for (PGPKeyRingEntry entry : entries)
                {
                    assertNotNull(entry.getPublicKey());
                    assertNotNull(entry.getPrivateKey());
                    assertTrue(imported.contains(entry));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testImportSecretKeyRingInvalidPasswordIgnoreParsingErrors()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TestUtils.getTestDataDir(), "pgp/test-multiple-sub-keys.gpg.key");

                List<PGPKeyRingEntry> imported = importKeyRing(new FileInputStream(file), "invalid");

                assertEquals(0, imported.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testImportSecretKeyRingInvalidPasswordDoNotIgnoreParsingErrors()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TestUtils.getTestDataDir(), "pgp/test-multiple-sub-keys.gpg.key");

                try {
                    importKeyRing(new FileInputStream(file), "invalid", false);

                    fail();
                }
                catch (IOException e) {
                    assertEquals("Error parsing PGP keypair with key ID D80D1572D0486F55", e.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
