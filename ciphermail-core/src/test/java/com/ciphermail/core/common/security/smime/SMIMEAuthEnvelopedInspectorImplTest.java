/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.keystore.KeyStoreKeyProvider;
import com.ciphermail.core.test.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;

class SMIMEAuthEnvelopedInspectorImplTest
{
    private static SecurityFactory securityFactory;
    private static KeyStoreKeyProvider keyStoreKeyProvider;

    @BeforeAll
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        KeyStore keyStore = loadKeyStore(new File(TestUtils.getTestDataDir(), "keys/testCertificates.p12"),
                "test");

        keyStoreKeyProvider = new KeyStoreKeyProvider(keyStore, "test");
    }

    private static KeyStore loadKeyStore(File file, String password)
    throws Exception
    {
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());

        return keyStore;
    }

    @Test
    void testAES256CGM()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/smime-encrypted-gcm-256.eml");

        SMIMEEnvelopedInspector inspector = new SMIMEAuthEnvelopedInspectorImpl(message,  keyStoreKeyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        Assertions.assertNotNull(decrypted);
        Assertions.assertTrue(decrypted.isMimeType("multipart/mixed"));

        MailUtils.validateMessage(decrypted);

        String mime = MailUtils.partToMimeString(decrypted);

        Assertions.assertTrue(mime.contains("filename=\"cityinfo.zip\""));

        Assertions.assertEquals("2.16.840.1.101.3.4.1.46", inspector.getEncryptionAlgorithmOID());

        SMIMEEncryptionAlgorithm encryptionAlg = SMIMEEncryptionAlgorithm.fromOID(inspector.getEncryptionAlgorithmOID());

        Assertions.assertEquals(SMIMEEncryptionAlgorithm.AES256_GCM, encryptionAlg);

        Assertions.assertEquals(256, SMIMEEncryptionAlgorithm.getKeySize(encryptionAlg));
    }

    @Test
    void testOpenSSLAES128CGM()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/smime-encrypted-openssl-aes128-gcm.eml");

        SMIMEEnvelopedInspector inspector = new SMIMEAuthEnvelopedInspectorImpl(message,  keyStoreKeyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        Assertions.assertNotNull(decrypted);

        MailUtils.validateMessage(decrypted);

        String mime = MailUtils.partToMimeString(decrypted);

        System.out.println(mime);

        Assertions.assertEquals("test 123", mime.trim());

        Assertions.assertEquals("2.16.840.1.101.3.4.1.6", inspector.getEncryptionAlgorithmOID());

        SMIMEEncryptionAlgorithm encryptionAlg = SMIMEEncryptionAlgorithm.fromOID(inspector.getEncryptionAlgorithmOID());

        Assertions.assertEquals(SMIMEEncryptionAlgorithm.AES128_GCM, encryptionAlg);

        Assertions.assertEquals(128, SMIMEEncryptionAlgorithm.getKeySize(encryptionAlg));
    }

    @Test
    void testOpenSSLAES192CGM()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/smime-encrypted-openssl-aes192-gcm.eml");

        SMIMEEnvelopedInspector inspector = new SMIMEAuthEnvelopedInspectorImpl(message,  keyStoreKeyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        Assertions.assertNotNull(decrypted);

        MailUtils.validateMessage(decrypted);

        String mime = MailUtils.partToMimeString(decrypted);

        System.out.println(mime);

        Assertions.assertEquals("test 123", mime.trim());

        Assertions.assertEquals("2.16.840.1.101.3.4.1.26", inspector.getEncryptionAlgorithmOID());

        SMIMEEncryptionAlgorithm encryptionAlg = SMIMEEncryptionAlgorithm.fromOID(inspector.getEncryptionAlgorithmOID());

        Assertions.assertEquals(SMIMEEncryptionAlgorithm.AES192_GCM, encryptionAlg);

        Assertions.assertEquals(192, SMIMEEncryptionAlgorithm.getKeySize(encryptionAlg));
    }

    @Test
    void testOpenSSLAES256CGM()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/smime-encrypted-openssl-aes256-gcm.eml");

        SMIMEEnvelopedInspector inspector = new SMIMEAuthEnvelopedInspectorImpl(message,  keyStoreKeyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        Assertions.assertNotNull(decrypted);

        MailUtils.validateMessage(decrypted);

        String mime = MailUtils.partToMimeString(decrypted);

        System.out.println(mime);

        Assertions.assertEquals("test 123", mime.trim());

        Assertions.assertEquals("2.16.840.1.101.3.4.1.46", inspector.getEncryptionAlgorithmOID());

        SMIMEEncryptionAlgorithm encryptionAlg = SMIMEEncryptionAlgorithm.fromOID(inspector.getEncryptionAlgorithmOID());

        Assertions.assertEquals(SMIMEEncryptionAlgorithm.AES256_GCM, encryptionAlg);

        Assertions.assertEquals(256, SMIMEEncryptionAlgorithm.getKeySize(encryptionAlg));
    }
}
