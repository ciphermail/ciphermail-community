/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.mail.MessageParser.PartFilter;
import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MessageParserTest
{
    @Test
    public void testSimpleMessageWithAttachment()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        MessageParser parser = new MessageParser(false);

        parser.parseMessage(message);

        assertEquals(1, parser.getAttachments().size());
        assertEquals(1, parser.getInlineTextParts().size());
        assertEquals("Test", parser.getInlineTextParts().get(0).getContent());
    }

    @Test
    public void testGetMultipartAlternativeRelated()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/multipart_alternative_related.eml");

        MessageParser parser = new MessageParser(false);

        parser.parseMessage(message);

        assertEquals(0, parser.getAttachments().size());
        assertEquals(1, parser.getInlineTextParts().size());
        assertTrue(((String) parser.getInlineTextParts().get(0).getContent()).contains(
                "Workshop 'ICT-contracten in de praktijk:"));
    }

    @Test
    public void testGetClearSigned()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-validcertificate.eml");

        MessageParser parser = new MessageParser(false);

        parser.parseMessage(message);

        assertEquals(2, parser.getAttachments().size());
        assertEquals("application/octet-stream;\r\n\tname=\"cityinfo.zip\"",
                parser.getAttachments().get(0).getContentType());
        assertEquals("application/pkcs7-signature; name=smime.p7s; smime-type=signed-data",
                parser.getAttachments().get(1).getContentType());
        assertEquals(1, parser.getInlineTextParts().size());
        assertEquals("Test", parser.getInlineTextParts().get(0).getContent());
    }

    @Test
    public void testPartFilter()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-validcertificate.eml");

        PartFilter partFilter = (parent, part, context) ->
        {
            try {
                return !part.isMimeType("application/pkcs7-signature");
            }
            catch (MessagingException e) {
                throw new PartException(e);
            }
        };

        MessageParser parser = new MessageParser(false);

        parser.setPartFilter(partFilter);

        parser.parseMessage(message);

        assertEquals(1, parser.getAttachments().size());
        assertEquals("application/octet-stream;\r\n\tname=\"cityinfo.zip\"",
                parser.getAttachments().get(0).getContentType());
        assertEquals(1, parser.getInlineTextParts().size());
        assertEquals("Test", parser.getInlineTextParts().get(0).getContent());
    }

    @Test
    public void testHTMLOnly()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/html-only.eml");

        MessageParser parser = new MessageParser(false);

        parser.parseMessage(message);

        assertEquals(1, parser.getAttachments().size());
        assertEquals("text/html; charset=ISO-8859-9", parser.getAttachments().get(0).getContentType());
        assertEquals(0, parser.getInlineTextParts().size());
    }

    @Test
    public void testTextOnly()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/attachment-uuencoded.eml");

        MessageParser parser = new MessageParser(false);

        parser.parseMessage(message);

        assertEquals(0, parser.getAttachments().size());
        assertEquals(1, parser.getInlineTextParts().size());
        assertTrue(((String) parser.getInlineTextParts().get(0).getContent()).contains(
                "begin 644 james-project-logo.gif"));
    }

    @Test
    public void testHTMLAlternative()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/html-alternative.eml");

        MessageParser parser = new MessageParser(false);

        parser.parseMessage(message);

        assertEquals(0, parser.getAttachments().size());
        assertEquals(1, parser.getInlineTextParts().size());
        assertTrue(((String) parser.getInlineTextParts().get(0).getContent()).contains(
                " * Loredana Niculae: How is your marketing"));
    }

    @Test
    public void testHTMLAlternativeDeepScan()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/html-alternative.eml");

        MessageParser parser = new MessageParser(true);

        parser.parseMessage(message);

        assertEquals(1, parser.getAttachments().size());
        assertEquals(1, parser.getInlineTextParts().size());
        assertTrue(((String) parser.getInlineTextParts().get(0).getContent()).contains(
                " * Loredana Niculae: How is your marketing"));
    }

    @Test
    public void testAttachmentNoBody()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/only-attachment.eml");

        MessageParser parser = new MessageParser(false);

        parser.parseMessage(message);

        assertEquals(1, parser.getAttachments().size());
        assertEquals("application/octet-stream;\r\n\tname=\"cityinfo.zip\"",
                parser.getAttachments().get(0).getContentType());
        assertEquals(0, parser.getInlineTextParts().size());
    }

    @Test
    public void testTextAttachment()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/text-attachment.eml");

        MessageParser parser = new MessageParser(false);

        parser.parseMessage(message);

        assertEquals(1, parser.getAttachments().size());
        assertEquals("text/plain; charset=UTF-8;\r\n name=\"test.txt\"",
                parser.getAttachments().get(0).getContentType());
        assertEquals(1, parser.getInlineTextParts().size());
        assertEquals("\r\n--\r\nDJIGZO email encryption\r\n", parser.getInlineTextParts().get(0).getContent());
    }

    @Test
    public void testTextAttachmentInline()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/text-attachment-inline.eml");

        MessageParser parser = new MessageParser(false);

        parser.setMaxInlineSize(-1);
        parser.parseMessage(message);

        assertEquals(0, parser.getAttachments().size());
        assertEquals(2, parser.getInlineTextParts().size());
        assertEquals("\r\n--\r\nDJIGZO email encryption\r\n", parser.getInlineTextParts().get(0).getContent());
        assertEquals("test 123\r\n", parser.getInlineTextParts().get(1).getContent());
    }

    @Test
    public void testTextAttachmentInlineMaxInlineSizeExceeded()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/text-attachment-inline.eml");

        MessageParser parser = new MessageParser(false);

        parser.setMaxInlineSize(10);
        parser.parseMessage(message);

        assertEquals(1, parser.getAttachments().size());
        assertEquals(1, parser.getInlineTextParts().size());
        assertEquals("\r\n--\r\nDJIGZO email encryption\r\n", parser.getAttachments().get(0).getContent());
        assertEquals("test 123\r\n", parser.getInlineTextParts().get(0).getContent());
    }

    @Test
    public void testTextAttachmentInlineMaxInlineSizeExceededAll()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/text-attachment-inline.eml");

        MessageParser parser = new MessageParser(false);

        parser.setMaxInlineSize(0);
        parser.parseMessage(message);

        assertEquals(2, parser.getAttachments().size());
        assertEquals(0, parser.getInlineTextParts().size());
        assertEquals("\r\n--\r\nDJIGZO email encryption\r\n", parser.getAttachments().get(0).getContent());
        assertEquals("test 123\r\n", parser.getAttachments().get(1).getContent());
    }

    @Test
    public void testAlternativeWithMixedPartInHTMLNoDeepScan()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/alternative-with-mixed-inside-html-part.eml");

        MessageParser parser = new MessageParser(false);

        parser.parseMessage(message);

        assertEquals(0, parser.getAttachments().size());
        assertEquals(1, parser.getInlineTextParts().size());
        assertEquals("The text part\n", parser.getInlineTextParts().get(0).getContent());
    }

    @Test
    public void testAlternativeWithMixedPartInHTMLDeepScan()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/alternative-with-mixed-inside-html-part.eml");

        MessageParser parser = new MessageParser(true);

        parser.parseMessage(message);

        assertEquals(4, parser.getAttachments().size());
        assertEquals(2, parser.getInlineTextParts().size());
        assertEquals("The text part\n", parser.getInlineTextParts().get(0).getContent());
        assertEquals("Second text part\n", parser.getInlineTextParts().get(1).getContent());
    }
}
