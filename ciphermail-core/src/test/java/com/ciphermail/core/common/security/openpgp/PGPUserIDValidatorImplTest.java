/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class PGPUserIDValidatorImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static final PGPUserIDValidator userIDValidator = new PGPUserIDValidatorImpl(
            new PGPSignatureValidatorImpl());

    @Test
    public void testValidateUserID()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(masterKey);

        assertEquals(1, userIDs.size());

        for (byte[] userID : userIDs) {
            assertTrue(userIDValidator.validateUserID(userID, masterKey));
        }
    }

    @Test
    public void testValidateUserIDWrongKeyNoSignatures()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        PGPPublicKey subKey = keys.get(1);

        assertFalse(subKey.isMasterKey());

        List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(masterKey);

        assertEquals(1, userIDs.size());

        for (byte[] userID : userIDs) {
            assertFalse(userIDValidator.validateUserID(userID, subKey));
        }
    }

    @Test
    public void testValidateUserIDRevocationSignature()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-revoked-userid.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        byte[] userID = PGPUtils.userIDToByteArray(
                "Revoked User ID (This User ID will be revoked) <test123@example.com>");

        try {
            userIDValidator.validateUserID(userID, masterKey);

            fail();
        }
        catch (PGPUserIDRevokedException e)
        {
            assertEquals("User ID 'Revoked User ID (This User ID will be revoked) <test123@example.com>' " +
            		"from Key with ID 4EC4E8813E11A9A0 is revoked.", e.getMessage());
            assertEquals("Revoked User ID (This User ID will be revoked) <test123@example.com>", e.getUserID());
            assertEquals("4EC4E8813E11A9A0", PGPUtils.getKeyIDHex(e.getKeyID()));
        }
    }

    @Test
    public void testValidateUserIDNotAllSelfSigned()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "re@pgp.com-0x53691E61FA85D00F.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(masterKey);

        assertEquals(3, userIDs.size());

        for (byte[] userID : userIDs)
        {
            System.out.println(PGPUtils.userIDToString(userID));

            assertTrue(userIDValidator.validateUserID(userID, masterKey));
        }
    }

    @Test
    public void testValidateV3UserIDSignatures()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "v3-userid-signatures.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(masterKey);

        assertEquals(4, userIDs.size());

        for (byte[] userID : userIDs)
        {
            System.out.println(PGPUtils.userIDToString(userID));

            assertTrue(userIDValidator.validateUserID(userID, masterKey));
        }
    }

    @Test
    public void testValidateInvalidUTF8()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "invalid_UTF16_codepoint.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(masterKey);

        assertEquals(1, userIDs.size());

        for (byte[] userID : userIDs)
        {
            System.out.println(PGPUtils.userIDToString(userID));

            assertTrue(userIDValidator.validateUserID(userID, masterKey));
        }
    }
}
