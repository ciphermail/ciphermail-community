/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import com.ciphermail.core.common.cache.SimpleMemoryPatternCache;
import com.ciphermail.core.common.dlp.MatchFilter;
import com.ciphermail.core.common.dlp.MatchFilterRegistry;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.dlp.PolicyPatternMarshaller;
import com.ciphermail.core.common.dlp.Validator;
import com.ciphermail.core.common.dlp.ValidatorRegistry;
import com.ciphermail.core.common.dlp.impl.matchfilter.MaskingFilter;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

public class PolicyPatternImplTest
{
    private static PolicyPatternMarshaller marshaller;

    static class DummyValidator implements Validator
    {
        private final String name;
        private final String description;

        DummyValidator(String name, String description)
        {
            this.name = name;
            this.description = description;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public boolean isValid(String input) {
            return false;
        }
    }

    @BeforeClass
    public static void beforeClass()
    throws JAXBException
    {
        MatchFilterRegistry matchFilterRegistry = new MatchFilterRegistryImpl(Collections.singletonList(
                (MatchFilter) new MaskingFilter()));

        ValidatorRegistry validatorRegistry = new ValidatorRegistryImpl();

        validatorRegistry.addValidator(new DummyValidator("name", "desc"));

        marshaller = new PolicyPatternMarshallerImpl(new SimpleMemoryPatternCache(), matchFilterRegistry,
                validatorRegistry);
    }

    @Test
    public void testClone()
    throws Exception
    {
        PolicyPatternImpl policyPattern = new PolicyPatternImpl();

        MatchFilter filter = new MaskingFilter();

        policyPattern.setName("test name");
        policyPattern.setDescription("test & pattern");
        policyPattern.setValidator(new DummyValidator("name", "desc"));
        policyPattern.setMatchFilter(filter);
        policyPattern.setPattern(Pattern.compile(".*"));
        policyPattern.setThreshold(10);

        PolicyPattern clone = PolicyPatternImpl.clone(policyPattern, "clone");

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        marshaller.marshall(clone, bos);

        clone = marshaller.unmarshall(new ByteArrayInputStream(bos.toByteArray()));

        assertEquals("clone", clone.getName());
        assertEquals("test & pattern", clone.getDescription());
        assertEquals("name", clone.getValidator().getName());
        assertEquals("Mask", clone.getMatchFilter().getName());
        assertEquals(".*", clone.getPattern().pattern());
        assertEquals(10, clone.getThreshold());
    }
}
