/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.security.openpgp.PGPKeyFlagChecker;
import com.ciphermail.core.common.security.openpgp.PGPKeyFlagCheckerImpl;
import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.common.security.openpgp.PGPSignatureValidator;
import com.ciphermail.core.common.security.openpgp.PGPSignatureValidatorImpl;
import com.ciphermail.core.common.security.openpgp.PGPUserIDValidatorImpl;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SubKeyBindingSignatureValidatorTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static final PGPSignatureValidator signatureValidator = new PGPSignatureValidatorImpl();

    private static final PGPKeyFlagChecker keyFlagChecker = new PGPKeyFlagCheckerImpl(signatureValidator,
            new PGPUserIDValidatorImpl(signatureValidator));

    private static final PGPPublicKeyValidator validator = new SubKeyBindingSignatureValidator(signatureValidator,
            keyFlagChecker);

    @Test
    public void testSubKeys()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test-multiple-sub-keys.gpg.asc"));

        assertEquals(6, keys.size());

        PGPPublicKey master = null;

        PGPPublicKey otherMaster = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-expiration-2030-12-31.gpg.asc")).get(0);

        assertTrue(otherMaster.isMasterKey());

        int i = 0;

        for (PGPPublicKey publicKey : keys)
        {
            if (publicKey.isMasterKey()) {
                master = publicKey;
            }
            else {
                i++;

                Context context = new ContextImpl();

                context.set(StandardValidatorContextObjects.MASTER_KEY, master);

                PGPPublicKeyValidatorResult result = validator.validate(publicKey, context);

                assertTrue(result.isValid());
                assertNull(result.getFailureMessage());

                // should not validate
                context.set(StandardValidatorContextObjects.MASTER_KEY, otherMaster);

                result = validator.validate(publicKey, context);

                assertFalse(result.isValid());
                assertNotNull(result.getFailureMessage());
            }
        }

        assertEquals(5, i);
    }

    @Test
    public void testMissingMasterFromContext()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test-multiple-sub-keys.gpg.asc"));

        assertEquals(6, keys.size());

        PGPPublicKey otherMaster = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-expiration-2030-12-31.gpg.asc")).get(0);

        assertTrue(otherMaster.isMasterKey());

        int i = 0;

        for (PGPPublicKey publicKey : keys)
        {
            i++;

            Context context = new ContextImpl();

            PGPPublicKeyValidatorResult result = validator.validate(publicKey, context);

            if (publicKey.isMasterKey())
            {
                assertTrue(result.isValid());
                assertNull(result.getFailureMessage());
            }
            else {
                assertFalse(result.isValid());
                assertEquals("Master key not available. Unable to check subkey binding signature.",
                        result.getFailureMessage());
            }
        }

        assertEquals(6, i);
    }

    @Test
    public void testMaster()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test-multiple-sub-keys.gpg.asc"));

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        Context context = new ContextImpl();

        PGPPublicKeyValidatorResult result = validator.validate(master, context);

        assertTrue(result.isValid());
    }

    @Test
    public void testMultipleSubkeyBindings()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "multiple-sub-key-bindings-per-subkey.asc"));

        assertEquals(7, keys.size());

        Context context = new ContextImpl();

        context.set(StandardValidatorContextObjects.MASTER_KEY, keys.get(0));

        int i = 0;

        for (PGPPublicKey key : keys)
        {
            String keyID = PGPUtils.getKeyIDHex(key.getKeyID());

            if ("3D182120ACD964D9".equals(keyID))
            {
                i++;

                PGPPublicKeyValidatorResult result = validator.validate(key, context);

                assertTrue(result.isValid());
            }
            else if ("54C21956B2A67D71".equals(keyID))
            {
                i++;

                PGPPublicKeyValidatorResult result = validator.validate(key, context);

                assertTrue(result.isValid());
            }
            else if ("A38C7C0C6C860E33".equals(keyID))
            {
                i++;

                PGPPublicKeyValidatorResult result = validator.validate(key, context);

                assertTrue(result.isValid());
            }
            else if ("16846342BF5BAE8A".equals(keyID))
            {
                i++;

                PGPPublicKeyValidatorResult result = validator.validate(key, context);

                assertTrue(result.isValid());
            }
            else if ("0C96953C847974F2".equals(keyID))
            {
                i++;

                PGPPublicKeyValidatorResult result = validator.validate(key, context);

                assertTrue(result.isValid());
            }
            else if ("F59CA933556EFC4C".equals(keyID))
            {
                i++;

                PGPPublicKeyValidatorResult result = validator.validate(key, context);

                assertTrue(result.isValid());
            }
        }

        assertEquals(6, i);
    }

    @Test
    public void testMissingPrimaryKeyBindingSig()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "missing-primary-key-binding-signature-0x0549B8A5640444E6.asc"));

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        PGPPublicKey sub = keys.get(1);

        assertFalse(sub.isMasterKey());

        Context context = new ContextImpl();

        context.set(StandardValidatorContextObjects.MASTER_KEY, master);

        SubKeyBindingSignatureValidator validator = new SubKeyBindingSignatureValidator(signatureValidator,
                keyFlagChecker);

        PGPPublicKeyValidatorResult result = validator.validate(sub, context);

        // Key should not be valid because there is no primary key binding sig
        assertFalse(result.isValid());

        // Disable checking the primary key binding sig
        validator.setCheckPrimaryKeyBindingSignature(false);

        result = validator.validate(sub, context);

        // Key should now be valid
        assertTrue(result.isValid());
    }
}
