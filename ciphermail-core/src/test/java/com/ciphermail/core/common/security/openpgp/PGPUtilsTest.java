/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PGPUtilsTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Test
    public void testGetLengthWithoutTrailingWhitespace()
    {
        assertEquals(0, PGPUtils.getLengthWithoutTrailingWhitespace(null));
        assertEquals(0, PGPUtils.getLengthWithoutTrailingWhitespace(new byte[]{}));
        assertEquals(1, PGPUtils.getLengthWithoutTrailingWhitespace(new byte[]{1}));
        assertEquals(1, PGPUtils.getLengthWithoutTrailingWhitespace(new byte[]{1, ' '}));
        assertEquals(2, PGPUtils.getLengthWithoutTrailingWhitespace(new byte[]{1, 2, ' ', '\t', '\n', '\r'}));
    }

    @Test
    public void testGetKeyIDHex()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "test@example.com.gpg.asc"));

        assertEquals("4EC4E8813E11A9A0", PGPUtils.getKeyIDHex(publicKey.getKeyID()));

        assertEquals("D80D1572D0486F55", PGPUtils.getKeyIDHex(-2878621003955015851L));

        assertNull(PGPUtils.getKeyIDHex(null));
    }

    @Test
    public void testGetKeyIDHexStartWith0()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "expired-and-non-expired-userid.asc"));

        assertEquals("0E699AB9D18505AD", PGPUtils.getKeyIDHex(publicKey.getKeyID()));
    }

    @Test
    public void testGetKeyIDFromHex()
    throws Exception
    {
        assertEquals(5675917072183437728L, PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        assertEquals(-2878621003955015851L, PGPUtils.getKeyIDFromHex("D80D1572D0486F55"));
    }

    @Test
    public void testGetShortKeyIDHex()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "short-key-collision-1-19980101.gpg.asc"));

        assertEquals("19980101", PGPUtils.getShortKeyIDHex(publicKey.getKeyID()));

        publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "test@example.com.gpg.asc"));

        assertEquals("3E11A9A0", PGPUtils.getShortKeyIDHex(publicKey.getKeyID()));

        assertNull(PGPUtils.getShortKeyIDHex(null));
    }

    @Test
    public void testRemovePGPFilenameExtension()
    throws Exception
    {
        assertEquals("test.txt", PGPUtils.removePGPFilenameExtension("test.txt.pgp"));
        assertEquals("test.asc", PGPUtils.removePGPFilenameExtension("test.asc.sig"));
        assertEquals("test.txt", PGPUtils.removePGPFilenameExtension("test.txt.asc"));
        assertEquals("test.txt", PGPUtils.removePGPFilenameExtension("test.txt.gpg"));
        assertEquals("test.txt", PGPUtils.removePGPFilenameExtension("test.txt"));
        assertNull(PGPUtils.removePGPFilenameExtension(null));
    }

    @Test
    public void testIsPGPFilenameExtension()
    throws Exception
    {
        assertTrue(PGPUtils.isPGPFilenameExtension("test.txt.pgp"));
        assertTrue(PGPUtils.isPGPFilenameExtension("test.txt.asc"));
        assertTrue(PGPUtils.isPGPFilenameExtension("test.txt.gpg"));
        assertTrue(PGPUtils.isPGPFilenameExtension("test.txt.sig"));
        assertFalse(PGPUtils.isPGPFilenameExtension("test.txt"));
        assertFalse(PGPUtils.isPGPFilenameExtension(null));
    }

    @Test
    public void testGetEmailAddressesFromUserID()
    throws Exception
    {
        assertEquals("test@example.com", StringUtils.join(PGPUtils.getEmailAddressesFromUserID(
                "test<test@example.com>"), ","));
        assertEquals("test@example.com", StringUtils.join(PGPUtils.getEmailAddressesFromUserID(
                "test@example.com"), ","));
    }

    @Test
    public void testGetEmailAddressesFromUserIDs()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE, "test@example.com.gpg.asc"));

        assertEquals("test@example.com", StringUtils.join(PGPUtils.getEmailAddressesFromUserIDs(
                PGPPublicKeyInspector.getUserIDsAsStrings(publicKey)), ","));

        publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE, "test@example.com-expiration-2030-12-31.gpg.asc"));

        assertEquals("test@example.com", StringUtils.join(PGPUtils.getEmailAddressesFromUserIDs(
                PGPPublicKeyInspector.getUserIDsAsStrings(publicKey)), ","));

        publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "test@example.com-additional-userid.gpg.asc"));

        assertEquals("test@example.com,test2@example.com", StringUtils.join(PGPUtils.getEmailAddressesFromUserIDs(
                PGPPublicKeyInspector.getUserIDsAsStrings(publicKey)), ","));
    }

    @Test
    public void testTrimRightExcessiveNewlines()
    {
        assertArrayEquals(new byte[]{'a', '\r', '\n'}, PGPUtils.trimRightExcessiveNewlines(
                new byte[]{'a', '\r', '\n', '\r', '\n'}));

        assertArrayEquals(new byte[]{'a', '\r', '\n'}, PGPUtils.trimRightExcessiveNewlines(
                new byte[]{'a', '\r', '\n', '\r', '\n', '\r', '\n', '\r', '\n'}));

        assertArrayEquals(new byte[]{'a', '\r', '\n'}, PGPUtils.trimRightExcessiveNewlines(
                new byte[]{'a', '\r', '\n'}));

        assertArrayEquals(new byte[]{'a'}, PGPUtils.trimRightExcessiveNewlines(
                new byte[]{'a'}));

        assertArrayEquals(new byte[]{}, PGPUtils.trimRightExcessiveNewlines(
                new byte[]{}));

        assertNull(PGPUtils.trimRightExcessiveNewlines(null));
    }

    public void testGetMinRequiredHashAlgorithm(String keyFile, int expectedHash)
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, keyFile));

        assertTrue(keys.size() > 0);

        for (PGPPublicKey key : keys) {
            assertEquals(expectedHash, PGPUtils.getMinRequiredHashAlgorithm(key));
        }
    }

    @Test
    public void testGetMinRequiredHashAlgorithm()
    throws Exception
    {
        testGetMinRequiredHashAlgorithm("rsa-1024@example.com.asc", HashAlgorithmTags.SHA256);
        testGetMinRequiredHashAlgorithm("rsa-2048@example.com.asc", HashAlgorithmTags.SHA256);
        testGetMinRequiredHashAlgorithm("rsa-3072@example.com.asc", HashAlgorithmTags.SHA256);
        testGetMinRequiredHashAlgorithm("rsa-4096@example.com.asc", HashAlgorithmTags.SHA256);
        testGetMinRequiredHashAlgorithm("curve-25519@example.com.asc", HashAlgorithmTags.SHA256);
        testGetMinRequiredHashAlgorithm("nist-p-256@example.com.asc", HashAlgorithmTags.SHA256);
        testGetMinRequiredHashAlgorithm("nist-p-384@example.com.asc", HashAlgorithmTags.SHA384);
        testGetMinRequiredHashAlgorithm("nist-p-521@example.com.asc", HashAlgorithmTags.SHA512);
        testGetMinRequiredHashAlgorithm("brainpool-p-256@example.com.asc", HashAlgorithmTags.SHA256);
        testGetMinRequiredHashAlgorithm("brainpool-p-384@example.com.asc", HashAlgorithmTags.SHA384);
        testGetMinRequiredHashAlgorithm("brainpool-p-512@example.com.asc", HashAlgorithmTags.SHA512);
        testGetMinRequiredHashAlgorithm("secp256k1@example.com.asc", HashAlgorithmTags.SHA256);
    }

    public void testgetMinRequiredSymmetricKeyAlgorithm(String keyFile, int expected)
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, keyFile));

        assertTrue(keys.size() > 0);

        for (PGPPublicKey key : keys) {
            assertEquals(expected, PGPUtils.getMinRequiredSymmetricKeyAlgorithm(key));
        }
    }

    @Test
    public void testGetMinRequiredSymmetricKeyAlgorithm()
    throws Exception
    {
        testgetMinRequiredSymmetricKeyAlgorithm("rsa-1024@example.com.asc", SymmetricKeyAlgorithmTags.AES_128);
        testgetMinRequiredSymmetricKeyAlgorithm("rsa-2048@example.com.asc", SymmetricKeyAlgorithmTags.AES_128);
        testgetMinRequiredSymmetricKeyAlgorithm("rsa-3072@example.com.asc", SymmetricKeyAlgorithmTags.AES_128);
        testgetMinRequiredSymmetricKeyAlgorithm("rsa-4096@example.com.asc", SymmetricKeyAlgorithmTags.AES_128);
        testgetMinRequiredSymmetricKeyAlgorithm("curve-25519@example.com.asc", SymmetricKeyAlgorithmTags.AES_128);
        testgetMinRequiredSymmetricKeyAlgorithm("nist-p-256@example.com.asc", SymmetricKeyAlgorithmTags.AES_128);
        testgetMinRequiredSymmetricKeyAlgorithm("nist-p-384@example.com.asc", SymmetricKeyAlgorithmTags.AES_192);
        testgetMinRequiredSymmetricKeyAlgorithm("nist-p-521@example.com.asc", SymmetricKeyAlgorithmTags.AES_256);
        testgetMinRequiredSymmetricKeyAlgorithm("brainpool-p-256@example.com.asc", SymmetricKeyAlgorithmTags.AES_128);
        testgetMinRequiredSymmetricKeyAlgorithm("brainpool-p-384@example.com.asc", SymmetricKeyAlgorithmTags.AES_192);
        testgetMinRequiredSymmetricKeyAlgorithm("brainpool-p-512@example.com.asc", SymmetricKeyAlgorithmTags.AES_256);
        testgetMinRequiredSymmetricKeyAlgorithm("secp256k1@example.com.asc", SymmetricKeyAlgorithmTags.AES_128);
    }
}
