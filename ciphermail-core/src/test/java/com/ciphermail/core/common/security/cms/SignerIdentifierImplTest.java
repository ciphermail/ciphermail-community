/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.cms;

import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.math.BigInteger;
import java.security.cert.CertSelector;
import java.security.cert.X509Certificate;
import java.util.Objects;

import static org.junit.Assert.assertTrue;

public class SignerIdentifierImplTest
{
    /*
     * Check if the selector returned from KeyTransRecipientIdImpl works on selecting
     * on subjectKeyIdentifier. X509CertSelector expects the subjectKeyIdentifier to
     * be DER encoded.
     */
    @Test
    public void testX509SelectorSubjectKeyIdentifier()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/subjectkeyident.cer");

        X509Certificate certificate = Objects.requireNonNull(TestUtils.loadCertificate(file));

        byte[] subjectKeyIdentifier = X509CertificateInspector.getSubjectKeyIdentifier(certificate);

        SignerIdentifier id = new SignerIdentifierImpl(null, null, subjectKeyIdentifier);

        CertSelector selector = id.getSelector();

        assertTrue(selector.match(certificate));
    }

    @Test
    public void testX509SelectorIssuer()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/subjectkeyident.cer");

        X509Certificate certificate = Objects.requireNonNull(TestUtils.loadCertificate(file));

        SignerIdentifier id = new SignerIdentifierImpl(
                new X500Principal("CN=AddTrust Class 1 CA Root, OU=AddTrust TTP Network, O=AddTrust AB, C=SE"), null, null);

        CertSelector selector = id.getSelector();

        assertTrue(selector.match(certificate));
    }

    @Test
    public void testX509SelectorSerialNumber()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/subjectkeyident.cer");

        X509Certificate certificate = Objects.requireNonNull(TestUtils.loadCertificate(file));

        SignerIdentifier id = new SignerIdentifierImpl(
                null, new BigInteger("571D8005CE05222616E82BBAF0CC71D2", 16), null);

        CertSelector selector = id.getSelector();

        assertTrue(selector.match(certificate));
    }

    @Test
    public void testX509SelectorAll()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/subjectkeyident.cer");

        X509Certificate certificate = Objects.requireNonNull(TestUtils.loadCertificate(file));

        byte[] subjectKeyIdentifier = X509CertificateInspector.getSubjectKeyIdentifier(certificate);

        SignerIdentifier id = new SignerIdentifierImpl(
                new X500Principal("CN=AddTrust Class 1 CA Root, OU=AddTrust TTP Network, O=AddTrust AB, C=SE"),
                new BigInteger("571D8005CE05222616E82BBAF0CC71D2", 16),
                subjectKeyIdentifier);

        CertSelector selector = id.getSelector();

        assertTrue(selector.match(certificate));
    }
}
