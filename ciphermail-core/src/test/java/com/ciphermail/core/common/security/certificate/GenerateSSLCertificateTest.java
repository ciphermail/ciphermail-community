/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.impl.StandardSerialNumberGenerator;
import org.apache.commons.lang.time.DateUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.security.auth.x500.X500Principal;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertNotNull;

public class GenerateSSLCertificateTest
{
    private static SecurityFactory securityFactory;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }

    /*
     * Generates a SSL certificate
     */
    @Test
    public void generateSSLCertificate()
    throws Exception
    {
        X509CertificateBuilder certificateBuilder = securityFactory.createX509CertificateBuilder();

        KeyPairGenerator keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");

        SecureRandom randomSource = securityFactory.createSecureRandom();

        keyPairGenerator.initialize(1024, randomSource);

        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        X500PrincipalBuilder subjectBuilder = X500PrincipalBuilder.getInstance();

        subjectBuilder.setCommonName("www.example.com");
        subjectBuilder.setCountryCode("NL");
        subjectBuilder.setLocality("Amsterdam");
        subjectBuilder.setState("NH");

        X500Principal subject = subjectBuilder.buildPrincipal();

        Set<ExtendedKeyUsageType> extendedKeyUsage = new TreeSet<>();

        extendedKeyUsage.add(ExtendedKeyUsageType.CLIENTAUTH);
        extendedKeyUsage.add(ExtendedKeyUsageType.EMAILPROTECTION);
        extendedKeyUsage.add(ExtendedKeyUsageType.SERVERAUTH);

        SerialNumberGenerator sng = new StandardSerialNumberGenerator();

        BigInteger serialNumber = sng.generate();

        Date now = new Date();

        certificateBuilder.setSubject(subject);
        certificateBuilder.setIssuer(subject);
        certificateBuilder.setExtendedKeyUsage(extendedKeyUsage, false);
        certificateBuilder.setNotBefore(DateUtils.addDays(now, -20));
        certificateBuilder.setNotAfter(DateUtils.addYears(now, 20));
        certificateBuilder.setPublicKey(keyPair.getPublic());
        certificateBuilder.setSerialNumber(serialNumber);
        certificateBuilder.setSignatureAlgorithm("SHA1WithRSAEncryption");

        X509Certificate certificate = certificateBuilder.generateCertificate(keyPair.getPrivate(), null);

        assertNotNull(certificate);
    }
}
