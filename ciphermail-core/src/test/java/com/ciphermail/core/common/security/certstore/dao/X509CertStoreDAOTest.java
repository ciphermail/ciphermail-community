/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore.dao;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.asn1.DERUtils;
import com.ciphermail.core.common.security.certificate.KeyUsageType;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certificate.X509ExtensionInspector;
import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreEntity;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class X509CertStoreDAOTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    private KeyStore keyStore;

    @Before
    public void setup()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                dao.removeAllEntries();

                assertEquals(0, dao.getRowCount());

                keyStore = loadKeyStore(new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12"), "test");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private X509CertStoreDAO getDAO(String storeName) {
        return X509CertStoreDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), storeName);
    }

    private X509CertStoreDAO getDAO() {
        return getDAO("test");
    }

    private KeyStore loadKeyStore(File file, String password)
    throws Exception
    {
        KeyStore localKeyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        // initialize key store
        localKeyStore.load(new FileInputStream(file), password.toCharArray());

        Enumeration<String> aliases = localKeyStore.aliases();

        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();

            Certificate certificate = localKeyStore.getCertificate(alias);

            if (!(certificate instanceof X509Certificate)) {
                // only X509Certificates are supported
                continue;
            }

            boolean hasPrivateKey = localKeyStore.isKeyEntry(alias) &&
                    (localKeyStore.getKey(alias, null) instanceof PrivateKey);

            alias = hasPrivateKey ? alias : null;

            // Because we are checking the order of import sorted on import timestamp, there should be some time
            // between items otherwise the order might not be deterministic if the import is too fast
            ThreadUtils.sleepQuietly(10);

            getDAO().addCertificate((X509Certificate) certificate, alias);
        }

        return localKeyStore;
    }

    private int getEntryCount(CloseableIterator<?> entryIterator)
    throws CloseableIteratorException
    {
        int i = 0;

        assertFalse(entryIterator.isClosed());

        while(entryIterator.hasNext())
        {
            Object entry = entryIterator.next();

            assertNotNull(entry);

            i++;
        }

        assertTrue(entryIterator.isClosed());

        // closing it again should be no problem
        entryIterator.close();

        return i;
    }

    @Test
    public void testSearchByIssuerCount()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                Date now = new Date();

                long count = dao.getSearchByIssuerCount("%MITM Test CA%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, now);

                assertEquals(20, count);

                Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");

                count = dao.getSearchByIssuerCount("%MITM Test CA%", Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, future);

                assertEquals(1, count);

                now = new Date();

                count = dao.getSearchByIssuerCount("%CN=MITM Test Root%", Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, now);

                assertEquals(2, count);

                count = dao.getSearchByIssuerCount("%CN=MITM Test Root%", Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, now);

                assertEquals(0, count);

                count = dao.getSearchByIssuerCount("%MITM Test CA%", Expired.MATCH_EXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, now);

                assertEquals(1, count);

                count = dao.getSearchByIssuerCount("%MITM Test CA%", Expired.MATCH_EXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, future);

                assertEquals(19, count);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchBySubjectCount()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                Date now = new Date();

                Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");

                long count = dao.getSearchBySubjectCount("%MITM Test CA%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, now);

                assertEquals(1, count);

                count = dao.getSearchBySubjectCount("%MITM Test CA%", Expired.MATCH_ALL,
                        MissingKeyAlias.NOT_ALLOWED, now);

                assertEquals(0, count);

                count = dao.getSearchBySubjectCount("%Not yet valid%", Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, future);

                assertEquals(1, count);

                count = dao.getSearchBySubjectCount("%CN=%", Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, now);

                assertEquals(18, count);

                count = dao.getSearchBySubjectCount("%%", Expired.MATCH_EXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, now);

                assertEquals(1, count);

                count = dao.getSearchBySubjectCount("%%", Expired.MATCH_EXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, future);

                assertEquals(21, count);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetByEmailCount()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                assertEquals(22, dao.getRowCount());

                Date now = new Date();

                Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");

                long count = dao.getByEmailCount("test@example.com", Match.EXACT, Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, now);

                assertEquals(18, count);

                count = dao.getByEmailCount("%@%", Match.LIKE, Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, future);

                assertEquals(20, count);

                count = dao.getByEmailCount("%@%", Match.LIKE, Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, now);

                assertEquals(20, count);

                count = dao.getByEmailCount("%@%", Match.LIKE, Expired.MATCH_ALL,
                        MissingKeyAlias.NOT_ALLOWED, now);

                assertEquals(18, count);

                count = dao.getByEmailCount("%@%", Match.LIKE, Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, now);

                assertEquals(17, count);

                count = dao.getByEmailCount("%@%", Match.LIKE, Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, future);

                assertEquals(1, count);

                // Should return the entries without an email address or without a valid email address
                count = dao.getByEmailCount(null, Match.LIKE, Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, now);

                assertEquals(2, count);

                // Check expire required
                count = dao.getByEmailCount("%@%", Match.LIKE, Expired.MATCH_EXPIRED_ONLY, MissingKeyAlias.ALLOWED, now);

                assertEquals(1, count);

                count = dao.getByEmailCount("%%", Match.LIKE, Expired.MATCH_EXPIRED_ONLY, MissingKeyAlias.ALLOWED, future);

                // Only 19 since one is not expired and one has an invalid email address
                assertEquals(19, count);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchBySubject()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                Date now = new Date();

                Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");

                CloseableIterator<X509CertStoreEntity> iterator = dao.searchBySubject(
                        "%MITM Test CA%", Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null);

                assertEquals(1, getEntryCount(iterator));

                iterator = dao.searchBySubject("%MITM Test CA%", Expired.MATCH_ALL,
                        MissingKeyAlias.NOT_ALLOWED, now, null, null);

                assertEquals(0, getEntryCount(iterator));

                iterator = dao.searchBySubject("%Not yet valid%", Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, future, null, null);

                assertEquals(1, getEntryCount(iterator));

                iterator = dao.searchBySubject("%CN=%", Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, now, null, null);

                assertEquals(18, getEntryCount(iterator));

                iterator = dao.searchBySubject("%CN=%", Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, now, 0, 100);

                assertEquals(18, getEntryCount(iterator));

                iterator = dao.searchBySubject("%CN=%", Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, now, 0, 4);

                assertEquals(4, getEntryCount(iterator));

                iterator = dao.searchBySubject("%%", Expired.MATCH_EXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, now, null, null);

                List<X509CertStoreEntity> entries = CloseableIteratorUtils.toList(iterator);

                assertEquals(1, entries.size());
                assertTrue(X509CertificateInspector.isExpired(entries.get(0).getCertificate()));

                iterator = dao.searchBySubject("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, null, null);

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(22, entries.size());

                X509CertStoreEntity entry = entries.get(0);

                iterator = dao.searchBySubject("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, null, null);

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(entry, entries.get(0));

                entry.setCreationDate(DateUtils.addDays(now, 1));

                iterator = dao.searchBySubject("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, null, null);

                entries = CloseableIteratorUtils.toList(iterator);

                assertNotEquals(entry, entries.get(0));

                iterator = dao.searchBySubject("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, 1, null);

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(21, entries.size());

                iterator = dao.searchBySubject("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, null, 10);

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(10, entries.size());

                iterator = dao.searchBySubject("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, 4, 6);

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(6, entries.size());

            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchByIssuer()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                Date now = new Date();

                CloseableIterator<X509CertStoreEntity> iterator = dao.searchByIssuer("%MITM Test CA%",
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null);

                assertEquals(20, getEntryCount(iterator));

                Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");

                iterator = dao.searchByIssuer("%MITM Test CA%", Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                        future, null, null);

                assertEquals(1, getEntryCount(iterator));

                now = new Date();

                iterator = dao.searchByIssuer("%CN=MITM Test Root%", Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                        now, null, null);

                assertEquals(2, getEntryCount(iterator));

                iterator = dao.searchByIssuer("%CN=MITM Test Root%", Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.NOT_ALLOWED,
                        now, null, null);

                assertEquals(0, getEntryCount(iterator));

                iterator = dao.searchByIssuer("%MITM Test CA%", Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, 0, 100);

                assertEquals(20, getEntryCount(iterator));

                iterator = dao.searchByIssuer("%MITM Test CA%", Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, 0, 5);

                assertEquals(5, getEntryCount(iterator));

                iterator = dao.searchByIssuer("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, null, null);

                List<X509CertStoreEntity> entries;

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(22, entries.size());

                X509CertStoreEntity entry = entries.get(0);

                iterator = dao.searchByIssuer("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, null, null);

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(entry, entries.get(0));

                entry.setCreationDate(DateUtils.addDays(now, 1));

                iterator = dao.searchByIssuer("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, null, null);

                entries = CloseableIteratorUtils.toList(iterator);

                assertNotEquals(entry, entries.get(0));

                iterator = dao.searchByIssuer("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, 1, null);

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(21, entries.size());

                iterator = dao.searchByIssuer("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, null, 10);

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(10, entries.size());

                iterator = dao.searchByIssuer("%%", Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, null, 4, 6);

                entries = CloseableIteratorUtils.toList(iterator);
                assertEquals(6, entries.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertStoreCertSelectorIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509CertSelector selector = new X509CertSelector();

                Set<KeyUsageType> keyUsages = new HashSet<>();

                keyUsages.add(KeyUsageType.CRLSIGN);

                selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));

                CloseableIterator<X509CertStoreEntity> iterator = dao.getCertStoreIterator(selector,
                        MissingKeyAlias.ALLOWED, null, null);

                assertEquals(3, getEntryCount(iterator));

                iterator = dao.getCertStoreIterator(selector,
                        MissingKeyAlias.ALLOWED, 1, null);

                // NOTE: Looks weird why 3 items are returned. However first and max result parameter is only used
                // for the pre-filtering (i.e., reading data from the database). But because of the X509CertSelector
                // some records will be skipped.
                assertEquals(3, getEntryCount(iterator));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertStoreIteratorHasNextMultiple()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509CertSelector selector = new X509CertSelector();

                Set<KeyUsageType> keyUsages = new HashSet<>();

                keyUsages.add(KeyUsageType.CRLSIGN);

                selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));

                CloseableIterator<X509CertStoreEntity> iterator = dao.getCertStoreIterator(selector,
                        MissingKeyAlias.ALLOWED, null, null);

                // check that hasNext does not advance to the next entry
                iterator.hasNext();
                iterator.hasNext();
                iterator.hasNext();

                assertEquals(3, getEntryCount(iterator));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificateIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509CertSelector selector = new X509CertSelector();

                Set<KeyUsageType> keyUsages = new HashSet<>();

                keyUsages.add(KeyUsageType.CRLSIGN);

                selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));

                CloseableIterator<X509Certificate> iterator = dao.getCertificateIterator(selector,
                        MissingKeyAlias.ALLOWED, null, null);

                assertEquals(3, getEntryCount(iterator));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetPreFilteredEntriesScrollable()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509Certificate notYetValidCertificate = (X509Certificate) keyStore.getCertificate("NotYetValid");
                X509Certificate validCertificate = (X509Certificate) keyStore.getCertificate("ValidCertificate");
                X509Certificate rootCertificate = (X509Certificate) keyStore.getCertificate("root");

                X509CertSelector selector;

                selector = new X509CertSelector();

                // Select on issuer
                selector.setIssuer(notYetValidCertificate.getIssuerX500Principal());

                List<X509Certificate> certificates;

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(20, certificates.size());

                // Check if certificate expiration is checked when added as an additional constraint
                selector.setCertificateValid(new Date());

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(19, certificates.size());

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, 3, null)));

                assertEquals(16, certificates.size());

                // Far into the future. It should only match one certificate
                selector.setCertificateValid(DateUtils.addDays(notYetValidCertificate.getNotAfter(), -2));

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(1, certificates.size());

                /*
                 * Even further into the future. It should not match any certificate
                 */
                selector.setCertificateValid(DateUtils.addDays(notYetValidCertificate.getNotAfter(), 2));

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(0, certificates.size());

                // Use an issuer which should not match
                X500PrincipalBuilder x500PrincipalBuilder = X500PrincipalBuilder.getInstance();
                x500PrincipalBuilder.setCommonName("test");

                selector.setIssuer(x500PrincipalBuilder.buildPrincipal());

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(0, certificates.size());

                // Reset selector
                selector = new X509CertSelector();

                // Check if certificate expiration is checked
                selector.setCertificateValid(new Date());

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(21, certificates.size());

                // Reset selector
                selector = new X509CertSelector();

                // Select on subject
                selector.setSubject(rootCertificate.getSubjectX500Principal());

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(1, certificates.size());
                assertEquals(rootCertificate, certificates.get(0));

                selector.setSubject(x500PrincipalBuilder.buildPrincipal());

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(0, certificates.size());

                // Reset selector
                selector = new X509CertSelector();

                // Select on serial number
                selector.setSerialNumber(notYetValidCertificate.getSerialNumber());

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(1, certificates.size());
                assertEquals(notYetValidCertificate, certificates.get(0));

                // Select a serial number that does not match
                selector.setSerialNumber(BigInteger.valueOf(2808));
                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(0, certificates.size());

                // Reset selector
                selector = new X509CertSelector();

                // Select on Subject Key Identifier
                //
                // Note: X509CertSelector expects that subjectKeyIdentifier is DER encoded!!
                selector.setSubjectKeyIdentifier(DERUtils.toDEREncodedOctetString(
                        X509ExtensionInspector.getSubjectKeyIdentifier(validCertificate)));

                certificates = CloseableIteratorUtils.toList(new X509CertStoreCertificateIterator(
                        dao.getPreFilteredEntriesScrollable(selector, MissingKeyAlias.ALLOWED, null, null)));

                assertEquals(1, certificates.size());
                assertEquals(validCertificate, certificates.get(0));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificateIteratorHasNextMultiple()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509CertSelector selector = new X509CertSelector();

                Set<KeyUsageType> keyUsages = new HashSet<>();

                keyUsages.add(KeyUsageType.CRLSIGN);

                selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));

                CloseableIterator<X509Certificate> iterator = dao.getCertificateIterator(selector,
                        MissingKeyAlias.ALLOWED, null, null);

                // check that hasNext does not advance to the next entry
                iterator.hasNext();
                iterator.hasNext();
                iterator.hasNext();
                iterator.hasNext();

                assertEquals(3, getEntryCount(iterator));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificates()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509CertSelector selector = new X509CertSelector();

                Set<KeyUsageType> keyUsages = new HashSet<>();

                keyUsages.add(KeyUsageType.CRLSIGN);

                selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsages));

                Collection<X509Certificate> certificates = dao.getCertificates(selector, MissingKeyAlias.ALLOWED,
                        null, null);

                assertEquals(3, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testRowCount()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509CertStoreDAO otherDao = getDAO("other-dao");

                assertEquals(22, dao.getRowCount());
                assertEquals(0, otherDao.getRowCount());

                X509CertStoreDAO otherStoreDAO = X509CertStoreDAO.getInstance(SessionAdapterFactory.create(
                        sessionManager.getSession()),
                        "some_non_existing_store");

                assertEquals(0, otherStoreDAO.getRowCount());

                Date now = new Date();

                Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");

                assertEquals(22, dao.getRowCount(Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now));

                assertEquals(21, dao.getRowCount(Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED, now));

                assertEquals(19, dao.getRowCount(Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.NOT_ALLOWED, now));

                assertEquals(1, dao.getRowCount(Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED, future));

                assertEquals(1, dao.getRowCount(Expired.MATCH_EXPIRED_ONLY, MissingKeyAlias.ALLOWED, now));

                assertEquals(21, dao.getRowCount(Expired.MATCH_EXPIRED_ONLY, MissingKeyAlias.ALLOWED, future));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testCertStoreIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();
                X509CertStoreDAO otherDAO = getDAO("other-store");

                assertEquals(22, dao.getRowCount());

                Date now = new Date();

                Date future = TestUtils.parseDate("03-Nov-2030 17:56:52 GMT");

                List<X509CertStoreEntity> entries;

                entries = CloseableIteratorUtils.toList(dao.getCertStoreIterator(Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(22, entries.size());

                entries = CloseableIteratorUtils.toList(otherDAO.getCertStoreIterator(Expired.MATCH_ALL,
                        MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(0, entries.size());

                List<X509CertStoreEntity> entriesA = CloseableIteratorUtils.toList(dao.getCertStoreIterator(
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, 4));

                assertEquals(4, entriesA.size());

                List<X509CertStoreEntity> entriesB = CloseableIteratorUtils.toList(dao.getCertStoreIterator(
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, 2, 4));

                assertEquals(4, entriesB.size());

                assertEquals(entriesA.get(2), entriesB.get(0));
                assertEquals(entriesA.get(3), entriesB.get(1));

                entries = CloseableIteratorUtils.toList(dao.getCertStoreIterator(Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(21, entries.size());

                for (X509CertStoreEntity entry : entries)
                {
                    X509Certificate cert = entry.getCertificate();

                    assertFalse(X509CertificateInspector.isExpired(cert));
                }

                entries = CloseableIteratorUtils.toList(dao.getCertStoreIterator(Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED, now, null, null));

                assertEquals(19, entries.size());

                for (X509CertStoreEntity entry : entries)
                {
                    X509Certificate cert = entry.getCertificate();

                    assertFalse(X509CertificateInspector.isExpired(cert));
                    assertNotNull(entry.getKeyAlias());
                }

                entries = CloseableIteratorUtils.toList(dao.getCertStoreIterator(Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, future, null, null));

                assertEquals(1, entries.size());

                entries = CloseableIteratorUtils.toList(dao.getCertStoreIterator(Expired.MATCH_EXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(1, entries.size());

                for (X509CertStoreEntity entry : entries)
                {
                    X509Certificate cert = entry.getCertificate();

                    assertTrue(X509CertificateInspector.isExpired(cert));
                }

                entries = CloseableIteratorUtils.toList(dao.getCertStoreIterator(Expired.MATCH_EXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, future, null, null));

                assertEquals(21, entries.size());

                entries = CloseableIteratorUtils.toList(dao.getCertStoreIterator(Expired.MATCH_EXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, future, 1, null));

                assertEquals(20, entries.size());

                entries = CloseableIteratorUtils.toList(dao.getCertStoreIterator(Expired.MATCH_EXPIRED_ONLY,
                        MissingKeyAlias.ALLOWED, future, null, 5));

                assertEquals(5, entries.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddExistingCertificateSameStore()
    {
        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    X509CertStoreDAO dao = getDAO();

                    X509Certificate certificate = (X509Certificate) keyStore.getCertificate("ValidCertificate");
                    assertNotNull(certificate);

                    dao.addCertificate(certificate, null);
                }
                catch (Exception e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch(Exception e) {
            assertTrue(ExceptionUtils.indexOfThrowable(e, ConstraintViolationException.class) >= 0);
        }
    }

    @Test
    public void testAddExistingCertificateDifferentStore()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("ValidCertificate");
                assertNotNull(certificate);

                X509CertStoreDAO dao = X509CertStoreDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()),
                        "other store");

                dao.addCertificate(certificate, null);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddEmail()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("multipleEmail");
                assertNotNull(certificate);

                X509CertStoreEntity entry = dao.getByCertificate(certificate);

                assertEquals(certificate, entry.getCertificate());

                assertTrue(entry.getEmail().contains("test@example.com"));
                assertTrue(entry.getEmail().contains("test2@example.com"));
                assertTrue(entry.getEmail().contains("test3@example.com"));

                // It should be possible to add an email address
                entry.getEmail().add("newAddress@example.com");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("multipleEmail");
                assertNotNull(certificate);

                X509CertStoreEntity entry = dao.getByCertificate(certificate);

                assertTrue(entry.getEmail().contains("newAddress@example.com"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetByEmail()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509CertStoreEntity entry;

                Date now = new Date();

                List<X509CertStoreEntity> entries = CloseableIteratorUtils.toList(dao.getByEmail(
                        "test@Example.COM", Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(18, entries.size());

                // again but now without expired certificates
                entries = CloseableIteratorUtils.toList(dao.getByEmail("test@Example.COM", Match.EXACT,
                        Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(17, entries.size());

                // ca@example.com should have no alias. Find with missing alias allowed
                entries = CloseableIteratorUtils.toList(dao.getByEmail("ca@Example.COM", Match.EXACT,
                        Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(1, entries.size());

                entry = entries.get(0);
                assertNull(entry.getKeyAlias());
                assertEquals("115FCAD6B536FD8D49E72922CD1F0DA", new X509CertificateInspector(
                        entry.getCertificate()).getSerialNumberHex());

                // now missing alias not allowed
                entries = CloseableIteratorUtils.toList(dao.getByEmail("ca@Example.COM", Match.EXACT,
                        Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.NOT_ALLOWED, now, null, null));

                assertEquals(0, entries.size());

                // no match on email
                entries = CloseableIteratorUtils.toList(dao.getByEmail("xxx", Match.EXACT,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(0, entries.size());

                // null email. We should now get the entries without an (valid) email
                entries = CloseableIteratorUtils.toList(dao.getByEmail(null, Match.EXACT,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(2, entries.size());

                // match with spaces begin and end
                entries = CloseableIteratorUtils.toList(dao.getByEmail("  test@Example.COM  ", Match.EXACT,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(18, entries.size());

                // set the date to a different date
                now = TestUtils.parseDate("17-Oct-2006 07:38:35 GMT");

                // expired allowed
                entries = CloseableIteratorUtils.toList(dao.getByEmail("test@Example.COM", Match.EXACT,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(18, entries.size());

                // expired not allowed
                entries = CloseableIteratorUtils.toList(dao.getByEmail("test@Example.COM", Match.EXACT,
                        Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(0, entries.size());

                // ca is one second valid
                now = TestUtils.parseDate("01-Nov-2007 07:39:35 GMT");

                entries = CloseableIteratorUtils.toList(dao.getByEmail("ca@Example.COM", Match.EXACT,
                        Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(1, entries.size());

                // ca is one second not yet valid
                now = TestUtils.parseDate("01-Nov-2007 06:37:35 GMT");

                entries = CloseableIteratorUtils.toList(dao.getByEmail("ca@Example.COM", Match.EXACT,
                        Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(0, entries.size());

                now = new Date();

                // test like
                entries = CloseableIteratorUtils.toList(dao.getByEmail("test%", Match.LIKE,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(18, entries.size());

                // test like
                entries = CloseableIteratorUtils.toList(dao.getByEmail("test3%", Match.LIKE,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(1, entries.size());

                // test like
                entries = CloseableIteratorUtils.toList(dao.getByEmail("%", Match.LIKE,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(20, entries.size());

                // test like case insensitive
                entries = CloseableIteratorUtils.toList(dao.getByEmail("TEST3%", Match.LIKE,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, null, null));

                assertEquals(1, entries.size());

                // test first result
                entries = CloseableIteratorUtils.toList(dao.getByEmail("%", Match.LIKE,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, now, 1, null));

                assertEquals(19, entries.size());

                // test max result
                entries = CloseableIteratorUtils.toList(dao.getByEmail("%", Match.LIKE,
                        Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, null, 10, null));

                assertEquals(10, entries.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetByThumbprint()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("NotYetValid");
                assertNotNull(certificate);

                X509CertificateInspector inspector = new X509CertificateInspector(certificate);

                X509CertStoreEntity entry = dao.getByThumbprint(inspector.getThumbprint());

                assertEquals(certificate, entry.getCertificate());

                // uppercase
                entry = dao.getByThumbprint("8DD973D3B38C0A8CBB055FA41F20CB36041041C9BB70662D93B54" +
                    "B4F34FE6D0CF03B70E70DFAE8EC567D9122C43E74CEBD0E8DB0D421CD3DE8245CF6D6102945");

                assertNotNull(entry);
                assertNotNull(entry.getCertificate());

                // lowercase
                entry = dao.getByThumbprint("8dd973d3b38c0a8cbb055fa41f20cb36041041c9bb70662d93b54" +
                    "b4f34fe6d0cf03b70e70dfae8ec567d9122c43e74cebd0e8db0d421cd3de8245cf6d6102945");

                assertNotNull(entry);
                assertNotNull(entry.getCertificate());

                // non existing
                entry = dao.getByThumbprint("xxx");

                assertNull(entry);

                X509CertStoreDAO otherDao = getDAO("other-store");
                assertNull(otherDao.getByThumbprint("8DD973D3B38C0A8CBB055FA41F20CB36041041C9BB70662D93B54" +
                    "B4F34FE6D0CF03B70E70DFAE8EC567D9122C43E74CEBD0E8DB0D421CD3DE8245CF6D6102945"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetByCertificate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("multipleEmail");
                assertNotNull(certificate);

                X509CertStoreEntity entry = dao.getByCertificate(certificate);

                assertEquals(certificate, entry.getCertificate());

                assertTrue(entry.getEmail().contains("test@example.com"));
                assertTrue(entry.getEmail().contains("test2@example.com"));
                assertTrue(entry.getEmail().contains("test3@example.com"));

                // no email test
                certificate = (X509Certificate) keyStore.getCertificate("noEmail");
                assertNotNull(certificate);

                entry = dao.getByCertificate(certificate);

                assertEquals(certificate, entry.getCertificate());

                assertEquals("[]", entry.getEmail().toString());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertStoreIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreDAO dao = getDAO();

                Date now = new Date();

                // allow expired and missing
                X509CertSelector selector = new X509CertSelector();

                CloseableIterator<X509CertStoreEntity> entryIterator = dao.getCertStoreIterator(selector,
                        MissingKeyAlias.ALLOWED, null, null);
                assertEquals(22, getEntryCount(entryIterator));

                // not allow expired, allow missing
                selector = new X509CertSelector();
                selector.setCertificateValid(now);

                entryIterator = dao.getCertStoreIterator(selector, MissingKeyAlias.ALLOWED, null, null);
                assertEquals(21, getEntryCount(entryIterator));

                // not allow expired, not allow missing
                selector = new X509CertSelector();
                selector.setCertificateValid(now);

                entryIterator = dao.getCertStoreIterator(selector, MissingKeyAlias.NOT_ALLOWED, null, null);
                assertEquals(19, getEntryCount(entryIterator));

                // a date in the past so everything is expired
                now = TestUtils.parseDate("01-Oct-2006 00:00:00 GMT");

                // not allow expired, not allow missing
                selector = new X509CertSelector();
                selector.setCertificateValid(now);

                entryIterator = dao.getCertStoreIterator(selector, MissingKeyAlias.NOT_ALLOWED, null, null);
                assertEquals(0, getEntryCount(entryIterator));

                // a date in the future so everything is not yet valid
                now = TestUtils.parseDate("01-Oct-2080 00:00:00 GMT");

                // not allow expired, not allow missing
                selector = new X509CertSelector();
                selector.setCertificateValid(now);

                entryIterator = dao.getCertStoreIterator(selector, MissingKeyAlias.NOT_ALLOWED, null, null);
                assertEquals(0, getEntryCount(entryIterator));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
