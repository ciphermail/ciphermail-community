/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.repository.hibernate;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MimeMessageWithID;
import com.ciphermail.core.common.mail.repository.MailRepositorySearchField;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class MailRepositoryDAOTest
{
    @Autowired
    private SessionManager sessionManager;

    private MimeMessage message;

    @Before
    public void setup()
    throws Exception
    {
        assertEquals(0, createDAO().getRepositoryNames().size());

        message = createMessage();
    }

    private MailRepositoryDAO createDAO() {
        return MailRepositoryDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    private MimeMessage createMessage()
    throws Exception
    {
        MimeMessage localMessage = new MimeMessageWithID(MailSession.getDefaultSession(), "test-message-id");

        localMessage.setSubject("test");
        localMessage.setText("body");
        localMessage.setFrom(new InternetAddress("from@example.com"));

        localMessage.saveChanges();

        return localMessage;
    }

    @Test
    public void testGetItemCount()
    throws Exception
    {
        MailRepositoryDAO dao = createDAO();

        assertEquals(0, dao.getItemCount("repo"));

        MailRepositoryEntity entity = new MailRepositoryEntity("repo", message);
        dao.persist(entity);

        assertEquals(1, dao.getItemCount("repo"));

        entity = new MailRepositoryEntity("repo", message);
        dao.persist(entity);

        assertEquals(2, dao.getItemCount("repo"));

        entity = new MailRepositoryEntity("repo2", message);
        dao.persist(entity);

        assertEquals(1, dao.getItemCount("repo2"));
        assertEquals(2, dao.getItemCount("repo"));

        List<String> repositoryNames = dao.getRepositoryNames();

        assertEquals(2, repositoryNames.size());
        assertThat(repositoryNames, allOf(hasItem("repo"), hasItem("repo2")));
    }

    @Test
    public void testGetItems()
    throws Exception
    {
        MailRepositoryDAO dao = createDAO();

        Date now = new Date();

        MailRepositoryEntity entity;

        entity = new MailRepositoryEntity("repo", message);
        dao.persist(entity);

        entity = new MailRepositoryEntity("repo", message, DateUtils.addDays(now, 1));
        dao.persist(entity);

        entity = new MailRepositoryEntity("repo", message, DateUtils.addDays(now, -1));
        dao.persist(entity);

        entity = new MailRepositoryEntity("repo2", message, DateUtils.addDays(now, -3));
        dao.persist(entity);

        assertEquals(3, dao.getItemCount("repo"));

        List<MailRepositoryEntity> items = dao.getItems("repo", null, null);

        assertEquals(3, items.size());

        assertNotNull(items.get(0).getID());
        assertNotNull(items.get(1).getID());
        assertNotNull(items.get(2).getID());

        items = dao.getItems("repo", 1, null);

        assertEquals(2, items.size());

        items = dao.getItems("repo", 2, 1);

        assertEquals(1, items.size());

        items = dao.getItems("other-repo", null, null);

        assertEquals(0, items.size());
    }

    @Test
    public void testGetItemsBefore()
    throws Exception
    {
        MailRepositoryDAO dao = createDAO();

        Date now = new Date();

        MailRepositoryEntity entity;

        entity = new MailRepositoryEntity("repo", message, DateUtils.addDays(now, -2));
        dao.persist(entity);

        entity = new MailRepositoryEntity("repo", message,
                DateUtils.addDays(now, -3));
        dao.persist(entity);

        entity = new MailRepositoryEntity("repo", message, now);
        dao.persist(entity);

        entity = new MailRepositoryEntity("repo2", message);
        dao.persist(entity);

        List<MailRepositoryEntity> items = dao.getItemsBefore("repo", DateUtils.addDays(now, -1), null, null);

        assertEquals(2, items.size());

        items = dao.getItemsBefore("repo", DateUtils.addDays(now, -1), 1, null);

        assertEquals(1, items.size());

        items = dao.getItemsBefore("repo", DateUtils.addDays(now, -1), 1, 0);

        assertEquals(0, items.size());

        items = dao.getItemsBefore("repo", DateUtils.addDays(now, -4), null, null);

        assertEquals(0, items.size());

        items = dao.getItemsBefore("repo", DateUtils.addDays(now, 1), null, null);

        assertEquals(3, items.size());
    }

    @Test
    public void testSearchItems()
    throws Exception
    {
        MailRepositoryDAO dao = createDAO();

        Date now = new Date();

        MailRepositoryEntity entity;

        entity = new MailRepositoryEntity("repo2", message, DateUtils.addDays(now, -100));
        dao.persist(entity);

        entity = new MailRepositoryEntity("repo", message, DateUtils.addDays(now, 1));
        dao.persist(entity);

        List<InternetAddress> recipients1 = new LinkedList<>();

        recipients1.add(new InternetAddress("r1@example.com"));
        recipients1.add(new InternetAddress("r2@example.com"));

        entity.setRecipients(recipients1);
        entity.setSender(new InternetAddress("test1@example.com"));

        entity = new MailRepositoryEntity("repo", message);
        dao.persist(entity);

        List<InternetAddress> recipients2 = new LinkedList<>();

        recipients2.add(new InternetAddress("r1@example.com"));
        recipients2.add(new InternetAddress("r2@example.com"));
        recipients2.add(new InternetAddress("r3@example.com"));

        entity.setRecipients(recipients2);
        entity.setSender(new InternetAddress("test2@example.com"));

        List<MailRepositoryEntity> items;

        items = dao.searchItems("repo", MailRepositorySearchField.MESSAGE_ID, "test-message-id", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.MESSAGE_ID, "test-MESSAGE-id", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.MESSAGE_ID, "%-message-id", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.MESSAGE_ID, "X", null, null);
        assertEquals(0, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SUBJECT, "test", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SUBJECT, "TEST", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SUBJECT, "test", 1, null);
        assertEquals(1, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SUBJECT, "test", 1, 0);
        assertEquals(0, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SUBJECT, "%st%", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SUBJECT, "X", null, null);
        assertEquals(0, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SENDER, "test1@example.com", null, null);
        assertEquals(1, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SENDER, "test1@EXAMPLE.COM", null, null);
        assertEquals(1, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SENDER, "test2@example.com", null, null);
        assertEquals(1, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.SENDER, "X", null, null);
        assertEquals(0, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.FROM, "%@example.com", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.FROM, "%@EXAMPLE.com", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.FROM, "%@example.COM", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.FROM, "X", null, null);
        assertEquals(0, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.RECIPIENTS, "%@example.com", null, null);
        assertEquals(2, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.RECIPIENTS, "%@example.com", 1, null);
        assertEquals(1, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.RECIPIENTS, "%@example.com", 0, 1);
        assertEquals(1, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.RECIPIENTS, "r3@example.com", null, null);
        assertEquals(1, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.RECIPIENTS, "R3@example.COM", null, null);
        assertEquals(1, items.size());

        items = dao.searchItems("repo", MailRepositorySearchField.RECIPIENTS, "X", null, null);
        assertEquals(0, items.size());
    }

    @Test
    public void testGetSearchCount()
    throws Exception
    {
        MailRepositoryDAO dao = createDAO();

        MailRepositoryEntity entity = new MailRepositoryEntity("repo2", message);
        dao.persist(entity);

        entity = new MailRepositoryEntity("repo", message);
        dao.persist(entity);

        List<InternetAddress> recipients1 = new LinkedList<>();

        recipients1.add(new InternetAddress("r1@example.com"));
        recipients1.add(new InternetAddress("r2@example.com"));

        entity.setRecipients(recipients1);
        entity.setSender(new InternetAddress("test1@example.com"));

        entity = new MailRepositoryEntity("repo", message);
        dao.persist(entity);

        List<InternetAddress> recipients2 = new LinkedList<>();

        recipients2.add(new InternetAddress("r1@example.com"));
        recipients2.add(new InternetAddress("r2@example.com"));
        recipients2.add(new InternetAddress("r3@example.com"));

        entity.setRecipients(recipients2);
        entity.setSender(new InternetAddress("test2@example.com"));

        assertEquals(2, dao.getSearchCount("repo", MailRepositorySearchField.MESSAGE_ID, "%-message-id"));
        assertEquals(0, dao.getSearchCount("repo", MailRepositorySearchField.MESSAGE_ID, "X"));

        assertEquals(2, dao.getSearchCount("repo", MailRepositorySearchField.SUBJECT, "test"));
        assertEquals(0, dao.getSearchCount("repo", MailRepositorySearchField.SUBJECT, "X"));

        assertEquals(2, dao.getSearchCount("repo", MailRepositorySearchField.SENDER, "test%@example.com"));
        assertEquals(0, dao.getSearchCount("repo", MailRepositorySearchField.SENDER, "X"));

        assertEquals(2, dao.getSearchCount("repo", MailRepositorySearchField.FROM, "from@example.com"));
        assertEquals(0, dao.getSearchCount("repo", MailRepositorySearchField.FROM, "X"));

        assertEquals(2, dao.getSearchCount("repo", MailRepositorySearchField.RECIPIENTS, "%@example.com"));
        assertEquals(1, dao.getSearchCount("repo", MailRepositorySearchField.RECIPIENTS, "r3@example.com"));
        assertEquals(0, dao.getSearchCount("repo", MailRepositorySearchField.RECIPIENTS, "X"));
    }
}
