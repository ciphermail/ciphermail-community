/*
 * Copyright (c) 2009-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.impl.StandardSerialNumberGenerator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class SMIMECABuilderTest
{
    @Test
    public void testBuildCA()
    throws Exception
    {
        CABuilder builder = new SMIMECABuilder(new StandardSerialNumberGenerator());

        X500PrincipalBuilder issuerBuilder = X500PrincipalBuilder.getInstance();

        issuerBuilder.setCommonName("Test Root");
        issuerBuilder.setCountryCode("NL");
        issuerBuilder.setLocality("Amsterdam");
        issuerBuilder.setState("NH");

        X500PrincipalBuilder subjectBuilder = X500PrincipalBuilder.getInstance();

        issuerBuilder.setCommonName("Test CA");
        issuerBuilder.setCountryCode("NL");
        issuerBuilder.setLocality("Amsterdam");
        issuerBuilder.setState("NH");

        CABuilderParameters parameters = new CABuilderParametersImpl();

        parameters.setRootSubject(issuerBuilder.buildPrincipal());
        parameters.setIntermediateSubject(subjectBuilder.buildPrincipal());

        CABuilderResult result = builder.buildCA(parameters);

        assertNotNull(result);
        assertNotNull(result.getIntermediate());
        assertNotNull(result.getIntermediate().getCertificate());
        assertNotNull(result.getIntermediate().getPrivateKey());
        assertNotNull(result.getRoot());
        assertNotNull(result.getRoot().getCertificate());
        assertNotNull(result.getRoot().getPrivateKey());
        assertEquals(parameters.getRootSubject(), result.getRoot().getCertificate().getSubjectX500Principal());
        assertEquals(parameters.getRootSubject(), result.getRoot().getCertificate().getIssuerX500Principal());
        assertEquals(parameters.getIntermediateSubject(), result.getIntermediate().getCertificate().getSubjectX500Principal());
        assertEquals(parameters.getRootSubject(), result.getIntermediate().getCertificate().getIssuerX500Principal());

        /*
         * The intermediate should be signed by the root
         */
        result.getIntermediate().getCertificate().verify(result.getRoot().getCertificate().getPublicKey());
    }
}
