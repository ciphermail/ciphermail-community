/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.cms.SignerInfo;
import com.ciphermail.core.common.security.cms.SignerInfoException;
import com.ciphermail.core.test.TestUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.math.BigInteger;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SMIMESignedInspectorImplTest
{
    private static SecurityFactory securityFactory;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }

    /*
     * Check for some headers which should exist because they were added to the signed or encrypted blob.
     */
    private static void checkForEmbeddedHeaders(MimeMessage message)
    throws Exception
    {
        // the message should contain the signed from, to and subject
        assertEquals("<test@example.com>", message.getHeader("from", ","));
        assertEquals("<test@example.com>", message.getHeader("to", ","));
        assertEquals("normal message with attachment", message.getHeader("subject", ","));
    }

    private static X509Certificate getCertificate(List<X509Certificate> certificates, String serial)
    {
        Map<BigInteger, X509Certificate> certMap = new HashMap<>();

        for (X509Certificate certificate : certificates)
        {
            certMap.put(certificate.getSerialNumber(), certificate);
        }

        return certMap.get(new BigInteger(serial, 16));
    }

    @Test
    public void testOpenSSLClearAttachmentOnly()
    throws Exception
    {
        // A signed message that is signed again with OpenSSL
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/openssl-signed-only-attachment.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider(), true);

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(2, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("application/octet-stream"));
    }

    @Test
    public void testOpenSSLClearSignedTwiceMultiAlt()
    throws Exception
    {
        // A signed message that is signed again with OpenSSL
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/openssl-signed-twice-multi-alternative.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider(), true);

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(2, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("multipart/signed"));
    }

    @Test
    public void testMultiAlt()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/multi-alternative.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(1, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("multipart/mixed"));
    }

    @Test
    public void testClearSignedQPSoftLineBreaks()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/quotable-soft-line-breaks.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(1, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("text/plain"));
    }

    @Test(expected = SignerInfoException.class)
    public void testClearSignedTampered()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/clear-signed-tampered.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider(), false);

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(3, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        // Verify fails when using BC's CMSProcessable
        signerInfo.verify(signerCertificate.getPublicKey());
    }

    @Test
    public void testClearSignedDjigzo()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/clear-signed-djigzo.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(3, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("multipart/alternative"));
    }

    /*
     * Fails when the CMSProcessableBodyPartInbound is used (which is used by BC by default)
     */
    @Test
    public void testClearSignedExtraCRLF()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/bb-signed-extra-cr-lf.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(1, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("multipart/mixed"));

        MailUtils.validateMessage(unsignedMessage);
    }

    public void testClearSignedExtraCRLFUsingCMSProcessableBodyPartInbound()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/bb-signed-extra-cr-lf.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider(), false);

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(1, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("multipart/mixed"));

        MailUtils.validateMessage(unsignedMessage);
    }

    @Test
    public void testClearSigned()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/clear-signed-validcertificate.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(3, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("multipart/mixed"));

        checkForEmbeddedHeaders(unsignedMessage);

        MailUtils.validateMessage(unsignedMessage);
    }

    @Test
    public void testClearSignedSwappedParts()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/clear-signed-swapped-parts.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(3, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        // the signing certificate should be there
        X509Certificate signerCertificate = getCertificate(certificates, "115fcd741088707366e9727452c9770");

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("multipart/mixed"));

        checkForEmbeddedHeaders(unsignedMessage);

        MailUtils.validateMessage(unsignedMessage);
    }

    @Test
    public void testOpaqueSigned()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/signed-opaque-validcertificate.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(3, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("multipart/mixed"));

        checkForEmbeddedHeaders(unsignedMessage);

        MailUtils.validateMessage(unsignedMessage);
    }

    @Test
    public void testClearSignedOnePart()
    throws Exception
    {
        MimeMessage signedMessage = TestUtils.loadTestMessage("mail/clear-signed-evolution.eml");

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(signedMessage,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        List<X509Certificate> certificates = inspector.getCertificates();

        assertEquals(3, certificates.size());

        List<X509CRL> crls = inspector.getCRLs();

        assertEquals(0, crls.size());

        List<SignerInfo> signers = inspector.getSigners();

        assertEquals(1, signers.size());

        SignerInfo signerInfo = signers.get(0);

        X509Certificate signerCertificate = certificates.get(0);

        assertNotNull(signerCertificate);

        assertTrue(signerInfo.verify(signerCertificate.getPublicKey()));

        assertNotNull(inspector.getContent());

        MimeMessage unsignedMessage = inspector.getContentAsMimeMessage();

        assertTrue(unsignedMessage.isMimeType("text/plain"));

        String content = (String) unsignedMessage.getContent();

        assertEquals("test\r\n", content);

        File file = TestUtils.createTempFile(".eml");

        MailUtils.writeMessage(unsignedMessage, file);

        unsignedMessage = MailUtils.loadMessage(file);

        assertTrue(unsignedMessage.isMimeType("text/plain"));

        content = (String) unsignedMessage.getContent();

        assertEquals("test\r\n", content);
    }
}
