/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.selector;

import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crl.CRLUtils;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.CRL;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class EncryptionCertificateSelectorTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore certStore;

    @Autowired
    private X509CRLStoreExt crlStore;

    @Autowired
    private PKISecurityServices pKISecurityServices;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Clean key/root/crl store
                certStore.removeAllEntries();
                rootStore.removeAllEntries();
                crlStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private static void addCertificates(File file, X509CertStoreExt certStore)
    throws Exception
    {
        addCertificates(CertificateUtils.readCertificates(file), certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore)
    throws Exception
    {
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate && !certStore.contains((X509Certificate) certificate)) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    private static void addCRLs(File file, X509CRLStoreExt crlStore)
    throws Exception
    {
        addCRLs(CRLUtils.readCRLs(file), crlStore);
    }

    private static void addCRLs(Collection<? extends CRL> crls, X509CRLStoreExt crlStore)
    throws Exception
    {
        for (CRL crl : crls)
        {
            if (crl instanceof X509CRL && !crlStore.contains((X509CRL) crl)) {
                crlStore.addCRL((X509CRL) crl);
            }
        }
    }

    @Test
    public void testGetCertificates()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-root.cer"), rootStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-ca.cer"), certStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/testCertificates.p7b"), certStore);

                EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);

                Set<X509Certificate> certificates = selector.getMatchingCertificates("test@example.com");

                Set<String> serials = new HashSet<>();

                for (X509Certificate certificate : certificates)
                {
                    String serial = X509CertificateInspector.getSerialNumberHex(certificate);

                    serials.add(serial);
                }

                assertEquals(13, certificates.size());
                assertEquals(serials.size(), certificates.size());

                assertTrue(serials.contains("116A448F117FF69FE4F2D4D38F689D7")); /* CriticalEKU */
                assertTrue(serials.contains("1178C38151374D6C4B29F891B9B4A77")); /* KeyUsageNotForSigning*/
                assertTrue(serials.contains("115FD0E5EE990D9426C93DEA720E970")); /* NoCN */
                assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoExtendedKeyUsage*/
                assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoKeyUsage*/
                assertTrue(serials.contains("115FCDE9DC082E7E9C8EEF4CC69B94C")); /* UppercaseEmail*/
                assertTrue(serials.contains("115FCD741088707366E9727452C9770")); /* ValidCertificate*/
                assertTrue(serials.contains("115FCEECCD07FE8929F68CC6B359A5A")); /* EmailInAltNamesNotInSubject*/
                assertTrue(serials.contains("115FCEB7F46B98775DBB8287965F838")); /* EmailInSubjectNotInAltNames*/
                assertTrue(serials.contains("115FD1392A8FF07AA727558FA50B262")); /* MD5Hash*/
                assertTrue(serials.contains("115FD110A82F742D0AE14A71B651962")); /* MultipleEmail*/
                assertTrue(serials.contains("115FD1606444BC50DE5464AF7D0D468")); /* RSA2048*/
                assertTrue(serials.contains("115FD16008275F2616B8A235D761FFF")); /* SHA256Hash*/
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesMissingRoot()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/example-self-signed-10.p7b"), rootStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-ca.cer"), certStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/testCertificates.p7b"), certStore);

                EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);

                assertEquals(10, pKISecurityServices.getRootStore().getCertificates(null).size());

                Set<X509Certificate> certificates = selector.getMatchingCertificates("test@example.com");

                // root is missing
                assertEquals(0, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesIntermediateRevoked()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-root.cer"), rootStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-ca.cer"), certStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/testCertificates.p7b"), certStore);
                addCRLs(new File(TestUtils.getTestDataDir(),
                        "crls/test-root-ca-revoked.crl"), crlStore);

                EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);

                Set<X509Certificate> certificates = selector.getMatchingCertificates("test@example.com");

                // all certificates should have been revoked because the intermediate has been revoked
                assertEquals(0, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }


    @Test
    public void testGetCertificatesEndEntityRevoked()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-root.cer"), rootStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-ca.cer"), certStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/testCertificates.p7b"), certStore);
                addCRLs(new File(TestUtils.getTestDataDir(),
                        "crls/test-ca.crl"), crlStore);

                EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);

                Set<X509Certificate> certificates = selector.getMatchingCertificates("test@example.com");

                Set<String> serials = new HashSet<>();

                for (X509Certificate certificate : certificates)
                {
                    String serial = X509CertificateInspector.getSerialNumberHex(certificate);

                    serials.add(serial);
                }

                assertEquals(12, certificates.size());
                assertEquals(serials.size(), certificates.size());

                assertTrue(serials.contains("116A448F117FF69FE4F2D4D38F689D7")); /* CriticalEKU */
                assertTrue(serials.contains("1178C38151374D6C4B29F891B9B4A77")); /* KeyUsageNotForSigning*/
                assertTrue(serials.contains("115FD0E5EE990D9426C93DEA720E970")); /* NoCN */
                assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoExtendedKeyUsage*/
                assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoKeyUsage*/
                assertTrue(serials.contains("115FCDE9DC082E7E9C8EEF4CC69B94C")); /* UppercaseEmail*/
                // this certificate is revoked
                assertFalse(serials.contains("115FCD741088707366E9727452C9770")); /* ValidCertificate*/

                assertTrue(serials.contains("115FCEECCD07FE8929F68CC6B359A5A")); /* EmailInAltNamesNotInSubject*/
                assertTrue(serials.contains("115FCEB7F46B98775DBB8287965F838")); /* EmailInSubjectNotInAltNames*/
                assertTrue(serials.contains("115FD1392A8FF07AA727558FA50B262")); /* MD5Hash*/
                assertTrue(serials.contains("115FD110A82F742D0AE14A71B651962")); /* MultipleEmail*/
                assertTrue(serials.contains("115FD1606444BC50DE5464AF7D0D468")); /* RSA2048*/
                assertTrue(serials.contains("115FD16008275F2616B8A235D761FFF")); /* SHA256Hash*/
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesSpeed()
    {
        // We will import the certs in a separate transaction. For some reason doing this in the same transaction
        // slows-dowm the repeated test. Another solution is to flush and clear the session just after importing
        // the certificates
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-root.cer"), rootStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/mitm-test-ca.cer"), certStore);
                addCertificates(new File(TestUtils.getTestDataDir(),
                        "certificates/testCertificates.p7b"), certStore);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                final int tries = 1000;

                long start = System.currentTimeMillis();

                for (int i = 0; i < tries; i++)
                {
                    EncryptionCertificateSelector selector = new EncryptionCertificateSelector(pKISecurityServices);

                    Set<X509Certificate> certificates = selector.getMatchingCertificates("test3@example.com");

                    assertFalse(certificates.isEmpty());
                }

                long diff = System.currentTimeMillis() - start;

                double timePerMatch = diff * 0.001 / tries;

                System.out.println("Seconds / match: " + timePerMatch);

                double expected = 0.03 / TestProperties.getTestPerformanceFactor();

                if (timePerMatch > expected)
                {
                    // Note: This might fail on slower systems!!
                    fail("seconds/match too slow. Note: This might fail on slower systems!!!");
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}