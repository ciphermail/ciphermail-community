/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.tools.GPG;
import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPKeyConverter;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class PGPSignatureBuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static final JcePBESecretKeyDecryptorBuilder secretKeyDecryptorBuilder =
            new JcePBESecretKeyDecryptorBuilder();

    private static final JcaPGPKeyConverter keyConverter = new JcaPGPKeyConverter();

    private static List<PGPSecretKey> secretKeys;

    @BeforeClass
    public static void beforeClass()
    throws IOException
    {
        secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/test-multiple-sub-keys.gpg.key"));
    }

    protected PGPSignatureBuilder createBuilder()
    {
        PGPSignatureBuilder builder = new PGPSignatureBuilder();

        builder.setHashAlgorithm(PGPHashAlgorithm.SHA256);
        builder.setPGPDocumentType(PGPDocumentType.TEXT);

        return builder;
    }

    @Test
    public void testSignTextDocumentSingleLine()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        String textToSign = "test 123";

        InputStream input = IOUtils.toInputStream(textToSign, "US-ASCII");

        File outputFile = TestUtils.createTempFile(".asc");

        OutputStream output = new FileOutputStream(outputFile);

        try {
            PGPSecretKey secretKey = secretKeys.get(0);

            PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                    "test".toCharArray()));

            builder.sign(input, output, secretKey.getPublicKey(), keyConverter.getPrivateKey(privateKey));
        }
        finally {
            output.close();
        }

        String signed = FileUtils.readFileToString(outputFile, "US-ASCII");

        assertTrue(signed.contains(textToSign));

        GPG gpg = new GPG();

        assertTrue(gpg.verify(outputFile));
    }

    @Test
    public void testSignTextDocumentMultiline()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        String textToSign = "test 123  \n  \r  test  \r--\r\n";

        InputStream input = IOUtils.toInputStream(textToSign, "US-ASCII");

        File outputFile = TestUtils.createTempFile(".asc");

        OutputStream output = new FileOutputStream(outputFile);

        try {
            PGPSecretKey secretKey = secretKeys.get(0);

            PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                    "test".toCharArray()));

            builder.sign(input, output, secretKey.getPublicKey(), keyConverter.getPrivateKey(privateKey));
        }
        finally {
            output.close();
        }

        String signed = FileUtils.readFileToString(outputFile, "US-ASCII");

        // add extra - for dash escaping
        assertTrue(signed.contains("test 123  \r\n  \r\n  test  \r\n- --\r\n"));

        GPG gpg = new GPG();

        assertTrue(gpg.verify(outputFile));
    }

    @Test
    public void testSignTextDocumentAttachedOpqaue()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        builder.setClearSign(false);
        builder.setDetached(false);

        String textToSign = "test 123  \n  \r  test  \r--\r\n";

        InputStream input = IOUtils.toInputStream(textToSign, "US-ASCII");

        File outputFile = TestUtils.createTempFile(".asc");

        OutputStream output = new FileOutputStream(outputFile);

        try {
            PGPSecretKey secretKey = secretKeys.get(0);

            PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                    "test".toCharArray()));

            builder.sign(input, output, secretKey.getPublicKey(), keyConverter.getPrivateKey(privateKey));
        }
        finally {
            output.close();
        }

        GPG gpg = new GPG();

        assertTrue(gpg.verify(outputFile));
    }

    @Test
    public void testSignTextDocumentStartWithEmptyLine()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        String textToSign = "\ntest 123";

        InputStream input = IOUtils.toInputStream(textToSign, "US-ASCII");

        File outputFile = TestUtils.createTempFile(".asc");

        OutputStream output = new FileOutputStream(outputFile);

        try {
            PGPSecretKey secretKey = secretKeys.get(0);

            PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                    "test".toCharArray()));

            builder.sign(input, output, secretKey.getPublicKey(), keyConverter.getPrivateKey(privateKey));
        }
        finally {
            output.close();
        }

        String signed = FileUtils.readFileToString(outputFile, "US-ASCII");

        assertTrue(signed.contains(textToSign));

        GPG gpg = new GPG();

        assertTrue(gpg.verify(outputFile));
    }

    @Test
    public void testSignBinaryDocument()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        builder.setPGPDocumentType(PGPDocumentType.BINARY);

        // We need to copy the document to sign to tmp store so we can verify it
        File fileToSign = TestUtils.createTempFile(".jar");

        FileUtils.copyFile(new File(TEST_BASE, "documents/testJAR.jar"), fileToSign);

        // Signature file must be the same filename with .asc added
        File signatureFile = new File(fileToSign.getPath() + ".asc");
        signatureFile.deleteOnExit();;

        OutputStream output = new FileOutputStream(signatureFile);

        try {
            PGPSecretKey secretKey = secretKeys.get(0);

            PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                    "test".toCharArray()));

            builder.sign(new FileInputStream(fileToSign), output, secretKey.getPublicKey(),
                    keyConverter.getPrivateKey(privateKey));
        }
        finally {
            output.close();
        }

        GPG gpg = new GPG();

        assertTrue(gpg.verify(signatureFile));
    }

    @Test
    public void testSignBinaryDocumentNotDetached()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        builder.setPGPDocumentType(PGPDocumentType.BINARY);
        builder.setDetached(false);

        File fileToSign = new File(TEST_BASE, "documents/djigzo-whitepaper.pdf");

        File signedFile = TestUtils.createTempFile(".pgp");

        OutputStream output = new FileOutputStream(signedFile);

        try {
            PGPSecretKey secretKey = secretKeys.get(0);

            PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                    "test".toCharArray()));

            builder.sign(new FileInputStream(fileToSign), output, secretKey.getPublicKey(),
                    keyConverter.getPrivateKey(privateKey));
        }
        finally {
            output.close();
        }

        GPG gpg = new GPG();

        assertTrue(gpg.verify(signedFile));
    }

    @Test
    public void testSignBinaryDocumentNotDetachedNoArmor()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        builder.setPGPDocumentType(PGPDocumentType.BINARY);
        builder.setDetached(false);
        builder.setArmor(false);

        File fileToSign = new File(TEST_BASE, "documents/djigzo-whitepaper.pdf");

        File signedFile = TestUtils.createTempFile(".pgp");

        OutputStream output = new FileOutputStream(signedFile);

        try {
            PGPSecretKey secretKey = secretKeys.get(0);

            PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                    "test".toCharArray()));

            builder.sign(new FileInputStream(fileToSign), output, secretKey.getPublicKey(),
                    keyConverter.getPrivateKey(privateKey));
        }
        finally {
            output.close();
        }

        GPG gpg = new GPG();

        assertTrue(gpg.verify(signedFile));
    }

    @Test
    public void testSignBinaryDocumentSmall()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        builder.setPGPDocumentType(PGPDocumentType.BINARY);

        // We need to copy the document to sign to tmp store so we can verify it
        File fileToSign = TestUtils.createTempFile(".txt");

        FileUtils.copyFile(new File(TEST_BASE, "documents/test.txt"), fileToSign);

        // Signature file must be the same filename with .asc added
        File signatureFile = new File(fileToSign.getPath() + ".asc");
        signatureFile.deleteOnExit();;

        OutputStream output = new FileOutputStream(signatureFile);

        try {
            PGPSecretKey secretKey = secretKeys.get(0);

            PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                    "test".toCharArray()));

            builder.sign(new FileInputStream(fileToSign), output, secretKey.getPublicKey(),
                    keyConverter.getPrivateKey(privateKey));
        }
        finally {
            output.close();
        }

        GPG gpg = new GPG();

        assertTrue(gpg.verify(signatureFile));
    }

    @Test
    public void testSignBinaryEmptyFile()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        builder.setPGPDocumentType(PGPDocumentType.BINARY);

        // We need to copy the document to sign to tmp store so we can verify it
        File fileToSign = TestUtils.createTempFile(".xml");

        FileUtils.copyFile(new File(TEST_BASE, "documents/null-file.xml"), fileToSign);

        // Signature file must be the same filename with .asc added
        File signatureFile = new File(fileToSign.getPath() + ".asc");
        signatureFile.deleteOnExit();;

        OutputStream output = new FileOutputStream(signatureFile);

        try {
            PGPSecretKey secretKey = secretKeys.get(0);

            PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                    "test".toCharArray()));

            builder.sign(new FileInputStream(fileToSign), output, secretKey.getPublicKey(),
                    keyConverter.getPrivateKey(privateKey));
        }
        finally {
            output.close();
        }

        GPG gpg = new GPG();

        assertTrue(gpg.verify(signatureFile));
    }

    @Test
    public void testSpeedSignBinaryDocument()
    throws Exception
    {
        PGPSignatureBuilder builder = createBuilder();

        builder.setPGPDocumentType(PGPDocumentType.BINARY);

        PGPSecretKey secretKey = secretKeys.get(0);

        PGPPrivateKey privateKey = secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray()));

        File fileToSign = new File(TEST_BASE, "mail/test-1280K.eml");

        int repeat = 200;

        long start = System.currentTimeMillis();

        byte[] input = FileUtils.readFileToByteArray(fileToSign);

        for (int i = 0; i < repeat; i++)
        {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                builder.sign(new ByteArrayInputStream(input), bos,
                        secretKey.getPublicKey(),
                        keyConverter.getPrivateKey(privateKey));

                assertTrue(bos.size() > 0);
        }

        double signsPerSec = repeat * 1000.0 / (System.currentTimeMillis() - start);

        System.out.println("Signs/second: " + signsPerSec);

        double expected = 20 * TestProperties.getTestPerformanceFactor();

        assertTrue("can fail on slower systems.", signsPerSec > expected);
    }
}
