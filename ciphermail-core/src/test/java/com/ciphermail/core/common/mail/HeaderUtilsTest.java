/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.mail.matcher.ContentHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.NotHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.RegExHeaderNameMatcher;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertTrue;

class HeaderUtilsTest
{
    private int getHeaderCount(Enumeration<?> headerEnum)
    {
        int count = 0;

        while (headerEnum.hasMoreElements())
        {
            headerEnum.nextElement();
            count++;
        }

        return count;
    }

    @Test
    void testCopyHeaders()
    throws Exception
    {
        MimeBodyPart destination = new MimeBodyPart();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        HeaderMatcher matcher = new ContentHeaderNameMatcher();

        HeaderUtils.copyHeaders(message, destination, matcher);

        Assertions.assertEquals(1, getHeaderCount(destination.getAllHeaders()));
        Assertions.assertEquals(message.getContentType(), destination.getContentType());
    }

    @Test
    void testCopyHeadersRegEx()
    throws Exception
    {
        MimeBodyPart destination = new MimeBodyPart();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        HeaderMatcher matcher = new RegExHeaderNameMatcher("^x-test.*", Pattern.CASE_INSENSITIVE);

        HeaderUtils.copyHeaders(message, destination, matcher);

        Assertions.assertEquals(4, getHeaderCount(destination.getAllHeaders()));
    }

    @Test
    void testCopyHeadersNotMatcher()
    throws Exception
    {
        MimeBodyPart destination = new MimeBodyPart();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        HeaderMatcher matcher = new RegExHeaderNameMatcher("^x-test.*", Pattern.CASE_INSENSITIVE);

        HeaderMatcher notMatcher = new NotHeaderNameMatcher(matcher);

        HeaderUtils.copyHeaders(message, destination, notMatcher);

        Assertions.assertEquals(10, getHeaderCount(destination.getAllHeaders()));
    }

    @Test
    void testRemoveHeaders()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        HeaderMatcher matcher = new RegExHeaderNameMatcher("^x-test.*", Pattern.CASE_INSENSITIVE);

        HeaderUtils.removeHeaders(message, matcher);

        Assertions.assertEquals(10, getHeaderCount(message.getAllHeaders()));
    }

    @Test
    void testPrependHeaderLine()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/html-alternative.eml");

        Assertions.assertNull(message.getHeader("X-Djigzo-Test"));

        HeaderUtils.prependHeaderLine(message, "X-Djigzo-Test: test");

        // Don't call message#saveChanges because that result in resorting
        MailUtils.validateMessage(message);

        Assertions.assertEquals("test", message.getHeader("X-Djigzo-Test", ", "));

        Enumeration<?> headerEnum = message.getAllHeaderLines();

        Assertions.assertEquals("X-Djigzo-Test: test", (headerEnum.nextElement()));
    }

    @Test
    void testPrependHeaderLineMultipleLines()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/html-alternative.eml");

        Assertions.assertNull(message.getHeader("X-Djigzo-Test"));

        HeaderUtils.prependHeaderLine(message, "X-Djigzo-Test: test", "X-Djigzo-Test-2: test");

        // Don't call message#saveChanges because that result in resorting
        MailUtils.validateMessage(message);

        Assertions.assertEquals("test", message.getHeader("X-Djigzo-Test", ", "));
        Assertions.assertEquals("test", message.getHeader("X-Djigzo-Test-2", ", "));

        Enumeration<?> headerEnum = message.getAllHeaderLines();

        Assertions.assertEquals("X-Djigzo-Test: test", (headerEnum.nextElement()));
        Assertions.assertEquals("X-Djigzo-Test-2: test", (headerEnum.nextElement()));
    }

    @Test
    void testRemoveAngleBrackets()
    {
        Assertions.assertEquals("test", HeaderUtils.removeAngleBrackets("<test>"));
        Assertions.assertEquals("", HeaderUtils.removeAngleBrackets("<>"));
        Assertions.assertEquals("test", HeaderUtils.removeAngleBrackets("test"));
        Assertions.assertEquals("<test", HeaderUtils.removeAngleBrackets("<test"));
        Assertions.assertEquals("<test", HeaderUtils.removeAngleBrackets("<test"));
        Assertions.assertEquals(" <test> ", HeaderUtils.removeAngleBrackets(" <test> "));
        Assertions.assertNull(HeaderUtils.removeAngleBrackets(null));
    }

    @Test
    void testGetCharset()
    {
        Assertions.assertEquals("ISO-8859-1", HeaderUtils.getCharsetFromContentType(
                "text/plain; charset=ISO-8859-1", "us-ascii"));
        // us-ascii will be mapped to ISO-8859-1
        Assertions.assertEquals("ISO-8859-1", HeaderUtils.getCharsetFromContentType(
                "text/plain; charset=us-ascii", "us-ascii"));
        Assertions.assertEquals("something", HeaderUtils.getCharsetFromContentType(
                "text/plain; charset=something", "us-ascii"));
        Assertions.assertEquals("other", HeaderUtils.getCharsetFromContentType(
                "text/plain", "other"));
        Assertions.assertNull(HeaderUtils.getCharsetFromContentType("text/plain", null));
    }

    @Test
    void testGetContentTypeParameter()
    throws Exception
    {
        Assertions.assertEquals("flowed", HeaderUtils.getContentTypeParameter(
                "text/plain; charset=UTF-8; format=flowed",
                "format"));
        Assertions.assertNull(HeaderUtils.getContentTypeParameter("text/plain;   charset=UTF-8; format=flowed",
                "123"));
    }

    @Test
    void testsetContentTypeParameter()
    throws Exception
    {
        Assertions.assertEquals("text/plain; charset=UTF-8; name=123", HeaderUtils.setContentTypeParameter(
                "text/plain; charset=UTF-8; name=abc", "name", "123"));
        Assertions.assertEquals("text/plain; charset=UTF-8; name=123", HeaderUtils.setContentTypeParameter(
                "text/plain; charset=UTF-8", "name", "123"));
        Assertions.assertEquals("text/plain; charset=UTF-8; name=\"ü\"", HeaderUtils.setContentTypeParameter(
                "text/plain; charset=UTF-8", "name", "ü"));
    }

    @Test
    void testGetHeaderLines()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/folded-header.eml");

        String[] headerLines = HeaderUtils.getHeaderLines(message);

        Assertions.assertEquals(5, headerLines.length);

        /*
         * The lines are "unfolded" but the CR/LF and tab are still in the line
         */
        Assertions.assertEquals("X-Folded-Header: Test of a \r\n\tfolded header", headerLines[2]);
    }

    @Test
    void testDecodeTextQuietly()
    {
        Assertions.assertEquals("test", HeaderUtils.decodeTextQuietly("test"));
        Assertions.assertEquals("Ö", HeaderUtils.decodeTextQuietly("=?UTF-8?B?w5Y=?="));
        Assertions.assertNull(HeaderUtils.decodeTextQuietly(null));
    }

    @Test
    void testEncodeTextQuietly()
    {
        Assertions.assertEquals("test", HeaderUtils.encodeTextQuietly("test", "UTF-8"));
        Assertions.assertEquals("=?UTF-8?B?w5Y=?=", HeaderUtils.encodeTextQuietly("Ö", "UTF-8"));
        Assertions.assertNull(HeaderUtils.encodeTextQuietly(null, "UTF-8"));
    }

    @Test
    void testSkipHeaders()
    throws IOException
    {
        String headers = "h1: v1\r\nh2:v2\r\n\r\nbody";

        InputStream input = IOUtils.toInputStream(headers, StandardCharsets.UTF_8);

        HeaderUtils.skipHeaders(input);

        String body = IOUtils.toString(input, StandardCharsets.UTF_8);

        Assertions.assertEquals("body", body);

        input = IOUtils.toInputStream(headers, StandardCharsets.UTF_8);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        HeaderUtils.skipHeaders(input, bos);

        body = IOUtils.toString(input, StandardCharsets.UTF_8);

        Assertions.assertEquals("body", body);
        Assertions.assertEquals("h1: v1\r\nh2:v2\r\n\r\n", bos.toString());
    }

    @Test
    void testIsValidHeaderName()
    {
        assertTrue(HeaderUtils.isValidHeaderName("X-CipherMail-Test"));
        assertTrue(HeaderUtils.isValidHeaderName("X_CipherMail_Test"));
        Assertions.assertFalse(HeaderUtils.isValidHeaderName(" "));
        Assertions.assertFalse(HeaderUtils.isValidHeaderName("Test 123"));
        Assertions.assertTrue(HeaderUtils.isValidHeaderName("Test123"));
        Assertions.assertFalse(HeaderUtils.isValidHeaderName("Test@123"));
    }
}
