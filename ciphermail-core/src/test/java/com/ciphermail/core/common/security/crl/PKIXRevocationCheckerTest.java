/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilder;
import com.ciphermail.core.common.security.certpath.PKIXCertificatePathBuilder;
import com.ciphermail.core.common.security.certpath.SMIMEExtendedKeyUsageCertPathChecker;
import com.ciphermail.core.common.security.certpath.TrustAnchorBuilder;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.certstore.jce.X509CertStoreParameters;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PKIXRevocationCheckerTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt x509CertStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt x509RootStore;

    @Autowired
    private X509CRLStoreExt x509CRLStore;

    @Autowired
    private TrustAnchorBuilder trustAnchorBuilder;

    private CertStore certStore;
    private X509CertStoreParameters certStoreParams;
    private X509CertStoreParameters rootStoreParams;

    @Before
    public void setup()
    throws Exception
    {
        certStoreParams = new X509CertStoreParameters(x509CertStore, x509CRLStore);

        certStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, certStoreParams,
                CipherMailProvider.PROVIDER);

        rootStoreParams = new X509CertStoreParameters(x509RootStore);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStoreParams.getCertStore().removeAllEntries();
                rootStoreParams.getCertStore().removeAllEntries();
                certStoreParams.getCRLStore().removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCRL(File crlFile, X509CRLStoreExt crlStore)
    throws Exception
    {
        crlStore.addCRL(TestUtils.loadX509CRL(crlFile));
    }

    private void addCRL(InputStream crlInputStream, X509CRLStoreExt crlStore)
    throws Exception
    {
        crlStore.addCRL(TestUtils.loadX509CRL(crlInputStream));
    }

    private void addCertificates(File file, X509CertStoreExt certStore)
    throws Exception
    {
        for (Certificate certificate : CertificateUtils.readCertificates(file))
        {
            if (certificate instanceof X509Certificate && !certStore.contains((X509Certificate) certificate)) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    private PKIXCertPathBuilderResult getCertPathBuilderResult(X509CertSelector selector)
    throws Exception
    {
        CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

        builder.setTrustAnchors(trustAnchorBuilder.getTrustAnchors());
        builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
        builder.addCertStore(certStore);
        builder.setRevocationEnabled(false);

        Date now = TestUtils.parseDate("24-Mar-2008 16:38:35 GMT");

        builder.setDate(now);

        CertPathBuilderResult result = builder.buildPath(selector);

        assertTrue(result instanceof PKIXCertPathBuilderResult);

        return (PKIXCertPathBuilderResult) result;
    }

    @Test
    public void testNullExtKeyUsage()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/windows-xp-all-roots.p7b"),
                        rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/windows-xp-all-intermediates.p7b"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/thawte-freemail-valid-till-091108.p7b"),
                        certStoreParams.getCertStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("2A0968372BE9522A92577B6F186CBBAA"));

                selector.setIssuer(new X500Principal(
                        "CN=Thawte Personal Freemail Issuing CA, O=Thawte Consulting (Pty) Ltd., C=ZA"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("24-Mar-2007 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEndCertRevoked()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
                assertEquals(RevocationReason.PRIVILEGE_WITHDRAWN, revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUnknownMissingEndEntityCRL()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
                assertNull(revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUnknownMissingIntermediateCRL()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
                assertNull(revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testNotRevoked()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationStatus.getStatus());
                assertNull(revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEndCertRevokedInFuture()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2005 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationStatus.getStatus());
                assertNull(revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEndCertRevokedCRLOverdue()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("22-Nov-2040 11:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
                assertEquals(RevocationReason.PRIVILEGE_WITHDRAWN, revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testNotRevokedButCRLOverdue()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2030 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.EXPIRED, revocationStatus.getStatus());
                assertNull(revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEndCertRevokedButCertExpiresBeforeCRLValid()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca-thisupdate-far-future.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("22-Nov-2028 11:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
                assertEquals(RevocationReason.KEY_COMPROMISE, revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEndCertNotRevokedCRLNotYetValid()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca-thisupdate-far-future.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("15-Dec-2007 11:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
                assertNull(revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEndCertNotRevokedCRLNotYetValidButOtherValid()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca-thisupdate-far-future.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-ca.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("15-Dec-2007 11:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.NOT_REVOKED, revocationStatus.getStatus());
                assertNull(revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testCARevoked()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-root-ca-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
                assertEquals(RevocationReason.CA_COMPROMISE, revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testCARevokedMultipleCRLs()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
                assertEquals(RevocationReason.CA_COMPROMISE, revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testMissingRootCRL()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
                assertNull(revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testIncorrectCACRL()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crls
                addCRL(new File(TEST_BASE, "crls/test-ca-signed-incorrect-key.crl"), certStoreParams.getCRLStore());
                addCRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"), certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2007 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.UNKNOWN, revocationStatus.getStatus());
                assertNull(revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testLargeCRL()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"), certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                // add crl
                addCRL(new GZIPInputStream(new FileInputStream(new File(TEST_BASE, "crls/mitm-ca-large.crl.gz"))),
                        certStoreParams.getCRLStore());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                PKIXCertPathBuilderResult result = getCertPathBuilderResult(selector);

                CertPath certPath = result.getCertPath();

                TrustAnchor trustAnchor = result.getTrustAnchor();

                assertNotNull(trustAnchor);
                assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                        trustAnchor.getTrustedCert().getSubjectX500Principal().toString());

                PKIXRevocationChecker revocationChecker = new PKIXRevocationChecker(certStoreParams.getCRLStore());

                Date now = TestUtils.parseDate("01-Dec-2012 16:38:35 GMT");

                RevocationResult revocationStatus = revocationChecker.getRevocationStatus(certPath, trustAnchor, now);

                assertEquals(RevocationStatus.REVOKED, revocationStatus.getStatus());
                assertEquals(RevocationReason.PRIVILEGE_WITHDRAWN, revocationStatus.getReason());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
