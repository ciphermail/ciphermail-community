/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties.hibernate;

import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class NamedBlobManagerImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private NamedBlobManager namedBlobManager;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                namedBlobManager.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private NamedBlob createNamedBlob(String category, String name) {
        return transactionOperations.execute(status -> namedBlobManager.createNamedBlob(category, name));
    }

    private NamedBlob getNamedBlob(String category, String name) {
        return transactionOperations.execute(status -> namedBlobManager.getNamedBlob(category, name));
    }

    private void deleteNamedBlob(String category, String name) {
        transactionOperations.executeWithoutResult(status -> namedBlobManager.deleteNamedBlob(category, name));
    }

    private List<? extends NamedBlob> getByCategory(String category, Integer firstResult, Integer maxResults) {
        return transactionOperations.execute(status -> namedBlobManager.getByCategory(category, firstResult, maxResults));

    }

    private long getByCategoryCount(String category) {
        return transactionOperations.execute(status -> namedBlobManager.getByCategoryCount(category)).longValue();
    }

    private void addChild(String parentCategory, String parentName, String childCategory, String childName)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            NamedBlob parent = namedBlobManager.getNamedBlob(parentCategory, parentName);

            assertNotNull(parent);

            NamedBlob child = namedBlobManager.getNamedBlob(childCategory, childName);

            assertNotNull(child);

            parent.getNamedBlobs().add(child);
        });
    }

    private void deleteChild(String parentCategory, String parentName, String childCategory, String childName)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            NamedBlob parent = namedBlobManager.getNamedBlob(parentCategory, parentName);

            assertNotNull(parent);

            NamedBlob child = namedBlobManager.getNamedBlob(childCategory, childName);

            assertNotNull(child);

            parent.getNamedBlobs().remove(child);
        });
    }

    private void assertChildSize(String parentCategory, String parentName, int size)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            NamedBlob parent = namedBlobManager.getNamedBlob(parentCategory, parentName);

            assertNotNull(parent);

            assertEquals(size, parent.getNamedBlobs().size());
        });
    }

    private long getReferencedByCount(String parentCategory, String name)
    {
        return transactionOperations.execute(status ->
        {
            NamedBlob namedBlob = namedBlobManager.getNamedBlob(parentCategory, name);

            assertNotNull(namedBlob);

            return namedBlobManager.getReferencedByCount(namedBlob);
        }).longValue();
    }

    private List<NamedBlob> getReferencedBy(String parentCategory, String name, Integer firstResult,
            Integer maxResults)
    {
        return transactionOperations.execute(status ->
        {
            NamedBlob namedBlob = namedBlobManager.getNamedBlob(parentCategory, name);

            assertNotNull(namedBlob);

            List<? extends NamedBlob> blobs = namedBlobManager.getReferencedBy(namedBlob, firstResult, maxResults);

            List<NamedBlob> result = new LinkedList<>();

            // copy to force them to be loaded (except the binary blob and childs)
            for (NamedBlob blob : blobs) {
                result.add(blob);
            }

            return result;
        });
    }

    @Test
    public void testCreateNamedBlob()
    {
        String category = "category";
        String name = "name";

        assertEquals(0, getByCategoryCount(category));

        NamedBlob blob = createNamedBlob(category, name);

        assertEquals(0, getReferencedByCount(category, name));

        assertNotNull(blob);
        assertEquals(category, blob.getCategory());
        assertEquals(name, blob.getName());
        assertEquals(0, blob.getNamedBlobs().size());

        assertEquals(1, getByCategoryCount(category));
    }

    @Test
    public void testCreateDuplicateNamedBlob()
    {
        String category = "category";
        String name = "name";

        assertEquals(0, getByCategoryCount(category));

        createNamedBlob(category, name);

        try {
            createNamedBlob(category, name);

            fail("DataIntegrityViolationException should have been thrown.");
        }
        catch(DataIntegrityViolationException e) {
            // expected
        }
    }

    @Test
    public void testGetNamedBlob()
    {
        String category = "category";
        String name = "name";

        assertEquals(0, getByCategoryCount(category));

        createNamedBlob(category, name);

        assertEquals(1, getByCategoryCount(category));

        NamedBlob blob = getNamedBlob(category, name);

        assertNotNull(blob);
        assertEquals(category, blob.getCategory());
        assertEquals(name, blob.getName());

        assertEquals(1, getByCategoryCount(category));
    }

    @Test
    public void testDeleteNamedBlob()
    {
        String category = "category";
        String name = "name";
        String name2 = "name2";

        assertEquals(0, getByCategoryCount(category));

        createNamedBlob(category, name);
        createNamedBlob(category, name2);

        assertEquals(2, getByCategoryCount(category));

        NamedBlob blob = getNamedBlob(category, name);

        assertNotNull(blob);

        deleteNamedBlob(category, name);

        assertEquals(1, getByCategoryCount(category));

        assertNull(getNamedBlob(category, name));
        assertNotNull(getNamedBlob(category, name2));
    }

    @Test
    public void testGetByCategory()
    {
        String category1 = "category1";
        String category2 = "category2";

        assertEquals(0, getByCategoryCount(category1));
        assertEquals(0, getByCategoryCount(category2));

        int toAdd = 9;

        for (int i = toAdd; i > 0; i--)
        {
            createNamedBlob(category1, "name" + i);
            createNamedBlob(category2, "other" + i);
        }

        assertEquals(toAdd, getByCategoryCount(category1));
        assertEquals(toAdd, getByCategoryCount(category2));

        List<? extends NamedBlob> blobs1 = getByCategory(category1, null, null);

        assertEquals(toAdd, blobs1.size());

        for (int i = 0; i < toAdd; i++)
        {
            assertEquals(category1, blobs1.get(i).getCategory());
            assertEquals("name" + (i + 1), blobs1.get(i).getName());
        }

        List<? extends NamedBlob> blobs2 = getByCategory(category2, null, null);

        assertEquals(toAdd, blobs2.size());

        for (int i = 0; i < toAdd; i++)
        {
            assertEquals(category2, blobs2.get(i).getCategory());
            assertEquals("other" + (i + 1), blobs2.get(i).getName());
        }

        // now test with only a few results
        int firstResult = 2;
        int maxResults = 3;

        blobs1 = getByCategory(category1, firstResult, maxResults);

        assertEquals(maxResults, blobs1.size());

        for (int i = 0; i < maxResults; i++)
        {
            assertEquals(category1, blobs1.get(i).getCategory());
            assertEquals("name" + (i + 1 + firstResult), blobs1.get(i).getName());
        }
    }

    @Test
    public void testAddChildBlobs()
    {
        String parentCategory = "parentCategory";
        String parentName = "parentName";
        String childCategory = "childCategory";
        String childName = "childName";

        createNamedBlob(parentCategory, parentName);
        createNamedBlob(childCategory, childName);

        addChild(parentCategory, parentName, childCategory, childName);

        assertChildSize(parentCategory, parentName, 1);

        assertEquals(1, getReferencedByCount(childCategory, childName));
    }

    @Test
    public void testDeleteChildInUse()
    {
        String category = "category";
        String parentName = "parentName";
        String childName = "childName";

        createNamedBlob(category, parentName);
        createNamedBlob(category, childName);

        addChild(category, parentName, category, childName);

        assertChildSize(category, parentName, 1);

        assertEquals(1, getReferencedByCount(category, childName));

        try {
            deleteNamedBlob(category, childName);

            fail("should fail.");
        }
        catch (DataIntegrityViolationException e) {
            // expected
        }

        deleteChild(category, parentName, category, childName);
        deleteNamedBlob(category, childName);
        assertChildSize(category, parentName, 0);
    }

    @Test
    public void testReferencedBy()
    {
        String category = "category";
        String parent1 = "parent1";
        String parent2 = "parent2";
        String child = "childName";

        createNamedBlob(category, parent1);
        createNamedBlob(category, parent2);
        createNamedBlob(category, child);

        addChild(category, parent1, category, child);
        addChild(category, parent2, category, child);

        assertEquals(2, getReferencedByCount(category, child));

        List<NamedBlob> blobs = getReferencedBy(category, child, null, null);

        // sort to make order deterministic
        blobs.sort(Comparator.comparing(Object::toString));

        assertEquals(2, blobs.size());

        NamedBlob blob = blobs.get(0);

        assertEquals(parent1, blob.getName());

        blob = blobs.get(1);

        assertEquals(parent2, blob.getName());

        // because order of returned blobs can differ between databases, we collect the result and sort
        List<NamedBlob> toSort = new LinkedList<>();

        // get second item
        blobs = getReferencedBy(category, child, 1, 1);
        assertEquals(1, blobs.size());


        toSort.add(blobs.get(0));

        assertEquals(parent2, blob.getName());

        // check with null maxResults
        assertEquals(1, getReferencedBy(category, child, 1, null).size());

        // get first item
        blobs = getReferencedBy(category, child, 0, 1);

        assertEquals(1, blobs.size());

        toSort.add(blobs.get(0));

        // sort to make order deterministic
        toSort.sort(Comparator.comparing(Object::toString));
        assertEquals(parent1, toSort.get(0).getName());
        assertEquals(parent2, toSort.get(1).getName());

        deleteChild(category, parent1, category, child);

        assertEquals(1, getReferencedByCount(category, child));

        blobs = getReferencedBy(category, child, null, null);

        assertEquals(1, blobs.size());

        blob = blobs.get(0);

        assertEquals(parent2, blob.getName());

        deleteChild(category, parent2, category, child);

        assertEquals(0, getReferencedByCount(category, child));

        blobs = getReferencedBy(category, child, null, null);

        assertEquals(0, blobs.size());
    }
}
