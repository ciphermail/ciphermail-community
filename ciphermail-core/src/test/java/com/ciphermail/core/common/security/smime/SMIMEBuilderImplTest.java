/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.SkipHeadersOutputStream;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.cms.SignerIdentifier;
import com.ciphermail.core.common.security.cms.SignerInfo;
import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.common.util.WordIterator;
import com.ciphermail.core.test.OpenSSL;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientInfoGenerator;
import org.bouncycastle.mail.smime.SMIMEEnvelopedGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Test for SMIMEBuilderImpl. This test requires openssl to be installed and in the path because
 * openssl is used for interoperability testing and testing whether the build s/mime messages
 * are correct.
 *
 * @author Martijn Brinkers
 *
 */
class SMIMEBuilderImplTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;

    private static X509Certificate rootCertificate;
    private static X509Certificate encryptionCertificate;
    private static KeyStore.PasswordProtection passwd;
    private static PrivateKeyEntry privateKeyEntry;

    @BeforeAll
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        keyStore = loadKeyStore(new File(TEST_BASE, "keys/testCertificates.p12"), "test");

        rootCertificate = (X509Certificate) keyStore.getCertificate("root");
        encryptionCertificate = (X509Certificate) keyStore.getCertificate("ValidCertificate");

        passwd = new KeyStore.PasswordProtection("test".toCharArray());

        privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("ValidCertificate", passwd);
    }

    private static KeyStore loadKeyStore(File file, String password)
    throws Exception
    {
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());

        return keyStore;
    }
    /*
     * verify the message using openssl
     */
    private static void verifyMessage(File messageFile, X509Certificate rootCertificate, File outputFile)
    throws Exception
    {
        OpenSSL openssl = new OpenSSL();

        openssl.setCertificateAuthorities(rootCertificate);

        FileOutputStream output = new FileOutputStream(outputFile);
        ByteArrayOutputStream error = new ByteArrayOutputStream();

        try {
            openssl.cmsVerify(messageFile, output, error);
        }
        finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(error);
        }

        String errorText = error.toString(StandardCharsets.US_ASCII);

        Assertions.assertTrue(errorText.startsWith("CMS Verification successful"), "Error message: " + errorText);
    }

    private static String as1Dump(File messageFile)
    throws Exception
    {
        OpenSSL openssl = new OpenSSL();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ByteArrayOutputStream error = new ByteArrayOutputStream();

        try {
            openssl.cmsASN1Dump(messageFile, output, error);
        }
        finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(error);
        }

        String asn1 = output.toString(StandardCharsets.UTF_8);

        // Print before normalization
        System.out.println(asn1);

        // Normalize the asn1 dump to make it easier to match
        WordIterator wi = new WordIterator(new StringReader(asn1));

        StringWriter sw = new StringWriter();

        String word;

        while ((word = wi.nextWord()) != null)
        {
            word = StringUtils.trimToNull(word);

            if (word != null) {
                sw.append(word.toLowerCase()).append(' ');
            }
        }

        return sw.toString();
    }

    /*
     * decrypt the message using openssl
     */
    private static void decryptMessage(File messageFile, PrivateKey privateKey, File outputFile)
    throws Exception
    {
        OpenSSL openssl = new OpenSSL();

        openssl.setPrivateKey(privateKey);

        FileOutputStream output = new FileOutputStream(outputFile);
        ByteArrayOutputStream error = new ByteArrayOutputStream();

        try {
            openssl.cmsDecrypt(messageFile, output, error);
        }
        finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(error);
        }

        output.close();
    }

    /*
     * Check for some headers which should exist because they were added to the signed or encrypted blob.
     */
    private static void checkForEmbeddedHeaders(MimeMessage message)
    throws Exception
    {
        // the message should contain the signed from, to and subject
        Assertions.assertEquals("<test@example.com>", message.getHeader("from", ","));
        Assertions.assertEquals("<test@example.com>", message.getHeader("to", ","));
        Assertions.assertEquals("normal message with attachment", message.getHeader("subject", ","));
    }

    /*
     * Check for some headers which should exist because they also exist on the source message.
     */
    private static void checkForSourceHeaders(MimeMessage message)
    throws Exception
    {
        // the message should contain the signed from, to and subject
        Assertions.assertEquals("<test@example.com>", message.getHeader("from", ","));
        Assertions.assertEquals("<test@example.com>", message.getHeader("to", ","));
        Assertions.assertEquals("normal message with attachment", message.getHeader("subject", ","));
        Assertions.assertEquals("1.0", message.getHeader("MIME-Version", ","));
        Assertions.assertEquals("3", message.getHeader("X-Priority", ","));
        Assertions.assertEquals("Normal", message.getHeader("X-MSMail-Priority", ","));
        Assertions.assertEquals("Produced By Microsoft MimeOLE V6.00.2800.1896", message.getHeader("X-MimeOLE", ","));
        Assertions.assertEquals("test 1,test 2", message.getHeader("X-Test", ","));
        Assertions.assertEquals("test 3", message.getHeader("X-Test-a", ","));
        Assertions.assertEquals("test 4", message.getHeader("X-Test-b", ","));
    }

    @Test
    void testClearSignExtraCRLFPreamble()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/extra-cr-lf-preamble.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE, SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals("Martijn Brinkers <martijn@djigzo.com>", newMessage.getHeader("from", ","));
        Assertions.assertEquals("Martijn Brinkers <martijn@djigzo.com>", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test multiple attachments extra CR/LF preamble", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));
    }

    @Test
    void testEncryptReceivedHeadersOrder()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/text-message-with-received-headers.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        String[] returnPaths = newMessage.getHeader("Return-Path");

        Assertions.assertEquals(2, returnPaths.length);
        Assertions.assertEquals("<return1@example.com>", returnPaths[0]);
        Assertions.assertEquals("<return2@example.com>", returnPaths[1]);

        String[] received = newMessage.getHeader("Received");
        Assertions.assertEquals(7, received.length);
        Assertions.assertEquals("from secure.example.com (unknown [192.168.0.6])\r\n\tby example.com (Postfix) " +
                                "with ESMTP id 0183D43843\r\n\tfor <martijn@example.com>; Sat, 22 Aug 2009 18:30:27 +0200 (CEST)", received[0]);
        Assertions.assertEquals("from test (desktop.box [192.168.178.20])\r\n\tby host.example.com (Postfix) with " +
                                "SMTP id 9883623F5\r\n\tfor <martijn@example.com>; Sat, 22 Aug 2009 12:30:24 -0400 (EDT)", received[6]);
    }

    /*
     * Create an S/MIME compressed message.
     * This is not supported by Openssl, evolution and Outlook.
     */
    @Test
    void testCompressDeprecatedHeaders()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.setUseDeprecatedContentTypes(true);

        builder.compress();

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.DEPRECATED_COMPRESSED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.COMPRESSED, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));
    }

    @Test
    void testEncryptDeprecatedHeaders()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.setUseDeprecatedContentTypes(true);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.DEPRECATED_ENCRYPTED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());
    }

    @Test
    void testOpaqueSignDeprecatedHeaders()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.setUseDeprecatedContentTypes(true);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.OPAQUE);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.DEPRECATED_ENCAPSULATED_SIGNED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());
    }

    @Test
    void testClearSignDeprecatedHeaders()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.setUseDeprecatedContentTypes(true);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        SMIMEUtils.dissectSigned((Multipart) newMessage.getContent());

        Assertions.assertEquals(SMIMEHeader.DEPRECATED_DETACHED_SIGNATURE_TYPE, SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());
    }

    @Test
    void testClearSignSimpleTextMessageLF()
    throws Exception
    {
        File mail = new File(TEST_BASE, "mail/simple-text-message-LF.eml");

        // check to make sure the file does not contain CR
        String body = IOUtils.toString(new FileInputStream(mail), StandardCharsets.UTF_8);

        Assertions.assertFalse(body.contains("\r"));

        MimeMessage message = MailUtils.loadMessage(mail);

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        // Check if the new style RFC 5751 micalg name is used as the default style
        Assertions.assertTrue(newMessage.getContentType().contains("sha-1"));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());
    }

    @Test
    void testClearSignRFC3851MicAlgs()
    throws Exception
    {
        File mail = new File(TEST_BASE, "mail/simple-text-message-LF.eml");

        // check to make sure the file does not contain CR
        String body = IOUtils.toString(new FileInputStream(mail), StandardCharsets.UTF_8);

        Assertions.assertFalse(body.contains("\r"));

        MimeMessage message = MailUtils.loadMessage(mail);

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.setMicAlgStyle(MicAlgStyle.RFC3851);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        // Check if the new style RFC 5751 micalg name is used
        Assertions.assertTrue(newMessage.getContentType().contains("sha1"));
    }

    @Test
    void testClearSignRFC5751MicAlgs()
    throws Exception
    {
        File mail = new File(TEST_BASE, "mail/simple-text-message-LF.eml");

        // check to make sure the file does not contain CR
        String body = IOUtils.toString(new FileInputStream(mail), StandardCharsets.UTF_8);

        Assertions.assertFalse(body.contains("\r"));

        MimeMessage message = MailUtils.loadMessage(mail);

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.setMicAlgStyle(MicAlgStyle.RFC5751);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        // Check if the new style RFC 5751 micalg name is used
        Assertions.assertTrue(newMessage.getContentType().contains("sha-1"));
    }

    @Test
    void testClearSignKeyUsageNotForSigning()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("KeyUsageNotForSigning", passwd);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));
    }

    @Test
    void testClearSignMissingSMIMEExtKeyUsage()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("NoSMIMEExtKeyUsage", passwd);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));
    }

    @Test
    void testClearSignUnknownCharset()
    throws Exception
    {
        // unknown-charset.eml contains a non-valid charset. We need to test if signing still works if
        // we use CharsetAliasProvider which will provide support for this character set
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-charset.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA, (X509Certificate) privateKeyEntry.getCertificate());

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        MailUtils.validateMessage(newMessage);

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);
    }

    @Test
    void testClearSignUnknownCharsetFallback()
    throws Exception
    {
        // unknown-charset.eml contains a non-valid charset. We need to test if signing still works if
        // we use CharsetAliasProvider which will provide support for this character set
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-charset-xxx.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA, (X509Certificate) privateKeyEntry.getCertificate());

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        MailUtils.validateMessage(newMessage);

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);
    }

    @Test
    void testClearSignUnknownContentType()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-type.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA, (X509Certificate) privateKeyEntry.getCertificate());

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        MailUtils.validateMessage(newMessage);

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("application/xxx"));
    }

    @Test
    void testClearSignUnknownContentTypeMultipart()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-type-multipart.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA, (X509Certificate) privateKeyEntry.getCertificate());

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        MailUtils.validateMessage(newMessage);

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        MailUtils.loadMessage(opensslOutputFile);
    }

    @Test
    void testClearSignNoCertificates()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA, (X509Certificate) privateKeyEntry.getCertificate());

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        String asn1 = as1Dump(file);

        // The result might be different for different openssl versions. We therefore use a regex
        Assertions.assertTrue(Pattern.compile(
            "certificates: <(absent|empty)> crls: <(absent|empty)>")
            .matcher(asn1).find());
    }

    @Test
    void testClearSignAddEncryptionKeyPreference()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA, (X509Certificate) privateKeyEntry.getCertificate());

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());

        String asn1 = as1Dump(file);

        // 1.3.6.1.4.1.311.16.4 is a Microsoft specific OID for encryption key preference
        Assertions.assertTrue(asn1.contains(
                "object: undefined (1.3.6.1.4.1.311.16.4) set: sequence: 0:d=0 hl=2 l= 120 cons: "
                + "sequence 2:d=1 hl=2 l= 100 cons: sequence 4:d=2 hl=2 l= 11 cons: "
                + "set 6:d=3 hl=2 l= 9 cons: sequence 8:d=4 hl=2 l= 3 prim: "
                + "object :countryname 13:d=4 hl=2 l= 2 prim: printablestring :nl "
                + "17:d=2 hl=2 l= 11 cons: set 19:d=3 hl=2 l= 9 cons: sequence 21:d=4 hl=2 l= 3 prim: "
                + "object :stateorprovincename 26:d=4 hl=2 l= 2 prim: utf8string :nh 30:d=2 hl=2 l= 18 "
                + "cons: set 32:d=3 hl=2 l= 16 cons: sequence 34:d=4 hl=2 l= 3 prim: "
                + "object :localityname 39:d=4 hl=2 l= 9 prim: utf8string :amsterdam 50:d=2 hl=2 l= 21 cons: "
                + "set 52:d=3 hl=2 l= 19 cons: sequence 54:d=4 hl=2 l= 3 prim: "
                + "object :commonname 59:d=4 hl=2 l= 12 prim: utf8string :mitm test ca "
                + "73:d=2 hl=2 l= 29 cons: "
                + "set 75:d=3 hl=2 l= 27 cons: sequence 77:d=4 hl=2 l= 9 prim: "
                + "object :emailaddress 88:d=4 hl=2 l= 14 prim: ia5string :ca@example.com 104:d=1 hl=2 l= 16 prim: "
                + "integer :0115fcd741088707366e9727452c9770"));

        Assertions.assertTrue(asn1.contains(
                "object: id-smime-aa-encrypkeypref (1.2.840.113549.1.9.16.2.11) set: (unknown): "
                + "0:d=0 hl=2 l= 120 cons: cont [ 0 ] 2:d=1 hl=2 l= 100 cons: sequence 4:d=2 hl=2 l= 11 "
                + "cons: set 6:d=3 hl=2 l= 9 cons: sequence 8:d=4 hl=2 l= 3 prim: "
                + "object :countryname 13:d=4 hl=2 l= 2 prim: printablestring :nl "
                + "17:d=2 hl=2 l= 11 cons: set 19:d=3 hl=2 l= 9 cons: sequence 21:d=4 hl=2 l= 3 prim: "
                + "object :stateorprovincename 26:d=4 hl=2 l= 2 prim: utf8string :nh 30:d=2 hl=2 l= 18 "
                + "cons: set 32:d=3 hl=2 l= 16 cons: sequence 34:d=4 hl=2 l= 3 prim: "
                + "object :localityname 39:d=4 hl=2 l= 9 prim: utf8string :amsterdam 50:d=2 hl=2 l= 21 "
                + "cons: set 52:d=3 hl=2 l= 19 cons: sequence 54:d=4 hl=2 l= 3 prim: "
                + "object :commonname 59:d=4 hl=2 l= 12 prim: utf8string :mitm test ca 73:d=2 hl=2 l= 29 "
                + "cons: set 75:d=3 hl=2 l= 27 cons: sequence 77:d=4 hl=2 l= 9 prim: "
                + "object :emailaddress 88:d=4 hl=2 l= 14 prim: ia5string :ca@example.com 104:d=1 hl=2 l= 16 "
                + "prim: integer :0115fcd741088707366e9727452c9770"));
    }

    @Test
    void testClearSignSimpleTextMessage()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE, SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());
    }

    @Test
    void testOpaqueSignSimpleTextMessage()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.OPAQUE);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.ENCAPSULATED_SIGNED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());
    }

    /*
     * Check whether quoted printable soft breaks inside an already signed message are
     * not changed by Javamail.
     *
     * @throws Exception
     */
    @Test
    void testEncryptSignedQuotedPrintableSoftBreaks()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/qp-soft-breaks-signed.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertTrue(newMessage.isMimeType("multipart/signed"));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(opensslOutputFileSigned, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));
    }

    /*
     * Check whether quoted printable soft breaks inside an already signed message are
     * not changed by Javamail. Now using SMIMEEnvelopedGenerator directly and not
     * using our own makeContentBodyPart.
     */
    @Test
    void testEncryptSignedQuotedPrintableSoftBreaksDirectBC()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/qp-soft-breaks-signed.eml");

        SMIMEEnvelopedGenerator envelopedGenerator = new SMIMEEnvelopedGenerator();

        JceKeyTransRecipientInfoGenerator infoGenerator = new JceKeyTransRecipientInfoGenerator(encryptionCertificate);

        envelopedGenerator.addRecipientInfoGenerator(infoGenerator);

        JceCMSContentEncryptorBuilder encryptorBuilder = new JceCMSContentEncryptorBuilder(
                new ASN1ObjectIdentifier("1.2.840.113549.3.7"), 168).setProvider(
                        SecurityFactoryBouncyCastle.PROVIDER_NAME);

        MimeBodyPart bodyPart = envelopedGenerator.generate(message, encryptorBuilder.build());

        MimeMessage newMessage = new MimeMessage(MailSession.getDefaultSession());

        newMessage.setContent(bodyPart.getContent(), bodyPart.getContentType());

        newMessage.saveChanges();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertTrue(newMessage.isMimeType("multipart/signed"));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(opensslOutputFileSigned, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));
    }

    /*
     * Clear sign a multipart mixed message
     */
    @Test
    void testClearSign()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        // the message should contain the signed from, to and subject
        checkForEmbeddedHeaders(newMessage);
    }

    /*
     * Clear sign a multipart mixed message
     */
    @Test
    void testClearSignNoFromProtectedHeader()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "subject", "to");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        // the message should contain the subject
        Assertions.assertEquals("normal message with attachment", newMessage.getHeader("subject", ","));
        Assertions.assertEquals("<test@example.com>", message.getHeader("to", ","));
        Assertions.assertNull(newMessage.getHeader("from"));
    }

    /*
     * Clear sign a multipart mixed message
     */
    @Test
    void testClearSignNoProtectedHeader()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertNull(newMessage.getHeader("subject", ","));
        Assertions.assertEquals("<test@example.com>", message.getHeader("to", ","));
        Assertions.assertNull(newMessage.getHeader("from"));
    }

    /*
     * Clear sign a multipart mixed message
     */
    @Test
    void testClearSignDefaultProtectedHeader()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertEquals("normal message with attachment", newMessage.getHeader("subject", ","));
        Assertions.assertEquals("<test@example.com>", message.getHeader("to", ","));
        Assertions.assertNull(newMessage.getHeader("from"));
    }

    /*
     * Clear sign a multipart mixed message using multiple signers.
     */
    @Test
    void testClearSignMultipleSigners()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        PrivateKeyEntry md5Entry = (PrivateKeyEntry) keyStore.getEntry("md5Hash", passwd);

        builder.addSigner(md5Entry.getPrivateKey(), (X509Certificate) md5Entry.getCertificate(),
                SMIMESigningAlgorithm.SHA256_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(md5Entry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        // the message should contain the signed from, to and subject
        checkForEmbeddedHeaders(newMessage);

        String asn1 = as1Dump(file);

        // Check if two signature layers
        Assertions.assertTrue(asn1.contains(
            "contenttype: pkcs7-signeddata (1.2.840.113549.1.7.2) " +
            "d.signeddata: " +
            "version: 1 " +
            "digestalgorithms: " +
            "algorithm: sha1 (1.3.14.3.2.26) " +
            "parameter: null " +
            "algorithm: sha256 (2.16.840.1.101.3.4.2.1) " +
            "parameter: <absent> " +
            "encapcontentinfo: " +
            "econtenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "econtent: <absent>"
        ));

        // Check if certs are attached
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=valid certificate/emailaddress=test@example.com"));
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=md5 hash/emailaddress=test@example.com"));
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com"));
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=mitm test root/emailaddress=root@example.com"));

        // check signers
        Assertions.assertTrue(asn1.contains(
                "signerinfos: " +
                "version: 1 " +
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443394451216042738351345741452842864 " +
                "digestalgorithm: " +
                "algorithm: sha1 (1.3.14.3.2.26) " +
                "parameter: null " +
                "signedattrs: " +
                "object: contenttype (1.2.840.113549.1.9.3) " +
                "set: " +
                "object:pkcs7-data (1.2.840.113549.1.7.1) " +
                "object: signingtime (1.2.840.113549.1.9.5)"
        ));

        Assertions.assertTrue(asn1.contains(
                "version: 1 " +
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443399230168074213216606584579338850 " +
                "digestalgorithm: " +
                "algorithm: sha256 (2.16.840.1.101.3.4.2.1) " +
                "parameter: <absent> " +
                "signedattrs: " +
                "object: contenttype (1.2.840.113549.1.9.3) " +
                "set: " +
                "object:pkcs7-data (1.2.840.113549.1.7.1) " +
                "object: signingtime (1.2.840.113549.1.9.5)"
        ));
    }

    /*
     * Opaque sign a multipart/mixed message.
     */
    @Test
    void testOpaqueSign()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.OPAQUE);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        // the message should contain the signed from, to and subject
        checkForEmbeddedHeaders(newMessage);

        String asn1 = as1Dump(file);

        Assertions.assertTrue(asn1.contains(
            "cms_contentinfo: " +
            "contenttype: pkcs7-signeddata (1.2.840.113549.1.7.2) " +
            "d.signeddata: " +
            "version: 1 " +
            "digestalgorithms: " +
            "algorithm: sha1 (1.3.14.3.2.26) " +
            "parameter: null " +
            "encapcontentinfo: " +
            "econtenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "econtent: " +
            "0000 - 46 72 6f 6d 3a 20 3c 74-65 73 74 40 65 78 61 from: <test@exa " +
            "000f - 6d 70 6c 65 2e 63 6f 6d-3e 0d 0a 54 6f 3a 20 mple.com>..to: "
        ));

        // Check S/MIME Capabilities
        Assertions.assertTrue(asn1.contains("object: s/mime capabilities (1.2.840.113549.1.9.15)"));
        Assertions.assertTrue(asn1.contains("aes-256-cbc"));
        Assertions.assertTrue(asn1.contains("aes-192-cbc"));
        Assertions.assertTrue(asn1.contains("aes-128-cbc"));
    }

    /*
     * Opaque sign a multipart/mixed message signed by multiple signers.
     */
    @Test
    void testOpaqueSignMultipleSigners()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        PrivateKeyEntry md5Entry = (PrivateKeyEntry) keyStore.getEntry("md5Hash", passwd);

        builder.addSigner(md5Entry.getPrivateKey(), (X509Certificate) md5Entry.getCertificate(),
                SMIMESigningAlgorithm.SHA256_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(md5Entry.getCertificateChain()));

        builder.sign(SMIMESignMode.OPAQUE);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        // the message should contain the signed from, to and subject
        checkForEmbeddedHeaders(newMessage);
    }

    /*
     * Create an S/MIME compressed message.
     * This is not supported by Openssl, evolution and Outlook.
     */
    @Test
    void testCompressed()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.compress();

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.COMPRESSED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.COMPRESSED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);
    }

    /*
     * Create an S/MIME compressed message.
     * This is not supported by Openssl, evolution and Outlook.
     */
    @Test
    void testSimpleMessageCompress()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.compress();

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.COMPRESSED, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));
    }

    /*
     * 3-DES Encrypt a multipart/mixed message using the issuerAndSerialNumber RecipientIdentifier.
     */
    @Test
    void testEncryptIssuerSerialRecipientId()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForEmbeddedHeaders(newMessage);
    }

    @Test
    void testSimpleMessageEncrypt()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.ENCRYPTED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());

        String asn1 = as1Dump(file);

        Assertions.assertTrue(asn1.contains(
            "keyencryptionalgorithm: " +
            "algorithm: rsaencryption (1.2.840.113549.1.1.1) " +
            "parameter: null"
        ));

        Assertions.assertTrue(asn1.contains(
            "contentencryptionalgorithm: " +
            "algorithm: des-ede3-cbc (1.2.840.113549.3.7)"
        ));
    }

    /*
     * Encrypt a multipart/mixed message with multiple recipients
     */
    @Test
    void testEncryptMultipleRecipients()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        Enumeration<String> aliases = keyStore.aliases();

        int i = 15;

        while (aliases.hasMoreElements() && i != 0)
        {
            String alias = aliases.nextElement();

            if (keyStore.isKeyEntry(alias))
            {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate(alias);

                builder.addRecipient(certificate, SMIMERecipientMode.ISSUER_SERIAL);

                i--;
            }
        }

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForEmbeddedHeaders(newMessage);

        String asn1 = as1Dump(file);

        Assertions.assertTrue(Pattern.compile(
                "cms_contentinfo: " +
                "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
                "d.envelopeddata: " +
                "version: (0|<absent>) " +
                "originatorinfo: <absent> " +
                "recipientinfos:").matcher(asn1).find());

        // Check recipients

        // Recipient 1
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443394451216042738351345741452842864"
        ));
        // Recipient 2
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443395034508224125736543770741684556"
        ));
        // Recipient 3
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443396316723948473749594365746322010"
        ));
        // Recipient 4
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443397697981126236216722249816655967"
        ));
        // Recipient 5
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443397945485235849630294529957493240"
        ));
        // Recipient 6
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443398179434643154869438233948942294"
        ));
        // Recipient 7
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443398378861737051419504946032199126"
        ));
        // Recipient 8
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443398616593380104951512331406229304"
        ));
        // Recipient 9
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443398818012100993100937817588033904 "
        ));
        // Recipient 10
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443399029575811980020654288962656610"
        ));
        // Recipient 11
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443399230168074213216606584579338850"
        ));
        // Recipient 12
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443399422621163784095952903333093375"
        ));
        //  Recipient 13
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1451494164269949491095181262429383846"
        ));
        // Recipient 14
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1451494804175535494132001768233192055"
        ));
        // Recipient 15
        Assertions.assertTrue(asn1.contains(
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443399424402921266693579916096623720"
        ));
    }

    /*
     * AES Encrypt a multipart/mixed message using the issuerAndSerialNumber RecipientIdentifier.
     *
     * Outlook does not support AES encrypted email.
     */
    @Test
    void testEncryptIssuerSerialRecipientIdAES()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES128_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForEmbeddedHeaders(newMessage);
    }

    /*
     * Creates a message with subject key identifier as the public key identifier.
     *
     * This is not supported by Openssl and evolution (only issuer/serial number recipient id).
     * It is supported by Outlook.
     */
    @Test
    void testEncryptSubjectKeyIdRecipientId()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.SUBJECT_KEY_ID_IF_AVAILABLE);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);
    }

    /*
     * Creates a message with subject key identifier and issuer/serial number as the public key identifier.
     *
     * This is not supported by Openssl and evolution (only issuer/serial number recipient id).
     * It is supported by Outlook.
     */
    @Test
    void testEncryptBothRecipientId()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.BOTH);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);
    }

    /*
     * First sign a multipart/mixed message, then encrypt it.
     */
    @Test
    void testSignEncrypt()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(opensslOutputFileSigned, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        // the message should contain the signed from, to and subject
        checkForEmbeddedHeaders(newMessage);
    }

    /*
     * First sign a multipart/mixed message, then encrypt it and again sign it.
     */
    @Test
    void testSignEncryptSign()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFileEncrypted = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFileEncrypted);

        newMessage = MailUtils.loadMessage(opensslOutputFileEncrypted);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(opensslOutputFileEncrypted, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(opensslOutputFileSigned, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        // the message should contain the signed from, to and subject
        checkForEmbeddedHeaders(newMessage);
    }

    /*
     * First encrypt a message and then sign it.
     */
    @Test
    void testEncryptSign()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFileEncrypted = TestUtils.createTempFile(".eml");

        verifyMessage(file, rootCertificate, opensslOutputFileEncrypted);

        newMessage = MailUtils.loadMessage(opensslOutputFileEncrypted);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        decryptMessage(opensslOutputFileEncrypted, privateKeyEntry.getPrivateKey(), opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        // the message should contain the signed from, to and subject
        checkForEmbeddedHeaders(newMessage);
    }

    /*
     * Encrypt message, then sign and again encrypt.
     */
    @Test
    void testEncryptSignEncrypt()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFileSigned = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFileSigned);

        newMessage = MailUtils.loadMessage(opensslOutputFileSigned);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFileEncrypted = TestUtils.createTempFile(".eml");

        verifyMessage(opensslOutputFileSigned, rootCertificate, opensslOutputFileEncrypted);

        newMessage = MailUtils.loadMessage(opensslOutputFileEncrypted);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        decryptMessage(opensslOutputFileEncrypted, privateKeyEntry.getPrivateKey(), opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        // the message should contain the signed from, to and subject
        checkForEmbeddedHeaders(newMessage);
    }

    @Test
    void testEncryptSignEncryptRepeat()
    throws Exception
    {
        for (int i = 0; i < 200; i++) {
            testEncryptSignEncrypt();
        }
    }

    /*
     * Opaque sign a multipart/mixed message.
     */
    @Test
    void testOpaqueSignNullCertificates()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates((X509Certificate)null);

        builder.sign(SMIMESignMode.OPAQUE);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);
    }

    /*
     * Opaque sign a multipart/mixed message.
     */
    @Test
    void testOpaqueSignEmptyCertificateArray()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(new X509Certificate[]{});
        builder.addCertificates(new LinkedList<X509Certificate>());

        builder.sign(SMIMESignMode.OPAQUE);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);
    }

    @Test
    void testBuildMultiLayer()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "subject");

        // ValidCertificate is for test@example.com
        // multipleEmail is for test@example.com, test2@example.com and test3@example.com
        // NoEmail contains no email
        builder.addRecipient((X509Certificate) keyStore.getCertificate("NoEmail"),
                SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES128_CBC);

        builder.addRecipient((X509Certificate) keyStore.getCertificate("ValidCertificate"),
                SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES256_CBC);

        builder.addRecipient((X509Certificate) keyStore.getCertificate("multipleEmail"),
                SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES256_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));
    }

    /*
     * Test for bug APPLIANCE-2
     */
    @Test
    void testEncryptBase64EncodeBug()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setSubject("test");
        message.setContent("test", "text/plain");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        X509Certificate certificate = TestUtils.loadCertificate(
                "src/test/resources/testdata/certificates/certificate-base64-encode-bug.cer");

        builder.addRecipient(certificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.DES_EDE3_CBC);

        MimeMessage newMessage = builder.buildMessage();

        newMessage.saveChanges();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        newMessage.writeTo(new SkipHeadersOutputStream(bos));

        String blob = bos.toString(StandardCharsets.US_ASCII);

        // check if all lines are not longer than 76 characters
        LineIterator it = IOUtils.lineIterator(new StringReader(blob));

        while (it.hasNext())
        {
            String next = it.next();

            if (next.length() > 76) {
                Assertions.fail("Line length exceeds 76: " + next);
            }
        }
    }

    /*
     * Clear sign a multipart mixed message with subject key id instead of issuer/serial
     */
    @Test
    void testClearSignSubjectKeyIdentifier()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        builder.addSigner(privateKeyEntry.getPrivateKey(), X509CertificateInspector.getSubjectKeyIdentifier(
                (X509Certificate) privateKeyEntry.getCertificate()), SMIMESigningAlgorithm.SHA1_WITH_RSA);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File signedMimeFile = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(signedMimeFile);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(signedMimeFile);

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(signedMimeFile, rootCertificate, opensslOutputFile);

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(newMessage,
                SecurityFactoryBouncyCastle.PROVIDER_NAME, SecurityFactoryBouncyCastle.PROVIDER_NAME);

        List<SignerInfo> signers = inspector.getSigners();

        Assertions.assertEquals(1, signers.size());

        SignerIdentifier signerId = signers.get(0).getSignerId();

        Assertions.assertNull(signerId.getIssuer());
        Assertions.assertNull(signerId.getSerialNumber());
        Assertions.assertEquals("9C82DE52944798337B05036A187DED41632EFBFF", HexUtils.hexEncode(
                signerId.getSubjectKeyIdentifier()));

        String asn1 = as1Dump(signedMimeFile);

        Assertions.assertTrue(asn1.contains(
            "cms_contentinfo: " +
            "contenttype: pkcs7-signeddata (1.2.840.113549.1.7.2) " +
            "d.signeddata: " +
            "version: 3 " +
            "digestalgorithms: " +
            "algorithm: sha1 (1.3.14.3.2.26) " +
            "parameter: null " +
            "encapcontentinfo: " +
            "econtenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "econtent: <absent>"
        ));

        // Check if certs are attached
        Assertions.assertTrue(asn1.contains(
            "subject: c=nl, st=nh, l=amsterdam, cn=valid certificate/emailaddress=test@example.com"));
        Assertions.assertTrue(asn1.contains(
            "subject: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com"));
        Assertions.assertTrue(asn1.contains(
            "subject: c=nl, st=nh, l=amsterdam, cn=mitm test root/emailaddress=root@example.com"));

        // Check if subject key identifier is used
        Assertions.assertTrue(asn1.contains(
            "signerinfos: " +
            "version: 3 " +
            "d.subjectkeyidentifier:"
        ));

        // Check algorithm
        Assertions.assertTrue(asn1.contains(
            "signaturealgorithm: " +
            "algorithm: rsaencryption (1.2.840.113549.1.1.1) " +
            "parameter: null"
        ));
    }

    @Test
    void testClearSignSHA256WITHRSAANDMGF1()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("rsa2048", passwd);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA256_WITH_RSA_AND_MGF1);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File signedMimeFile = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(signedMimeFile);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(signedMimeFile);

        Assertions.assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE, SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(signedMimeFile, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());

        String asn1 = as1Dump(signedMimeFile);

        // Check if sha256
        Assertions.assertTrue(asn1.contains(
            "cms_contentinfo: " +
            "contenttype: pkcs7-signeddata (1.2.840.113549.1.7.2) " +
            "d.signeddata: " +
            "version: 1 " +
            "digestalgorithms: " +
            "algorithm: sha256 (2.16.840.1.101.3.4.2.1) " +
            "parameter: <absent> " +
            "encapcontentinfo: " +
            "econtenttype: pkcs7-data (1.2.840.113549.1.7.1)"
        ), asn1);

        // Check if certs are attached
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=rsa 2048/emailaddress=test@example.com"));
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com"));
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=mitm test root/emailaddress=root@example.com"));

        // Check if RSASSA-PSS is used
        Assertions.assertTrue(asn1.contains(
            "signaturealgorithm: " +
            "algorithm: rsassapss (1.2.840.113549.1.1.10) " +
            "parameter: sequence: " +
            "0:d=0 hl=2 l= 52 cons: sequence " +
            "2:d=1 hl=2 l= 15 cons: cont [ 0 ] " +
            "4:d=2 hl=2 l= 13 cons: sequence " +
            "6:d=3 hl=2 l= 9 prim: object :sha256 " +
            "17:d=3 hl=2 l= 0 prim: null " +
            "19:d=1 hl=2 l= 28 cons: cont [ 1 ] " +
            "21:d=2 hl=2 l= 26 cons: sequence " +
            "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
            "34:d=3 hl=2 l= 13 cons: sequence " +
            "36:d=4 hl=2 l= 9 prim: object :sha256 " +
            "47:d=4 hl=2 l= 0 prim: null " +
            "49:d=1 hl=2 l= 3 cons: cont [ 2 ] " +
            "51:d=2 hl=2 l= 1 prim: integer :20"
        ));
    }

    @Test
    void testClearSignSHA384WITHRSAANDMGF1()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("rsa2048", passwd);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA384_WITH_RSA_AND_MGF1);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File signedMimeFile = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(signedMimeFile);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(signedMimeFile);

        Assertions.assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE, SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(signedMimeFile, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());

        String asn1 = as1Dump(signedMimeFile);

        // Check if sha384
        Assertions.assertTrue(asn1.contains(
            "cms_contentinfo: " +
            "contenttype: pkcs7-signeddata (1.2.840.113549.1.7.2) " +
            "d.signeddata: " +
            "version: 1 " +
            "digestalgorithms: " +
            "algorithm: sha384 (2.16.840.1.101.3.4.2.2) " +
            "parameter: <absent> " +
            "encapcontentinfo: " +
            "econtenttype: pkcs7-data (1.2.840.113549.1.7.1)"
        ), asn1);

        // Check if certs are attached
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=rsa 2048/emailaddress=test@example.com"));
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com"));
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=mitm test root/emailaddress=root@example.com"));

        // Check if RSASSA-PSS is used
        Assertions.assertTrue(asn1.contains(
            "signaturealgorithm: " +
            "algorithm: rsassapss (1.2.840.113549.1.1.10) " +
            "parameter: sequence: " +
            "0:d=0 hl=2 l= 52 cons: sequence " +
            "2:d=1 hl=2 l= 15 cons: cont [ 0 ] " +
            "4:d=2 hl=2 l= 13 cons: sequence " +
            "6:d=3 hl=2 l= 9 prim: object :sha384 " +
            "17:d=3 hl=2 l= 0 prim: null " +
            "19:d=1 hl=2 l= 28 cons: cont [ 1 ] " +
            "21:d=2 hl=2 l= 26 cons: sequence " +
            "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
            "34:d=3 hl=2 l= 13 cons: sequence " +
            "36:d=4 hl=2 l= 9 prim: object :sha384 " +
            "47:d=4 hl=2 l= 0 prim: null " +
            "49:d=1 hl=2 l= 3 cons: cont [ 2 ] " +
            "51:d=2 hl=2 l= 1 prim: integer :30"
        ));
    }

    @Test
    void testClearSignSHA512WITHRSAANDMGF1()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message, "to", "subject", "from");

        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("rsa2048", passwd);

        builder.addSigner(privateKeyEntry.getPrivateKey(), (X509Certificate) privateKeyEntry.getCertificate(),
                SMIMESigningAlgorithm.SHA512_WITH_RSA_AND_MGF1);

        builder.addCertificates(CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain()));

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage newMessage = builder.buildMessage();

        File signedMimeFile = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(signedMimeFile);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(signedMimeFile);

        Assertions.assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE, SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        verifyMessage(signedMimeFile, rootCertificate, opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals("test@example.com", newMessage.getHeader("from", ","));
        Assertions.assertEquals("test@example.com", newMessage.getHeader("to", ","));
        Assertions.assertEquals("test simple message", newMessage.getHeader("subject", ","));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());

        String asn1 = as1Dump(signedMimeFile);

        // Check if sha512
        Assertions.assertTrue(asn1.contains(
            "cms_contentinfo: " +
            "contenttype: pkcs7-signeddata (1.2.840.113549.1.7.2) " +
            "d.signeddata: " +
            "version: 1 " +
            "digestalgorithms: " +
            "algorithm: sha512 (2.16.840.1.101.3.4.2.3) " +
            "parameter: <absent> " +
            "encapcontentinfo: " +
            "econtenttype: pkcs7-data (1.2.840.113549.1.7.1)"
        ), asn1);

        // Check if certs are attached
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=rsa 2048/emailaddress=test@example.com"));
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com"));
        Assertions.assertTrue(asn1.contains(
                "subject: c=nl, st=nh, l=amsterdam, cn=mitm test root/emailaddress=root@example.com"));

        // Check if RSASSA-PSS is used
        Assertions.assertTrue(asn1.contains(
            "signaturealgorithm: " +
            "algorithm: rsassapss (1.2.840.113549.1.1.10) " +
            "parameter: sequence: " +
            "0:d=0 hl=2 l= 52 cons: sequence " +
            "2:d=1 hl=2 l= 15 cons: cont [ 0 ] " +
            "4:d=2 hl=2 l= 13 cons: sequence " +
            "6:d=3 hl=2 l= 9 prim: object :sha512 " +
            "17:d=3 hl=2 l= 0 prim: null " +
            "19:d=1 hl=2 l= 28 cons: cont [ 1 ] " +
            "21:d=2 hl=2 l= 26 cons: sequence " +
            "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
            "34:d=3 hl=2 l= 13 cons: sequence " +
            "36:d=4 hl=2 l= 9 prim: object :sha512 " +
            "47:d=4 hl=2 l= 0 prim: null " +
            "49:d=1 hl=2 l= 3 cons: cont [ 2 ] " +
            "51:d=2 hl=2 l= 1 prim: integer :40"
        ));
    }

    @Test
    void testEncryptRSAES_OAEP_SHA1()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        X509Certificate encryptionCertificate = (X509Certificate) keyStore.getCertificate("rsa2048");
        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("rsa2048", passwd);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL,
                SMIMEEncryptionScheme.RSAES_OAEP_SHA1);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES128_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File encryptedFile = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(encryptedFile);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(encryptedFile);

        Assertions.assertEquals(SMIMEHeader.ENCRYPTED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslEncrypted = TestUtils.createTempFile(".eml");

        decryptMessage(encryptedFile, privateKeyEntry.getPrivateKey(), opensslEncrypted);

        newMessage = MailUtils.loadMessage(opensslEncrypted);

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());

        String asn1 = as1Dump(encryptedFile);

        Assertions.assertTrue(Pattern.compile(
            "cms_contentinfo: " +
            "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
            "d.envelopeddata: " +
            "version: (0|<absent>) " +
            "originatorinfo: <absent> " +
            "recipientinfos: " +
            "d.ktri: " +
            "version: (0|<absent>) " +
            "d.issuerandserialnumber: " +
            "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
            "serialnumber: 1443399424402921266693579916096623720 " +
            "keyencryptionalgorithm: " +
            "algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
            "parameter: sequence: " +
            "0:d=0 hl=2 l= 0 cons: sequence").matcher(asn1).find());

        Assertions.assertTrue(asn1.contains(
            "encryptedcontentinfo: " +
            "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "contentencryptionalgorithm: " +
            "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }

    @Test
    void testEncryptRSAES_OAEP_SHA256()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        X509Certificate encryptionCertificate = (X509Certificate) keyStore.getCertificate("rsa2048");
        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("rsa2048", passwd);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL,
                SMIMEEncryptionScheme.RSAES_OAEP_SHA256);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES128_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File encryptedFile = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(encryptedFile);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(encryptedFile);

        Assertions.assertEquals(SMIMEHeader.ENCRYPTED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslEncrypted = TestUtils.createTempFile(".eml");

        decryptMessage(encryptedFile, privateKeyEntry.getPrivateKey(), opensslEncrypted);

        newMessage = MailUtils.loadMessage(opensslEncrypted);

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());

        String asn1 = as1Dump(encryptedFile);

        Assertions.assertTrue(Pattern.compile(
            "cms_contentinfo: " +
            "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
            "d.envelopeddata: " +
            "version: (0|<absent>) " +
            "originatorinfo: <absent> " +
            "recipientinfos: " +
            "d.ktri: " +
            "version: (0|<absent>) " +
            "d.issuerandserialnumber: " +
            "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
            "serialnumber: 1443399424402921266693579916096623720 " +
            "keyencryptionalgorithm: " +
            "algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
            "parameter: sequence: " +
            "0:d=0 hl=2 l= 47 cons: sequence " +
            "2:d=1 hl=2 l= 15 cons: cont \\[ 0 \\] " +
            "4:d=2 hl=2 l= 13 cons: sequence " +
            "6:d=3 hl=2 l= 9 prim: object :sha256 " +
            "17:d=3 hl=2 l= 0 prim: null " +
            "19:d=1 hl=2 l= 28 cons: cont \\[ 1 \\] " +
            "21:d=2 hl=2 l= 26 cons: sequence " +
            "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
            "34:d=3 hl=2 l= 13 cons: sequence " +
            "36:d=4 hl=2 l= 9 prim: object :sha256 " +
            "47:d=4 hl=2 l= 0 prim: null ").matcher(asn1).find());

        Assertions.assertTrue(asn1.contains(
            "encryptedcontentinfo: " +
            "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "contentencryptionalgorithm: " +
            "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }

    @Test
    void testEncryptRSAES_OAEP_SHA384()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        X509Certificate encryptionCertificate = (X509Certificate) keyStore.getCertificate("rsa2048");
        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("rsa2048", passwd);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL,
                SMIMEEncryptionScheme.RSAES_OAEP_SHA384);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES128_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File encryptedFile = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(encryptedFile);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(encryptedFile);

        Assertions.assertEquals(SMIMEHeader.ENCRYPTED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslEncrypted = TestUtils.createTempFile(".eml");

        decryptMessage(encryptedFile, privateKeyEntry.getPrivateKey(), opensslEncrypted);

        newMessage = MailUtils.loadMessage(opensslEncrypted);

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());

        String asn1 = as1Dump(encryptedFile);

        Assertions.assertTrue(Pattern.compile(
                "cms_contentinfo: " +
                "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
                "d.envelopeddata: " +
                "version: (0|<absent>) " +
                "originatorinfo: <absent> " +
                "recipientinfos: " +
                "d.ktri: " +
                "version: (0|<absent>) " +
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443399424402921266693579916096623720 " +
                "keyencryptionalgorithm: " +
                "algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
                "parameter: sequence: " +
                "0:d=0 hl=2 l= 47 cons: sequence " +
                "2:d=1 hl=2 l= 15 cons: cont \\[ 0 \\] " +
                "4:d=2 hl=2 l= 13 cons: sequence " +
                "6:d=3 hl=2 l= 9 prim: object :sha384 " +
                "17:d=3 hl=2 l= 0 prim: null " +
                "19:d=1 hl=2 l= 28 cons: cont \\[ 1 \\] " +
                "21:d=2 hl=2 l= 26 cons: sequence " +
                "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
                "34:d=3 hl=2 l= 13 cons: sequence " +
                "36:d=4 hl=2 l= 9 prim: object :sha384 " +
                "47:d=4 hl=2 l= 0 prim: null ").matcher(asn1).find());

        Assertions.assertTrue(asn1.contains(
            "encryptedcontentinfo: " +
            "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "contentencryptionalgorithm: " +
            "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }

    @Test
    void testEncryptRSAES_OAEP_SHA512()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        SMIMEBuilder builder = new SMIMEBuilderImpl(message);

        X509Certificate encryptionCertificate = (X509Certificate) keyStore.getCertificate("rsa2048");
        PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("rsa2048", passwd);

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL,
                SMIMEEncryptionScheme.RSAES_OAEP_SHA512);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES128_CBC);

        MimeMessage newMessage = builder.buildMessage();

        File encryptedFile = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(encryptedFile);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(encryptedFile);

        Assertions.assertEquals(SMIMEHeader.ENCRYPTED_CONTENT_TYPE, newMessage.getContentType());

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage));

        File opensslEncrypted = TestUtils.createTempFile(".eml");

        decryptMessage(encryptedFile, privateKeyEntry.getPrivateKey(), opensslEncrypted);

        newMessage = MailUtils.loadMessage(opensslEncrypted);

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        Assertions.assertTrue(newMessage.isMimeType("text/plain"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        String content = (String) newMessage.getContent();

        Assertions.assertEquals("test", content.trim());

        String asn1 = as1Dump(encryptedFile);

        Assertions.assertTrue(Pattern.compile(
                "cms_contentinfo: " +
                "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
                "d.envelopeddata: " +
                "version: (0|<absent>) " +
                "originatorinfo: <absent> " +
                "recipientinfos: " +
                "d.ktri: " +
                "version: (0|<absent>) " +
                "d.issuerandserialnumber: " +
                "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                "serialnumber: 1443399424402921266693579916096623720 " +
                "keyencryptionalgorithm: " +
                "algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
                "parameter: sequence: " +
                "0:d=0 hl=2 l= 47 cons: sequence " +
                "2:d=1 hl=2 l= 15 cons: cont \\[ 0 \\] " +
                "4:d=2 hl=2 l= 13 cons: sequence " +
                "6:d=3 hl=2 l= 9 prim: object :sha512 " +
                "17:d=3 hl=2 l= 0 prim: null " +
                "19:d=1 hl=2 l= 28 cons: cont \\[ 1 \\] " +
                "21:d=2 hl=2 l= 26 cons: sequence " +
                "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
                "34:d=3 hl=2 l= 13 cons: sequence " +
                "36:d=4 hl=2 l= 9 prim: object :sha512 " +
                "47:d=4 hl=2 l= 0 prim: null ").matcher(asn1).find());

        Assertions.assertTrue(asn1.contains(
            "encryptedcontentinfo: " +
            "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "contentencryptionalgorithm: " +
            "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }
}
