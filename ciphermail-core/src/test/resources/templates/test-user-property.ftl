Subject: The message has been encrypted
From: test@example.com
Content-Type: text/plain
Content-Transfer-Encoding: 7bit
Mime-Version: 1.0

${.vars["test.user.property.1"]!"property 1 not set"}
${.vars["test.user.property.2"]!"property 2 not set"}
${non_existing!"non existing property not set"}
