This is a revocation certificate for the OpenPGP key:

pub   rsa2048 2020-07-20 [S]
      5B52F849E4DE4470CD9051E3E7F615742F64D491
uid          CipherMail RSA 2048 test key <rsa-2048@example.com>

A revocation certificate is a kind of "kill switch" to publicly
declare that a key shall not anymore be used.  It is not possible
to retract such a revocation certificate once it has been published.

Use it to revoke this key in case of a compromise or loss of
the secret key.  However, if the secret key is still accessible,
it is better to generate a new revocation certificate and give
a reason for the revocation.  For details see the description of
of the gpg command "--generate-revocation" in the GnuPG manual.

To avoid an accidental use of this file, a colon has been inserted
before the 5 dashes below.  Remove this colon with a text editor
before importing and publishing this revocation certificate.

:-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: This is a revocation certificate

iQE2BCABCgAgFiEEW1L4SeTeRHDNkFHj5/YVdC9k1JEFAl8VvHYCHQAACgkQ5/YV
dC9k1JE7MQf/a2fFc2ddEiruoXNqhkAM3+DoJZczntfPytNasKCHW+oAEBpP/LPi
+TOAC0Gwe08YUC9ppqbz9DFECPq/7ek9geQ+9Fc7ULaCZ8w/bKLMONovf92HsHkA
C4Mg9YaElSOGZnpbayY6m+ABPwpYmTQUjbz2AN0ElsWspLJU0VJq/YEksnq81MCy
jLdzDvKSYwYf+7zXlO2gPScEsayMeqnYSjEbN8yAQ2q86l6sqF13+uPX821Pqza/
VzQm7864xFO8gBxcUtAfRjAFz/osRKfjmJmdr1zeO5PUq+bgHnhqhcHyo65Mqqbn
ssq0uX08RtNUc6cJk4K9RMB5DbfS8hjOpg==
=IZrY
-----END PGP PUBLIC KEY BLOCK-----
