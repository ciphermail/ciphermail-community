<#import "macros.ftl" as macros>
<@macros.init/>
<@macros.passwordReset/>
From: <${from!"postmaster"}>
<#if recipient??>
To: ${recipient}>
</#if>
Subject: Reset your CiperMail Portal password
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Auto-Submitted: auto-generated

A password reset was requested for your email address.

To reset your CipherMail Portal password, click the "reset password" link below and follow the instructions.

${macros.portalPasswordResetURL!"URL not set"}

If you did not request a password reset, please ignore this message.

If you receive multiple password reset requests in a short time frame, please contact support.

<#if macros.footer?has_content>
---
${macros.footer!} <#if macros.footerLink?has_content>(${macros.footerLink!})</#if>
</#if>