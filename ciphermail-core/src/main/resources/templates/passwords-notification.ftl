<#import "macros.ftl" as macros>
<@macros.init/>
From: <${from!"postmaster"}>
<#if to??>
To: <#list to as recipient><${recipient}><#if recipient?has_next>, </#if></#list>
</#if>
<#if replyTo??>
Reply-To: <${replyTo}>
</#if>
Subject: The message has been encrypted
In-Reply-To: ${(mail.message.messageID)!""}
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 8bit
Auto-Submitted: auto-replied

The message with Subject:

${subject!""}

has been sent encrypted to the following recipients:

<#if mail??>
<#-- we need to show the recipients of the source message -->
<#list mail.recipients as recipient>
${recipient}
</#list>

The passwords are:

<#assign passwords = mail.getAttribute("ciphermail-encrypted-passwords")>
<#assign emails = passwords?keys>
<#list emails as email>
<#assign pwc = passwords[email]>
recipient: ${email} password: ${pwc.password} <#if (pwc.passwordID)??>id: ${pwc.passwordID}</#if>
</#list>
</#if>

<#if macros.footer?has_content>
---
${macros.footer!} <#if macros.footerLink?has_content>(${macros.footerLink!})</#if>
</#if>