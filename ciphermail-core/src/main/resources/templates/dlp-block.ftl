<#import "macros.ftl" as macros>
<@macros.init/>
From: <${from!"postmaster"}>
<#if to??>
To: <#list to as recipient><${recipient}><#if recipient?has_next>, </#if></#list>
</#if>
<#if replyTo??>
Reply-To: <${replyTo}>
</#if>
Subject: *** DLP block warning ***
In-Reply-To: ${(mail.message.messageID)!""}
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 8bit
Auto-Submitted: auto-replied

The message with subject:

${subject!""}

Was blocked because it violated the following DLP policies:

<#if mail??>
<#assign violations = mailAttributes.getPolicyViolations(mail)>
<#list violations as violation>
${(violation!"")?truncate(900)}
</#list>
</#if>

<#if macros.footer?has_content>
---
${macros.footer!} <#if macros.footerLink?has_content>(${macros.footerLink!})</#if>
</#if>