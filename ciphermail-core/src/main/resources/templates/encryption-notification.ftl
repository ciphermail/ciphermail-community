<#import "macros.ftl" as macros>
<@macros.init/>
From: <${from!"postmaster"}>
<#if to??>
To: <#list to as recipient><${recipient}><#if recipient?has_next>, </#if></#list>
</#if>
<#if replyTo??>
Reply-To: <${replyTo}>
</#if>
Subject: The message has been encrypted
In-Reply-To: ${(mail.message.messageID)!""}
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 8bit
Auto-Submitted: auto-replied

<#if mail??>
<#compress>
<#assign notificationSubject = mail.getAttribute("runtime.encryptionNotification.subject")!>
<#assign notificationRecipients = mail.getAttribute("runtime.encryptionNotification.recipients")!>
<#assign encryptionMethod = mail.getAttribute("runtime.encryptionNotification.method")!>

<#if !notificationSubject?has_content>
  <#assign notificationSubject = subject!"">
</#if>

<#if !notificationRecipients?has_content>
  <#assign notificationRecipients = mail.recipients>
</#if>
</#compress>
</#if>
The message with Subject:

${notificationSubject!""}

has been sent encrypted <#if encryptionMethod?has_content>with ${encryptionMethod} </#if>to the following recipients:

<#if notificationRecipients??>
<#list notificationRecipients as recipient>
${recipient}
</#list>
</#if>

<#if macros.footer?has_content>
---
${macros.footer!} <#if macros.footerLink?has_content>(${macros.footerLink!})</#if>
</#if>