<#import "macros.ftl" as macros>
<@macros.init/>
<@macros.sms/>
Content-Type: multipart/mixed; boundary="${boundary!"boundary-not-set"}"

--${boundary!"boundary-not-set"}
Content-Type: multipart/related; boundary="${boundary!"boundary-not-set"}-1"

--${boundary!"boundary-not-set"}-1
Content-Type: multipart/alternative; boundary="${boundary!"boundary-not-set"}-2"

--${boundary!"boundary-not-set"}-2
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 8bit

You have received a secure message

This message contains a password encrypted PDF. Opening the PDF requires that you enter the password sent to you by SMS.

The password ID of this email is: ${macros.passwordID!"not set"}

<#if (from.personal)?has_content>
Kind regards,
${from.personal}
</#if>

<#if macros.footer?has_content>
---
${macros.footer!} <#if macros.footerLink?has_content>(${macros.footerLink!})</#if>
</#if>

--${boundary!"boundary-not-set"}-2
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: 8bit

<!--suppress XmlUnusedNamespaceDeclaration -->
<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <title>
    You have received a secure message
  </title> <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
    #outlook a { padding:0; }
    body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
    table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
    img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
    p { display:block;margin:13px 0; }
  </style> <!--[if mso]>
  <noscript>
    <xml>
      <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
  </noscript>
  <![endif]--> <!--[if lte mso 11]>
  <style type="text/css">
    .mj-outlook-group-fix { width:100% !important; }
  </style>
  <![endif]-->
  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-33-333333333333336 { width:33.333333333333336% !important; max-width: 33.333333333333336%; }
      .mj-column-per-100 { width:100% !important; max-width: 100%; }
    }
  </style>
  <style media="screen and (min-width:480px)">
    .moz-text-html .mj-column-per-33-333333333333336 { width:33.333333333333336% !important; max-width: 33.333333333333336%; }
    .moz-text-html .mj-column-per-100 { width:100% !important; max-width: 100%; }
  </style>
  <style type="text/css">



    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile { width: 100% !important; }
      td.mj-full-width-mobile { width: auto !important; }
    }

  </style>
  <style type="text/css">

  </style>
</head>
<body style="word-spacing:normal;">
<div style=""><!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
  <div style="margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
      <tbody>
      <tr>
        <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:200px;" ><![endif]-->
          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              </tbody>
            </table>
          </div> <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:200px;" ><![endif]-->
          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                    <tbody>
                    <tr>
                      <td style="width:150px;"><img alt="logo" height="auto" src="cid:logo.cid" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="150"></td>
                    </tr>
                    </tbody>
                  </table></td>
              </tr>
              </tbody>
            </table>
          </div> <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:200px;" ><![endif]-->
          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              </tbody>
            </table>
          </div> <!--[if mso | IE]></td></tr></table><![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div> <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
  <div style="margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
      <tbody>
      <tr>
        <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0;padding-top:0;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"><p style="border-top:solid 2px #E7EDF8;font-size:1px;margin:0px auto;width:100%;"></p> <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 2px #E7EDF8;font-size:1px;margin:0px auto;width:550px;" role="presentation" width="550px" ><tr><td style="height:0;line-height:0;"> &nbsp;
</td></tr></table><![endif]--></td>
              </tr>
              <tr>
                <td align="left" style="font-size:0px;padding:10px 25px;padding-top:40px;word-break:break-word;">
                  <div style="font-family:helvetica;font-size:16px;line-height:1;text-align:left;color:#556C8E;">
                    <h2 style="color: #00154A;font-weight: bold;line-height: 150%; font-size: 24px;">You have received a secure message</h2>
                    <p style="line-height: 160%;color: #556C8E;">This message contains a password encrypted PDF. Opening the PDF requires that you enter the password sent to you by SMS.</p>
                    <p style="line-height: 160%;color: #556C8E;">The password ID of this email is: <b>${macros.passwordID?html!"not set"}</b></p>
                  </div></td>
              </tr>
              </tbody>
            </table>
          </div> <!--[if mso | IE]></td></tr></table><![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div> <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
  <div style="margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
      <tbody>
      <tr>
        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody><!-- <#if (from.personal)?has_content> -->
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"><p style="border-top:solid 2px #E7EDF8;font-size:1px;margin:0px auto;width:100%;"></p> <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 2px #E7EDF8;font-size:1px;margin:0px auto;width:550px;" role="presentation" width="550px" ><tr><td style="height:0;line-height:0;"> &nbsp;
</td></tr></table><![endif]--></td>
              </tr>
              <tr>
                <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <div style="font-family:helvetica;font-size:16px;line-height:160%;text-align:left;color:#556C8E;">
                    <p>Kind regards,<br>${from.personal?html}</p>
                  </div></td>
              </tr> <!-- </#if> -->
              </tbody>
            </table>
          </div> <!--[if mso | IE]></td></tr></table><![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div> <!--[if mso | IE]></td></tr></table><![endif]--> <!-- <#if macros.footer?has_content> --> <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#F8F9FC" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
  <div style="background:#F8F9FC;background-color:#F8F9FC;margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#F8F9FC;background-color:#F8F9FC;width:100%;">
      <tbody>
      <tr>
        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <div style="font-family:helvetica;font-size:11px;line-height:1;text-align:center;color:#9B9B9B;">
                    ${macros.footer?html!} <!-- <#if macros.footerLink?has_content> --> <a href="${macros.footerLink!}" style="color: #8095B5;display: inline-block;margin: 0 6px;text-decoration: none;"> ${macros.footerLinkTitle?html!} </a> <!-- </#if> -->
                  </div></td>
              </tr>
              </tbody>
            </table>
          </div> <!--[if mso | IE]></td></tr></table><![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div> <!--[if mso | IE]></td></tr></table><![endif]--> <!-- </#if> -->
</div>
</body>
</html>

--${boundary!"boundary-not-set"}-2--

--${boundary!"boundary-not-set"}-1
Content-Disposition: attachment; filename=logo.png
Content-Type: image/png; name=logo.png
Content-ID: <logo.cid>
Content-Transfer-Encoding: base64

${macros.logo}

--${boundary!"boundary-not-set"}-1--

--${boundary!"boundary-not-set"}
Content-Disposition: attachment; filename=encrypted.pdf
Content-Type: application/pdf; name=encrypted.pdf
Content-Transfer-Encoding: base64
X-CipherMail-Marker: attachment

JVBERi0xLjQKJcOkw7zDtsOfCjIgMCBvYmoKPDwvTGVuZ3RoIDMgMCBSL0ZpbHRlci9GbGF0ZURl
Y29kZT4+CnN0cmVhbQp4nB2KuwrCQBBF+/mKqYWNM2P2bgLLFBEt7AIDFmLnoxNM4+9nIwcOB+6V
TvlHXxZO0hJqzWXcvDz5uuPPf9xY3jQFZXQDF+vbIR68PyurcbxuFeoJ1aQIesAE8KFi9JQrjn6o
yH6PC52CZpp5Bfe6GasKZW5kc3RyZWFtCmVuZG9iagoKMyAwIG9iagoxMTQKZW5kb2JqCgo1IDAg
b2JqCjw8L1R5cGUvRm9udC9TdWJ0eXBlL1R5cGUxL0Jhc2VGb250L1RpbWVzLVJvbWFuCi9FbmNv
ZGluZy9XaW5BbnNpRW5jb2RpbmcKPj4KZW5kb2JqCgo2IDAgb2JqCjw8L0YxIDUgMCBSCj4+CmVu
ZG9iagoKNyAwIG9iago8PC9Gb250IDYgMCBSCi9Qcm9jU2V0Wy9QREYvVGV4dF0KPj4KZW5kb2Jq
CgoxIDAgb2JqCjw8L1R5cGUvUGFnZS9QYXJlbnQgNCAwIFIvUmVzb3VyY2VzIDcgMCBSL01lZGlh
Qm94WzAgMCA2MTIgNzkyXS9Hcm91cDw8L1MvVHJhbnNwYXJlbmN5L0NTL0RldmljZVJHQi9JIHRy
dWU+Pi9Db250ZW50cyAyIDAgUj4+CmVuZG9iagoKNCAwIG9iago8PC9UeXBlL1BhZ2VzCi9SZXNv
dXJjZXMgNyAwIFIKL01lZGlhQm94WyAwIDAgNTk1IDg0MiBdCi9LaWRzWyAxIDAgUiBdCi9Db3Vu
dCAxPj4KZW5kb2JqCgo4IDAgb2JqCjw8L1R5cGUvQ2F0YWxvZy9QYWdlcyA0IDAgUgovT3BlbkFj
dGlvblsxIDAgUiAvWFlaIG51bGwgbnVsbCAwXQo+PgplbmRvYmoKCjkgMCBvYmoKPDwvQXV0aG9y
PEZFRkYwMDZEMDA2MTAwNzIwMDc0MDA2OTAwNkEwMDZFPgovQ3JlYXRvcjxGRUZGMDA1NzAwNzIw
MDY5MDA3NDAwNjUwMDcyPgovUHJvZHVjZXI8RkVGRjAwNEYwMDcwMDA2NTAwNkUwMDRGMDA2NjAw
NjYwMDY5MDA2MzAwNjUwMDJFMDA2RjAwNzIwMDY3MDAyMDAwMzIwMDJFMDAzMz4KL0NyZWF0aW9u
RGF0ZShEOjIwMDgwMzI0MjIyODIzKzAxJzAwJyk+PgplbmRvYmoKCnhyZWYKMCAxMAowMDAwMDAw
MDAwIDY1NTM1IGYgCjAwMDAwMDA0MDEgMDAwMDAgbiAKMDAwMDAwMDAxOSAwMDAwMCBuIAowMDAw
MDAwMjA0IDAwMDAwIG4gCjAwMDAwMDA1NDMgMDAwMDAgbiAKMDAwMDAwMDIyNCAwMDAwMCBuIAow
MDAwMDAwMzE3IDAwMDAwIG4gCjAwMDAwMDAzNDggMDAwMDAgbiAKMDAwMDAwMDY0MSAwMDAwMCBu
IAowMDAwMDAwNzI0IDAwMDAwIG4gCnRyYWlsZXIKPDwvU2l6ZSAxMC9Sb290IDggMCBSCi9JbmZv
IDkgMCBSCi9JRCBbIDwzRTlDMTAyOTVGNTgzMUNDRTdFQzFBREQ2OTFBNTk3Qz4KPDNFOUMxMDI5
NUY1ODMxQ0NFN0VDMUFERDY5MUE1OTdDPiBdCi9Eb2NDaGVja3N1bSAvMjc1QjJEQjg3OTIwNjZE
RTU1NEQwRTExQUI5RjI0NDcKPj4Kc3RhcnR4cmVmCjk1MgolJUVPRgo=

--${boundary!"boundary-not-set"}--
