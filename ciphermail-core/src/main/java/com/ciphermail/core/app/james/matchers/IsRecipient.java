/*
 * Copyright (c) 2012-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.james.MailAddressUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Matcher that matches recipients against a regular expression.
 *
 * This matcher is named IsRecipient and not RecipientIs because the James defaults matchers already contain a
 * matcher named RecipientIs.
 *
 * @author Martijn Brinkers
 *
 */
public class IsRecipient extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(IsRecipient.class);

    /*
     * The pattern against which the email address is matched
     */
    private Pattern pattern;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        pattern = Pattern.compile(StringUtils.defaultString(StringUtils.trimToNull(getCondition())));
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        Collection<MailAddress> result = new LinkedList<>();

        Collection<MailAddress> addresses = MailAddressUtils.getRecipients(mail);

        if (addresses != null)
        {
            for (MailAddress address : addresses)
            {
                Matcher matcher = pattern.matcher(address != null ? address.toString() : "");

                if (matcher.matches()) {
                    result.add(address);
                }
            }
        }

        return result;
    }
}
