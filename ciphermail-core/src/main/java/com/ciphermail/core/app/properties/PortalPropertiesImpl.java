/*
 * Copyright (c) 2011-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import com.ciphermail.core.common.security.password.validator.PasswordStrengthValidator;
import com.ciphermail.core.common.util.URIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Nonnull;
import java.util.Optional;

@UserPropertiesType(name = "portal", displayName = "Portal", order = 110)
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PortalPropertiesImpl extends DelegatedHierarchicalProperties implements PortalProperties
{
    /*
     * Portal property names
     */
    public static final String PASSWORD                          = "portal-password";
    private static final String ENABLED                          = "portal-enabled";
    private static final String TFA_ENABLED                      = "portal-2fa-enabled";
    private static final String TFA_SECRET                       = "portal-2fa-secret";
    private static final String BASE_URL                         = "portal-base-url";
    private static final String AUTO_SIGNUP                      = "portal-auto-signup";
    private static final String SIGNUP_VALIDITY_INTERVAL_SECONDS = "portal-signup-validity-interval-seconds";
    private static final String PASSWORD_POLICY                  = "portal-password-policy";
    private static final String PASSWORD_POLICY_URL              = "portal-password-policy-url";
    private static final String PASSWORD_RESET_TIMESTAMP         = "portal-password-reset-timestamp";
    private static final String PASSWORD_RESET_INTERVAL          = "portal-password-reset-interval";
    private static final String RESET_VALIDITY_INTERVAL_SECONDS  = "portal-password-reset-validity-interval-seconds";
    // these properties should be package private
    static final String SIGNUP_URL                               = "portal-signup-url";
    static final String PASSWORD_RESET_URL                       = "portal-password-reset-url";

    @Autowired
    @Qualifier("portalPasswordStrengthValidator")
    private PasswordStrengthValidator passwordStrengthValidator;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public PortalPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(name = PASSWORD)
    public void setPassword(String password)
    throws HierarchicalPropertiesException
    {
        String validatedPassword = UserPropertiesValidators.validatePassword(PASSWORD,
                password,
                passwordStrengthValidator, Optional.ofNullable(getPasswordPolicy()).orElse(""));

        setProperty(PASSWORD, validatedPassword != null ? passwordEncoder.encode(validatedPassword) : null);
    }

    @Override
    @UserProperty(name = PASSWORD, editor = UserPropertyEditors.PASSWORD_EDITOR,
            order = 10,
            allowNull = true
    )
    public String getPassword()
    throws HierarchicalPropertiesException
    {
        return getProperty(PASSWORD);
    }

    @Override
    @UserProperty(name = ENABLED)
    public void setEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, ENABLED, enabled);
    }

    @Override
    @UserProperty(name = ENABLED,
            order = 20
    )
    public Boolean getEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, ENABLED);
    }

    @Override
    @UserProperty(name = TFA_ENABLED,
            order = 25
    )
    public void set2FAEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, TFA_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = TFA_ENABLED,
            order = 25
    )
    public Boolean get2FAEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, TFA_ENABLED);
    }

    @Override
    @UserProperty(name = TFA_SECRET)
    public void set2FASecret(String secret)
    throws HierarchicalPropertiesException
    {
        setProperty(TFA_SECRET, secret);
    }

    @Override
    @UserProperty(name = TFA_SECRET,
            visible = false,
            allowNull = true,
            domain = false,
            global = false)
    public String get2FASecret()
    throws HierarchicalPropertiesException
    {
        return getProperty(TFA_SECRET);
    }

    @Override
    @UserProperty(name = BASE_URL)
    public void setBaseURL(String url)
    throws HierarchicalPropertiesException
    {
        setProperty(BASE_URL, UserPropertiesValidators.validateURI(
                BASE_URL, url, URIUtils.URIType.FULL));
    }

    @Override
    @UserProperty(name = BASE_URL,
            order = 30,
            allowNull = true
    )
    public String getBaseURL()
    throws HierarchicalPropertiesException
    {
        return getProperty(BASE_URL);
    }

    @Override
    @UserProperty(name = SIGNUP_URL)
    public void setSignupURL(String url)
    throws HierarchicalPropertiesException
    {
        setProperty(SIGNUP_URL, UserPropertiesValidators.validateURI(
                SIGNUP_URL, url, URIUtils.URIType.FULL));
    }

    @Override
    @UserProperty(name = SIGNUP_URL,
            visible = false,
            allowNull = true
    )
    public String getSignupURL()
    throws HierarchicalPropertiesException
    {
        return getProperty(SIGNUP_URL);
    }

    @Override
    @UserProperty(name = AUTO_SIGNUP)
    public void setAutoSignup(Boolean autoSignup)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, AUTO_SIGNUP, autoSignup);
    }

    @Override
    @UserProperty(name = AUTO_SIGNUP,
            order = 40
    )
    public Boolean getAutoSignup()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, AUTO_SIGNUP);
    }

    @Override
    @UserProperty(name = SIGNUP_VALIDITY_INTERVAL_SECONDS)
    public void setSignupValidityIntervalSeconds(Long validityIntervalSeconds)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, SIGNUP_VALIDITY_INTERVAL_SECONDS, validityIntervalSeconds);
    }

    @Override
    @UserProperty(name = SIGNUP_VALIDITY_INTERVAL_SECONDS,
            visible = false
    )
    public Long getSignupValidityIntervalSeconds()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, SIGNUP_VALIDITY_INTERVAL_SECONDS);
    }

    @Override
    @UserProperty(name = PASSWORD_POLICY)
    public void setPasswordPolicy(String passwordPolicy)
    throws HierarchicalPropertiesException
    {
        setProperty(PASSWORD_POLICY, UserPropertiesValidators.validatePasswordPolicy(PASSWORD_POLICY,
                passwordPolicy));
    }

    @Override
    @UserProperty(name = PASSWORD_POLICY,
            order = 60
    )
    public String getPasswordPolicy()
    throws HierarchicalPropertiesException
    {
        return getProperty(PASSWORD_POLICY);
    }

    @Override
    @UserProperty(name = PASSWORD_POLICY_URL)
    public void setPasswordPolicyURL(String passwordPolicyURL)
    throws HierarchicalPropertiesException
    {
        setProperty(PASSWORD_POLICY_URL, UserPropertiesValidators.validateURI(
                PASSWORD_POLICY_URL, passwordPolicyURL, URIUtils.URIType.FULL));
    }

    @Override
    @UserProperty(name = PASSWORD_POLICY_URL,
            order = 70,
            allowNull = true
    )
    public String getPasswordPolicyURL()
    throws HierarchicalPropertiesException
    {
        return getProperty(PASSWORD_POLICY_URL);
    }

    @Override
    @UserProperty(name = PASSWORD_RESET_TIMESTAMP)
    public void setPasswordResetTimestamp(Long passwordResetTimestamp)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, PASSWORD_RESET_TIMESTAMP, passwordResetTimestamp);
    }

    @Override
    @UserProperty(name = PASSWORD_RESET_TIMESTAMP,
            order = 80,
            allowNull = true
    )
    public Long getPasswordResetTimestamp()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, PASSWORD_RESET_TIMESTAMP, null);
    }

    @Override
    @UserProperty(name = PASSWORD_RESET_INTERVAL)
    public void setPasswordResetInterval(Long interval)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, PASSWORD_RESET_INTERVAL, interval);
    }

    @Override
    @UserProperty(name = PASSWORD_RESET_INTERVAL,
            visible = false
    )
    public Long getPasswordResetInterval()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, PASSWORD_RESET_INTERVAL);
    }

    @Override
    @UserProperty(name = PASSWORD_RESET_URL)
    public void setPasswordResetURL(String url)
    throws HierarchicalPropertiesException
    {
        setProperty(PASSWORD_RESET_URL, UserPropertiesValidators.validateURI(
                PASSWORD_RESET_URL, url, URIUtils.URIType.FULL));
    }

    @Override
    @UserProperty(name = PASSWORD_RESET_URL,
            visible = false,
            allowNull = true
    )
    public String getPasswordResetURL()
    throws HierarchicalPropertiesException
    {
        return getProperty(PASSWORD_RESET_URL);
    }

    @Override
    @UserProperty(name = RESET_VALIDITY_INTERVAL_SECONDS)
    public void setPasswordResetValidityIntervalSeconds(Long validityIntervalSeconds)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, RESET_VALIDITY_INTERVAL_SECONDS, validityIntervalSeconds);
    }

    @Override
    @UserProperty(name = RESET_VALIDITY_INTERVAL_SECONDS,
            visible = false
    )
    public Long getPasswordResetValidityIntervalSeconds()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, RESET_VALIDITY_INTERVAL_SECONDS);
    }
}
