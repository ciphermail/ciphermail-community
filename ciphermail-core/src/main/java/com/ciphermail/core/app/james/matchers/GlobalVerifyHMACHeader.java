/*
 * Copyright (c) 2012-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.UnhandledException;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplateBuilder;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.Objects;

/**
 * Extension of VerifyHMACHeader that reads the secret from the global preferences.
 *
 * @author Martijn Brinkers
 *
 */
@SuppressWarnings({"java:S6813"})
public class GlobalVerifyHMACHeader extends VerifyHMACHeader
{
    private static final Logger logger = LoggerFactory.getLogger(GlobalVerifyHMACHeader.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * Used for getting the secret from the global preferences
     */
    @Inject
    private GlobalPreferencesManager globalPreferencesManager;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    @Override
    public void init()
    throws MessagingException
    {
        super.init();

        Objects.requireNonNull(globalPreferencesManager);
        Objects.requireNonNull(transactionOperations);
    }

    @VisibleForTesting
    protected byte[] getSecretTransacted(Mail mail)
    {
        return transactionOperations.execute(status ->
        {
            try {
                return MiscStringUtils.getBytesASCII(globalPreferencesManager.getGlobalUserPreferences().
                        getProperties().getProperty(getSecret()));
            }
            catch (HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Override
    protected byte[] getSecret(Mail mail)
    throws MessagingException
    {
        try {
            RetryTemplateBuilder retryTemplateBuilder = RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder()
                    .withListener(new LoggingRetryListener(getLogger()));

            return retryTemplateBuilder.build().execute(status -> getSecretTransacted(mail));
        }
        catch(UnhandledException e) {
            throw new MessagingException("Error getting secret.", e);
        }
    }
}
