/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.openpgp.keyserver;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.properties.PGPProperties;
import com.ciphermail.core.app.properties.PGPPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClient;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientSettings;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServers;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServersImpl;
import com.ciphermail.core.common.security.openpgp.keyserver.KeyServerKeyInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of KeyServerClient that reads all the key servers to query from the global settings
 *
 * @author Martijn Brinkers
 *
 */
public class KeyServerClientImpl implements KeyServerClient
{
    private static final Logger logger = LoggerFactory.getLogger(KeyServerClientImpl.class);

    /*
     * Used for accessing the global preferences
     */
    private final GlobalPreferencesManager globalPreferencesManager;

    /*
     * Client which interfaces with a PGP key server using the OpenPGP HTTP key server Protocol (HKP)
     */
    private final HKPKeyServerClient hkpKeyServerClient;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    /*
     * If true, duplicate results are suppressed
     */
    private boolean skipDuplicateKeys = true;

    public KeyServerClientImpl(
            @Nonnull GlobalPreferencesManager globalPreferencesManager,
            @Nonnull HKPKeyServerClient hkpKeyServerClient,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        this.globalPreferencesManager = Objects.requireNonNull(globalPreferencesManager);
        this.hkpKeyServerClient = Objects.requireNonNull(hkpKeyServerClient);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
    }

    @Override
    public @Nonnull KeyServerClientSearchResult searchKeys(String query, boolean exact, Integer maxKeys)
    throws IOException
    {
        Collection<KeyServerKeyInfo> keys = skipDuplicateKeys ? new LinkedHashSet<>() : new LinkedList<>();

        List<String> errors = new LinkedList<>();

        boolean done = false;

        for (HKPKeyServerClientSettings clientSettings : getServers().getClientSettings())
        {
            try {
                List<KeyServerKeyInfo> subResults = hkpKeyServerClient.searchKeys(clientSettings, query, exact, maxKeys);

                if (subResults != null)
                {
                    for (KeyServerKeyInfo keyInfo : subResults)
                    {
                        keys.add(keyInfo);

                        if (maxKeys != null && keys.size() >= maxKeys)
                        {
                            done = true;

                            break;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                logger.debug("Error searching key server @" + clientSettings, e);

                errors.add(ExceptionUtils.getRootCauseMessage(e) + " (" + clientSettings.getServerURL() + ")");
            }

            if (done) {
                break;
            }
        }

        return new KeyServerClientSearchResultImpl(keys, errors);
    }

    @Override
    public PGPPublicKeyRingCollection getKeys(@Nonnull String keyID)
    throws IOException
    {
        PGPPublicKeyRingCollection keyRing = null;

        String lastError = null;

        for (HKPKeyServerClientSettings clientSettings : getServers().getClientSettings())
        {
            try {
                keyRing = hkpKeyServerClient.getKeys(clientSettings, keyID);

                if (keyRing != null) {
                    break;
                }
            }
            catch (Exception e)
            {
                logger.debug("Error getting key @" + clientSettings, e);

                lastError = ExceptionUtils.getRootCauseMessage(e);
            }
        }

        if (keyRing == null && lastError != null) {
            throw new IOException(lastError);
        }

        return keyRing;
    }

    @Override
    public KeyServerClientSubmitResult submitKeys(PGPPublicKeyRingCollection keyRingCollection)
    throws IOException
    {
        List<String> responses = new LinkedList<>();
        List<String> urls = new LinkedList<>();
        List<String> errors = new LinkedList<>();

        for (HKPKeyServerClientSettings clientSettings : getServers().getClientSettings())
        {
            try {
                responses.add(hkpKeyServerClient.submitKeys(clientSettings, keyRingCollection));

                urls.add(clientSettings.getServerURL());
            }
            catch (Exception e)
            {
                logger.debug("Error getting key @" + clientSettings, e);

                errors.add(ExceptionUtils.getRootCauseMessage(e) + " (" + clientSettings.getServerURL() + ")");
            }
        }

        return new KeyServerClientSubmitResultImpl(responses, urls, errors);
    }

    private PGPProperties getPGPProperties()
    throws HierarchicalPropertiesException
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(PGPPropertiesImpl.class)
                .createInstance(globalPreferencesManager.getGlobalUserPreferences().getProperties());
    }

    @Override
    public @Nonnull HKPKeyServers getServers()
    throws IOException
    {
        try {
            String json = getPGPProperties().getJSONEncodedPGPKeyServers();

            return StringUtils.isNotEmpty(json) ? HKPKeyServersImpl.fromJSON(json) : new HKPKeyServersImpl();
        }
        catch (HierarchicalPropertiesException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void setServers(HKPKeyServers servers)
    throws IOException
    {
        try {
            getPGPProperties().setJSONEncodedPGPKeyServers(servers != null ? HKPKeyServersImpl.toJSON(servers) : null);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IOException(e);
        }
    }

    @Override
    public boolean isSkipDuplicateKeys() {
        return skipDuplicateKeys;
    }

    @Override
    public void setSkipDuplicateKeys(boolean skipDuplicateKeys) {
        this.skipDuplicateKeys = skipDuplicateKeys;
    }
}
