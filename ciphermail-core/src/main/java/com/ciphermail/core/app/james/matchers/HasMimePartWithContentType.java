/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.mail.PartScanner;
import com.ciphermail.core.common.mail.PartScanner.PartListener;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

/**
 * Matcher that matches if a message has a specific content-type
 * <p>
 * Usage:
 * <p>
 * HasMimePartWithContentType=matchOnError=false, multipart/* | Matches if a multipart is found
 * <p>
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 */
public class HasMimePartWithContentType extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(HasMimePartWithContentType.class);

    /*
     * Max mime depth to search for parts
     */
    private int maxMimeDepth = 8;

    /*
     * The content type to match on
     */
    private String contentType;

    final PartListener partListener = (parent, part, context) -> HasMimePartWithContentType.this.onPart(part, context);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        contentType = StringUtils.trimToNull(getCondition());
    }

    protected String getContentType() {
        return contentType;
    }

    private boolean onPart(Part part, Object context)
    {
        boolean continueScanning = true;

        try {
            MutableBoolean match = (MutableBoolean) context;

            // check if conditionType matches the type of the message or of conditionType is null
            // that the message is a S/MIME message (does not matter what type of S/MIME message)
            if (part.isMimeType(getContentType()))
            {
                match.setValue(true);

                // Stop scanning
                continueScanning = false;
            }
        }
        catch (Exception e) {
            getLogger().error("Error handling MIME part", e);
        }

        return continueScanning;
    }

    protected boolean hasMatchingContentType(MimeMessage message)
    throws MessagingException, IOException, PartException
    {
        PartScanner partScanner = new PartScanner(partListener, maxMimeDepth);

        partScanner.setExceptionOnMaxDepthReached(true);

        MutableBoolean match = new MutableBoolean();

        if (getContentType() != null) {
            partScanner.scanPart(message, match);
        }

        return match.booleanValue();
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        Collection<MailAddress> matching = Collections.emptyList();

        try {
            if (hasMatchingContentType(mail.getMessage())) {
                matching = mail.getRecipients();
            }
        }
        catch (IOException | PartException e) {
            throw new MessagingException("Error matching content-type", e);
        }

        return matching;
    }

    public int getMaxMimeDepth() {
        return maxMimeDepth;
    }

    public void setMaxMimeDepth(int maxMimeDepth) {
        this.maxMimeDepth = maxMimeDepth;
    }
}
