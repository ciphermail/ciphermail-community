/*
 * Copyright (c) 2015-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Mailet which adds a "Disposition-Notification-To" header to the message
 */
@SuppressWarnings({"java:S6813"})
public class RequestDispositionNotification extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(RequestDispositionNotification.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * The recipient of the disposition notification
     */
    private String staticRecipient;

    /*
     * The attribute to read the value from
     */
    private String attribute;

    /*
     * If set and an error occurs removing the headers, the name of next processor
     */
    private String errorProcessor;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        RECIPIENT       ("recipient"),
        ATTRIBUTE       ("attribute"),
        ERROR_PROCESSOR ("errorProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    public final void initMailet()
    throws MessagingException
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        Objects.requireNonNull(messageOriginatorIdentifier);

        staticRecipient = StringUtils.trimToNull(getInitParameter(Parameter.RECIPIENT.name));
        attribute = StringUtils.trimToNull(getInitParameter(Parameter.ATTRIBUTE.name));

        errorProcessor = getInitParameter(Parameter.ERROR_PROCESSOR.name);
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            final MimeMessage sourceMessage = mail.getMessage();

            String address = null;

            if (attribute != null)
            {
                address = StringUtils.trimToNull(MailAttributesUtils.getManagedAttributeValue(mail,
                        attribute, String.class).orElse(null));
            }

            if (address == null) {
                address = staticRecipient;
            }

            InternetAddress recipientAddress = null;

            if (address != null)
            {
                if (EmailAddressUtils.isValid(address)) {
                    recipientAddress = new InternetAddress(address);
                }
            }
            else {
                recipientAddress = messageOriginatorIdentifier.getOriginator(mail);
            }

            // If from header from Mail is invalid email address, the dummy address is returned by
            // messageOriginatorIdentifier. We never want to mail to the dummy address.
            if (EmailAddressUtils.isInvalidDummyAddress(recipientAddress)) {
                recipientAddress = null;
            }

            if (recipientAddress != null)
            {
                try {
                    MimeMessage clone = MailUtils.cloneMessageWithFixedMessageID(sourceMessage);

                    clone.setHeader("Disposition-Notification-To", recipientAddress.toString());

                    clone.saveChanges();

                    MailUtils.validateMessage(clone);

                    mail.setMessage(clone);
                }
                catch (Exception e)
                {
                    logger.warn("Message with message-id {} and MailID {} is not a valid MIME message. " +
                            "The Disposition-Notification-To header can therefore not be added.",
                            MailUtils.getMessageIDQuietly(sourceMessage),
                            CoreApplicationMailAttributes.getMailID(mail));

                    if (StringUtils.isNotEmpty(errorProcessor)) {
                        mail.setState(errorProcessor);
                    }
                }
            }
            else {
                logger.warn("A valid recipient for the disposition notification was not found. "
                    + "Disposition-Notification-To header not added.");
            }
        }
        catch (Exception e) {
            logger.error("Error adding Disposition-Notification-To header.", e);
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (errorProcessor != null) {
            requiredProcessors.add(new ProcessingState(errorProcessor));
        }

        return requiredProcessors;
    }
}
