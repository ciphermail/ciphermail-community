/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.service;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.properties.CipherMailFactoryProperties;
import com.ciphermail.core.app.properties.CipherMailFactoryPropertiesImpl;
import com.ciphermail.core.app.properties.DKIMProperties;
import com.ciphermail.core.app.properties.DKIMPropertiesImpl;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.properties.FactoryPropertiesProvider;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.PropertiesResolver;
import com.ciphermail.core.common.properties.PropertiesResolver.PropertiesResolverException;
import com.ciphermail.core.common.properties.StandardHierarchicalProperties;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.PKISecurityServicesFactory;
import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.security.certpath.CertStoreTrustAnchorBuilder;
import com.ciphermail.core.common.security.certpath.TrustAnchorBuilder;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.certstore.X509StoreEventListener;
import com.ciphermail.core.common.security.crl.CRLDownloader;
import com.ciphermail.core.common.security.crl.CRLStoreMaintainer;
import com.ciphermail.core.common.security.crl.CRLStoreUpdaterParameters;
import com.ciphermail.core.common.security.crl.DefaultCRLStoreUpdaterParametersBuilder;
import com.ciphermail.core.common.security.crypto.RandomGenerator;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.common.security.keystore.KeyStoreProviderKeyStoreWrapper;
import com.ciphermail.core.common.security.keystore.jce.DatabaseKeyStoreLoadStoreParameter;
import com.ciphermail.core.common.security.openpgp.PGPSecurityFactoryFactory;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.common.util.Base32Utils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.DefaultFileComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * CipherMailServices contains factory-method implementations for the construction of Spring beans which require more
 * sophisticated creation.
 */
@Component
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class CipherMailServices
{
    private static final Logger logger = LoggerFactory.getLogger(CipherMailServices.class);

    /*
     * The default nr of random bytes used for the generation of the secret values
     */
    private static final int DEFAULT_SECRET_LENGTH = 32;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private RandomGenerator randomGenerator;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    private CipherMailServices() {
        // Empty on purpose
    }

    @PostConstruct
    public void initialize()
    {
        logger.info("Initializing back-end");

        Objects.requireNonNull(transactionOperations).executeWithoutResult(status ->
        {
            try {
                // Read the factory properties from the properties file and synchronize the settings with the database
                globalPreferencesManager.syncFactoryProperties();

                initializeDefaultSettings();
            }
            catch (HierarchicalPropertiesException | NoSuchAlgorithmException | IOException |
                   NoSuchProviderException e)
            {
                throw new UnhandledException(e);
            }
        });
    }

    public void initializeDefaultSettings()
    throws HierarchicalPropertiesException, NoSuchAlgorithmException, IOException, NoSuchProviderException
    {
        UserProperties properties = globalPreferencesManager.getGlobalUserPreferences().getProperties();

        initializeServerSecret(properties);
        initializeSystemMailSecret(properties);
        initializeDKIMKeyPair(properties);
    }

    private void initializeServerSecret(@Nonnull UserProperties properties)
    throws HierarchicalPropertiesException
    {
        // Generate a random global server secret if there is no server secret set
        if (StringUtils.isBlank(properties.getServerSecret()))
        {
            logger.info("Generating server secret.");

            properties.setServerSecret(Base32Utils.base32Encode(randomGenerator.generateRandom(DEFAULT_SECRET_LENGTH)));
        }
    }

    private void initializeSystemMailSecret(@Nonnull UserProperties properties)
    throws HierarchicalPropertiesException
    {
        // Generate a random global secret if there is no secret set
        if (StringUtils.isBlank(properties.getSystemMailSecret()))
        {
            logger.info("Generating system mail secret.");

            properties.setSystemMailSecret(Base32Utils.base32Encode(randomGenerator.generateRandom(DEFAULT_SECRET_LENGTH)));
        }
    }

    private void initializeDKIMKeyPair(@Nonnull UserProperties properties)
    throws HierarchicalPropertiesException, NoSuchAlgorithmException, NoSuchProviderException, IOException
    {
        // Generate a DKIM key pair if it does not exist
        DKIMProperties dkimProperties = userPropertiesFactoryRegistry.getFactoryForClass(DKIMPropertiesImpl.class)
                .createInstance(properties);

        if (StringUtils.isBlank(dkimProperties.getSystemKeyPair()))
        {
            logger.info("Generating DKIM key pair.");

            // The DKIM pair will be generated with BC since we need the key to be
            // exportable (which doesn't work with an HSM)
            //
            // Note: if HSM support is required for this key, we need to store the
            // key in the key store.
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA",
                    SecurityFactoryBouncyCastle.PROVIDER_NAME);

            keyPairGenerator.initialize(2048);

            KeyPair keyPair = keyPairGenerator.generateKeyPair();

            StringWriter pem = new StringWriter();

            JcaPEMWriter writer = new JcaPEMWriter(pem);

            writer.writeObject(keyPair);
            writer.close();

            dkimProperties.setSystemKeyPair(pem.toString());
        }
    }

    /*
     * Event listener that will force an update of the TrustAnchorBuilder cache when a certificate
     * is added and/or removed to/from the root store.
     */
    private static class RootStoreEventListener implements X509StoreEventListener
    {
        private final TrustAnchorBuilder trustAnchorBuilder;

        public RootStoreEventListener(@Nonnull TrustAnchorBuilder trustAnchorBuilder)  {
            this.trustAnchorBuilder = Objects.requireNonNull(trustAnchorBuilder);
        }

        @Override
        public void onChange() {
            trustAnchorBuilder.refresh();
        }
    }

    /**
     * Builds the FactoryProperties service.
     */
    public static CipherMailFactoryProperties buildCipherMailFactoryProperties(
            @Nonnull File factoryPropertiesDir,
            FactoryPropertiesProvider factoryPropertiesProvider,
            File propertiesResolverBaseDir)
    throws IOException, PropertiesResolverException
    {
        Properties properties = new Properties();

        if (!factoryPropertiesDir.isDirectory() || !factoryPropertiesDir.exists()) {
            throw new IllegalArgumentException(String.format("Directory %s does not exist or is not readable",
                    factoryPropertiesDir));
        }

        // Load additional property files in sorted order.
        Collection<File> unsortedPropertyFiles = FileUtils.listFiles(factoryPropertiesDir,
                new String[]{"properties"}, true /* recursive */);

        ArrayList<File> sortedPropertyFiles = new ArrayList<>(unsortedPropertyFiles);

        sortedPropertyFiles.sort(DefaultFileComparator.DEFAULT_COMPARATOR);

        for (File additionalPropertyFile : sortedPropertyFiles)
        {
            logger.info("Loading factory properties from {}", additionalPropertyFile);

            try(FileReader fileReader = new FileReader(additionalPropertyFile)) {
                properties.load(fileReader);
            }
        }

        // The loaded factory properties can contain special properties with a binding (like file:) which
        // need to be resolved
        PropertiesResolver propertiesResolver = new PropertiesResolver();

        if (propertiesResolverBaseDir != null) {
            propertiesResolver.setBaseDir(propertiesResolverBaseDir);
        }

        // Load the properties from the factoryPropertiesProvider and merge with the properties loaded from the
        // factory properties file
        if (factoryPropertiesProvider != null)
        {
            List<Properties> additionalProperties = factoryPropertiesProvider.getProperties();

            if (additionalProperties != null)
            {
                for (Properties additional : additionalProperties) {
                    properties.putAll(additional);
                }
            }
        }

        propertiesResolver.resolve(properties);

        // The implementation of HierarchicalProperties must be thread safe because the service
        // will be a singleton.
        HierarchicalProperties factoryProperties = new StandardHierarchicalProperties(
                UserPreferencesCategory.GLOBAL.name(), null, properties);

        return CipherMailFactoryPropertiesImpl.getInstance(factoryProperties);
    }

    /**
     * Builds the KeyStoreProvider service.
     */
    public static KeyStoreProvider buildKeyStoreProvider(
            @Nonnull String storeName,
            @Nonnull SessionManager sessionManager)
    throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException
    {
        // because we are going to use the CipherMailProvider database KeyStore we have to sure that the
        // CipherMail provider is registered.
        CipherMailProvider.initialize(sessionManager);

        // The implementation of KeyStore must be thread safe because the service
        // will be a singleton.
        KeyStore keyStore = KeyStore.getInstance(CipherMailProvider.DATABASE_KEYSTORE, CipherMailProvider.PROVIDER);
        keyStore.load(new DatabaseKeyStoreLoadStoreParameter(storeName, sessionManager));

        return new KeyStoreProviderKeyStoreWrapper(keyStore);
    }

    /**
     * Builds the PGP KeyStoreProvider service.
     */
    public static KeyStoreProvider buildPGPKeyStoreProvider(
            @Nonnull String storeName,
            @Nonnull SessionManager sessionManager)
    throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException
    {
        // because we are going to use the MITM database KeyStore we have to sure that the MITM provider
        // is registered.
        CipherMailProvider.initialize(sessionManager);

        // The implementation of KeyStore must be thread safe because the service
        // will be a singleton.
        KeyStore keyStore = KeyStore.getInstance(CipherMailProvider.DATABASE_KEYSTORE, CipherMailProvider.PROVIDER);

        DatabaseKeyStoreLoadStoreParameter keystoreParameters = new DatabaseKeyStoreLoadStoreParameter(storeName,
                sessionManager);

        // The KeyStore should use the PGP security factory.
        // Note: the PGP security factory will not be used by all functionality. For example Certificates will be
        // created using the default provider (see CertificateUserType). Key however will be created with the
        // PGP security factory.
        keystoreParameters.setSecurityFactory(PGPSecurityFactoryFactory.getSecurityFactory());

        keyStore.load(keystoreParameters);

        return new KeyStoreProviderKeyStoreWrapper(keyStore);
    }

    /**
     * Builds the TrustAnchorBuilder service.
     */
    public static TrustAnchorBuilder buildTrustAnchorBuilder(
            @Nonnull X509CertStoreExt rootStore,
            long trustAnchorBuilderUpdateCheckInterval)
    {
        // The implementation of TrustAnchorBuilder must be thread safe because the service
        // will be a singleton.
        TrustAnchorBuilder trustAnchorBuilder = new CertStoreTrustAnchorBuilder(rootStore,
                trustAnchorBuilderUpdateCheckInterval);

        // register the listener for changes to the root store (add/remove of certificates) so we can
        // force an update of the trustAnchorBuilder.
        rootStore.setStoreEventListener(new RootStoreEventListener(trustAnchorBuilder));

        return trustAnchorBuilder;
    }

    /**
     * Builds the PKISecurityServices service.
     */
    public static PKISecurityServices buildPKISecurityServices(@Nonnull PKISecurityServicesFactory factory)
    {
        // The implementation of PKISecurityServices must be thread safe because the service
        // will be a singleton.
        return factory.createPKISecurityServices();
    }

    /**
     * Builds the CRLStoreUpdaterParameters service
     */
    public static CRLStoreUpdaterParameters buildCRLStoreUpdaterParameters(
            @Nonnull PKISecurityServices pKISecurityServices,
            @Nonnull CRLDownloader crlDownloader,
            @Nonnull CRLStoreMaintainer crlStoreMaintainer,
            boolean checkTrust)
    {
        DefaultCRLStoreUpdaterParametersBuilder parametersBuilder = new DefaultCRLStoreUpdaterParametersBuilder(
                pKISecurityServices, crlDownloader, crlStoreMaintainer);

        parametersBuilder.setCheckTrust(checkTrust);

        // The implementation of CRLStoreUpdaterParameters must be thread safe because the service
        // will be a singleton.
        return parametersBuilder.createCRLStoreUpdaterParameters();
    }
}
