/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.common.util.ThreadAwareContext;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.apache.mailet.base.GenericMatcher;
import org.slf4j.Logger;

import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractContextAwareCipherMailMatcher extends GenericMatcher
{
    private static final String CONDITION_REG_EXPR = "(?i)^\\s*matchOnError\\s*=\\s*(true|false)\\s*(?:,(.*)|)";

    /*
     * Example patterns:
     * 1) matchOnError=True, other condition
     * 2) MatchOnError = true
     * 3) MatchOnError = faLse,
     */
    private final Pattern conditionPattern = Pattern.compile(CONDITION_REG_EXPR);

    /*
     * True if the matcher should match all recipients on error, false if nothing should match.
     */
    private boolean matchOnError;

    protected boolean isMatchOnError() {
        return matchOnError;
    }

    protected abstract Logger getLogger();

    /*
     * The activationContext can be used to store objects used in the matcher in a thread safe way.
     */
    private final ThreadAwareContext activationContext = new ThreadAwareContext();

    public ThreadAwareContext getActivationContext() {
        return activationContext;
    }

    @Override
    public final String getCondition()
    {
        String condition = super.getCondition();

        if (condition == null) {
            throw new IllegalArgumentException("Condition does not match: " + CONDITION_REG_EXPR);
        }
        // condition must be xml unescaped.
        condition = StringEscapeUtils.unescapeXml(condition);

        // Remove the matchOnErrorPart
        Matcher matcher = conditionPattern.matcher(condition);

        if (!matcher.matches()) {
            throw new IllegalArgumentException("Condition does not match: " + CONDITION_REG_EXPR);
        }

        matchOnError = Boolean.parseBoolean(matcher.group(1));

        condition = matcher.group(2);

        if (condition == null) {
            condition = "";
        }

        return condition.trim();
    }

    @Override
    public final Collection<MailAddress> match(Mail mail)
    {
        try {
            try {
                return matchMail(mail);
            }
            finally {
                activationContext.clear();
            }
        }
        catch (MessagingException e) {
            getLogger().error("MessagingException in match.", e);
        }
        catch(Exception e) {
            getLogger().error("Unhandled Exception.", e);
        }

        Collection<MailAddress> matching = Collections.emptyList();

        if (isMatchOnError()) {
            matching = mail.getRecipients();
        }

        return matching;
    }

    public abstract Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException;
}
