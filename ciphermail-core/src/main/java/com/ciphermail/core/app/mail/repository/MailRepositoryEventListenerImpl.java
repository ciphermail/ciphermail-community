/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.mail.repository;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.JamestUtils;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MailEnqueuer;
import com.ciphermail.core.app.properties.DLPProperties;
import com.ciphermail.core.app.properties.DLPPropertiesImpl;
import com.ciphermail.core.app.properties.TemplateProperties;
import com.ciphermail.core.app.properties.TemplatePropertiesImpl;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.repository.MailRepositoryEventListener;
import com.ciphermail.core.common.mail.repository.MailRepositoryItem;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.template.TemplateBuilder;
import com.ciphermail.core.common.template.TemplateObjectsFactory;
import com.ciphermail.core.common.util.CollectionUtils;
import freemarker.template.SimpleHash;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.lang.StringUtils;
import org.apache.james.server.core.MailImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Default implementation of MailRepositoryEventListener.
 *
 * @author Martijn Brinkers
 *
 */
public class MailRepositoryEventListenerImpl implements MailRepositoryEventListener
{
    private static final Logger logger = LoggerFactory.getLogger(MailRepositoryEventListenerImpl.class);

    /*
     * Internally used enum to know which event was fired
     */
    private enum Notification {DELETE, RELEASE, EXPIRE}

    /*
     * For getting the user objects from an email address
     */
    private final UserWorkflow userWorkflow;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    /*
     * Service used to build Freemarker templates
     */
    private final TemplateBuilder templateBuilder;

    /*
     * MailEnqueuer is used for injecting the released mail into the James spool.
     */
    private final MailEnqueuer mailEnqueuer;

    /*
     * The processor for newly injected messages
     */
    private final String newMailProcessor;

    public MailRepositoryEventListenerImpl(
            @Nonnull UserWorkflow userWorkflow,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry,
            @Nonnull TemplateBuilder templateBuilder,
            @Nonnull MailEnqueuer mailEnqueuer,
            @Nonnull String newMailProcessor)
    {
        this.userWorkflow = Objects.requireNonNull(userWorkflow);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
        this.templateBuilder = Objects.requireNonNull(templateBuilder);
        this.mailEnqueuer = Objects.requireNonNull(mailEnqueuer);
        this.newMailProcessor = Objects.requireNonNull(newMailProcessor);
    }

    private void sendNotification(@Nonnull MailRepositoryItem item, @Nonnull Set<String> recipients,
            @Nonnull String templateSource)
    {
        SimpleHash root = TemplateObjectsFactory.createSimpleHash();
        SimpleHash org = TemplateObjectsFactory.createSimpleHash();

        org.put("subject", StringUtils.defaultString(item.getSubject()));
        org.put("id", Objects.toString(item.getID()));
        org.put("from", StringUtils.defaultString(item.getFromHeader()));
        org.put("messageID", StringUtils.defaultString(item.getMessageID()));

        try {
            // We will make a shallow copy of the original recipients to make sure that all recipients are
            // retrieved from the database in the current transaction (the collection might be a Hibernate collection)
            // and that the user cannot permanently change the collection (if the collection is a Hibernate collection)
            Collection<InternetAddress> originalRecipients = item.getRecipients();

            List<InternetAddress> copy = originalRecipients != null ?
                    new ArrayList<>(originalRecipients) : new ArrayList<>();

            org.put("recipients", copy);
        }
        catch (AddressException e) {
            logger.error("Error getting original recipients", e);
        }

        root.put("org", org);
        root.put("recipients", recipients);

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            Writer writer = new OutputStreamWriter(bos);

            Template template = templateBuilder.createTemplate(new StringReader(templateSource));

            template.process(root, writer);

            MimeMessage message = MailUtils.byteArrayToMessage(bos.toByteArray());

            message.saveChanges();

            MailUtils.validateMessage(message);

            MailImpl mail = MailImpl.builder().name(MailImpl.getId())
                    .state(newMailProcessor)
                    .addRecipients(MailAddressUtils.toMailAddressList(recipients))
                    .mimeMessage(message)
                    .build();

            try {
                mailEnqueuer.enqueue(mail);
            }
            finally {
                JamestUtils.dispose(mail);
            }
        }
        catch (IOException e) {
            logger.error("Error creating the notification template.", e);
        }
        catch (TemplateException e) {
            logger.error("The template is not a valid Freemarker template or variables are missing.", e);
        }
        catch (MessagingException e) {
            logger.error("The resulting mime message is not valid.", e);
        }
    }

    private DLPProperties createDLPProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(DLPPropertiesImpl.class)
                .createInstance(properties);
    }

    private TemplateProperties createTemplateProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(TemplatePropertiesImpl.class)
                .createInstance(properties);
    }

    /*
     * We assume that the caller already started a database transaction.
     */
    private void notifyUsers(@Nonnull Notification notification, @Nonnull MailRepositoryItem item)
    {
        try {
            // Get the user. If From is invalid, use the default user
            String filteredOriginator = EmailAddressUtils.canonicalizeAndValidate(
                    EmailAddressUtils.getEmailAddress(item.getOriginator()),
                    false /* use INVALID_EMAIL if invalid or null */);

            User user = userWorkflow.getUser(filteredOriginator, UserNotExistResult.DUMMY_IF_NOT_EXIST);

            UserProperties properties = user.getUserPreferences().getProperties();

            DLPProperties dlpProperties = createDLPProperties(properties);

            TemplateProperties templateProperties = createTemplateProperties(properties);

            boolean notifyOriginator;
            boolean notifyDLPManagers;

            String templateSource;

            switch (notification)
            {
                case DELETE -> {
                    notifyOriginator = dlpProperties.getSendDeleteNotifyToOriginator();
                    notifyDLPManagers = dlpProperties.getSendDeleteNotifyToDLPManagers();
                    templateSource = templateProperties.getDLPDeleteNotificationTemplate();
                }
                case RELEASE -> {
                    notifyOriginator = dlpProperties.getSendReleaseNotifyToOriginator();
                    notifyDLPManagers = dlpProperties.getSendReleaseNotifyToDLPManagers();
                    templateSource = templateProperties.getDLPReleaseNotificationTemplate();
                }
                case EXPIRE -> {
                    notifyOriginator = dlpProperties.getSendExpireNotifyToOriginator();
                    notifyDLPManagers = dlpProperties.getSendExpireNotifyToDLPManagers();
                    templateSource = templateProperties.getDLPExpireNotificationTemplate();
                }
                default -> throw new IllegalArgumentException("Unknown Notification: " + notification);
            }

            Set<String> recipients = new HashSet<>();

            // It can be that the originator address stored in the MailRepositoryItem is the invalid dummy
            // address (invalid@invalid.tld). We do not want to send email to the dummy user.
            if (EmailAddressUtils.isInvalidDummyAddress(filteredOriginator)) {
                notifyOriginator = false;
            }

            if (notifyOriginator && filteredOriginator != null) {
                recipients.add(filteredOriginator);
            }

            Set<String> managers = Set.copyOf(EmailAddressUtils.parseAddressList(dlpProperties.getDLPManagers()));

            if (CollectionUtils.isEmpty(managers)) {
                notifyDLPManagers = false;
            }

            if (notifyDLPManagers) {
                recipients.addAll(managers);
            }

            if (!recipients.isEmpty())
            {
                if (StringUtils.isNotBlank(templateSource)) {
                    sendNotification(item, recipients, templateSource);
                }
                else {
                    logger.debug("The notification template is empty.");
                }
            }
            else {
                logger.debug("There are no recipient to notify.");
            }
        }
        catch (Exception e) {
            logger.error("Error while notifying users.", e);
        }
    }

    private void log(@Nonnull MailRepositoryItem item, String action, String repository)
    {
        logger.info("The message with id {} from {} with message-id {} was {} from {}",
                item.getID(), item.getFromHeader(), item.getMessageID(), action, repository);
    }

    @Override
    public void onDeleted(String repository, @Nonnull MailRepositoryItem item)
    {
        log(item, "deleted", repository);

        notifyUsers(Notification.DELETE, item);
    }

    @Override
    public void onReleased(String repository, @Nonnull MailRepositoryItem item)
    {
        log(item, "released", repository);

        notifyUsers(Notification.RELEASE, item);
    }

    @Override
    public void onExpired(String repository, @Nonnull MailRepositoryItem item)
    {
        log(item, "deleted because it expired", repository);

        notifyUsers(Notification.EXPIRE, item);
    }
}
