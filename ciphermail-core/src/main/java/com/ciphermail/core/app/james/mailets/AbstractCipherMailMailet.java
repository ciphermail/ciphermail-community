/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.common.logging.LogLevel;
import com.ciphermail.core.common.util.ThreadAwareContext;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.base.GenericMailet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Objects;

/**
 * Extension of GenericMailet which adds some basic functionality shared by all CipherMail Mailets.
 */
public abstract class AbstractCipherMailMailet extends GenericMailet
{
    /*
     * The logger to be used for logging the log parameter message. This is done to allow all the logs to be changed
     * at once. Only the log message from the Mailet config (see log parameter) is logged with this logger. Classes
     * extending this Mailet should use #getLogger()
     */
    private static final Logger logLogger = LoggerFactory.getLogger("com.ciphermail.core.app.james.mailets.Default");

    /*
     * The activationContext can be used to store objects used in the matcher in a thread safe way.
     */
    private final ThreadAwareContext activationContext = new ThreadAwareContext();

    public ThreadAwareContext getActivationContext() {
        return activationContext;
    }

    /*
     * Names of values added by this mailet to the activation context
     */
    private static final String MAILID_ACTIVATION_CONTENT_KEY = AbstractCipherMailMailet.class.getName() + "#MailID";
    private static final String RECIPIENTS_ACTIVATION_CONTENT_KEY = AbstractCipherMailMailet.class.getName() + "#Recipients";

    /*
     * The log message to show when the Mailet is executed
     */
    private String log;

    /*
     * The logLevel variable defines the level of detail of the logging information to be used by instances of AbstractCipherMailMailet.
    * This parameter can take one of the following values from the LogLevel enum: INFO, WARN, ERROR, DEBUG, TRACE, or OFF.
     * Based on this value, the application will filter out log messages that are less detailed than the specified level.
     */
    private LogLevel logLevel;

    /*
     * If true, the recipients will be logged
     */
    private boolean logRecipients = true;

    /*
     * If true Exceptions are caught.
     */
    private boolean catchExceptions = true;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        LOG                     ("log"),
        LOG_LEVEL               ("logLevel"),
        LOG_RECIPIENTS          ("logRecipients"),
        CATCH_EXCEPTIONS        ("catchExceptions");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    protected abstract Logger getLogger();

    protected LogLevel getLogLevel() {
        return logLevel;
    }

    protected void setLogLevel(LogLevel logLevel) {
        this.logLevel = logLevel;
    }

    protected boolean isCatchExceptions() {
        return catchExceptions;
    }

    protected String getLog() {
        return log;
    }

    protected Integer getIntegerInitParameter(String name, Integer defaultIfEmpty)
    {
        Integer value = defaultIfEmpty;

        String parameter = StringUtils.trimToNull(getInitParameter(name));

        if (parameter != null) {
            value = Integer.parseInt(parameter);
        }

        return value;
    }

    protected Long getLongInitParameter(String name, Long defaultIfEmpty)
    {
        Long value = defaultIfEmpty;

        String parameter = StringUtils.trimToNull(getInitParameter(name));

        if (parameter != null) {
            value = Long.parseLong(parameter);
        }

        return value;
    }

    protected Boolean getBooleanInitParameter(String name, Boolean defaultIfEmpty)
    {
        Boolean value = defaultIfEmpty;

        String parameter = StringUtils.trimToNull(getInitParameter(name));

        if (parameter != null)
        {
            value = Objects.requireNonNull(BooleanUtils.toBooleanObject(parameter),
                    parameter + " is not a valid boolean value.");
        }

        return value;
    }

    @Override
    public final void init()
    throws MessagingException
    {
        log = getInitParameter(Parameter.LOG.name);

        String param = getInitParameter(Parameter.LOG_LEVEL.name);

        if (param != null) {
            logLevel = LogLevel.valueOf(param.trim().toUpperCase());
        }

        if (logLevel == null) {
            logLevel = LogLevel.INFO;
        }

        logRecipients = getBooleanInitParameter(Parameter.LOG_RECIPIENTS.name, logRecipients);
        catchExceptions = getBooleanInitParameter(Parameter.CATCH_EXCEPTIONS.name, catchExceptions);

        StrBuilder sb = new StrBuilder();

        sb.append("catchExceptions: ");
        sb.append(catchExceptions);

        getLogger().info("{}", sb);

        initMailet();
    }

    protected void initMailet()
    throws MessagingException
    {
        // override to add behavior.
    }

    private void logMessage(String message)
    {
        switch (getLogLevel())
        {
            case INFO -> logLogger.info(message);
            case WARN -> logLogger.warn(message);
            case ERROR -> logLogger.error(message);
            case DEBUG -> logLogger.debug(message);
            case TRACE -> logLogger.trace(message);
            case OFF -> { /* do nothing */ }
        }
    }

    protected String createLogLine(String log) {
        return createLogLine(log, false);
    }

    protected String createLogLine(String log, boolean logRecipients)
    {
        return createLogLine(log, activationContext.get(MAILID_ACTIVATION_CONTENT_KEY, String.class),
                logRecipients ? activationContext.get(RECIPIENTS_ACTIVATION_CONTENT_KEY, Collection.class) : null);
    }

    protected String createLogLine(String log, @Nonnull Mail mail, boolean logRecipients) {
        return createLogLine(log, getMailID(mail), logRecipients ? mail.getRecipients() : null);
    }

    protected String createLogLine(String log, String mailID, Collection<?> recipients)
    {
        StrBuilder sb = new StrBuilder();

        sb.append(log);

        if (mailID != null) {
            sb.append("; MailID: ").append(mailID);
        }

        if (recipients != null && !recipients.isEmpty()) {
            sb.append("; Recipients: ").append(recipients);
        }

        return sb.toString();
    }

    protected String getMailID(@Nonnull Mail mail) {
        return CoreApplicationMailAttributes.getMailID(mail);
    }

    @Override
    public final void service(Mail mail)
    {
        try {
            try {
                activationContext.set(MAILID_ACTIVATION_CONTENT_KEY, getMailID(mail));
                activationContext.set(RECIPIENTS_ACTIVATION_CONTENT_KEY, mail.getRecipients());

                String logLine = getLog();

                if (logLine != null) {
                    logMessage(createLogLine(logLine, logRecipients));
                }

                serviceMail(mail);
            }
            finally {
                activationContext.clear();
            }
        }
        catch(Exception e)
        {
            getLogger().error("Unhandled Exception.", e);

            if (!isCatchExceptions()) {
                throw e;
            }
        }
    }

    protected abstract void serviceMail(Mail mail);
}
