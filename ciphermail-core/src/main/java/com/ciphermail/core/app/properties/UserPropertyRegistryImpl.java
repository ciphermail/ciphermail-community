/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import javax.annotation.Nonnull;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class UserPropertyRegistryImpl implements UserPropertyRegistry
{
    private static final Logger logger = LoggerFactory.getLogger(UserPropertyRegistryImpl.class);

    private final PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();

    /*
     * Mapping from property name to property details
     */
    private final Map<String, UserPropertyDescriptor> properties = new HashMap<>();

    /*
     * Mapping from UserPropertiesType to a list of all properties for the properties.
     * This will be used to group all properties for a UserPropertiesType
     */
    private final Map<UserPropertiesType, List<UserPropertyDescriptor>> groupedProperties = new HashMap<>();

    public UserPropertyRegistryImpl(@Nonnull Environment environment, @Nonnull String... packages)
    {
        try {
            for (String basePackage : packages) {
                findUserPropertiesTypes(environment, basePackage);
            }
        }
        catch (IOException | ClassNotFoundException e) {
            throw new UnhandledException(e);
        }
    }

    public UserPropertyRegistryImpl addClasses(@Nonnull UserPropertiesType userPropertiesType,
            @Nonnull Class<?>... classes)
    {
        for (Class<?> clazz : classes) {
            scanClass(userPropertiesType, clazz);
        }

        return this;
    }

    /*
     * Scans packages for classes with a UserPropertiesType annotation
     */
    private void findUserPropertiesTypes(Environment environment, String basePackage)
    throws IOException, ClassNotFoundException
    {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);

        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                                   resolveBasePackage(basePackage) + "/**/*.class";

        Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);

        for (Resource resource : resources)
        {
            if (resource.isReadable())
            {
                MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);

                UserPropertiesType annotation = getUserPropertiesTypeAnnotation(environment, metadataReader);

                if (annotation != null)
                {
                    logger.info("Found UserPropertiesType class {}",
                            metadataReader.getClassMetadata().getClassName());

                    // scan for all getter/setter properties
                    scanClass(annotation, Class.forName(metadataReader.getClassMetadata().getClassName()));
                }
            }
        }
    }

    private String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage));
    }

    /*
     * Returns true if the class has a UserPropertiesType annotation and if a profile is set and if so, whether
     * it matches the active profile. This is needed because some properties should only be available for the gateway
     * and some only for webmail
     */
    private UserPropertiesType getUserPropertiesTypeAnnotation(Environment environment, MetadataReader metadataReader)
    {
        try {
            ClassMetadata classMetadata = metadataReader.getClassMetadata();

            if (!classMetadata.isConcrete()) {
                // Only concrete implementations can be used
                return null;
            }

            Class<?> c = Class.forName(classMetadata.getClassName());

            UserPropertiesType annotation = c.getAnnotation(UserPropertiesType.class);

            // Only accept if the class has a UserPropertiesType annotation
            if (annotation != null)
            {
                // now check if there is a Profile annotation and if so, whether it contains the active profiles
                Profile profile = c.getAnnotation(Profile.class);

                if (profile == null || environment.acceptsProfiles(Profiles.of(profile.value()))) {
                    return annotation;
                }
            }
        }
        catch(Exception e) {
            logger.error("Error checking whether class is-a DefaultPropertyProvider", e);
        }

        return null;
    }

    private void scanClass(UserPropertiesType userPropertiesType, Class<?> beanClass)
    {
        // get all beans methods from the bean
        PropertyDescriptor[] propertyDescriptors = propertyUtilsBean.getPropertyDescriptors(beanClass);

        // if this is a category only class, handle it in a special way
        if (userPropertiesType.type().equals(UserPropertiesType.Type.CATEGORY))
        {
            // special category only type which should not have any properties
            if (propertyDescriptors != null && propertyDescriptors.length > 0) {
                throw new IllegalArgumentException(String.format("Category type for for class %s should not contain " +
                                                                 "any properties", beanClass));
            }

            groupedProperties.put(userPropertiesType, new LinkedList<>());

            return;
        }

        if (propertyDescriptors == null || propertyDescriptors.length == 0) {
            throw new IllegalArgumentException(String.format("PropertyDescriptor's for class %s not found", beanClass));
        }

        // check whether the bean methods are annotated with the UserProperty annotation because only annotated methods
        // are treated as property methods
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors)
        {
            Method readMethod = propertyDescriptor.getReadMethod();

            if (readMethod != null)
            {
                // check if method contains the UserProperty annotation
                UserProperty getterProperty = MethodUtils.getAnnotation(readMethod,
                        UserProperty.class, false, false);

                if (getterProperty != null)
                {
                    if (properties.containsKey(getterProperty.name())) {
                        throw new IllegalArgumentException(
                                String.format("There is already a property named %s (%s)",
                                getterProperty.name(), readMethod));
                    }

                    // now check if the setter is also annotated
                    UserProperty setterProperty = null;

                    Method writeMethod = propertyDescriptor.getWriteMethod();

                    if (writeMethod != null) {
                        // check if method contains the UserProperty annotation
                        setterProperty = MethodUtils.getAnnotation(writeMethod,
                                UserProperty.class, false, false);

                        if (setterProperty == null) {
                            throw new IllegalArgumentException(String.format(
                                    "The setter does not contain a %s annotation (%s)",
                                    UserProperty.class.getSimpleName(), writeMethod));
                        }

                        // the property name of the getter/setter should match
                        if (!getterProperty.name().equals(setterProperty.name())) {
                            throw new IllegalArgumentException(String.format(
                                    "Name of the setter (%s) does not match the name of the getter (%s)",
                                    writeMethod, readMethod));
                        }

                        // the display name key of the getter/setter should match
                        if (!getterProperty.displayNameKey().equals(setterProperty.displayNameKey())) {
                            throw new IllegalArgumentException(String.format(
                                "displayNameKey of the setter (%s) does not match the displayNameKey of the getter (%s)",
                                writeMethod, readMethod));
                        }

                        // the description key of the getter/setter should match
                        if (!getterProperty.descriptionKey().equals(setterProperty.descriptionKey())) {
                            throw new IllegalArgumentException(String.format(
                                "descriptionKey of the setter (%s) does not match the descriptionKey of the getter (%s)",
                                writeMethod, readMethod));
                        }
                    }

                    logger.info("Adding property {}", getterProperty.name());

                    UserPropertyDescriptor userPropertyDescriptor = new UserPropertyDescriptor(getterProperty,
                            setterProperty, propertyDescriptor, userPropertiesType);

                    properties.put(getterProperty.name(), userPropertyDescriptor);

                    groupedProperties.computeIfAbsent(userPropertiesType, k -> new LinkedList<>())
                            .add(userPropertyDescriptor);
                }
            }
        }

        // find all methods with a UserProperty annotation and check if they are part of a property
        // if not, it would indicate that the methods were not proper bean methods or not accessible
        for (Method method : MethodUtils.getMethodsListWithAnnotation(beanClass, UserProperty.class,
                false, true))
        {
            UserProperty property = MethodUtils.getAnnotation(method, UserProperty.class,
                    false,true);

            if (property == null) {
                throw new IllegalArgumentException(String.format("Unable to get annotation for method %s of class %s",
                        method, beanClass));
            }

            // check if the property is a registered property
            UserPropertyDescriptor existingProperty = properties.get(property.name());

            if (existingProperty == null) {
                throw new IllegalArgumentException(String.format(
                        "Method %s of class %s is not a valid public bean method or is only a setter property",
                        method, beanClass));
            }
            else {
                // check if the method is an already registered read or write method
                if (!method.equals(existingProperty.propertyDescriptor().getReadMethod()) &&
                    !method.equals(existingProperty.propertyDescriptor().getWriteMethod()))
                {
                    throw new IllegalArgumentException(String.format(
                            "Method %s of class %s a contains a duplicate property", method, beanClass));
                }
            }
        }
    }

    @Override
    public Map<String, UserPropertyDescriptor> getProperties() {
        return properties;
    }

    @Override
    public Map<UserPropertiesType, List<UserPropertyDescriptor>> getGroupedProperties() {
        return groupedProperties;
    }
}
