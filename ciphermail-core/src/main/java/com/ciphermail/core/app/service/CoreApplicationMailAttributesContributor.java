/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.service;

import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.SimpleHash;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.common.template.TemplateHashModelBuilderContributor;
import com.ciphermail.core.common.template.TemplateUtils;
import org.apache.commons.lang.UnhandledException;

import javax.annotation.Nonnull;

/**
 * Implementation of {@link} TemplateHashModelBuilderContributor} which adds a static reference to
 * {@link CoreApplicationMailAttributes} under the key "mailAttributes"
 */
public class CoreApplicationMailAttributesContributor implements TemplateHashModelBuilderContributor
{
    /*
     * Key under which the CoreApplicationMailAttributes will be registered in the SimpleHash
     */
    private static final String CORE_APPLICATION_MAIL_ATTRIBUTES_PROPERTY_NAME = "mailAttributes";

    /*
     * Reference to CoreApplicationMailAttributes
     */
    private final TemplateModel templateModel;

    public CoreApplicationMailAttributesContributor()
    {
        try {
            BeansWrapperBuilder beansWrapperBuilder = new BeansWrapperBuilder(TemplateUtils.getFreemarkerVersion());

            templateModel = beansWrapperBuilder.build().getStaticModels().get(
                    CoreApplicationMailAttributes.class.getName());
        }
        catch (TemplateModelException e) {
            throw new UnhandledException(e);
        }
    }

    @Override
    public void contribute(@Nonnull SimpleHash simpleHash) {
        simpleHash.put(CORE_APPLICATION_MAIL_ATTRIBUTES_PROPERTY_NAME, templateModel);
    }
}
