/*
 * Copyright (c) 2018-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.ParseException;

/**
 * Mailet which changes the sender and from to a "static" email address to make it safe for spf and DMARC. The
 * from is rewritten to "in name of original@from.com" and if there is no Reply-To header, a Reply-To header will
 * be added containing the original from.
 */
public class SafeSenderRewrite extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SafeSenderRewrite.class);

    /*
     * The email address for the null sender
     */
    private static final String NULL_SENDER_EMAIL = "<>";

    /*
     * The new sender if the Mail attribute does not contain the sender
     */
    private String newSender;

    /*
     * The new sender is read from this Mail attribute
     */
    private String senderAttribute;

    /*
     * The value for rewrite enabled if the Mail attribute does not contain the enable value
     */
    private boolean enableRewrite;

    /*
     * The enable value is read from this Mail attribute
     */
    private String enableRewriteAttribute;

    /*
     * The template to use for creating the personal part of the rewritten from
     */
    private String personalTemplate = "in name of %s";

    /*
     * The personal template value is read from this Mail attribute
     */
    private String personalTemplateAttribute;

    /*
     * If set, a Reply-To header is added if there is no Reply-To header yet
     */
    private boolean addReplyTo = true;

    /*
     * The addReplyTo value is read from this Mail attribute
     */
    private String addReplyToAttribute;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        SENDER                      ("sender"),
        SENDER_ATTRIBUTE            ("senderAttribute"),
        ENABLE                      ("enable"),
        ENABLE_ATTRIBUTE            ("enableAttribute"),
        PERSONAL_TEMPLATE           ("personalTemplate"),
        PERSONAL_TEMPLATE_ATTRIBUTE ("personalTemplateAttribute"),
        ADD_REPLY_TO                ("addReplyTo"),
        ADD_REPLY_TO_ATTRIBUTE      ("addReplyToAttribute");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void initMailet()
    throws MessagingException
    {
        newSender = StringUtils.trimToNull(getInitParameter(Parameter.SENDER.name));
        senderAttribute = StringUtils.trimToNull(getInitParameter(Parameter.SENDER_ATTRIBUTE.name));

        enableRewrite = getBooleanInitParameter(Parameter.ENABLE.name, enableRewrite);
        enableRewriteAttribute = StringUtils.trimToNull(getInitParameter(Parameter.ENABLE_ATTRIBUTE.name));

        personalTemplate = getInitParameter(Parameter.PERSONAL_TEMPLATE.name, personalTemplate);
        personalTemplateAttribute = StringUtils.trimToNull(getInitParameter(Parameter.PERSONAL_TEMPLATE_ATTRIBUTE.name));

        addReplyTo = getBooleanInitParameter(Parameter.ADD_REPLY_TO.name, addReplyTo);
        addReplyToAttribute = StringUtils.trimToNull(getInitParameter(Parameter.ADD_REPLY_TO_ATTRIBUTE.name));
    }

    private String getNewSender(Mail mail)
    {
        String localNewSender = null;

        if (senderAttribute != null)
        {
            localNewSender = StringUtils.trimToNull(MailAttributesUtils.getManagedAttributeValue(
                    mail, senderAttribute, String.class).orElse(null));
        }

        if (localNewSender == null) {
            localNewSender = this.newSender;
        }

        return localNewSender;
    }

    private boolean isNullSender(String email) {
        return email == null || NULL_SENDER_EMAIL.equals(email);
    }

    private MailAddress toMailAddress(String email)
    {
        if (isNullSender(email)) {
            return null;
        }

        MailAddress result = null;

        try {
            result = new MailAddress(email);
        }
        catch (ParseException e) {
            logger.error("Email address {} is not valid", email, e);
        }

        return result;
    }

    private boolean isRewriteEnabled(Mail mail)
    {
        return enableRewriteAttribute != null ? MailAttributesUtils.attributeAsBoolean(
                mail, enableRewriteAttribute, enableRewrite) : enableRewrite;
    }

    private String getPersonalTemplate(Mail mail)
    {
        return personalTemplateAttribute != null ? StringUtils.defaultString(MailAttributesUtils.getManagedAttributeValue(
                mail, personalTemplateAttribute, String.class).orElse(personalTemplate)) : personalTemplate;
    }

    private boolean isAddReplyTo(Mail mail)
    {
        return addReplyToAttribute != null ? MailAttributesUtils.attributeAsBoolean(
                mail, addReplyToAttribute, addReplyTo) : addReplyTo;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        if (isRewriteEnabled(mail))
        {
            String localNewSender = getNewSender(mail);

            if (localNewSender != null)
            {
                logger.debug("Replacing sender '{}' by '{}'", mail.getMaybeSender() , localNewSender);

                // We can only change the sender if the mail implementation is MailImpl. In practice, this is always
                // the case.
                if (mail instanceof MailImpl mailImpl)
                {
                    mailImpl.setSender(toMailAddress(localNewSender));

                    try {
                        MimeMessage sourceMessage = mail.getMessage();

                        String originalFrom = EmailAddressUtils.getEmailAddress(EmailAddressUtils.getAddress(
                                EmailAddressUtils.getFromQuietly(sourceMessage)));

                        if (originalFrom != null)
                        {
                                // We need to rewrite from and optionally set a Reply-To header. We need to do this on a
                                // cloned MimeMessage for safety reasons because the MimeMessage might be an invalid email
                                MimeMessage clone = MailUtils.cloneMessageWithFixedMessageID(sourceMessage);

                                // If newSender is null, use "in name of ..." <> for the from
                                String newFrom = !isNullSender(localNewSender) ?
                                    new InternetAddress(localNewSender, String.format(getPersonalTemplate(mail), originalFrom))
                                            .toString()
                                    : "\"" + MimeUtility.encodeWord(String.format(getPersonalTemplate(mail), originalFrom))
                                            + "\" <>";

                                clone.setHeader("From", newFrom);

                                // Add a Reply - To header if there is no Reply-To header yet. We want the reply to go to the
                                // correct email address
                                if (isAddReplyTo(mail) && clone.getHeader("Reply-To") == null) {
                                    clone.setHeader("Reply-To", originalFrom);
                                }

                                clone.saveChanges();

                                MailUtils.validateMessage(clone);

                                mail.setMessage(clone);
                        }
                    }
                    catch (Exception e) {
                        logger.warn("Error replacing From or adding Reply-To headers", e);
                    }
                }
                else {
                    logger.warn("Unable to rewrite sender. Mail is not a MailImpl but a {}", mail.getClass());
                }
            }
            else {
                logger.debug("New sender not set. Sender is not rewritten.");
            }
        }
        else {
            logger.debug("Sender rewrite is not enabled");
        }
    }
}
