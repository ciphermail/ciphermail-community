/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.common.jackson.JacksonUtil;
import org.apache.mailet.Mail;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Helper class for storing PGP Public keys into a James {@link Mail} object as a JSON encoded string
 */
public class PGPPublicKeys
{
    /*
     * The public keys which will de serialized
     */
    private LinkedList<PGPPublicKey> keys;

    public PGPPublicKeys() {
        this.keys = new LinkedList<>();
    }

    public PGPPublicKeys(Collection<PGPPublicKey> publicKeys) {
        setPublicKeys(publicKeys);
    }

    public List<PGPPublicKey> getPublicKeys() {
        return keys;
    }

    public void setPublicKeys(Collection<PGPPublicKey> publicKeys) {
        this.keys = publicKeys != null ? new LinkedList<>(publicKeys) : new LinkedList<>();
    }

    /**
     * Serializes PGPPublicKeys to JSON
     */
    public static String toJSON(PGPPublicKeys publicKeys)
    throws IOException
    {
        Objects.requireNonNull(publicKeys);

        List<byte[]> encodedKeys = new ArrayList<>(publicKeys.keys.size());

        for (PGPPublicKey publicKey : publicKeys.keys) {
            encodedKeys.add(publicKey.getEncoded());
        }

        return JacksonUtil.getObjectMapper().writeValueAsString(encodedKeys);
    }

    /**
     * Deserializes JSON to PGPPublicKeys
     */
    public static PGPPublicKeys fromJSON(@Nonnull String json)
    throws IOException
    {
        Objects.requireNonNull(json);

        List<byte[]> encodedKeys = JacksonUtil.getObjectMapper().readValue(json, new TypeReference<List<byte[]>>(){});

        List<PGPPublicKey> keys = new ArrayList<>(encodedKeys.size());

        for (byte[] encodedKey : encodedKeys) {
            keys.add(PGPKeyUtils.decodePublicKey(encodedKey));
        }

        return new PGPPublicKeys(keys);
    }
}
