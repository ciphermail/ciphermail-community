/*
 * Copyright (c) 2009-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.properties.SMSProperties;
import com.ciphermail.core.app.properties.SMSPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.PhoneNumberUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Matcher that can detect a telephone number in the subject. If a telephone number is found
 * the telephone number of the recipient is set to the subject telephone number. if there are
 * multiple recipient and the subject contains a telephone number the user telephone number
 * property is not set because it's impossible to match the number with the correct recipient.
 * <p>
 * The telephone number is only detected if the subject ends with the telephone number (unless
 * the default telephone regular expression has been overridden).
 * <p>
 * Examples of valid subjects with a telephone number:
 * <p>
 * "test 123456"
 * "other +1234567   "
 * "hi! +(800)12(34)56"
 * <p>
 * Examples of invalid subjects with a telephone number:
 * <p>
 * "some subject"
 * "Hi 123"
 * "Hi 12345678 some text"
 *
 * @author Martijn Brinkers
 *
 */
@SuppressWarnings("java:S6813")
public class SubjectPhoneNumber extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(SubjectPhoneNumber.class);

    /*
     * Default expression used to detect a phone number in the subject. This only matches on
     * lines where the line ends with the telehpone number.
     *
     * Matching examples:
     *
     * "test 123456"
     * "other +1234567   "
     * "hi! +(800)12(34)56"
     *
     * Non matching examples:
     *
     * "some subject"
     * "Hi 123"
     * "Hi 12345678 some text"
     */
    private static final String DEFAULT_PHONE_NUMBER_EXPRESSION = "(\\+|\\s|^)([\\s\\-()]*\\d){6,25}\\s*$";

    /*
     * Pattern used to remove all non numbers
     */
    private static final Pattern phoneCleanPattern = Pattern.compile("[^0-9]*");

    /*
     * The pattern which is matched against the subject
     */
    private Pattern phoneNumberPattern;

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    private String getSubjectExpression()
    {
        String expression = getCondition();

        if (StringUtils.isBlank(expression)) {
            expression = DEFAULT_PHONE_NUMBER_EXPRESSION;
        }

        return expression;
    }

    @Override
    public void init()
    {
        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(transactionOperations);

        String expression = StringUtils.trimToNull(getSubjectExpression());

        if (expression == null) {
            throw new IllegalArgumentException("Expression is missing.");
        }

        phoneNumberPattern = Pattern.compile(expression);

        StrBuilder sb = new StrBuilder();

        sb.append("phoneNumberPattern: ");
        sb.append(phoneNumberPattern);

        getLogger().info("{}", sb);
    }

    private SMSProperties createSMSProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMSPropertiesImpl.class)
                .createInstance(properties);
    }

    private void setRecipientPhoneNumber(@Nonnull MailAddress recipient, String phoneNumber)
    throws AddressException, HierarchicalPropertiesException
    {
        String email = EmailAddressUtils.canonicalizeAndValidate(recipient.toString(), true);

        if (email == null)
        {
            // Should not happen because MailAddress should always be a valid email address
            getLogger().warn("{} is not a valid email address.", recipient);

            return;
        }

        User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

        createSMSProperties(user.getUserPreferences().getProperties()).setSMSPhoneNumber(phoneNumber);

        userWorkflow.makePersistent(user);
    }

    private String cleanPhoneNumber(String phoneNumber, String defaultCountryCode)
    {
        if (StringUtils.isBlank(phoneNumber)) {
            return null;
        }

        // We will assume that the regular expression used to match the phone number only matched
        // valid phone numbers. We will remove all non number characters
        phoneNumber = phoneCleanPattern.matcher(phoneNumber).replaceAll("");

        // If a phone number starts with 0 we will add the default country code
        phoneNumber = PhoneNumberUtils.addCountryCode(phoneNumber, defaultCountryCode);

        phoneNumber = PhoneNumberUtils.normalizeAndValidatePhoneNumber(phoneNumber);

        return phoneNumber;
    }

    private boolean addPhoneNumberTransacted(Mail mail, MailAddress recipient, String phoneNumber)
    throws MessagingException, HierarchicalPropertiesException
    {
        String defaultCountryCode = null;

        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        User user = userWorkflow.getUser(originator.getAddress(), UserNotExistResult.DUMMY_IF_NOT_EXIST);

        if (user != null) {
            defaultCountryCode = createSMSProperties(user.getUserPreferences().getProperties())
                    .getPhoneDefaultCountryCode();
        }

        phoneNumber = cleanPhoneNumber(phoneNumber, defaultCountryCode);

        boolean added = false;

        if (StringUtils.isNotEmpty(phoneNumber))
        {
            setRecipientPhoneNumber(recipient, phoneNumber);

            added = true;
        }
        else {
            getLogger().warn("Phone number is missing.");
        }

        return added;
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        String phoneNumber = null;

        MimeMessage message = mail.getMessage();

        // We need to check whether the original subject is stored in the mail attributes because
        // the check for the telephone number should be done on the original subject. The reason
        // for this is that the current subject can result in the detection of an incorrect
        // telephone number because the subject was changed because the subject trigger was
        // removed.
        //
        // Example:
        //
        // original subject is: "test 123 [encrypt] 123456". The subject is: "test 123 123456" after
        // the trigger has been removed. The telephone nr 123123456 is detected instead of 123456.
        //
        // See bug report: https://jira.djigzo.com/browse/GATEWAY-11
        //
        String originalSubject = CoreApplicationMailAttributes.getOriginalSubject(mail);

        if (originalSubject == null) {
            originalSubject = MailUtils.getSafeSubject(message);
        }

        if (StringUtils.isNotBlank(originalSubject))
        {
            Matcher matcher = phoneNumberPattern.matcher(originalSubject);

            if (matcher.find())
            {
                phoneNumber = matcher.group();

                // Remove the match and set the subject without the number.
                //
                // Note: the match should be removed from the current subject!!
                //
                // Note2: using substringBeforeLast is only correct if the pattern matches the end
                // of the subject (which is the default). If the pattern matches something in the
                // middle, substringBeforeLast should not be used.
                message.setSubject(StringUtils.substringBeforeLast(MailUtils.getSafeSubject(message), phoneNumber));
            }
        }

        List<MailAddress> matchingRecipients = null;

        if (StringUtils.isNotBlank(phoneNumber))
        {
            matchingRecipients = MailAddressUtils.getRecipients(mail);

            int nrOfRecipients = CollectionUtils.getSize(matchingRecipients);

            if (nrOfRecipients == 1)
            {
                MailAddress matchingRecipient = matchingRecipients.get(0);
                String foundPhoneNumber = phoneNumber;

                try {
                    boolean added = Boolean.TRUE.equals(transactionOperations.execute(status ->
                    {
                        try {
                            return addPhoneNumberTransacted(mail, matchingRecipient, foundPhoneNumber);
                        }
                        catch (MessagingException | HierarchicalPropertiesException e) {
                            throw new UnhandledException(e);
                        }
                    }));

                    if (!added) {
                        // addPhoneNumbers has more thorough checks to see if the phone
                        // number is valid and if not, there will be no match
                        matchingRecipients = Collections.emptyList();
                    }
                }
                catch (UnhandledException e) {
                    throw new MessagingException("Exception adding phone numbers.", e);
                }
            }
            else {
                // A telephone number was found but, we cannot add the number to a users account
                // because we do not know to which recipient the number belongs. We will however
                // return all the recipients.
                getLogger().warn("Found {} recipients but only one is supported.", nrOfRecipients);
            }
        }

        return matchingRecipients != null ? matchingRecipients : Collections.emptyList();
    }
}
