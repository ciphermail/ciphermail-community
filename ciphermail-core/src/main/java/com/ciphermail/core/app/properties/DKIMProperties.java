/*
 * Copyright (c) 2011-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

import javax.annotation.Nonnull;

/**
 * Java interface wrapper around hierarchical properties for the DKIM properties
 *
 * @author Martijn Brinkers
 *
 */
public interface DKIMProperties
{
    /**
     * The DKIM keypair which signs system generated emails. The keypair should be a PEM encoded PKCS#8 keypair.
     * <p>
     * Note: this key is not used for "normal" DKIM signing
     */
    void setSystemKeyPair(String keyPair)
    throws HierarchicalPropertiesException;

    String getSystemKeyPair()
    throws HierarchicalPropertiesException;

    /**
     * If true, outgoing email is DKIM signed (assuming a DKIM key is available)
     */
    void setSigningEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSigningEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The identifier under which the DKIM key is stored
     * <p>
     * Note: this is a different DKIM key than the system key pair. This key is stored in the DKIM key store. The key id
     * is used as the base name for the key alias. The key alias for the key is set to "DKIM:$keyId" where $keyId is
     * replaced by the value of keyid
     */
    void setKeyIdentifier(String keyId)
    throws HierarchicalPropertiesException;

    String getKeyIdentifier()
    throws HierarchicalPropertiesException;

    /**
     * Sets the template which is used for the DKIM header
     */
    void setSignatureTemplate(String template)
    throws HierarchicalPropertiesException;

    String getSignatureTemplate()
    throws HierarchicalPropertiesException;
}
