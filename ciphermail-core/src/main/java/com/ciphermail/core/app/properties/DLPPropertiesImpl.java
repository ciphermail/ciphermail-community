/*
 * Copyright (c) 2010-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;

import javax.annotation.Nonnull;

/**
 * Implementation of DLPProperties.
 *
 * @author Martijn Brinkers
 *
 */
@UserPropertiesType(name = "dlp", displayName = "DLP", order = 100)
public class DLPPropertiesImpl extends DelegatedHierarchicalProperties implements DLPProperties
{
    private static final String ENABLED                             = "dlp-enabled";
    private static final String DLP_MANAGERS                        = "dlp-dlp-managers";
    private static final String SEND_WARNING_TO_ORIGINATOR          = "dlp-send-warning-to-originator";
    private static final String SEND_WARNING_TO_DLP_MANAGERS        = "dlp-send-warning-to-dlp-managers";
    private static final String SEND_QUARANTINE_TO_ORIGINATOR       = "dlp-send-quarantine-to-originator";
    private static final String SEND_QUARANTINE_TO_DLP_MANAGERS     = "dlp-send-quarantine-to-dlp-managers";
    private static final String SEND_BLOCK_TO_ORIGINATOR            = "dlp-send-block-to-originator";
    private static final String SEND_BLOCK_TO_DLP_MANAGERS          = "dlp-send-block-to-dlp-managers";
    private static final String SEND_ERROR_TO_ORIGINATOR            = "dlp-send-error-to-originator";
    private static final String SEND_ERROR_TO_DLP_MANAGERS          = "dlp-send-error-to-dlp-managers";
    private static final String QUARANTINE_ON_ERROR                 = "dlp-quarantine-on-error";
    private static final String QUARANTINE_ON_FAILED_ENCRYPTION     = "dlp-quarantine-on-failed-encryption";
    private static final String SEND_RELEASE_NOTIFY_TO_ORIGINATOR   = "dlp-send-release-notify-to-originator";
    private static final String SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS = "dlp-send-release-notify-to-dlp-managers";
    private static final String SEND_DELETE_NOTIFY_TO_ORIGINATOR    = "dlp-send-delete-notify-to-originator";
    private static final String SEND_DELETE_NOTIFY_TO_DLP_MANAGERS  = "dlp-send-delete-notify-to-dlp-managers";
    private static final String SEND_EXPIRE_NOTIFY_TO_ORIGINATOR    = "dlp-send-expire-notify-to-originator";
    private static final String SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS  = "dlp-send-expire-notify-to-dlp-managers";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public DLPPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(name = ENABLED)
    public void setEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, ENABLED, enabled);
    }

    @Override
    @UserProperty(name = ENABLED,
            order = 10
    )
    public @Nonnull Boolean getEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, ENABLED);
    }

    @Override
    @UserProperty(
            name = DLP_MANAGERS
    )
    public void setDLPManagers(String recipients)
    throws HierarchicalPropertiesException
    {
        setProperty(DLP_MANAGERS, UserPropertiesValidators.validateEmailList(DLP_MANAGERS, recipients));
    }

    @Override
    @UserProperty(name = DLP_MANAGERS,
            order = 30,
            allowNull = true)
    public String getDLPManagers()
    throws HierarchicalPropertiesException
    {
        return getProperty(DLP_MANAGERS);
    }

    @Override
    @UserProperty(name = SEND_WARNING_TO_ORIGINATOR)
    public void setSendWarningToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_WARNING_TO_ORIGINATOR, enabled);
    }

    @Override
    @UserProperty(name = SEND_WARNING_TO_ORIGINATOR,
            order = 40
    )
    public @Nonnull Boolean getSendWarningToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_WARNING_TO_ORIGINATOR);
    }

    @Override
    @UserProperty(name = SEND_WARNING_TO_DLP_MANAGERS)
    public void setSendWarningToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_WARNING_TO_DLP_MANAGERS, enabled);
    }

    @Override
    @UserProperty(name = SEND_WARNING_TO_DLP_MANAGERS,
            order = 50
    )
    public @Nonnull Boolean getSendWarningToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_WARNING_TO_DLP_MANAGERS);
    }

    @Override
    @UserProperty(name = SEND_QUARANTINE_TO_ORIGINATOR)
    public void setSendQuarantineToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_QUARANTINE_TO_ORIGINATOR, enabled);
    }

    @Override
    @UserProperty(name = SEND_QUARANTINE_TO_ORIGINATOR,
            order = 60)
    public @Nonnull Boolean getSendQuarantineToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_QUARANTINE_TO_ORIGINATOR);
    }

    @Override
    @UserProperty(name = SEND_QUARANTINE_TO_DLP_MANAGERS)
    public void setSendQuarantineToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_QUARANTINE_TO_DLP_MANAGERS, enabled);
    }

    @Override
    @UserProperty(name = SEND_QUARANTINE_TO_DLP_MANAGERS,
            order = 70
    )
    public @Nonnull Boolean getSendQuarantineToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_QUARANTINE_TO_DLP_MANAGERS);
    }

    @Override
    @UserProperty(name = SEND_BLOCK_TO_ORIGINATOR)
    public void setSendBlockToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_BLOCK_TO_ORIGINATOR, enabled);
    }

    @Override
    @UserProperty(name = SEND_BLOCK_TO_ORIGINATOR,
            order = 80
    )
    public @Nonnull Boolean getSendBlockToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_BLOCK_TO_ORIGINATOR);
    }

    @Override
    @UserProperty(name = SEND_BLOCK_TO_DLP_MANAGERS)
    public void setSendBlockToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_BLOCK_TO_DLP_MANAGERS, enabled);
    }

    @Override
    @UserProperty(name = SEND_BLOCK_TO_DLP_MANAGERS,
            order = 90
    )
    public @Nonnull Boolean getSendBlockToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_BLOCK_TO_DLP_MANAGERS);
    }

    @Override
    @UserProperty(name = SEND_ERROR_TO_ORIGINATOR)
    public void setSendErrorToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_ERROR_TO_ORIGINATOR, enabled);
    }

    @Override
    @UserProperty(name = SEND_ERROR_TO_ORIGINATOR,
            order = 100
    )
    public @Nonnull Boolean getSendErrorToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_ERROR_TO_ORIGINATOR);
    }

    @Override
    @UserProperty(name = SEND_ERROR_TO_DLP_MANAGERS)
    public void setSendErrorToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_ERROR_TO_DLP_MANAGERS, enabled);
    }

    @Override
    @UserProperty(name = SEND_ERROR_TO_DLP_MANAGERS,
            order = 110
    )
    public @Nonnull Boolean getSendErrorToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_ERROR_TO_DLP_MANAGERS);
    }

    @Override
    @UserProperty(name = QUARANTINE_ON_ERROR)
    public void setQuarantineOnError(Boolean quarantine)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, QUARANTINE_ON_ERROR, quarantine);
    }

    @Override
    @UserProperty(name = QUARANTINE_ON_ERROR,
            order = 120
    )
    public @Nonnull Boolean getQuarantineOnError()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, QUARANTINE_ON_ERROR);
    }

    @Override
    @UserProperty(name = QUARANTINE_ON_FAILED_ENCRYPTION)
    public void setQuarantineOnFailedEncryption(Boolean quarantine)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, QUARANTINE_ON_FAILED_ENCRYPTION, quarantine);
    }

    @Override
    @UserProperty(name = QUARANTINE_ON_FAILED_ENCRYPTION,
            order = 130
    )
    public @Nonnull Boolean getQuarantineOnFailedEncryption()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, QUARANTINE_ON_FAILED_ENCRYPTION);
    }

    @Override
    @UserProperty(name = SEND_RELEASE_NOTIFY_TO_ORIGINATOR)
    public void setSendReleaseNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_RELEASE_NOTIFY_TO_ORIGINATOR, enabled);
    }

    @Override
    @UserProperty(name = SEND_RELEASE_NOTIFY_TO_ORIGINATOR,
            order = 140
    )
    public @Nonnull Boolean getSendReleaseNotifyToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_RELEASE_NOTIFY_TO_ORIGINATOR);
    }

    @Override
    @UserProperty(name = SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS)
    public void setSendReleaseNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS, enabled);
    }

    @Override
    @UserProperty(name = SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS,
            order = 150
    )
    public @Nonnull Boolean getSendReleaseNotifyToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_RELEASE_NOTIFY_TO_DLP_MANAGERS);
    }

    @Override
    @UserProperty(name = SEND_DELETE_NOTIFY_TO_ORIGINATOR)
    public void setSendDeleteNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_DELETE_NOTIFY_TO_ORIGINATOR, enabled);
    }

    @Override
    @UserProperty(name = SEND_DELETE_NOTIFY_TO_ORIGINATOR,
            order = 160
    )
    public @Nonnull Boolean getSendDeleteNotifyToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_DELETE_NOTIFY_TO_ORIGINATOR);
    }

    @Override
    @UserProperty(name = SEND_DELETE_NOTIFY_TO_DLP_MANAGERS)
    public void setSendDeleteNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_DELETE_NOTIFY_TO_DLP_MANAGERS, enabled);
    }

    @Override
    @UserProperty(name = SEND_DELETE_NOTIFY_TO_DLP_MANAGERS,
            order = 170
    )
    public @Nonnull Boolean getSendDeleteNotifyToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_DELETE_NOTIFY_TO_DLP_MANAGERS);
    }

    @Override
    @UserProperty(name = SEND_EXPIRE_NOTIFY_TO_ORIGINATOR)
    public void setSendExpireNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_EXPIRE_NOTIFY_TO_ORIGINATOR, enabled);
    }

    @Override
    @UserProperty(name = SEND_EXPIRE_NOTIFY_TO_ORIGINATOR,
            order = 180
    )
    public @Nonnull Boolean getSendExpireNotifyToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_EXPIRE_NOTIFY_TO_ORIGINATOR);
    }

    @Override
    @UserProperty(name = SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS)
    public void setSendExpireNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS, enabled);
    }

    @Override
    @UserProperty(name = SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS,
            order = 190
    )
    public @Nonnull Boolean getSendExpireNotifyToDLPManagers()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_EXPIRE_NOTIFY_TO_DLP_MANAGERS);
    }
}
