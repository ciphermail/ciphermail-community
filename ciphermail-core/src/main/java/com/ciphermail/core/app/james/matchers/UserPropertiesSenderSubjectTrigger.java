/*
 * Copyright (c) 2015-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.JSONUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * Implementation of the abstract class AbstractSenderSubjectTrigger which reads the values for TriggerDetails from
 * the sending user properties
 */
public class UserPropertiesSenderSubjectTrigger extends AbstractSenderSubjectTrigger
{
    private static final Logger logger = LoggerFactory.getLogger(UserPropertiesSenderSubjectTrigger.class);

    /*
     * JSON property names
     */
    protected static final String JSON_TRIGGER_PROPERTY_NAME_PARAM = "triggerProperty";
    protected static final String JSON_REMOVE_PATTERN_PROPERTY_NAME_PARAM = "removePatternProperty";
    protected static final String JSON_ENABLED_PROPERTY_NAME_PARAM = "enabledProperty";
    protected static final String JSON_LOG_ON_MATCH_PARAM = "logOnMatch";
    protected static final String JSON_LOG_ON_NO_MATCH_PARAM = "logOnNoMatch";
    protected static final String JSON_LOG_ON_DISABLED_PARAM = "logOnDisabled";

    /*
     * User property to read trigger from
     */
    private String triggerProperty;

    /*
     * User property to read trigger from
     */
    private String removePatternProperty;

    /*
     * User property to read the enabled property from
     */
    private String enabledProperty;

    /*
     * The string to log when there is a match
     */
    private String logOnMatch;

    /*
     * The string to log when there is a no match
     */
    private String logOnNoMatch;

    /*
     * The string to log when the trigger is not enabled
     */
    private String logOnDisabled;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    protected String getJSONInitString() {
        return getCondition();
    }

    protected String[] getSupportedNamedParameters()
    {
        return new String[] {
                JSON_TRIGGER_PROPERTY_NAME_PARAM,
                JSON_REMOVE_PATTERN_PROPERTY_NAME_PARAM,
                JSON_ENABLED_PROPERTY_NAME_PARAM,
                JSON_LOG_ON_MATCH_PARAM,
                JSON_LOG_ON_NO_MATCH_PARAM,
                JSON_LOG_ON_DISABLED_PARAM};
    }

    protected void checkSupportedNamedParameters(JSONObject parameters)
    throws JSONException
    {
        JSONUtils.checkSupportedNamedParameters(parameters, getSupportedNamedParameters());
    }

    protected void init(JSONObject parameters)
    {
        triggerProperty = StringUtils.trimToNull(parameters.optString(JSON_TRIGGER_PROPERTY_NAME_PARAM));
        removePatternProperty = StringUtils.trimToNull(parameters.optString(JSON_REMOVE_PATTERN_PROPERTY_NAME_PARAM));
        enabledProperty = StringUtils.trimToNull(parameters.optString(JSON_ENABLED_PROPERTY_NAME_PARAM));
        logOnMatch = StringUtils.trimToNull(parameters.optString(JSON_LOG_ON_MATCH_PARAM));
        logOnNoMatch = StringUtils.trimToNull(parameters.optString(JSON_LOG_ON_NO_MATCH_PARAM));
        logOnDisabled = StringUtils.trimToNull(parameters.optString(JSON_LOG_ON_DISABLED_PARAM));
    }

    @Override
    public void init()
    {
        super.init();

        String condition = StringUtils.trimToNull(getJSONInitString());

        if (condition != null)
        {
            try {
                JSONObject parameters = new JSONObject(condition);

                checkSupportedNamedParameters(parameters);

                init(parameters);
            }
            catch (JSONException e) {
                throw new IllegalArgumentException("Illegal JSON value", e);
            }
        }
    }

    @Override
    protected void logOnMatch(Mail mail)
    {
        if (logOnMatch != null)
        {
            String mailID = null;

            if (mail != null) {
                mailID = CoreApplicationMailAttributes.getMailID(mail);
            }

            getLogger().info(logOnMatch, mailID);
        }
        else {
            super.logOnMatch(mail);
        }
    }

    @Override
    protected void logOnNoMatch(Mail mail)
    {
        if (logOnNoMatch != null)
        {
            String mailID = null;

            if (mail != null) {
                mailID = CoreApplicationMailAttributes.getMailID(mail);
            }

            getLogger().info(logOnNoMatch, mailID);
        }
        else {
            super.logOnNoMatch(mail);
        }
    }

    @Override
    protected void logOnDisabled(Mail mail)
    {
        if (logOnDisabled != null)
        {
            String mailID = null;

            if (mail != null) {
                mailID = CoreApplicationMailAttributes.getMailID(mail);
            }

            getLogger().info(logOnDisabled, mailID);
        }
        else {
            super.logOnDisabled(mail);
        }
    }

    protected String getDefaultSubjectTrigger() {
        return null;
    }

    protected boolean getDefaultRemovePattern() {
        return false;
    }

    protected boolean getDefaultEnabled() {
        return false;
    }

    @Override
    protected TriggerDetails getTriggerDetails(@Nonnull User user)
    throws HierarchicalPropertiesException
    {
        String subjectTrigger = getDefaultSubjectTrigger();
        boolean removePattern = getDefaultRemovePattern();
        boolean enabled = getDefaultEnabled();

        if (StringUtils.isNotEmpty(triggerProperty)) {
            subjectTrigger = user.getUserPreferences().getProperties().getProperty(triggerProperty);
        }

        if (StringUtils.isNotEmpty(removePatternProperty))
        {
            String value = user.getUserPreferences().getProperties().getProperty(removePatternProperty);

            if (StringUtils.isNotEmpty(value)) {
                removePattern = BooleanUtils.toBoolean(value);
            }
        }

        if (StringUtils.isNotEmpty(enabledProperty))
        {
            String value = user.getUserPreferences().getProperties().getProperty(enabledProperty);

            if (StringUtils.isNotEmpty(value)) {
                enabled = BooleanUtils.toBoolean(value);
            }
        }

        return new TriggerDetails(subjectTrigger, removePattern, enabled);
    }
}
