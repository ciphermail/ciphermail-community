/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

/**
 * Java interface wrapper around hierarchical properties for the template properties.
 * <p>
 * Not every template needs setters and getters. The Template edit page can work with property names directly. You
 * only need to add setter/getters if you want to use the template from Java
 *
 * @author Martijn Brinkers
 *
 */
public interface TemplateProperties
{
    /**
     * The template (the source of the template) for the portal signup notification email
     */
    void setPortalSignupTemplate(String template)
    throws HierarchicalPropertiesException;

    String getPortalSignupTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the portal password reset email
     */
    void setPortalPasswordResetTemplate(String template)
    throws HierarchicalPropertiesException;

    String getPortalPasswordResetTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) used for an encrypted PDF.
     */
    void setEncryptedPdfTemplate(String template)
    throws HierarchicalPropertiesException;

    String getEncryptedPdfTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) used for an encrypted PDF for which the password is sent via SMS.
     */
    void setEncryptedPdfSmsTemplate(String template)
    throws HierarchicalPropertiesException;

    String getEncryptedPdfSmsTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) used for an encrypted PDF for which the password is created with
     * the One Time Password (OTP) service.
     */
    void setEncryptedPdfOTPTemplate(String template)
    throws HierarchicalPropertiesException;

    String getEncryptedPdfOTPTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the notification when encryption failed
     */
    void setEncryptionFailedNotificationTemplate(String template)
    throws HierarchicalPropertiesException;

    String getEncryptionFailedNotificationTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the notification when the message was encrypted
     */
    void setEncryptionNotificationTemplate(String template)
    throws HierarchicalPropertiesException;

    String getEncryptionNotificationTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the notification with the generated passwords
     */
    void setPasswordsNotificationTemplate(String template)
    throws HierarchicalPropertiesException;

    String getPasswordsNotificationTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the SMS with the password
     */
    void setSMSPasswordTemplate(String template)
    throws HierarchicalPropertiesException;

    String getSMSPasswordTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the DLP warning message
     */
    void setDLPWarningTemplate(String template)
    throws HierarchicalPropertiesException;

    String getDLPWarningTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the DLP quarantine warning message
     */
    void setDLPQuarantineTemplate(String template)
    throws HierarchicalPropertiesException;

    String getDLPQuarantineTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the DLP block warning message
     */
    void setDLPBlockTemplate(String template)
    throws HierarchicalPropertiesException;

    String getDLPBlockTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the DLP error warning message
     */
    void setDLPErrorTemplate(String template)
    throws HierarchicalPropertiesException;

    String getDLPErrorTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the DLP release notification
     */
    void setDLPReleaseNotificationTemplate(String template)
    throws HierarchicalPropertiesException;

    String getDLPReleaseNotificationTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the DLP delete notification
     */
    void setDLPDeleteNotificationTemplate(String template)
    throws HierarchicalPropertiesException;

    String getDLPDeleteNotificationTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the DLP expire notification
     */
    void setDLPExpireNotificationTemplate(String template)
    throws HierarchicalPropertiesException;

    String getDLPExpireNotificationTemplate()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the custom 1 template
     */
    void setCustom1Template(String template)
    throws HierarchicalPropertiesException;

    String getCustom1Template()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the custom 2 template
     */
    void setCustom2Template(String template)
    throws HierarchicalPropertiesException;

    String getCustom2Template()
    throws HierarchicalPropertiesException;

    /**
     * The template (the source of the template) for the custom 3 template
     */
    void setCustom3Template(String template)
    throws HierarchicalPropertiesException;

    String getCustom3Template()
    throws HierarchicalPropertiesException;
}
