/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.james.MailAttributesUtils;
import net.sourceforge.jeval.VariableResolver;
import net.sourceforge.jeval.function.FunctionException;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Objects;

/**
 * Helper class which is used by JEval to evaluate mail properties (like size etc.).
 *
 * @author Martijn Brinkers
 *
 */
public class MailVariableResolver implements VariableResolver
{
    /*
     * The property value used when the property value is null.
     */
    private static final String NULL_STRING = "'null'";

    private static final String MAIL_VAR_PREFIX = "mail.";
    private static final String SIZE = MAIL_VAR_PREFIX + "size";
    private static final String RECIPIENTS_SIZE = MAIL_VAR_PREFIX + "recipients.size";
    private static final String CONTENT_TYPE = MAIL_VAR_PREFIX + "message.contentType";
    private static final String MAIL_ATTRIBUTE_PREFIX = MAIL_VAR_PREFIX + "attribute.";

    /*
     * The mail from which properties are resolved from
     */
    private final Mail mail;

    public MailVariableResolver(@Nonnull Mail mail) {
        this.mail = Objects.requireNonNull(mail);
    }

    /**
     * Returns the mail value of the mail variable. Returns null if the mail does not support a variable with the given name.
     */
    @Override
    public String resolveVariable(String variable)
    throws FunctionException
    {
        // Note: non numbers must be single quoted (ie. 'value')
        try {
            // Mail vars should start with mail.
            if (StringUtils.startsWithIgnoreCase(variable, MAIL_VAR_PREFIX))
            {
                if (SIZE.equalsIgnoreCase(variable)) {
                    return Long.toString(mail.getMessageSize());
                }
                else if (RECIPIENTS_SIZE.equalsIgnoreCase(variable)) {
                    return Integer.toString(mail.getRecipients().size());
                }
                else if (CONTENT_TYPE.equalsIgnoreCase(variable))
                {
                    MimeMessage message = mail.getMessage();

                    return message != null ? message.getContentType() : "";
                }
                else if (StringUtils.startsWithIgnoreCase(variable, MAIL_ATTRIBUTE_PREFIX)) {
                    // Check to see whether Mail has an attribute with the name
                    String attrName = StringUtils.substringAfter(variable, MAIL_ATTRIBUTE_PREFIX);

                    if (StringUtils.isNotEmpty(variable))
                    {
                        String attributeValue = MailAttributesUtils.attributeAsString(mail, attrName);

                        if (attributeValue != null) {
                            return "'" + attributeValue + "'";
                        }
                    }
                }

                return NULL_STRING;
            }

            return null;
        }
        catch(MessagingException e) {
            throw new FunctionException(e);
        }
    }
}
