/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.smtp;

import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.james.core.MaybeSender;
import org.apache.james.protocols.smtp.SMTPSession;
import org.apache.james.protocols.smtp.hook.HookResult;
import org.apache.james.protocols.smtp.hook.HookReturnCode;
import org.apache.james.protocols.smtp.hook.MailHook;
import org.apache.james.queue.api.MailQueue;
import org.apache.james.queue.api.MailQueueFactory;
import org.apache.james.queue.api.MailQueueName;
import org.apache.james.queue.api.ManageableMailQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * MAIL SMTP command hanlder which will delay for some time if the number of emails in a James queue exceeds some limit.
 * If the maximum number of emails is exceeded, a TEMP fail will be returned.
 */
public class ThrottlingMailHook implements MailHook
{
    private static final Logger logger = LoggerFactory.getLogger(ThrottlingMailHook.class);

    @Inject
    private MailQueueFactory<? extends MailQueue> mailQueueFactory;

    /*
     * Maximum allowed number of items in the queue. If exceeded, a TEMP fail will be returned.
     */
    private long maxQeueuSize;

    /*
     * The number of received MAIl commands after which a new queue check will be done
     */
    private long checkInterval;

    /*
     * List of limits and delays
     */
    private final List<Throttling> throttlings = new LinkedList<>();

    /*
     * The caches largest queue size
     */
    private final AtomicLong cachedQueueSize = new AtomicLong();

    /*
     * The active delay
     */
    private final AtomicLong cachedDelay = new AtomicLong();

    /*
     * Tracks how many MAIL commands were received after the last check
     */
    private final AtomicLong mailCounter = new AtomicLong();

    static class Throttling
    {
        final long limit;
        final long delay;

        public Throttling(long limit, long delay)
        {
            this.limit = limit;
            this.delay = delay;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }

    @Override
    public void init(Configuration config)
    {
        maxQeueuSize = config.getInt("max");
        checkInterval = config.getInt("checkInterval");

        List<Object> limits = config.getList ("throttling.limit");

        Throttling throttling = null;

        for (int i = 0; i < limits.size(); i++)
        {
            long limit = config.getLong(String.format("throttling(%d).limit", i));
            long delay = config.getLong(String.format("throttling(%d).delay", i));

            // check whether limit and delay are not decreasing
            if (throttling != null)
            {
                if (limit < throttling.limit) {
                    throw new IllegalArgumentException(String.format("Limit cannot decrease: %d", limit));
                }
                if (delay < throttling.delay) {
                    throw new IllegalArgumentException(String.format("Delay cannot decrease: %d", delay));
                }
            }

            throttling = new Throttling(limit, delay);

            throttlings.add(throttling);
        }
    }

    /*
     * Checks all queue sizes and sets cachedQueueSize to the largest size
     */
    private void checkLargestCurrentQueueSize()
    throws MailQueue.MailQueueException
    {
        Set<MailQueueName> mailQueueNames = mailQueueFactory.listCreatedMailQueues();

        long max = 0;

        for (MailQueueName mailQueueName : mailQueueNames)
        {
            try {
                logger.debug("Checking queue {}", mailQueueName);

                try (ManageableMailQueue mailQueue = mailQueueFactory.getQueue(mailQueueName).filter(
                        ManageableMailQueue.class::isInstance).map(ManageableMailQueue.class::cast).orElseThrow(
                        () -> new MailQueue.MailQueueException(String.format(
                                "Queue with name %s not found", mailQueueName))))
                {
                    long size = mailQueue.getSize();

                    logger.debug("Queue {} size {}", mailQueueName, size);

                    max = Math.max(max, size);
                }
            }
            catch (IOException e) {
                throw new MailQueue.MailQueueException("Error getting queue size", e);
            }
        }

        cachedQueueSize.set(max);
    }

    private void configureCachedDelay()
    {
        long currentSize = cachedQueueSize.get();
        long delay = 0;

        // step through list of limits and get the current delay
        for (Throttling throttling : throttlings)
        {
            if (currentSize < throttling.limit) {
                break;
            }

            delay = throttling.delay;
        }

        cachedDelay.set(delay);
    }

    private void sleep(long delay)
    {
        if (delay > 0)
        {
            try {
                logger.debug("Sleep for {} msec", delay);

                Thread.sleep(delay);
            }
            catch (InterruptedException e) {
                logger.warn("Thread was interrupted");
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public HookResult doMail(SMTPSession session, MaybeSender sender)
    {
        HookResult hookResult = HookResult.DECLINED;

        try {
            // if the max queue size is reached, we will always check the current queue size
            if (cachedQueueSize.get() > maxQeueuSize || mailCounter.decrementAndGet() <= 0)
            {
                // reset mail counter, check mail queue size and recalculate the current delay
                mailCounter.set(checkInterval);
                checkLargestCurrentQueueSize();
                configureCachedDelay();
            }

            // check again to see if we are over the max limit
            if (cachedQueueSize.get() > maxQeueuSize)
            {
                logger.warn("Maximum queue size reached. Maximum: {}, Current: {}",
                        maxQeueuSize, cachedQueueSize.get());

                hookResult = HookResult.builder()
                        .hookReturnCode(HookReturnCode.denySoft())
                        .smtpDescription(String.format("Queue size %d exceeds the maximum size %d",
                                cachedQueueSize.get(), maxQeueuSize))
                        .build();
            }
            else {
                sleep(cachedDelay.get());
            }
        }
        catch (MailQueue.MailQueueException e)
        {
            logger.error("Error checking queue size", e);

            hookResult = HookResult.builder()
                    .hookReturnCode(HookReturnCode.denySoft())
                    .smtpDescription("Error checking queue size")
                    .build();
        }

        return hookResult;
    }

    @VisibleForTesting
    long getMaxQeueuSize() {
        return maxQeueuSize;
    }

    @VisibleForTesting
    long getCheckInterval() {
        return checkInterval;
    }

    @VisibleForTesting
    List<Throttling> getThrottlings() {
        return throttlings;
    }

    @VisibleForTesting
    public AtomicLong getCachedQueueSize() {
        return cachedQueueSize;
    }

    @VisibleForTesting
    public AtomicLong getCachedDelay() {
        return cachedDelay;
    }

    @VisibleForTesting
    public AtomicLong getMailCounter() {
        return mailCounter;
    }

    @VisibleForTesting
    public MailQueueFactory getMailQueueFactory() {
        return mailQueueFactory;
    }
}
