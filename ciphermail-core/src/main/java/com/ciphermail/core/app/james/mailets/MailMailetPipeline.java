/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import org.apache.commons.lang.UnhandledException;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * MailMailet implementation that executes registered MailMailet one after the other
 */
public class MailMailetPipeline implements MailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(MailMailetPipeline.class);

    /*
     * All registered MailMailet's
     */
    private final List<MailMailet> mailMailets = new LinkedList<>();

    /**
     * If true, if a MailMailet from the pipeline throws an exception, others will be tried
     */
    private boolean catchExceptions = true;

    /**
     * Services the mail
     */
    @Override
    public void serviceMail(Mail mail)
    {
        for (MailMailet mailMailet : mailMailets)
        {
            try {
                mailMailet.serviceMail(mail);
            }
            catch (Exception e)
            {
                logger.error("Error servicing mail", e);

                if (!catchExceptions) {
                    throw new UnhandledException(e);
                }
            }
        }
    }

    public void addMailMailet(MailMailet mailMailet)
    {
        if (mailMailet != null) {
            mailMailets.add(mailMailet);
        }
    }

    public boolean isCatchExceptions() {
        return catchExceptions;
    }

    public void setCatchExceptions(boolean catchExceptions) {
        this.catchExceptions = catchExceptions;
    }
}
