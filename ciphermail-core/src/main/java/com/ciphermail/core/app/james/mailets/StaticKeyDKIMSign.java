/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.Context;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMException;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.StringReader;
import java.security.PrivateKey;
import java.util.Objects;

/**
 * Digitally signs a message with the DKIM protocol. The key is read from the Mailet configuration and should be
 * a PEM encoded private key file without password
 */
public class StaticKeyDKIMSign extends AbstractDKIMSign
{
    private static final Logger logger = LoggerFactory.getLogger(StaticKeyDKIMSign.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        PRIVATE_KEY        ("privateKey"),
        SIGNATURE_TEMPLATE ("signatureTemplate");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * The private key used to sign the message with
     */
    private PrivateKey privateKey;

    /*
     * The DKIM signature template
     */
    private String signatureTemplate;

    @Override
    protected void initMailet()
    throws MessagingException
    {
        super.initMailet();

        signatureTemplate = Objects.requireNonNull(StringUtils.trimToNull(
                getInitParameter(Parameter.SIGNATURE_TEMPLATE.name)),
                Parameter.SIGNATURE_TEMPLATE.name + " parameter is missing");

        privateKey = parseKey(getInitParameter(Parameter.PRIVATE_KEY.name));
    }

    @Override
    protected String getProvider()
    {
        // Because we will use a PEM for the DKIM key, we need to use the non-sensitive provider
        return SecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider();
    }

    protected static PrivateKey parseKey(String pkcs8)
    throws MessagingException
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        JcaPEMKeyConverter keyConverter = new JcaPEMKeyConverter();

        // Since we are importing PEM, we will use the non-sensitive provider because any private material
        // will not be stored on HSM
        keyConverter.setProvider(securityFactory.getNonSensitiveProvider());

        PrivateKey key = null;

        pkcs8 = StringUtils.trimToNull(pkcs8);

        if (pkcs8 != null)
        {
            PEMParser pem = new PEMParser(new StringReader(pkcs8));

            Object o;

            try {
                o = pem.readObject();
            }
            catch (IOException e) {
                throw new MessagingException("Unable to read PEM encoded private key", e);
            }
            finally {
                IOUtils.closeQuietly(pem);
            }

            try {
                if (o instanceof PEMKeyPair pemKeyPair)
                {
                    // We are only interested in private keys and not in "stand-alone" public keys
                    key = keyConverter.getKeyPair(pemKeyPair).getPrivate();
                }
                else if (o instanceof PrivateKeyInfo privateKeyInfo) {
                    key = keyConverter.getPrivateKey(privateKeyInfo);
                }
                else if (o == null) {
                    throw new MessagingException("The PEM encoded blob did not return any object.");
                }
                else {
                    throw new MessagingException("The PEM input is not a PrivateKey or KeyPair but a " +
                            o.getClass());
                }
            }
            catch (PEMException e) {
                throw new MessagingException("The PEM input is not a valid PrivateKey or KeyPair.", e);
            }
        }

        return key;
    }

    @Override
    protected PrivateKey getPrivateKey(Mail mail, Context context)
    throws MessagingException
    {
        return privateKey;
    }

    @Override
    protected String getSignatureTemplate(Mail mail, Context context) {
        return signatureTemplate;
    }
}
