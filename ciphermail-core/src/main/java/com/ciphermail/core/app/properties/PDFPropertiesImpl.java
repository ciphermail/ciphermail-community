/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import com.ciphermail.core.common.properties.Parent;
import com.ciphermail.core.common.util.URIUtils;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;

/**
 * Implementation of PDFProperties
 */
@UserPropertiesType(name = "pdf", displayName = "PDF", order = PDFPropertiesImpl.ORDER)
public class PDFPropertiesImpl extends DelegatedHierarchicalProperties implements PDFProperties
{
    public static final int ORDER = 50;

    private static final String PDF_ENCRYPTION_ENABLED                  = "pdf-encryption-enabled";
    private static final String PDF_MAX_SIZE                            = "pdf-max-size";
    private static final String PDF_ONLY_ENCRYPT_IF_MANDATORY           = "pdf-only-encrypt-if-mandatory";
    private static final String PASSWORDS_SEND_TO_ORIGINATOR            = "pdf-passwords-send-to-originator";
    private static final String OTP_ENABLED                             = "pdf-otp-enabled";
    private static final String PASSWORD                                = "pdf-user-password";
    private static final String SUBJECT_PASSWORD_TRIGGER_REGEX          = "pdf-subject-password-trigger-regex";
    private static final String SUBJECT_PASSWORD_TRIGGER_REGEX_ENABLED  = "pdf-subject-password-trigger-regex-enabled";
    private static final String PASSWORD_LENGTH                         = "pdf-password-length";
    private static final String PDF_REPLY_ENABLED                       = "pdf-reply-enabled";
    private static final String PDF_REPLY_SUBJECT_FORMAT_STRING         = "pdf-reply-subject-format-string";
    private static final String PDF_REPLY_FROM_FORMAT_STRING            = "pdf-reply-from-format-string";
    private static final String PDF_REPLY_CC                            = "pdf-reply-cc";
    private static final String PDF_REPLY_VALIDITY_INTERVAL             = "pdf-reply-validity-interval";
    private static final String PDF_REPLY_SENDER                        = "pdf-reply-sender";
    private static final String PDF_REPLY_SENDER_ENABLED                = "pdf-reply-sender-enabled";
    private static final String PDF_REPLY_ATTACHMENTS_MAX_SIZE          = "pdf-reply-attachments-max-size";
    private static final String PDF_REPLY_BODY_MAX_SIZE                 = "pdf-reply-body-max-size";
    private static final String PDF_SIGN_EMAIL                          = "pdf-sign-email";
    private static final String PDF_DEEP_SCAN                           = "pdf-deep-scan";
    private static final String PDF_QR_CODE_ENABLED                     = "pdf-qrcode-enabled";
    // these properties should be package private
    static final String OTP_URL                                         = "pdf-otp-url";
    static final String PDF_REPLY_URL                                   = "pdf-reply-url";
    static final String OTP_SECRET_KEY                                  = "pdf-otp-secret-key";
    static final String OTP_SUB_KEY                                     = "pdf-otp-sub-key";
    static final String PDF_REPLY_SECRET_KEY                            = "pdf-reply-secret-key";
    static final String PDF_REPLY_SUB_KEY                               = "pdf-reply-sub-key";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public PDFPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(name = PDF_ENCRYPTION_ENABLED)
    public void setPdfEncryptionAllowed(Boolean allowed)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PDF_ENCRYPTION_ENABLED,
                allowed);
    }

    @Override
    @UserProperty(name = PDF_ENCRYPTION_ENABLED,
            order = 10
    )
    public Boolean getPdfEncryptionAllowed()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PDF_ENCRYPTION_ENABLED, false);
    }

    @Override
    @UserProperty(name = PDF_MAX_SIZE)
    public void setPdfMaxSize(Long size)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, PDF_MAX_SIZE, size);
    }

    @Override
    @UserProperty(name = PDF_MAX_SIZE,
            order = 20
    )
    public Long getPdfMaxSize()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, PDF_MAX_SIZE, null);
    }

    @Override
    @UserProperty(name = PDF_ONLY_ENCRYPT_IF_MANDATORY)
    public void setPdfOnlyEncryptIfMandatory(Boolean encryptIfMandatory)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PDF_ONLY_ENCRYPT_IF_MANDATORY, encryptIfMandatory);
    }

    @Override
    @UserProperty(name = PDF_ONLY_ENCRYPT_IF_MANDATORY,
            order = 30
    )
    public Boolean getPdfOnlyEncryptIfMandatory()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PDF_ONLY_ENCRYPT_IF_MANDATORY, false);
    }

    @Override
    @UserProperty(name = PASSWORDS_SEND_TO_ORIGINATOR)
    public void setPasswordsSendToOriginator(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PASSWORDS_SEND_TO_ORIGINATOR, value);
    }

    @Override
    @UserProperty(name = PASSWORDS_SEND_TO_ORIGINATOR,
            order = 40
    )
    public Boolean getPasswordsSendToOriginator()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PASSWORDS_SEND_TO_ORIGINATOR, false);
    }

    @Override
    @UserProperty(name = OTP_ENABLED)
    public void setOTPEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, OTP_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = OTP_ENABLED,
            order = 50
    )
    public Boolean getOTPEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, OTP_ENABLED,false);
    }

    @Override
    @UserProperty(name = OTP_SECRET_KEY)
    public void setOTPSecretKey(String secretKey)
    throws HierarchicalPropertiesException
    {
        setProperty(OTP_SECRET_KEY, StringUtils.trimToNull(secretKey));
    }

    @Override
    @UserProperty(name = OTP_SECRET_KEY,
            domain = false, global = false,
            visible = false,
            allowNull = true
    )
    public String getOTPSecretKey()
    throws HierarchicalPropertiesException
    {
        return getProperty(OTP_SECRET_KEY);
    }

    @Override
    @UserProperty(name = OTP_SUB_KEY)
    public void setOTPSubKey(String subKey)
    throws HierarchicalPropertiesException
    {
        setProperty(OTP_SUB_KEY, subKey);
    }

    @Override
    @UserProperty(name = OTP_SUB_KEY,
            visible = false,
            allowNull = true
    )
    public String getOTPSubKey()
    throws HierarchicalPropertiesException
    {
        return getProperty(OTP_SUB_KEY);
    }

    @Override
    @UserProperty(name = PASSWORD)
    public void setPassword(String password)
    throws HierarchicalPropertiesException
    {
        setProperty(PASSWORD, password);
    }

    @Override
    @UserProperty(name = PASSWORD,
            order = 70,
            allowNull = true
    )
    public String getPassword()
    throws HierarchicalPropertiesException
    {
        return getProperty(PASSWORD);
    }

    @Override
    @UserProperty(name = SUBJECT_PASSWORD_TRIGGER_REGEX,
            order = 74
    )
    public void setSubjectPasswordTriggerRegEx(String trigger)
    throws HierarchicalPropertiesException
    {
        setProperty(SUBJECT_PASSWORD_TRIGGER_REGEX, UserPropertiesValidators.validateRegEx(
                SUBJECT_PASSWORD_TRIGGER_REGEX, trigger));
    }

    @Override
    @UserProperty(name = SUBJECT_PASSWORD_TRIGGER_REGEX,
            order = 74
    )
    public String getSubjectPasswordTriggerRegEx()
    throws HierarchicalPropertiesException
    {
        return getProperty(SUBJECT_PASSWORD_TRIGGER_REGEX);
    }

    @Override
    @UserProperty(name = SUBJECT_PASSWORD_TRIGGER_REGEX_ENABLED,
            order = 76
    )
    public void setSubjectPasswordTriggerRegExEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SUBJECT_PASSWORD_TRIGGER_REGEX_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = SUBJECT_PASSWORD_TRIGGER_REGEX_ENABLED,
            order = 76
    )
    public Boolean getSubjectPasswordTriggerRegExEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SUBJECT_PASSWORD_TRIGGER_REGEX_ENABLED, false);
    }

    @Override
    @UserProperty(name = PASSWORD_LENGTH)
    public void setPasswordLength(Integer length)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(this, PASSWORD_LENGTH, length);
    }

    @Override
    @UserProperty(name = PASSWORD_LENGTH,
            order = 80
    )
    public Integer getPasswordLength()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(this, PASSWORD_LENGTH, 16);
    }

    @Override
    @UserProperty(name = PDF_REPLY_ENABLED)
    public void setPdfReplyEnabled(Boolean allowed)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PDF_REPLY_ENABLED, allowed);
    }

    @Override
    @UserProperty(name = PDF_REPLY_ENABLED,
            order = 90
    )
    public Boolean getPdfReplyEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PDF_REPLY_ENABLED, false);
    }

    @Override
    @UserProperty(name = PDF_REPLY_SECRET_KEY)
    public void setPdfReplySecretKey(String secretKey)
    throws HierarchicalPropertiesException
    {
        setProperty(PDF_REPLY_SECRET_KEY, StringUtils.trimToNull(secretKey));
    }

    @Override
    @UserProperty(name = PDF_REPLY_SECRET_KEY,
            visible = false,
            allowNull = true
    )
    public String getPdfReplySecretKey()
    throws HierarchicalPropertiesException
    {
        return getProperty(PDF_REPLY_SECRET_KEY);
    }

    @Override
    @UserProperty(name = PDF_REPLY_SUB_KEY)
    public void setPdfReplySubKey(String subKey)
    throws HierarchicalPropertiesException
    {
        setProperty(PDF_REPLY_SUB_KEY, subKey);
    }

    @Override
    @UserProperty(name = PDF_REPLY_SUB_KEY,
            visible = false,
            allowNull = true
    )
    public String getPdfReplySubKey()
    throws HierarchicalPropertiesException
    {
        return getProperty(PDF_REPLY_SUB_KEY);
    }

    @Override
    @UserProperty(name = PDF_REPLY_SUBJECT_FORMAT_STRING)
    public void setPdfReplySubjectFormatString(String formatString)
    throws HierarchicalPropertiesException
    {
        setProperty(PDF_REPLY_SUBJECT_FORMAT_STRING, UserPropertiesValidators.validateFormatString(
                PDF_REPLY_SUBJECT_FORMAT_STRING, formatString, "test subject"));
    }

    @Override
    @UserProperty(name = PDF_REPLY_SUBJECT_FORMAT_STRING,
            order = 100
    )
    public String getPdfReplySubjectFormatString()
    throws HierarchicalPropertiesException
    {
        return getProperty(PDF_REPLY_SUBJECT_FORMAT_STRING);
    }

    @Override
    @UserProperty(name = PDF_REPLY_FROM_FORMAT_STRING)
    public void setPdfReplyFromFormatString(String formatString)
    throws HierarchicalPropertiesException
    {
        setProperty(PDF_REPLY_FROM_FORMAT_STRING, UserPropertiesValidators.validateFormatString(
                PDF_REPLY_FROM_FORMAT_STRING, formatString, "test from"));
    }

    @Override
    @UserProperty(name = PDF_REPLY_FROM_FORMAT_STRING,
            order = 110
    )
    public String getPdfReplyFromFormatString()
    throws HierarchicalPropertiesException
    {
        return getProperty(PDF_REPLY_FROM_FORMAT_STRING);
    }

    @Override
    @UserProperty(name = PDF_REPLY_CC)
    public void setPdfReplyCC(Boolean sendCC)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PDF_REPLY_CC, sendCC);
    }

    @Override
    @UserProperty(name = PDF_REPLY_CC,
            order = 120
    )
    public Boolean getPdfReplyCC()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PDF_REPLY_CC, false);
    }

    @Override
    @UserProperty(name = PDF_REPLY_VALIDITY_INTERVAL)
    public void setPdfReplyValidityInterval(Long size)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, PDF_REPLY_VALIDITY_INTERVAL, size);
    }

    @Override
    @UserProperty(name = PDF_REPLY_VALIDITY_INTERVAL,
            order = 130
    )
    public Long getPdfReplyValidityInterval()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, PDF_REPLY_VALIDITY_INTERVAL, null);
    }

    @Override
    @UserProperty(name = PDF_REPLY_SENDER)
    public void setPdfReplySender(String email)
    throws HierarchicalPropertiesException
    {
        setProperty(PDF_REPLY_SENDER, UserPropertiesValidators.validateEmail(PDF_REPLY_SENDER, email));
    }

    @Override
    @UserProperty(name = PDF_REPLY_SENDER,
            order = 150,
            allowNull = true
    )
    public String getPdfReplySender()
    throws HierarchicalPropertiesException
    {
        return getProperty(PDF_REPLY_SENDER);
    }

    @Override
    @UserProperty(name = PDF_REPLY_SENDER_ENABLED)
    public void setPdfReplySenderEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PDF_REPLY_SENDER_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = PDF_REPLY_SENDER_ENABLED,
            order = 160
    )
    public Boolean getPdfReplySenderEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PDF_REPLY_SENDER_ENABLED, true);
    }

    @Override
    @UserProperty(name = PDF_REPLY_ATTACHMENTS_MAX_SIZE)
    public void setPdfReplyAttachmentsMaxSize(Long maxSize)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, PDF_REPLY_ATTACHMENTS_MAX_SIZE, maxSize);
    }

    @Override
    @UserProperty(name = PDF_REPLY_ATTACHMENTS_MAX_SIZE,
            order = 170
    )
    public Long getPdfReplyAttachmentsMaxSize()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, PDF_REPLY_ATTACHMENTS_MAX_SIZE);
    }

    @Override
    @UserProperty(name = PDF_REPLY_BODY_MAX_SIZE)
    public void setPdfReplyBodyMaxSize(Long maxSize)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, PDF_REPLY_BODY_MAX_SIZE, maxSize);
    }

    @Override
    @UserProperty(name = PDF_REPLY_BODY_MAX_SIZE,
            order = 180
    )
    public Long getPdfReplyBodyMaxSize()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, PDF_REPLY_BODY_MAX_SIZE);
    }

    @Override
    @UserProperty(name = PDF_SIGN_EMAIL)
    public void setPdfSignEmail(Boolean sign)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PDF_SIGN_EMAIL, sign);
    }

    @Override
    @UserProperty(name = PDF_SIGN_EMAIL,
            order = 190
    )
    public Boolean getPdfSignEmail()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PDF_SIGN_EMAIL, false);
    }

    @Override
    @UserProperty(name = PDF_DEEP_SCAN)
    public void setPdfDeepScan(Boolean deepscan)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PDF_DEEP_SCAN, deepscan);
    }

    @Override
    @UserProperty(name = PDF_DEEP_SCAN,
            order = 200
    )
    public Boolean getPdfDeepScan()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PDF_DEEP_SCAN, false);
    }

    @Override
    @UserProperty(name = PDF_QR_CODE_ENABLED)
    public void setPdfQrCodeEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PDF_QR_CODE_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = PDF_QR_CODE_ENABLED,
            order = 210
    )
    public Boolean getPdfQrCodeEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PDF_QR_CODE_ENABLED, false);
    }

    @Override
    @UserProperty(name = OTP_URL)
    public void setOTPURL(String url)
    throws HierarchicalPropertiesException
    {
        setProperty(OTP_URL, UserPropertiesValidators.validateURI(
                OTP_URL, url, URIUtils.URIType.FULL));
    }

    @Override
    @UserProperty(name = OTP_URL,
            visible = false,
            allowNull = true
    )
    public String getOTPURL()
    throws HierarchicalPropertiesException
    {
        // Note: the URL will not be inherited from the parent. This allows us to change the base URL which will then
        // result in changing all URLs based on the base URL
        return getProperty(OTP_URL, Parent.DO_NOT_CHECK_PARENT);
    }

    @Override
    @UserProperty(name = PDF_REPLY_URL)
    public void setPdfReplyURL(String url)
    throws HierarchicalPropertiesException
    {
        setProperty(PDF_REPLY_URL, UserPropertiesValidators.validateURI(
                PDF_REPLY_URL, url, URIUtils.URIType.FULL));
    }

    @Override
    @UserProperty(name = PDF_REPLY_URL,
            visible = false,
            allowNull = true
    )
    public String getPdfReplyURL()
    throws HierarchicalPropertiesException
    {
        // Note: the URL will not be inherited from the parent. This allows us to change the base URL which will then
        // result in changing all URLs based on the base URL
        return getProperty(PDF_REPLY_URL, Parent.DO_NOT_CHECK_PARENT);
    }
}
