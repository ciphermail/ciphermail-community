/*
 * Copyright (c) 2011-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;

import javax.annotation.Nonnull;

@UserPropertiesType(name = "dkim", displayName = "DKIM")
public class DKIMPropertiesImpl extends DelegatedHierarchicalProperties implements DKIMProperties
{
    private static final String SYSTEM_KEY_PAIR     = "dkim-key-pair";
    private static final String SIGNING_ENABLED     = "dkim-signing-enabled";
    private static final String KEY_IDENTIFIER      = "dkim-key-identifier";
    private static final String SIGNATURE_TEMPLATE  = "dkim-signature-template";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public DKIMPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(name = SYSTEM_KEY_PAIR,
            editor = UserPropertyEditors.MULTILINE_EDITOR
    )
    public void setSystemKeyPair(String keyPair)
    throws HierarchicalPropertiesException
    {
        setProperty(SYSTEM_KEY_PAIR, UserPropertiesValidators.validateKeyPair(SYSTEM_KEY_PAIR, keyPair));
    }

    @Override
    @UserProperty(name = SYSTEM_KEY_PAIR,
            editor = UserPropertyEditors.MULTILINE_EDITOR,
            order = 10,
            allowNull = true
    )
    public String getSystemKeyPair()
    throws HierarchicalPropertiesException
    {
        return getProperty(SYSTEM_KEY_PAIR);
    }

    @Override
    @UserProperty(name = SIGNING_ENABLED)
    public void setSigningEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SIGNING_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = SIGNING_ENABLED,
            order = 20
    )
    @Nonnull public Boolean getSigningEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SIGNING_ENABLED);
    }

    @Override
    @UserProperty(name = KEY_IDENTIFIER)
    public void setKeyIdentifier(String keyId)
    throws HierarchicalPropertiesException
    {
        setProperty(KEY_IDENTIFIER, keyId);
    }

    @Override
    @UserProperty(name = KEY_IDENTIFIER,
            order = 30,
            allowNull = true
    )
    public String getKeyIdentifier()
    throws HierarchicalPropertiesException
    {
        return getProperty(KEY_IDENTIFIER);
    }

    @Override
    @UserProperty(name = SIGNATURE_TEMPLATE)
    public void setSignatureTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(SIGNATURE_TEMPLATE, UserPropertiesValidators.validateDKIMSignatureTemplate(SIGNATURE_TEMPLATE,
                template));
    }

    @Override
    @UserProperty(name = SIGNATURE_TEMPLATE,
            order = 40
    )
    public String getSignatureTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(SIGNATURE_TEMPLATE);
    }
}
