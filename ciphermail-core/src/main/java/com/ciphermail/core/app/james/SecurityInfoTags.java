/*
 * Copyright (c) 2012-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

/**
 * SecurityInfoTags contains the tags which are added to the subject when a message is handled by the SMIMEHandler.
 * The SecurityInfoTags is serializable so it can be added to Mail as an attribute
 *
 * @author Martijn Brinkers
 *
 */
public class SecurityInfoTags
{
    /*
     * The tag to add to the subject when the message was decrypted
     */
    private String decryptedTag;

    /*
     * The tag to add to the subject when the message was signed and valid
     */
    private String signedValidTag;

    /*
     * The tag to add to the subject when the message was signed and valid but signed using a different
     * email address then the sender
     */
    private String signedByValidTag;

    /*
     * The tag to add to the subject when the message was signed but not valid.
     */
    private String signedInvalidTag;

    /*
     * Tag that will be added to the subject if the messages contains mixed content (i.e., some but not all parts are
     * signed and/or encrypted)
     */
    private String mixedContentTag;

    public SecurityInfoTags() {
        // Required for JSON serialization
    }

    public SecurityInfoTags(String decryptedTag, String signedValidTag, String signedByValidTag,
            String signedInvalidTag, String mixedContentTag)
    {
        this.decryptedTag = decryptedTag;
        this.signedValidTag = signedValidTag;
        this.signedByValidTag = signedByValidTag;
        this.signedInvalidTag = signedInvalidTag;
        this.mixedContentTag = mixedContentTag;
    }

    public String getDecryptedTag() {
        return decryptedTag;
    }

    public String getSignedValidTag() {
        return signedValidTag;
    }

    public String getSignedByValidTag() {
        return signedByValidTag;
    }

    public String getSignedInvalidTag() {
        return signedInvalidTag;
    }

    public String getMixedContentTag() {
        return mixedContentTag;
    }
}
