/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.template.TemplateBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;

@UserPropertiesType(name = "template", displayName = "Template", order = 60)
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class TemplatePropertiesImpl extends DelegatedHierarchicalProperties implements TemplateProperties
{
    private static final String PORTAL_SIGNUP                  = "template-portal-signup";
    private static final String PORTAL_PASSWORD_RESET          = "template-portal-password-reset";
    private static final String ENCRYPTED_PDF                  = "template-encrypted-pdf";
    private static final String ENCRYPTED_PDF_SMS              = "template-encrypted-pdf-sms";
    private static final String ENCRYPTED_PDF_OTP              = "template-encrypted-pdf-otp";
    private static final String ENCRYPTION_FAILED_NOTIFICATION = "template-encryption-failed-notification";
    private static final String ENCRYPTION_NOTIFICATION        = "template-encryption-notification";
    private static final String PASSWORDS_NOTIFICATION         = "template-passwords-notification";
    private static final String SMS_PASSWORD                   = "template-sms-password";
    private static final String DLP_WARNING                    = "template-dlp-warning";
    private static final String DLP_QUARANTINE                 = "template-dlp-quarantine";
    private static final String DLP_BLOCK                      = "template-dlp-block";
    private static final String DLP_ERROR                      = "template-dlp-error";
    private static final String DLP_RELEASE_NOTIFICATION       = "template-dlp-release-notification";
    private static final String DLP_DELETE_NOTIFICATION        = "template-dlp-delete-notification";
    private static final String DLP_EXPIRE_NOTIFICATION        = "template-dlp-expire-notification";
    private static final String CUSTOM_1                       = "template-custom-1";
    private static final String CUSTOM_2                       = "template-custom-2";
    private static final String CUSTOM_3                       = "template-custom-3";

    @Autowired
    private TemplateBuilder templateBuilder;

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public TemplatePropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(name = PORTAL_SIGNUP, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setPortalSignupTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(PORTAL_SIGNUP, UserPropertiesValidators.validateTemplate(PORTAL_SIGNUP,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = PORTAL_SIGNUP, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 5)
    public String getPortalSignupTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(PORTAL_SIGNUP);
    }

    @Override
    @UserProperty(name = PORTAL_PASSWORD_RESET, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setPortalPasswordResetTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(PORTAL_PASSWORD_RESET, UserPropertiesValidators.validateTemplate(PORTAL_PASSWORD_RESET,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = PORTAL_PASSWORD_RESET, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 5)
    public String getPortalPasswordResetTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(PORTAL_PASSWORD_RESET);
    }

    @Override
    @UserProperty(name = ENCRYPTED_PDF, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setEncryptedPdfTemplate(String template)
    throws HierarchicalPropertiesException
    {
        this.setProperty(ENCRYPTED_PDF, UserPropertiesValidators.validateTemplate(ENCRYPTED_PDF,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = ENCRYPTED_PDF, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 10)
    public String getEncryptedPdfTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(ENCRYPTED_PDF);
    }

    @Override
    @UserProperty(name = ENCRYPTED_PDF_SMS, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setEncryptedPdfSmsTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(ENCRYPTED_PDF_SMS, UserPropertiesValidators.validateTemplate(ENCRYPTED_PDF_SMS,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = ENCRYPTED_PDF_SMS, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 20)
    public String getEncryptedPdfSmsTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(ENCRYPTED_PDF_SMS);
    }

    @Override
    @UserProperty(name = ENCRYPTED_PDF_OTP, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setEncryptedPdfOTPTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(ENCRYPTED_PDF_OTP, UserPropertiesValidators.validateTemplate(ENCRYPTED_PDF_OTP,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = ENCRYPTED_PDF_OTP, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 25)
    public String getEncryptedPdfOTPTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(ENCRYPTED_PDF_OTP);
    }

    @Override
    @UserProperty(name = ENCRYPTION_FAILED_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setEncryptionFailedNotificationTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(ENCRYPTION_FAILED_NOTIFICATION, UserPropertiesValidators.validateTemplate(
                ENCRYPTION_FAILED_NOTIFICATION, template, templateBuilder));
    }

    @Override
    @UserProperty(name = ENCRYPTION_FAILED_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 40)
    public String getEncryptionFailedNotificationTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(ENCRYPTION_FAILED_NOTIFICATION);
    }

    @Override
    @UserProperty(name = ENCRYPTION_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setEncryptionNotificationTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(ENCRYPTION_NOTIFICATION, UserPropertiesValidators.validateTemplate(ENCRYPTION_NOTIFICATION,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = ENCRYPTION_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 50)
    public String getEncryptionNotificationTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(ENCRYPTION_NOTIFICATION);
    }

    @Override
    @UserProperty(name = PASSWORDS_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setPasswordsNotificationTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(PASSWORDS_NOTIFICATION, UserPropertiesValidators.validateTemplate(PASSWORDS_NOTIFICATION,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = PASSWORDS_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 60)
    public String getPasswordsNotificationTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(PASSWORDS_NOTIFICATION);
    }

    @Override
    @UserProperty(name = SMS_PASSWORD, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setSMSPasswordTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(SMS_PASSWORD, UserPropertiesValidators.validateTemplate(SMS_PASSWORD,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = SMS_PASSWORD, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 70)
    public String getSMSPasswordTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(SMS_PASSWORD);
    }

    @Override
    @UserProperty(name = DLP_WARNING, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setDLPWarningTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(DLP_WARNING, UserPropertiesValidators.validateTemplate(DLP_WARNING,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = DLP_WARNING, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 80)
    public String getDLPWarningTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(DLP_WARNING);
    }

    @Override
    @UserProperty(name = DLP_QUARANTINE, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setDLPQuarantineTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(DLP_QUARANTINE, UserPropertiesValidators.validateTemplate(DLP_QUARANTINE,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = DLP_QUARANTINE, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 90)
    public String getDLPQuarantineTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(DLP_QUARANTINE);
    }

    @Override
    @UserProperty(name = DLP_BLOCK, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setDLPBlockTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(DLP_BLOCK, UserPropertiesValidators.validateTemplate(DLP_BLOCK,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = DLP_BLOCK, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 100)
    public String getDLPBlockTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(DLP_BLOCK);
    }

    @Override
    @UserProperty(name = DLP_ERROR, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setDLPErrorTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(DLP_ERROR, UserPropertiesValidators.validateTemplate(DLP_ERROR,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = DLP_ERROR, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 110)
    public String getDLPErrorTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(DLP_ERROR);
    }

    @Override
    @UserProperty(name = DLP_RELEASE_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setDLPReleaseNotificationTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(DLP_RELEASE_NOTIFICATION, UserPropertiesValidators.validateTemplate(DLP_RELEASE_NOTIFICATION,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = DLP_RELEASE_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 120)
    public String getDLPReleaseNotificationTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(DLP_RELEASE_NOTIFICATION);
    }

    @Override
    @UserProperty(name = DLP_DELETE_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setDLPDeleteNotificationTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(DLP_DELETE_NOTIFICATION, UserPropertiesValidators.validateTemplate(DLP_DELETE_NOTIFICATION,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = DLP_DELETE_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 130)
    public String getDLPDeleteNotificationTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(DLP_DELETE_NOTIFICATION);
    }

    @Override
    @UserProperty(name = DLP_EXPIRE_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setDLPExpireNotificationTemplate(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(DLP_EXPIRE_NOTIFICATION, UserPropertiesValidators.validateTemplate(DLP_EXPIRE_NOTIFICATION,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = DLP_EXPIRE_NOTIFICATION, editor = UserPropertyEditors.MULTILINE_EDITOR, order = 140)
    public String getDLPExpireNotificationTemplate()
    throws HierarchicalPropertiesException
    {
        return getProperty(DLP_EXPIRE_NOTIFICATION);
    }

    @Override
    @UserProperty(name = CUSTOM_1, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setCustom1Template(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(CUSTOM_1, UserPropertiesValidators.validateTemplate(CUSTOM_1,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = CUSTOM_1, editor = UserPropertyEditors.MULTILINE_EDITOR, visible = false)
    public String getCustom1Template()
    throws HierarchicalPropertiesException
    {
        return getProperty(CUSTOM_1);
    }

    @Override
    @UserProperty(name = CUSTOM_2, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setCustom2Template(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(CUSTOM_2, UserPropertiesValidators.validateTemplate(CUSTOM_2,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = CUSTOM_2, editor = UserPropertyEditors.MULTILINE_EDITOR, visible = false)
    public String getCustom2Template()
    throws HierarchicalPropertiesException
    {
        return getProperty(CUSTOM_2);
    }

    @Override
    @UserProperty(name = CUSTOM_3, editor = UserPropertyEditors.MULTILINE_EDITOR)
    public void setCustom3Template(String template)
    throws HierarchicalPropertiesException
    {
        setProperty(CUSTOM_3, UserPropertiesValidators.validateTemplate(CUSTOM_3,
                template, templateBuilder));
    }

    @Override
    @UserProperty(name = CUSTOM_3, editor = UserPropertyEditors.MULTILINE_EDITOR, visible = false)
    public String getCustom3Template()
    throws HierarchicalPropertiesException
    {
        return getProperty(CUSTOM_3);
    }
}
