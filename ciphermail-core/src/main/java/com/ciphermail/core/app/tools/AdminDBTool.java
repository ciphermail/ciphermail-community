/*
 * Copyright (c) 2012-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.tools;

import com.ciphermail.core.app.admin.Admin;
import com.ciphermail.core.app.admin.AdminManager;
import com.ciphermail.core.app.admin.AdminManagerImpl;
import com.ciphermail.core.app.admin.AuthenticationType;
import com.ciphermail.core.app.admin.RoleManager;
import com.ciphermail.core.app.admin.RoleManagerImpl;
import com.ciphermail.core.common.hibernate.HibernateInfo;
import com.ciphermail.core.common.hibernate.HibernateSessionFactoryBuilder;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SessionManagerImpl;
import inet.ipaddr.IPAddressString;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Properties;


/**
 * The AdminTool class is a command-line tool that allows configuring admin credentials by directly connecting to the
 * database. This should be used to add the initial admin or to reset an admin password. For all other admin related
 * tasks, use the CLI tool.
 * <p>
 * To use the tool, provide the required command line options:
 * -   --admin-name: The name of the admin.
 * -   --admin-password: The password of the admin.
 * <p>
 * Optional command line options:
 * -   --admin-ip-addresses: The authorized IP range for the admin.
 * <p>
 * If the required options are provided, the tool will create a new admin with the given name and password if the admin
 * does not yet exist and optionally set the authorized IP range for the admin. If the admin already exists, only
 * the password and optionally the authorized IP range will be changed.
 * <p>
 * The tool uses Hibernate for database operations, and the database connection details must be provided using
 * the following options:
 * -   --db-driver-class: The database driver class.
 * -   --db-url: The database JDBC URL.
 * -   --db-username: The database JDBC authentication username.
 * -   --db-password: The database JDBC authentication password.
 * -   --db-dialect: The database dialect (optional, default: auto detection).
 * -   --db-physical-naming-strategy-class-name: The naming strategy for the database (optional).
 */
@SuppressWarnings({"java:S106", "java:S112"})
public class AdminDBTool
{
    private static final String COMMAND_NAME = AdminDBTool.class.getName();

    /*
     * The command line instance
     */
    private static CommandLine commandLine;

    /*
     * The CLI option for showing help
     */
    private static final String HELP_OPTION_SHORT = "h";
    private static final String HELP_OPTION_LONG = "help";

    /*
     * General command line options
     */
    private static final String ADMIN_NAME_OPTION = "admin-name";
    private static final String ADMIN_PASSWORD_OPTION = "admin-password";
    private static final String ROLE_OPTION = "role";
    private static final String ADMIN_IP_ADDRESSES_OPTION = "ip-addresses";
    private static final String SET_ADMIN_CREDENTIALS_OPTION = "set-admin-credentials";

    /*
     * Database command line options
     */
    private static final String DB_DRIVER_CLASS_OPTION = "db-driver-class";
    private static final String DB_URL_OPTION = "db-url";
    private static final String DB_USERNAME_OPTION = "db-username";
    private static final String DB_PASSWORD_OPTION = "db-password";
    private static final String DB_DIALECT_OPTION = "db-dialect";
    private static final String DB_PHYSICAL_NAMING_STRATEGY_CLASS_NAME_OPTION = "db-physical-naming-strategy-class-name";

    /*
     * Default role name if not explicitly set
     */
    private static final String DEFAULT_ROLE_NAME = "admin";

    public static class LocalServices
    {
        @Bean
        public HibernateInfo hibernateInfoService()  {
            return new HibernateInfo();
        }

        @Bean
        public SessionFactory sessionFactoryService(HibernateInfo hibernateInfo)
        {
            Properties props = new Properties();

            props.setProperty("hibernate.connection.driver_class", commandLine.getOptionValue(DB_DRIVER_CLASS_OPTION));
            props.setProperty("hibernate.connection.url", commandLine.getOptionValue(DB_URL_OPTION));
            props.setProperty("hibernate.connection.username", commandLine.getOptionValue(DB_USERNAME_OPTION));
            props.setProperty("hibernate.connection.password", commandLine.getOptionValue(DB_PASSWORD_OPTION));

            if (commandLine.hasOption(DB_DIALECT_OPTION)) {
                props.setProperty("hibernate.dialect", commandLine.getOptionValue(DB_DIALECT_OPTION));
            }

            HibernateSessionFactoryBuilder builder = HibernateSessionFactoryBuilder.createInstance()
                    .setHibernateInfo(hibernateInfo)
                    .setProperties(props).setPackagesToScan("com.ciphermail");

            if (commandLine.hasOption(DB_PHYSICAL_NAMING_STRATEGY_CLASS_NAME_OPTION))
            {
                builder.setPhysicalNamingStrategyClassName(commandLine.getOptionValue(
                        DB_PHYSICAL_NAMING_STRATEGY_CLASS_NAME_OPTION));
            }

            return builder.build();
        }

        @Bean
        public HibernateTransactionManager transactionManagerService(SessionFactory sessionFactory)  {
            return new HibernateTransactionManager(sessionFactory);
        }

        @Bean
        public TransactionOperations defaultTransactionTemplateService(HibernateTransactionManager transactionManager)  {
            return new TransactionTemplate(transactionManager);
        }

        @Bean
        public SessionManager sessionManagerService(SessionFactory sessionFactory)  {
            return new SessionManagerImpl(sessionFactory);
        }

        @Bean
        public AdminManager adminManagerService(SessionManager sessionManager) {
            return new AdminManagerImpl(sessionManager);
        }

        @Bean
        public RoleManager roleManagerService(SessionManager sessionManager) {
            return new RoleManagerImpl(sessionManager);
        }
    }

    private static Options createCommandLineOptions()
    {
        Options options = new Options();

        options.addOption(Option.builder(HELP_OPTION_SHORT).longOpt(HELP_OPTION_LONG).desc("Show help").build());

        options.addOption(Option.builder().longOpt(DB_DRIVER_CLASS_OPTION).argName("class").hasArg().required().
                desc("The database driver class").build());

        options.addOption(Option.builder().longOpt(DB_URL_OPTION).argName("url").hasArg().required().
                desc("The database JDBC url").build());

        options.addOption(Option.builder().longOpt(DB_USERNAME_OPTION).argName("username").hasArg().required().
                desc("The database JDBC authentication username").build());

        options.addOption(Option.builder().longOpt(DB_PASSWORD_OPTION).argName("password").hasArg().required().
                desc("The database JDBC authentication password").build());

        options.addOption(Option.builder().longOpt(DB_DIALECT_OPTION).argName("dialect").hasArg().
                desc("The database dialect (default: auto detection)").build());

        options.addOption(Option.builder().longOpt(DB_PHYSICAL_NAMING_STRATEGY_CLASS_NAME_OPTION).argName("strategy").hasArg().
                desc("The optional naming strategy").build());

        options.addOption(Option.builder().longOpt(ADMIN_NAME_OPTION).argName("name").hasArg().
                desc("Name of admin").build());

        options.addOption(Option.builder().longOpt(ADMIN_PASSWORD_OPTION).argName("password").hasArg().
                desc("Password of admin").build());

        options.addOption(Option.builder().longOpt(ROLE_OPTION).argName("name").hasArg().
                desc("Admin role (default: Admin)").build());

        options.addOption(Option.builder().longOpt(ADMIN_IP_ADDRESSES_OPTION).argName("ip-range").hasArg().
                desc("The optional authorized IP range for the amin").build());

        options.addOption(Option.builder().longOpt(SET_ADMIN_CREDENTIALS_OPTION).
                desc("Set credentials for the admin (creates one if the admin does not yet exist)").build());

        return options;
    }

    private static void setAdminCredentials()
    throws MissingArgumentException
    {
        if (!commandLine.hasOption(ADMIN_NAME_OPTION)) {
            throw new MissingArgumentException(ADMIN_NAME_OPTION);
        }

        if (!commandLine.hasOption(ADMIN_PASSWORD_OPTION)) {
            throw new MissingArgumentException(ADMIN_PASSWORD_OPTION);
        }

        if (commandLine.hasOption(ADMIN_IP_ADDRESSES_OPTION) && !new IPAddressString(commandLine.getOptionValue(
                ADMIN_IP_ADDRESSES_OPTION)).isValid())
        {
                throw new IllegalArgumentException(String.format(
                        "IP address range %s is invalid", commandLine.getOptionValue(ADMIN_IP_ADDRESSES_OPTION)));
        }

        String adminName = commandLine.getOptionValue(ADMIN_NAME_OPTION);
        String adminPassword = commandLine.getOptionValue(ADMIN_PASSWORD_OPTION);
        String ipAddresses = StringUtils.trimToNull(commandLine.getOptionValue(ADMIN_IP_ADDRESSES_OPTION));
        String role = Optional.ofNullable(StringUtils.trimToNull(commandLine.getOptionValue(ROLE_OPTION))).
                orElse(DEFAULT_ROLE_NAME);

        try (AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(
                LocalServices.class))
        {
            TransactionOperations transactionOperations = applicationContext.getBean(TransactionOperations.class);

            transactionOperations.executeWithoutResult(status ->
            {
                AdminManager adminManager = applicationContext.getBean(AdminManager.class);
                RoleManager roleManager = applicationContext.getBean(RoleManager.class);

                // add role if the role does not exist
                if (roleManager.getRole(role) == null) {
                    roleManager.persistRole(roleManager.createRole(role));
                }

                Admin admin = adminManager.getAdmin(adminName);

                if (admin == null)
                {
                    admin = adminManager.createAdmin(adminName);
                    adminManager.persistAdmin(admin);
                }

                admin.setPassword(Argon2PasswordEncoder.defaultsForSpringSecurity_v5_8().encode(adminPassword));
                admin.setAuthenticationType(AuthenticationType.USERNAME_PASSWORD);

                admin.getRoles().clear();
                admin.getRoles().add(Optional.ofNullable(roleManager.getRole(role)).orElseThrow(
                        () -> new IllegalArgumentException(String.format("Role %s does not exist", role))));

                if (commandLine.hasOption(ADMIN_IP_ADDRESSES_OPTION)) {
                    admin.setIpAddresses(ipAddresses != null ? List.of(ipAddresses) : Collections.emptyList());
                }
            });
        }
    }

    private static void handleCommandline(String[] args)
    throws Exception
    {
        CommandLineParser parser = new DefaultParser();

        Options options = createCommandLineOptions();

        HelpFormatter formatter = new HelpFormatter();

        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, false);

            throw e;
        }

        if (commandLine.getOptions().length == 0 || commandLine.hasOption(HELP_OPTION_SHORT) ||
            commandLine.hasOption(HELP_OPTION_LONG))
        {
            formatter.printHelp(COMMAND_NAME, options, false);

            System.exit(1);

            return;
        }

        if (commandLine.hasOption(SET_ADMIN_CREDENTIALS_OPTION)) {
            setAdminCredentials();
        }
        else {
            formatter.printHelp(COMMAND_NAME, options, false);
        }
    }

    public static void main(String[] args)
    throws Exception
    {
        try {
            AdminDBTool.handleCommandline(args);
        }
        catch (MissingArgumentException | MissingOptionException e)
        {
            System.err.println("Some required parameters are missing: " + e.getMessage());

            System.exit(2);
        }
        catch (UnrecognizedOptionException e)
        {
            System.err.println(e.getMessage());

            System.exit(3);
        }
    }
}
