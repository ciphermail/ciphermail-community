/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.dlp;

import com.ciphermail.core.app.NamedBlobCategories;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.common.dlp.PolicyPatternMarshaller;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.NamedBlobSelectorImpl;
import com.ciphermail.core.common.properties.NamedBlobUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of {@link UserPreferencesPolicyPatternManager} that returns {@link UserPolicyPatternNode} referenced by
 * the {@link UserPreferences}.
 *
 * If policy patterns are specified in the {@link UserPreferences}, those policy patterns
 * will be used. If not, the inherited preferences are checked starting with domains then global etc.
 * All policy patterns are either all inherited from a level higher or not inherited at all.
 *
 * @author Martijn Brinkers
 *
 */
public class UserPreferencesPolicyPatternManagerImpl implements UserPreferencesPolicyPatternManager
{
    private static final Logger logger = LoggerFactory.getLogger(UserPreferencesPolicyPatternManagerImpl.class);

    /*
     * Used for marshalling/unmarshalling PolicyPattern's to/from xml.
     */
    private final PolicyPatternMarshaller policyPatternMarshaller;

    public UserPreferencesPolicyPatternManagerImpl(@Nonnull PolicyPatternMarshaller policyPatternMarshaller) {
        this.policyPatternMarshaller = Objects.requireNonNull(policyPatternMarshaller);
    }

    private Set<PolicyPatternNode> getPatternsInternal(@Nonnull UserPreferences userPreferences,
            Set<UserPreferences> seen, boolean inherited)
    {
        // UserPreferences inherit other UserPreferences. This can result in a loop. If a loop is detected, return.
        if (seen.contains(userPreferences))
        {
            // This can happen because of the way the hierarchical properties work. If the user inherits
            // from a domain and the user, domain and global patterns do not have any patterns,
            // the global setting patterns can be checked multiple times because the domain setting
            // inherit from the global settings.
            if (logger.isDebugEnabled()) {
                logger.info("UserPreferences was already seen (loop). {}", userPreferences);
            }

            return null;
        }

        seen.add(userPreferences);

        Set<PolicyPatternNode> selectedNodes = null;

        // The policy patterns are stored in the NamedBlob's under the NamedBlobCategories#POLICY_PATTERN_CATEGORY
        // category so we should select those categories from the named blobs.
        NamedBlobSelectorImpl selector = new NamedBlobSelectorImpl();

        selector.setCategory(NamedBlobCategories.DLP_POLICY_PATTERN_CATEGORY);

        List<NamedBlob> selectedBlobs = NamedBlobUtils.select(userPreferences.getNamedBlobs(), selector);

        // If policy patterns are specified in the NamedBlob's of the userPreferences, those policy patterns
        // will be used. If not, the inherited preferences are checked starting with domains then global etc.
        // All policy patterns are either all inherited from a level higher or not inherited at all.
        if (selectedBlobs != null)
        {
            selectedNodes = new HashSet<>();

            for (NamedBlob blob : selectedBlobs) {
                selectedNodes.add(new UserPolicyPatternNodeImpl(blob, policyPatternMarshaller, inherited));
            }
        }
        else {
            // The userPreferences does not contain any policy patterns so we need to check the inherited
            // userPreferences.
            //
            // The inherited preferences start with global, then domain etc. because we need to iterate the other
            // way around we need to store them temporarily in a collection.
            Set<UserPreferences> inheritedPreferences = userPreferences.getInheritedUserPreferences();

            LinkedList<UserPreferences> reversed = new LinkedList<>();

            for (UserPreferences pref : inheritedPreferences) {
                reversed.addFirst(pref);
            }

            // Step through the inherited prefs and recursively handle until it returns a non null collection.
            for (UserPreferences pref : reversed)
            {
                selectedNodes = getPatternsInternal(pref, seen, true);

                if (selectedNodes != null) {
                    break;
                }
            }
        }

        return selectedNodes;
    }

    @Override
    public Set<PolicyPatternNode> getPatterns(@Nonnull UserPreferences userPreferences)
    {
        Set<PolicyPatternNode> selected = getPatternsInternal(userPreferences, new HashSet<>(), false);

        if (selected == null) {
            selected = new HashSet<>();
        }

        return selected;
    }

    @Override
    public void setPatterns(@Nonnull UserPreferences userPreferences, Collection<PolicyPatternNode> patterns)
    {
        // The existing NamedNodes from the Pattern collection should be removed before we add the new
        // NamedNodes.
        NamedBlobSelectorImpl selector = new NamedBlobSelectorImpl();

        selector.setCategory(NamedBlobCategories.DLP_POLICY_PATTERN_CATEGORY);

        // Get all NamedBlob's for this userPreferences and remove the Pattern blobs
        Set<NamedBlob> currentNamedBlobs = userPreferences.getNamedBlobs();

        List<NamedBlob> currentPatternBlobs = NamedBlobUtils.select(currentNamedBlobs, selector);

        if (currentPatternBlobs != null) {
            currentPatternBlobs.forEach(currentNamedBlobs::remove);
        }

        if (patterns != null)
        {
            for (PolicyPatternNode pattern : patterns)
            {
                if (!(pattern instanceof PolicyPatternNodeImpl)) {
                    throw new IllegalArgumentException("Pattern must be a PolicyPatternNodeImpl.");
                }

                currentNamedBlobs.add(((PolicyPatternNodeImpl) pattern).getNamedBlob());
            }
        }
   }
}