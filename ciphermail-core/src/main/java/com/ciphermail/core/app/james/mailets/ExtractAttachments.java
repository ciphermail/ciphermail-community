/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MimeUtils;
import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.mail.PartScanner;
import com.ciphermail.core.common.mail.PartScanner.PartListener;
import com.ciphermail.core.common.security.smime.SMIMEHeader;
import com.ciphermail.core.common.security.smime.SMIMEHeader.Strict;
import com.ciphermail.core.common.util.MiscFilenameUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Mailet that reads all the attachments from an email and writes them to a directory.
 */
public class ExtractAttachments extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(ExtractAttachments.class);

    /*
     * The default filename to use if the filename cannot be determined but is a plain/text file
     */
    private static final String DEFAULT_UNKNOWN_TEXT_FILENAME = "attachment.txt";

    /*
     * The default filename to use if the filename cannot be determined
     */
    private static final String DEFAULT_UNKNOWN_ATTACHMENT_FILENAME = "attachment.bin";

    /*
     * The default sender to use if the sender is unknown
     */
    private static final String DEFAULT_UNKNOWN_SENDER = "unknown";

    /*
     * The default filename format
     */
    private static final String DEFAULT_FILENAME_FORMAT = "%s_%s_%s";

    /*
     * Default maximum depth of a mime structure we will scan
     */
    private static final int DEFAULT_MAX_MIME_DEPTH = 8;

    /*
     * If a unique filename cannot be created after max tries, an exception will be thrown
     */
    private static final int MAX_CREATE_FILENAME_TRIES = 1000;

    /*
     * The filename to use if the filename cannot be determined but is a plain/text file
     */
    private String unknownTextFilename = DEFAULT_UNKNOWN_TEXT_FILENAME;

    /*
     * The filename to use if the filename cannot be determined but is a plain/text file
     */
    private String unknownAttachmentFilename = DEFAULT_UNKNOWN_ATTACHMENT_FILENAME;

    /*
     * The sender to use if the sender is unknown
     */
    private String unknownSender = DEFAULT_UNKNOWN_SENDER;

    /*
     * The format string for the generated file
     */
    private String filenameFormat = DEFAULT_FILENAME_FORMAT;

    /*
     * The Mail attribute to use for storing whether there we attachments
     */
    private String attachmentsFoundAttribute;

    /*
     * The Mail attribute to use when there was an error extracting any attachment
     */
    private String errorAttribute;

    /*
     * If true, digital signatures are skipped
     */
    private boolean skipSignatures = true;

    /*
     * Extract the attachment even if content-disposition is not set to attachment but filename is set
     */
    private boolean alwaysExtractIfFilenameSet;

    /*
     * If true, a folder for each recipient will be created in which the extracted attachments will be stored
     */
    private boolean createRecipientDir;

    /*
     * The next processor if an attachment was extracted
     */
    private String extractedProcessor;

    /*
     * Default maximum depth of a mime structure we will scan
     */
    private int maxMIMEDepth = DEFAULT_MAX_MIME_DEPTH;

    /*
     * The directory where attachments will be stored.
     */
    private File attachmentsDir;

    /*
     * Gets called for every MIME part
     */
    private final PartListener partListener = new PartListenerImpl();

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        ATTACHMENTS_DIR                ("attachmentsDir"),
        UNKNOWN_TEXT_FILENAME          ("unknownTextFilename"),
        UNKNOWN_ATTACHMENT_FILENAME    ("unknownAttachmentFilename"),
        UNKNOWN_SENDER                 ("unknownSender"),
        FILENAME_FORMAT                ("filenameFormat"),
        ATTACHMENTS_FOUND_ATTRIBUTE    ("attachmentsFoundAttribute"),
        ERROR_ATTRIBUTE                ("errorAttribute"),
        SKIP_SIGNATURES                ("skipSignatures"),
        ALWAYS_EXTRACT_IF_FILENAME_SET ("alwaysExtractIfFilenameSet"),
        CREATE_RECIPIENT_DIR           ("createRecipientDir"),
        EXTRACTED_PROCESSOR            ("extractedProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * Internal context class to store information
     */
    private static class Context
    {
        /*
         * True if any attachment was found
         */
        private boolean attachmentsFound;

        /*
         * True if an error while extracting attachments ocurred
         */
        private boolean error;

        /*
         * The MIME message
         */
        private MimeMessage message;

        /*
         * The recipients of the message
         */
        private List<MailAddress> recipients;

        boolean isAttachmentsFound() {
            return attachmentsFound;
        }

        void setAttachmentsFound(boolean attachmentsFound) {
            this.attachmentsFound = attachmentsFound;
        }

        boolean isError() {
            return error;
        }

        void setError(boolean error) {
            this.error = error;
        }

        MimeMessage getMessage() {
            return message;
        }

        void setMessage(MimeMessage message) {
            this.message = message;
        }

        public List<MailAddress> getRecipients() {
            return recipients;
        }

        public void setRecipients(List<MailAddress> recipients) {
            this.recipients = recipients;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    private void initializeAttachmentsDir()
    {
        String param = StringUtils.trimToNull(getInitParameter(Parameter.ATTACHMENTS_DIR.name));

        if (param == null) {
            throw new IllegalArgumentException("attachmentsDir is required.");
        }

        attachmentsDir = new File(param);

        if (!attachmentsDir.exists() || !attachmentsDir.isDirectory()) {
            throw new IllegalArgumentException(attachmentsDir + " does not exist or is not a directory.");
        }

        if (!attachmentsDir.canWrite()) {
            throw new IllegalArgumentException(attachmentsDir + " is not writable.");
        }
    }

    @Override
    public final void initMailet()
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        initializeAttachmentsDir();

        unknownAttachmentFilename = getInitParameter(Parameter.UNKNOWN_ATTACHMENT_FILENAME.name,
                DEFAULT_UNKNOWN_ATTACHMENT_FILENAME);

        unknownTextFilename = getInitParameter(Parameter.UNKNOWN_TEXT_FILENAME.name, DEFAULT_UNKNOWN_TEXT_FILENAME);

        unknownSender = getInitParameter(Parameter.UNKNOWN_SENDER.name, DEFAULT_UNKNOWN_SENDER);

        filenameFormat = getInitParameter(Parameter.FILENAME_FORMAT.name, DEFAULT_FILENAME_FORMAT);

        attachmentsFoundAttribute = getInitParameter(Parameter.ATTACHMENTS_FOUND_ATTRIBUTE.name);

        errorAttribute = getInitParameter(Parameter.ERROR_ATTRIBUTE.name);

        skipSignatures = getBooleanInitParameter(Parameter.SKIP_SIGNATURES.name, skipSignatures);

        alwaysExtractIfFilenameSet = getBooleanInitParameter(Parameter.ALWAYS_EXTRACT_IF_FILENAME_SET.name,
                alwaysExtractIfFilenameSet);

        createRecipientDir = getBooleanInitParameter(Parameter.CREATE_RECIPIENT_DIR.name, createRecipientDir);

        extractedProcessor = getInitParameter(Parameter.EXTRACTED_PROCESSOR.name);

        StrBuilder sb = new StrBuilder();

        sb.append("attachmentsDir: ");
        sb.append(attachmentsDir);
        sb.append("; unknownAttachmentFilename: ");
        sb.append(unknownAttachmentFilename);
        sb.append("; unknownTextFilename: ");
        sb.append(unknownTextFilename);
        sb.append("; unknownSender: ");
        sb.append(unknownSender);
        sb.append("; filenameFormat: ");
        sb.append(filenameFormat);
        sb.append("; attachmentsFoundAttribute: ");
        sb.append(attachmentsFoundAttribute);
        sb.append("; errorAttribute: ");
        sb.append(errorAttribute);
        sb.append("; skipSignatures: ");
        sb.append(skipSignatures);
        sb.append("; alwaysExtractIfFilenameSet: ");
        sb.append(alwaysExtractIfFilenameSet);
        sb.append("; createRecipientDir: ");
        sb.append(createRecipientDir);
        sb.append("; extractedProcessor: ");
        sb.append(extractedProcessor);

        getLogger().info("{}", sb);
    }

    private boolean isExtractAttachment(Part part)
    throws MessagingException
    {
        // Skip S/MIME signature parts
        if (skipSignatures && SMIMEHeader.Type.CLEAR_SIGNED == SMIMEHeader.getSMIMEContentType(part, Strict.YES)) {
            return false;
        }

        // Assume it's an attachment if the Content-Disposition is equal to attachment
        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
            return true;
        }

        // If disposition is not attachment
        if (alwaysExtractIfFilenameSet && getAttachmentFilename(part, null) != null) {
            return true;
        }

        return false;
    }

    private String getAttachmentFilename(Part part, String defaultIfEmpty)
    {
        String filename = HeaderUtils.decodeTextQuietly(MimeUtils.getFilenameQuietly(part));

        if (StringUtils.isEmpty(filename))
        {
            try {
                filename = part.getDescription();
            }
            catch (MessagingException e) {
                // ignore
            }

            if (StringUtils.isEmpty(filename)) {
                filename = defaultIfEmpty;
            }
        }

        return filename;
    }

    private String getDefaultAttachmentNameIfUnknown(Part part)
    {
        String name = unknownAttachmentFilename;

        try {
            if (part.isMimeType("text/plain")) {
                name = unknownTextFilename;
            }
        }
        catch (MessagingException e) {
            // ignore
        }

        return name;
    }

    private String getAttachmentFilename(Part part) {
        return getAttachmentFilename(part, getDefaultAttachmentNameIfUnknown(part));
    }

    private void writeAttachment(MimeMessage message, List<MailAddress> recipients,  Part part)
    throws IOException, MessagingException
    {
        String sender = EmailAddressUtils.getEmailAddress(EmailAddressUtils.getAddress(
                EmailAddressUtils.getFromQuietly(message)));

        if (StringUtils.isEmpty(sender)) {
            sender = unknownSender;
        }

        String attachmentFilename = getAttachmentFilename(part);

        if (createRecipientDir)
        {
            for (MailAddress recipient : recipients)
            {
                File baseDir = new File(attachmentsDir, MiscFilenameUtils.safeEncodeFilenameBlacklist(
                        recipient.toString(), true));

                FileUtils.forceMkdir(baseDir);

                writePart(part, sender, attachmentFilename, baseDir);
            }
        }
        else {
            writePart(part, sender, attachmentFilename, attachmentsDir);
        }
    }

    private void writePart(Part part, String sender, String attachmentFilename, File baseDir)
    throws IOException, MessagingException
    {
        File file = null;

        for (int i = 0; i < MAX_CREATE_FILENAME_TRIES; i++)
        {
            file = new File(baseDir, MiscFilenameUtils.safeEncodeFilenameBlacklist(String.format(filenameFormat,
                    System.currentTimeMillis(), sender, attachmentFilename), true ));

            if (file.createNewFile()) {
                // A unique file was created
                break;
            }

            // File already exists, reset
            file = null;
        }

        if (file == null) {
            throw new IOException("A attachment filename could not be created.");
        }

        FileOutputStream output = new FileOutputStream(file);

        try {
            logger.info("Writing attachment: {}", file);

            IOUtils.copy(part.getInputStream(), output);
        }
        finally {
            IOUtils.closeQuietly(output);
        }
    }

    @Override
    public void serviceMail(Mail mail)
    {
        Context context = new Context();

        try {
            PartScanner partScanner = new PartScanner(partListener, maxMIMEDepth);

            MimeMessage message = mail.getMessage();

            context.setMessage(message);
            context.setRecipients(MailAddressUtils.getRecipients(mail));

            partScanner.scanPart(message, context);
        }
        catch (Exception e) {
            logger.error("Error extracting attachments", e);
        }

        if (attachmentsFoundAttribute != null) {
            mail.setAttribute(Attribute.convertToAttribute(attachmentsFoundAttribute, context.isAttachmentsFound()));
        }

        if (errorAttribute != null) {
            mail.setAttribute(Attribute.convertToAttribute(errorAttribute, context.isError()));
        }

        if (StringUtils.isNotEmpty(extractedProcessor) && context.isAttachmentsFound()) {
            mail.setState(extractedProcessor);
        }
    }

    /*
     * Gets called for every MIME part and writes the part if it's an attachment
     */
    private class PartListenerImpl implements PartListener
    {
        @Override
        public boolean onPart(Part parent, Part part, Object ctx)
        throws PartException
        {
            Context context = (Context) ctx;

            try {
                if (logger.isDebugEnabled())
                {
                    logger.debug("MIME part. Content-Type: {}, Content-Disposition: {}", part.getContentType(),
                            part.getDisposition());
                }

                if (isExtractAttachment(part))
                {
                    context.setAttachmentsFound(true);

                    writeAttachment(context.getMessage(), context.getRecipients(), part);
                }
            }
            catch (Exception e)
            {
                logger.error("Error handling part", e);

                context.setError(true);
            }

            return true;
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (extractedProcessor != null) {
            requiredProcessors.add(new ProcessingState(extractedProcessor));
        }

        return requiredProcessors;
    }
}
