/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.james.PGPPublicKeys;
import com.ciphermail.core.common.security.openpgp.PGPCompressionAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPEncryptionAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPMIMEEncryptionBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Mailet which encrypts the email with PGP/MIME
 */
public class PGPMIMEEncrypt extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(PGPMIMEEncrypt.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        ENCRYPTION_ALGORITHM            ("encryptionAlgorithm"),
        ENCRYPTION_ALGORITHM_ATTRIBUTE  ("encryptionAlgorithmAttribute"),
        COMPRESSION_ALGORITHM           ("compressionAlgorithm"),
        COMPRESSION_ALGORITHM_ATTRIBUTE ("compressionAlgorithmAttribute"),
        ADD_INTEGRITY_PACKET            ("addIntegrityPacket"),
        ADD_INTEGRITY_PACKET_ATTRIBUTE  ("addIntegrityPacketAttribute"),
        RETAIN_MESSAGE_ID               ("retainMessageID"),
        DISPOSITION_INLINE              ("dispositionInline"),
        ENCRYPTED_PROCESSOR             ("encryptedProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * The encryption algorithm to use
     */
    private PGPEncryptionAlgorithm encryptionAlgorithm = PGPEncryptionAlgorithm.AES_128;

    /*
     * The Mail attribute from which the encryption algorithm is read. If null or if there is no
     * attribute with the given name, the encryptionAlgorithm is used.
     */
    private String encryptionAlgorithmAttribute;

    /*
     * The compression algorithm to use
     */
    private PGPCompressionAlgorithm compressionAlgorithm = PGPCompressionAlgorithm.ZLIB;

    /*
     * The Mail attribute from which the compression algorithm is read. If null or if there is no
     * attribute with the given name, the compressionAlgorithm is used.
     */
    private String compressionAlgorithmAttribute;

    /*
     * The Mail attribute from which the addIntegrityPacket value is read. If null or if there is no
     * attribute with the given name, the addIntegrityPacket value is used.
     */
    private String addIntegrityPacketAttribute;

    /*
     * If true an integrity packet will be added to the PGP encrypted message
     */
    private boolean addIntegrityPacket = true;

    /*
     * If true the original Message-ID will be used for the encrypted message
     */
    private boolean retainMessageID = true;

    /*
     * If true, the "OpenPGP encrypted message" blob will be attached with content disposition inline. Enigmail
     * set the content disposition to inline. Also Mailvelope (PGP browser plugin) will only support PGP/MIME if
     * the content disposition is set to inline.
     */
    private boolean dispositionInline = true;

    /*
     * The next processor if enryption succeeded. If null, the default next processor will be used (i.e,
     * the current configured processor will be used)
     */
    private String encryptedProcessor;

    @Override
    public void initMailet()
    throws MessagingException
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        String param = getInitParameter(Parameter.ENCRYPTION_ALGORITHM.name);

        if (param != null)
        {
            encryptionAlgorithm = Objects.requireNonNull(PGPEncryptionAlgorithm.fromName(param),
                    param + " is not a valid encryption algorithm");
        }

        encryptionAlgorithmAttribute = getInitParameter(Parameter.ENCRYPTION_ALGORITHM_ATTRIBUTE.name);

        param = getInitParameter(Parameter.COMPRESSION_ALGORITHM.name);

        if (param != null)
        {
            compressionAlgorithm = Objects.requireNonNull(PGPCompressionAlgorithm.fromName(param),
                    param + " is not a valid compression algorithm");
        }

        compressionAlgorithmAttribute = getInitParameter(Parameter.COMPRESSION_ALGORITHM_ATTRIBUTE.name);

        addIntegrityPacket = getBooleanInitParameter(Parameter.ADD_INTEGRITY_PACKET.name, addIntegrityPacket);
        addIntegrityPacketAttribute = getInitParameter(Parameter.ADD_INTEGRITY_PACKET_ATTRIBUTE.name);

        retainMessageID = getBooleanInitParameter(Parameter.RETAIN_MESSAGE_ID.name, retainMessageID);

        dispositionInline = getBooleanInitParameter(Parameter.DISPOSITION_INLINE.name, dispositionInline);

        encryptedProcessor = getInitParameter(Parameter.ENCRYPTED_PROCESSOR.name);

        StrBuilder sb = new StrBuilder();

        sb.append("encryptionAlgorithm: ");
        sb.append(encryptionAlgorithm);
        sb.append("; ");
        sb.append("encryptionAlgorithmAttribute: ");
        sb.append(encryptionAlgorithmAttribute);
        sb.append("; ");
        sb.append("compressionAlgorithm: ");
        sb.append(compressionAlgorithm);
        sb.append("; ");
        sb.append("compressionAlgorithmAttribute: ");
        sb.append(compressionAlgorithmAttribute);
        sb.append("; ");
        sb.append("addIntegrityPacket: ");
        sb.append(addIntegrityPacket);
        sb.append("; ");
        sb.append("addIntegrityPacketAttribute: ");
        sb.append(addIntegrityPacketAttribute);
        sb.append("; ");
        sb.append("retainMessageID: ");
        sb.append(retainMessageID);
        sb.appendSeparator("; ");
        sb.append("dispositionInline: ");
        sb.append(dispositionInline);
        sb.appendSeparator("; ");
        sb.append("Encrypted processor: ");
        sb.append(encryptedProcessor);

        getLogger().info("{}", sb);
    }

    private PGPEncryptionAlgorithm getEncryptionAlgorithm(Mail mail)
    {
        PGPEncryptionAlgorithm algorithm = null;

        // Check if the Mail attribute contains the signing algorithm
        if (StringUtils.isNotEmpty(encryptionAlgorithmAttribute))
        {
            Optional<String> attributeValue = MailAttributesUtils.getManagedAttributeValue(mail,
                    encryptionAlgorithmAttribute, String.class);

            if (attributeValue.isPresent())
            {
                algorithm = PGPEncryptionAlgorithm.fromName(attributeValue.get());

                if (algorithm == null) {
                    getLogger().warn("The attribute value {} is not a valid encryption algorithm.", attributeValue);
                }
            }
            else {
                getLogger().debug("Attribute with name {} was not found", encryptionAlgorithmAttribute);
            }
        }

        if (algorithm == null) {
            // Use the default algorithm
            algorithm = this.encryptionAlgorithm;
        }

        getLogger().debug("Encryption algorithm: {}", algorithm);

        return algorithm;
    }

    private PGPCompressionAlgorithm getCompressionAlgorithm(Mail mail)
    {
        PGPCompressionAlgorithm algorithm = null;

        // Check if the Mail attribute contains the signing algorithm
        if (StringUtils.isNotEmpty(compressionAlgorithmAttribute))
        {
            Optional<String> attributeValue = MailAttributesUtils.getManagedAttributeValue(mail,
                    compressionAlgorithmAttribute, String.class);

            if (attributeValue.isPresent())
            {
                algorithm = PGPCompressionAlgorithm.fromName(attributeValue.get());

                if (algorithm == null)
                {
                    getLogger().warn("The attribute value {} is not a valid compression algorithm.",
                            attributeValue);
                }
            }
            else {
                getLogger().debug("Attribute with name {} was not found", compressionAlgorithmAttribute);
            }
        }

        if (algorithm == null) {
            // Use the default algorithm
            algorithm = this.compressionAlgorithm;
        }

        getLogger().debug("Compression algorithm: {}", algorithm);

        return algorithm;
    }

    private boolean getAddIntegrityPacket(Mail mail)
    {
        // Get value from the mail attribute
        boolean addPacket = MailAttributesUtils.attributeAsBoolean(mail, addIntegrityPacketAttribute,
                    this.addIntegrityPacket);

        getLogger().debug("addIntegrityPacket: {}", addPacket);

        return addPacket;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        // The PGP public keys should be stored in the Mail attributes
        PGPPublicKeys publicKeysAttribute = CoreApplicationMailAttributes.getPGPPublicKeys(mail);

        if (publicKeysAttribute != null)
        {
            List<PGPPublicKey> publicKeys = publicKeysAttribute.getPublicKeys();

            if (CollectionUtils.isNotEmpty(publicKeys))
            {
                PGPMIMEEncryptionBuilder encryptionBuilder = new PGPMIMEEncryptionBuilder();

                encryptionBuilder.setEncryptionAlgorithm(getEncryptionAlgorithm(mail));
                encryptionBuilder.setCompressionAlgorithm(getCompressionAlgorithm(mail));
                encryptionBuilder.setAddIntegrityPacket(getAddIntegrityPacket(mail));
                encryptionBuilder.setRetainMessageId(retainMessageID);
                encryptionBuilder.setDispositionInline(dispositionInline);

                try {
                    MimeMessage encryptedMessage = encryptionBuilder.encrypt(mail.getMessage(), publicKeys);

                    if (encryptedMessage != null)
                    {
                        getLogger().info("Message was PGP/MIME encrypted. Encryption algorithm: {}; " +
                        		"Compression algorithm: {}; Add integrity packet: {}; MailID: {}; Recipients: {}",
                                encryptionBuilder.getEncryptionAlgorithm().getFriendlyName(),
                                encryptionBuilder.getCompressionAlgorithm().getFriendlyName(),
                                encryptionBuilder.isAddIntegrityPacket(),
                                CoreApplicationMailAttributes.getMailID(mail),
                                mail.getRecipients());

                        mail.setMessage(encryptedMessage);

                        if (StringUtils.isNotEmpty(encryptedProcessor)) {
                            mail.setState(encryptedProcessor);
                        }
                    }
                    else {
                        // Should normally not happen
                        getLogger().warn("Message was not PGP encrypted");
                    }
                }
                catch (IOException | MessagingException | PGPException e) {
                    getLogger().error("Error PGP encryping the message", e);
                }
            }
            else {
                getLogger().warn("PGP public keys container was empty.");
            }
        }
        else {
            getLogger().warn("PGP public keys container was not found in the Mail attributes.");
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (encryptedProcessor != null) {
            requiredProcessors.add(new ProcessingState(encryptedProcessor));
        }

        return requiredProcessors;
    }
}
