/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.core.app.acme;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

/**
 * The AcmeSettings interface defines methods to manage the settings required for communication
 * with an ACME (Automated Certificate Management Environment) server.
 */
public interface AcmeSettings
{
    /**
     *  URI string of the ACME server to connect to. This is either the location of the CA's ACME directory
     *  (using http or https protocol), or a special URI (using the acme protocol).
     */
    void setServerUri(String serverUri)
    throws HierarchicalPropertiesException;

    /**
     *  URI string of the ACME server to connect to. This is either the location of the CA's ACME directory
     *  (using http or https protocol), or a special URI (using the acme protocol).
     */
    String getServerUri()
    throws HierarchicalPropertiesException;

    /**
     * Set the PEM encoded account keypair
     */
    void setAccountKeyPair(String pemEncodedKeyPair)
    throws HierarchicalPropertiesException;

    /**
     * Get the PEM encoded account keypair
     */
    String getAccountKeyPair()
    throws HierarchicalPropertiesException;

    /**
     * Set the account email address
     */
    void setAccountEmail(String accountEmail)
    throws HierarchicalPropertiesException;

    /**
     * Get the account email address
     */
    String getAccountEmail()
    throws HierarchicalPropertiesException;

    /**
     * Set the KID. Use this if your CA requires an individual account identification (e.g. your customer number) and
     * a shared secret for registration. See the documentation of your CA about how to retrieve the key identifier
     * and MAC key.
     */
    void setKeyIdentifier(String kid)
    throws HierarchicalPropertiesException;

    /**
     * Get the KID. Use this if your CA requires an individual account identification (e.g. your customer number) and
     * a shared secret for registration. See the documentation of your CA about how to retrieve the key identifier
     * and MAC key.
     */
    String getKeyIdentifier()
    throws HierarchicalPropertiesException;

    /**
     * Set the encoded MAC key. Use this if your CA requires an individual account identification (e.g. your customer
     * number) and a shared secret for registration. See the documentation of your CA about how to retrieve the key
     * identifier and MAC key.
     */
    void setEncodedMacKey(String encodedMacKey)
    throws HierarchicalPropertiesException;

    /**
     * Get the encoded MAC key. Use this if your CA requires an individual account identification (e.g. your customer
     * number) and a shared secret for registration. See the documentation of your CA about how to retrieve the key
     * identifier and MAC key.
     */
    String getEncodedMacKey()
    throws HierarchicalPropertiesException;

    /**
     * Set the token-challenge cache as a json encoded {@link AcmeTokenCache} object
     */
    void setTokenCacheJSON(String tokenCacheJSON)
    throws HierarchicalPropertiesException;

    /**
     * Get the token-challenge cache as a json encoded {@link AcmeTokenCache} object
     */
    String getTokenCacheJSON()
    throws HierarchicalPropertiesException;

    /**
     * Set to true if the Terms Of Service of the acme provider is accepted
     */
    void setTosAccepted(Boolean accepted)
    throws HierarchicalPropertiesException;

    /**
     * Returns true if the Terms Of Service of the acme provider is accepted
     */
    Boolean getTosAccepted()
    throws HierarchicalPropertiesException;

    /**
     * Set to true if ACME certificates should be auto-renewed
     */
    void setAutoRenewalEnabled(Boolean autoRenewalEnabled)
    throws HierarchicalPropertiesException;

    /**
     * Returns true if ACME certificates should be auto-renewed
     */
    Boolean getAutoRenewalEnabled()
    throws HierarchicalPropertiesException;
}
