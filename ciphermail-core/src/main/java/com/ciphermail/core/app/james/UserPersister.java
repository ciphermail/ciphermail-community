/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.workflow.UserWorkflow;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

/**
 * Helper class for making non-persistent users persistent.
 */
public class UserPersister
{
    private static final Logger logger = LoggerFactory.getLogger(UserPersister.class);

    /*
     * Used for adding and retrieving users
     */
    private final UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    private final TransactionOperations transactionOperations;

    public static UserPersister getInstance(UserWorkflow userWorkflow, TransactionOperations transactionOperations) {
        return new UserPersister(userWorkflow, transactionOperations);
    }

    private UserPersister(@Nonnull UserWorkflow userWorkflow, @Nonnull TransactionOperations transactionOperations)
    {
        this.userWorkflow = Objects.requireNonNull(userWorkflow);
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
    }

    /**
     * Persists users that are not yet persistent. Each user is made persistent in a separate
     * transaction to make sure that if one user cannot be persisted it does not influence other users.
     * This method does not guarantee that users are persisted.
     */
    public void tryToMakeNonPersistentUsersPersistent(Collection<User> users)
    {
        for (User nonPersistentUser : getNonPersistentUsers(users))
        {
            try {
                transactionOperations.executeWithoutResult(status ->
                    userWorkflow.makePersistent(nonPersistentUser)
                );
            }
            catch(ConstraintViolationException e)
            {
                logger.error("Error persisting user {}. The user was probably already persisted. " +
                        "Skipping this user.", nonPersistentUser.getEmail(), e);
            }
        }
    }

    private Collection<User> getNonPersistentUsers(Collection<User> users)
    {
        Collection<User> nonPersistentUsers = new HashSet<>();

        if (users != null)
        {
            for (User user : users)
            {
                if (!userWorkflow.isPersistent(user)) {
                    nonPersistentUsers.add(user);
                }
            }
        }

        return nonPersistentUsers;
    }
}
