/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.JamestUtils;
import com.ciphermail.core.app.james.MailAddressUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Mailet that splits the email into a separate email for every recipient
 */
public class SingleRecipientMailSplitter extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SingleRecipientMailSplitter.class);

    /*
     * If true, the original message will be retained
     */
    private boolean passThrough;

    /*
     * The processor to use for the separated message(s)
     */
    private String processor;

    /*
     * If true the original Message-ID will be used for the signed message
     */
    private boolean retainMessageID;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        PASS_THROUGH      ("passThrough"),
        PROCESSOR         ("processor"),
        RETAIN_MESSAGE_ID ("retainMessageID");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public final void initMailet()
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        passThrough = getBooleanInitParameter(Parameter.PASS_THROUGH.name, true);

        processor = Objects.requireNonNull(getInitParameter(Parameter.PROCESSOR.name),
                Parameter.PROCESSOR.name +  " parameter is missing");

        retainMessageID = getBooleanInitParameter(Parameter.RETAIN_MESSAGE_ID.name, true);

        StrBuilder sb = new StrBuilder();

        sb.append("passThrough: ");
        sb.append(passThrough);
        sb.append("; processor: ");
        sb.append(processor);
        sb.append("; retainMessageID: ");
        sb.append(retainMessageID);

        getLogger().info("{}", sb);
    }

    @Override
    public void serviceMail(Mail mail)
    {
        List<MailAddress> recipients = MailAddressUtils.getRecipients(mail);

        try {
            MimeMessage message = mail.getMessage();

            for (MailAddress recipient : recipients)
            {
                MailImpl clone = MailImpl.duplicateWithoutMessage(mail).build();

                try {
                    MimeMessage clonedMessage = new MimeMessage(message);

                    if (!retainMessageID) {
                        // Need to save changes to update the messageID
                        clonedMessage.saveChanges();
                    }

                    clone.setMessage(clonedMessage);
                    clone.setRecipients(List.of(recipient));
                    clone.setState(processor);

                    getMailetContext().sendMail(clone);
                }
                finally {
                    JamestUtils.dispose(clone);
                }
            }

            if (!passThrough) {
                mail.setState(Mail.GHOST);
            }
        }
        catch (Exception e) {
            logger.error("There was an error splitting the email", e);
        }
    }
    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (processor != null) {
            requiredProcessors.add(new ProcessingState(processor));
        }

        return requiredProcessors;
    }
}
