/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app;

import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.util.CloseableIterator;

import javax.annotation.Nonnull;

/**
 * Manages UserPreferences @See {@link UserPreferences}
 *
 * @author Martijn Brinkers
 *
 */
public interface UserPreferencesManager
{
    /**
     * Add a new UserPreferences.
     */
    @Nonnull UserPreferences addUserPreferences(@Nonnull String name);

    /**
     * Deletes the UserPreferences. Returns true if UserPreferences was deleted.
     */
    boolean deleteUserPreferences(@Nonnull UserPreferences userPreferences);

    /**
     * Returns true if the userPreferences is in use and cannot be deleted.
     */
    boolean isInUse(@Nonnull UserPreferences userPreferences);

    /**
     * Returns the UserPreferences with given name. Returns null if UserPreferences does not exist.
     */
    UserPreferences getUserPreferences(@Nonnull String name);

    /**
     * Returns the number of UserPreferences
     */
    long getUserPreferencesCount();

    /**
     * Returns an iterator that can be used to get all the names of the UserPreferences.
     * This iterator must be manually closed.
     */
    CloseableIterator<String> getNameIterator();

    /**
     * Returns an iterator that can be used to get all the names of the UserPreferences.
     * This iterator must be manually closed.
     *
     * Note: The returned iterator is stateful, i.e., if the implementation uses a database connection,
     * a new transaction is not started. The downside of a stateful iterator is that it might consume
     * more memory when a large number of items is returned.
     */
    CloseableIterator<String> getStatefulNameIterator();

    /**
     * Returns an iterator that can be used to get all the names of the UserPreferences.
     * This iterator must be manually closed.
     */
    CloseableIterator<String> getNameIterator(Integer firstResult, Integer maxResults,
            SortDirection sortDirection);
}
