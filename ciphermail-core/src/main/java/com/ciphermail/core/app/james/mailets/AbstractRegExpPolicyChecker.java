/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.common.dlp.MimeMessageTextExtractor;
import com.ciphermail.core.common.dlp.PolicyChecker;
import com.ciphermail.core.common.dlp.PolicyCheckerContext;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.dlp.PolicyViolation;
import com.ciphermail.core.common.dlp.PolicyViolationException;
import com.ciphermail.core.common.dlp.TextNormalizer;
import com.ciphermail.core.common.dlp.impl.PolicyCheckerContextImpl;
import com.ciphermail.core.common.extractor.ExtractedPart;
import com.ciphermail.core.common.extractor.impl.TextExtractorUtils;
import com.ciphermail.core.common.util.SizeUtils;
import com.ciphermail.core.common.util.StringIterator;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Abstract base class for (DLP) RegExpPolicyChecker mailets.
 */
@SuppressWarnings({"java:S6813"})
public abstract class AbstractRegExpPolicyChecker extends AbstractCipherMailMailet
{
    /*
     * As a sanity measure, the length of the error message stored in Mail attributes
     * will be limited to this length.
     */
    private static final int MAX_POLICY_VIOLATION_ERROR_MESSAGE_LENGTH = 1024;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        WARN_PROCESSOR             ("warnProcessor"),
        MUST_ENCRYPT_PROCESSOR     ("mustEncryptProcessor"),
        QUARANTINE_PROCESSOR       ("quarantineProcessor"),
        BLOCK_PROCESSOR            ("blockProcessor"),
        ERROR_PROCESSOR            ("errorProcessor"),
        DELAY_EVALUATION_PROCESSOR ("delayEvaluationProcessor"),
        MAX_STRING_LENGTH          ("maxStringLength"),
        MAX_INIT_BUFFER_SIZE       ("maxInitBufferSize");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * is used to normalize the extracted text (lowercase, remove words on skip list etc.)
     */
    @Inject
    private TextNormalizer textNormalizer;

    /*
     * The policy checker pipeline
     */
    @Inject
    private PolicyChecker policyChecker;

    /*
     * Used to extract the text from the email message.
     */
    @Inject
    private MimeMessageTextExtractor textExtractor;

    /*
     * The next processor when the policy was violated with WARN action.
     */
    private String warnProcessor;

    /*
     * The next processor when the policy was violated with MUST_ENCRYPT action.
     */
    private String mustEncryptProcessor;

    /*
     * The next processor when the policy was violated with QUARANTINE action.
     */
    private String quarantineProcessor;

    /*
     * The next processor when the policy was violated with BLOCK action.
     */
    private String blockProcessor;

    /*
     * The next processor when an error occurred when checking the policy
     */
    private String errorProcessor;

    /*
     * The processor to use when the policy evaluation is delayed
     */
    private String delayEvaluationProcessor;

    /*
     * The max length of the a string returned by the string iterator
     */
    private int maxStringLength = SizeUtils.KB * 512;

    /*
     * The max initial size of the buffer used by the string iterator
     */
    private int maxInitBufferSize = SizeUtils.KB * 64;

    private String getMandatoryInitParameter(String name)
    {
        String value = getInitParameter(name);

        if (StringUtils.isBlank(value)) {
            throw new IllegalArgumentException("Init parameter " + name + " is missing.");
        }

        return value;
    }

    @Override
    public void initMailet()
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        Objects.requireNonNull(textNormalizer);
        Objects.requireNonNull(policyChecker);
        Objects.requireNonNull(textExtractor);

        warnProcessor = getMandatoryInitParameter(Parameter.WARN_PROCESSOR.name);
        mustEncryptProcessor = getMandatoryInitParameter(Parameter.MUST_ENCRYPT_PROCESSOR.name);
        quarantineProcessor = getMandatoryInitParameter(Parameter.QUARANTINE_PROCESSOR.name);
        blockProcessor = getMandatoryInitParameter(Parameter.BLOCK_PROCESSOR.name);
        errorProcessor = getMandatoryInitParameter(Parameter.ERROR_PROCESSOR.name);
        // The delayEvaluationProcessor is not mandatory
        delayEvaluationProcessor = getInitParameter(Parameter.DELAY_EVALUATION_PROCESSOR.name);

        maxStringLength = getIntegerInitParameter(Parameter.MAX_STRING_LENGTH.name, maxStringLength);
        maxInitBufferSize = getIntegerInitParameter(Parameter.MAX_INIT_BUFFER_SIZE.name, maxInitBufferSize);

        StrBuilder sb = new StrBuilder();

        sb.append("warnProcessor: ").append(warnProcessor).
            append("; mustEncryptProcessor: ").append(mustEncryptProcessor).
            append("; quarantineProcessor: ").append(quarantineProcessor).
            append("; blockProcessor: ").append(blockProcessor).
            append("; errorProcessor: ").append(errorProcessor).
            append("; delayEvaluationProcessor: ").append(delayEvaluationProcessor).
            append("; maxStringLength: ").append(maxStringLength).
            append("; maxInitBufferSize: ").append(maxInitBufferSize);

        getLogger().info("{}", sb);
    }

    /**
     * Should return the PolicyPattern's which are used to check the message content with
     */
    protected abstract Collection<PolicyPattern> getPolicyPatterns(Mail mail)
    throws MessagingException, IOException;

    private void logException(Exception e)
    {
        if (getLogger().isDebugEnabled()) {
            getLogger().warn("Error extracting text from email.", e);
        }
        else {
            getLogger().warn("Error extracting text from email. Message: {}", ExceptionUtils.getRootCauseMessage(e));
        }
    }

    private void handleError(Mail mail, Exception e)
    {
        logException(e);

        // Get the root cause and add make sure the error message has some sane length
        String errorMessage = StringUtils.abbreviate(ExceptionUtils.getRootCauseMessage(e),
                MAX_POLICY_VIOLATION_ERROR_MESSAGE_LENGTH);

        // Place the error message in the Mail attributes so the exact details
        // of the error can be retrieved by other Mailets
        CoreApplicationMailAttributes.setPolicyViolationErrorMessage(mail, errorMessage);

        mail.setState(errorProcessor);
    }

    private void handlePolicyViolation(Mail mail, PolicyViolationException e)
    {
        PolicyViolation mainViolation = null;

        Collection<PolicyViolation> violations = e.getViolations();

        if (violations != null)
        {
            // We will place the violations in the Mail attributes so the exact details
            // of the violation can be retrieved by other Mailets
            CoreApplicationMailAttributes.setPolicyViolations(mail, violations);

            // Multiple policies might be violated. We need to check which policy is the
            // most important policy.
            for (PolicyViolation violation : violations)
            {
                if (violation == null) {
                    continue;
                }

                if (mainViolation == null) {
                    mainViolation = violation;
                }
                else if (violation.getPriority().isHigherPriorityThen(mainViolation.getPriority())) {
                    // Higher priority goes before lower priority
                    mainViolation = violation;
                }
                else if (violation.getPriority() == mainViolation.getPriority() && !violation.isDelayEvaluation()) {
                    // direct evaluation goes before delayed evaluation
                    mainViolation = violation;
                }
            }
        }

        if (mainViolation == null) {
            // Should never happen
            throw new IllegalStateException("No main violation found.");
        }

        CoreApplicationMailAttributes.setMainPolicyViolation(mail, mainViolation);

        String nextProcessor;

        if (StringUtils.isNotEmpty(delayEvaluationProcessor) && mainViolation.isDelayEvaluation()) {
            nextProcessor = delayEvaluationProcessor;
        }
        else {
            nextProcessor = switch (mainViolation.getPriority())
            {
                case WARN -> warnProcessor;
                case MUST_ENCRYPT -> mustEncryptProcessor;
                case QUARANTINE -> quarantineProcessor;
                case BLOCK -> blockProcessor;
                default ->
                    throw new IllegalArgumentException("Unknown PolicyViolationAction: " + mainViolation.getPriority());
            };
        }

        // If debug is enabled, log full policy violation (including the text that violated the policy)
        if (getLogger().isDebugEnabled())
        {
            getLogger().warn(createLogLine("Policy was violated. Policy violation: {}", mail, false),
                    mainViolation);
        }
        else {
            getLogger().warn(createLogLine(String.format("Policy was violated. Rule: %s, Priority: %s, " +
                    "Delayed Evaluation: %s", mainViolation.getRule(), mainViolation.getPriority(),
                    mainViolation.isDelayEvaluation()), mail, false));
        }

        mail.setState(nextProcessor);
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            List<ExtractedPart> extractedParts = null;

            try {
                PolicyCheckerContext context = new PolicyCheckerContextImpl();

                context.setPatterns(getPolicyPatterns(mail));

                extractedParts = textExtractor.extractText(mail.getMessage());

                policyChecker.init(context);

                if (extractedParts != null)
                {
                    for (ExtractedPart part : extractedParts)
                    {
                        StringIterator contentIterator = new StringIterator(part.getContent(), maxStringLength,
                                StandardCharsets.UTF_8.name(), maxInitBufferSize);

                        // We need to track whether the content of a part is too big to fit
                        // in one call. If so, partial will be true and the data top be
                        // analyzed will contain some overlap between different partial updates
                        // to make it possible to analyze the boundaries.
                        boolean partial = false;

                        String extractedText;

                        while((extractedText = contentIterator.getNext()) != null)
                        {
                            StringWriter normalized = new StringWriter(extractedText.length());

                            // The extracted text should be normalized before checking the policy
                            textNormalizer.normalize(new StringReader(extractedText), normalized);

                            context.setContent(normalized.toString());
                            context.setPartial(partial);

                            policyChecker.update(context);

                            partial = true;
                        }
                    }

                    try {
                        policyChecker.finish(context);
                    }
                    catch(PolicyViolationException e) {
                        handlePolicyViolation(mail, e);
                    }
                }
            }
            finally {
                TextExtractorUtils.closeQuitely(extractedParts);
            }
        }
        catch (Exception e) {
            handleError(mail, e);
        }
    }

    @VisibleForTesting
    public void setTextNormalizer(TextNormalizer textNormalizer) {
        this.textNormalizer = textNormalizer;
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        requiredProcessors.add(new ProcessingState(warnProcessor));
        requiredProcessors.add(new ProcessingState(mustEncryptProcessor));
        requiredProcessors.add(new ProcessingState(quarantineProcessor));
        requiredProcessors.add(new ProcessingState(blockProcessor));
        requiredProcessors.add(new ProcessingState(errorProcessor));

        if (delayEvaluationProcessor != null) {
            requiredProcessors.add(new ProcessingState(delayEvaluationProcessor));
        }

        return requiredProcessors;
    }
}
