/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import com.ciphermail.core.common.security.smime.SMIMEEncryptionAlgorithm;
import com.ciphermail.core.common.security.smime.SMIMEEncryptionScheme;
import com.ciphermail.core.common.security.smime.SMIMESigningAlgorithm;

import javax.annotation.Nonnull;

/**
 * Implementation of SMIMEProperties
 */
@UserPropertiesType(name = "smime", displayName = "S/MIME", order = 30)
public class SMIMEPropertiesImpl extends DelegatedHierarchicalProperties implements SMIMEProperties
{
    private static final String SMIME_SKIP_SIGN_ONLY                          = "smime-skip-sign-only";
    private static final String SMIME_ABORT_DECRYPTION_ON_INVALID_7BIT_CHARS  = "smime-abort-decryption-on-invalid-7bit-chars";
    private static final String SMIME_ADD_ADDITIONAL_CERTS                    = "smime-add-additional-certs";
    private static final String SMIME_ALWAYS_USE_FRESHEST_SIGNING_CERT        = "smime-always-use-freshest-signing-cert";
    private static final String SMIME_AUTO_IMPORT_CERTIFICATES_FROM_MAIL      = "smime-auto-import-certificates-from-mail";
    private static final String SMIME_AUTO_IMPORT_CERTIFICATES_SKIP_UNTRUSTED = "smime-auto-import-certificates-skip-untrusted";
    private static final String SMIME_AUTO_REQUEST_CERTIFICATE                = "smime-auto-request-certificate";
    private static final String SMIME_AUTO_SELECT_ENCRYPTION_CERTS            = "smime-auto-select-encryption-certs";
    private static final String SMIME_CHECK_INVALID_7BIT_CHARS                = "smime-check-invalid-7bit-chars";
    private static final String SMIME_ENABLED                                 = "smime-enabled";
    private static final String SMIME_ENCRYPTION_ALGORITHM                    = "smime-encryption-algorithm";
    private static final String SMIME_ENCRYPTION_SCHEME                       = "smime-encryption-scheme";
    private static final String SMIME_MAX_SIZE_SMIME                          = "smime-max-size";
    private static final String SMIME_PROTECT_HEADERS                         = "smime-protect-headers";
    private static final String SMIME_REMOVE_SMIME_SIGNATURE                  = "smime-remove-signature";
    private static final String SMIME_SIGNING_ALGORITHM                       = "smime-signing-algorithm";
    private static final String SMIME_SKIP_CALENDAR                           = "smime-skip-calendar";
    private static final String SMIME_SKIP_SIGNING_CALENDAR                   = "smime-skip-signing-calendar";
    private static final String SMIME_STRICT                                  = "smime-strict";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public SMIMEPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(name = SMIME_ENABLED)
    public void setSMIMEEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = SMIME_ENABLED, order = 10)
    public @Nonnull Boolean getSMIMEEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_ENABLED);
    }

    @Override
    @UserProperty(name = SMIME_SKIP_SIGN_ONLY, order = 15)
    public Boolean getSkipSignOnly()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_SKIP_SIGN_ONLY);
    }

    @Override
    @UserProperty(name = SMIME_SKIP_SIGN_ONLY)
    public void setSkipSignOnly(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_SKIP_SIGN_ONLY, value);
    }

    @Override
    @UserProperty(name = SMIME_STRICT)
    public void setSMIMEStrict(Boolean strict)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_STRICT, strict);
    }

    @Override
    @UserProperty(name = SMIME_STRICT, order = 20)
    public @Nonnull Boolean getSMIMEStrict()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_STRICT);
    }

    @Override
    @UserProperty(name = SMIME_SKIP_CALENDAR)
    public void setSMIMESkipCalendar(Boolean skip)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_SKIP_CALENDAR, skip);
    }

    @Override
    @UserProperty(name = SMIME_SKIP_CALENDAR, order = 30)
    public @Nonnull Boolean getSMIMESkipCalendar()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_SKIP_CALENDAR);
    }

    @Override
    @UserProperty(name = SMIME_SKIP_SIGNING_CALENDAR)
    public void setSMIMESkipSigningCalendar(Boolean skip)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_SKIP_SIGNING_CALENDAR, skip);
    }

    @Override
    @UserProperty(name = SMIME_SKIP_SIGNING_CALENDAR, order = 40)
    public @Nonnull Boolean getSMIMESkipSigningCalendar()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_SKIP_SIGNING_CALENDAR);
    }

    @Override
    @UserProperty(name = SMIME_ENCRYPTION_ALGORITHM)
    public void setSMIMEEncryptionAlgorithm(SMIMEEncryptionAlgorithm algorithm)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, SMIME_ENCRYPTION_ALGORITHM, algorithm);
    }

    @Override
    @UserProperty(name = SMIME_ENCRYPTION_ALGORITHM, order = 60)
    public SMIMEEncryptionAlgorithm getSMIMEEncryptionAlgorithm()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(this, SMIME_ENCRYPTION_ALGORITHM,
                SMIMEEncryptionAlgorithm.class);
    }

    @Override
    @UserProperty(name = SMIME_ENCRYPTION_SCHEME)
    public void setSMIMEEncryptionScheme(SMIMEEncryptionScheme scheme)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, SMIME_ENCRYPTION_SCHEME, scheme);
    }

    @Override
    @UserProperty(name = SMIME_ENCRYPTION_SCHEME, order = 70)
    public SMIMEEncryptionScheme getSMIMEEncryptionScheme()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(this, SMIME_ENCRYPTION_SCHEME, SMIMEEncryptionScheme.class);
    }

    @Override
    @UserProperty(name = SMIME_SIGNING_ALGORITHM)
    public void setSMIMESigningAlgorithm(SMIMESigningAlgorithm algorithm)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, SMIME_SIGNING_ALGORITHM, algorithm);
    }

    @Override
    @UserProperty(name = SMIME_SIGNING_ALGORITHM, order = 80)
    public SMIMESigningAlgorithm getSMIMESigningAlgorithm()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(this, SMIME_SIGNING_ALGORITHM, SMIMESigningAlgorithm.class);
    }

    @Override
    @UserProperty(name = SMIME_CHECK_INVALID_7BIT_CHARS)
    public void setSMIMECheckInvalid7BitChars(Boolean checkInvalid7BitChars)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_CHECK_INVALID_7BIT_CHARS, checkInvalid7BitChars);
    }

    @Override
    @UserProperty(name = SMIME_CHECK_INVALID_7BIT_CHARS, order = 90)
    public @Nonnull Boolean getSMIMECheckInvalid7BitChars()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_CHECK_INVALID_7BIT_CHARS);
    }

    @Override
    @UserProperty(name = SMIME_ABORT_DECRYPTION_ON_INVALID_7BIT_CHARS)
    public void setSMIMEAbortDecryptionOnInvalid7BitChars(Boolean abortDecryptionOnInvalid7BitChars)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_ABORT_DECRYPTION_ON_INVALID_7BIT_CHARS,
                abortDecryptionOnInvalid7BitChars);
    }

    @Override
    @UserProperty(name = SMIME_ABORT_DECRYPTION_ON_INVALID_7BIT_CHARS, order = 100)
    public @Nonnull Boolean getSMIMEAbortDecryptionOnInvalid7BitChars()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_ABORT_DECRYPTION_ON_INVALID_7BIT_CHARS);
    }

    @Override
    @UserProperty(name = SMIME_AUTO_SELECT_ENCRYPTION_CERTS)
    public void setAutoSelectEncryptionCerts(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_AUTO_SELECT_ENCRYPTION_CERTS, enabled);
    }

    @Override
    @UserProperty(name = SMIME_AUTO_SELECT_ENCRYPTION_CERTS, order = 110)
    public @Nonnull Boolean getAutoSelectEncryptionCerts()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_AUTO_SELECT_ENCRYPTION_CERTS);
    }

    @Override
    @UserProperty(name = SMIME_ALWAYS_USE_FRESHEST_SIGNING_CERT)
    public void setAlwaysUseFreshestSigningCert(Boolean useFreshestSigningCert)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_ALWAYS_USE_FRESHEST_SIGNING_CERT, useFreshestSigningCert);
    }

    @Override
    @UserProperty(name = SMIME_ALWAYS_USE_FRESHEST_SIGNING_CERT, order = 120)
    public @Nonnull Boolean getAlwaysUseFreshestSigningCert()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_ALWAYS_USE_FRESHEST_SIGNING_CERT);
    }

    @Override
    @UserProperty(name = SMIME_MAX_SIZE_SMIME)
    public void setMaxSizeSMIME(Long size)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, SMIME_MAX_SIZE_SMIME, size);
    }

    @Override
    @UserProperty(name = SMIME_MAX_SIZE_SMIME, order = 140)
    public @Nonnull Long getMaxSizeSMIME()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, SMIME_MAX_SIZE_SMIME);
    }

    @Override
    @UserProperty(name = SMIME_REMOVE_SMIME_SIGNATURE)
    public void setRemoveSMIMESignature(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_REMOVE_SMIME_SIGNATURE, value);
    }

    @Override
    @UserProperty(name = SMIME_REMOVE_SMIME_SIGNATURE, order = 150)
    public @Nonnull Boolean getRemoveSMIMESignature()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_REMOVE_SMIME_SIGNATURE);
    }

    @Override
    @UserProperty(name = SMIME_AUTO_REQUEST_CERTIFICATE)
    public void setAutoRequestCertificate(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_AUTO_REQUEST_CERTIFICATE, value);
    }

    @Override
    @UserProperty(name = SMIME_AUTO_REQUEST_CERTIFICATE, order = 160)
    public @Nonnull Boolean getAutoRequestCertificate()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_AUTO_REQUEST_CERTIFICATE);
    }

    @Override
    @UserProperty(name = SMIME_AUTO_IMPORT_CERTIFICATES_FROM_MAIL)
    public void setAutoImportCertificatesFromMail(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_AUTO_IMPORT_CERTIFICATES_FROM_MAIL, value);
    }

    @Override
    @UserProperty(name = SMIME_AUTO_IMPORT_CERTIFICATES_FROM_MAIL, order = 170)
    public @Nonnull Boolean getAutoImportCertificatesFromMail()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_AUTO_IMPORT_CERTIFICATES_FROM_MAIL);
    }

    @Override
    @UserProperty(name = SMIME_AUTO_IMPORT_CERTIFICATES_SKIP_UNTRUSTED)
    public void setAutoImportCertificatesSkipUntrusted(Boolean value)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_AUTO_IMPORT_CERTIFICATES_SKIP_UNTRUSTED, value);
    }

    @Override
    @UserProperty(name = SMIME_AUTO_IMPORT_CERTIFICATES_SKIP_UNTRUSTED, order = 180)
    public @Nonnull Boolean getAutoImportCertificatesSkipUntrusted()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_AUTO_IMPORT_CERTIFICATES_SKIP_UNTRUSTED);
    }

    @Override
    @UserProperty(name = SMIME_PROTECT_HEADERS)
    public void setSMIMEProtectHeaders(Boolean protect)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_PROTECT_HEADERS, protect);
    }

    @Override
    @UserProperty(name = SMIME_PROTECT_HEADERS, visible = false)
    public @Nonnull Boolean getSMIMEProtectHeaders()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_PROTECT_HEADERS);
    }

    @Override
    @UserProperty(name = SMIME_ADD_ADDITIONAL_CERTS)
    public void setAddAdditionalCertificates(Boolean add)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMIME_ADD_ADDITIONAL_CERTS, add);
    }

    @Override
    @UserProperty(name = SMIME_ADD_ADDITIONAL_CERTS, visible = false)
    public @Nonnull Boolean getAddAdditionalCertificates()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMIME_ADD_ADDITIONAL_CERTS);
    }
}
