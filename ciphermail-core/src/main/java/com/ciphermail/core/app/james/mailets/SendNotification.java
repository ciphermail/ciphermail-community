/*
 * Copyright (c) 2015-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.common.notification.NotificationService;
import com.ciphermail.core.common.notification.NotificationSeverity;
import com.ciphermail.core.common.notification.StandardNotificationFacilities;
import com.ciphermail.core.common.notification.StringNotificationMessage;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Mailet that sends a notification message to the notification service
 */
@SuppressWarnings({"java:S6813"})
public class SendNotification extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SendNotification.class);

    /*
     * The message to send
     */
    private String message;

    /*
     * The Mail attribute for retrieving the message value. If there is no attribute or the value is null,
     * the value from message will be used
     */
    private String messageAttribute;

    /*
     * The severity of the message
     */
    private NotificationSeverity severity;

    /*
     * The Mail attribute for retrieving the severity value. If there is no attribute or the value is null,
     * the value from severity will be used
     */
    private String severityAttribute;

    /*
     * The facility of the message
     */
    private String facility;

    /*
     * The Mail attribute for retrieving the facility value. If there is no attribute or the value is null,
     * the value from facility will be used
     */
    private String facilityAttribute;

    /*
     * The notifications will be sent to the notificationService
     */
    @Inject
    private NotificationService notificationService;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        MESSAGE            ("message"),
        MESSAGE_ATTRIBUTE  ("messageAttribute"),
        SEVERITY           ("severity"),
        SEVERITY_ATTRIBUTE ("severityAttribute"),
        FACILITY           ("facility"),
        FACILITY_ATTRIBUTE ("facilityAttribute");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        Objects.requireNonNull(notificationService);

        message = Objects.requireNonNull(getInitParameter(Parameter.MESSAGE.name),
                Parameter.MESSAGE.name + " is required");
        messageAttribute = getInitParameter(Parameter.MESSAGE_ATTRIBUTE.name);
        severity = Objects.requireNonNullElse(NotificationSeverity.fromName(getInitParameter(Parameter.SEVERITY.name)),
                NotificationSeverity.NOTICE);
        severityAttribute = getInitParameter(Parameter.SEVERITY_ATTRIBUTE.name);
        facility = Objects.requireNonNullElse(getInitParameter(Parameter.FACILITY.name),
                StandardNotificationFacilities.INFO);
        facilityAttribute = getInitParameter(Parameter.FACILITY_ATTRIBUTE.name);
    }

    @Override
    public void serviceMail(Mail mail)
    {
        List<String> messages = new LinkedList<>();

        if (messageAttribute != null) {
            messages.addAll(MailAttributesUtils.getAttributeValueAsStringList(mail, messageAttribute));
        }

        // remove empty strings from the list of messages
        messages.removeIf(StringUtils::isEmpty);

        // If the attributes did not return a message (or if the attribute is not set), use the default message
        if (messages.isEmpty()) {
            messages.add(message);
        }

        NotificationSeverity localSeverity = null;

        if (severityAttribute != null)
        {
            Optional<String> optionalSeverity = MailAttributesUtils.getManagedAttributeValue(
                    mail, severityAttribute, String.class);

            if (optionalSeverity.isPresent())
            {
                localSeverity = NotificationSeverity.fromName(optionalSeverity.get());

                if (localSeverity == null) {
                    logger.warn("{} is not a valid NotificationSeverity", optionalSeverity.get());
                }
            }
        }

        // Use default is not set
        if (localSeverity == null) {
            localSeverity = this.severity;
        }

        String localFacility = null;

        if (facilityAttribute != null)
        {
            localFacility = MailAttributesUtils.getManagedAttributeValue(mail, facilityAttribute,
                    String.class).orElse(null);
        }

        // Use default is not set
        if (localFacility == null) {
            localFacility = this.facility;
        }

        for (String messageToSend : messages)
        {
            getLogger().debug("Sending notification. Message: {}, Facility: {}, Severity: {}", messageToSend,
                    localFacility, localSeverity);

            notificationService.sendNotification(localFacility, localSeverity,
                    new StringNotificationMessage(messageToSend));
        }
    }
}
