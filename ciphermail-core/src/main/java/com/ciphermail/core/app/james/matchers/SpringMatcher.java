/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.Collection;


/**
 * Matcher implementation that gets an instance of MailMatcher from Spring
 *
 * @author Martijn Brinkers
 *
 */
public class SpringMatcher extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(SpringMatcher.class);

    @Inject
    private AbstractApplicationContext applicationContext;

    /*
     * The mailMatcher from the spring context
     */
    private MailMatcher mailMatcher;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    throws MessagingException
    {
        String serviceId = getCondition();

        mailMatcher = applicationContext.getBean(serviceId, MailMatcher.class);

        StrBuilder sb = new StrBuilder();

        sb.append("Initializing matcher " + getMatcherName());
        sb.append("; Spring service id: ").append(serviceId);
        sb.append("; MailMatcher Class: ").append(mailMatcher.getClass());

        getLogger().info("{}", sb);
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        return mailMatcher.matches(mail);
    }
}
