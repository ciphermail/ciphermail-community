/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.core.app.acme;

import com.ciphermail.core.app.properties.UserPropertiesType;
import com.ciphermail.core.app.properties.UserPropertiesValidators;
import com.ciphermail.core.app.properties.UserProperty;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import com.ciphermail.core.common.util.URIUtils;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.annotation.Nonnull;

/**
 * AcmeSettingsImpl represents the implementation of the AcmeSettings interface.
 * It provides methods to get and set various Acme settings.
 */
@UserPropertiesType(name = "acme", visible = false)
public class AcmeSettingsImpl extends DelegatedHierarchicalProperties implements AcmeSettings
{
    /*
     * Property names
     */
    public static final String SERVER_URI = "acme-server-uri";
    public static final String ACCOUNT_KEYPAIR = "acme-account-keypair";
    public static final String ACCOUNT_EMAIL = "acme-account-email";
    public static final String ACCOUNT_KEY_IDENTIFIER = "acme-account-kid";
    public static final String ACCOUNT_MAC = "acme-account-mac";
    public static final String TOKEN_CACHE = "acme-token-cache";
    public static final String TOS_ACCEPTED = "acme-tos-accepted";
    public static final String AUTO_RENEWAL_ENABLED = "acme-auto-renewal-enabled";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public AcmeSettingsImpl(@Nonnull HierarchicalProperties delegatedProperties) {
        super(delegatedProperties);
    }

    public static AcmeSettingsImpl getInstance(@Nonnull HierarchicalProperties delegatedProperties) {
        return new AcmeSettingsImpl(delegatedProperties);
    }

    @Override
    @UserProperty(name = SERVER_URI, user = false, domain = false)
    public void setServerUri(String serverUri)
    throws HierarchicalPropertiesException
    {
        this.setProperty(SERVER_URI, UserPropertiesValidators.validateURI(
                SERVER_URI, serverUri, URIUtils.URIType.FULL));
    }

    @Override
    @UserProperty(name = SERVER_URI, user = false, domain = false)
    public String getServerUri()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(SERVER_URI);
    }

    @Override
    @UserProperty(name = ACCOUNT_KEYPAIR, user = false, domain = false)
    public void setAccountKeyPair(String pemEncodedKeyPair)
    throws HierarchicalPropertiesException
    {
        setProperty(ACCOUNT_KEYPAIR, UserPropertiesValidators.validateKeyPair(ACCOUNT_KEYPAIR, pemEncodedKeyPair));
    }

    @Override
    @UserProperty(name = ACCOUNT_KEYPAIR, user = false, domain = false, allowNull = true)
    public String getAccountKeyPair()
    throws HierarchicalPropertiesException
    {
        return getProperty(ACCOUNT_KEYPAIR);
    }

    @Override
    @UserProperty(name = ACCOUNT_EMAIL, user = false, domain = false)
    public void setAccountEmail(String accountEmail)
    throws HierarchicalPropertiesException
    {
        setProperty(ACCOUNT_EMAIL, UserPropertiesValidators.validateEmail(ACCOUNT_EMAIL, accountEmail));
    }

    @Override
    @UserProperty(name = ACCOUNT_EMAIL, user = false, domain = false, allowNull = true)
    public String getAccountEmail()
    throws HierarchicalPropertiesException
    {
        return getProperty(ACCOUNT_EMAIL);
    }

    @Override
    @UserProperty(name = ACCOUNT_KEY_IDENTIFIER, user = false, domain = false)
    public void setKeyIdentifier(String kid)
    throws HierarchicalPropertiesException
    {
        setProperty(ACCOUNT_KEY_IDENTIFIER, kid);
    }

    @Override
    @UserProperty(name = ACCOUNT_KEY_IDENTIFIER, user = false, domain = false, allowNull = true)
    public String getKeyIdentifier()
    throws HierarchicalPropertiesException
    {
        return getProperty(ACCOUNT_KEY_IDENTIFIER);
    }

    @Override
    @UserProperty(name = ACCOUNT_MAC, user = false, domain = false)
    public void setEncodedMacKey(String encodedMacKey)
    throws HierarchicalPropertiesException
    {
        setProperty(ACCOUNT_MAC, encodedMacKey);
    }

    @Override
    @UserProperty(name = ACCOUNT_MAC, user = false, domain = false, allowNull = true)
    public String getEncodedMacKey()
    throws HierarchicalPropertiesException
    {
        return getProperty(ACCOUNT_MAC);
    }

    @Override
    @UserProperty(name = TOKEN_CACHE, user = false, domain = false)
    public void setTokenCacheJSON(String tokenCacheJSON)
    throws HierarchicalPropertiesException
    {
        if (tokenCacheJSON != null)
        {
            try {
                JacksonUtil.getObjectMapper().readValue(tokenCacheJSON, AcmeTokenCache.class);
            }
            catch (JsonProcessingException e) {
                throw new IllegalArgumentException("Input is not a valid json encoded AcmeTokenCache");
            }
        }

        setProperty(TOKEN_CACHE, tokenCacheJSON);
    }

    @Override
    @UserProperty(name = TOKEN_CACHE, user = false, domain = false, allowNull = true)
    public String getTokenCacheJSON()
    throws HierarchicalPropertiesException
    {
        return getProperty(TOKEN_CACHE);
    }

    @Override
    @UserProperty(name = TOS_ACCEPTED, user = false, domain = false)
    public void setTosAccepted(Boolean accepted)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, TOS_ACCEPTED, accepted);
    }

    @Override
    @UserProperty(name = TOS_ACCEPTED, user = false, domain = false)
    public Boolean getTosAccepted()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, TOS_ACCEPTED, false);
    }

    @Override
    @UserProperty(name = AUTO_RENEWAL_ENABLED, user = false, domain = false)
    public void setAutoRenewalEnabled(Boolean autoRenewalEnabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, AUTO_RENEWAL_ENABLED, autoRenewalEnabled);
    }

    @Override
    @UserProperty(name = AUTO_RENEWAL_ENABLED, user = false, domain = false)
    public Boolean getAutoRenewalEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, AUTO_RENEWAL_ENABLED, false);
    }
}
