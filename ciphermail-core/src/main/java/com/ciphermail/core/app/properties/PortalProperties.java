/*
 * Copyright (c) 2011-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

/**
 * Java interface wrapper around hierarchical properties for the Portal properties
 *
 * @author Martijn Brinkers
 *
 */
public interface PortalProperties
{
    /**
     * The portal login password
     */
    void setPassword(String password)
    throws HierarchicalPropertiesException;

    String getPassword()
    throws HierarchicalPropertiesException;

    /**
     * If true, the user is allowed to login on the portal
     */
    void setEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    Boolean getEnabled()
    throws HierarchicalPropertiesException;

    /**
     * If true, 2FA is enabled for the user
     */
    void set2FAEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    Boolean get2FAEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The 2FA secret for the user
     */
    void set2FASecret(String secret)
    throws HierarchicalPropertiesException;

    String get2FASecret()
    throws HierarchicalPropertiesException;

    /**
     * The base URL of the portal
     */
    void setBaseURL(String url)
    throws HierarchicalPropertiesException;

    String getBaseURL()
    throws HierarchicalPropertiesException;

    /**
     * Signup URL for the portal
     */
    void setSignupURL(String url)
    throws HierarchicalPropertiesException;

    String getSignupURL()
    throws HierarchicalPropertiesException;

    /**
     * If true, the user is invited to create an account password
     */
    void setAutoSignup(Boolean autoSignup)
    throws HierarchicalPropertiesException;

    Boolean getAutoSignup()
    throws HierarchicalPropertiesException;

    /**
     * The time (in seconds) a signup link is valid
     */
    void setSignupValidityIntervalSeconds(Long validityIntervalSeconds)
    throws HierarchicalPropertiesException;

    Long getSignupValidityIntervalSeconds()
    throws HierarchicalPropertiesException;

    /**
     * The password policy for the portal password
     */
    void setPasswordPolicy(String passwordPolicy)
    throws HierarchicalPropertiesException;

    String getPasswordPolicy()
    throws HierarchicalPropertiesException;

    /**
     * The URL to the password policy page for the portal password
     */
    void setPasswordPolicyURL(String passwordPolicyURL)
    throws HierarchicalPropertiesException;

    String getPasswordPolicyURL()
    throws HierarchicalPropertiesException;

    /**
     * The timestamp a password reset was initiated
     */
    void setPasswordResetTimestamp(Long passwordResetTimestamp)
    throws HierarchicalPropertiesException;

    Long getPasswordResetTimestamp()
    throws HierarchicalPropertiesException;

    /**
     * The minimal time in milliseconds between consecutive password resets
     */
    void setPasswordResetInterval(Long interval)
    throws HierarchicalPropertiesException;

    Long getPasswordResetInterval()
    throws HierarchicalPropertiesException;

    /**
     * Password Reset URL for the portal
     */
    void setPasswordResetURL(String url)
    throws HierarchicalPropertiesException;

    String getPasswordResetURL()
    throws HierarchicalPropertiesException;

    /**
     * The time (in seconds) a password reset link is valid
     */
    void setPasswordResetValidityIntervalSeconds(Long validityIntervalSeconds)
    throws HierarchicalPropertiesException;

    Long getPasswordResetValidityIntervalSeconds()
    throws HierarchicalPropertiesException;
}
