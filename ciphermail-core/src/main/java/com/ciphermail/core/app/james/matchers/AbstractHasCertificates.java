/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.EncryptionRecipientSelector;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.Certificates;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.UserPersister;
import com.ciphermail.core.app.properties.SMIMEProperties;
import com.ciphermail.core.app.properties.SMIMEPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.AbstractRetryListener;
import com.google.common.annotations.VisibleForTesting;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings("java:S6813")
public abstract class AbstractHasCertificates extends AbstractContextAwareCipherMailMatcher
{
    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Used for the selection of encryption certificates for particular user(s).
     */
    @Inject
    private EncryptionRecipientSelector encryptionRecipientSelector;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    protected static final String ACTIVATION_CONTEXT_KEY = "certificates";

    /*
     * Keeps track of certificates and users that have matched.
     */
    protected static class HasCertificateActivationContext
    {
        /*
         * Collection of all the encryption certificates for all users
         */
        private final Set<X509Certificate> certificates = new HashSet<>();

        /*
         * All the users with valid encryption certificate(s)
         */
        private final Set<User> users = new HashSet<>();

        /*
         * All the users for which the user should be persisted
         */
        private final Set<User> usersToPersist = new HashSet<>();

        public Set<X509Certificate> getCertificates() {
            return certificates;
        }

        public Set<User> getUsers() {
            return users;
        }

        public Set<User> getUsersToPersist() {
            return usersToPersist;
        }

        public void clear()
        {
            certificates.clear();
            users.clear();
            usersToPersist.clear();
        }
    }

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(transactionOperations);
        Objects.requireNonNull(encryptionRecipientSelector);
    }

    private Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses,
            HasCertificateActivationContext context)
    throws MessagingException
    {
        MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = AbstractHasCertificates.this::hasMatch;

        MailAddressMatcher matcher = new MailAddressMatcher(transactionOperations, userWorkflow,
                hasMatchEventHandler, getLogger());

        return matcher.getMatchingMailAddressesWithRetry(mailAddresses, new AbstractRetryListener()
        {
            @Override
            public <T, E extends Throwable> void onError(RetryContext retryContext, RetryCallback<T, E> retryCallback,
                    Throwable throwable)
            {
                context.clear();
            }
        });
    }

    private SMIMEProperties createSMIMEProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMIMEPropertiesImpl.class)
                .createInstance(properties);
    }

    @VisibleForTesting
    protected boolean hasMatch(User user)
    throws MessagingException
    {
        HasCertificateActivationContext context = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                HasCertificateActivationContext.class);

        Collection<X509Certificate> certificates = context.getCertificates();

        Collection<User> encryptionUsers;

        try {
            encryptionUsers = encryptionRecipientSelector.select(Collections.singleton(user),
                    certificates);
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Exception while selecting recipients.", e);
        }

        context.getUsers().addAll(encryptionUsers);

        return !encryptionUsers.isEmpty();
    }

    @Override
    public final Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        HasCertificateActivationContext context = new HasCertificateActivationContext();

        getActivationContext().set(ACTIVATION_CONTEXT_KEY, context);

        Collection<MailAddress> matching = getMatchingMailAddresses(getMailAddresses(mail), context);

        addCertificatesProperty(mail, context);

        makeNonPersistentUsersPersistent(context.getUsersToPersist());

        return matching;
    }

    /*
     * Adds certificates of all matching users to the mail attributes.
     */
    private void addCertificatesProperty(Mail mail, HasCertificateActivationContext context)
    {
        Set<X509Certificate> certificates = context.getCertificates();

        if (!certificates.isEmpty())
        {
            // Add the certificates to the attributes of the mail.
            CoreApplicationMailAttributes.setCertificates(mail, new Certificates(certificates));
        }
    }

    private void makeNonPersistentUsersPersistent(Collection<User> encryptionUsers)
    {
        UserPersister.getInstance(userWorkflow, transactionOperations)
                .tryToMakeNonPersistentUsersPersistent(encryptionUsers);
    }

    protected abstract Collection<MailAddress> getMailAddresses(Mail mail)
    throws MessagingException;
}
