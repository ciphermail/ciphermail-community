/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.util.BeanUtilsBeanBuilder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.Converter;

import javax.annotation.Nonnull;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

public class UserPropertiesAccessorImpl implements UserPropertiesAccessor
{
    /*
     * Contains all the user property objects
     */
    private final UserPropertyRegistry userPropertyRegistry;

    /*
     * BeanUtilsBean is responsible for setting/getting properties
     */
    private final BeanUtilsBean beanUtilsBean;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    public UserPropertiesAccessorImpl(
            @Nonnull UserPropertyRegistry userPropertyRegistry,
            @Nonnull BeanUtilsBeanBuilder beanUtilsBeanBuilder,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        this.userPropertyRegistry = Objects.requireNonNull(userPropertyRegistry);
        this.beanUtilsBean = Objects.requireNonNull(beanUtilsBeanBuilder).createBeanUtilsBean();
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);

        checkConverters();
    }

    // check if there is a converter for every property
    private void checkConverters()
    {
        for (UserPropertyRegistry.UserPropertyDescriptor descriptor : userPropertyRegistry.getProperties().values())
        {
            PropertyDescriptor propertyDescriptor = descriptor.propertyDescriptor();

            // enums should be handled by beanUtilsBean directly
            if (propertyDescriptor.getPropertyType().isEnum()) {
                continue;
            }

            Converter converter = beanUtilsBean.getConvertUtils().lookup(String.class,
                    propertyDescriptor.getPropertyType());

            if (converter == null) {
                throw new IllegalArgumentException(String.format("There is no converter for class %s",
                        propertyDescriptor.getPropertyType()));
            }
        }
    }

    private @Nonnull Object createUserPropertiesInstance(@Nonnull HierarchicalProperties properties,
            @Nonnull UserPropertyRegistry.UserPropertyDescriptor userPropertyDescriptor)
    {
        Class<?> userPropertiesClass = userPropertyDescriptor.propertyDescriptor().getReadMethod().getDeclaringClass();

        UserPropertiesFactory factory = userPropertiesFactoryRegistry.getFactoryForClass(userPropertiesClass);

        return factory.createInstance(properties);
    }

    @Override
    public void setProperty(@Nonnull HierarchicalProperties properties, @Nonnull String propertyName, String value)
    throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, InstantiationException
    {
        UserPropertyRegistry.UserPropertyDescriptor userPropertyDescriptor = userPropertyRegistry.getProperties()
                .get(propertyName);

        if (userPropertyDescriptor == null) {
            throw new IllegalArgumentException(String.format("Property with name %s not found", propertyName));
        }

        // set the property on the user properties instance
        beanUtilsBean.setProperty(createUserPropertiesInstance(properties, userPropertyDescriptor),
                userPropertyDescriptor.propertyDescriptor().getName(), value);
    }

    @Override
    public String getProperty(@Nonnull HierarchicalProperties properties, @Nonnull String propertyName)
    throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, InstantiationException
    {
        UserPropertyRegistry.UserPropertyDescriptor userPropertyDescriptor = userPropertyRegistry.getProperties()
                .get(propertyName);

        if (userPropertyDescriptor == null) {
            throw new IllegalArgumentException(String.format("Property with name %s not found", propertyName));
        }

        // get the property from the user properties instance
        return beanUtilsBean.getSimpleProperty(createUserPropertiesInstance(properties, userPropertyDescriptor),
                userPropertyDescriptor.propertyDescriptor().getName());
    }
}
