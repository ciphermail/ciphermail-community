/*
 * Copyright (c) 2009-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.ca;

import com.ciphermail.core.app.properties.CACategory;
import com.ciphermail.core.app.properties.UserPropertiesType;
import com.ciphermail.core.app.properties.UserPropertiesValidators;
import com.ciphermail.core.app.properties.UserProperty;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import com.ciphermail.core.common.security.ca.CACertificateSignatureAlgorithm;
import com.ciphermail.core.common.security.ca.CAProperties;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistry;
import com.ciphermail.core.common.security.ca.handlers.BuiltInCertificateRequestHandler;
import com.ciphermail.core.common.util.URIUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Implementation of CASettings that reads and writes  settings from a HierarchicalProperties object.
 */
@UserPropertiesType(name = "ca-settings", displayName = "Settings", parent = CACategory.NAME, order = 10)
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class CAPropertiesImpl implements CAProperties
{
    private static final String DEFAULT_COMMON_NAME = "ca-default-common-name";
    private static final String CERTIFICATE_VALIDITY_DAYS = "ca-validity";
    private static final String KEY_LENGTH = "ca-key-length";
    private static final String SIGNATURE_ALGORITHM = "ca-signature-algorithm";
    private static final String CRL_DIST_POINT = "ca-crl-dist-point";
    private static final String DEFAULT_CERTIFICATE_REQUEST_HANDLER = "ca-default-certificate-request-handler";
    private static final String ISSUER_THUMBPRINT = "ca-issuer-thumbprint";

    @Autowired
    private CertificateRequestHandlerRegistry certificateRequestHandlerRegistry;

    /*
     * The properties to read/write to/from
     */
    private final HierarchicalProperties properties;

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public CAPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        this.properties = Objects.requireNonNull(properties);
    }

    @Override
    @UserProperty(name = DEFAULT_COMMON_NAME,
            user = false, domain = false
    )
    public void setDefaultCommonName(String commonName)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(DEFAULT_COMMON_NAME, commonName);
    }

    @Override
    @UserProperty(name = DEFAULT_COMMON_NAME,
            user = false, domain = false,
            order = 20
    )
    public String getDefaultCommonName()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(DEFAULT_COMMON_NAME);
    }

    @Override
    @UserProperty(name = CERTIFICATE_VALIDITY_DAYS,
            user = false, domain = false
    )
    public void setCertificateValidityDays(Integer days)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, CERTIFICATE_VALIDITY_DAYS, days);
    }

    @Override
    @UserProperty(name = CERTIFICATE_VALIDITY_DAYS,
            user = false, domain = false,
            order = 30
    )
    public @Nonnull Integer getCertificateValidityDays()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(properties, CERTIFICATE_VALIDITY_DAYS, 365 * 5);
    }

    @Override
    @UserProperty(name = KEY_LENGTH,
            user = false, domain = false
    )
    public void setKeyLength(Integer length)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setInteger(properties, KEY_LENGTH, length);
    }

    @Override
    @UserProperty(name = KEY_LENGTH,
            user = false, domain = false,
            order = 40
    )
    public @Nonnull Integer getKeyLength()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getInteger(properties, KEY_LENGTH);
    }

    @Override
    @UserProperty(name = SIGNATURE_ALGORITHM,
            user = false, domain = false
    )
    public void setSignatureAlgorithm(CACertificateSignatureAlgorithm signatureAlgorithm)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(properties, SIGNATURE_ALGORITHM, signatureAlgorithm);
    }

    @Override
    @UserProperty(name = SIGNATURE_ALGORITHM,
            user = false, domain = false,
            order = 50
    )
    public @Nonnull CACertificateSignatureAlgorithm getSignatureAlgorithm()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(properties, SIGNATURE_ALGORITHM,
                CACertificateSignatureAlgorithm.class, CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION);
    }

    @Override
    @UserProperty(name = CRL_DIST_POINT,
            user = false, domain = false
    )
    public void setCRLDistributionPoint(String url)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(CRL_DIST_POINT, UserPropertiesValidators.validateURI(
                CRL_DIST_POINT, url, URIUtils.URIType.FULL));
    }

    @Override
    @UserProperty(name = CRL_DIST_POINT,
            user = false, domain = false,
            order = 60,
            allowNull = true
    )
    public String getCRLDistributionPoint()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(CRL_DIST_POINT);
    }

    @Override
    @UserProperty(name = DEFAULT_CERTIFICATE_REQUEST_HANDLER,
            user = false, domain = false
    )
    public void setDefaultCertificateRequestHandler(String certificateRequestHandler)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(DEFAULT_CERTIFICATE_REQUEST_HANDLER, UserPropertiesValidators
                .validateCertificateRequestHandler(DEFAULT_CERTIFICATE_REQUEST_HANDLER,
                        certificateRequestHandler,
                        certificateRequestHandlerRegistry));
    }

    @Override
    @UserProperty(name = DEFAULT_CERTIFICATE_REQUEST_HANDLER,
            user = false, domain = false,
            order = 70
    )
    public String getDefaultCertificateRequestHandler()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getProperty(properties, DEFAULT_CERTIFICATE_REQUEST_HANDLER,
                BuiltInCertificateRequestHandler.NAME);
    }

    @Override
    @UserProperty(name = ISSUER_THUMBPRINT,
            user = false, domain = false,
            visible = false
    )
    public void setIssuerThumbprint(String thumbprint)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(ISSUER_THUMBPRINT, UserPropertiesValidators.
                validateSHA512Thumbprint(ISSUER_THUMBPRINT, thumbprint));
    }

    @Override
    @UserProperty(name = ISSUER_THUMBPRINT,
            user = false, domain = false,
            visible = false,
            allowNull = true
    )
    public String getIssuerThumbprint()
    throws HierarchicalPropertiesException
    {
        return properties.getProperty(ISSUER_THUMBPRINT);
    }
}
