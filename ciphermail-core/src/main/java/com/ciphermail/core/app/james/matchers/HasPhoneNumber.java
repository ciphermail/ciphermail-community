/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.PhoneNumber;
import com.ciphermail.core.app.james.PhoneNumbers;
import com.ciphermail.core.app.properties.SMSProperties;
import com.ciphermail.core.app.properties.SMSPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.AbstractRetryListener;
import com.google.common.annotations.VisibleForTesting;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Objects;

/**
 * Matcher that matches all recipients with a phone number. The phone numbers will be added to the mail attributes
 * as a map that maps the recipients email address to phone number
 * (@See {@link CoreApplicationMailAttributes#setPhoneNumbers(Mail, PhoneNumbers)}
 *
 * Usage:
 *
 * HasPhoneNumber=matchOnError=false | Matches when the recipient has a phone number
 *
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 *
 * @author Martijn Brinkers
 *
 */
@SuppressWarnings("java:S6813")
public class HasPhoneNumber extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(HasPhoneNumber.class);

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    protected static final String ACTIVATION_CONTEXT_KEY = "hasPhoneNumber";

    /*
     * Keeps track of Passwords of matching users
     */
    protected static class HasPhoneNumberActivationContext
    {
        private final PhoneNumbers phoneNumbers = new PhoneNumbers();

        public PhoneNumbers getPhoneNumbers() {
            return phoneNumbers;
        }

        public void clear() {
            phoneNumbers.clear();
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(transactionOperations);
    }

    private Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses,
            final HasPhoneNumberActivationContext context)
    throws MessagingException
    {
        MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = HasPhoneNumber.this::hasMatch;

        MailAddressMatcher matcher = new MailAddressMatcher(transactionOperations, userWorkflow,
                hasMatchEventHandler, getLogger());

        return matcher.getMatchingMailAddressesWithRetry(mailAddresses, new AbstractRetryListener()
        {
            @Override
            public <T, E extends Throwable> void onError(RetryContext retryContext, RetryCallback<T, E> retryCallback,
                    Throwable throwable)
            {
                context.clear();
            }
        });
    }

    private SMSProperties createSMSProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMSPropertiesImpl.class)
                .createInstance(properties);
    }

    @VisibleForTesting
    protected boolean hasMatch(@Nonnull User user)
    throws MessagingException
    {
        boolean match = false;

        String smsPhoneNumber;

        try {
            // TODO create PhoneBook API. The default implementation will return the phone number from the
            // user property. The phone book implementation can however be replaced (from spring xml) by
            // an implementation that reads the number from some external resource (for example LDAP).

            smsPhoneNumber = createSMSProperties(user.getUserPreferences()
                    .getProperties()).getSMSPhoneNumber();
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Exception getting smsPhoneNumber.", e);
        }

        if (smsPhoneNumber != null)
        {
            PhoneNumber phoneNumber = new PhoneNumber(smsPhoneNumber);

            HasPhoneNumberActivationContext hasPhoneNumberContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                    HasPhoneNumberActivationContext.class);

            hasPhoneNumberContext.getPhoneNumbers().put(user.getEmail(), phoneNumber);

            match = true;
        }
        else {
            logger.debug("User {} has no sms phoneNumber", user.getEmail());
        }

        return match;
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        HasPhoneNumberActivationContext context = new HasPhoneNumberActivationContext();

        getActivationContext().set(ACTIVATION_CONTEXT_KEY, context);

        Collection<MailAddress> matchingUsers = getMatchingMailAddresses(MailAddressUtils.getRecipients(mail), context);

        CoreApplicationMailAttributes.setPhoneNumbers(mail, context.getPhoneNumbers());

        return matchingUsers;
    }
}
