/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.stats;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.util.Functional;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class LogStatsSubscriber implements StatsSubscriber
{
    private static final String STATS_LOGGER_NAME = "stats";

    private static final Logger logger = LoggerFactory.getLogger(LogStatsSubscriber.class);

    /*
     * Logger which will result in logging to a separate log file
     */
    private static final Logger statsLogger = LoggerFactory.getLogger(STATS_LOGGER_NAME);

    /*
     * String return result when a parameter value results in an error
     */
    static final String ERROR = "<error>";

    /*
     * If true, stats will be written to the stats logger
     */
    private boolean enabled;

    private record Stats(
            long time,
            String name,
            String sender,
            String originator,
            List<String> recipients,
            long size,
            String mailID) {}

    private final MessageOriginatorIdentifier messageOriginatorIdentifier;

    public LogStatsSubscriber(@Nonnull MessageOriginatorIdentifier messageOriginatorIdentifier) {
        this.messageOriginatorIdentifier = messageOriginatorIdentifier;
    }

    @Override
    public void notifyStats(@Nonnull String name, @Nonnull StatsEvent statsEvent)
    throws IOException
    {
        if (!enabled) {
            return;
        }

        Mail mail = statsEvent.getMail();

        String sender = null;
        String originator = null;
        List<String> recipients = null;
        long size = 0;
        String mailID = null;

        if (mail != null)
        {
            sender = Functional.catchAllGet(() -> mail.getMaybeSender().asString(), ERROR, logger);
            originator = Functional.catchAllGet(() ->  messageOriginatorIdentifier.getOriginator(mail).toString(),
                    ERROR, logger);
            recipients = Functional.catchAllGet(() ->  mail.getRecipients().stream()
                    .map(MailAddress::toString).toList(), Collections.emptyList(), logger);
            size = Functional.catchAllGet(mail::getMessageSize, 0L, logger);
            mailID = Functional.catchAllGet(() -> CoreApplicationMailAttributes.getMailID(mail), ERROR, logger);
        }

        statsLogger.atInfo().log(JacksonUtil.getObjectMapper().writeValueAsString(
                new Stats(
                        System.currentTimeMillis(),
                        name,
                        sender,
                        originator,
                        recipients,
                        size,
                        mailID)));
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
