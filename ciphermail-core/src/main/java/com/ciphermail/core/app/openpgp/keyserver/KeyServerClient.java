/*
 * Copyright (c) 2014-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.openpgp.keyserver;

import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServers;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Client interface for communicating with a key server
 *
 * @author Martijn Brinkers
 *
 */
public interface KeyServerClient
{
    /**
     * Searches the key servers for the specified keys and returns the results from all key servers.
     * If a search on a key server fails, the error message will be stored and the next server will be tried.
     * maxKeys limit the number of keys that will be returned.
     */
    @Nonnull KeyServerClientSearchResult searchKeys(String query, boolean exact, Integer maxKeys)
    throws IOException;

    /**
     * Returns the keys from the key server with the given Key ID. The keys from the first key server with a result
     * will be returned (i.e., if a key server returns a valid key ring, the other servers won't be tried).
     */
    PGPPublicKeyRingCollection getKeys(String keyID)
    throws IOException;

    /**
     * Submits the keys from the keyRingCollection to all the key servers. If a submission fails, the error message
     * will be stored and the next server will be tried.
     */
    KeyServerClientSubmitResult submitKeys(PGPPublicKeyRingCollection keyRingCollection)
    throws IOException;

    /**
     * Sets the registered HPK key servers
     */
    void setServers(HKPKeyServers servers)
    throws IOException;

    /**
     * Gets the registered HPK key servers
     */
    @Nonnull HKPKeyServers getServers()
    throws IOException;

    /**
     * If true, duplicate results are suppressed
     */
    boolean isSkipDuplicateKeys();
    void setSkipDuplicateKeys(boolean skipDuplicateKeys);
}
