/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractHeaderValueRegEx extends AbstractContextAwareCipherMailMatcher
{
    protected abstract String getHeader(Mail mail)
    throws MessagingException;

    protected abstract Pattern getHeaderValuePattern(Mail mail)
    throws MessagingException;

    protected Collection<MailAddress> onMatch(Mail mail, Matcher matcher)
    throws MessagingException, IOException
    {
        return MailAddressUtils.getRecipients(mail);
    }

    protected Collection<MailAddress> onNoMatch(Mail mail)
    throws MessagingException
    {
        return Collections.emptyList();
    }

    /*
     * Override to add functionality
     */
    protected void logOnMatch(Mail mail)
    {
        String mailID = null;

        if (mail != null) {
            mailID = CoreApplicationMailAttributes.getMailID(mail);
        }

        getLogger().debug("#logOnMatch. MailID: {}", mailID);
    }

    /*
     * Override to add functionality
     */
    protected void logOnNoMatch(Mail mail)
    {
        String mailID = null;

        if (mail != null) {
            mailID = CoreApplicationMailAttributes.getMailID(mail);
        }

        getLogger().debug("#logOnNoMatch. MailID: {}", mailID);
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        try {
            String header = getHeader(mail);

            if (StringUtils.isNotEmpty(header))
            {
                String[] headerValues = mail.getMessage().getHeader(header);

                if (headerValues != null)
                {
                    Pattern pattern = getHeaderValuePattern(mail);

                    if (pattern != null)
                    {
                        for (String headerValue : headerValues)
                        {
                            headerValue = HeaderUtils.decodeHeaderValue(headerValue);

                            Matcher matcher = pattern.matcher(headerValue);

                            if (matcher.find())
                            {
                                logOnMatch(mail);

                                return onMatch(mail, matcher);
                            }
                        }
                    }
                }
            }
        }
        catch (IOException e) {
            throw new MessagingException("Message cannot be read.", e);
        }

        logOnNoMatch(mail);

        return onNoMatch(mail);
    }
}