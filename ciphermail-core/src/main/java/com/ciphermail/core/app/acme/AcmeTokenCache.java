/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.core.app.acme;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The AcmeTokenCache class represents a token-challenge cache used in ACME (Automated Certificate Management
 * Environment) operations. It provides methods to store, retrieve, and remove challenges associated with tokens.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(
        fieldVisibility = JsonAutoDetect.Visibility.ANY,
        setterVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE,
        creatorVisibility = JsonAutoDetect.Visibility.NONE
)
public class AcmeTokenCache
{
    public record Challenge(String authorization, long created){}

    private final Map<String, Challenge> tokenChallengeMap = new HashMap<>();

    public Challenge getChallenge(@Nonnull String token) {
        return tokenChallengeMap.get(token);
    }

    public void addChallenge(@Nonnull String token, @Nonnull String authorization) {
        tokenChallengeMap.put(token, new Challenge(authorization, System.currentTimeMillis()));
    }

    public void removeChallenge(@Nonnull String token) {
        tokenChallengeMap.remove(token);
    }

    public List<String> getStaleTokens(long maxAge)
    {
        long now = System.currentTimeMillis();

        return tokenChallengeMap.entrySet().stream().filter(entry -> entry.getValue().created < (now - maxAge))
                .map(Map.Entry::getKey).toList();
    }
}
