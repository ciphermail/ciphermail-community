/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.admin;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.util.SizeUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.Table;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.UuidGenerator;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Hibernate entity for the Admin (@See Admin)
 */
@Entity(name = "Admin")
@Table
public class Admin
{
    /*
     * Name of the database column
     */
    static final String NAME_COLUMN = "name";

    /*
     * names of keys for values stored in nameValues
     */
    static final String PASSWORD_NAME = "password";
    static final String AUTHENTICATION_TYPE = "authentication-type";
    static final String IP_ADDRESSES = "ip-addresses";
    static final String TFA_ENABLED = "tfa-enabled";
    static final String TFA_SECRET = "tfa-secret";

    @Id
    @UuidGenerator
    private UUID id;

    @Column(name = NAME_COLUMN, unique = true, nullable = false, length = 255)
    private String name;

    @ManyToMany(cascade = CascadeType.PERSIST)
    // Hibernate will replace the instance and should therefore be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<Role> roles = new HashSet<>();

    /*
     * General name/value storage which we can use to store additional information without requiring
     * any database changes
     */
    @ElementCollection
    @Column(name = "value", length = SizeUtils.KB * 32)
    @MapKeyColumn(name = "name")
    // Hibernate will replace the instance and should therefore be final
    @SuppressWarnings("FieldMayBeFinal")
    private Map<String, String> nameValues = new HashMap<>();

    protected Admin() {
        // Required by Hibernate
    }

    public Admin(@Nonnull String name) {
        this.name = Objects.requireNonNull(name);
    }

    public @Nonnull String getName() {
        return name;
    }

    /**
     * Sets the authentication type for the admin.
     *
     * @param authenticationType The authentication type to set. Cannot be null.
     */
    public Admin setAuthenticationType(@Nonnull AuthenticationType authenticationType)
    {
        nameValues.put(AUTHENTICATION_TYPE, Objects.requireNonNull(authenticationType).name());

        return this;
    }

    /**
     * Retrieves the authentication type for the admin.
     *
     * @return The authentication type of the admin, or null if not set.
     */
    public AuthenticationType getAuthenticationType()
    {
        return Optional.ofNullable(nameValues.get(AUTHENTICATION_TYPE)).map(AuthenticationType::valueOf)
                .orElse(null);
    }

    /**
     * Sets the list of IP addresses for the admin.
     *
     * @param ipAddresses The list of IP addresses to set for the admin. Cannot be null.
     */
    public Admin setIpAddresses(@Nonnull List<String> ipAddresses)
    {
        try {
            nameValues.put(IP_ADDRESSES, JacksonUtil.getObjectMapper().writeValueAsString(ipAddresses));

            return this;
        }
        catch (JsonProcessingException e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * Retrieves the list of IP addresses for the admin.
     *
     * @return The list of IP addresses for the admin. If the IP addresses are not set or if the value is null,
     *         it returns an empty list.
     * @throws UnhandledException if there is an IOException while reading the value from the nameValues map.
     */
    public @Nonnull List<String> getIpAddresses()
    {
        String value = StringUtils.trimToNull(nameValues.get(IP_ADDRESSES));

        try {
            return value != null ? JacksonUtil.getObjectMapper().readValue(value.getBytes(StandardCharsets.UTF_8),
                    new TypeReference<>(){}) : Collections.emptyList();
        }
        catch (IOException e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * Sets the (encoded) password for the admin.
     * <p>
     * Note: the password should be pre-encoded (for example, using Argon2PasswordEncoder)
     *
     * @param password The password to set for the admin. Cannot be null.
     * @return The current admin object with the password set.
     */
    public Admin setPassword(@Nonnull String password)
    {
        nameValues.put(PASSWORD_NAME, password);

        return this;
    }

    /**
     * Retrieves the encoded password of the admin.
     *
     * @return The encoded password of the admin, or null if not set.
     */
    public String getPassword() {
        return nameValues.get(PASSWORD_NAME);
    }

    /**
     * Enables or disables two-factor authentication (2FA) for the user.
     *
     * @param tfaEnabled boolean flag indicating whether to enable (true)
     *                   or disable (false) 2FA.
     */
    public void setEnable2FA(boolean tfaEnabled) {
        nameValues.put(TFA_ENABLED, Boolean.toString(tfaEnabled));
    }

    /**
     * Checks whether two-factor authentication (2FA) is enabled.
     *
     * @return true if 2FA is enabled, false otherwise.
     */
    public boolean isEnable2FA() {
        return Boolean.parseBoolean(nameValues.get(TFA_ENABLED));
    }

    /**
     * Sets the two-factor authentication (2FA) secret.
     *
     * @param tfaSecret The two-factor authentication secret.
     */
    public void set2FASecret(String tfaSecret) {
        nameValues.put(TFA_SECRET, tfaSecret);
    }

    /**
     * Retrieves the 2FA (Two-Factor Authentication) secret from the nameValues map.
     *
     * @return the 2FA secret as a String, or null if the key TFA_SECRET does not exist in the map
     */
    public String get2FASecret() {
        return nameValues.get(TFA_SECRET);
    }

    /**
     * Retrieves the roles assigned to the admin.
     *
     * @return A set of Role objects representing the roles assigned to the admin.
     */
    public @Nonnull Set<Role> getRoles() {
        return Collections.checkedSet(roles, Role.class);
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * Admin is equals iff username is equal.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Admin otherAdminEntity)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(name, otherAdminEntity.name)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(name)
            .toHashCode();
    }
}
