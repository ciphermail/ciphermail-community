/*
 * Copyright (c) 2021-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.security.password.validator.PasswordStrengthValidator;
import com.ciphermail.core.common.util.JSONUtils;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.json.JSONException;
import org.json.JSONObject;
import org.passay.PasswordData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Objects;

/**
 * Matcher which matches if the password, stored in the mail attributes, for a recipient is a weak password according
 * to the password policy of the sender
 */
@SuppressWarnings({"java:S6813"})
public class IsWeakPassword extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(IsWeakPassword.class);

    /*
     * JSON property names
     */
    private static final String JSON_SPRING_SERVICE_ID_NAME_PARAM = "springServiceId";

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * The PasswordStrengthValidator service (from Spring) to use for validating the password
     */
    @Inject
    private PasswordStrengthValidator passwordStrengthValidator;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    throws MessagingException
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(passwordStrengthValidator);
        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(transactionOperations);

        String condition = StringUtils.trimToNull(getCondition());

        if (condition == null) {
            throw new IllegalArgumentException("condition is missing.");
        }

        // The condition should be a JSON string
        try {
            JSONObject parameters = new JSONObject(condition);

            JSONUtils.checkSupportedNamedParameters(parameters,
                    JSON_SPRING_SERVICE_ID_NAME_PARAM);

            String springServiceId = StringUtils.trimToNull(parameters.optString(
                    JSON_SPRING_SERVICE_ID_NAME_PARAM));

            if (springServiceId == null) {
                throw new MessagingException("springServiceId is missing");
            }
        }
        catch (JSONException e) {
            throw new IllegalArgumentException("Illegal JSON value", e);
        }
    }

    @VisibleForTesting
    protected boolean hasMatch(InternetAddress originator, User user, Passwords passwords)
    {
        // True until proven otherwise
        boolean weakPassword = true;

        PasswordContainer passwordContainer = passwords.get(user.getEmail());

        if (passwordContainer != null)
        {
            // The policy of the sender (originator) will be used
            weakPassword = !passwordStrengthValidator.isStrongPassword(new PasswordData(originator.getAddress(),
                    passwordContainer.getPassword()));

            getLogger().debug("Password for {}, is weak: {}", user.getEmail(), weakPassword);
        }
        else {
            getLogger().warn("No password found for user {}", user.getEmail());
        }

        return weakPassword;
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        Collection<MailAddress> result = new LinkedList<>();

        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        if (passwords != null)
        {
            InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

            MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = user ->
                    IsWeakPassword.this.hasMatch(originator, user, passwords);

            MailAddressMatcher matcher = new MailAddressMatcher(transactionOperations, userWorkflow,
                    hasMatchEventHandler, getLogger());

            result = matcher.getMatchingMailAddressesWithRetry(MailAddressUtils.getRecipients(mail));
        }
        else {
            getLogger().warn("No passwords found");
        }

        return result != null ? result : Collections.emptyList();
    }
}
