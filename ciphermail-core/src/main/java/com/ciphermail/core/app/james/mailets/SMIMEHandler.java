/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.EncryptionRecipientSelector;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.JamestUtils;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.james.SecurityInfoTags;
import com.ciphermail.core.app.properties.SMIMEProperties;
import com.ciphermail.core.app.properties.SMIMEPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.MimeMessageWithID;
import com.ciphermail.core.common.mail.SkipHeadersOutputStream;
import com.ciphermail.core.common.notification.NotificationService;
import com.ciphermail.core.common.notification.NotificationSeverity;
import com.ciphermail.core.common.notification.StandardNotificationFacilities;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.StaticKeysPKISecurityServices;
import com.ciphermail.core.common.security.certificate.X509CertificateNotificationMessage;
import com.ciphermail.core.common.security.certificate.validator.PKITrustCheckCertificateValidator;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.cms.SignerIdentifier;
import com.ciphermail.core.common.security.cms.SignerInfo;
import com.ciphermail.core.common.security.smime.SMIMEInspector;
import com.ciphermail.core.common.security.smime.SMIMESignedInspector;
import com.ciphermail.core.common.security.smime.handler.CertificateCollectionEvent;
import com.ciphermail.core.common.security.smime.handler.RecursiveSMIMEHandler;
import com.ciphermail.core.common.security.smime.handler.SMIMEHandlerException;
import com.ciphermail.core.common.security.smime.handler.SMIMEInfoHandlerImpl;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.ReadableOutputStreamBuffer;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import com.ciphermail.core.common.util.SizeUtils;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestOutputStream;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Mailet that tries to decrypt an encrypted S/MIME message, gets all certificates from the S/MIME message and adds
 * security info to the headers. The S/MIME handler can handle 'normal' message with attached S/MIME messages.
 */
@SuppressWarnings("java:S6813")
public class SMIMEHandler extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SMIMEHandler.class);

    /*
     * If certain subject tags (signed, encrypted etc.) should not be added, this special tag value can be used.
     * Settings the subject tag value to an empty string "" (in the GUI) will not work because that will result
     * in inheriting the value from the system factory settings. If the subject tag value is equal to <empty> we will
     * skip adding the tag
     */
    private static final String SKIP_SUBJECT_TAG = "<skip>";

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        REMOVE_SIGNATURE                                    ("removeSignature"),
        REMOVE_SIGNATURE_ATTRIBUTE                          ("removeSignatureAttribute"),
        IMPORT_CERTIFICATES                                 ("importCertificates"),
        ADD_CMS_CERTIFICATES                                ("addCMSCertificates"),
        MAX_CMS_CERTIFICATES                                ("maxCMSCertificates"),
        SEND_IMPORT_NOTIFICATION                            ("sendImportNotification"),
        ADD_INFO                                            ("addInfo"),
        DECRYPT                                             ("decrypt"),
        DECOMPRESS                                          ("decompress"),
        MAX_RECURSION                                       ("maxRecursion"),
        PROTECTED_HEADER                                    ("protectedHeader"),
        RETAIN_MESSAGE_ID                                   ("retainMessageID"),
        STRICT                                              ("strict"),
        STRICT_ATTRIBUTE                                    ("strictAttribute"),
        THRESHOLD                                           ("threshold"),
        SUBJECT_TEMPLATE                                    ("subjectTemplate"),
        HANDLED_PROCESSOR                                   ("handledProcessor"),
        CHECK_INVALID_7BIT_CHARS                            ("checkInvalid7BitChars"),
        CHECK_INVALID_7BIT_CHARS_ATTRIBUTE                  ("checkInvalid7BitCharsAttribute"),
        ABORT_DECRYPTION_ON_INVALID_7BIT_CHARS              ("abortDecryptionOnInvalid7BitChars"),
        ABORT_DECRYPTION_ON_INVALID_7BIT_CHARS_ATTRIBUTE    ("abortDecryptionOnInvalid7BitCharsAttribute");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    private class CertificateCollectionEventImpl implements CertificateCollectionEvent
    {
        private final boolean skipIfUntrusted;

        CertificateCollectionEventImpl(boolean skipIfUntrusted) {
            this.skipIfUntrusted = skipIfUntrusted;
        }

        @Override
        public void certificatesEvent(Collection<? extends X509Certificate> certificates)
        {
            if (certificates == null) {
                return;
            }

            for (X509Certificate certificate : certificates)
            {
                try {
                    if (!pKISecurityServices.getKeyAndCertStore().contains(certificate))
                    {
                        boolean add = true;

                        if (skipIfUntrusted)
                        {
                            // Add the certificates from the email as extra certificates because there can be
                            // intermediate certificates in the collection
                            PKITrustCheckCertificateValidator certificateValidator = pKISecurityServices.
                                    getPKITrustCheckCertificateValidatorFactory().createValidator(certificates);

                            if (!certificateValidator.isValid(certificate))
                            {
                                if (logger.isDebugEnabled())
                                {
                                    logger.debug("The following certificate is not trusted and will therefore " +
                                            "not be imported: {}", certificate);
                                }

                                add = false;
                            }
                        }

                        if (add)
                        {
                            // FIXME would be better to do this outside the current transaction so it's less
                            // likely that the transaction has to be retried
                            boolean imported = keyAndCertificateWorkflow.addCertificateTransacted(certificate);

                            if (imported && sendImportNotification)
                            {
                                notificationService.sendNotification(
                                        StandardNotificationFacilities.CERTIFICATE_IMPORT,
                                        NotificationSeverity.INFORMATIONAL,
                                        new X509CertificateNotificationMessage(certificate));
                            }
                        }
                    }
                }
                catch (Exception e) {
                    getLogger().error("Error adding certificate. Skipping this certificate.", e);
                }
            }
        }
    }

    /*
     * The maximum search depth for embedded S/MIME attachments.
     */
    private int maxRecursion = 32;

    /*
     * If true and the message is signed the signature will be removed.
     */
    private boolean removeSignature;

    /*
     * The name of the removeSignature Mail Attribute
     */
    private String removeSignatureAttribute;

    /*
     * The default value for checkInvalid7BitChars
     */
    private boolean checkInvalid7BitChars;

    /*
     * The name of the checkInvalid7BitChars Mail Attribute
     */
    private String checkInvalid7BitCharsAttribute;

    /*
     * The default value for abortDecryptionOnInvalid7BitChars
     */
    private boolean abortDecryptionOnInvalid7BitChars;

    /*
     * The name of the abortDecryptionOnInvalid7BitChars Mail Attribute
     */
    private String abortDecryptionOnInvalid7BitCharsAttribute;

    /*
     * If true, the message will only be decrypted for a recipient if the recipient
     * has the correct private key. In non-strict mode, if the message can be decrypted,
     * the message will be delivered undecrypted to all recipients.
     */
    private boolean strict;

    /*
     * The name of the strict Mail Attribute
     */
    private String strictAttribute;

    /*
     * If true all certificates attached to signed message will be imported into the certificate store
     */
    private boolean importCertificates = true;

    /*
     * If true the certificates from the CMS blob will be added to the CertPathBuilder for certificate validation
     */
    private boolean addCMSCertificates = true;

    /*
     * The max number of certificates extracted from the S/MIME blob to be added to the certificate validator.
     * This is a protection against injecting too many certs and slowing down the verification process
     */
    private int maxCMSCertificates = 20;

    /*
     * If true security info will be added to the message headers.
     */
    private boolean addInfo = true;

    /*
     * If true and the message is encrypted the message will be decrypted
     */
    private boolean decrypt = true;

    /*
     * If true and the message is compressed the message will be decompressed
     */
    private boolean decompress = true;

    /*
     * If true, the original Message-ID will be used for the handled message
     */
    private boolean retainMessageID = true;

    /*
     * The next processor for messages that were handled
     */
    private String handledProcessor;

    /*
     * Determines which headers will be encrypted and/or signed
     */
    private String[] protectedHeaders = new String[]{};

    /*
     * Provides PKI services (root store, path builder etc.)
     */
    @Inject
    private PKISecurityServices pKISecurityServices;

    /*
     * Is used for importing extracted certificates
     */
    @Inject
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    /*
     * For getting users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used for the selection of encryption certificates for particular user(s).
     */
    @Inject
    private EncryptionRecipientSelector encryptionRecipientSelector;

    /*
     * Is used to execute a method within a database transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * For sending certificate import notifications
     */
    @Inject
    private NotificationService notificationService;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    /*
     * If exceeded, messages are stored in a temp file and not in memory
     */
    private int threshold = SizeUtils.MB * 5;

    /*
     * The subject template. The first parameter is replaced with the current subject and the second
     * parameter is replaced with the encryption or signing tag.
     *
     *  Example subject:
     *
     *  %1$s %2$s
     */
    private String subjectTemplate = "%1$s %2$s";

    /*
     * If true and a new certificate is imported, a sendImportNotification will be sent
     */
    private boolean sendImportNotification = true;

    /*
     * Retry listener which just logs
     */
    private final RetryListener loggingRetryListener = new LoggingRetryListener(getLogger());

    @Override
    protected Logger getLogger() {
        return logger;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    private void initProtectedHeaders()
    {
        String param = getInitParameter(Parameter.PROTECTED_HEADER.name);

        if (param != null) {
            protectedHeaders = param.split("\\s*,\\s*");
        }
    }

    @Override
    public final void initMailet()
    throws MessagingException
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        Objects.requireNonNull(transactionOperations);
        Objects.requireNonNull(pKISecurityServices);
        Objects.requireNonNull(keyAndCertificateWorkflow);
        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(encryptionRecipientSelector);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(notificationService);

        removeSignature = getBooleanInitParameter(Parameter.REMOVE_SIGNATURE.name, removeSignature);
        removeSignatureAttribute = getInitParameter(Parameter.REMOVE_SIGNATURE_ATTRIBUTE.name);
        checkInvalid7BitChars = getBooleanInitParameter(Parameter.CHECK_INVALID_7BIT_CHARS.name, checkInvalid7BitChars);
        checkInvalid7BitCharsAttribute = getInitParameter(Parameter.CHECK_INVALID_7BIT_CHARS_ATTRIBUTE.name);
        abortDecryptionOnInvalid7BitChars = getBooleanInitParameter(
                Parameter.ABORT_DECRYPTION_ON_INVALID_7BIT_CHARS.name, abortDecryptionOnInvalid7BitChars);
        abortDecryptionOnInvalid7BitCharsAttribute = getInitParameter(
                Parameter.ABORT_DECRYPTION_ON_INVALID_7BIT_CHARS_ATTRIBUTE.name);
        strict = getBooleanInitParameter(Parameter.STRICT.name, strict);
        strictAttribute = getInitParameter(Parameter.STRICT_ATTRIBUTE.name);
        importCertificates = getBooleanInitParameter(Parameter.IMPORT_CERTIFICATES.name, importCertificates);
        addCMSCertificates = getBooleanInitParameter(Parameter.ADD_CMS_CERTIFICATES.name, addCMSCertificates);
        maxCMSCertificates = getIntegerInitParameter(Parameter.MAX_CMS_CERTIFICATES.name, maxCMSCertificates);
        addInfo = getBooleanInitParameter(Parameter.ADD_INFO.name, addInfo);
        decrypt = getBooleanInitParameter(Parameter.DECRYPT.name, decrypt);
        decompress = getBooleanInitParameter(Parameter.DECOMPRESS.name, decompress);
        retainMessageID = getBooleanInitParameter(Parameter.RETAIN_MESSAGE_ID.name, retainMessageID);
        maxRecursion = getIntegerInitParameter(Parameter.MAX_RECURSION.name, maxRecursion);
        threshold = getIntegerInitParameter(Parameter.THRESHOLD.name, threshold);
        subjectTemplate = getInitParameter(Parameter.SUBJECT_TEMPLATE.name, subjectTemplate);
        sendImportNotification = getBooleanInitParameter(Parameter.SEND_IMPORT_NOTIFICATION.name,
                sendImportNotification);

        handledProcessor = Objects.requireNonNull(getInitParameter(Parameter.HANDLED_PROCESSOR.name),
                Parameter.HANDLED_PROCESSOR.name + " parameter is missing");

        initProtectedHeaders();

        StrBuilder sb = new StrBuilder();

        sb.append("Remove signature: ").append(removeSignature);
        sb.appendSeparator("; ");
        sb.append("removeSignatureAttribute: ").append(removeSignatureAttribute);
        sb.appendSeparator("; ");
        sb.append("Check Invalid 7Bit Chars: ").append(checkInvalid7BitChars);
        sb.appendSeparator("; ");
        sb.append("checkInvalid7BitCharsAttribute: ").append(checkInvalid7BitCharsAttribute);
        sb.appendSeparator("; ");
        sb.append("Abort Decryption On Invalid 7Bit Chars: ").append(abortDecryptionOnInvalid7BitChars);
        sb.appendSeparator("; ");
        sb.append("abortDecryptionOnInvalid7BitCharsAttribute: ").append(abortDecryptionOnInvalid7BitCharsAttribute);
        sb.appendSeparator("; ");
        sb.append("Strict: ").append(strict);
        sb.appendSeparator("; ");
        sb.append("strictAttribute: ").append(strictAttribute);
        sb.appendSeparator("; ");
        sb.append("importCertificates: ").append(importCertificates);
        sb.appendSeparator("; ");
        sb.append("addCMSCertificates: ").append(addCMSCertificates);
        sb.appendSeparator("; ");
        sb.append("maxCMSCertificates: ").append(maxCMSCertificates);
        sb.appendSeparator("; ");
        sb.append("Add info: ").append(addInfo);
        sb.appendSeparator("; ");
        sb.append("Decrypt: ").append(decrypt);
        sb.appendSeparator("; ");
        sb.append("Decompress: ").append(decompress);
        sb.appendSeparator("; ");
        sb.append("Retain Message-ID: ").append(retainMessageID);
        sb.appendSeparator("; ");
        sb.append("handledProcessor: ").append(handledProcessor);
        sb.appendSeparator("; ");
        sb.append("Protected headers: ");
        sb.append(StringUtils.join(protectedHeaders, ","));
        sb.appendSeparator("; ");
        sb.append("Max recursion: ").append(maxRecursion);
        sb.appendSeparator("; ");
        sb.append("threshold: ").append(threshold);
        sb.appendSeparator("; ");
        sb.append("subjectTemplate: ").append(subjectTemplate);
        sb.appendSeparator("; ");
        sb.append("sendImportNotification: ").append(sendImportNotification);

        getLogger().info("{}", sb);
    }

    private boolean isStrict(Mail mail) {
        return MailAttributesUtils.attributeAsBoolean(mail, strictAttribute, this.strict);
    }

    private boolean isRemoveSignatureAttribute (Mail mail) {
        return MailAttributesUtils.attributeAsBoolean(mail, removeSignatureAttribute, this.removeSignature);
    }

    private boolean isCheckInvalid7BitCharsAttribute(Mail mail) {
        return MailAttributesUtils.attributeAsBoolean(mail, checkInvalid7BitCharsAttribute, this.checkInvalid7BitChars);
    }

    private boolean isAbortDecryptionOnInvalid7BitChars (Mail mail)
    {
        return MailAttributesUtils.attributeAsBoolean(mail, abortDecryptionOnInvalid7BitCharsAttribute,
                this.abortDecryptionOnInvalid7BitChars);
    }

    private void sendNewMessage(Mail sourceMail, Collection<MailAddress> recipients, MimeMessage message,
            String processor)
    throws MessagingException
    {
        // Create a clone of the email
        MailImpl newMail = MailImpl.duplicateWithoutMessage (sourceMail).build();

        try {
            newMail.setRecipients(recipients);
            newMail.setMessage(message);
            newMail.setState(processor);

            getMailetContext().sendMail(newMail);
        }
        finally {
            JamestUtils.dispose(newMail);
        }
    }

    @Override
    public void serviceMail(final Mail mail)
    {
        try {
            final MimeMessage sourceMessage = mail.getMessage();

            if (sourceMessage != null)
            {
                RetryTemplate retryTemplate = RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder()
                        .withListener(loggingRetryListener)
                        .build();

                Messages messages = retryTemplate.execute(ctx ->
                    transactionOperations.execute(status ->
                    {
                        try {
                            return isStrict(mail) ? handleMessageTransactedStrict(mail) :
                                handleMessageTransactedNonStrict(mail);
                        }
                        catch (MessagingException e) {
                            throw new UnhandledException(e);
                        }
                    })
                );

                if (messages != null)
                {
                    try {
                        Collection<MimeSource> mimeSources = messages.getMimeSources();

                        // Send all handled S/MIME message to the handledProcessor next processor
                        for (MimeSource mimeSource : mimeSources)
                        {
                            InputStream mimeInput = mimeSource.getMimeSource().getInputStream();

                            MimeMessage message = retainMessageID ? new MimeMessageWithID(
                                    MailSession.getDefaultSession(), mimeInput, sourceMessage.getMessageID()) :
                                    new MimeMessage(MailSession.getDefaultSession(), mimeInput);

                            sendNewMessage(mail, mimeSource.getRecipients(), message, handledProcessor);
                        }

                        // Send email to all the recipients that need the message unchanged
                        Set<MailAddress> asIsRecipients = messages.getAsIsRecipients();

                        if (CollectionUtils.isEmpty(asIsRecipients)) {
                            // We no longer need the original Mail so ghost it
                            mail.setState(Mail.GHOST);
                        }
                        else {
                            mail.setRecipients(asIsRecipients);
                        }
                    }
                    finally {
                        messages.close();
                    }
                }
            }
        }
        catch (UnhandledException | MessagingException | IOException e) {
            getLogger().error("Error handling the message.", e);
        }
    }

    private User getSenderUser(Mail mail)
    throws MessagingException, HierarchicalPropertiesException
    {
        InternetAddress originatorAddress = messageOriginatorIdentifier.getOriginator(mail);

        String sender = originatorAddress.getAddress();

        sender = EmailAddressUtils.canonicalizeAndValidate(sender, false);

        return userWorkflow.getUser(sender, UserNotExistResult.DUMMY_IF_NOT_EXIST);
    }

    private SMIMEProperties createSMIMEProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMIMEPropertiesImpl.class)
                .createInstance(properties);
    }

    private void addCertificatesEventListener(RecursiveSMIMEHandler recursiveSMIMEHandler, Mail mail)
    throws MessagingException
    {
        try {
            // Check if the "static" Mailet setting overrides the dynamic user setting
            if (importCertificates)
            {
                SMIMEProperties smimeProperties = createSMIMEProperties(getSenderUser(mail)
                        .getUserPreferences().getProperties());

                if (smimeProperties.getAutoImportCertificatesFromMail())
                {
                    recursiveSMIMEHandler.setCertificatesEventListener(new CertificateCollectionEventImpl(
                            smimeProperties.getAutoImportCertificatesSkipUntrusted()));
                }
            }
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error adding CertificatesEventListener", e);
        }
    }

    @VisibleForTesting
    protected Messages handleMessageTransactedNonStrict(Mail mail)
    throws MessagingException
    {
        MimeMessage sourceMessage = mail.getMessage();

        RecursiveSMIMEHandler recursiveSMIMEHandler = new RecursiveSMIMEHandler(pKISecurityServices);

        recursiveSMIMEHandler.setAddInfo(addInfo);
        recursiveSMIMEHandler.setDecrypt(decrypt);
        recursiveSMIMEHandler.setDecompress(decompress);
        recursiveSMIMEHandler.setMaxRecursion(maxRecursion);
        recursiveSMIMEHandler.setRemoveSignature(isRemoveSignatureAttribute(mail));
        recursiveSMIMEHandler.setCheckInvalid7BitChars(isCheckInvalid7BitCharsAttribute(mail));
        recursiveSMIMEHandler.setAbortDecryptionOnInvalid7BitChars(isAbortDecryptionOnInvalid7BitChars(mail));
        recursiveSMIMEHandler.setProtectedHeaders(protectedHeaders);
        recursiveSMIMEHandler.setMailID(CoreApplicationMailAttributes.getMailID(mail));

        addCertificatesEventListener(recursiveSMIMEHandler, mail);

        // Use a different SMIMEInfoHandler allowing us to add tags (like [encrypt]) to the subject of the message
        ExtSMIMEInfoHandlerImpl smimeInfoHandler = new ExtSMIMEInfoHandlerImpl(mail,
                MailAddressUtils.getRecipients(mail));

        smimeInfoHandler.setAddCMSCertificates(addCMSCertificates);
        smimeInfoHandler.setMaxCMSCertificates(maxCMSCertificates);

        recursiveSMIMEHandler.setSmimeInfoHandler(smimeInfoHandler);

        MimeMessage handledMessage;

        try {
            handledMessage = recursiveSMIMEHandler.handlePart(sourceMessage);

            // A message can contain attached S/MIME messages. We want some headers to be also added to the outer (main)
            // message. For example "Message Contains Invalid 7Bit Chars" and "Decryption Key Not Found". Main reason
            // for this is that this makes it easier to detect these emails just by looking at the main headers.
            // Otherwise you need to scan every part to see whether these headers are in some attached message.
            smimeInfoHandler.addOuterHeaders(handledMessage);
        }
        catch (SMIMEHandlerException e) {
            throw new MessagingException("Error handling message.", e);
        }

        Messages messages = new Messages(threshold);

        try {
            List<MailAddress> recipients = MailAddressUtils.getRecipients(mail);

            if (handledMessage != null) {
                messages.addMessage(handledMessage, recipients);
            }
            else {
                // The message was not encrypted, could not be decrypted etc.
                messages.addAsIsRecipient(recipients);
            }
        }
        catch (Exception e) {
            // We need to catch all to make sure messages will always be closed.
            // If not there is a change that temp files won't be deleted.
            messages.close();

            if (e instanceof RuntimeException runtimeException) {
                throw runtimeException;
            }

            if (e instanceof MessagingException messagingException) {
                throw messagingException;
            }

            throw new MessagingException("Error handling user", e);
        }

        return messages;
    }

    /*
     * Returns all the private keys that are selected for the user. The private keys are the automatically
     * selected keys (i.e., matching email address and valid), the manually selected and the inherited.
     */
    private Set<KeyAndCertificate> getKeyAndCertificates(User user)
    throws MessagingException
    {
        try {
            Set<KeyAndCertificate> keys = new HashSet<>();

            Set<X509Certificate> certificates = new HashSet<>();

            encryptionRecipientSelector.select(Collections.singleton(user), certificates);

            KeyAndCertStore keyAndCertStore = pKISecurityServices.getKeyAndCertStore();

            for (X509Certificate certificate : certificates)
            {
                X509CertStoreEntry entry = keyAndCertStore.getByCertificate(certificate);

                if (entry != null)
                {
                    KeyAndCertificate keyAndCertificate = keyAndCertStore.getKeyAndCertificate(entry);

                    if (keyAndCertificate != null && keyAndCertificate.getPrivateKey() != null) {
                        keys.add(keyAndCertificate);
                    }
                }
            }
            return keys;
        }
        catch (CertStoreException | KeyStoreException | HierarchicalPropertiesException e) {
            throw new MessagingException("Error getting key and certificates for user: " + user.getEmail(), e);
        }
    }

    private MimeMessage handleMessageForUser(Mail mail, User user)
    throws MessagingException
    {
        MimeMessage handledMessage = null;

        Set<KeyAndCertificate> keys = getKeyAndCertificates(user);

        if (logger.isDebugEnabled()) {
            logger.debug("Nr of keys: {}", keys.size());
        }

        RecursiveSMIMEHandler recursiveSMIMEHandler = new RecursiveSMIMEHandler(
                new StaticKeysPKISecurityServices(pKISecurityServices, keys));

        recursiveSMIMEHandler.setAddInfo(addInfo);
        recursiveSMIMEHandler.setDecrypt(decrypt);
        recursiveSMIMEHandler.setDecompress(decompress);
        recursiveSMIMEHandler.setMaxRecursion(maxRecursion);
        recursiveSMIMEHandler.setRemoveSignature(isRemoveSignatureAttribute(mail));
        recursiveSMIMEHandler.setCheckInvalid7BitChars(isCheckInvalid7BitCharsAttribute(mail));
        recursiveSMIMEHandler.setAbortDecryptionOnInvalid7BitChars(isAbortDecryptionOnInvalid7BitChars(mail));
        recursiveSMIMEHandler.setProtectedHeaders(protectedHeaders);
        recursiveSMIMEHandler.setMailID(CoreApplicationMailAttributes.getMailID(mail));

        addCertificatesEventListener(recursiveSMIMEHandler, mail);

        // Use a different SMIMEInfoHandler so we can add tags (like [encrypt]) to the subject of the message
        ExtSMIMEInfoHandlerImpl smimeInfoHandler = new ExtSMIMEInfoHandlerImpl(mail,
                MailAddressUtils.toMailAddressList(user.getEmail()));

        smimeInfoHandler.setAddCMSCertificates(addCMSCertificates);
        smimeInfoHandler.setMaxCMSCertificates(maxCMSCertificates);

        recursiveSMIMEHandler.setSmimeInfoHandler(smimeInfoHandler);

        try {
            handledMessage = recursiveSMIMEHandler.handlePart(mail.getMessage());

            // A message can contain attached S/MIME messages. We want some headers to be also added to the outer (main)
            // message. For example "Message Contains Invalid 7Bit Chars" and "Decryption Key Not Found". Main reason
            // for this is that this makes it easier to detect these emails just by looking at the main headers.
            // Otherwise you need to scan every part to see whether these headers are in some attached message.
            smimeInfoHandler.addOuterHeaders(handledMessage);
        }
        catch (SMIMEHandlerException | MessagingException e) {
            logger.error("Error checking for S/MIME for user {}", user.getEmail());
        }

        return handledMessage;
    }

    private Messages handleMessageTransactedStrict(Mail mail)
    throws MessagingException
    {
        Messages messages = new Messages(threshold);

        try {
            List<MailAddress> recipients = MailAddressUtils.getRecipients(mail);

            for (MailAddress recipient : recipients)
            {
                if (recipient == null) {
                    continue;
                }

                MimeMessage handledMessage = null;

                try {
                    handledMessage = handleMessageForUser(mail, userWorkflow.getUser(recipient.toString(),
                            UserNotExistResult.DUMMY_IF_NOT_EXIST));
                }
                catch (AddressException | HierarchicalPropertiesException e) {
                    throw new MessagingException("Error strict handling message for user: " + recipient, e);
                }

                if (handledMessage != null) {
                    messages.addMessage(handledMessage, recipient);
                }
                else {
                    // The message was not encrypted, could not be decrypted etc.
                    messages.addAsIsRecipient(recipient);
                }
            }

            return messages;
        }
        catch (Exception e) {
            // We need to catch all to make sure messages will always be closed.
            // If not there is a change that temp files won't be deleted.
            messages.close();

            if (e instanceof RuntimeException runtimeException) {
                throw runtimeException;
            }

            if (e instanceof MessagingException messagingException) {
                throw messagingException;
            }

            throw new MessagingException("Error handling user", e);
        }
    }

    /*
     * Stores the message source and the recipients of the message.
     */
    private static class MimeSource
    {
        /*
         * Buffer that stores the raw mime source of the message. If the number of bytes
         * written to ReadableOutputStreamBuffer exceeds the threshold, the bytes are
         * written to a temporary file.
         */
        final ReadableOutputStreamBuffer mimeSource;

        /*
         * The recipients of the message
         */
        final Set<MailAddress> recipients = new HashSet<>();

        MimeSource(ReadableOutputStreamBuffer mimeSource) {
            this.mimeSource = mimeSource;
        }

        Set<MailAddress> getRecipients() {
            return recipients;
        }

        ReadableOutputStreamBuffer getMimeSource() {
            return mimeSource;
        }
    }

    /*
     * Keeps track of all the resulting messages when messages are being handled by the
     * RecursiveSMIMEHandler.
     */
    @VisibleForTesting
    protected static class Messages
    {
        /*
         * The memory threshold of the buffer
         */
        final int threshold;

        /*
         * Mapping from SHA1 hash of the message to the message and recipients for the message
         */
        final Map<String, MimeSource> digestedMessages = new HashMap<>();

        /*
         * Set of recipients to which the message should be send as-is, i.e., the message
         * was not an S/MIME message or the message could not be decrypted for the
         * recipient
         */
        final Set<MailAddress> asIsRecipients = new HashSet<>();

        /*
         * Keep track of all the buffers that must be closed
         */
        final Set<ReadableOutputStreamBuffer> buffers = new HashSet<>();

        Messages(int threshold) {
            this.threshold = threshold;
        }

        /*
         * Returns a message digest used to calculate the hash of the message
         */
        MessageDigest createDigest()
        throws IOException
        {
            try {
                return SecurityFactoryFactory.getSecurityFactory().createMessageDigest("SHA1");
            }
            catch (Exception e) {
                throw new IOException(e);
            }
        }

        /*
         * Returns the available threshold.
         */
        int getThreshold()
        {
            long newThreshold = this.threshold;

            // calculate the available threshold based on the number of bytes
            // currently used by the buffers
            for (ReadableOutputStreamBuffer buffer : buffers)
            {
                // If byte count of the buffer exceeds,
                newThreshold = newThreshold - buffer.getByteCount();

                if (newThreshold < 0)
                {
                    newThreshold = 0;
                    break;
                }
            }

            return (int) newThreshold;
        }

        void releaseBuffer(ReadableOutputStreamBuffer buffer)
        {
            IOUtils.closeQuietly(buffer);
            buffers.remove(buffer);
        }

        // Closes all buffers.
        // Note: close should always be called to make sure that all temporary files are cleaned up
        void close()
        {
            for (ReadableOutputStreamBuffer buffer : buffers) {
                IOUtils.closeQuietly(buffer);
            }
        }

        MimeSource addMessage(MimeMessage message)
        throws MessagingException, IOException
        {
            message.saveChanges();

            ReadableOutputStreamBuffer mime = new ReadableOutputStreamBuffer(getThreshold());

            // Store a reference to make sure we can always close the buffers even
            // when an exception has occurred.
            buffers.add(mime);

            // Calculate a SHA1 of the message content so we can check wether this message
            // is the same message as another message. The reason for doing this is that
            // when in strict mode, a message with multiple recipents is decrypted for
            // each recipient. If the resulting message is the same, we do need to
            // create multiple message but can add the recipient as a recipient of that
            // particular message. This is an optimization because with domain encryption
            // it can happen that a message has hundreds of recipients all encrypted with
            // the same key. We do not want such a message to be exploded into hundreds
            // of separate messages.
            DigestOutputStream digestOutputStream = new DigestOutputStream(NullOutputStream.INSTANCE,
                    createDigest());

            // We need to split the mime output because we only want to calculate the SHA1
            // of the message body. The main reason for this is that saveChanges will add
            // a new message-id for each message.
            TeeOutputStream output = new TeeOutputStream(new BufferedOutputStream(mime),
                    new SkipHeadersOutputStream(digestOutputStream));

            message.writeTo(output);

            // We cannot close the mime stream yet because we need to be able to read the
            // content. We therefore need to flush all buffers.
            digestOutputStream.flush();

            String messageDigest = HexUtils.hexEncode(digestOutputStream.getMessageDigest().digest());

            MimeSource mimeSource = digestedMessages.get(messageDigest);

            if (mimeSource == null)
            {
                mimeSource = new MimeSource(mime);

                digestedMessages.put(messageDigest, mimeSource);
            }
            else {
                // We don't need to keep the mime content because the mime content
                // has already been stored. We will release it to save memory.
                releaseBuffer(mime);
            }

            return mimeSource;
        }

        void addMessage(MimeMessage message, Collection<MailAddress> recipients)
        throws MessagingException
        {
            try {
                addMessage(message).getRecipients().addAll(recipients);
            }
            catch (IOException e) {
                throw new MessagingException("Error adding message.", e);
            }
        }

        void addMessage(MimeMessage message, MailAddress recipient)
        throws MessagingException
        {
            try {
                addMessage(message).getRecipients().add(recipient);
            }
            catch (IOException e) {
                throw new MessagingException("Error adding message.", e);
            }
        }

        void addAsIsRecipient(Collection<MailAddress> recipients) {
            asIsRecipients.addAll(recipients);
        }

        void addAsIsRecipient(MailAddress recipient) {
            asIsRecipients.add(recipient);
        }

        Collection<MimeSource> getMimeSources() {
            return digestedMessages.values();
        }

        public Set<MailAddress> getAsIsRecipients() {
            return asIsRecipients;
        }
    }

    /*
     * Extension of SMIMEInfoHandlerImpl which is used for adding security information to the subject
     */
    private class ExtSMIMEInfoHandlerImpl extends SMIMEInfoHandlerImpl
    {
        /*
         * The tags that will be added to the subject (can be null)
         */
        private final SecurityInfoTags tags;

        /*
         * The MailID of the Mail
         */
        private final String mailID;

        /*
         * The recipients of the mail
         */
        private final Collection<MailAddress> recipients;

        public ExtSMIMEInfoHandlerImpl(Mail mail, Collection<MailAddress> recipients)
        {
            super(pKISecurityServices);

            this.recipients = recipients;

            tags = CoreApplicationMailAttributes.getSecurityInfoTags(mail);
            mailID = CoreApplicationMailAttributes.getMailID(mail);
        }

        private void addTagToSubject(MimeMessage message, String tag)
        {
            if (SKIP_SUBJECT_TAG.equals(tag))
            {
                logger.debug("Skip tag detected. Skip adding the subject tag.");

                return;
            }

            try {
                String currentSubject = MailUtils.getSafeSubject(message);

                String newSubject = String.format(subjectTemplate, StringUtils.defaultString(currentSubject),
                        StringUtils.defaultString(tag));

                message.setSubject(newSubject);
            }
            catch (Exception e) {
                logger.error("Error while appending text to subject");
            }
        }

        private boolean isSenderMismatch(MimeMessage message, Set<String> signers)
        {
            // Get the first From header and canonicalize it
            String canonicalizedFrom = EmailAddressUtils.canonicalize(EmailAddressUtils.getEmailAddress(
                    EmailAddressUtils.getAddress(EmailAddressUtils.getFromQuietly(message))));

            if (signers != null)
            {
                for (String signer : signers)
                {
                    if (StringUtils.equals(canonicalizedFrom, EmailAddressUtils.canonicalize(signer))) {
                        // On of the signers is equals to the from so no mismatch
                        return false;
                    }
                }
            }

            return true;
        }

        private String getSignerInfo(SMIMEInspector sMIMEInspector)
        {
            StrBuilder sb = new StrBuilder();

            try {
                if (sMIMEInspector != null)
                {
                    SMIMESignedInspector inspector = sMIMEInspector.getSignedInspector();

                    if (inspector != null)
                    {
                        List<SignerInfo> signers = inspector.getSigners();

                        if (signers != null)
                        {
                            for (SignerInfo signerInfo : signers)
                            {
                                if (signerInfo != null)
                                {
                                    SignerIdentifier signerId = signerInfo.getSignerId();

                                    if (signerId != null)
                                    {
                                        sb.appendSeparator(',').append(signerId.getIssuer()).append("/").
                                            append(BigIntegerUtils.hexEncode(signerId.getSerialNumber())).append("/").
                                            append(HexUtils.hexEncode(signerId.getSubjectKeyIdentifier()));

                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
                logger.error("Error getting signer info", e);
            }

            return sb.toString();
        }

        @Override
        protected void onSigned(MimeMessage message, SMIMEInspector sMIMEInspector, int level, boolean signatureValid,
                Set<String> signers)
        {
            if (signatureValid) {
                logger.info("S/MIME signature was valid; MailID: {}", mailID);
            }
            else {
                logger.warn("S/MIME signature was not valid; Signer IDs: {}; MailID: {}", getSignerInfo(sMIMEInspector),
                        mailID);
            }

            if (tags == null) {
                // there is nothing to add to the subject so return
                return;
            }

            String tag;

            if (signatureValid) {
                // If there is a mismatch between "sender" and email address from certificate, show the
                // email address(es) of the signer(s).
                if (isSenderMismatch(message, signers))
                {
                    // There is a mismatch between the from and the signers. We will therefore add the signers
                    // to the subject
                    try {
                        tag = String.format(tags.getSignedByValidTag(), StringUtils.defaultString(
                                StringUtils.join(signers, ", ")));
                    }
                    catch (Exception e)
                    {
                        logger.error("Invalid format string", e);

                        tag = "<invalid format string>";
                    }
                }
                else {
                    tag = tags.getSignedValidTag();
                }
            }
            else {
                tag = tags.getSignedInvalidTag();
            }

            addTagToSubject(message, tag);
        }

        @Override
        protected void onEncrypted(MimeMessage message, SMIMEInspector sMIMEInspector, int level, boolean decrypted)
        {
            // Log the decryption event (see https://jira.djigzo.com/browse/GATEWAY-42).
            // Note: we will also log the MailID otherwise it's unclear for which message the log entry is
            if (decrypted)
            {
                getLogger().info("S/MIME message has been decrypted. MailID: {}; Recipients: {}",
                        mailID, recipients);
            }

            if (tags == null) {
                // there is nothing to add
                return;
            }

            if (decrypted) {
                addTagToSubject(message, tags.getDecryptedTag());
            }
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (handledProcessor != null) {
            requiredProcessors.add(new ProcessingState(handledProcessor));
        }

        return requiredProcessors;
    }
}
