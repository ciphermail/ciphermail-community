/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.apache.commons.lang.text.StrBuilder;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * PasswordContainer is used to temporarily store passwords, typically ephemeral passwords like OTP generated passwords,
 * in a mail object.
 *
 * @author Martijn Brinkers
 *
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PasswordContainer
{
    /*
     * Key name prefix for sensitive variables
     */
    protected static final String SENSITIVE_KEY_PREFIX = "sensitive";

    /*
     * Names of keys for the value from store
     */
    private static final String PASSWORD_KEY = SENSITIVE_KEY_PREFIX + ":password";
    private static final String PASSWORD_ID_KEY = "passwordID";
    private static final String PASSWORD_LENGTH_KEY = "passwordLength";

    /*
     * Store for variables
     */
    protected final Map<String, Object> store = Collections.synchronizedMap(new HashMap<>());

    public static PasswordContainer createInstance() {
        return new PasswordContainer();
    }

    /**
     * Construct an empty PasswordContainer
     */
    public PasswordContainer()
    {
        // Empty on purpose
    }

    /**
     * Sets the ephemeral password which will be stored in the Mail object
     */
    public PasswordContainer setPassword(String password)
{
        store.put(PASSWORD_KEY, password);

        return this;
    }

    /**
     * @return The ephemeral password stored in the Mail object
     */
    public String getPassword() {
        return (String) store.get(PASSWORD_KEY);
    }

    /**
     * Sets the ID which identifies the password. This will be used to identify which password should be used when
     * a recipient receives multiple passwords (for example, by SMS text)
     */
    public PasswordContainer setPasswordID(String passwordID)
    {
        store.put(PASSWORD_ID_KEY, passwordID);

        return this;
    }

    /**
     * @return The password ID
     */
    public String getPasswordID() {
        return (String) store.get(PASSWORD_ID_KEY);
    }

    /**
     * Sets the length of the generated password. This is used with OTP mode.
     */
    public PasswordContainer setPasswordLength(Integer passwordLength)
    {
        store.put(PASSWORD_LENGTH_KEY, passwordLength);

        return this;
    }

    /**
     * @return The password length
     */
    public Integer getPasswordLength() {
        return (Integer) store.get(PASSWORD_LENGTH_KEY);
    }

    @Override
    public String toString()
    {
        StrBuilder sb = new StrBuilder();

        store.forEach((key, value) -> sb.appendSeparator(", ").append(key).append(":").append(
                key.startsWith(SENSITIVE_KEY_PREFIX) ? "***" : value));

        sb.insert(0, getClass().getSimpleName() + "[").append("]");

        return sb.toString();
    }
}
