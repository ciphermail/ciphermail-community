/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import org.apache.james.lifecycle.api.Disposable;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

public class JamestUtils
{
    private static final Logger logger = LoggerFactory.getLogger(JamestUtils.class);

    private static final int BYTE_MASK = 0x0f;

    private static final char[] HEX_DIGITS = new char[] {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    /*
     * File extension for Mail object files
     */
    public static final String MAIL_OBJECT_FILE_EXTENSION = ".FileObjectStore";

    /*
     * File extension for Mail MIME stream files
     */
    public static final String MAIL_MIME_FILE_EXTENSION = ".FileStreamStore";

    private JamestUtils() {
        // Empty on purpose
    }

    /**
     * Creates a filename which identifies the Mail object. James spool manager requires that the filename is based
     * on the name of the mail item.
     */
    // TODO: CHECK IF WE STILL NEED A FILENAME WHICH IS BASED ON THE NAME OF THE MAIL ITEM?
    public static String createBaseFileName(@Nonnull Mail mail)
    {
        // A mail item should have a unique name
        String mailName = mail.getName();

        if (mailName == null)
        {
            // Should not happen in production. Might happen during tests
            logger.warn("Mail item has no name");

            mailName = MailImpl.getId();

            mail.setName(mailName);
        }

        byte[] bytes = mail.getName().getBytes();
        char[] buffer = new char[bytes.length << 1];

        for(int i = 0, j = 0; i < bytes.length; i++)
        {
            int k = bytes[ i ];
            buffer[ j++ ] = HEX_DIGITS[ ( k >>> 4 ) & BYTE_MASK ];
            buffer[ j++ ] = HEX_DIGITS[ k & BYTE_MASK ];
        }

        return new String(buffer);
    }

    /**
     * Dispose the given object if it's an instanceof {@link Disposable}
     */
    public static void dispose(Object obj)
    {
        if (obj instanceof Disposable disposable) {
            disposable.dispose();
        }
    }
}
