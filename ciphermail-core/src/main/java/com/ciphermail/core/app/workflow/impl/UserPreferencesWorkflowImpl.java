/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow.impl;

import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.impl.hibernate.UserPreferencesDAO;
import com.ciphermail.core.app.impl.hibernate.UserPreferencesEntity;
import com.ciphermail.core.app.impl.hibernate.UserPreferencesHibernate;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserPreferencesWorkflow;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.hibernate.NamedBlobEntity;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreEntity;

import javax.annotation.Nonnull;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class UserPreferencesWorkflowImpl implements UserPreferencesWorkflow
{
    /*
     * The store with certificates and keys
     */
    private final KeyAndCertStore keyAndCertStore;

    /*
     * Handles database sessions
     */
    private final SessionManager sessionManager;

    /*
     * Provides a factory for creating UserPropertiesImpl instances
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    public UserPreferencesWorkflowImpl(
            @Nonnull KeyAndCertStore keyAndCertStore,
            @Nonnull SessionManager sessionManager,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        this.keyAndCertStore = Objects.requireNonNull(keyAndCertStore);
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
    }

    /*
     * Creates a UserPreferences from a UserPreferencesEntity.
     */
    private UserPreferences createUserPreferences(@Nonnull UserPreferencesEntity entity)
    {
        return new UserPreferencesHibernate(entity, keyAndCertStore, sessionManager.getSession(),
                userPropertiesFactoryRegistry);
    }

    @Override
    public List<UserPreferences> getReferencingFromCertificates(@Nonnull X509Certificate certificate,
            Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        List<UserPreferences> result = new LinkedList<>();

        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        // We can only check if the X509CertStoreEntry is a Hibernate entity.
        if (certificateEntry instanceof X509CertStoreEntity x509CertStoreEntity)
        {
            List<UserPreferencesEntity> entities = createDAO().getReferencingFromCertificates(
                    x509CertStoreEntity, firstResult, maxResults);

            if (entities != null)
            {
                for (UserPreferencesEntity entity : entities) {
                    result.add(createUserPreferences(entity));
                }
            }
        }

        return result;
    }

    @Override
    public long getReferencingFromCertificatesCount(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        long result = 0;

        // We can only check if the X509CertStoreEntry is a Hibernate entity.
        if (certificateEntry instanceof X509CertStoreEntity x509CertStoreEntity) {
            result = createDAO().referencingFromCertificatesCount(x509CertStoreEntity);
        }

        return result;
    }

    @Override
    public List<UserPreferences> getReferencingFromNamedCertificates(@Nonnull X509Certificate certificate,
            Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        List<UserPreferences> result = new LinkedList<>();

        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        // We can only check if the X509CertStoreEntry is a Hibernate entity.
        if (certificateEntry instanceof X509CertStoreEntity x509CertStoreEntity)
        {
            List<UserPreferencesEntity> entities = createDAO().getReferencingFromNamedCertificates(
                    x509CertStoreEntity, firstResult, maxResults);

            if (entities != null)
            {
                for (UserPreferencesEntity entity : entities) {
                    result.add(createUserPreferences(entity));
                }
            }
        }

        return result;
    }

    @Override
    public long getReferencingFromNamedCertificatesCount(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        long result = 0;

        // We can only check if the X509CertStoreEntry is a Hibernate entity.
        if (certificateEntry instanceof X509CertStoreEntity x509CertStoreEntity)  {
            result = createDAO().referencingFromNamedCertificatesCount(x509CertStoreEntity);
        }

        return result;
    }

    @Override
    public List<UserPreferences> getReferencingFromKeyAndCertificate(@Nonnull X509Certificate certificate,
            Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        List<UserPreferences> result = new LinkedList<>();

        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        // We can only check if the X509CertStoreEntry is a Hibernate entity.
        if (certificateEntry instanceof X509CertStoreEntity x509CertStoreEntity)
        {
            List<UserPreferencesEntity> entities = createDAO().getReferencingFromKeyAndCertificate(
                    x509CertStoreEntity, firstResult, maxResults);

            if (entities != null)
            {
                for (UserPreferencesEntity entity : entities) {
                    result.add(createUserPreferences(entity));
                }
            }
        }

        return result;
    }

    @Override
    public long getReferencingFromKeyAndCertificateCount(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        X509CertStoreEntry certificateEntry = keyAndCertStore.getByCertificate(certificate);

        long result = 0;

        // We can only check if the X509CertStoreEntry is a Hibernate entity.
        if (certificateEntry instanceof X509CertStoreEntity x509CertStoreEntity) {
            result = createDAO().referencingFromKeyAndCertificateCount(x509CertStoreEntity);
        }

        return result;
    }

    private UserPreferencesDAO createDAO() {
        return UserPreferencesDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    @Override
    public long getReferencingFromNamedBlobsCount(@Nonnull NamedBlob namedBlob)
    {
        // We only support NamedBlobEntity's
        if (!(namedBlob instanceof NamedBlobEntity namedBlobEntity)) {
            throw new IllegalArgumentException("namedBlob is not a NamedBlobEntity");
        }

        return createDAO().referencingFromNamedBlobsCount(namedBlobEntity);
    }

    @Override
    public List<UserPreferences> getReferencingFromNamedBlobs(@Nonnull NamedBlob namedBlob,
            Integer firstResult, Integer maxResults)
    {
        List<UserPreferences> result = new LinkedList<>();

        // We only support NamedBlobEntity's
        if (!(namedBlob instanceof NamedBlobEntity namedBlobEntity)) {
            throw new IllegalArgumentException("namedBlob is not a NamedBlobEntity");
        }

        List<UserPreferencesEntity> entities = createDAO().getReferencingFromNamedBlobs(
                namedBlobEntity, firstResult, maxResults);

        if (entities != null)
        {
            for (UserPreferencesEntity entity : entities) {
                result.add(createUserPreferences(entity));
            }
        }

        return result;
    }
}
