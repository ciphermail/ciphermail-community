/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.jackson.JacksonUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Attribute;
import org.apache.mailet.AttributeName;
import org.apache.mailet.AttributeValue;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Mailet that can be used to delete and add mail attributes.
 */
@SuppressWarnings({"java:S6813"})
public class MailAttributes extends AbstractTransactedMailet
{
    private static final Logger logger = LoggerFactory.getLogger(MailAttributes.class);

    // Pattern for detecting user variable references  like #{some.var}
    private static final Pattern USER_VAR_PATTERN = Pattern.compile("#\\{\\s*(.*?)\\s*}");

    // the parameter types supported by set/add
    private enum AttributeValueType {
        BOOLEAN,
        STRING,
        BYTES,
        INT,
        LONG,
        FLOAT,
        DATE,
        DOUBLE
    }

    // Attributes to set/add should have a type and name
    private static class NameTypeAndValue
    {
        NameTypeAndValue() {
            // required for JSON deserialization
        }

        static NameTypeAndValue of(String name, AttributeValueType type, String value)
        {
            Objects.requireNonNull(name);
            Objects.requireNonNull(type);
            Objects.requireNonNull(value);

            NameTypeAndValue nameTypeAndValue = new NameTypeAndValue();

            nameTypeAndValue.name = name;
            nameTypeAndValue.type = type;
            nameTypeAndValue.value = value;

            return nameTypeAndValue;
        }

        @JsonProperty("name")
        private String name;

        @JsonProperty("type")
        private AttributeValueType type;

        @JsonProperty("value")
        private String value;
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        ADD        ("add"),
        DELETE     ("delete"),
        PROCESSOR  ("processor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    private enum SpecialVariable
    {
        ORIGINATOR  ("originator"),
        REPLY_TO    ("replyto"),
        SENDER      ("sender"),
        FROM        ("from"),
        RECIPIENTS  ("recipients"),
        SUBJECT     ("subject"),
        MESSAGE_ID  ("messageid");

        // Note: make sure friendlyName is lowercase
        private final String friendlyName;

        // RegEx which matches ${VAR}
        private static final Pattern PATTERN = Pattern.compile("\\$\\{([^}]+)}");

        private static final Map<String, SpecialVariable> friendlyNameToEnum;

        static
        {
            friendlyNameToEnum = new HashMap<>();

            for (SpecialVariable specialVariable : SpecialVariable.values()) {
                friendlyNameToEnum.put(specialVariable.friendlyName, specialVariable);
            }
        }

        SpecialVariable(String name) {
            this.friendlyName = name;
        }

        static SpecialVariable fromFriendlyName(String friendlyName)
        {
            SpecialVariable result = null;

            friendlyName = StringUtils.trimToNull(friendlyName);

            if (friendlyName != null)
            {
                Matcher matcher = PATTERN.matcher(friendlyName);

                if (matcher.matches()) {
                    result = friendlyNameToEnum.get(StringUtils.lowerCase(StringUtils.trimToEmpty(matcher.group(1))));
                }
            }

            return result;
        }
    }

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * The attributes to delete
     */
    private List<String> deleteAttributes;

    /*
     * The attributes to add
     */
    private List<NameTypeAndValue> addAttributes;

    /*
     * The next processor (can be null in which case the processor is not changed)
     */
    private String nextProcessor;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    private void initDeleteAttributes()
    {
        deleteAttributes = getInitParameterAsOptional(Parameter.DELETE.name).map(json ->
        {
            try {
                return JacksonUtil.getObjectMapper().readValue(json, new TypeReference<List<String>>(){});
            }
            catch (JsonProcessingException e) {
                throw new UnhandledException(e);
            }
        }).orElse(Collections.emptyList());
    }

    private void initAddAttributes()
    {
        addAttributes = getInitParameterAsOptional(Parameter.ADD.name).map(json ->
        {
            try {
                return JacksonUtil.getObjectMapper().readValue(json,
                        new TypeReference<List<NameTypeAndValue>>(){});
            }
            catch (JsonProcessingException e) {
                throw new UnhandledException(e);
            }
        }).orElse(Collections.emptyList());
    }

    private void initAttributes()
    {
        initDeleteAttributes();
        initAddAttributes();

        nextProcessor = getInitParameter(Parameter.PROCESSOR.name);
    }

    @Override
    protected void initMailetTransacted()
    throws MessagingException
    {
        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);

        initAttributes();
    }

    private static void copyMailAddresses(Address address, Collection<String> target)
    {
        if (address != null) {
            target.add(address.toString());
        }
    }

    private static void copyMailAddresses(List<InternetAddress> source, Collection<String> target)
    {
        if (source != null)
        {
            for (InternetAddress address : source)
            {
                if (address != null) {
                    target.add(address.toString());
                }
            }
        }
    }

    private static void copyMailAddresses(Collection<MailAddress> source, Collection<String> target)
    {
        if (source != null)
        {
            for (MailAddress address : source)
            {
                if (address != null) {
                    target.add(address.toString());
                }
            }
        }
    }

    private void addValueNullSafe(String value, List<String> result)
    {
        if (value != null) {
            result.add(value);
        }
    }

    private void handleSpecialVariable(Mail mail, SpecialVariable specialVariable, List<String> result)
    throws MessagingException
    {
        switch (specialVariable)
        {
            case ORIGINATOR -> copyMailAddresses(messageOriginatorIdentifier.getOriginator(mail), result);
            case REPLY_TO -> copyMailAddresses(EmailAddressUtils.getReplyToNonStrict(mail.getMessage()), result);
            case SENDER -> copyMailAddresses(MailAddressUtils.toInternetAddress(mail.getMaybeSender()), result);
            case FROM -> copyMailAddresses(EmailAddressUtils.getFromNonStrict(mail.getMessage()), result);
            case RECIPIENTS -> copyMailAddresses(MailAddressUtils.getRecipients(mail), result);
            case SUBJECT -> addValueNullSafe(MailUtils.getSafeSubject(mail.getMessage()), result);
            case MESSAGE_ID -> addValueNullSafe(MailUtils.getMessageIDQuietly(mail.getMessage()), result);
            default -> throw new MessagingException("Unsupported SpecialVariable.");
        }
    }

    private void handleUserVariable(Mail mail, String userPropertyName, List<String> result)
    throws MessagingException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        try {
            User user = userWorkflow.getUser(originator.getAddress(),
                    UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

            String value = user.getUserPreferences().getProperties().getProperty(userPropertyName);

            value = StringUtils.trimToNull(value);

            if (value != null) {
                result.add(value);
            }
        }
        catch(HierarchicalPropertiesException e) {
            getLogger().error("Error getting user property " + userPropertyName, e);
        }
    }

    // Checks whether the attribute value is a special variable or a user property reference. If so, resolve the value
    private List<String> parseRawAttribute(String attributeValue, Mail mail)
    throws MessagingException
    {
        attributeValue = StringUtils.trimToNull(attributeValue);

        List<String> result = new LinkedList<>();

        if (attributeValue != null)
        {
            // Check if the input is a special variable
            SpecialVariable specialVariable = SpecialVariable.fromFriendlyName(attributeValue);

            if (specialVariable != null) {
                handleSpecialVariable(mail, specialVariable, result);
            }
            else {
                // Check if the input is a user property
                Matcher matcher = USER_VAR_PATTERN.matcher(attributeValue);

                if (matcher.matches()) {
                    handleUserVariable(mail, matcher.group(1), result);
                }
                else {
                    // variable is not a special address
                    result.add(attributeValue);
                }
            }
        }

        return result;
    }

    private static AttributeValue<?> toAttributeValue(String stringValue, AttributeValueType type)
    {
        return switch (type)
        {
            case BOOLEAN -> AttributeValue.of(Boolean.valueOf(stringValue));
            case STRING -> AttributeValue.of(stringValue);
            case BYTES -> AttributeValue.of(Base64.getDecoder().decode(stringValue));
            case INT -> AttributeValue.of(Integer.valueOf(stringValue));
            case LONG -> AttributeValue.of(Long.valueOf(stringValue));
            case FLOAT -> AttributeValue.of(Float.valueOf(stringValue));
            case DATE -> AttributeValue.of(ZonedDateTime.parse(stringValue, DateTimeFormatter.ISO_ZONED_DATE_TIME));
            case DOUBLE -> AttributeValue.of(Double.valueOf(stringValue));
            default -> throw new IllegalArgumentException("type " + type + " is not supported");
        };
    }

    private void deleteAttributes(Mail mail)
    {
        for (String attribute : deleteAttributes) {
            mail.removeAttribute(AttributeName.of(attribute));
        }
    }

    private static void addMailAttribute(Mail mail, NameTypeAndValue nameTypeAndValue)
    {
        // check if there is already an existing mail attribute
        Optional<Attribute> mailAttribute = mail.getAttribute(AttributeName.of(nameTypeAndValue.name));

        if (mailAttribute.isPresent())
        {
            // mail attribute already exists
            AttributeValue<?> existingAttributeValue = mailAttribute.get().getValue();

            List<AttributeValue<?>> newAttributeValue = new LinkedList<>();

            // add existing values if the previous value was already a collection
            if (existingAttributeValue.value() instanceof Collection<?> collectionOfValues)
            {
                for (Object value : collectionOfValues) {
                    newAttributeValue.add((AttributeValue<?>) value);
                }
            }
            else {
                newAttributeValue.add(existingAttributeValue);
            }

            newAttributeValue.add(toAttributeValue(nameTypeAndValue.value,
                    nameTypeAndValue.type));

            mail.setAttribute(new Attribute(AttributeName.of(nameTypeAndValue.name), AttributeValue.of(newAttributeValue)));
        }
        else {
            // mail attribute does not exist so add it as a single value
            mail.setAttribute(new Attribute(AttributeName.of(nameTypeAndValue.name), toAttributeValue(nameTypeAndValue.value,
                    nameTypeAndValue.type)));
        }
    }

    // If an attribute already exists and is a Collection, the new value will be added to the collection.
    // If not, a single value will be added
    private void addAttribute(Mail mail, NameTypeAndValue nameTypeAndValue)
    throws MessagingException
    {
        List<String> rawAttributeValues = parseRawAttribute(nameTypeAndValue.value, mail);

        if (!rawAttributeValues.isEmpty())
        {
            // if there are multiple values, add them as a list, otherwise as a single value
            if (rawAttributeValues.size() == 1)
            {
                addMailAttribute(mail, NameTypeAndValue.of(nameTypeAndValue.name, nameTypeAndValue.type,
                        rawAttributeValues.get(0)));
            }
            else {
                for (String rawAttributeValue : rawAttributeValues)
                {
                    addMailAttribute(mail, NameTypeAndValue.of(nameTypeAndValue.name,
                            nameTypeAndValue.type, rawAttributeValue));
                }
            }
        }
    }

    private void addAttributes(Mail mail)
    throws MessagingException
    {
        for (NameTypeAndValue nameTypeAndValue : addAttributes) {
            addAttribute(mail, nameTypeAndValue);
        }
    }

    @Override
    protected void serviceMailTransacted(Mail mail)
    throws MessagingException
    {
        // First delete attributes, then set and finally add
        deleteAttributes(mail);
        addAttributes(mail);

        if (nextProcessor != null) {
            mail.setState(nextProcessor);
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (nextProcessor != null) {
            requiredProcessors.add(new ProcessingState(nextProcessor));
        }

        return requiredProcessors;
    }
}
