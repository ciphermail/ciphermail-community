/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.admin;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Admin manager implementation which uses Hibernate to store Admin's.
 */
public class AdminManagerImpl implements AdminManager
{
    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;

    public AdminManagerImpl(@Nonnull SessionManager sessionManager) {
        this.sessionManager = Objects.requireNonNull(sessionManager);
    }

    @Override
    public @Nonnull Admin createAdmin(@Nonnull String name) {
        return new Admin(name);
    }

    @Override
    public @Nonnull Admin persistAdmin(@Nonnull Admin admin) {
        return getDAO().persist(admin);
    }

    @Override
    public Admin getAdmin(@Nonnull String name) {
        return getDAO().getAdmin(name);
    }

    @Override
    public boolean deleteAdmin(@Nonnull String name) {
        return getDAO().deleteAdmin(name);
    }

    @Override
    public void deleteAll() {
        getDAO().deleteAll();
    }

    @Override
    public long getAdminCount() {
        return getDAO().getAdminCount();
    }

    @Override
    public @Nonnull List<Admin> getAdmins(Integer firstResult, Integer maxResults, SortDirection sortDirection) {
        return getDAO().getAdmins(firstResult, maxResults, sortDirection);
    }

    private void addPermissions(Role role, Set<String> permissions, Set<Role> handled)
    {
        // stop if we already handled the role
        // this can happen if we have some cycle in the role inheritance (a -> b -> c -> a)
        if (handled.contains(role)) {
            return;
        }

        handled.add(role);

        permissions.addAll(role.getPermissions());
        role.getRoles().forEach(childRole -> addPermissions(childRole, permissions, handled));
    }

    @Override
    public Set<String> getEffectivePermissions(@Nonnull String name)
    {
        Admin admin = getAdmin(name);

        Set<String> permissions = new HashSet<>();
        Set<Role> handled = new HashSet<>();

        if (admin != null)  {
            admin.getRoles().forEach(role -> addPermissions(role, permissions, handled));
        }

        return permissions;
    }

    private AdminDAO getDAO() {
        return AdminDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }
}
