/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.ciphermail.core.common.jackson.JacksonUtil;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.mailet.Attribute;
import org.apache.mailet.AttributeName;
import org.apache.mailet.AttributeValue;
import org.apache.mailet.Mail;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class MailAttributesUtils
{
    private MailAttributesUtils() {
        // Empty on purpose
    }

    /**
     * Returns an instance of clazz by deserializing the JSON attribute value. If the attribute does not exist or
     * is not a string, an empty Optional will be returned.
     *
     * @param mail The Mail from which the attribute should be read
     * @param attributeName The name of the attribute
     * @param clazz The class of the type to instantiate and deserialize
     * @return The optional object
     * @throws UnhandledException when the string value is not valid JSON or when the object cannot be instantiated.
     */
    public static <T> Optional<T> deserializeObjectFromJSON(@Nonnull Mail mail, @Nonnull String attributeName,
            @Nonnull Class<T> clazz)
    {
        return mail.getAttribute(AttributeName.of(attributeName))
                .map(Attribute::getValue)
                .flatMap(v -> v.valueAs(String.class))
                .map(j ->
                {
                    try {
                        return JacksonUtil.getObjectMapper().readValue(j, clazz);
                    }
                    catch (JsonProcessingException e) {
                        throw new UnhandledException(e);
                    }
                });
    }

    /**
     * Returns an instance of clazz by deserializing the JSON attribute value. If the attribute does not exist or
     * is not a string, an empty Optional will be returned. Use this method if you want to deserialize a generic class.
     *
     * @param mail The Mail from which the attribute should be read
     * @param attributeName The name of the attribute
     * @param typeReference Type reference of the class to deserialize
     * @return The optional object
     * @throws UnhandledException when the string value is not valid JSON or when the object cannot be instantiated.
     */
    public static <T> Optional<T> deserializeObjectFromJSON(@Nonnull Mail mail, @Nonnull String attributeName,
            @Nonnull TypeReference<T> typeReference)
    {
        return mail.getAttribute(AttributeName.of(attributeName))
                .map(Attribute::getValue)
                .flatMap(v -> v.valueAs(String.class))
                .map(j ->
                {
                    try {
                        return JacksonUtil.getObjectMapper().readValue(j, typeReference);
                    }
                    catch (JsonProcessingException e) {
                        throw new UnhandledException(e);
                    }
                });
    }

    /**
     * Stores the value as a JSON encoded string under the attribute name in the Mail object
     * @param mail The Mail instance
     * @param attributeName The name of the attribute
     * @param value The instance to serialize. If null, the attribute will be removed
     * @throws UnhandledException when the object cannot be serialized to a JSON string.
     */
    public static <T> void serializeObjectToJSON(@Nonnull Mail mail, @Nonnull String attributeName, T value)
    {
        try {
            if (value != null)
            {
                mail.setAttribute(new Attribute(AttributeName.of(attributeName), AttributeValue.of(
                        JacksonUtil.getObjectMapper().writeValueAsString(value))));
            }
            else {
                mail.removeAttribute(AttributeName.of(attributeName));
            }
        }
        catch (JsonProcessingException e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * Returns the value of a named attribute. Only values supported by {@link AttributeValue} are supported.
     * @param mail The Mail instance
     * @param attributeName The name of the attribute
     * @param clazz The type of object to return
     * @return The optional object
     */
    public static <T> Optional<T> getManagedAttributeValue(@Nonnull Mail mail, @Nonnull String attributeName,
            @Nonnull Class<T> clazz)
    {
        return mail.getAttribute(AttributeName.of(attributeName))
                .map(Attribute::getValue)
                .flatMap(v -> v.valueAs(clazz));
    }

    /**
     * Returns a list with the values of the input which are instances of AttributeValue
     * @param values
     * @return
     */
    public static @Nonnull List<AttributeValue<?>> toAttributeValueList(@Nonnull Collection<?> values)
    {
        List<AttributeValue<?>> attributeValues = new LinkedList<>();

        for (Object value : values)
        {
            if (value instanceof AttributeValue<?>) {
                attributeValues.add((AttributeValue<?>) value);
            }
        }

        return attributeValues;
    }

    public static @Nonnull <T extends Collection>  List<AttributeValue<?>> toAttributeValueList(@Nonnull Optional<T> values)
    {
        List<AttributeValue<?>> attributeValues = new LinkedList<>();

        if (values.isPresent())
        {
            for (Object value : values.get())
            {
                if (value instanceof AttributeValue<?>) {
                    attributeValues.add((AttributeValue<?>) value);
                }
            }
        }

        return attributeValues;
    }

    /**
     * Sets the attribute. Only objects supported by AttributeValue are allowed.
     * @param mail
     * @param attributeName
     * @param value
     */
    public static void setManagedAttributeValue(@Nonnull Mail mail, @Nonnull String attributeName, Object value)
    {
        if (value != null) {
            mail.setAttribute(Attribute.convertToAttribute(attributeName, value));
        }
        else {
            mail.removeAttribute(AttributeName.of(attributeName));
        }
    }

    /**
     * Returns the attribute value as a List of {@link AttributeValue}'s. If the attribute value is not already
     * a collection, a single valued collection will be returned. If there is no attribute with the provided name,
     * an empty collection will be returned. This helper method can be used for attributes which might be a single
     * value of which contain multiple values.
     *
     * @param mail
     * @param attributeName
     * @return
     */
    public static List<AttributeValue<?>> getAttributeValueAsList(@Nonnull Mail mail, @Nonnull String attributeName)
    {
        List<AttributeValue<?>> result;

        Optional<AttributeValue<?>> value = mail.getAttribute(AttributeName.of(attributeName)).map(Attribute::getValue);

        if (value.isPresent())
        {
            // check if the attribute value is already a collection
            @SuppressWarnings("unchecked")
            Optional<Collection<AttributeValue<?>>> values = value.map(AttributeValue::value)
                    .filter(Collection.class::isInstance)
                    .map(Collection.class::cast);

            result = values.isPresent() ? new LinkedList<>(values.get()) : List.of(value.get());
        }
        else {
            result = Collections.emptyList();
        }

        return result;
    }

    /**
     * Returns the attribute value as a List of strings. If the attribute value is not already
     * a collection, a single valued collection will be returned. If there is no attribute with the provided name,
     * an empty collection will be returned. This helper method can be used for attributes which might be a single
     * value of which contain multiple values. If an attribute value is not a string, it will be converted to a
     * string using {@link #attributeValueAsString(AttributeValue, String)}
     *
     * @param mail
     * @param attributeName
     * @return List of strings
     */
    public static List<String> getAttributeValueAsStringList(@Nonnull Mail mail, @Nonnull String attributeName)
    {
        List<String> result = new LinkedList<>();

        Optional<AttributeValue<?>> optionalValue = mail.getAttribute(
                AttributeName.of(attributeName)).map(Attribute::getValue);

        if (optionalValue.isPresent())
        {
            // check if the attribute value is already a collection
            @SuppressWarnings("unchecked")
            Optional<Collection<AttributeValue<?>>> values = optionalValue.map(AttributeValue::value)
                    .filter(Collection.class::isInstance)
                    .map(Collection.class::cast);

            if (values.isPresent())
            {
                // the attribute value is a collection
                for (AttributeValue<?> attributeValue : values.get())
                {
                    String stringValue = attributeValueAsString(attributeValue, null);

                    if (stringValue != null) {
                        result.add(stringValue);
                    }
                }
            }
            else {
                // the attribute value is a single vallue
                String stringValue = attributeValueAsString(optionalValue.get(), null);

                if (stringValue != null) {
                    result.add(stringValue);
                }
            }
        }

        return result;
    }

    /**
     * Returns the string attribute as a Boolean. If the attribute value is String, the value will be converted to
     * a boolean value using
     */
    public static Boolean attributeAsBoolean(@Nonnull Mail mail, String attributeName, Boolean defaultValue)
    {
        if (StringUtils.isNotEmpty(attributeName))
        {
            Optional<Attribute> attribute = mail.getAttribute(AttributeName.of(attributeName));

            if (attribute.isPresent())
            {
                AttributeValue<?> attributeValue = attribute.get().getValue();

                // try boolean first
                Optional<Boolean> booleanOptional = attributeValue.valueAs(Boolean.class);

                if (booleanOptional.isPresent()) {
                    return booleanOptional.get();
                }

                // try string next
                Optional<String> stringOptional = attributeValue.valueAs(String.class);

                if (stringOptional.isPresent())
                {
                    // try to convert string to boolean
                    Boolean booleanValue = BooleanUtils.toBooleanObject(stringOptional.get());

                    if (booleanValue != null) {
                        return booleanValue;
                    }
                }
            }
        }

        return defaultValue;
    }

    /**
     * Returns the attribute value as a String. Only String, Boolean, Integer, Long, Float and Double are supported.
     * If the value cannot be converted to a string, the default value will be returned.
     */
    public static String attributeValueAsString(@Nonnull AttributeValue<?> attributeValue, String defaultValue)
    {
        Object value = attributeValue.value();

        if (value instanceof String castValue) {
            return castValue;
        }
        else if (value instanceof Boolean castValue) {
            return castValue.toString();
        }
        else if (value instanceof Integer castValue) {
            return castValue.toString();
        }
        else if (value instanceof Long castValue) {
            return castValue.toString();
        }
        else if (value instanceof Float castValue) {
            return castValue.toString();
        }
        else if (value instanceof Double castValue) {
            return castValue.toString();
        }

        return defaultValue;
    }

    /**
     * Returns the attribute value as a String. The attribute value is converted using the method attributeValueAsString
     * If an attribute with the given name does not exist or the value cannot be converted to a string, null will be
     * returned.
     */
    public static String attributeAsString(@Nonnull Mail mail, String attributeName)  {
        return attributeAsString(mail, attributeName, null);
    }

    /**
     * Returns the attribute value as a String. The attribute value is converted using the method attributeValueAsString
     * If an attribute with the given name does not exist or the value cannot be converted to a string, the default
     * value will be returned.
     */
    public static String attributeAsString(@Nonnull Mail mail, String attributeName, String defaultValue)
    {
        if (StringUtils.isNotEmpty(attributeName))
        {
            Optional<Attribute> attribute = mail.getAttribute(AttributeName.of(attributeName));

            if (attribute.isPresent()) {
                return attributeValueAsString(attribute.get().getValue(), defaultValue);
            }
        }

        return defaultValue;
    }
}
