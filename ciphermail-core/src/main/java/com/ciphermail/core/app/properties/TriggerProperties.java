/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

import javax.annotation.Nonnull;

/**
 * User properties for header and subject triggers
 */
public interface TriggerProperties
{
    /**
     * The trigger that can trigger signing the message when a header
     * matches the trigger
     */
    void setSignHeaderTrigger(String trigger)
    throws HierarchicalPropertiesException;

    String getSignHeaderTrigger()
    throws HierarchicalPropertiesException;

    /**
     * If true a header can force signing of a message (see ForceSigningHeaderTrigger)
     */
    void setSignHeaderTriggerEnabled(Boolean allowed)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSignHeaderTriggerEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The trigger that can trigger encrypting the message when a header
     * matches the trigger
     */
    void setEncryptHeaderTrigger(String trigger)
    throws HierarchicalPropertiesException;

    String getEncryptHeaderTrigger()
    throws HierarchicalPropertiesException;

    /**
     * If true a header can force encrypt of a message (see ForceEncryptHeaderTrigger)
     */
    void setEncryptHeaderTriggerEnabled(Boolean allowed)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getEncryptHeaderTriggerEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The encryption subject trigger
     */
    void setSubjectTriggerRegEx(String trigger)
    throws HierarchicalPropertiesException;

    String getSubjectTriggerRegEx()
    throws HierarchicalPropertiesException;

    /**
     * If the encryption subject trigger is enabled
     */
    void setSubjectTriggerRegExEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSubjectTriggerRegExEnabled()
    throws HierarchicalPropertiesException;

    /**
     * If the matching encryption subject pattern should be removed from the subject
     */
    void setSubjectTriggerRegExRemovePattern(Boolean removePattern)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSubjectTriggerRegExRemovePattern()
    throws HierarchicalPropertiesException;

    /**
     * The signing subject trigger
     */
    void setSignSubjectTriggerRegEx(String trigger)
    throws HierarchicalPropertiesException;

    String getSignSubjectTriggerRegEx()
    throws HierarchicalPropertiesException;

    /**
     * If the signing subject trigger is enabled
     */
    void setSignSubjectTriggerRegExEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSignSubjectTriggerRegExEnabled()
    throws HierarchicalPropertiesException;

    /**
     * If the matching signing subject pattern should be removed from the subject
     */
    void setSignSubjectTriggerRegExRemovePattern(Boolean removePattern)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSignSubjectTriggerRegExRemovePattern()
    throws HierarchicalPropertiesException;
}
