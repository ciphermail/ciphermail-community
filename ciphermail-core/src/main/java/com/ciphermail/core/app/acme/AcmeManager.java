/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.core.app.acme;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.certificate.TLSKeyPairBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.exception.AcmeException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.util.List;
import java.util.Optional;

/**
 * The AcmeManager interface provides methods for managing ACME (Automated Certificate Management Environment) operations,
 * such as setting account key pair, creating account key pair, ordering certificates, etc.
 */
public interface AcmeManager
{
    void setAccountKeyPair(KeyPair accountKeyPair)
    throws HierarchicalPropertiesException, IOException;

    KeyPair getAccountKeyPair()
    throws HierarchicalPropertiesException, IOException;

    void createAccountKeyPair(@Nonnull TLSKeyPairBuilder.Algorithm algorithm)
    throws GeneralSecurityException, HierarchicalPropertiesException, IOException;

    Optional<URI> getTermsOfService()
    throws HierarchicalPropertiesException, AcmeException;

    void setTosAccepted(boolean accepted)
    throws HierarchicalPropertiesException;

    boolean isTosAccepted()
    throws HierarchicalPropertiesException;

    @Nonnull Account findOrRegisterAccount()
    throws HierarchicalPropertiesException, IOException, AcmeException;

    void deactivateAccount()
    throws HierarchicalPropertiesException, IOException, AcmeException;

    void orderCertificate(@Nonnull TLSKeyPairBuilder.Algorithm algorithm, @Nonnull List<String> domains)
    throws HierarchicalPropertiesException, AcmeException, IOException;

    void renewCertificate(boolean forceRenewal)
    throws AcmeException, IOException;

    AcmeTokenCache.Challenge getChallenge(@Nonnull String token)
    throws HierarchicalPropertiesException, JsonProcessingException;
}

