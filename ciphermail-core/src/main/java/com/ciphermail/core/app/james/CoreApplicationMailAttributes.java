/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.ciphermail.core.common.dlp.PolicyViolation;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.lang.UnhandledException;
import org.apache.mailet.Mail;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class CoreApplicationMailAttributes
{
    private static final String BASE_ATTRIBUTE                           = "ciphermail-";
    private static final String CERTIFICATES_ATTRIBUTE                   = BASE_ATTRIBUTE + "certificates";
    private static final String PASSWORDS_ATTRIBUTE                      = BASE_ATTRIBUTE + "encrypted-passwords";
    private static final String PHONE_NUMBERS_ATTRIBUTE                  = BASE_ATTRIBUTE + "phoneNumbers";
    private static final String MESSAGE_ATTRIBUTE                        = BASE_ATTRIBUTE + "message";
    private static final String MAIL_ID                                  = BASE_ATTRIBUTE + "mail-id";
    private static final String ORIGINAL_SUBJECT                         = BASE_ATTRIBUTE + "original-subject";
    private static final String POLICY_VIOLATIONS                        = BASE_ATTRIBUTE + "policy-violations";
    private static final String POLICY_VIOLATION_ERROR_MESSAGE           = BASE_ATTRIBUTE + "policy-violation-error-message";
    private static final String DLP_MAIN_POLICY_VIOLATION                = BASE_ATTRIBUTE + "dlp-main-policy-violation";
    private static final String DLP_MAIN_POLICY_VIOLATION_PRIORITY_VALUE = BASE_ATTRIBUTE + "dlp-main-policy-violation-priority-value";
    private static final String MAIL_REPOSITORY_ID                       = BASE_ATTRIBUTE + "mail-repository-id";
    private static final String PORTAL_SIGNUPS                           = BASE_ATTRIBUTE + "portal-signups";
    private static final String PROTECTED_HEADERS                        = BASE_ATTRIBUTE + "protected-headers";
    private static final String SECURITY_INFO_TAGS                       = BASE_ATTRIBUTE + "security-info-tags";
    private static final String PGP_PUBLIC_KEYS                          = BASE_ATTRIBUTE + "pgp-public-keys";

    private CoreApplicationMailAttributes() {
        // empty on purpose
    }

    public static Certificates getCertificates(@Nonnull Mail mail)
    {
        Objects.requireNonNull(mail);

        return MailAttributesUtils.getManagedAttributeValue(mail, CERTIFICATES_ATTRIBUTE, String.class)
            .map(j ->
            {
                try {
                    return Certificates.fromJSON(j);
                }
                catch (IOException | CertificateException e) {
                    throw new UnhandledException(e);
                }
            }).orElse(null);
    }

    public static void setCertificates(@Nonnull Mail mail, @Nonnull Certificates certificates)
    {
        Objects.requireNonNull(mail);
        Objects.requireNonNull(certificates);

        try {
            MailAttributesUtils.setManagedAttributeValue(mail, CERTIFICATES_ATTRIBUTE, Certificates.toJSON(certificates));
        }
        catch (IOException | CertificateEncodingException e) {
            throw new UnhandledException(e);
        }
    }

    public static Passwords getPasswords(@Nonnull Mail mail) {
        return MailAttributesUtils.deserializeObjectFromJSON(mail, PASSWORDS_ATTRIBUTE, Passwords.class).orElse(null);
    }

    public static void setPasswords(@Nonnull Mail mail, Passwords passwords) {
        MailAttributesUtils.serializeObjectToJSON(mail, PASSWORDS_ATTRIBUTE, passwords);
    }

    public static PhoneNumbers getPhoneNumbers(@Nonnull Mail mail) {
        return MailAttributesUtils.deserializeObjectFromJSON(mail, PHONE_NUMBERS_ATTRIBUTE, PhoneNumbers.class).orElse(null);
    }

    public static void setPhoneNumbers(@Nonnull Mail mail, PhoneNumbers phoneNumbers) {
        MailAttributesUtils.serializeObjectToJSON(mail, PHONE_NUMBERS_ATTRIBUTE, phoneNumbers);
    }

    public static String getMessage(@Nonnull Mail mail) {
        return MailAttributesUtils.getManagedAttributeValue(mail, MESSAGE_ATTRIBUTE, String.class).orElse(null);
    }

    public static void setMessage(@Nonnull Mail mail, String message) {
        MailAttributesUtils.setManagedAttributeValue(mail, MESSAGE_ATTRIBUTE, message);
    }

    public static String getMailID(@Nonnull Mail mail) {
        return MailAttributesUtils.getManagedAttributeValue(mail, MAIL_ID, String.class).orElse(null);
    }

    public static void setMailID(@Nonnull Mail mail, String id) {
        MailAttributesUtils.setManagedAttributeValue(mail, MAIL_ID, id);
    }

    public static String getOriginalSubject(@Nonnull Mail mail) {
        return MailAttributesUtils.getManagedAttributeValue(mail, ORIGINAL_SUBJECT, String.class).orElse(null);
    }

    public static void setOriginalSubject(@Nonnull Mail mail, String subject) {
        MailAttributesUtils.setManagedAttributeValue(mail, ORIGINAL_SUBJECT, subject);
    }

    public static List<PolicyViolation> getPolicyViolations(@Nonnull Mail mail)
    {
        return MailAttributesUtils.deserializeObjectFromJSON(mail, POLICY_VIOLATIONS,
                new TypeReference<List<PolicyViolation>>(){}).orElse(null);
    }

    public static void setPolicyViolations(@Nonnull Mail mail, Collection<PolicyViolation> violations) {
        MailAttributesUtils.serializeObjectToJSON(mail, POLICY_VIOLATIONS, violations);
    }

    public static String getPolicyViolationErrorMessage(@Nonnull Mail mail)
    {
        return MailAttributesUtils.getManagedAttributeValue(mail, POLICY_VIOLATION_ERROR_MESSAGE,
                String.class).orElse(null);
    }

    public static void setPolicyViolationErrorMessage(@Nonnull Mail mail, String message) {
        MailAttributesUtils.setManagedAttributeValue(mail, POLICY_VIOLATION_ERROR_MESSAGE, message);
    }

    public static void setMainPolicyViolation(@Nonnull Mail mail, PolicyViolation policyViolation)
    {
        MailAttributesUtils.serializeObjectToJSON(mail, DLP_MAIN_POLICY_VIOLATION, policyViolation);

        // Set the priority value as a separate attribute. This makes it easier to match on priority
        MailAttributesUtils.serializeObjectToJSON(mail, DLP_MAIN_POLICY_VIOLATION_PRIORITY_VALUE,
                policyViolation != null ? policyViolation.getPriority().getPriority() : null);
    }

    public static PolicyViolation getMainPolicyViolation(@Nonnull Mail mail)
    {
        return MailAttributesUtils.deserializeObjectFromJSON(mail, DLP_MAIN_POLICY_VIOLATION,
                PolicyViolation.class).orElse(null);
    }

    public static UUID getMailRepositoryID(@Nonnull Mail mail)
    {
        return MailAttributesUtils.getManagedAttributeValue(mail, MAIL_REPOSITORY_ID, String.class)
                .map(UUID::fromString).orElse(null);
    }

    public static void setMailRepositoryID(@Nonnull Mail mail, UUID id) {
        MailAttributesUtils.setManagedAttributeValue(mail, MAIL_REPOSITORY_ID, Objects.toString(id, null));
    }

    public static PortalSignups getPortalSignups(@Nonnull Mail mail) {
        return MailAttributesUtils.deserializeObjectFromJSON(mail, PORTAL_SIGNUPS, PortalSignups.class).orElse(null);
    }

    public static void setPortalSignups(@Nonnull Mail mail, PortalSignups portalSignups) {
        MailAttributesUtils.serializeObjectToJSON(mail, PORTAL_SIGNUPS, portalSignups);
    }

    public static List<String> getProtectedHeaders(@Nonnull Mail mail)
    {
        return MailAttributesUtils.deserializeObjectFromJSON(mail, PROTECTED_HEADERS,
                new TypeReference<List<String>>(){}).orElse(Collections.emptyList());
    }

    public static void setProtectedHeaders(@Nonnull Mail mail, Collection<String> protectedHeaders) {
        MailAttributesUtils.serializeObjectToJSON(mail, PROTECTED_HEADERS, protectedHeaders != null ?
                new ArrayList<>(protectedHeaders) : Collections.emptyList());
    }

    public static SecurityInfoTags getSecurityInfoTags(@Nonnull Mail mail) {
        return MailAttributesUtils.deserializeObjectFromJSON(mail, SECURITY_INFO_TAGS, SecurityInfoTags.class).orElse(null);
    }

    public static void setSecurityInfoTags(@Nonnull Mail mail, SecurityInfoTags tags) {
        MailAttributesUtils.serializeObjectToJSON(mail, SECURITY_INFO_TAGS, tags);
    }

    public static PGPPublicKeys getPGPPublicKeys(@Nonnull Mail mail)
    {
        return MailAttributesUtils.getManagedAttributeValue(mail, PGP_PUBLIC_KEYS, String.class)
            .map(j ->
            {
                try {
                    return PGPPublicKeys.fromJSON(j);
                }
                catch (IOException e) {
                    throw new UnhandledException(e);
                }
            }).orElse(null);
    }

    public static void setPGPPublicKeys(@Nonnull Mail mail, PGPPublicKeys publicKeys)
    {
        try {
            MailAttributesUtils.setManagedAttributeValue(mail, PGP_PUBLIC_KEYS,
                    publicKeys != null ? PGPPublicKeys.toJSON(publicKeys) : null);
        }
        catch (IOException e) {
            throw new UnhandledException(e);
        }
    }
}
