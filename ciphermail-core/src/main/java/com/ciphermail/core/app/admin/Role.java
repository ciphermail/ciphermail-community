/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.admin;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.UuidGenerator;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * Hibernate entity implementation of {@link Role}
 */
@Entity(name = Role.ENTITY_NAME)
@Table
public class Role
{
    static final String ENTITY_NAME = "Role";

    static final String NAME_COLUMN = "name";

    @Id
    @UuidGenerator
    private UUID id;

    @Column (name = NAME_COLUMN, length = 255, unique = true, nullable = false)
    private String name;

    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "permission")
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<String> permissions = new HashSet<>();

    /*
     * A role can inherit other roles
     */
    @ManyToMany(cascade = CascadeType.PERSIST)
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<Role> roles = new HashSet<>();

    protected Role() {
        // Required by Hibernate
    }

    public Role(@Nonnull String name) {
        this.name = Objects.requireNonNull(name);
    }

    public @Nonnull String getName() {
        return name;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * Authority is equals iff role is equal.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Role otherRole)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(name, otherRole.name)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(name)
            .toHashCode();
    }
}
