/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.common.util.NameValueUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.util.regex.Pattern;

/**
 * A regular expression matcher that matches the value of a header to the provided pattern.
 *
 * Usage:
 *
 * HeaderValueRegEx=matchOnError=false,subject=(?i)test | Matches when the subject contains test (case insensitive)
 *
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 *
 * @author Martijn Brinkers
 *
 */
public class HeaderValueRegEx extends AbstractHeaderValueRegEx
{
    private static final Logger logger = LoggerFactory.getLogger(HeaderValueRegEx.class);

    private String header;
    private Pattern headerValuePattern;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        String condition = StringUtils.trimToNull(getCondition());

        if (condition == null) {
            throw new IllegalArgumentException("Condition is missing.");
        }

        String[] pair = NameValueUtils.nameValueToPair(condition);

        if (pair == null || pair.length != 2) {
            throw new IllegalArgumentException("Condition is invalid. It should be 'name=value'");
        }

        header = pair[0];
        headerValuePattern = Pattern.compile(pair[1]);

        StrBuilder sb = new StrBuilder();

        sb.append("Header: ");
        sb.append(header);
        sb.appendSeparator("; ");
        sb.append("pattern: ");
        sb.append(headerValuePattern);

        getLogger().info("{}", sb);
    }

    @Override
    protected String getHeader(Mail mail)
    throws MessagingException
    {
        return header;
    }

    @Override
    protected Pattern getHeaderValuePattern(Mail mail) {
        return headerValuePattern;
    }
}