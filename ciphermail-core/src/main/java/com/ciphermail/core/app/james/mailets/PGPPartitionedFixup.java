/*
 * Copyright (c) 2015-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.MimeTypes;
import com.ciphermail.core.common.mail.MimeUtils;
import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.mail.PartScanner;
import com.ciphermail.core.common.mail.PartScanner.PartListener;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Mailet which fixes PGP partitioned encoded messages to look like normal messages
 *
 * PGP desktop (now Symantec encryption desktop) and PGP universal can create message with a PGP specific
 * format called partitioned.
 *
 */
public class PGPPartitionedFixup extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(PGPPartitionedFixup.class);

    /*
     * Filename of the HTML part
     */
    private static final String HTML_PARTITION_FILENAME = "PGPexch.htm";

    /*
     * PGP partitioning specific header
     */
    private static final String X_CONTENT_PGP_UNIVERSAL_SAVED_CONTENT_TYPE_HEADER =
            "X-Content-PGP-Universal-Saved-Content-Type";

    /*
     * PGP partitioning specific header
     */
    private static final String X_PGP_MIME_STRUCTURE = "X-PGP-MIME-Structure";

    /*
     * Max mime depth to search for parts
     */
    private int maxMimeDepth = 8;

    /*
     * If true the original Message-ID will be used for the handled message
     */
    private boolean retainMessageID = true;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        MAX_MIME_DEPTH ("maxMimeDepth");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    private static class HTMLPartitionPartListener implements PartListener
    {
        private Part partitionedHTMLPart;

        @Override
        public boolean onPart(Part parent, Part part, Object context)
        throws PartException
        {
            try {
                if (part.isMimeType(MimeTypes.OCTET_STREAM) &&
                        HTML_PARTITION_FILENAME.equals(MimeUtils.getFilenameQuietly(part)))
                {
                    this.partitionedHTMLPart = part;

                    // The part was found. Stop scanning
                    return false;
                }

                return true;
            }
            catch (MessagingException e) {
                throw new PartException(e);
            }
        }
    }

    private static class MultipartAlternativeScanner extends PartScanner
    {
        private Multipart multipart;

        MultipartAlternativeScanner(int maxDepth) {
            super(null, maxDepth);
        }

        @Override
        protected boolean scanMultipart(Part parent, Part part, int depth, Object context)
        throws IOException, PartException
        {
            try {
                if (part.isMimeType("multipart/alternative"))
                {
                    // Only accept this if the part contains two text parts
                    Multipart localMultipart = (Multipart) part.getContent();

                    if (localMultipart.getCount() == 2 &&
                        localMultipart.getBodyPart(0).isMimeType("text/plain") &&
                        localMultipart.getBodyPart(1).isMimeType("text/plain"))
                    {
                        this.multipart = localMultipart;

                        // The part was found. Stop scanning
                        return true;
                    }
                }

                return super.scanMultipart(parent, part, depth, context);
            }
            catch (MessagingException e) {
                throw new PartException(e);
            }
        }
    }

    @Override
    public void initMailet()
    {
        maxMimeDepth = getIntegerInitParameter(Parameter.MAX_MIME_DEPTH.name, maxMimeDepth);

        StrBuilder sb = new StrBuilder();

        sb.append("maxMimeDepth: ").append(maxMimeDepth);

        getLogger().info("{}", sb);
    }

    private Multipart findAlternativePart(MimeMessage message)
    throws MessagingException, IOException, PartException
    {
        MultipartAlternativeScanner partScanner = new  MultipartAlternativeScanner(maxMimeDepth);

        partScanner.scanPart(message);

        return partScanner.multipart;
    }

    /*
     * There are two forms of PGP partitioned format (at least from our experience). Email generated by
     * PGP desktop (now Symantec encryption desktop) and email generated by PGP universal.
     *
     * PGP desktop:
     *
     * HTML emails generated by PGP desktop have a PGPexch.htm attachment without any special headers. The outer
     * part is multipart/mixed. The outer part contains a multipart/alternative part with two similar text parts
     * (one text part was HTML containing a PGP blob). The HTML from PGPexch.htm should replace the second text part
     * and the PGPexch.htm part should be removed. If there are only two sub-parts of the outer multipart, the
     * outer multipart should be removed.
     *
     * PGP universal:
     *
     * HTML emails generated by PGP universal have a PGPexch.htm attachment and a special PGP header
     * "X-PGP-MIME-Structure: alternative". The PGPexch.htm part is part of a multipart/mixed with the first
     * part being the text part and the second part being the PGPexch.htm part. The multipart/mixed should be
     * changed into a multipart/alternative and the PGPexch.htm part should be changed into a normal html part
     */
    private boolean handlePartitionedMail(MimeMessage message)
    throws MessagingException, IOException, PartException
    {
        boolean handled = false;

        HTMLPartitionPartListener partListener = new HTMLPartitionPartListener();

        PartScanner partScanner = new PartScanner(partListener, maxMimeDepth);

        // See whether there is a PGPexch.htm part
        partScanner.scanPart(message);

        Part partitionedHTMLPart = partListener.partitionedHTMLPart;

        if (partitionedHTMLPart != null)
        {
            // Check whether it's generated by PGP desktop or universal gateway
            if (partitionedHTMLPart.getHeader(X_CONTENT_PGP_UNIVERSAL_SAVED_CONTENT_TYPE_HEADER) != null) {
                handled = handlePGPUniversal(partitionedHTMLPart);
            }
            else {
                handled = handlePGPDesktop(message, partitionedHTMLPart);
            }
        }
        else {
            logger.debug("HTML partitioned part not found");
        }

        return handled;
    }

    private boolean handlePGPUniversal(Part partitionedHTMLPart)
    throws MessagingException, IOException
    {
        boolean handled = false;

        // We need to change the part into a normal HTML part
        String savedContentType = partitionedHTMLPart.getHeader(X_CONTENT_PGP_UNIVERSAL_SAVED_CONTENT_TYPE_HEADER)[0];

        ByteArrayOutputStream html = new ByteArrayOutputStream();

        IOUtils.copy(partitionedHTMLPart.getInputStream(), html);

        partitionedHTMLPart.setDataHandler(new DataHandler(new ByteArrayDataSource(html.toByteArray(),
                savedContentType)));

        partitionedHTMLPart.removeHeader("Content-Disposition");

        String[] headers = partitionedHTMLPart.getHeader(X_PGP_MIME_STRUCTURE);

        if (headers != null && headers[0].equalsIgnoreCase("alternative"))
        {
            // Found the multipart/mixed part which must be changed to multipart/alternative
            if (partitionedHTMLPart instanceof MimeBodyPart partitionedHTMLBodyPart)
            {
                Multipart mp = partitionedHTMLBodyPart.getParent();

                if (mp != null)
                {
                    if (mp.getCount() == 2)
                    {
                        if (MimeUtils.isMimeType(mp, "multipart/mixed"))
                        {
                            // Javamail unfortunately does not allow us to change the content type to alternative. We
                            // therefore need to create a new multipart and replace the mixed with the new alternative
                            // part
                            MimeMultipart alternative = new MimeMultipart("alternative");

                            for (int i = 0; i < mp.getCount(); i++) {
                                alternative.addBodyPart(mp.getBodyPart(i));
                            }

                            Part parent = mp.getParent();

                            if (parent instanceof BodyPart parentBody)
                            {
                                // if the parent is a BodyPart, then the parent of the parent must be a multipart
                                Multipart outer = parentBody.getParent();

                                MimeBodyPart body = new MimeBodyPart();

                                body.setContent(alternative);

                                outer.addBodyPart(body, 0);
                                outer.removeBodyPart(parentBody);
                            }
                            else {
                                parent.setContent(alternative);
                            }

                            handled = true;
                        }
                        else {
                            logger.warn("Part is not a multipart/mixed");
                        }
                    }
                    else {
                        logger.warn("Multipart does not have exactly two parts");
                    }
                }
                else {
                    logger.warn("Part is not a multipart");
                }
            }
        }
        else {
            logger.warn("X-PGP-MIME-Structure: alternative header not found");
        }

        return handled;
    }

    private boolean handlePGPDesktop(MimeMessage message, Part partitionedHTMLPart)
    throws MessagingException, IOException, PartException
    {
        boolean handled = false;

        // Search for the text part that should be replaced with the HTML
        Multipart multipartAlternative = findAlternativePart(message);

        if (multipartAlternative != null)
        {
            String charset = HeaderUtils.getCharsetFromContentType(partitionedHTMLPart.getContentType(), null);

            // Replace the second part with the HTML content
            ByteArrayOutputStream html = new ByteArrayOutputStream();

            IOUtils.copy(partitionedHTMLPart.getInputStream(), html);

            String contentType = charset != null ? "text/html; charset=" +
                    MimeUtils.fromJavaCharsetToMimeCharset(charset) : "text/html;";

            multipartAlternative.getBodyPart(1).setDataHandler(new DataHandler(
                    new ByteArrayDataSource(html.toByteArray(),contentType)));

            // Remove the HTML partitioned part
            if (partitionedHTMLPart instanceof MimeBodyPart partitionedHTMLBodyPart)
            {
                Multipart mp = partitionedHTMLBodyPart.getParent();

                if (mp != null)
                {
                    if (mp.getCount() >= 2)
                    {
                        mp.removeBodyPart(partitionedHTMLBodyPart);

                        // If multipart only has two parts, then we need to remove the multipart otherwise we end up
                        // with a multipart with only one part after removing the HTML partitioned part. If there
                        // are more then 2 parts, we need to remove the HTML partitioned part
                        if (mp.getCount() <= 1) {
                            // Remove the multipart
                             Part parent = mp.getParent();

                             if (parent != null)
                             {
                                 BodyPart bp = mp.getBodyPart(0);

                                 parent.setContent(bp.getContent(), bp.getContentType());
                             }
                             else {
                                 // Should not happen
                                 logger.warn("Parent not found");
                             }
                        }

                        handled = true;
                    }
                    else {
                        // Should not happen
                        logger.warn("Parent of partitioned has less then two parts");
                    }
                }
                else {
                    // Should not happen
                    logger.warn("Parent of partitioned part is not a MultiPart");
                }
            }
            else {
                // Should not happen
                logger.warn("Partitioned part is not a MimeBodyPart");
            }
        }
        else {
            logger.debug("multipart/alternative not found");
        }

        return handled;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            MimeMessage message = mail.getMessage();

            MimeMessage clone = retainMessageID ? MailUtils.cloneMessageWithFixedMessageID(message) :
                MailUtils.cloneMessage(message);

            boolean handled = handlePartitionedMail(clone);

            if (handled)
            {
                clone.saveChanges();

                MailUtils.validateMessage(clone);

                mail.setMessage(clone);

                logger.info("PGP partitioned message was fixed-up");
            }
        }
        catch (MessagingException | IOException | PartException e) {
            logger.error("Error handling mail", e);
        }
    }

    public boolean isRetainMessageID() {
        return retainMessageID;
    }

    public void setRetainMessageID(boolean retainMessageID) {
        this.retainMessageID = retainMessageID;
    }
}