/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.slf4j.Logger;
import org.springframework.retry.RetryListener;
import org.springframework.retry.support.RetryTemplateBuilder;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

/**
 * Helper class for matchers to make it easier to create matchers that use the User objects for determining
 * if a mail matches or not
 *
 * @author Martijn Brinkers
 *
 */
public class MailAddressMatcher
{
    /*
     * Used for adding and retrieving users
     */
    private final UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    private final TransactionOperations transactionOperations;

    /*
     * The event instance to call to see if there is a match
     */
    private final HasMatchEventHandler hasMatchEventHandler;

    /*
     * The logger of the class that "owns" this MailAddressHandler
     */
    private final Logger logger;

    /*
     * Retry listener which just logs
     */
    private final RetryListener loggingRetryListener;

    /*
     * Event handler used to check for a match
     */
    public interface HasMatchEventHandler
    {
        boolean hasMatch(User user)
        throws MessagingException;
    }

    public MailAddressMatcher(TransactionOperations transactionOperations, UserWorkflow userWorkflow,
            HasMatchEventHandler hasMatchEventHandler, Logger logger)
    {
        Objects.requireNonNull(transactionOperations);
        Objects.requireNonNull(transactionOperations);
        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(hasMatchEventHandler);
        Objects.requireNonNull(logger);

        this.transactionOperations = transactionOperations;
        this.userWorkflow = userWorkflow;
        this.hasMatchEventHandler = hasMatchEventHandler;
        this.logger = logger;
        this.loggingRetryListener = new LoggingRetryListener(logger);
    }

    public Collection<MailAddress> getMatchingMailAddressesWithRetry(final Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        return getMatchingMailAddressesWithRetry(mailAddresses, null);
    }

    public Collection<MailAddress> getMatchingMailAddressesWithRetry(final Collection<MailAddress> mailAddresses,
            RetryListener retryListener)
    throws MessagingException
    {
        return getMatchingMailAddressesWithRetry(mailAddresses, retryListener, null);
    }

    public Collection<MailAddress> getMatchingMailAddressesWithRetry(final Collection<MailAddress> mailAddresses,
            RetryListener retryListener, Integer maxRetryAttempts)
    throws MessagingException
    {
        RetryTemplateBuilder retryTemplateBuilder = RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder(
                        maxRetryAttempts)
                .withListener(loggingRetryListener);

        if (retryListener != null) {
            retryTemplateBuilder.withListener(retryListener);
        }

        return retryTemplateBuilder.build().execute(ctx -> getMatchingMailAddresses(mailAddresses));
    }

    public Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        Collection<MailAddress> matching;

        try {
            matching = transactionOperations.execute(status ->
            {
                try {
                    return getMatchingMailAddressesAction(mailAddresses);
                }
                catch (HierarchicalPropertiesException | MessagingException e) {
                    throw new UnhandledException(e);
                }
            });
        }
        catch (UnhandledException e)
        {
            Throwable cause = e.getCause();

            if (cause instanceof MessagingException messagingException) {
                throw messagingException;
            }
            else if (cause instanceof HierarchicalPropertiesException hierarchicalPropertiesException) {
                throw new MessagingException("Error handling mail addresses", hierarchicalPropertiesException);
            }
            else {
                throw e;
            }
        }

        return matching;
    }

    /**
     * Called for each recipient.
     */
    private boolean hasMatch(User user)
    throws MessagingException
    {
        return hasMatchEventHandler.hasMatch(user);
    }

    private Collection<MailAddress> getMatchingMailAddressesAction(Collection<MailAddress> mailAddresses)
    throws HierarchicalPropertiesException, MessagingException
    {
        Collection<MailAddress> matching = new HashSet<>();

        for (MailAddress mailAddress : mailAddresses)
        {
            if (mailAddress != null)
            {
                // We will only accept valid email addresses. If an email address is invalid it will be
                // converted to EmailAddressUtils.INVALID_EMAIL
                String validatedEmail = EmailAddressUtils.canonicalizeAndValidate(mailAddress.toString(), false);

                if (validatedEmail != null)
                {
                    User user = userWorkflow.getUser(validatedEmail, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                    if (hasMatch(user)) {
                        matching.add(mailAddress);
                    }
                }
                else {
                    logger.debug("{} is not a valid email address.", mailAddress);
                }
            }
        }

        return matching;
    }
}
