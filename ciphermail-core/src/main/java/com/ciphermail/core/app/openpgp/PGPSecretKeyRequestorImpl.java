/*
 * Copyright (c) 2014-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.openpgp;

import com.ciphermail.core.app.openpgp.keyserver.KeyServerClient;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClientSubmitResult;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClientSubmitResultImpl;
import com.ciphermail.core.app.properties.PGPProperties;
import com.ciphermail.core.app.properties.PGPPropertiesImpl;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.openpgp.PGPKeyGenerationType;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingExporter;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporter;
import com.ciphermail.core.common.security.openpgp.PGPSecretKeyRingGenerator;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.PrivateKeysAndPublicKeyRing;
import com.ciphermail.core.common.security.openpgp.UserIDBuilder;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListEntry;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListException;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.core.common.util.DirectAccessByteArrayOutputStream;
import com.ciphermail.core.common.util.EventHandler;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPPublicKeyRingCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of PGPSecretKeyRequestor
 *
 * @author Martijn Brinkers
 *
 */
public class PGPSecretKeyRequestorImpl implements PGPSecretKeyRequestor
{
    private static final Logger logger = LoggerFactory.getLogger(PGPSecretKeyRequestorImpl.class);

    /*
     * For retrieving the user preferences
     */
    private final UserWorkflow userWorkflow;

    /*
     * For generating new secret keys
     */
    private final PGPSecretKeyRingGenerator secretKeyRingGenerator;

    /*
     * For importing the generated keys
     */
    private final PGPKeyRingImporter keyRingImporter;

    /*
     * For exporting public keys
     */
    private final PGPKeyRingExporter keyRingExporter;

    /*
     * Manages the trust for PGP keys
     */
    private final PGPTrustList trustList;

    /*
     * The KeyServerClient service
     */
    private final KeyServerClient keyServerClient;

    /*
     * Resolves the User ID from the email address
     */
    private final UserIDResolver userIDResolver;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    /*
     * Event handlers for the keyring generated event
     */
    private final EventHandler<PGPSecretKeyRingGeneratedEventHandler> secretKeyRingGeneratedEventHandlers = new EventHandler<>();

    /*
     * If true, newly generated keys are automatically trusted
     */
    private boolean autoTrust = true;

    public PGPSecretKeyRequestorImpl(
            @Nonnull UserWorkflow userWorkflow,
            @Nonnull PGPSecretKeyRingGenerator secretKeyRingGenerator,
            @Nonnull PGPKeyRingImporter keyRingImporter,
            @Nonnull PGPKeyRingExporter keyRingExporter,
            @Nonnull PGPTrustList trustList,
            @Nonnull KeyServerClient keyServerClient,
            @Nonnull UserIDResolver userIDResolver,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        this.userWorkflow = Objects.requireNonNull(userWorkflow);
        this.secretKeyRingGenerator = Objects.requireNonNull(secretKeyRingGenerator);
        this.keyRingImporter = Objects.requireNonNull(keyRingImporter);
        this.keyRingExporter = Objects.requireNonNull(keyRingExporter);
        this.trustList = Objects.requireNonNull(trustList);
        this.keyServerClient = Objects.requireNonNull(keyServerClient);
        this.userIDResolver = Objects.requireNonNull(userIDResolver);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
    }

    @Override
    public PGPSecretKeyRequestorResult requestSecretKey(@Nonnull String email)
    throws IOException, PGPException
    {
        email = EmailAddressUtils.canonicalizeAndValidate(email, true /* null if invalid */);

        if (email == null) {
            throw new PGPException("Email is not a valid email address");
        }

        try {
            UserProperties properties = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST).
                    getUserPreferences().getProperties();

            PGPProperties pgpProperties = userPropertiesFactoryRegistry.getFactoryForClass(PGPPropertiesImpl.class)
                    .createInstance(properties);

            return requestSecretKey(email, null, pgpProperties.getPGPKeyGenerationType(),
                    pgpProperties.getPGPAutoPublishKeys());
        }
        catch (AddressException | HierarchicalPropertiesException e) {
            throw new PGPException("Error getting user preferences", e);
        }
    }

    private String createUserID(String email, String username)
    throws PGPException
    {
        String userID = null;

        if (StringUtils.isEmpty(username)) {
            // The user name was not set so try to resolve the user ID
            userID = userIDResolver.resolveUserID(email);
        }

        if (StringUtils.isEmpty(userID)) {
            // The user name was explicitly set or the resolver returned an empty User ID
            UserIDBuilder userIDBuilder = new UserIDBuilder();

            userIDBuilder.setEmail(email);
            userIDBuilder.setUsername(username);

            userID = userIDBuilder.build();
        }

        return userID;
    }

    @Override
    public PGPSecretKeyRequestorResult requestSecretKey(@Nonnull String email, String name,
            PGPKeyGenerationType keyType, boolean publishKeys)
    throws IOException, PGPException
    {
        email = EmailAddressUtils.canonicalizeAndValidate(email, true /* null if invalid */);

        if (email == null) {
            throw new PGPException("Email is not a valid email address");
        }

        try {
            PrivateKeysAndPublicKeyRing privateKeysAndPublicKeyRing = secretKeyRingGenerator.generateKeyRing(
                    keyType, createUserID(email, name));

            List<PGPKeyRingEntry> imported = keyRingImporter.importKeyRing(privateKeysAndPublicKeyRing);

            PGPKeyRingEntry masterKeyEntry = null;

            for (PGPKeyRingEntry keyRingEntry : imported)
            {
                if (keyRingEntry.isMasterKey())
                {
                    masterKeyEntry = keyRingEntry;

                    break;
                }
            }

            if (masterKeyEntry == null) {
                throw new PGPException("Master key entry not found.");
            }

            logger.info("A new PGP key was created for {} with Key ID {}", email, PGPUtils.getKeyIDHex(
                    masterKeyEntry.getKeyID()));

            // Fire the secret keyring generated event to all the handlers
            for (PGPSecretKeyRingGeneratedEventHandler handler : secretKeyRingGeneratedEventHandlers.getEventHandlers())
            {
                try {
                    handler.secretKeyRingGenerated(email, imported);
                }
                catch (Exception e) {
                    logger.error("Error Event#secretKeyRingGenerated.", e);
                }
            }

            if (autoTrust)
            {
                PGPTrustListEntry trustListEntry = trustList.createEntry(masterKeyEntry.getSHA256Fingerprint());

                trustListEntry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(trustListEntry, true /* include sub keys */);
            }

            KeyServerClientSubmitResult keySubmitResult = null;

            if (publishKeys)
            {
                DirectAccessByteArrayOutputStream encodedKeyRing = new DirectAccessByteArrayOutputStream();

                if (keyRingExporter.exportPublicKeys(Collections.singletonList(masterKeyEntry), encodedKeyRing))
                {
                    try {
                        keySubmitResult = keyServerClient.submitKeys(new JcaPGPPublicKeyRingCollection(
                                PGPUtil.getDecoderStream(encodedKeyRing.getInputStream())));
                    }
                    catch (Exception e)
                    {
                        // Failure publishing the key should not result in an exception. It can happen that the
                        // external Key server is not accessible or down. The key however is already generated.
                        logger.warn("An error occurred publishing the key. Message: {}", e.getMessage());

                        if (logger.isDebugEnabled()) {
                            logger.debug("Error publishing key", e);
                        }

                        keySubmitResult = new KeyServerClientSubmitResultImpl(Collections.emptyList(),
                                Collections.emptyList(), List.of(e.getMessage()));
                    }
                }
            }

            return new PGPSecretKeyRequestorResultImpl(masterKeyEntry, keySubmitResult);
        }
        catch (PGPTrustListException e) {
            throw new PGPException("There was a problem with the trust list", e);
        }
    }

    public boolean isAutoTrust() {
        return autoTrust;
    }

    public void setAutoTrust(boolean autoTrust) {
        this.autoTrust = autoTrust;
    }

    public void addSecretKeyRingGeneratedEventHandler(PGPSecretKeyRingGeneratedEventHandler handler) {
        secretKeyRingGeneratedEventHandlers.addEventHandler(handler);
    }

    public void removeSecretKeyRingGeneratedEventHandler(PGPSecretKeyRingGeneratedEventHandler handler) {
        secretKeyRingGeneratedEventHandlers.removeEventHandler(handler);
    }

    static class PGPSecretKeyRequestorResultImpl implements PGPSecretKeyRequestorResult
    {
        final PGPKeyRingEntry masterKeyEntry;
        final KeyServerClientSubmitResult keyServerClientSubmitResult;

        PGPSecretKeyRequestorResultImpl(PGPKeyRingEntry masterKeyEntry,
                KeyServerClientSubmitResult keyServerClientSubmitResult)
        {
            this.masterKeyEntry = masterKeyEntry;
            this.keyServerClientSubmitResult = keyServerClientSubmitResult;
        }

        @Override
        public PGPKeyRingEntry getMasterKeyEntry() {
            return masterKeyEntry;
        }

        @Override
        public KeyServerClientSubmitResult getKeyServerClientSubmitResult() {
            return keyServerClientSubmitResult;
        }
    }
}
