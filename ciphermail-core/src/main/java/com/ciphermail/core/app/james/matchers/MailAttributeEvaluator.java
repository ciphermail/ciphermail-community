/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.james.eval.GetContentTypeFunction;
import com.ciphermail.core.app.james.eval.GetMailAttributeFunction;
import com.ciphermail.core.app.james.eval.GetMailSizeFunction;
import com.ciphermail.core.app.james.eval.IsMIMETypeFunction;
import com.ciphermail.core.app.james.eval.ToFloatFunction;
import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Collections;

/**
 * Matcher that matches an expression against the mail attributes.
 * <p>
 * Usage:
 * <p>
 * MailAttributeEvaluator=matchOnError=false,getMailAttribute('abc')'=='123' | Matches when the mail contains an
 * attribute 'abc' with value '123'
 * <p>
 * Note: If an attribute value is null or the attribute does not exist the string "null" will be returned. So in order to
 * check for a null or non-existing attribute you need to check for null.
 * <p>
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 *
 * @author Martijn Brinkers
 *
 */
public class MailAttributeEvaluator extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(MailAttributeEvaluator.class);

    /*
     * The expression to evaluate
     */
    private String expression;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    {
        expression = StringUtils.trimToNull(getCondition());

        if (expression == null) {
            throw new IllegalArgumentException("Expression must be specified.");
        }

        StrBuilder sb = new StrBuilder();

        sb.append("Expression: ");
        sb.append(expression);

        getLogger().info("{}", sb);
    }

    /*
     * Note: Evaluator is no thread safe.
     */
    private Evaluator getEvaluator() {
        return new Evaluator();
    }

    protected void addDefaultFunctions(Evaluator evaluator)
    {
        evaluator.putFunction(new ToFloatFunction());
        evaluator.putFunction(new IsMIMETypeFunction());
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        try {
            Evaluator evaluator = getEvaluator();

            addDefaultFunctions(evaluator);

            evaluator.putFunction(new GetContentTypeFunction(mail));
            evaluator.putFunction(new GetMailAttributeFunction(mail));
            evaluator.putFunction(new GetMailSizeFunction(mail));

            return evaluator.getBooleanResult(expression) ? mail.getRecipients() : Collections.emptyList();
        }
        catch (EvaluationException e) {
            throw new MessagingException("expression cannot be evaluated.", e);
        }
    }
}
