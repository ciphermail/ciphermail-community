/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import freemarker.template.SimpleHash;
import freemarker.template.TemplateException;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Attribute;
import org.apache.mailet.AttributeName;
import org.apache.mailet.AttributeValue;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.ParseException;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Notify is an extension of SendMailTemplate that allows some mail parameters to be set (like subject, from, to etc.).
 */
public class Notify extends SenderTemplatePropertySendMail
{
    private static final Logger logger = LoggerFactory.getLogger(Notify.class);

    /**
     * Pattern to detect user variable references in special email addresses (i.e., address
     * like #{some.var}
     */
    private static final Pattern USER_VAR_PATTERN = Pattern.compile("#\\{\\s*(.*?)\\s*}");

    /**
     * Pattern to detect Mail attribute pattern like ${some.var}
     */
    private static final Pattern MAIL_ATTR_PATTERN = Pattern.compile("\\$\\{\\s*(.*?)\\s*}");

    /*
     * Subject of the notification
     */
    private String subject;

    /*
     * from of the notification
     */
    private String from;

    /*
     * replyTo of the notification
     */
    private String replyTo;

    /*
     * to headers of the notification
     */
    private String to;

    /*
     * cc headers of the notification
     */
    private String cc;

    /*
     * The sender of the notification
     */
    private String sender;

    /*
     * The recipients of the notification
     */
    private String recipients;

    /*
     * If true, the source message will be attached as RFC822
     */
    private boolean attachSourceMessage;

    /*
     * The name of the attached source message
     */
    private String sourceMessageName;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        SUBJECT               ("subject"),
        FROM                  ("from"),
        REPLY_TO              ("replyTo"),
        TO                    ("to"),
        CC                    ("cc"),
        SENDER                ("sender"),
        RECIPIENTS            ("recipients"),
        ATTACH_SOURCE_MESSAGE ("attachSourceMessage"),
        SOURCE_MESSAGE_NAME   ("sourceMessageName");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    private String checkAddressInitParameter(String input) {
        return (input == null || SpecialAddress.fromName(input) == SpecialAddress.NULL) ? null : input;
    }

    @Override
    public void initMailetTransacted()
    throws MessagingException
    {
        super.initMailetTransacted();

        subject = getInitParameter(Parameter.SUBJECT.name);
        from = checkAddressInitParameter(getInitParameter(Parameter.FROM.name));
        replyTo = checkAddressInitParameter(getInitParameter(Parameter.REPLY_TO.name));
        to = checkAddressInitParameter(getInitParameter(Parameter.TO.name));
        cc = checkAddressInitParameter(getInitParameter(Parameter.CC.name));
        sender = checkAddressInitParameter(getInitParameter(Parameter.SENDER.name));
        recipients = Objects.requireNonNull(getInitParameter(Parameter.RECIPIENTS.name),
                "recipients must be specified");
        attachSourceMessage = getBooleanInitParameter(Parameter.ATTACH_SOURCE_MESSAGE.name, false);
        sourceMessageName = getInitParameter(Parameter.SOURCE_MESSAGE_NAME.name, "forwarded.eml");
    }

    @Override
    protected MimeMessage createMessage(Mail mail, SimpleHash root)
    throws MessagingException, TemplateException, IOException
    {
        MimeMessage message = super.createMessage(mail, root);

        if (attachSourceMessage)
        {
            try {
                MimeMessage withAttachment = MailUtils.attachMessageAsRFC822(message, mail.getMessage(),
                        sourceMessageName);

                MailUtils.validateMessage(withAttachment);

                message = withAttachment;
            }
            catch (Exception e) {
                logger.error("source message cannot be attached due to an error; MailID: {}", getMailID(mail), e);
            }
        }

        return message;
    }

    @Override
    protected String getSubject(Mail mail)
    throws MessagingException
    {
        return SpecialAddress.fromName(subject) == SpecialAddress.SAME_AS_MESSAGE ? super.getSubject(mail) : subject;
    }

    @Override
    protected Address getFrom(Mail mail)
    throws MessagingException
    {
        Address fromAddress = null;

        if (SpecialAddress.fromName(from) == SpecialAddress.SAME_AS_MESSAGE) {
            fromAddress = super.getFrom(mail);
        }
        else {
            Collection<Address> addresses = parseAddress(from, mail);

            if (CollectionUtils.isNotEmpty(addresses))
            {
                // Pick the first one
                fromAddress = addresses.iterator().next();
            }
        }

        return fromAddress;
    }

    @Override
    protected Address getReplyTo(Mail mail)
    throws MessagingException
    {
        Address replyToAddress = null;

        if (SpecialAddress.fromName(replyTo) == SpecialAddress.SAME_AS_MESSAGE) {
            replyToAddress = super.getReplyTo(mail);
        }
        else {
            Collection<Address> addresses = parseAddress(replyTo, mail);

            if (CollectionUtils.isNotEmpty(addresses))
            {
                // Pick the first one
                replyToAddress = addresses.iterator().next();
            }
        }

        return replyToAddress;
    }

    @Override
    protected MailAddress getSender(Mail mail)
    throws MessagingException
    {
        MailAddress senderAddress = null;

        if (SpecialAddress.fromName(sender) == SpecialAddress.SAME_AS_MESSAGE) {
            senderAddress = super.getSender(mail);
        }
        else {
            Collection<Address> addresses = parseAddress(sender, mail);

            if (CollectionUtils.isNotEmpty(addresses))
            {
                // Pick the first one
                InternetAddress inetAddress = EmailAddressUtils.toInternetAddress(addresses.iterator().next());

                // We never want the invalid@invalid.tld or invalid addresses
                if (EmailAddressUtils.isValid(inetAddress) && !EmailAddressUtils.isInvalidDummyAddress(inetAddress)) {
                    senderAddress = new MailAddress(inetAddress);
                }
            }
        }

        return senderAddress;
    }

    @Override
    protected String getBody(Mail mail)
    throws MessagingException
    {
        return CoreApplicationMailAttributes.getMessage(mail);
    }

    private void copyAddresses(Collection<MailAddress> source, Collection<Address> target)
    {
        if (source != null)
        {
            for (MailAddress address : source)
            {
                InternetAddress inetAddress = MailAddressUtils.toInternetAddress(address);

                // We never want the invalid@invalid.tld address
                if (inetAddress == null || EmailAddressUtils.isInvalidDummyAddress(inetAddress)) {
                    continue;
                }

                target.add(inetAddress);
            }
        }
    }

    private void copyAddresses(List<InternetAddress> source, Collection<Address> target)
    {
        if (source != null)
        {
            for (InternetAddress address : source)
            {
                // We never want the invalid@invalid.tld address
                if (EmailAddressUtils.isInvalidDummyAddress(address)) {
                    continue;
                }

                target.add(address);
            }
        }
    }

    private void copyMailAddresses(List<InternetAddress> source, Collection<Address> target)
    {
        if (source != null)
        {
            for (InternetAddress address : source)
            {
                // We never want the invalid@invalid.tld address
                if (EmailAddressUtils.isInvalidDummyAddress(address)) {
                    continue;
                }

                target.add(address);
            }
        }
    }

    private List<MailAddress> toMailAddresses(Collection<Address> addresses)
    {
        List<MailAddress> mailAddresses = new LinkedList<>();

        if (addresses != null)
        {
            for (Address address : addresses)
            {
                try {
                    InternetAddress internetAddress = EmailAddressUtils.toInternetAddress(address);

                    if (internetAddress != null && !EmailAddressUtils.isInvalidDummyAddress(internetAddress)) {
                        mailAddresses.add(new MailAddress(internetAddress));
                    }
                }
                catch (ParseException e) {
                    logger.error("{} is not a valid email address", address);
                }
            }
        }

        return mailAddresses;
    }

    /*
     * Returns the recipients used for presenting to the recipient, i.e., for the mail template. The recipients should
     * not be used for sending email because the email addresses might not be valid (non strict)
     */
    private Collection<Address> getHeaderRecipients(String value, Message.RecipientType recipientType, Mail mail)
    throws MessagingException
    {
        Collection<Address> result = null;

        if (StringUtils.isNotEmpty(value))
        {
            result = new LinkedHashSet<>();

            String[] stringRecipients = value.split("\\s*,\\s*");

            for (String recipient : stringRecipients)
            {
                recipient = StringUtils.trimToNull(recipient);

                if (recipient == null) {
                    continue;
                }

                SpecialAddress specialAddress = SpecialAddress.fromName(recipient);

                if (specialAddress == SpecialAddress.SAME_AS_RECIPIENTS) {
                    copyAddresses(getRecipients(mail), result);
                }
                else if (specialAddress == SpecialAddress.SAME_AS_MESSAGE) {
                    copyAddresses(EmailAddressUtils.getRecipientsNonStrict(mail.getMessage(), recipientType), result);
                }
                else {
                    result.addAll(parseAddress(recipient, mail));
                }
            }
        }

        return result;
    }

    @Override
    protected Collection<Address> getTo(Mail mail)
    throws MessagingException
    {
        return getHeaderRecipients(to, Message.RecipientType.TO, mail);
    }

    @Override
    protected Collection<Address> getCC(Mail mail)
    throws MessagingException
    {
        return getHeaderRecipients(cc, Message.RecipientType.CC, mail);
    }

    @Override
    protected Collection<MailAddress> getRecipients(Mail mail)
    throws MessagingException
    {
        Collection<MailAddress> result = new LinkedHashSet<>();

        String[] stringRecipients = recipients.split("\\s*,\\s*");

        for (String recipient : stringRecipients)
        {
            recipient = StringUtils.trimToNull(recipient);

            if (recipient == null) {
                continue;
            }

            SpecialAddress specialAddress = SpecialAddress.fromName(recipient);

            if (specialAddress == SpecialAddress.SAME_AS_MESSAGE ||
                    specialAddress == SpecialAddress.SAME_AS_RECIPIENTS)
            {
                // we will handle SAME_AS_RECIPIENTS the same as SAME_AS_MESSAGE
                result.addAll(super.getRecipients(mail));
            }
            else {
                result.addAll(toMailAddresses(parseAddress(recipient, mail)));
            }
        }

        if (result.isEmpty()) {
            throw new MissingRecipientsException("There are no recipients");
        }

        return result;
    }

    /*
     * Parses the address and if it's a special address, the special address is converted into a
     * real email address.
     */
    private Collection<Address> parseAddress(String input, Mail mail)
    throws MessagingException
    {
        input = StringUtils.trimToNull(input);

        Collection<Address> result = new LinkedHashSet<>();

        if (input != null)
        {
            SpecialAddress specialAddress = SpecialAddress.fromName(input);

            if (specialAddress != null) {
                handleSpecialAddress(mail, result, specialAddress);
            }
            else {
                // Check if the address is a mail attribute
                Matcher matcher = MAIL_ATTR_PATTERN.matcher(input);

                if (matcher.matches()) {
                    handleMailAttribute(mail, matcher.group(1), result);
                }
                else {
                    // Check if the address is a user property address
                    matcher = USER_VAR_PATTERN.matcher(input);

                    if (matcher.matches()) {
                        handleUserProperty(mail, matcher.group(1), result);
                    }
                    else {
                        // Assume the input is a normal email address. However, we never want
                        // the invalid@invalid.tld address
                        if (!EmailAddressUtils.isInvalidDummyAddress(input)) {
                            result.add(new InternetAddress(input, false));
                        }
                    }
                }
            }
        }

        return result;
    }

    private void addNullSafe(Address address, Collection<Address> target)
    {
        if (address != null) {
            target.add(address);
        }
    }

    private void handleSpecialAddress(Mail mail, Collection<Address> result, SpecialAddress specialAddress)
    throws MessagingException
    {
        switch (specialAddress) {
            case ORIGINATOR -> addNullSafe(getMessageOriginatorIdentifier().getOriginator(mail), result);
            case REPLY_TO   -> copyMailAddresses(EmailAddressUtils.getReplyToNonStrict(mail.getMessage()), result);
            case SENDER     -> addNullSafe(MailAddressUtils.toInternetAddress(
                                    mail.getMaybeSender().asOptional().orElse(null)), result);
            case FROM       -> copyMailAddresses(EmailAddressUtils.getFromNonStrict(mail.getMessage()), result);
            case RECIPIENTS -> result.addAll(MailAddressUtils.toInternetAddressList(
                                    MailAddressUtils.getRecipients(mail)));
            default -> throw new MessagingException("Unsupported SpecialAddress");
        }
    }

    /*
     * Tries to convert input to MailAddress's.
     */
    private void addEmails(Object input, Collection<Address> target)
    {
        if (input == null) {
            return;
        }

        if (input instanceof AttributeValue) {
            addEmails(((AttributeValue<?>)input).getValue(), target);
        }
        else if (input instanceof String inputString)
        {
            String addresses = StringUtils.trimToNull(inputString);

            if (addresses != null)
            {
                try {
                    InternetAddress[] emails = Objects.requireNonNullElse(InternetAddress.parse(addresses, false),
                            new InternetAddress[]{});

                    for (InternetAddress email : emails)
                    {
                        // We never want the invalid@invalid.tld or invalid addresses
                        if (!EmailAddressUtils.isValid(email) || EmailAddressUtils.isInvalidDummyAddress(email)) {
                            continue;
                        }

                        target.add(email);
                    }
                }
                catch(AddressException e) {
                    getLogger().warn("Email address(s) are invalid {}", addresses);
                }
            }
        }
        else if (input instanceof MailAddress mailAddress)
        {
            InternetAddress emailAddress = MailAddressUtils.toInternetAddress(mailAddress);

            // We never want the invalid@invalid.tld address
            if (emailAddress != null && !EmailAddressUtils.isInvalidDummyAddress(emailAddress)) {
                target.add(emailAddress);
            }
        }
        else if (input instanceof InternetAddress inetAddress)
        {
            // We never want the invalid@invalid.tld or invalid addresses
            if (!EmailAddressUtils.isValid(inetAddress) || !EmailAddressUtils.isInvalidDummyAddress(inetAddress)) {
                target.add(inetAddress);
            }
        }
        else if (input instanceof Collection)
        {
            for (Object o : (Collection<?>) input) {
                addEmails(o, target);
            }
        }
        else if (input instanceof Object[] objects)
        {
            for (Object o : objects) {
                addEmails(o, target);
            }
        }
        else {
            // Fallback to #toString
            addEmails(input.toString(), target);
        }
    }

    private void handleUserProperty(Mail mail, String userProperty, Collection<Address> result)
    throws MessagingException
    {
        InternetAddress originator = getMessageOriginatorIdentifier().getOriginator(mail);

        try {
            User user = getUserWorkflow().getUser(originator.getAddress(),
                    UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

            String userValue = user.getUserPreferences().getProperties().getProperty(userProperty);

            addEmails(userValue, result);
        }
        catch(HierarchicalPropertiesException e) {
            getLogger().error("Error getting user property " + userProperty, e);
        }
    }

    private void handleMailAttribute(Mail mail, String attributeName, Collection<Address> result)
    {
        Optional<Attribute> attribute = mail.getAttribute(AttributeName.of(attributeName));

        if (getLogger().isDebugEnabled()) {
            getLogger().debug("Mail attr {}, value: {}", attributeName, attribute);
        }

        attribute.ifPresent(value -> addEmails(value.getValue(), result));
    }
}
