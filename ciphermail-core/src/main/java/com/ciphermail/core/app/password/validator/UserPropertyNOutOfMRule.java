/*
 * Copyright (c) 2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.password.validator;

import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.password.validator.AbstractJSONNOutOfMRule;
import com.ciphermail.core.common.security.password.validator.JSONNOutOfMRulePasswordData;
import org.passay.PasswordData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.util.Objects;

/**
 * Password rule which validates if at least N out M validations succeed. The rules are read from a JSON user property
 */
public class UserPropertyNOutOfMRule extends AbstractJSONNOutOfMRule
{
    private static final Logger logger = LoggerFactory.getLogger(UserPropertyNOutOfMRule.class);

    /*
     * Used for adding and retrieving users
     */
    private final UserWorkflow userWorkflow;

    /*
     * The name of the user property from which the JSON will be read from
     */
    private final String userProperty;

    public UserPropertyNOutOfMRule(@Nonnull UserWorkflow userWorkflow, @Nonnull String userProperty)
    {
        this.userWorkflow = Objects.requireNonNull(userWorkflow);
        this.userProperty = Objects.requireNonNull(userProperty);
    }

    @Override
    public String getJSON(PasswordData passwordData)
    {
        if (passwordData == null) {
            return null;
        }

        // check if PasswordData contains pre-configured JSON
        if (passwordData instanceof JSONNOutOfMRulePasswordData jsonnOutOfMRulePasswordData)
        {
            logger.atDebug().log("Using pre-configured JSON {}", jsonnOutOfMRulePasswordData.getJson());

            return jsonnOutOfMRulePasswordData.getJson();
        }

        String validatedEmail = EmailAddressUtils.canonicalizeAndValidate(passwordData.getUsername(), true);

        if (validatedEmail == null)
        {
            logger.warn("{} is not a valid email address", passwordData.getUsername());

            return null;
        }

        String result = null;

        try {
            UserProperties properties = userWorkflow.getUser(validatedEmail,
                    UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).getUserPreferences().
                    getProperties();

            result = properties.getProperty(userProperty);
        }
        catch (AddressException | HierarchicalPropertiesException e) {
            logger.error("Error reading property", e);
        }

        return result;
    }
}
