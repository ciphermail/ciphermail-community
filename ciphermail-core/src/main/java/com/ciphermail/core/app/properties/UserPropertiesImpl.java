/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.app.EncryptMode;
import com.ciphermail.core.app.UserLocality;
import com.ciphermail.core.common.properties.DefaultPropertyProviderRegistry;
import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import com.ciphermail.core.common.properties.Parent;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * UserProperties implementation that reads/writes the user property values from/to a  HierarchicalProperties instance.
 */
@UserPropertiesType(name = "general", displayName = "General", order = 10)
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class UserPropertiesImpl extends DelegatedHierarchicalProperties implements UserProperties
{
    private static final Logger logger = LoggerFactory.getLogger(UserPropertiesImpl.class);

    private static final String COMMENT                         = "comment";
    private static final String DATE_CREATED                    = "date-created";
    private static final String LOCALITY                        = "locality";
    private static final String ENCRYPT_MODE                    = "encrypt-mode";
    private static final String SKIP_CALENDAR                   = "skip-calendar";
    private static final String SEND_ENCRYPTION_NOTIFICATION    = "send-encryption-notification";
    private static final String PASSWORD_POLICY                 = "password-policy";
    private static final String SUBJECT_FILTER_ENABLED          = "subject-filter-enabled";
    private static final String SUBJECT_FILTER_REGEX            = "subject-filter-regex";
    private static final String POST_PROCESSING_HEADER_INTERNAL = "post-processing-header-internal";
    private static final String POST_PROCESSING_HEADER_EXTERNAL = "post-processing-header-external";
    private static final String ORGANIZATION_ID                 = "organization-id";
    private static final String SYSTEM_EMAIL_SENDER             = "system-email-sender";
    private static final String SYSTEM_EMAIL_FROM               = "system-email-from";
    private static final String FOOTER                          = "email-footer";
    private static final String FOOTER_LINK                     = "email-footer-link";
    private static final String FOOTER_LINK_TITLE               = "email-footer-link-title";
    private static final String SERVER_SECRET                   = "server-secret";
    private static final String CLIENT_SECRET                   = "client-secret";
    private static final String SYSTEM_MAIL_SECRET              = "system-mail-secret";
    private static final String CUSTOM_1                        = "custom-1";
    private static final String CUSTOM_2                        = "custom-2";
    private static final String CUSTOM_3                        = "custom-3";

    @Autowired
    private DefaultPropertyProviderRegistry defaultPropertyProviderRegistry;

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public UserPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(
            name = COMMENT
    )
    public void setComment(String comment)
    throws HierarchicalPropertiesException
    {
        setProperty(COMMENT, UserPropertiesValidators.validateMaxLength(COMMENT, comment, 250));
    }

    @Override
    @UserProperty(
            name = COMMENT,
            order = 10,
            allowNull = true
    )
    public String getComment()
    throws HierarchicalPropertiesException
    {
        return getProperty(COMMENT);
    }

    @Override
    @UserProperty(
            name = DATE_CREATED,
            // created should not be changed via REST API
            user = false, domain = false, global = false,
            editor = UserPropertyEditors.DATE_EDITOR
    )
    public void setDateCreated(Long time)
    throws HierarchicalPropertiesException
    {
        // Store the date as long
        HierarchicalPropertiesUtils.setLong(this, DATE_CREATED, time);
    }

    @Override
    @UserProperty(
            name = DATE_CREATED,
            // created is user only property
            domain = false, global = false,
            editor = UserPropertyEditors.DATE_EDITOR,
            order = 20,
            allowNull = true
    )
    public Long getDateCreated()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, DATE_CREATED, null);
    }

    @Override
    @UserProperty(name = LOCALITY,
            order = 30
    )
    public UserLocality getUserLocality()
    throws HierarchicalPropertiesException
    {
        // if locality is not set, assume EXTERNAL
        return Objects.requireNonNullElse(HierarchicalPropertiesUtils.getEnum(this,
                LOCALITY, UserLocality.class), UserLocality.EXTERNAL);
    }

    @Override
    @UserProperty(name = LOCALITY)
    public void setUserLocality(UserLocality locality)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, LOCALITY, locality);
    }

    @Override
    @UserProperty(name = ENCRYPT_MODE,
            order = 40
    )
    public EncryptMode getEncryptMode()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(this, ENCRYPT_MODE, EncryptMode.class);
    }

    @Override
    @UserProperty(name = ENCRYPT_MODE)
    public void setEncryptMode(EncryptMode encryptMode)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, ENCRYPT_MODE, encryptMode);
    }

    @Override
    @UserProperty(name = SEND_ENCRYPTION_NOTIFICATION,
            order = 50
    )
    public Boolean getSendEncryptionNotification()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SEND_ENCRYPTION_NOTIFICATION);
    }

    @Override
    @UserProperty(name = SEND_ENCRYPTION_NOTIFICATION)
    public void setSendEncryptionNotification(Boolean sendNotification)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SEND_ENCRYPTION_NOTIFICATION, sendNotification);
    }

    @Override
    @UserProperty(name = SKIP_CALENDAR,
            order = 60
    )
    public Boolean getSkipCalendar()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SKIP_CALENDAR);
    }

    @Override
    @UserProperty(name = SKIP_CALENDAR)
    public void setSkipCalendar(Boolean skip)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SKIP_CALENDAR, skip);
    }

    @Override
    @UserProperty(name = PASSWORD_POLICY)
    public void setPasswordPolicy(String passwordPolicy)
    throws HierarchicalPropertiesException
    {
        setProperty(PASSWORD_POLICY, UserPropertiesValidators.validatePasswordPolicy(PASSWORD_POLICY,
                passwordPolicy));
    }

    @Override
    @UserProperty(
            name = PASSWORD_POLICY,
            order = 80
    )
    public String getPasswordPolicy()
    throws HierarchicalPropertiesException
    {
        return getProperty(PASSWORD_POLICY);
    }

    @Override
    @UserProperty(
            name = SUBJECT_FILTER_ENABLED
    )
    public void setSubjectFilterEnabled(Boolean sign)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SUBJECT_FILTER_ENABLED, sign);
    }

    @Override
    @UserProperty(
            name = SUBJECT_FILTER_ENABLED,
            order = 130
    )
    public Boolean getSubjectFilterEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SUBJECT_FILTER_ENABLED, false);
    }

    @Override
    @UserProperty(
            name = SUBJECT_FILTER_REGEX,
            user = false, domain = false
    )
    public void setSubjectFilterRegEx(String subjectFilter)
    throws HierarchicalPropertiesException
    {
        setProperty(SUBJECT_FILTER_REGEX, UserPropertiesValidators.validateSubjectFilter(SUBJECT_FILTER_REGEX, subjectFilter));
    }

    @Override
    @UserProperty(
            name = SUBJECT_FILTER_REGEX,
            user = false, domain = false,
            order = 140,
            allowNull = true
    )
    public String getSubjectFilterRegEx()
    throws HierarchicalPropertiesException
    {
        return getProperty(SUBJECT_FILTER_REGEX);
    }

    @Override
    @UserProperty(
            name = POST_PROCESSING_HEADER_INTERNAL,
            user = false, domain = false
    )
    public void setPostProcessingHeaderInternal(String headerNameValue)
    throws HierarchicalPropertiesException
    {
        setProperty(POST_PROCESSING_HEADER_INTERNAL, UserPropertiesValidators.validateHeader(
                POST_PROCESSING_HEADER_INTERNAL, headerNameValue));
    }

    @Override
    @UserProperty(
            name = POST_PROCESSING_HEADER_INTERNAL,
            user = false, domain = false,
            order = 150,
            allowNull = true
    )
    public String getPostProcessingHeaderInternal()
    throws HierarchicalPropertiesException
    {
        return getProperty(POST_PROCESSING_HEADER_INTERNAL);
    }

    @Override
    @UserProperty(
            name = POST_PROCESSING_HEADER_EXTERNAL,
            user = false, domain = false
    )
    public void setPostProcessingHeaderExternal(String headerNameValue)
    throws HierarchicalPropertiesException
    {
        setProperty(POST_PROCESSING_HEADER_EXTERNAL, UserPropertiesValidators.validateHeader(
                POST_PROCESSING_HEADER_EXTERNAL, headerNameValue));
    }

    @Override
    @UserProperty(
            name = POST_PROCESSING_HEADER_EXTERNAL,
            user = false, domain = false,
            order = 160,
            allowNull = true
    )
    public String getPostProcessingHeaderExternal()
    throws HierarchicalPropertiesException
    {
        return getProperty(POST_PROCESSING_HEADER_EXTERNAL);
    }

    @Override
    @UserProperty(name = ORGANIZATION_ID)
    public void setOrganizationID(String organizationID)
    throws HierarchicalPropertiesException
    {
        setProperty(ORGANIZATION_ID, organizationID);
    }

    @Override
    @UserProperty(name = ORGANIZATION_ID,
            order = 165,
            allowNull = true
    )
    public String getOrganizationID()
    throws HierarchicalPropertiesException
    {
        return getProperty(ORGANIZATION_ID);
    }

    @Override
    @UserProperty(
            name = SYSTEM_EMAIL_SENDER,
            user = false, domain = false
    )
    public void setSystemSender(String email)
    throws HierarchicalPropertiesException
    {
        setProperty(SYSTEM_EMAIL_SENDER, UserPropertiesValidators.validateEmail(SYSTEM_EMAIL_SENDER, email));
    }

    @Override
    @UserProperty(
            name = SYSTEM_EMAIL_SENDER,
            user = false, domain = false,
            order = 170,
            allowNull = true
    )
    public String getSystemSender()
    throws HierarchicalPropertiesException
    {
        return getProperty(SYSTEM_EMAIL_SENDER);
    }

    @Override
    @UserProperty(
            name = SYSTEM_EMAIL_FROM,
            user = false, domain = false
    )
    public void setSystemFrom(String email)
    throws HierarchicalPropertiesException
    {
        setProperty(SYSTEM_EMAIL_FROM, UserPropertiesValidators.validateEmail(SYSTEM_EMAIL_FROM, email));
    }

    @Override
    @UserProperty(
            name = SYSTEM_EMAIL_FROM,
            user = false, domain = false,
            order = 180,
            allowNull = true
    )
    public String getSystemFrom()
    throws HierarchicalPropertiesException
    {
        return getProperty(SYSTEM_EMAIL_FROM);
    }

    @Override
    @UserProperty(
            name = FOOTER
    )
    public void setFooter(String footer)
    throws HierarchicalPropertiesException
    {
        setProperty(FOOTER, footer);
    }

    @Override
    @UserProperty(
            name = FOOTER,
            order = 190
    )
    public String getFooter()
    throws HierarchicalPropertiesException
    {
        return getProperty(FOOTER);
    }

    @Override
    @UserProperty(
            name = FOOTER_LINK
    )
    public void setFooterLink(String footerLink)
    throws HierarchicalPropertiesException
    {
        setProperty(FOOTER_LINK, footerLink);
    }

    @Override
    @UserProperty(
            name = FOOTER_LINK,
            order = 200
    )
    public String getFooterLink()
    throws HierarchicalPropertiesException
    {
        return getProperty(FOOTER_LINK);
    }

    @Override
    @UserProperty(
            name = FOOTER_LINK_TITLE
    )
    public void setFooterLinkTitle(String footerLinkTitle)
    throws HierarchicalPropertiesException
    {
        setProperty(FOOTER_LINK_TITLE, footerLinkTitle);
    }

    @Override
    @UserProperty(
            name = FOOTER_LINK_TITLE,
            order = 210
    )
    public String getFooterLinkTitle()
    throws HierarchicalPropertiesException
    {
        return getProperty(FOOTER_LINK_TITLE);
    }

    @Override
    @UserProperty(
            name = SERVER_SECRET
    )
    public void setServerSecret(String secret)
    throws HierarchicalPropertiesException
    {
        setProperty(SERVER_SECRET, StringUtils.trimToNull(secret));
    }

    @Override
    @UserProperty(
            name = SERVER_SECRET,
            visible = false,
            allowNull = true
    )
    public String getServerSecret()
    throws HierarchicalPropertiesException
    {
        return getProperty(SERVER_SECRET);
    }

    @Override
    @UserProperty(
            name = CLIENT_SECRET
    )
    public void setClientSecret(String secret)
    throws HierarchicalPropertiesException
    {
        setProperty(CLIENT_SECRET, StringUtils.trimToNull(secret));
    }

    @Override
    @UserProperty(
            name = CLIENT_SECRET,
            visible = false,
            allowNull = true
    )
    public String getClientSecret()
    throws HierarchicalPropertiesException
    {
        return getProperty(CLIENT_SECRET);
    }

    @Override
    @UserProperty(
            name = SYSTEM_MAIL_SECRET
    )
    public void setSystemMailSecret(String secret)
    throws HierarchicalPropertiesException
    {
        setProperty(SYSTEM_MAIL_SECRET, StringUtils.trimToNull(secret));
    }

    @Override
    @UserProperty(
            name = SYSTEM_MAIL_SECRET,
            user = false, domain = false,
            visible = false,
            allowNull = true
    )
    public String getSystemMailSecret()
    throws HierarchicalPropertiesException
    {
        return getProperty(SYSTEM_MAIL_SECRET);
    }

    @Override
    @UserProperty(
            name = CUSTOM_1
    )
    public void setCustom1(String value)
    throws HierarchicalPropertiesException
    {
        setProperty(CUSTOM_1, value);
    }

    @Override
    @UserProperty(
            name = CUSTOM_1,
            visible = false
    )
    public String getCustom1()
    throws HierarchicalPropertiesException
    {
        return getProperty(CUSTOM_1);
    }

    @Override
    @UserProperty(
            name = CUSTOM_2
    )
    public void setCustom2(String value)
    throws HierarchicalPropertiesException
    {
        setProperty(CUSTOM_2, value);
    }

    @Override
    @UserProperty(
            name = CUSTOM_2,
            visible = false
    )
    public String getCustom2()
    throws HierarchicalPropertiesException
    {
        return getProperty(CUSTOM_2);
    }

    @Override
    @UserProperty(
            name = CUSTOM_3
    )
    public void setCustom3(String value)
    throws HierarchicalPropertiesException
    {
        setProperty(CUSTOM_3, value);
    }

    @Override
    @UserProperty(
            name = CUSTOM_3,
            visible = false
    )
    public String getCustom3()
    throws HierarchicalPropertiesException
    {
        return getProperty(CUSTOM_3);
    }

    @Override
    public String getProperty(@Nonnull String propertyName)
    throws HierarchicalPropertiesException
    {
        return getProperty(propertyName, Parent.CHECK_PARENT);
    }

    @Override
    public String getProperty(@Nonnull String propertyName, @Nonnull Parent checkParent)
    throws HierarchicalPropertiesException
    {
        String value = super.getProperty(propertyName, checkParent);

        if (value == null)
        {
            // Some values need to have a 'calculated' default value since the default value will be based on other
            // property values. We therefore need to check whether the value is null and if so, get the default value.
            value = defaultPropertyProviderRegistry.getDefaultValue(this, propertyName);
        }

        // warn if a default property value is not available and the property is not allowed to be null
        if (value == null && !defaultPropertyProviderRegistry.isNullDefaultValueAllowed(propertyName)) {
            logger.warn("Missing factory property for property {}", propertyName);
        }

        return value;
    }

    @VisibleForTesting
    public void setDefaultPropertyProviderRegistry(DefaultPropertyProviderRegistry defaultPropertyProviderRegistry) {
        this.defaultPropertyProviderRegistry = Objects.requireNonNull(defaultPropertyProviderRegistry);
    }
}
