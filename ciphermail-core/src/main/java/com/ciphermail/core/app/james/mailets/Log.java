/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.google.common.annotations.VisibleForTesting;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.common.logging.LogLevel;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.util.Objects;

/**
 * Mailet that logs comments and or info about a mail message to the logger.
 */
@SuppressWarnings({"java:S6813"})
public class Log extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(Log.class);

    private enum LogDetail {COMMENT, BASIC, MIDDLE, FULL}

    /*
     * The comment that gets logged.
     */
    private String comment;

    /*
     * Default logDetail.
     */
    private LogDetail logDetail = LogDetail.BASIC;

    /*
     * If true, the Mail error message will be logged
     */
    private boolean logErrorMessage;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public final void initMailet()
    throws MessagingException
    {
        comment = getInitParameter("comment");

        // We want to set the default loglevel to INFO if it's not specified
        if (getInitParameter("logLevel") == null) {
            setLogLevel(LogLevel.INFO);
        }

        String parameter = getInitParameter("logDetail");

        if (parameter != null) {
            logDetail = LogDetail.valueOf(parameter.trim().toUpperCase());
        }

        logErrorMessage = getBooleanInitParameter("logErrorMessage", false);

        Objects.requireNonNull(messageOriginatorIdentifier);
    }

    /*
     * Returns the originator of the message (by default will return the from field)
     */
    private String getOriginator(Mail mail)
    {
        String originator = null;

        try {
            InternetAddress address = messageOriginatorIdentifier.getOriginator(mail);

            originator = address.getAddress();

            // We don't want the invalid@invalid.tld user in the logs
            if (EmailAddressUtils.isInvalidDummyAddress(originator)) {
                originator = "<>";
            }
        }
        catch (Exception e) {
            logger.debug("Error getting originator.", e);
        }

        if (StringUtils.isBlank(originator)) {
            originator = "<>";
        }

        return originator;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        StrBuilder strBuilder = new StrBuilder(256);

        strBuilder.setNewLineText("; ");

        strBuilder.append(comment);

        if (logDetail.compareTo(LogDetail.BASIC) >= 0)
        {
            String mailID = CoreApplicationMailAttributes.getMailID(mail);

            strBuilder.append("; MailID: ");
            strBuilder.appendln(mailID);
            strBuilder.append("Recipients: ");
            strBuilder.appendln(mail.getRecipients());
        }

        if (logDetail.compareTo(LogDetail.MIDDLE) >= 0)
        {
            strBuilder.append("Originator: ");
            strBuilder.appendln(getOriginator(mail));
            strBuilder.append("Sender: ");
            strBuilder.appendln(mail.getMaybeSender().asString());
            strBuilder.append("Remote address: ");
            strBuilder.appendln(mail.getRemoteAddr());

            try {
                if (mail.getMessage() != null)
                {
                    String subject = MailUtils.getSafeSubject(mail.getMessage());

                    strBuilder.append("Subject: ");
                    strBuilder.appendln(subject);
                }
            }
            catch(Exception e) {
                getLogger().error("Error getting subject.", e);
            }

            try {
                if (mail.getMessage() != null)
                {
                    String messageID = mail.getMessage().getMessageID();

                    strBuilder.append("Message-ID: ");
                    strBuilder.appendln(messageID);
                }
            }
            catch(Exception e) {
                getLogger().error("Error getting messageID.", e);
            }
        }

        if (logDetail.compareTo(LogDetail.FULL) >= 0)
        {
            strBuilder.append("Last updated: ");
            strBuilder.appendln(mail.getLastUpdated());
            strBuilder.append("Attribute names: ");
            strBuilder.appendAll(mail.attributeNames().toList());
        }

        if (logErrorMessage) {
            strBuilder.append("Error message: ").appendln(mail.getErrorMessage());
        }

        logMessage(strBuilder.toString());
    }

    private void logMessage(String message)
    {
        switch (getLogLevel()) {
            case WARN -> getLogger().warn(message);
            case ERROR -> getLogger().error(message);
            case DEBUG -> getLogger().debug(message);
            case TRACE -> getLogger().trace(message);
            default    -> getLogger().info(message);
        }
    }

    @VisibleForTesting
    void setMessageOriginatorIdentifier(MessageOriginatorIdentifier messageOriginatorIdentifier) {
        this.messageOriginatorIdentifier = messageOriginatorIdentifier;
    }
}
