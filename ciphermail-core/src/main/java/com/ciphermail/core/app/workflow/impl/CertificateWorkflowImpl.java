/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow.impl;

import com.ciphermail.core.app.impl.hibernate.UserPreferencesDAO;
import com.ciphermail.core.app.workflow.CertificateWorkflow;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreEntity;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import org.apache.commons.lang.UnhandledException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Objects;

public class CertificateWorkflowImpl implements CertificateWorkflow
{
    private static final Logger logger = LoggerFactory.getLogger(CertificateWorkflowImpl.class);

    /*
     * The certificate store to which certificates will be added
     */
    private final X509CertStoreExt certStore;

    /*
     * Manager for Hibernate sessions
     */
    private final SessionManager sessionManager;

    /*
     * Used to execute database actions within separate transactions
     */
    private final TransactionOperations transactionOperations;

    /*
     * Retry listener which just logs
     */
    private final RetryListener loggingRetryListener = new LoggingRetryListener(logger);

    public CertificateWorkflowImpl(@Nonnull X509CertStoreExt certStore, @Nonnull SessionManager sessionManager,
            @Nonnull TransactionOperations transactionOperations)
    {
        this.certStore = Objects.requireNonNull(certStore);
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
    }

    protected TransactionOperations getTransactionOperations() {
        return transactionOperations;
    }

    protected SessionManager getSessionManager() {
        return sessionManager;
    }

    protected RetryTemplate createRetryTemplate()
    {
        return RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder()
                .withListener(loggingRetryListener)
                .build();
    }

    @Override
    public boolean addCertificateTransacted(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        RetryTemplate retryTemplate = createRetryTemplate();

        try {
            return retryTemplate.execute(ctx -> transactionOperations.execute(status ->
            {
                try {
                   return addCertificateInternal(certificate);
                }
                catch (CertStoreException e) {
                    throw new UnhandledException(e);
                }
            }));
        }
        catch (UnhandledException e)
        {
            Throwable cause = e.getCause();

            if (cause instanceof CertStoreException certStoreException) {
                throw certStoreException;
            }
            else {
                throw e;
            }
        }
    }

    private boolean addCertificateInternal(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        boolean added = false;

        if (!certStore.contains(certificate))
        {
            certStore.addCertificate(certificate);

            added = true;
        }

        return added;
    }

    @Override
    public boolean isInUse(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        SessionAdapter session = SessionAdapterFactory.create(sessionManager.getSession());

        UserPreferencesDAO userPreferencesDAO = UserPreferencesDAO.getInstance(session);

        X509CertStoreEntry certificateEntry = certStore.getByCertificate(certificate);

        boolean inUse = false;

        // We can only check if the X509CertStoreEntry is a Hibernate entity.
        if (certificateEntry instanceof X509CertStoreEntity x509CertStoreEntity) {
            inUse = userPreferencesDAO.isReferencing(x509CertStoreEntity);
        }

        return inUse;
    }
}
