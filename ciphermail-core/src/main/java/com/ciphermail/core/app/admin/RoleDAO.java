/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.admin;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.hibernate.SortDirection;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * DAO for {@link Role}
 */
public class RoleDAO extends GenericHibernateDAO
{
    public RoleDAO(SessionAdapter session) {
        super(session);
    }

    public Role getRole(@Nonnull String name)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Role> criteriaQuery = criteriaBuilder.createQuery(Role.class);

        Root<Role> rootEntity = criteriaQuery.from(Role.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(Role.NAME_COLUMN), name));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }

    public boolean deleteRole(@Nonnull String name)
    {
        boolean deleted = false;

        Role role = getRole(name);

        if (role != null)
        {
            delete(role);

            deleted = true;
        }

        return deleted;
    }

    public void deleteAllRoles()
    {
        List<Role> roles = getRoles(null, null, null);

        for (Role role : roles) {
            delete(role);
        }
    }

    public @Nonnull List<Role> getRoles(Integer firstResult, Integer maxResults, SortDirection sortDirection)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Role> criteriaQuery = criteriaBuilder.createQuery(Role.class);

        Root<Role> rootEntity = criteriaQuery.from(Role.class);

        criteriaQuery.orderBy(sortDirection == SortDirection.DESC ?
                criteriaBuilder.desc(rootEntity.get(Role.NAME_COLUMN)) :
                criteriaBuilder.asc(rootEntity.get(Role.NAME_COLUMN)));

        Query<Role> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    public long getRoleCount()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<Role> entityRoot = criteriaQuery.from(Role.class);

        criteriaQuery.select(criteriaBuilder.count(entityRoot));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }
}
