/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import com.ciphermail.core.app.NamedCertificate;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.impl.NamedCertificateImpl;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.properties.UserPropertiesImpl;
import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.hibernate.HierarchicalPropertiesPropertyEntity;
import com.ciphermail.core.common.properties.hibernate.NamedBlobEntity;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreEntity;
import com.ciphermail.core.common.util.LinkedSet;
import com.ciphermail.core.common.util.LinkedSetImpl;
import com.ciphermail.core.common.util.LoopDetector;
import com.ciphermail.core.common.util.ObservableLinkedSet;
import com.ciphermail.core.common.util.ObservableRuntimeException;
import com.ciphermail.core.common.util.ObservableSet;
import com.ciphermail.core.common.util.SetBridge;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * UserPreferencesHibernate is not thread safe. Life-time should not be any longer than the lifetime of the
 * associated session because the instance cannot be used any longer after the session has been closed.
 *
 * @author Martijn Brinkers
 *
 */
public class UserPreferencesHibernate implements UserPreferences
{
    private static final Logger logger = LoggerFactory.getLogger(UserPreferencesHibernate.class) ;

    /*
     * Because UserPreferencesHibernate can contain references to other UserPreferencesHibernate there is
     * always the chance of a loop. The LoopDetector will be used to detect such loops.
     */
    private final LoopDetector loopDetector;

    /*
     * Tokens used by the loop detector. Exact value does not matter as long as they are unique.
     */
    private static final String GET_PROPERTIES_TOKEN = "1";
    private static final String GET_INH_CERTS_TOKEN  = "2";
    private static final String GET_INH_KEY_AND_CERTS_TOKEN = "3";
    private static final String GET_INH_NAMED_CERTS_TOKEN  = "4";

    private final UserPreferencesEntity userPreferencesEntity;
    private final KeyAndCertStore keyAndCertStore;
    private final Session session;

    /*
     * Provides a factory for creating UserPropertiesImpl instances
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    /**
     * Create a UserPreferences instance with a database back-end.
     * <p>
     * Note: the actual implementation of KeyAndCertStore should return {@link X509CertStoreEntity} instances ie. for
     * UserPreferencesHibernate we will rely on a specific implementation of the X509CertStoreExt. We can however not easily
     * check in advance if the X509CertStoreExt is a specific implementation because the KeyAndCertStore can use delegation
     * to delegate the X509CertStoreExt interface to another X509CertStoreExt. See getCertStoreEntry for reasons why we
     * need a specific implementation.
     */
    public UserPreferencesHibernate(
            @Nonnull UserPreferencesEntity userPreferencesEntity,
            @Nonnull KeyAndCertStore keyAndCertStore,
            @Nonnull Session session,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        this(userPreferencesEntity, keyAndCertStore, session, userPropertiesFactoryRegistry, new LoopDetector());
    }

    private UserPreferencesHibernate(
            @Nonnull UserPreferencesEntity userPreferencesEntity,
            @Nonnull KeyAndCertStore keyAndCertStore,
            @Nonnull Session session,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry,
            @Nonnull LoopDetector loopDetector)
    {
        this.userPreferencesEntity = Objects.requireNonNull(userPreferencesEntity);
        this.keyAndCertStore = Objects.requireNonNull(keyAndCertStore);
        this.session = Objects.requireNonNull(session);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
        this.loopDetector = Objects.requireNonNull(loopDetector);
    }

    @Override
    public @Nonnull String getName() {
        return userPreferencesEntity.getName();
    }

    @Override
    public @Nonnull String getCategory() {
        return userPreferencesEntity.getCategory();
    }

    protected UserPreferencesEntity getUserPreferencesEntity() {
        return userPreferencesEntity;
    }

    @Override
    public @Nonnull Set<X509Certificate> getCertificates()
    {
        Set<X509Certificate> certificates = new HashSet<>();

        Set<X509CertStoreEntity> storeEntries = userPreferencesEntity.getCertificates();

        for (X509CertStoreEntity entry : storeEntries) {
            certificates.add(entry.getCertificate());
        }

        // create a Set instance that wraps-up the certificates set so we can intercept changes
        // (like add and remove) made to the certificates set. We need this to reflect these
        // changes to the database
        ObservableSet<X509Certificate> certificateObserver = new ObservableSet<>(certificates);

        certificateObserver.setAddEvent(this::onAddCertificate);

        certificateObserver.setRemoveEvent(this::onRemoveCertificate);

        return certificateObserver;
    }

    @Override
    public @Nonnull Set<NamedCertificate> getNamedCertificates()
    {
        Set<NamedCertificate> namedCertificates = new HashSet<>();

        Set<NamedCertificateHibernate> entries = userPreferencesEntity.getNamedCertificates();

        for (NamedCertificateHibernate entry : entries)
        {
            namedCertificates.add(new NamedCertificateImpl(entry.getName(),
                    entry.getCertificateEntry().getCertificate()));
        }

        // create a Set instance that wraps-up the certificates set so we can intercept changes
        // (like add and remove) made to the certificates set. We need this to reflect these
        // changes to the database
        ObservableSet<NamedCertificate> certificateObserver = new ObservableSet<>(namedCertificates);

        certificateObserver.setAddEvent(this::onAddNamedCertificate);

        certificateObserver.setRemoveEvent(this::onRemoveNamedCertificate);

        return certificateObserver;
    }

    @Override
    public @Nonnull Set<X509Certificate> getInheritedCertificates()
    {
        Set<X509Certificate> inheritedCertificates = new HashSet<>();

        // because user preferences are hierarchical there is a change of a loop which we need to
        // detect and handle.
        String token = createToken(GET_INH_CERTS_TOKEN);

        if (!loopDetector.hasToken(token))
        {
            loopDetector.addToken(token);

            try {
                Set<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();

                for (UserPreferences userPreferences : inheritedUserPreferences)
                {
                    inheritedCertificates.addAll(userPreferences.getCertificates());
                    inheritedCertificates.addAll(userPreferences.getInheritedCertificates());
                }
            }
            finally {
                loopDetector.removeToken(token);
            }
        }
        else {
            logger.warn("Loop detected calling #getInheritedCertificates (token: {})", token);
        }

        return Collections.unmodifiableSet(inheritedCertificates);
    }

    @Override
    public @Nonnull Set<NamedCertificate> getInheritedNamedCertificates()
    {
        Set<NamedCertificate> inheritedNamedCertificates = new HashSet<>();

        // because user preferences are hierarchical there is a change of a loop which we need to
        // detect and handle.
        String token = createToken(GET_INH_NAMED_CERTS_TOKEN);

        if (!loopDetector.hasToken(token))
        {
            loopDetector.addToken(token);

            try {
                Set<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();

                for (UserPreferences userPreferences : inheritedUserPreferences)
                {
                    inheritedNamedCertificates.addAll(userPreferences.getNamedCertificates());
                    inheritedNamedCertificates.addAll(userPreferences.getInheritedNamedCertificates());
                }
            }
            finally {
                loopDetector.removeToken(token);
            }
        }
        else {
            logger.warn("Loop detected calling #getInheritedNamedCertificates (token: {})", token);
        }

        return Collections.unmodifiableSet(inheritedNamedCertificates);
    }

    /*
     * Returns a Set of all the inherited UserPreferences.
     */
    private LinkedSet<UserPreferences> getInheritedUserPreferencesInternal()
    {
        LinkedSetImpl<UserPreferences> inheritedUserPreferences = new LinkedSetImpl<>();

        // get a set of join table entries that associates this UserPreferences with the inherited UserPreferences.
        Set<InheritedUserPreferences> joinEntities = userPreferencesEntity.getInheritedPreferences();

        for (InheritedUserPreferences joinEntity : joinEntities)
        {
            // get the inherited entity
            UserPreferencesEntity inheritedUserPreferencesEntity = joinEntity.getInheritedPreferences();

            if (inheritedUserPreferencesEntity != null)
            {
                // because we create a new instance of UserPreferencesHibernate we need to share the
                // loop detector.
                inheritedUserPreferences.add(new UserPreferencesHibernate(inheritedUserPreferencesEntity,
                        keyAndCertStore, session, userPropertiesFactoryRegistry, loopDetector));
            }
        }

        return inheritedUserPreferences;
    }

    @Override
    public @Nonnull LinkedSet<UserPreferences> getInheritedUserPreferences()
    {
        LinkedSet<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();

        // Create a Set that wraps-up inheritedUserPreferences so we can keep track of changes to the Set and
        // reflect these changes in the database.
        ObservableLinkedSet<UserPreferences > userPreferencesObserver = new ObservableLinkedSet<>(inheritedUserPreferences);

        userPreferencesObserver.setAddEvent(this::onAddUserPreferences);

        userPreferencesObserver.setRemoveEvent(this::onRemoveUserPreferences);

        return userPreferencesObserver;
    }

    @Override
    public @Nonnull UserProperties getProperties()
    {
        HierarchicalProperties parent = null;

        // because user preferences are hierarchical there is a change of a loop which we need to
        // detect and handle.
        String token = createToken(GET_PROPERTIES_TOKEN);

        if (!loopDetector.hasToken(token))
        {
            loopDetector.addToken(token);

            try {
                // we need to build a tree of properties each inheriting from the previous.
                Set<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();

                for (UserPreferences userPreferences : inheritedUserPreferences)
                {
                    if (userPreferences != null) {
                        parent = new DelegatedHierarchicalProperties(userPreferences.getProperties(), parent);
                    }
                }
            }
            finally {
                loopDetector.removeToken(token);
            }
        }
        else {
            logger.warn("Loop detected calling #getProperties (token: {})", token);
        }

        return userPropertiesFactoryRegistry.getFactoryForClass(UserPropertiesImpl.class)
                .createInstance(new HierarchicalPropertiesPropertyEntity(
                        userPreferencesEntity.getPropertyEntity(), parent));
    }

    /**
     * Returns the UserProperties which are explicitly set, i.e., no inherited properties are returned. This is
     * for example used when exporting the user properties.
     */
    @Override
    public @Nonnull UserProperties getExplicitlySetProperties()
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(UserPropertiesImpl.class)
                .createInstance(new HierarchicalPropertiesPropertyEntity(
                        userPreferencesEntity.getPropertyEntity(), null));
    }

    @Override
    public @Nonnull Set<NamedBlob> getNamedBlobs() {
        return new SetBridge<>(userPreferencesEntity.getNamedBlobs(), NamedBlobEntity.class);
    }

    @Override
    public KeyAndCertificate getKeyAndCertificate()
    throws CertStoreException, KeyStoreException
    {
        KeyAndCertificate keyAndCertificate = null;

        X509CertStoreEntity certStoreEntry = userPreferencesEntity.getKeyAndCertificateEntry();

        if (certStoreEntry != null) {
            keyAndCertificate = keyAndCertStore.getKeyAndCertificate(certStoreEntry);
        }

        return keyAndCertificate;
    }

    @Override
    public void setKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException
    {
        if (keyAndCertificate != null)
        {
            // Note: We assume that the keyAndCertificate has already been added to the KeyAndCertStore.
            // We previously always added the keyAndCertificate to the KeyAndCertStore but this was problematic
            // when the PKCS11 provider is used since it will remove the chain from the KeyStore when setting
            // a key for an existing alias.
            X509CertStoreEntity certStoreEntry = getCertStoreEntry(keyAndCertificate.getCertificate());

            if (certStoreEntry != null) {
                userPreferencesEntity.setKeyAndCertificate(certStoreEntry);
            }
            else {
                // we have just added it but, it can be removed in another session
                logger.warn("certStoreEntry not found.");
            }
        }
        else {
            userPreferencesEntity.setKeyAndCertificate(null);
        }
    }

    @Override
    public Set<KeyAndCertificate> getInheritedKeyAndCertificates()
    throws CertStoreException, KeyStoreException
    {
        Set<KeyAndCertificate> inheritedKeyAndCertificates = new HashSet<>();

        // because user preferences are hierarchical there is a change of a loop which we need to
        // detect and handle.
        String token = createToken(GET_INH_KEY_AND_CERTS_TOKEN);

        if (!loopDetector.hasToken(token))
        {
            loopDetector.addToken(token);

            try {
                Set<UserPreferences> inheritedUserPreferences = getInheritedUserPreferencesInternal();

                for (UserPreferences userPreferences : inheritedUserPreferences)
                {
                    if (userPreferences.getKeyAndCertificate() != null) {
                        inheritedKeyAndCertificates.add(userPreferences.getKeyAndCertificate());
                    }

                    inheritedKeyAndCertificates.addAll(userPreferences.getInheritedKeyAndCertificates());
                }
            }
            finally {
                loopDetector.removeToken(token);
            }
        }
        else {
            logger.warn("Loop detected with token: {}", token);
        }

        return Collections.unmodifiableSet(inheritedKeyAndCertificates);
    }

    /*
     * Returns the X509CertStoreEntity with the given certificate. Returns null if there
     * is no entry with the certificate.
     */
    private X509CertStoreEntity getCertStoreEntry(X509Certificate certificate)
    throws CertStoreException
    {
        X509CertStoreEntry certStoreEntry = keyAndCertStore.getByCertificate(certificate);

        // This is a kind of 'hack' because we are now relying on a specific implementation of
        // the returned X509CertStoreEntry. The reason for this is that I would like to shield
        // Hibernate as much as possible from the API but, I would like still be able to use
        // the database build-in referential checks. I therefore must use a Hibernate entity
        // but, I do not want to make the API to depend on Hibernate.
        if (certStoreEntry != null && !(certStoreEntry instanceof X509CertStoreEntity)) {
            throw new IllegalArgumentException("The returned certStoreEntry is not a X509CertStoreEntity.");
        }

        return (X509CertStoreEntity) certStoreEntry;
    }

    /*
     * Gets called when a certificate is added to the collection of certificates (see getCertificates()).
     */
    private boolean onAddCertificate(X509Certificate certificate)
    {
        X509CertStoreEntity entry;

        try {
            // we try to get the entry from the X509CertStore.
            entry = getCertStoreEntry(certificate);
        }
        catch (CertStoreException e) {
            throw new ObservableRuntimeException(e);
        }

        if (entry == null)
        {
            logger.warn("Certificate entry not found. Adding it the the certStore.");

            X509CertStoreEntry newEntry;

            try {
                newEntry = keyAndCertStore.addCertificate(certificate);
            }
            catch (CertStoreException e) {
                throw new ObservableRuntimeException(e);
            }

            // see getCertStoreEntry why we need on a specific implementation.
            if (!(newEntry instanceof X509CertStoreEntity)) {
                throw new IllegalArgumentException("The returned certStoreEntry is not a X509CertStoreEntity.");
            }

            entry = (X509CertStoreEntity) newEntry;
        }

        userPreferencesEntity.getCertificates().add(entry);

        // We will always return true. It can happen that the certificate was already added in a different thread.
        // We therefore would like to always add it to the source of the ObservableCollection.
        return true;
    }

    /*
     * Gets called when a certificate is removed from the collection of certificates (see getCertificates()).
     */
    private boolean onRemoveCertificate(Object object)
    {
        if (!(object instanceof X509Certificate certificate)) {
            throw new IllegalArgumentException("Object must be a X509Certificate.");
        }

        X509CertStoreEntity entry;

        try {
            entry = getCertStoreEntry(certificate);
        }
        catch (CertStoreException e) {
            throw new ObservableRuntimeException(e);
        }

        if (entry != null) {
            userPreferencesEntity.getCertificates().remove(entry);
        }
        else {
            // the entry was probably removed.
            logger.warn("certStore entry not found.");
        }

        // We will always return true even when it's not removed. It can happen that the certificate
        // is already removed in a different thread. We therefore would like to always remove it from to the
        // source of the ObservableCollection.
        return true;
    }

    /*
     * Gets called when a namedCertificate is added to the collection of namedCertificates
     */
    private boolean onAddNamedCertificate(@Nonnull NamedCertificate namedCertificate)
    {
        X509CertStoreEntity entry;

        try {
            // we try to get the entry from the X509CertStore.
            entry = getCertStoreEntry(namedCertificate.getCertificate());
        }
        catch (CertStoreException e) {
            throw new ObservableRuntimeException(e);
        }

        if (entry == null)
        {
            logger.warn("Certificate entry not found. Adding it the the certStore.");

            X509CertStoreEntry newEntry;

            try {
                newEntry = keyAndCertStore.addCertificate(namedCertificate.getCertificate());
            }
            catch (CertStoreException e) {
                throw new ObservableRuntimeException(e);
            }

            // see getCertStoreEntry why we need on a specific implementation.
            if (!(newEntry instanceof X509CertStoreEntity)) {
                throw new IllegalArgumentException("The returned certStoreEntry is not a X509CertStoreEntity.");
            }

            entry = (X509CertStoreEntity) newEntry;
        }

        NamedCertificateHibernate embeddable = new NamedCertificateHibernate(
                namedCertificate.getName(), entry);

        userPreferencesEntity.getNamedCertificates().add(embeddable);

        // We will always return true. It can happen that the certificate was already added in a different thread.
        // We therefore would like to always add it to the source of the ObservableCollection.
        return true;
    }

    /*
     * Gets called when a certificate is removed from the collection of certificates (see getCertificates()).
     */
    private boolean onRemoveNamedCertificate(Object object)
    {
        if (!(object instanceof NamedCertificate namedCertificate)) {
            throw new IllegalArgumentException("Object must be a NamedCertificate.");
        }

        X509CertStoreEntity entry;

        try {
            entry = getCertStoreEntry(namedCertificate.getCertificate());
        }
        catch (CertStoreException e) {
            throw new ObservableRuntimeException(e);
        }

        if (entry != null)
        {
            NamedCertificateHibernate embeddable = new NamedCertificateHibernate(
                    namedCertificate.getName(), entry);

            userPreferencesEntity.getNamedCertificates().remove(embeddable);
        }
        else {
            // the entry was probably removed.
            logger.warn("certStore entry not found.");
        }
        // We will always return true even when it's not removed. It can happen that the certificate
        // is already removed in a different thread. We therefore would like to always remove it from to the
        // source of the ObservableCollection.
        return true;
    }

    /*
     * Gets called when a new UserPreferences object is added to the collection of UserPreferences.
     * This implementation only supports UserPreferences that are implemented by a UserPreferencesHibernate.
     * A IllegalArgumentException is thrown when the UserPreferences is not a UserPreferencesHibernate.
     */
    private boolean onAddUserPreferences(@Nonnull UserPreferences newUserPreferences)
    {
        if (!(newUserPreferences instanceof UserPreferencesHibernate newUserPreferencesHibernate)) {
            throw new IllegalArgumentException("newUserPreferences is not a UserPreferencesHibernate");
        }

        long newIndex = userPreferencesEntity.getInheritedPreferences().size() + 1L;

        // Get the entity of the new UserPreferences
        InheritedUserPreferences newInheritedUserPreferences = new InheritedUserPreferences(
                newUserPreferencesHibernate.userPreferencesEntity, newIndex);

        boolean added = false;

        if (!userPreferencesEntity.getInheritedPreferences().contains(newInheritedUserPreferences))
        {
            userPreferencesEntity.getInheritedPreferences().add(newInheritedUserPreferences);

            added = true;
        }

        return added;
    }

    /*
     * Gets called when a new UserPreferences object is removed from the collection of UserPreferences.
     * This implementation only supports UserPreferences that are implemented by a UserPreferencesHibernate.
     * A IllegalArgumentException is thrown when the UserPreferences is not a UserPreferencesHibernate.
     */
    private boolean onRemoveUserPreferences(Object object)
    {
        if (!(object instanceof UserPreferencesHibernate toRemoveUserPreferencesHibernate)) {
            throw new IllegalArgumentException("object is not a UserPreferencesHibernate");
        }

        // Get the entity of the UserPreferences to remove
        InheritedUserPreferences inheritedUserPreferences = new InheritedUserPreferences(
                toRemoveUserPreferencesHibernate.userPreferencesEntity);

        boolean removed = userPreferencesEntity.getInheritedPreferences().remove(inheritedUserPreferences);

        if (!removed) {
            logger.warn("UserPreferences not removed.");
        }

        return removed;
    }

    private String getBusinessKey() {
        return getCategory() + ":" + getName();
    }

    /*
     * Creates a token that is a unique identifier for a resource used by this class.
     * This token is used for loop detection. We cannot just compare memory locations
     * because a lot of delegation classes are used.
     */
    private String createToken(String prefix) {
        return prefix + getBusinessKey();
    }

    @Override
    public String toString() {
        return getBusinessKey();
    }

    /*
     * a UserPreferencesHibernate is identified by its unique name so equals compares the name.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof UserPreferences rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(getCategory(), rhs.getCategory())
            .append(getName(), rhs.getName())
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(getCategory())
            .append(getName())
            .toHashCode();
    }
}
