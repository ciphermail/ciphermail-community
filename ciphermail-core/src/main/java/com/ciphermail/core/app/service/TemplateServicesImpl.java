/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.service;

import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.properties.UserPropertiesType;
import com.ciphermail.core.app.properties.WhitelistUserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.ReadOnlyHierarchicalProperties;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.util.List;
import java.util.Objects;

/**
 * Services which should be accessible from a Freemarker template
 */
public class TemplateServicesImpl implements TemplateServices
{
    /*
     * Loads a user and it's properties
     */
    private final UserWorkflow userWorkflow;

    /*
     * Provides a factory for creating UserPropertiesImpl instances
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    /*
     * List of property name which are allowed to be accesses from the templates
     */
    private final List<String> propertyWhitelist;

    public TemplateServicesImpl(
            @Nonnull UserWorkflow userWorkflow,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry,
            @Nonnull List<String> propertyWhitelist)
    {
        this.userWorkflow = Objects.requireNonNull(userWorkflow);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
        this.propertyWhitelist = Objects.requireNonNull(propertyWhitelist);
    }

    /**
     * Returns the properties for the user. The returned properties is read-only.
     */
    @Override
    public Object getPropertiesInstance(String email, String propertiesClassName)
    throws AddressException, ClassNotFoundException, HierarchicalPropertiesException
    {
        Class<?> clazz = Class.forName(propertiesClassName);

        // check if class has a UserPropertiesType annotation because we do not want other classes to be constructed
        // because of security reasons (since this is called from an admin configurable template)
        if (clazz.getAnnotation(UserPropertiesType.class) == null)
        {
            throw new IllegalArgumentException(
                    "Class %s is not annotated with a UserPropertiesType".formatted(clazz.getName()));
        }

        // wrap the user properties read only and only allow properties from the white-list
        // Note: the WhitelistUserProperties must be auto wired to make sure that DefaultPropertyProviderRegistry is set
        return userPropertiesFactoryRegistry.getFactoryForClass(clazz).createInstance(
                new WhitelistUserProperties(new ReadOnlyHierarchicalProperties(userWorkflow.getUser(email,
                                UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).
                                    getUserPreferences().getProperties()),
                    propertyWhitelist), true);
    }
}
