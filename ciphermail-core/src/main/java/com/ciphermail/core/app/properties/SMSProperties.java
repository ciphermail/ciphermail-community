/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

import javax.annotation.Nonnull;

/**
 * SMS specific user properties
 */
public interface SMSProperties
{
    /**
     * The telephone number used to when sending SMS messages.
     */
    void setSMSPhoneNumber(String phoneNumber)
    throws HierarchicalPropertiesException;

    String getSMSPhoneNumber()
    throws HierarchicalPropertiesException;

    /**
     * If the user allows an SMS to be sent
     */
    void setSMSSendEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMSSendEnabled()
    throws HierarchicalPropertiesException;

    /**
     * If the user allows an SMS to be received
     */
    void setSMSReceiveEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMSReceiveEnabled()
    throws HierarchicalPropertiesException;

    /**
     * If the user is enabled to set the SMS phone number
     */
    void setSMSPhoneNumberSetEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMSPhoneNumberSetEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The default country code used when phone number starts with 0
     */
    void setPhoneDefaultCountryCode(String defaultCountryCode)
    throws HierarchicalPropertiesException;

    String getPhoneDefaultCountryCode()
    throws HierarchicalPropertiesException;

    /**
     * The SMS transport that is selected as the active SMS transport
     */
    void setActiveSMSTransportName(String defaultSMSTransportName)
    throws HierarchicalPropertiesException;

    String getActiveSMSTransportName()
    throws HierarchicalPropertiesException;
}
