/*
 * Copyright (c) 2020-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferencesPGPSigningKeySelector;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.openpgp.PGPHashAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.bouncycastle.openpgp.PGPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Mailet which auto selects a signing algorithm which is required for the signing key, i.e., if the signing key is for
 * example an EC 512bit key, the signing algorithm should be set to SHA-512. If not, gpg for example will not validate
 * the signature
 */
@SuppressWarnings({"java:S6813"})
public class PGPAutoSelectSigningAlgorithm extends AbstractTransactedMailet
{
    private static final Logger logger = LoggerFactory.getLogger(PGPAutoSelectSigningAlgorithm.class);

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Retrieves the PGP signing key for a user preferences instance
     */
    @Inject
    private UserPreferencesPGPSigningKeySelector signingKeySelector;

    /*
     * The selected signing algo will be set in the mail under this mail attribute
     */
    private String signingAlgorithmAttribute;

    /*
     * The next processor if a PGP signing certificate was available for the sender and a signing algorithm was
     * selected.
     */
    private String processor;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        SIGNING_ALGO_ATTRIBUTE ("signingAlgorithmAttribute"),
        PROCESSOR              ("processor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void initMailetTransacted()
    throws MessagingException
    {
        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(signingKeySelector);

        signingAlgorithmAttribute = Objects.requireNonNull(StringUtils.trimToNull(getInitParameter(
                Parameter.SIGNING_ALGO_ATTRIBUTE.name)),
                Parameter.SIGNING_ALGO_ATTRIBUTE.name + " must be set");

        processor = StringUtils.trimToNull(getInitParameter(Parameter.PROCESSOR.name));
    }

    @Override
    protected void serviceMailTransacted(Mail mail)
    throws MessagingException, IOException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        try {
            User user = userWorkflow.getUser(originator.getAddress(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

            PGPKeyRingEntry entry = signingKeySelector.select(user.getUserPreferences());

            if (entry != null)
            {
                getLogger().debug("There is a signing PGP key for user {}", user.getEmail());

                PGPHashAlgorithm hashAlgo = PGPHashAlgorithm.fromTag(PGPUtils.getMinRequiredHashAlgorithm(
                        entry.getPublicKey()));

                if (hashAlgo != null)
                {
                    getLogger().info("Signing algorithm {} selected", hashAlgo);

                    mail.setAttribute(Attribute.convertToAttribute(signingAlgorithmAttribute, hashAlgo.name()));

                    if (StringUtils.isNotEmpty(processor)) {
                        mail.setState(processor);
                    }
                }
                else {
                    getLogger().warn("Signing algorithm not detected");
                }
            }
            else {
                getLogger().debug("There is no signing PGP key for user {}", user.getEmail());
            }
        }
        catch (HierarchicalPropertiesException | PGPException e) {
            throw new MessagingException("Error auto selecting signing algorithm", e);
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (processor != null) {
            requiredProcessors.add(new ProcessingState(processor));
        }

        return requiredProcessors;
    }
}
