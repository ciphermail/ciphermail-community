/*
 * Copyright (c) 2009-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;

/**
 * Matcher that matches the recipient against a property read from the sender user properties.
 *
 * @author Martijn Brinkers
 *
 */
@SuppressWarnings({"java:S6813"})
public class RecipientIsSenderProperty extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(RecipientIsSenderProperty.class);

    /*
     * The key under which we store the activation context
     */
    private static final String ACTIVATION_CONTEXT_KEY = "context";

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * The user property of the target recipient
     */
    private String userProperty;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    throws MessagingException
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(transactionOperations);

        userProperty = StringUtils.trimToNull(getCondition());

        if (userProperty == null) {
            throw new MessagingException("userProperty is missing.");
        }

        StrBuilder sb = new StrBuilder();

        sb.append("userProperty: ");
        sb.append(userProperty);

        getLogger().info("{}", sb);
    }

    @VisibleForTesting
    protected boolean hasMatch(final User user)
    throws MessagingException
    {
        String value;

        try {
            value = user.getUserPreferences().getProperties().getProperty(userProperty);
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error getting value from property: " + userProperty, e);
        }

        getActivationContext().set(ACTIVATION_CONTEXT_KEY, value);

        return true;
    }

    private String getPropertyValue(Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = RecipientIsSenderProperty.this::hasMatch;

        MailAddressMatcher matcher = new MailAddressMatcher(transactionOperations, userWorkflow,
                hasMatchEventHandler, getLogger());

        matcher.getMatchingMailAddressesWithRetry(mailAddresses);

        return getActivationContext().get(ACTIVATION_CONTEXT_KEY, String.class);
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        Collection<MailAddress> matching = new HashSet<>();

        MailAddress sender = new MailAddress(originator);

        // get the email address from the sender property (if available)
        String matchingRecipient = EmailAddressUtils.canonicalize(
                getPropertyValue(Collections.singleton(sender)));

        if (matchingRecipient != null)
        {
            Collection<MailAddress> recipients = MailAddressUtils.getRecipients(mail);

            if (recipients != null)
            {
                // now check if a recipient of the email is equal to the value read from the sender property
                // and if so, return the recipient
                for (MailAddress recipient : recipients)
                {
                    String email = EmailAddressUtils.canonicalize(recipient.toString());

                    if (matchingRecipient.equals(email)) {
                        matching.add(recipient);
                    }
                }
            }
        }

        return matching;
    }
}
