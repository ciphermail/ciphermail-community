/*
 * Copyright (c) 2009-2023 CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.RegExHeaderNameMatcher;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Mailet that removes headers (matched against the header name and not header value) that match a regular expression.
 */
public class RemoveHeaders extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(RemoveHeaders.class);

    /*
     * The header reg ex
     */
    private Pattern pattern;

    /*
     * If set and an error occurs removing the headers, the name of next processor
     */
    private String errorProcessor;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        PATTERN         ("pattern"),
        ERROR_PROCESSOR ("errorProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    public final void initMailet()
    throws MessagingException
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        String param = Objects.requireNonNull(getInitParameter(Parameter.PATTERN.name),
                Parameter.PATTERN.name + " must be specified");

        pattern = Pattern.compile(param);
        errorProcessor = getInitParameter(Parameter.ERROR_PROCESSOR.name);
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            final MimeMessage sourceMessage = mail.getMessage();

            HeaderMatcher matcher = new RegExHeaderNameMatcher(pattern);

            List<String> headersToRemove = new LinkedList<>();

            for (Enumeration<?> headerEnum = sourceMessage.getAllHeaders(); headerEnum.hasMoreElements();)
            {
                Header header = (Header)headerEnum.nextElement();

                if (matcher.isMatch(header)) {
                    headersToRemove.add(header.getName());
                }
            }

            try {
                if (!headersToRemove.isEmpty())
                {
                    MimeMessage clone = MailUtils.cloneMessageWithFixedMessageID(sourceMessage);

                    for (String header : headersToRemove) {
                        clone.removeHeader(header);
                    }

                    clone.saveChanges();

                    MailUtils.validateMessage(clone);

                    mail.setMessage(clone);
                }
            }
            catch (Exception e)
            {
                logger.warn("Message with message-id {} and MailID {} is not a valid MIME message. " +
                        "The header(s) can therefore not be removed.",
                        MailUtils.getMessageIDQuietly(sourceMessage),
                        CoreApplicationMailAttributes.getMailID(mail));

                if (StringUtils.isNotEmpty(errorProcessor)) {
                    mail.setState(errorProcessor);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Error removing headers", e);

            if (StringUtils.isNotEmpty(errorProcessor)) {
                mail.setState(errorProcessor);
            }
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (errorProcessor != null) {
            requiredProcessors.add(new ProcessingState(errorProcessor));
        }

        return requiredProcessors;
    }
}
