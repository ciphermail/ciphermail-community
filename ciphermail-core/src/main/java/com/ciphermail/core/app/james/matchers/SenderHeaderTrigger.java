/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.trigger.HeaderTriggerUtils;
import com.ciphermail.core.common.util.JSONUtils;
import com.ciphermail.core.common.util.Result2;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.util.Collection;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Matcher that matches on a specific header and header value. The matcher condition should be specified as a JSON
 * string.
 * <p>
 * The property value must be a name value pair separated by ":"
 * <p>
 * Example:
 * <p>
 * X-SIGN : .*
 * <p>
 * If there is only a header name and no reg expression all header values will be accepted.
 * <p>
 * If the property value is empty or not set or the regular expression is not a valid
 * regular expression this matcher does not match.
 */
@SuppressWarnings({"java:S6813"})
public class SenderHeaderTrigger extends AbstractHeaderValueRegEx
{
    private static final Logger logger = LoggerFactory.getLogger(SenderHeaderTrigger.class);

    /*
     * JSON property names
     */
    private static final String JSON_TRIGGER_PROPERTY_NAME_PARAM = "triggerProperty";
    private static final String JSON_ENABLED_PROPERTY_NAME_PARAM = "enabledProperty";
    private static final String JSON_LOG_ON_MATCH_PARAM = "logOnMatch";
    private static final String JSON_LOG_ON_NO_MATCH_PARAM = "logOnNoMatch";
    private static final String JSON_LOG_ON_DISABLED_PARAM = "logOnDisabled";

    /*
     * The key under which we store the activation context
     */
    protected static final String ACTIVATION_CONTEXT_KEY = "trigger";

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Property under which the pattern is stored
     */
    private String triggerProperty;

    /*
     * Property under which the enabled value is stored
     */
    private String enabledProperty;

    /*
     * The string to log when there is a match
     */
    private String logOnMatch;

    /*
     * The string to log when there is a no match
     */
    private String logOnNoMatch;

    /*
     * The string to log when the trigger is not enabled
     */
    private String logOnDisabled;

    protected static class Trigger
    {
        /*
         * The header to match on
         */
        private final String header;

        /*
         * The pattern to match the header value on
         */
        private final Pattern pattern;

        /*
         * True if the trigger is enabled
         */
        private final boolean enabled;

        public Trigger(String header, Pattern pattern, boolean enabled)
        {
            this.header = header;
            this.pattern = pattern;
            this.enabled = enabled;
        }

        public String getHeader() {
            return header;
        }

        public Pattern getPattern() {
            return pattern;
        }

        public boolean isEnabled() {
            return enabled;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void logOnMatch(Mail mail)
    {
        if (logOnMatch != null)
        {
            String mailID = null;

            if (mail != null) {
                mailID = CoreApplicationMailAttributes.getMailID(mail);
            }

            getLogger().info(logOnMatch, mailID);
        }
        else {
            super.logOnMatch(mail);
        }
    }

    @Override
    protected void logOnNoMatch(Mail mail)
    {
        if (logOnNoMatch != null)
        {
            String mailID = null;

            if (mail != null) {
                mailID = CoreApplicationMailAttributes.getMailID(mail);
            }

            getLogger().info(logOnNoMatch, mailID);
        }
        else {
            super.logOnNoMatch(mail);
        }
    }

    private void logOnDisabled(Mail mail)
    {
        String mailID = null;

        if (mail != null) {
            mailID = CoreApplicationMailAttributes.getMailID(mail);
        }

        if (logOnDisabled != null)
        {
            getLogger().info(logOnDisabled, mailID);
        }
        else {
            getLogger().debug("#logOnDisabled. MailID: {}", mailID);
        }
    }

    @Override
    public void init()
    throws MessagingException
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(transactionOperations);

        String condition = StringUtils.trimToNull(getCondition());

        if (condition == null) {
            throw new IllegalArgumentException("condition is missing.");
        }

        // The condition should be a JSON string
        try {
            JSONObject parameters = new JSONObject(condition);

            JSONUtils.checkSupportedNamedParameters(parameters,
                    JSON_TRIGGER_PROPERTY_NAME_PARAM,
                    JSON_ENABLED_PROPERTY_NAME_PARAM,
                    JSON_LOG_ON_MATCH_PARAM,
                    JSON_LOG_ON_NO_MATCH_PARAM,
                    JSON_LOG_ON_DISABLED_PARAM);

            triggerProperty = StringUtils.trimToNull(parameters.optString(JSON_TRIGGER_PROPERTY_NAME_PARAM));
            enabledProperty = StringUtils.trimToNull(parameters.optString(JSON_ENABLED_PROPERTY_NAME_PARAM));
            logOnMatch = StringUtils.trimToNull(parameters.optString(JSON_LOG_ON_MATCH_PARAM));
            logOnNoMatch = StringUtils.trimToNull(parameters.optString(JSON_LOG_ON_NO_MATCH_PARAM));
            logOnDisabled = StringUtils.trimToNull(parameters.optString(JSON_LOG_ON_DISABLED_PARAM));
        }
        catch (JSONException e) {
            throw new IllegalArgumentException("Illegal JSON value", e);
        }

        if (triggerProperty == null) {
            throw new IllegalArgumentException("property condition is missing.");
        }
    }

    @Override
    protected String getHeader(Mail mail)
    throws MessagingException
    {
        Trigger trigger = getActivationContext().get(ACTIVATION_CONTEXT_KEY, Trigger.class);

        if (trigger == null) {
            return null;
        }

        return trigger.getHeader();
    }

    @Override
    protected Pattern getHeaderValuePattern(Mail mail)
    {
        Trigger trigger = getActivationContext().get(ACTIVATION_CONTEXT_KEY, Trigger.class);

        if (trigger == null) {
            return null;
        }

        return trigger.getPattern();
    }

    private Trigger getTriggerTransacted(Mail mail)
    throws MessagingException, HierarchicalPropertiesException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        String userEmail = originator.getAddress();

        userEmail = EmailAddressUtils.canonicalizeAndValidate(userEmail, false);

        User user = userWorkflow.getUser(userEmail, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

        UserProperties userProperties = user.getUserPreferences().getProperties();

        String triggerValue = StringUtils.trimToNull(userProperties.getProperty(triggerProperty));

        if (triggerValue == null)
        {
            getLogger().debug("triggerValue is null");

            return null;
        }

        Result2<String, Pattern> headerNameValue = null;

        try {
            headerNameValue = HeaderTriggerUtils.parseHeaderTrigger(triggerValue);
        }
        catch (Exception e) {
            getLogger().warn("Invalid Trigger value: " + triggerValue, e);
        }

        if (headerNameValue == null)
        {
            getLogger().debug("headerNameValue is null");

            return null;
        }

        boolean enabled = false;

        String enabledValue = user.getUserPreferences().getProperties().getProperty(enabledProperty);

        if (StringUtils.isNotEmpty(enabledValue)) {
            enabled = BooleanUtils.toBoolean(enabledValue);
        }

        return new Trigger(headerNameValue.getValue1(), headerNameValue.getValue2(), enabled);
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        try {
            Trigger trigger = transactionOperations.execute(status ->
            {
                try {
                    return getTriggerTransacted(mail);
                }
                catch (MessagingException | HierarchicalPropertiesException e) {
                    throw new UnhandledException(e);
                }
            });

            // We only want to log the disabled log line if a trigger was set and enabled is false. The reason for this
            // is that by default a trigger is not set and we do not want to clutter the logs.
            if (trigger != null && trigger.getHeader() != null)
            {
                if (trigger.isEnabled()) {
                    getActivationContext().set(ACTIVATION_CONTEXT_KEY, trigger);
                }
                else {
                    logOnDisabled(mail);
                }
            }

            return super.matchMail(mail);
        }
        catch (UnhandledException e) {
            throw new MessagingException("Error getting header trigger value", e);
        }
    }
}
