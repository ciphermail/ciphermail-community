/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.jackson.JacksonUtil;
import org.apache.commons.lang.UnhandledException;
import org.apache.mailet.Mail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * This class is used for storing certificates into a James {@link Mail} object as a JSON serialized mail
 * attribute.
 */
public class Certificates
{
    /*
     * The certificates that will be serialized
     */
    private Set<X509Certificate> certificateCollection;

    public Certificates() {
        this.certificateCollection = new HashSet<>();
    }

    public Certificates(Set<X509Certificate> certificates) {
        setCertificates(certificates);
    }

    public Set<X509Certificate> getCertificates() {
        return certificateCollection;
    }

    public void setCertificates(Set<X509Certificate> certificates) {
        this.certificateCollection = certificates != null ? new HashSet<>(certificates) : new HashSet<>();
    }

    /**
     * Serializes Certificates to JSON
     */
    public static String toJSON(Certificates certificates)
    throws IOException, CertificateEncodingException
    {
        Objects.requireNonNull(certificates);

        List<byte[]> encodedCertificates = new ArrayList<>(certificates.certificateCollection.size());

        for (X509Certificate certificate : certificates.certificateCollection) {
            encodedCertificates.add(certificate.getEncoded());
        }

        return JacksonUtil.getObjectMapper().writeValueAsString(encodedCertificates);
    }

    /**
     * Deserializes JSON to Certificates
     */
    public static Certificates fromJSON(String json)
    throws IOException, CertificateException
    {
        Objects.requireNonNull(json);

        CertificateFactory certificateFactory = null;

        try {
            certificateFactory = SecurityFactoryFactory.getSecurityFactory().
                    createCertificateFactory("X.509");
        }
        catch (NoSuchProviderException e) {
            throw new UnhandledException(e);
        }

        List<byte[]> encodedCertificates = JacksonUtil.getObjectMapper().readValue(json,
                new TypeReference<>(){});

        Set<X509Certificate> certificates = new HashSet<>(encodedCertificates.size());

        for (byte[] encodedCertificate : encodedCertificates)
        {
            certificates.add((X509Certificate) certificateFactory.generateCertificate(
                    new ByteArrayInputStream(encodedCertificate)));
        }

        return new Certificates(certificates);
    }
}
