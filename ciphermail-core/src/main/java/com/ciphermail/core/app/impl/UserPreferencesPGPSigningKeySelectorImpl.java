/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.UserPreferencesPGPSigningKeySelector;
import com.ciphermail.core.app.properties.PGPProperties;
import com.ciphermail.core.app.properties.PGPPropertiesImpl;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPPrivateKeySelector;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;

/**
 * Returns the PGP signing key for a UserPreferences object (user, domain etc). It checks if a signing certificate
 * is set for the user preferences. If so, the key will be checked for validity. If the pre-selected key is no longer
 * valid, or if there was no pre-selected key, the first available valid key will be selected.
 */
public class UserPreferencesPGPSigningKeySelectorImpl implements UserPreferencesPGPSigningKeySelector
{
    private static final Logger logger = LoggerFactory.getLogger(UserPreferencesPGPSigningKeySelectorImpl.class);

    /*
     * Returns the signing keys for an email address
     */
    private final PGPPrivateKeySelector signingKeySelector;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    public UserPreferencesPGPSigningKeySelectorImpl(@Nonnull PGPPrivateKeySelector signingKeySelector,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        this.signingKeySelector = Objects.requireNonNull(signingKeySelector);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
    }

    @Override
    public PGPKeyRingEntry select(UserPreferences userPreferences)
    throws HierarchicalPropertiesException, IOException, PGPException
    {
        PGPKeyRingEntry signingKey = null;

        if (userPreferences == null) {
            return null;
        }

        UserPreferencesCategory category = UserPreferencesCategory.fromName(userPreferences.getCategory());

        // only support user and domain category
        if (category != UserPreferencesCategory.USER && category != UserPreferencesCategory.DOMAIN)
        {
            logger.debug("Unsupported category {}", category);

            return null;
        }

        // get all available valid signing keys and check if there is already one selected,
        // and if so, if it is still in the list of acceptable keys.
        Set<PGPKeyRingEntry> validKeyEntries = signingKeySelector.select(userPreferences.getName());

        // Check if there was already a selected key for the preferences and whether the key
        // still exists and is still valid (i.e., it's in the list)
        final UserProperties userProperties = userPreferences.getProperties();

        final PGPProperties pgpProperties = userPropertiesFactoryRegistry.getFactoryForClass(PGPPropertiesImpl.class)
                .createInstance(userProperties);

        String sha256Fingerprint = pgpProperties.getPGPSigningKey();

        if (CollectionUtils.isNotEmpty(validKeyEntries))
        {
            PGPKeyRingEntry possibleKey = null;

            if (StringUtils.isNotEmpty(sha256Fingerprint))
            {
                // Step through all the available keys to see whether the selected one is still available
                for (PGPKeyRingEntry keyEntry : validKeyEntries)
                {
                    if (sha256Fingerprint.equals(keyEntry.getSHA256Fingerprint())) {
                        // The selected key is still in the list. We are done
                        possibleKey = keyEntry;

                        break;
                    }
                }

                if (possibleKey != null)
                {
                    if (possibleKey.getPrivateKey() != null) {
                        signingKey = possibleKey;
                    }
                    else {
                        // Should normally not happen since signingKeySelector should only return
                        // entries with a private key alias.
                        logger.warn("Private key of selected PGP signing key for {} is no longer available.",
                                userPreferences.getName());
                    }
                }
                else {
                    logger.warn("Selected PGP signing key for {} is no longer valid or no longer exists.",
                            userPreferences.getName());
                }
            }

            if (signingKey == null)
            {
                // There was no signing key selected so select the first from the list
                signingKey = validKeyEntries.iterator().next();

                // Key should always be non-null but to be on the safe side
                if (signingKey != null) {
                    // Store the new fingerprint
                    pgpProperties.setPGPSigningKey(signingKey.getSHA256Fingerprint());
                }
            }
        }
        else {
            logger.debug("There are no valid PGP signing keys for {}", userPreferences.getName());
        }

        if (signingKey == null && StringUtils.isNotEmpty(sha256Fingerprint)) {
            // There was no longer any valid signing key but the fingerprint was set, we should
            // therefore have to nullify the fingerprint
            logger.warn("Selected PGP signing key for {} is no longer valid or no longer exists",
                    userPreferences.getName());

            pgpProperties.setPGPSigningKey(null);
        }

        return signingKey;
    }
}
