/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import com.ciphermail.core.common.util.ByteArray;
import com.ciphermail.core.common.util.ColorUtils;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.awt.*;
import java.util.Collection;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper functions for setting and getting properties from HierarchicalProperties.
 *
 * @author Martijn Brinkers
 *
 */
public class HierarchicalPropertiesUtils
{
    private static final Logger logger = LoggerFactory.getLogger(HierarchicalPropertiesUtils.class);

    private HierarchicalPropertiesUtils() {
        // empty on purpose
    }

    /**
     * Copies all properties from source to target. Property names are matched against skipPattern and if they match
     * the property will be skipped (ie not copied).
     */
    public static void copyProperties(HierarchicalProperties source, @Nonnull HierarchicalProperties target,
            Pattern skipPattern)
    throws HierarchicalPropertiesException
    {
        if (source == null) {
            return;
        }

        Collection<String> names = source.getProperyNames(true);

        for (String name : names)
        {
            Matcher matcher = null;

            if (skipPattern != null) {
                matcher = skipPattern.matcher(name);
            }

            if (matcher == null || !matcher.matches())
            {
                String value = source.getProperty(name);

                target.setProperty(name, value);
            }
        }
    }

    /**
     * Returns the property with propertyName from properties if it exists. Returns defaultValue if property
     * does not exist.
     */
    public static String getProperty(@Nonnull HierarchicalProperties properties, @Nonnull String propertyName,
            String defaultValue)
    throws HierarchicalPropertiesException
    {
        String value = defaultValue;

        String property = properties.getProperty(propertyName);

        if (property != null) {
            value = property;
        }

        return value;
    }

    /**
     * Sets the boolean property named propertyName.
     */
    public static void setBoolean(@Nonnull HierarchicalProperties properties, @Nonnull String propertyName,
            Boolean value)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(propertyName,  value != null ? value.toString() : null);
    }

    /**
     * Returns the Boolean property with propertyName from properties if it exists, true if property equals "true", false if
     * not equals to "true". Returns defaultValue if property does not exist.
     */
    public static Boolean getBoolean(@Nonnull HierarchicalProperties properties, @Nonnull String propertyName,
            Boolean defaultValue)
    throws HierarchicalPropertiesException
    {
        Boolean value = defaultValue;

        String property = properties.getProperty(propertyName);

        if (property != null) {
            value = Boolean.valueOf(property);
        }

        return value;
    }

    /**
     * Returns the Boolean property with propertyName from properties if it exists, true if property equals "true", false if
     * not equals to "true". Returns false if property does not exist.
     */
    public static boolean getBoolean(@Nonnull HierarchicalProperties properties, @Nonnull String propertyName)
    throws HierarchicalPropertiesException
    {
        return getBoolean(properties, propertyName, false);
    }

    /**
     * Sets the integer property named propertyName.
     */
    public static void setInteger(@Nonnull HierarchicalProperties properties, @Nonnull String propertyName,
            Integer value)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(propertyName, ObjectUtils.toString(value, (Supplier<String>) null));
    }

    /**
     * Returns the Integer property with propertyName from properties if it exists and is a valid integer.
     * Returns defaultValue if property does not exist or is not a valid integer.
     */
    public static Integer getInteger(@Nonnull HierarchicalProperties properties, @Nonnull String propertyName,
            Integer defaultValue)
    throws HierarchicalPropertiesException
    {
        Integer value = defaultValue;

        String property = properties.getProperty(propertyName);

        if (property != null)
        {
            try {
                value = Integer.parseInt(property);
            }
            catch (NumberFormatException e) {
                logger.error("Value of property {} is not a number: {}", propertyName, property);
            }
        }

        return value;
    }

    /**
     * Returns the Integer property with propertyName from properties if it exists and is a valid integer.
     * Returns 0 if property does not exist or is not a valid integer.
     */
    public static int getInteger(@Nonnull HierarchicalProperties properties, @Nonnull String propertyName)
    throws HierarchicalPropertiesException
    {
        return getInteger(properties, propertyName, 0);
    }

    /**
     * Sets the long property named propertyName.
     */
    public static void setLong(HierarchicalProperties properties, String propertyName, Long value)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(propertyName, ObjectUtils.toString(value, (Supplier<String>) null));
    }

    /**
     * Returns the Long property with propertyName from properties if it exists and is a valid long.
     * Returns defaultValue if property does not exist or is not a valid long.
     * @throws HierarchicalPropertiesException
     */
    public static Long getLong(HierarchicalProperties properties, String propertyName, Long defaultValue)
    throws HierarchicalPropertiesException
    {
        Long value = defaultValue;

        String property = properties.getProperty(propertyName);

        if (property != null)
        {
            try {
                value = Long.parseLong(property);
            }
            catch (NumberFormatException e) {
                logger.error("Value of property {} is not a number: {}", propertyName, property);
            }
        }

        return value;
    }

    /**
     * Sets the double property named propertyName.
     */
    public static void setDouble(HierarchicalProperties properties, String propertyName, Double value)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(propertyName, ObjectUtils.toString(value, (Supplier<String>) null));
    }

    /**
     * Returns the Double property with propertyName from properties if it exists and is a valid double.
     * Returns defaultValue if property does not exist or is not a valid double.
     * @throws HierarchicalPropertiesException
     */
    public static Double getDouble(HierarchicalProperties properties, String propertyName, Double defaultValue)
    throws HierarchicalPropertiesException
    {
        Double value = defaultValue;

        String property = properties.getProperty(propertyName);

        if (property != null)
        {
            try {
                value = Double.parseDouble(property);
            }
            catch (NumberFormatException e) {
                logger.error("Value of property {} is not a number: {}", propertyName, property);
            }
        }

        return value;
    }

    /**
     * Sets the float property named propertyName.
     */
    public static void setFloat(HierarchicalProperties properties, String propertyName, Float value)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(propertyName, ObjectUtils.toString(value, (Supplier<String>) null));
    }

    /**
     * Returns the Float property with propertyName from properties if it exists and is a valid float.
     * Returns defaultValue if property does not exist or is not a valid float.
     * @throws HierarchicalPropertiesException
     */
    public static Float getFloat(HierarchicalProperties properties, String propertyName, Float defaultValue)
    throws HierarchicalPropertiesException
    {
        Float value = defaultValue;

        String property = properties.getProperty(propertyName);

        if (property != null)
        {
            try {
                value = Float.parseFloat(property);
            }
            catch (NumberFormatException e) {
                logger.error("Value of property {} is not a number: {}", propertyName, property);
            }
        }

        return value;
    }

    /**
     * Returns the Long property with propertyName from properties if it exists and is a valid long.
     * Returns 0 if property does not exist or is not a valid long.
     * @throws HierarchicalPropertiesException
     */
    public static long getLong(HierarchicalProperties properties, String propertyName)
    throws HierarchicalPropertiesException
    {
        return getLong(properties, propertyName, 0L);
    }

    /**
     * Sets property to a ByteArray. The underlying bytes are base64 encoded.
     * @throws HierarchicalPropertiesException
     */
    public static void setByteArray(HierarchicalProperties properties, String propertyName, ByteArray value)
    throws HierarchicalPropertiesException
    {
        String base64 = null;

        if (value != null) {
            base64 = MiscStringUtils.toStringFromASCIIBytes(Base64.encodeBase64(value.getBytes()));
        }

        properties.setProperty(propertyName, base64);
    }

    /**
     * Gets a property as a ByteArray. The underlying bytes are base64 decoded.
     * String. Returns defaultValue if property does not exist.
     * @throws HierarchicalPropertiesException
     */
    public static ByteArray getByteArray(HierarchicalProperties properties, String propertyName, ByteArray defaultValue)
    throws HierarchicalPropertiesException
    {
        ByteArray value = defaultValue;

        String property = properties.getProperty(propertyName);

        if (property != null) {
            value = new ByteArray(Base64.decodeBase64(MiscStringUtils.getBytesASCII(property)));
        }

        return value;
    }

    /**
     * Sets the color property named propertyName.
     */
    public static void setColor(HierarchicalProperties properties, String propertyName, Color value)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(propertyName,  ColorUtils.toString(value));
    }

    /**
     * Returns the Color property with propertyName from properties if it exists and is a valid Color.
     * Returns defaultValue if property does not exist or is not a valid Color.
     * @throws HierarchicalPropertiesException
     */
    public static Color getColor(HierarchicalProperties properties, String propertyName, Color defaultValue)
    throws HierarchicalPropertiesException
    {
        Color value = defaultValue;

        String property = properties.getProperty(propertyName);

        if (property != null)
        {
            try {
                value = ColorUtils.toColor(property);
            }
            catch (IllegalArgumentException e) {
                logger.error("Value of property {} is not a Color: {}", propertyName, property);
            }
        }

        return value;
    }

    /**
     * Sets the enum property
     */
    public static void setEnum(HierarchicalProperties properties, String propertyName, Enum<?> value)
    throws HierarchicalPropertiesException
    {
        properties.setProperty(propertyName, value != null ? value.name() : null);
    }

    /**
     * Gets the enum property
     */
    public static <E extends Enum<E>> E getEnum(HierarchicalProperties properties, String propertyName,
            Class<E> enumClass, E defaultValue)
    throws HierarchicalPropertiesException
    {
        return EnumUtils.getEnum(enumClass, getProperty(properties, propertyName, null), defaultValue);
    }

    public static <E extends Enum<E>> E getEnum(HierarchicalProperties properties, String propertyName,
            Class<E> enumClass)
    throws HierarchicalPropertiesException
    {
        return getEnum(properties, propertyName, enumClass, null);
    }
}
