/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.AbstractConverter;
import org.apache.commons.beanutils.converters.NumberConverter;
import org.apache.commons.beanutils.converters.StringConverter;

import javax.annotation.Nonnull;

public class BeanUtilsBeanBuilderImpl implements BeanUtilsBeanBuilder
{
    private final ConvertUtilsBean convertUtilsBean;

    /*
     * Extension of NumberConverter which throws an exception if conversion fails but which return null on empty values
     */
    private abstract static class NullDefaultNumberConverter extends NumberConverter
    {
        NullDefaultNumberConverter(boolean allowDecimals) {
            super(allowDecimals);
        }

        @Override
        protected <T> T handleMissing(final Class<T> type) {
            return null;
        }
    }

    /*
     * Boolean converter which throws an exception if conversion fails but which return null on empty values
     */
    private static class NullDefaultBooleanConverter extends AbstractConverter
    {
        /*
         * The set of strings that are known to map to Boolean.TRUE.
         */
        private final String[] trueStrings = {"true", "yes", "y", "on", "1"};

        /*
         * The set of strings that are known to map to Boolean.FALSE.
         */
        private final String[] falseStrings = {"false", "no", "n", "off", "0"};

        @Override
        protected <T> T convertToType(Class<T> type, Object value)
        {
            if (Boolean.class.equals(type) || Boolean.TYPE.equals(type))
            {
                String stringValue = value.toString().trim().toLowerCase();

                if (!stringValue.isEmpty())
                {
                    for (String trueString : trueStrings) {
                        if (trueString.equals(stringValue)) {
                            return type.cast(Boolean.TRUE);
                        }
                    }

                    for (String falseString : falseStrings) {
                        if (falseString.equals(stringValue)) {
                            return type.cast(Boolean.FALSE);
                        }
                    }
                }
                else {
                    return handleMissing(type);
                }
            }

            throw conversionException(type, value);
        }

        @Override
        protected Class<?> getDefaultType() {
            return Boolean.class;
        }

        @Override
        protected <T> T handleMissing(final Class<T> type) {
            return null;
        }
    }

    static class ByteArrayConverter extends AbstractConverter
    {
        public ByteArrayConverter() {
            setDefaultValue(null);
        }

        @Override
        protected <T> T convertToType(Class<T> type, Object value)
        {
            if (ByteArray.class.equals(type)) {
                return type.cast(new ByteArray(Base64Utils.decode(value.toString())));
            }

            throw conversionException(type, value);
        }

        @Override
        protected Class<?> getDefaultType() {
            return ByteArray.class;
        }

        @Override
        protected <T> T handleMissing(final Class<T> type) {
            return null;
        }
    }

    public BeanUtilsBeanBuilderImpl()
    {
        /*
         * ConvertUtilsBean extension which converts string to enum and which comes without any default converters
         */
        convertUtilsBean = new ConvertUtilsBean()
        {
            @SuppressWarnings("unchecked")
            @Override
            public Object convert(String value, Class clazz) {
                return clazz.isEnum() && value != null ? Enum.valueOf(clazz, value) : super.convert(value, clazz);
            }

            @Override
            public void deregister() {
                // disable all default converters
            }
        };

        // Register converters for native types to use null on empty or invalid values
        Converter converter = new StringConverter();
        convertUtilsBean.register(converter, String.class);

        converter = new NullDefaultBooleanConverter();
        convertUtilsBean.register(converter, Boolean.TYPE);
        convertUtilsBean.register(converter, Boolean.class);

        converter = new NullDefaultNumberConverter(false)
        {
            @Override
            protected Class<?> getDefaultType() {
                return Integer.class;
            }
        };
        convertUtilsBean.register(converter, Integer.TYPE);
        convertUtilsBean.register(converter, Integer.class);

        converter = new NullDefaultNumberConverter(false)
        {
            @Override
            protected Class<?> getDefaultType() {
                return Long.class;
            }
        };
        convertUtilsBean.register(converter, Long.TYPE);
        convertUtilsBean.register(converter, Long.class);

        converter = new NullDefaultNumberConverter(true)
        {
            @Override
            protected Class<?> getDefaultType() {
                return Float.class;
            }
        };
        convertUtilsBean.register(converter, Float.TYPE);
        convertUtilsBean.register(converter, Float.class);

        converter = new NullDefaultNumberConverter(true)
        {
            @Override
            protected Class<?> getDefaultType() {
                return Double.class;
            }
        };
        convertUtilsBean.register(converter, Double.TYPE);
        convertUtilsBean.register(converter, Double.class);

        // add converter from base64 to ByteArray
        converter = new ByteArrayConverter();

        convertUtilsBean.register(converter, ByteArray.class);
    }

    @Override
    @Nonnull public BeanUtilsBean createBeanUtilsBean() {
        return new BeanUtilsBean(convertUtilsBean);
    }

    public void registerConverter(Converter converter, Class<?> clazz) {
        convertUtilsBean.register(converter, clazz);
    }
}
