/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.keystore.BasicKeyStore;

import javax.annotation.Nonnull;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.CertStoreException;
import java.util.Collection;

public interface KeyAndCertStore extends X509CertStoreExt, BasicKeyStore
{
    /**
     * Add the keyAndCertificate to the underlying store. Returns the entry of the added item or if the entry
     * already exists, the existing item will be returned. If the entry already exists but there is no
     * associated private key but the new item does contain a private key, the private key will be imported and the
     * key alias will be set. If the entry already exists but the key alias of the new item is different (not likely),
     * the private key alias of the existing entry will be replaced by the new private key alias and the new private
     * key will be imported (note that the replaced private key will not be deleted)
     */
    @Nonnull X509CertStoreEntry addKeyAndCertificate(@Nonnull KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException;

    /**
     * Returns a KeyAndCertificate associated with the certStoreEntry. If the certStoreEntry has an
     * associates key entry and the key exists the key is returned.
     */
    KeyAndCertificate getKeyAndCertificate(@Nonnull X509CertStoreEntry certStoreEntry)
    throws CertStoreException, KeyStoreException;

    /**
     * Synchronizes the CertStore and/or the KeyStore. Certificates from the KeyStore are placed in the
     * CertStore and the key alias will be set. A check is done on certificates from the CertStore with a
     * key alias whether the key is still available in the KeyStore.
     *
     * The sync method can for example be used when an external KeyStore (for example an HSM) is attached.
     */
    void sync(@Nonnull SyncMode syncMode)
    throws KeyStoreException, CertStoreException;

    /**
     * Overrides BasicKeyStore.getMatchingKeys to return collection extending PrivateKey
     * @see BasicKeyStore
     */
    @Override
    Collection<? extends PrivateKey> getMatchingKeys(@Nonnull KeyIdentifier keyIdentifier)
    throws KeyStoreException;

    /**
     * Return a bounded collection extending PrivateKey matching the KeyIdentifier
     */
    Collection<? extends PrivateKey> getMatchingKeys(@Nonnull KeyIdentifier keyIdentifier, Integer firstResult,
            Integer maxResults)
    throws KeyStoreException;
}
