/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.trustlist;

import org.bouncycastle.openpgp.PGPPublicKey;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * A PGP trust List contains entries that determine whether a PGP key is valid or not.
 *
 * @author Martijn Brinkers
 *
 */
public interface PGPTrustList
{
    /**
     * A PGP trust list has a certain name.
     */
    String getName();

    /**
     * Returns all the CTL entries.
     */
    List<? extends PGPTrustListEntry> getEntries(Integer firstResult, Integer maxResults)
    throws PGPTrustListException;

    /**
     * Returns the number of CTL entries.
     */
    long size()
    throws PGPTrustListException;

    /**
     * Removes all entries
     */
    void deleteAll()
    throws PGPTrustListException;

    /**
     * Return the entry with the given fingerprint. Returns null if there is no entry with the given fingerprint.
     */
    PGPTrustListEntry getEntry(@Nonnull String sha256Fingerprint)
    throws PGPTrustListException;

    /**
     * Return the entry with the given PGP public key. Returns null if there is no entry.
     */
    PGPTrustListEntry getEntry(@Nonnull PGPPublicKey publicKey)
    throws PGPTrustListException;

    /**
     * Creates a new entry. The entry is only created but not added.
     */
    PGPTrustListEntry createEntry(@Nonnull String sha256Fingerprint)
    throws PGPTrustListException;

    /**
     * Creates a new entry. The entry is only created but not added.
     */
    PGPTrustListEntry createEntry(@Nonnull PGPPublicKey publicKey)
    throws PGPTrustListException;

    /**
     * Sets the entry. If the entry already exists, it will be overwritten with the new value. If includeSubkeys is
     * true and the key identified by entry has any sub keys, those sub keys will also be added to the trust list with
     * the same settings as the master entry.
     */
    void setEntry(@Nonnull PGPTrustListEntry entry, boolean includeSubkeys)
    throws PGPTrustListException;

    /**
     * Removes the entry. If includeSubkeys is true and the key identified by entry has any sub keys, those sub keys
     * will also be deleted from the trust list.
     */
    void deleteEntry(@Nonnull PGPTrustListEntry entry, boolean includeSubkeys)
    throws PGPTrustListException;

    /**
     * Returns the status of the public key. Whether a public key is valid depends
     * on whether an entry is found for the key and on the settings of the
     * entry.
     */
    PGPTrustListValidityResult checkValidity(@Nonnull PGPPublicKey publicKey)
    throws PGPTrustListException;
}
