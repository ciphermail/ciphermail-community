/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.crlstore.CRLStoreException;

import javax.annotation.Nonnull;
import java.security.cert.CRL;
import java.util.Collection;

/**
 * This CRL maintainer add CRLs to the CRL store and keeps the CRL store as up-to-date as possible by
 * removing CRLs that are superseded by new CRLs. You could think of CRLStoreMaintainer as some kind
 * of 'filter' between the caller and the CRL store to make sure that only new CRLs are added and
 * superseded CRLs are removed.
 *
 * It depends on the implementation whether CRLs that are added are checked for trust etc. The default
 * implementation (CRLStoreMaintainerImpl) checks the trust of CRLs.
 */
public interface CRLStoreMaintainer
{
    /**
     * Adds the CRLs to the store if these CRLs are new or newer than existing CRLs. It returns the
     * number of CRLs that were added to the store.
     */
    int addCRLs(@Nonnull Collection<? extends CRL> crls);

    /**
     * Adds the CRL to the store if the CRL is new or newer than existing CRLs. It returns true
     * if the CRL was added to the store.
     */
    boolean addCRL(@Nonnull CRL crl)
    throws CRLStoreException;
}
