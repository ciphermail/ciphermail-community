/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore.jce;

import java.security.InvalidAlgorithmParameterException;
import java.security.cert.CRL;
import java.security.cert.CRLSelector;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertStoreSpi;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.LinkedList;

import com.ciphermail.core.common.security.certstore.BasicCertStore;
import com.ciphermail.core.common.security.crlstore.BasicCRLStore;
import com.ciphermail.core.common.security.crlstore.CRLStoreException;

/**
 * X509CertStoreProvider creates a CertStoreSpi wrapper over a X509CertStoreExtension so a X509CertStoreExtension
 * can be uses as a CertStore.
 *
 * @author Martijn Brinkers
 *
 */
public class X509CertStoreProvider extends CertStoreSpi
{
    private final BasicCertStoreParameters certStoreParameters;

    public X509CertStoreProvider(CertStoreParameters certStoreParameters)
    throws InvalidAlgorithmParameterException
    {
        super(certStoreParameters);

        if (!(certStoreParameters instanceof BasicCertStoreParameters)) {
            throw new InvalidAlgorithmParameterException("params must be a BasicCertStoreParameters.");
        }

        this.certStoreParameters = (BasicCertStoreParameters) certStoreParameters;
    }

    @Override
    public Collection<? extends CRL> engineGetCRLs(CRLSelector crlSelector)
    throws CertStoreException
    {
        BasicCRLStore store = certStoreParameters.getCRLStore();

        try {
            return (store != null ? store.getCRLs(crlSelector) :
                        new LinkedList<X509CRL>());
        }
        catch (CRLStoreException e) {
            throw new CertStoreException(e);
        }
    }

    @Override
    public Collection<? extends Certificate> engineGetCertificates(CertSelector certSelector)
    throws CertStoreException
    {
        BasicCertStore store = certStoreParameters.getCertStore();

        return (store != null ? store.getCertificates(certSelector) :
                    new LinkedList<X509Certificate>());
    }
}
