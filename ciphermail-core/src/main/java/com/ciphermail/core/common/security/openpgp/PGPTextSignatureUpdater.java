/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.bouncycastle.openpgp.PGPSignatureGenerator;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Updates PGPSignatureGenerator with new lines
 *
 * @author Martijn Brinkers
 *
 */
public class PGPTextSignatureUpdater
{
    private static final byte[] CRLF = new byte[]{'\r', '\n'};

    /*
     * Generates the actual signature
     */
    private final PGPSignatureGenerator signatureGenerator;

    /*
     * True if the line being added is the first line
     */
    private boolean first = true;

    public PGPTextSignatureUpdater(@Nonnull PGPSignatureGenerator signatureGenerator) {
        this.signatureGenerator = Objects.requireNonNull(signatureGenerator);
    }

    public void addLine(byte[] line)
    {
        if (!first)
        {
            // add CR/LF before a new line but not for the first line
            signatureGenerator.update(CRLF);
        }

        first = false;

        // Trailing whitespace must be removed for signature calculation (see RFC 4880 7.1).
        //
        // "Also, any trailing whitespace -- spaces (0x20) and tabs (0x09) -- at
        // the end of any line is removed when the cleartext signature is
        // generated."
        signatureGenerator.update(line, 0, PGPUtils.getLengthWithoutTrailingWhitespace(line));
    }
}
