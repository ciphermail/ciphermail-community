/*
 * Copyright (c) 2013-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MimeMessageWithID;
import com.ciphermail.core.common.mail.matcher.ContentHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.NotHeaderNameMatcher;
import com.ciphermail.core.common.util.DirectAccessByteArrayOutputStream;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.activation.DataHandler;
import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.util.Collection;

/**
 * PGP encrypts a MimeMessage
 *
 * @author Martijn Brinkers
 *
 */
public class PGPMIMEEncryptionBuilder
{
    /*
     * The compression algorithm to use
     */
    private PGPCompressionAlgorithm compressionAlgorithm = PGPCompressionAlgorithm.ZLIB;

    /*
     * The encryption algorithm to use
     */
    private PGPEncryptionAlgorithm encryptionAlgorithm = PGPEncryptionAlgorithm.AES_128;

    /*
     * The filename used for the literal compressed packet
     */
    private String compressedFilename = "";

    /*
     * The filename used for the literal data when no compression is used
     */
    private String literalFilename = "";

    /*
     * If true, an integrity packet will be added
     */
    private boolean addIntegrityPacket = true;

    /*
     * If true, the message id of the source email will be retained
     */
    private boolean retainMessageID = true;

    /*
     * If true, the "OpenPGP encrypted message" blob will be attached with content disposition inline. Enigmail
     * set the content disposition to inline. Also Mailvelope (PGP browser plugin) will only support PGP/MIME if
     * the content disposition is set to inline.
     */
    private boolean dispositionInline = true;

    /*
     * If true, session keys will be obfuscated.
     */
    private boolean sessionKeyObfuscation;

    public MimeMessage encrypt(@Nonnull MimeMessage message, @Nonnull Collection<PGPPublicKey> encryptionKeys)
    throws IOException, MessagingException, PGPException
    {
        PGPEncryptionBuilder encryptionBuilder = new PGPEncryptionBuilder();

        encryptionBuilder.setAddIntegrityPacket(addIntegrityPacket);
        encryptionBuilder.setCompressionAlgorithm(compressionAlgorithm);
        encryptionBuilder.setEncryptionAlgorithm(encryptionAlgorithm);
        encryptionBuilder.setCompressedFilename(compressedFilename);
        encryptionBuilder.setLiteralFilename(literalFilename);
        encryptionBuilder.setSessionKeyObfuscation(sessionKeyObfuscation);

        DirectAccessByteArrayOutputStream mime = new DirectAccessByteArrayOutputStream();

        BodyPartUtils.writeMIMEBodyPart(message, new ContentHeaderNameMatcher(), mime);

        // create a new buffer because it cannot overlap with mime buffer
        DirectAccessByteArrayOutputStream encrypted = new DirectAccessByteArrayOutputStream();

        encryptionBuilder.encrypt(mime.getInputStream(), encrypted, encryptionKeys);

        MimeBodyPart identificationPart = new MimeBodyPart();

        identificationPart.setDataHandler(new DataHandler(new ByteArrayDataSource("Version: 1\r\n",
                "application/pgp-encrypted")));
        identificationPart.setDescription("PGP/MIME version identification");

        MimeBodyPart encryptedPart = new MimeBodyPart();

        encryptedPart.setDataHandler(new DataHandler(new ByteArrayDataSource(encrypted.getInputStream(),
                "application/octet-stream; name=\"encrypted.asc\"")));
        encryptedPart.setDescription("OpenPGP encrypted message");
        encryptedPart.setDisposition((dispositionInline ? "inline" : "attachment") + "; filename=\"encrypted.asc\"");

        Multipart mp = new MimeMultipart("encrypted; protocol=\"application/pgp-encrypted\"");

        mp.addBodyPart(identificationPart);
        mp.addBodyPart(encryptedPart);

        MimeMessage encryptedMessage = retainMessageID ? new MimeMessageWithID(MailSession.getDefaultSession(),
                message.getMessageID()) : new MimeMessage(MailSession.getDefaultSession());

        encryptedMessage.setContent(mp);

        HeaderMatcher contentMatcher = new ContentHeaderNameMatcher();

        // create a matcher that matches on everything expect content-*
        HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);

        // copy all non-content headers from source message to the new message
        HeaderUtils.copyHeaders(message, encryptedMessage, nonContentMatcher);

        encryptedMessage.saveChanges();

        return encryptedMessage;
    }

    public PGPEncryptionAlgorithm getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }

    public void setEncryptionAlgorithm(PGPEncryptionAlgorithm encryptionAlgorithm) {
        this.encryptionAlgorithm = encryptionAlgorithm;
    }

    public PGPCompressionAlgorithm getCompressionAlgorithm() {
        return compressionAlgorithm;
    }

    public void setCompressionAlgorithm(PGPCompressionAlgorithm compressionAlgorithm) {
        this.compressionAlgorithm = compressionAlgorithm;
    }

    public String getCompressedFilename() {
        return compressedFilename;
    }

    public void setCompressedFilename(String compressedFilename) {
        this.compressedFilename = compressedFilename;
    }

    public String getLiteralFilename() {
        return literalFilename;
    }

    public void setLiteralFilename(String literalFilename) {
        this.literalFilename = literalFilename;
    }

    public boolean isAddIntegrityPacket() {
        return addIntegrityPacket;
    }

    public void setAddIntegrityPacket(boolean addIntegrityPacket) {
        this.addIntegrityPacket = addIntegrityPacket;
    }

    public boolean isRetainMessageId() {
        return retainMessageID;
    }

    public void setRetainMessageId(boolean retainMessageID) {
        this.retainMessageID = retainMessageID;
    }

    public boolean isDispositionInline() {
        return dispositionInline;
    }

    public void setDispositionInline(boolean dispositionInline) {
        this.dispositionInline = dispositionInline;
    }

    public boolean isSessionKeyObfuscation() {
        return sessionKeyObfuscation;
    }

    public void setSessionKeyObfuscation(boolean sessionKeyObfuscation) {
        this.sessionKeyObfuscation = sessionKeyObfuscation;
    }
}
