/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


/**
 * Builds QR code images from the input
 */
public class QRBuilder
{
    private static final Logger logger = LoggerFactory.getLogger(QRBuilder.class);

    /*
     * Width (in pixels) of the QR code image
     */
    private int width = 200;

    /*
     * Height (in pixels) of the QR code image
     */
    private int height = 200;

    /*
     * Background color of the QR code image
     */
    private Color backgroundColor = Color.WHITE;

    /*
     * Cell color of the QR code image
     */
    private Color cellColor = Color.BLACK;

    /*
     * The image format of the QR code (png, jpg etc.)
     */
    private String imageFormat = "png";

    public byte[] buildQRCode(@Nonnull String content)
    throws IOException
    {
        logger.debug("Creating QR code {}", content);

        try {
            QRCodeWriter writer = new QRCodeWriter();

            BitMatrix matrix = writer.encode(content, BarcodeFormat.QR_CODE, width, height);

            width = matrix.getWidth();
            height = matrix.getHeight();

            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

            Graphics2D graphics = image.createGraphics();

            graphics.setColor(backgroundColor);
            graphics.fillRect(0, 0, width, height);

            graphics.setColor(cellColor);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (matrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            try {
                ImageIO.write(image, imageFormat, bos);
            }
            finally {
                bos.close();
            }

            return bos.toByteArray();
        }
        catch (WriterException e) {
            throw new IOException(e);
        }
    }

    public int getWidth() {
        return width;
    }

    public QRBuilder setWidth(int width)
    {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public QRBuilder setHeight(int height)
    {
        this.height = height;
        return this;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public QRBuilder setBackgroundColor(Color backgroundColor)
    {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public Color getCellColor() {
        return cellColor;
    }

    public QRBuilder setCellColor(Color cellColor)
    {
        this.cellColor = cellColor;
        return this;
    }

    public String getImageFormat() {
        return imageFormat;
    }

    public QRBuilder setImageFormat(String imageFormat)
    {
        this.imageFormat = imageFormat;
        return this;
    }
}
