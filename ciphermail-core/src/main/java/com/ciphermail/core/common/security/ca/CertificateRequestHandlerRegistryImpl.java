/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Default implementation of CertificateRequestHandlerRegistry.
 *
 * @author Martijn Brinkers
 *
 */
public class CertificateRequestHandlerRegistryImpl implements CertificateRequestHandlerRegistry
{
    private final Map<String, CertificateRequestHandler> handlers = new LinkedHashMap<>();

    @Override
    public synchronized @Nonnull List<CertificateRequestHandler> getHandlers() {
        return new ArrayList<>(handlers.values());
    }

    @Override
    public synchronized CertificateRequestHandler getHandler(String name) {
        return handlers.get(StringUtils.lowerCase(name));
    }

    @Override
    public synchronized void registerHandler(@Nonnull CertificateRequestHandler handler) {
        handlers.put(StringUtils.lowerCase(handler.getCertificateHandlerName()), handler);
    }

    @Override
    public synchronized boolean removeHandler(String name) {
        return handlers.remove(name) != null;
    }
}
