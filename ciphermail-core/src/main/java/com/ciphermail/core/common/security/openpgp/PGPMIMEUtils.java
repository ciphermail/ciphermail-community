/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.apache.commons.lang.ArrayUtils;

import javax.annotation.Nonnull;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;
import java.io.IOException;

/**
 * PGP mime utility methods
 *
 * @author Martijn Brinkers
 *
 */
public class PGPMIMEUtils
{
    /**
     * Content type for a PGP public keyring
     */
    public static final String PGP_PUBLIC_KEYRING_CONTENT_TYPE = "application/pgp-keys";

    /**
     * Non official content type for a PGP private keyring
     */
    public static final String PGP_SECRET_KEYRING_CONTENT_TYPE = "application/pgp-secret-keys";

    private PGPMIMEUtils() {
        // empty on purpose
    }

    /**
     * Returns the PGPMimeType from the contentType. Returns null if the PGPMimeType cannot be detected.
     *
     * See RFC 3156 for more information on PGP/MIME content types
     */
    public static PGPMIMEType getPGPMIMEType(@Nonnull String contentTypeHeader)
    throws ParseException
    {
        return getPGPMIMEType(new ContentType(contentTypeHeader));
    }

    /**
     * Returns the PGPMimeType from the contentType. Returns null if the PGPMimeType cannot be detected.
     *
     * See RFC 3156 for more information on PGP/MIME content types
     */
    public static PGPMIMEType getPGPMIMEType(@Nonnull ContentType contentType)
    {
        String primaryType = contentType.getPrimaryType();
        String subType  = contentType.getSubType();
        String protocol = contentType.getParameter("protocol");

        if (!"multipart".equalsIgnoreCase(primaryType)) {
            return null;
        }

        if ("encrypted".equalsIgnoreCase(subType) && "application/pgp-encrypted".equalsIgnoreCase(protocol)) {
            return PGPMIMEType.ENCRYPTED;
        }

        if ("signed".equalsIgnoreCase(subType) && "application/pgp-signature".equalsIgnoreCase(protocol)) {
            return PGPMIMEType.SIGNED;
        }

        return null;
    }

    /**
     * Returns the PGPMimeType from the content-type of the part. Returns null if the PGPMimeType cannot be detected.
     *
     * See RFC 3156 for more information on PGP/MIME content types
     */
    public static PGPMIMEType getPGPMIMEType(@Nonnull Part part, boolean enablePGPUniversalWorkaround)
    throws MessagingException, IOException
    {
        ContentType contentType = new ContentType(part.getContentType());

        PGPMIMEType type = null;

        // A user reported that a message created with PGP universal gateway seems to encode PGP/MIME with
        // multipart/mixed and add a header x-pgp-encoding-format: MIME to indicate that the message was PGP/MIME.
        // With our own tests we could not replicate this but this workaround will try to detect this situation and
        // work around it.
        if (enablePGPUniversalWorkaround)
        {
            String primaryType = contentType.getPrimaryType();
            String subType  = contentType.getSubType();

            if ("multipart".equalsIgnoreCase(primaryType) && "mixed".equalsIgnoreCase(subType))
            {
                String[] encoding = part.getHeader("x-pgp-encoding-format");

                if (!ArrayUtils.isEmpty(encoding))
                {
                    // Only support the first header
                    if ("MIME".equalsIgnoreCase(encoding[0]))
                    {
                        // The message is PGP universal MIME encoded. Now we need to detect whether it is signed or
                        // encrypted. PGP/MIME encrypted message should contain an application/pgp-encrypted part with
                        // the version
                        Multipart multipart = (Multipart) part.getContent();

                        // Now check if there is a version part
                        for (int i = 0; i < multipart.getCount(); i++)
                        {
                            if (multipart.getBodyPart(i).isMimeType("application/pgp-encrypted"))
                            {
                                type = PGPMIMEType.ENCRYPTED;

                                break;
                            }
                        }

                        if (type == null) {
                            type = PGPMIMEType.SIGNED;
                        }
                    }
                }
            }
        }

        if (type == null) {
            type = getPGPMIMEType(part.getContentType());
        }

        return type;
    }

    /**
     * Returns the signed part (first element in the array) and the signature part. Throws MessagingException if
     * the signed or signature part is not found or if the multipart does not contain two parts.
     */
    public static BodyPart[] dissectPGPMIMESigned(@Nonnull Multipart multipart)
    throws MessagingException
    {
        // There should be two parts, one signed part and one signature part
        if (multipart.getCount() != 2) {
            throw new MessagingException("Multipart contains " + multipart.getCount() + " parts.");
        }

        BodyPart signedPart = null;
        BodyPart signaturePart = null;

        for (int i = 0; i < 2; i++)
        {
            BodyPart part = multipart.getBodyPart(i);

            if (part.isMimeType("application/pgp-signature")) {
                signaturePart = part;
            }
            else {
                signedPart = part;
            }
        }

        if (signedPart == null) {
            throw new MessagingException("PGP/MIME signed part not found.");
        }

        if (signaturePart == null) {
            throw new MessagingException("PGP/MIME signature part not found.");
        }

        return new BodyPart[]{signedPart, signaturePart};
    }
}
