/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.asn1;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.util.ASN1Dump;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.security.cert.X509Extension;
import java.util.Enumeration;

public class ASN1Utils
{
    private ASN1Utils() {
        // empty on purpose
    }

    public static void dump(AttributeTable attributeTable, @Nonnull StringBuilder sb)
    {
        if (attributeTable == null) {
            return;
        }

        ASN1EncodableVector vector = attributeTable.toASN1EncodableVector();

        for (int i = 0; i < vector.size(); i++)
        {
            ASN1Encodable der = vector.get(i);

            sb.append(ASN1Dump.dumpAsString(der));
        }
    }

    public static String dump(AttributeTable attributeTable)
    {
        StringBuilder sb = new StringBuilder();

        dump(attributeTable, sb);

        return sb.toString();
    }

    public static ASN1Object getExtensionValue(X509Extension extension, String oid)
    throws IOException
    {
        if (extension == null) {
            return null;
        }

        byte[]  bytes = extension.getExtensionValue(oid);

        if (bytes == null)
        {
            return null;
        }

        return getObject(bytes);
    }

    private static ASN1Object getObject(@Nonnull byte[] extension)
    throws IOException
    {
        ASN1InputStream aIn = null;
        ASN1InputStream bIn = null;

        try {
            aIn = new ASN1InputStream(extension);
            ASN1OctetString octs = (ASN1OctetString)aIn.readObject();

            bIn = new ASN1InputStream(octs.getOctets());
            return bIn.readObject();
        }
        finally {
            IOUtils.closeQuietly(bIn);
            IOUtils.closeQuietly(aIn);
        }
    }

    public static ASN1EncodableVector toASN1EncodableVector(X500Principal principal)
    throws IOException
    {
        ASN1InputStream aIn = new ASN1InputStream(principal.getEncoded());

        try {
            ASN1Object der = aIn.readObject();

            Enumeration<?> e = ASN1Sequence.getInstance(der).getObjects();

            ASN1EncodableVector v = new ASN1EncodableVector();

            while (e.hasMoreElements())
            {
                Object o = e.nextElement();

                if (o instanceof ASN1Encodable asn1Encodable) {
                    v.add(asn1Encodable);
                }
            }

            return v;
        }
        finally {
            IOUtils.closeQuietly(aIn);
        }
    }
}
