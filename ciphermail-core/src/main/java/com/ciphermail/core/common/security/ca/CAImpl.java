/*
 * Copyright (c) 2009-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.KeyAndCertificateWithAdditonalCerts;
import com.ciphermail.core.common.security.ca.handlers.BuiltInCertificateRequestHandler;
import com.ciphermail.core.common.security.ca.hibernate.CertificateRequestEntity;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.DateTimeUtils;
import com.ciphermail.core.common.util.EventHandler;
import com.ciphermail.core.common.util.ThreadUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Default CA implementation.
 */
public class CAImpl implements CA
{
    private static final Logger logger = LoggerFactory.getLogger(CAImpl.class);

    /*
     * Time in milliseconds to sleep when an exception occurs in the thread
     */
    private static final int EXCEPTION_SLEEP_TIME = 10000;

    /*
     * If a certificate handler does not immediately issue a certificate, the request will be stored in the
     * CertificateRequestStore.
     */
    private final CertificateRequestStore certificateRequestStore;

    /*
     * The registry with all available CertificateRequestHandler's
     */
    private final CertificateRequestHandlerRegistry handlerRegistry;

    /*
     * Additional (can be null) resolver that can resolve certain parameters (for example the common name)
     * of a certificate request
     */
    private CertificateRequestResolver certificateRequestResolver;

    /*
     * Used for getting the signing certificate and to store the issued certificate
     */
    private final KeyAndCertStore keyAndCertStore;

    /*
     * Manages database transactions
     */
    private final TransactionOperations transactionOperations;

    /*
     * Manager for Hibernate sessions
     */
    private final SessionManager sessionManager;

    /*
     * Time in msec the certificate request background thread will wait until handling requests
     */
    private long threadStartupDelay = DateUtils.MILLIS_PER_SECOND * 30;

    /*
     * Sleep time (in seconds) when there is nothing to do. Can be pretty long (but you are advised to make it
     * shorter than expirationTime) because the thread will be 'kicked' when a new entry will be added.
     */
    private int threadSleepTime;

    /*
     * The maximum time in seconds a request will be kept
     */
    private int expirationTime;

    /*
     * The array with delay times used for updating nextUpdate.
     */
    private int[] delayTimes;

    /*
     * Is increased every time a RequestHandlerThread is created
     */
    private final AtomicInteger threadCounter = new AtomicInteger();

    /*
     * The thread that periodically the certificateRequestStore
     */
    private final RequestHandlerThread thread;

    /*
     * Event handlers for the certificate created event
     */
    private final EventHandler<CertificateCreatedEventHandler> certificateCreatedEventHandlers = new EventHandler<>();

    public CAImpl(
            @Nonnull CertificateRequestStore certificateRequestStore,
            @Nonnull CertificateRequestHandlerRegistry handlerRegistry,
            @Nonnull KeyAndCertStore keyAndCertStore,
            @Nonnull TransactionOperations transactionOperations,
            @Nonnull SessionManager sessionManager,
            int threadSleepTime,
            int expirationTime,
            @Nonnull int[] delayTimes)
    {
        this.certificateRequestStore = Objects.requireNonNull(certificateRequestStore);
        this.handlerRegistry = Objects.requireNonNull(handlerRegistry);
        this.keyAndCertStore = Objects.requireNonNull(keyAndCertStore);
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.threadSleepTime = threadSleepTime;
        this.expirationTime = expirationTime;
        this.delayTimes = Objects.requireNonNull(delayTimes);

        validateDelayTimes();

        thread = new RequestHandlerThread();
    }

    private void validateDelayTimes()
    {
        if (delayTimes.length == 0) {
            throw new IllegalArgumentException("Delay times must be specified.");
        }

        int prev = 0;

        for (int time : delayTimes)
        {
            if (time < prev) {
                throw new IllegalArgumentException("Delay times must be in increasing order.");
            }
            prev = time;
        }
    }

    public void start()
    {
        thread.setDaemon(true);
        thread.start();
    }

    public void kick() {
        thread.kick();
    }

    public void stop(long waitTime)
    throws InterruptedException
    {
        thread.requestStop();

        thread.join(waitTime);
    }

    private CertificateRequest toRequest(@Nonnull RequestParameters parameters, @Nonnull String handlerName)
    throws CAException
    {
        CertificateRequestEntity request = new CertificateRequestEntity(handlerName);

        String email = EmailAddressUtils.canonicalizeAndValidate(parameters.getEmail(), true);

        if (email == null) {
            throw new CAException(StringUtils.defaultString(parameters.getEmail()) + " is not a valid email address");
        }

        request.setSubject(parameters.getSubject());
        request.setEmail(email);
        request.setValidity(parameters.getValidity());
        request.setKeyLength(parameters.getKeyLength());
        request.setSignatureAlgorithm(parameters.getSignatureAlgorithm());
        request.setCRLDistributionPoint(parameters.getCRLDistributionPoint());

        return request;
    }

    private X509CertStoreEntry addKeyAndCertificate(String email, @Nonnull KeyAndCertificate keyAndCertificate)
    throws CAException
    {
        X509CertStoreEntry certStoreEntry;

        try {
            certStoreEntry = keyAndCertStore.addKeyAndCertificate(keyAndCertificate);

            // Check if additional certificates are available and if so import then as well
            if (keyAndCertificate instanceof KeyAndCertificateWithAdditonalCerts keyAndCertificateWithAdditonalCerts)
            {
                Collection<X509Certificate> additionalCertificates = keyAndCertificateWithAdditonalCerts.
                        getAdditionalCertificates();

                if (CollectionUtils.isNotEmpty(additionalCertificates))
                {
                    for (X509Certificate additionalCertificate : additionalCertificates)
                    {
                        if (!keyAndCertStore.contains(additionalCertificate)) {
                            keyAndCertStore.addCertificate(additionalCertificate);
                        }
                    }
                }
            }

            // Fire the certificate created event to all the handlers
            for (CertificateCreatedEventHandler handler : certificateCreatedEventHandlers.getEventHandlers())
            {
                try {
                    handler.certificateCreated(email, keyAndCertificate);
                }
                catch (Exception e) {
                    logger.error("Error Event#certificateCreated.", e);
                }
            }
        }
        catch (CertStoreException | KeyStoreException e) {
            throw new CAException(e);
        }

        return certStoreEntry;
    }

    private void setNextUpdate(@Nonnull CertificateRequest certificateRequest)
    {
        int index = certificateRequest.getIteration();

        // Use last delay if we have no more delay's left
        if (index > delayTimes.length - 1) {
            index = delayTimes.length - 1;
        }

        Date nextUpdate = DateUtils.addSeconds(new Date(), delayTimes[index]);

        logger.debug("Next update for {} set to {}", certificateRequest.getEmail(), nextUpdate);

        certificateRequest.setNextUpdate(nextUpdate);
    }

    @Override
    public @Nonnull RequestResponse requestCertificate(@Nonnull RequestParameters parameters)
    throws CAException
    {
        String handlerName = parameters.getCertificateRequestHandler();

        if (handlerName == null) {
            // Use the built-in certificate handler if no handler name was specified.
            handlerName = BuiltInCertificateRequestHandler.NAME;
        }

        logger.info("Requesting a certificate for user {} using handler {}", parameters.getEmail(), handlerName);

        CertificateRequestHandler handler = handlerRegistry.getHandler(handlerName);

        if (handler == null) {
            throw new CAException("CertificateRequestHandler with name " + handlerName + " not available");
        }

        CertificateRequest request = toRequest(parameters, handlerName);

        // If the certificateRequestResolver is set, resolve request parameters. For example, using an
        // LDAP certificateRequestResolver, the common name for the request can be resolved
        if (certificateRequestResolver != null)
        {
            try {
                certificateRequestResolver.resolve(parameters.getEmail(), request);
            }
            catch (CertificateRequestResolverException e) {
                throw new CAException(e);
            }
        }

        KeyAndCertificate keyAndCertificate = handler.handleRequest(request);

        X509CertStoreEntry certStoreEntry = null;

        if (keyAndCertificate != null)
        {
            // The certificate was issued immediately so handle it directly.
            certStoreEntry = addKeyAndCertificate(parameters.getEmail(), keyAndCertificate);
        }
        else {
            request.setLastUpdated(new Date());
            setNextUpdate(request);
            // The certificate was not issued immediately so the request will be scheduled.
            certificateRequestStore.addRequest(request);

            // wake up the thread
            thread.kick();
        }

        return new RequestResponse(request, certStoreEntry);
    }

    @Override
    public KeyAndCertificate handleRequest(@Nonnull X509Certificate certificate)
    throws CAException
    {
        KeyAndCertificate keyAndCertificate = null;

        CloseableIterator<? extends CertificateRequest> iterator = certificateRequestStore.getAllRequests();

        try {
            while(iterator.hasNext())
            {
                CertificateRequest request = iterator.next();

                if (request == null) {
                    continue;
                }

                CertificateRequestHandler handler = certificateRequestStore.getCertificateRequestHandler(request);

                if (handler == null) {
                    // A handler was not found. This can happen if the handler was removed but there were still
                    // requests in the pending certificate request store. Log and continue.
                    continue;
                }

                keyAndCertificate = handler.handleRequest(request, certificate);

                if (keyAndCertificate != null) {
                    // We found a request. The request is handled and therefore the certificate should be stored and the
                    // request should be removed. exit loop on first returned key and certificate. It's not likely that
                    // there will be other hits for other requests.
                    finishCertificateRequest(request, keyAndCertificate);

                    // Exit loop on first returned key and certificate. It's not likely that there will be other
                    // hits for other requests.
                    break;
                }
            }
        }
        catch (CloseableIteratorException e) {
            throw new CAException(e);
        }
        finally {
            CloseableIteratorUtils.closeQuietly(iterator);
        }

        return keyAndCertificate;
    }

    private void deleteRequest(CertificateRequest request) {
        certificateRequestStore.deleteRequest(request.getID());
    }

    private void finishCertificateRequest(@Nonnull CertificateRequest request,
            @Nonnull KeyAndCertificate keyAndCertificate)
    throws CAException
    {
        logger.info("A certificate for email {} was issued", request.getEmail());

        // The certificate was issued. We can add it and remove it from the certificateRequestStore
        addKeyAndCertificate(request.getEmail(), keyAndCertificate);
        deleteRequest(request);
    }

    /*
     * Note: this is called from the background daemon thread that handles requests from the pending requests store
     */
    private void handlePendingRequest(@Nonnull CertificateRequest request)
    throws CAException
    {
        CertificateRequestHandler handler = certificateRequestStore.getCertificateRequestHandler(request);

        if (handler == null) {
            // A handler was not found. We won't throw an exception because otherwise it will keep on throwing
            // exception is great succession. We will set the last message of the request.
            request.setLastMessage("A Certificate Request Handler with name " + request.getCertificateHandlerName() +
                    " was not found.");

            return;
        }

        KeyAndCertificate keyAndCertificate = handler.handleRequest(request);

        if (keyAndCertificate != null) {
            finishCertificateRequest(request, keyAndCertificate);
        }
        else {
            logger.debug("A certificate for email {} was not yet ready.", request.getEmail());
        }
    }

    /*
     * Note: this is called from the background daemon thread that handles requests from the pending requests store
     */
    private boolean isExpired(@Nonnull CertificateRequest request)
    {
        if (request.getCreated() == null)
        {
            // Should not happen.
            logger.warn("Created date is not set. The request will be expired.");

            return true;
        }

        long diff = DateTimeUtils.diffMilliseconds(new Date(), request.getCreated());

        return  diff > (expirationTime * DateUtils.MILLIS_PER_SECOND);
    }

    /*
     * Note: this is called from the background daemon thread that handles requests from the pending requests store
     */
    private boolean handlePendingRequest()
    {
        logger.debug("Check for pending request.");

        CertificateRequest request = certificateRequestStore.getNextRequest();

        if (request != null)
        {
            // Lock the entity to prevent the certificate request to be handled by a different server when using
            // a ciphermail cluster
            sessionManager.getSession().lock(request, LockMode.PESSIMISTIC_WRITE);

            if (!isExpired(request))
            {
                try {
                    handlePendingRequest(request);
                }
                catch (Exception t) {
                    // We won't throw an exception because otherwise it will keep on throwing exception in
                    // great succession. We will set the last message of the request.
                    request.setLastMessage("An error occurred handling the request. Message: " +
                            ExceptionUtils.getRootCauseMessage(t));

                    logger.error("An error occurred handling the request", t);
                }
                finally {
                    request.setLastUpdated(new Date());
                    request.setIteration(request.getIteration() + 1);

                    setNextUpdate(request);
                }

            }
            else {
                logger.warn("Certificate request for email {} expired. Request will be removed.", request.getEmail());

                deleteRequest(request);
            }
        }

        return request != null;
    }

    public CertificateRequestResolver getCertificateRequestResolver() {
        return certificateRequestResolver;
    }

    public void setCertificateRequestResolver(CertificateRequestResolver certificateRequestResolver) {
        this.certificateRequestResolver = certificateRequestResolver;
    }

    public long getThreadStartupDelay() {
        return threadStartupDelay;
    }

    public void setThreadStartupDelay(long threadStartupDelay) {
        this.threadStartupDelay = threadStartupDelay;
    }

    public int getThreadSleepTime() {
        return threadSleepTime;
    }

    public void setThreadSleepTime(int threadSleepTime) {
        this.threadSleepTime = threadSleepTime;
    }

    public int getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(int expirationTime) {
        this.expirationTime = expirationTime;
    }

    public int[] getDelayTimes() {
        return delayTimes;
    }

    public void setDelayTimes(int[] delayTimes) {
        this.delayTimes = delayTimes;
    }

    public void addCertificateCreatedEventHandler(CertificateCreatedEventHandler handler) {
        certificateCreatedEventHandlers.addEventHandler(handler);
    }

    public void removeCertificateCreatedEventHandler(CertificateCreatedEventHandler handler) {
        certificateCreatedEventHandlers.removeEventHandler(handler);
    }

    /*
     * Background thread that will handle pending certificate requests.
     */
    private class RequestHandlerThread extends Thread
    {
        private final AtomicBoolean stop = new AtomicBoolean();

        /*
         * Object used to notify the thread that it should wake up.
         */
        private final Object wakeup = new Object();

        public RequestHandlerThread() {
            super("Certificate request handler thread [" + threadCounter.incrementAndGet() + "]");
        }

        @Override
        public void run()
        {
            ThreadUtils.sleepQuietly(threadStartupDelay);

            logger.info("Certificate request handler thread started.");

            do {
                try {
                    synchronized (wakeup)
                    {
                        boolean nextHandled = Boolean.TRUE.equals(transactionOperations.execute(
                                status -> handlePendingRequest()));

                        if (!nextHandled) {
                            // Sleep for some time if there is nothing to do.
                            try {
                                wakeup.wait(threadSleepTime * DateUtils.MILLIS_PER_SECOND);
                            }
                            catch (InterruptedException e) {
                                Thread.currentThread().interrupt();
                            }
                        }
                    }
                }
                catch(Exception t)
                {
                    logger.error("Error in certificate request handler thread.", t);

                    // Sleep some time to make sure the thread doesn't consume 100% CPU when some kind of unhandled
                    // exception occurs over and over.
                    ThreadUtils.sleepQuietly(EXCEPTION_SLEEP_TIME);
                }
            }
            while(!stop.get());

            logger.info("Certificate request handler thread stopped.");
        }

        public void kick()
        {
            synchronized (wakeup) {
                wakeup.notifyAll();
            }
        }

        public void requestStop()
        {
            stop.set(true);

            kick();
        }
    }
}
