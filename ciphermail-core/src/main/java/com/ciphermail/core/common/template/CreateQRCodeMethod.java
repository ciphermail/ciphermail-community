/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.template;

import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.QRBuilder;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateModelException;

import java.io.IOException;
import java.util.List;

/**
 * A function for Freemarker that allows you to create a QR code image from the client secret
 * <p>
 * There should be 4 parameters to the Freemarker call:
 * <p>
 * content      : String
 * width        : Integer
 * height       : Integer
 * imageFormat  : String
 */
public class CreateQRCodeMethod extends TemplateMethodModelBase
{
    public static final String METHOD_NAME = "createQR";

    @Override
    public Object exec(@SuppressWarnings("rawtypes") List arguments)
    throws TemplateModelException
    {
        // There should be 4 parameters.
        //
        // [0] content      : String
        // [1] width        : Integer
        // [2] height       : Integer
        // [3] imageFormat  : String
        checkMethodArgCount(arguments, 4);

        QRBuilder qrBuilder = new QRBuilder();

        qrBuilder.setWidth(argToInt(arguments.get(1)));
        qrBuilder.setHeight(argToInt(arguments.get(2)));
        qrBuilder.setImageFormat(argToString(arguments.get(3)));

        try {
            return new SimpleScalar(Base64Utils.encodeChunked(qrBuilder.buildQRCode(argToString(arguments.get(0)))));
        }
        catch (IOException e) {
            throw new TemplateModelException(e);
        }
    }
}
