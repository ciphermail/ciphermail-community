/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.cms;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.cms.CMSCompressedDataParser;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.jcajce.ZlibExpanderProvider;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * Adapter to give org.bouncycastle.cms.CMSCompressedData a common interface.
 *
 * @author Martijn Brinkers
 *
 */
public class CMSCompressedDataParserAdapterImpl implements CMSCompressedDataAdapter
{
    /*
     * Class for reading a CMS Compressed Data stream.
     */
    private final CMSCompressedDataParser compressedData;

    /*
     * The max nr of bytes the compression might expand to
     */
    private final int limit;

    public CMSCompressedDataParserAdapterImpl(@Nonnull CMSCompressedDataParser compressedData)
    {
        this(compressedData, -1 /* unlimited */);
    }

    public CMSCompressedDataParserAdapterImpl(@Nonnull CMSCompressedDataParser compressedData, int limit)
    {
        this.compressedData = Objects.requireNonNull(compressedData);
        this.limit = limit;
    }

    @Override
    public byte[] getContent()
    throws CMSException, IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(getContentStream(), bos);

        return bos.toByteArray();
    }

    @Override
    public InputStream getContentStream()
    throws CMSException
    {
        return compressedData.getContent(new ZlibExpanderProvider(limit)).getContentStream();
    }
}
