/*
 * Copyright (c) 2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.ciphermail.core.common.util.URIUtils.URIType;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * HTMLLinkify can be used to add HTML anchors (<a> refs) to text.
 *
 * Note: the linkify does not detect all possible URLs. Only the ones mostly used (http and www links)
 *
 */
public class HTMLLinkify
{
    private static final Logger logger = LoggerFactory.getLogger(HTMLLinkify.class);

    private static final String ALLOWED_CHARS = "a-z0-9\\-_~:/?#@!$&'()*+,;=.\\[\\]";
    private static final String ALLOWED_CHARS_NO_POINT = "a-z0-9\\-_~:/?#@!$&'()*+,;=\\[\\]";

    private static final String DOUBLE_LINK_ALLOWED_CHARS = "a-z0-9\\-_~:/?#@!$&'()*+,;=.";
    private static final String DOUBLE_LINK_ALLOWED_CHARS_NO_POINT = "a-z0-9\\-_~:/?#@!$&'()*+,;=";

    /**
     * Pattern for link detection.
     *
     * Note: this pattern matches most general purpose links. However not all links are detected. The pattern might
     * also match invalid links. The matching pattern should therefore be checked with a URL checker
     */
    public static final Pattern HTTP_LINK_PATTERN = Pattern.compile(
            "(?i)(http://|https://|www\\.)[" + ALLOWED_CHARS + "]{1,1023}[" +
                    ALLOWED_CHARS_NO_POINT + "]");

    /**
     * Some links in text only emails are written as: [LINK](LINK). This pattern tries to match links without matching
     * square brackets. This pattern should be used after matching with {@link #HTTP_LINK_PATTERN} to detect whether the
     * link is written as [LINK](LINK).
     */
    public static final Pattern DOUBLE_HTTP_LINK_PATTERN = Pattern.compile(
            "(?i)(http://|https://|www\\.)[" + DOUBLE_LINK_ALLOWED_CHARS + "]+[" +
                    DOUBLE_LINK_ALLOWED_CHARS_NO_POINT + "]");

    /**
     * Scans the input for links and replaces the links with anchors (i.e., adds <a></a>)
     *
     */
    public static String linkify(final String input)
    {
        if (input == null) {
            return null;
        }

        String result = null;

        try {
            Matcher m = HTTP_LINK_PATTERN.matcher(input);

            StringBuilder sb = new StringBuilder(1024);

            while (m.find())
            {
                String match = m.group();

                // Check if the matched link has the form [LINK](LINK)
                Matcher subMatcher = DOUBLE_HTTP_LINK_PATTERN.matcher(match);

                if (subMatcher.find())
                {
                    String subMatch = subMatcher.group();

                    if (match.equals(subMatch + "](" + subMatch + ")")) {
                        // We matched [LINK](LINK)
                        match = subMatch;
                    }
                }

                String uriToCheck = match;

                // If the detected URL starts with www. we need to add http:// to make the URL valid
                if (StringUtils.startsWithIgnoreCase(uriToCheck, "www.")) {
                    uriToCheck = "http://" + uriToCheck;
                }

                // Check if the match is a valid URL
                if (URIUtils.isValidURI(uriToCheck, URIType.FULL))
                {
                    // We need to escape $
                    match = StringUtils.replace(match, "$", "\\$");

                    m.appendReplacement(sb, String.format("<a href=\"%s\">%s</a>", match, match));
                }
            }

            m.appendTail(sb);

            result = sb.toString();
        }
        catch (Exception e)
        {
            logger.error("Error trying to linkify text", e);

            result = input;
        }

        return result;
    }
}
