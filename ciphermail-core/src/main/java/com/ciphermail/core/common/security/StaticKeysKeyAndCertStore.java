/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509StoreEventListener;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of KeyAndCertStore that works on a static set of private keys. All calls are delegated to
 * to an existing KeyAndCertStore implementation except {@link #getMatchingKeys(KeyIdentifier)} and
 * {@link #getMatchingKeys(KeyIdentifier, Integer, Integer)}. Only keys from the provided KeyAndCertificate
 * collection can be returned as matching keys.
 *
 * The StaticKeysKeyAndCertStore is used when a message may only be decrypted with a subset of the private keys.
 *
 * @author Martijn Brinkers
 *
 */
public class StaticKeysKeyAndCertStore implements KeyAndCertStore
{
    /*
     * The delegate to delegate most calls to.
     */
    private final KeyAndCertStore delegate;

    /*
     * The static collection of KeyAndCertificate's that's being used by #getMatchingKeys.
     */
    private final Collection<KeyAndCertificate> keysAndCertificates;

    public StaticKeysKeyAndCertStore(@Nonnull  KeyAndCertStore delegate,
            @Nonnull Collection<KeyAndCertificate> keysAndCertificates)
    {
        this.delegate = Objects.requireNonNull(delegate);
        this.keysAndCertificates = Objects.requireNonNull(keysAndCertificates);
    }

    @Override
    public CloseableIterator<? extends X509CertStoreEntry> getByEmail(@Nonnull String email, Match match, Expired expired,
            MissingKeyAlias missingKeyAlias)
    throws CertStoreException
    {
        return delegate.getByEmail(email, match, expired, missingKeyAlias);
    }

    @Override
    public CloseableIterator<? extends X509CertStoreEntry> getByEmail(@Nonnull String email, Match match, Expired expired,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return delegate.getByEmail(email, match, expired, missingKeyAlias, firstResult, maxResults);
    }

    @Override
    public long getByEmailCount(@Nonnull String email, Match match, Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException
    {
        return delegate.getByEmailCount(email, match, expired, missingKeyAlias);
    }

    @Override
    public CloseableIterator<? extends X509CertStoreEntry> searchBySubject(@Nonnull String subject, Expired expired,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return delegate.searchBySubject(subject, expired, missingKeyAlias, firstResult, maxResults);
    }

    @Override
    public long getSearchBySubjectCount(@Nonnull String subject, Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException
    {
        return delegate.getSearchBySubjectCount(subject, expired, missingKeyAlias);
    }

    @Override
    public CloseableIterator<? extends X509CertStoreEntry> searchByIssuer(@Nonnull String issuer, Expired expired,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return delegate.searchByIssuer(issuer, expired, missingKeyAlias, firstResult, maxResults);
    }

    @Override
    public long getSearchByIssuerCount(@Nonnull String issuer, Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException
    {
        return delegate.getSearchByIssuerCount(issuer, expired, missingKeyAlias);
    }

    @Override
    public X509CertStoreEntry getByThumbprint(@Nonnull String thumbprint)
    throws CertStoreException
    {
        return delegate.getByThumbprint(thumbprint);
    }

    @Override
    public X509CertStoreEntry getByCertificate(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        return delegate.getByCertificate(certificate);
    }

    @Override
    public boolean contains(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        return delegate.contains(certificate);
    }

    @Override
    public long size() {
        return delegate.size();
    }

    @Override
    public long size(Expired expired, MissingKeyAlias missingKeyAlias) {
        return delegate.size(expired, missingKeyAlias);
    }

    @Override
    public CloseableIterator<? extends X509CertStoreEntry> getCertStoreIterator(Expired expired,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return delegate.getCertStoreIterator(expired, missingKeyAlias, firstResult, maxResults);
    }

    @Override
    @Nonnull public X509CertStoreEntry addCertificate(@Nonnull X509Certificate certificate, String keyAlias)
    throws CertStoreException
    {
        return delegate.addCertificate(certificate, keyAlias);
    }

    @Override
    @Nonnull public X509CertStoreEntry addCertificate(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        return delegate.addCertificate(certificate);
    }

    @Override
    public void removeCertificate(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        delegate.removeCertificate(certificate);
    }

    @Override
    public void removeAllEntries()
    throws CertStoreException
    {
        delegate.removeAllEntries();
    }

    @Override
    public CloseableIterator<? extends X509CertStoreEntry> getCertStoreIterator(CertSelector certSelector,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return delegate.getCertStoreIterator(certSelector, missingKeyAlias, firstResult, maxResults);
    }

    @Override
    public Collection<X509Certificate> getCertificates(CertSelector certSelector)
    throws CertStoreException
    {
        return delegate.getCertificates(certSelector);
    }

    @Override
    public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector)
    throws CertStoreException
    {
        return delegate.getCertificateIterator(certSelector);
    }

    @Override
    public void setStoreEventListener(@Nonnull X509StoreEventListener eventListener) {
        delegate.setStoreEventListener(eventListener);
    }

    @Override
    public @Nonnull X509CertStoreEntry addKeyAndCertificate(@Nonnull KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException
    {
        return delegate.addKeyAndCertificate(keyAndCertificate);
    }

    @Override
    public KeyAndCertificate getKeyAndCertificate(@Nonnull X509CertStoreEntry certStoreEntry)
    throws CertStoreException, KeyStoreException
    {
        return delegate.getKeyAndCertificate(certStoreEntry);
    }

    @Override
    public void sync(@Nonnull SyncMode syncMode)
    throws KeyStoreException, CertStoreException
    {
        delegate.sync(syncMode);
    }

    @Override
    public Collection<? extends PrivateKey> getMatchingKeys(@Nonnull KeyIdentifier keyIdentifier)
    throws KeyStoreException
    {
        return getMatchingKeys(keyIdentifier, null, null);
    }

    @Override
    public Collection<? extends PrivateKey> getMatchingKeys(@Nonnull KeyIdentifier keyIdentifier, Integer firstResult,
            Integer maxResults)
    throws KeyStoreException
    {
        Set<PrivateKey> matchingKeys = new HashSet<>();

        // Only CertSelectorKeyIdentifier is supported
        if (keyIdentifier instanceof CertSelectorKeyIdentifier certSelectorKeyIdentifier)
        {
            CertSelector selector;

            try {
                selector = certSelectorKeyIdentifier.getSelector();
            }
            catch (IOException e) {
                throw new KeyStoreException(e);
            }

            if (selector != null)
            {
                int i = 0;

                for (KeyAndCertificate keyAndCertificate : keysAndCertificates)
                {
                    if (keyAndCertificate.getPrivateKey() == null || keyAndCertificate.getCertificate() == null) {
                        continue;
                    }

                    if (selector.match(keyAndCertificate.getCertificate()))
                    {
                        if (firstResult == null || i >= firstResult)
                        {
                            matchingKeys.add(keyAndCertificate.getPrivateKey());

                            i++;

                            if (maxResults != null && i >= maxResults) {
                                break;
                            }
                        }
                    }
                }
            }
        }

        return matchingKeys;
    }
}
