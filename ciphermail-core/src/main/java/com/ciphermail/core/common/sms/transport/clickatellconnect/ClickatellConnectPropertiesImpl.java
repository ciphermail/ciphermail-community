/*
 * Copyright (c) 2018-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.transport.clickatellconnect;

import com.ciphermail.core.app.properties.SMSTransportsCategory;
import com.ciphermail.core.app.properties.UserPropertiesType;
import com.ciphermail.core.app.properties.UserProperty;
import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

import javax.annotation.Nonnull;

/**
 * Implementation of ClickatellConnectProperties that delegates property setter/getter to a HierarchicalProperties
 * instance.
 */
@UserPropertiesType(name = "clickatell-connect", displayName = "Clickatell/Connect",
        parent = SMSTransportsCategory.NAME, order = 10)
public class ClickatellConnectPropertiesImpl extends DelegatedHierarchicalProperties
    implements ClickatellConnectProperties
{
    /**
     * Property names
     */
    private static final String API_KEY = "sms-transport-clickatell-connect-api-key";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public ClickatellConnectPropertiesImpl(@Nonnull HierarchicalProperties delegatedProperties) {
        super(delegatedProperties);
    }

    public static ClickatellConnectPropertiesImpl getInstance(@Nonnull HierarchicalProperties delegatedProperties) {
        return new ClickatellConnectPropertiesImpl(delegatedProperties);
    }

    @Override
    @UserProperty(name = API_KEY,
            user = false, domain = false
    )
    public void setAPIKey(String apiKey)
    throws HierarchicalPropertiesException
    {
        setProperty(API_KEY, apiKey);
    }

    @Override
    @UserProperty(name = API_KEY,
            user = false, domain = false,
            allowNull = true
    )
    public String getAPIKey()
    throws HierarchicalPropertiesException
    {
        return getProperty(API_KEY);
    }

    @Override
    public void copyTo(ClickatellConnectProperties other)
    throws HierarchicalPropertiesException
    {
        if (other == null) {
            return;
        }

        other.setAPIKey(getAPIKey());
    }
}
