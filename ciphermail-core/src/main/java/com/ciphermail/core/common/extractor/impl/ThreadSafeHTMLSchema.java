/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.extractor.impl;

import org.ccil.cowan.tagsoup.ElementType;
import org.ccil.cowan.tagsoup.HTMLSchema;

/**
 * Creating an instance of HTMLSchema is pretty heavy. HTMLSchema however is not synchronized. To make
 * sure that HTMLSchema can be reused we will override all methods and synchronize access.
 *
 * @author Martijn Brinkers
 *
 */
public class ThreadSafeHTMLSchema extends HTMLSchema
{
    public ThreadSafeHTMLSchema() {
        super();
    }

    @Override
    public synchronized void elementType(String name, int model, int memberOf, int flags)
    {
        super.elementType(name, model, memberOf, flags);
    }

    @Override
    public synchronized ElementType rootElementType()
    {
        return super.rootElementType();
    }

    @Override
    public synchronized void attribute(String elemName, String attrName, String type, String value)
    {
        super.attribute(elemName, attrName, type, value);
    }

    @Override
    public synchronized void parent(String name, String parentName)
    {
        super.parent(name, parentName);
    }

    @Override
    public synchronized void entity(String name, int value)
    {
        super.entity(name, value);
    }

    @Override
    public synchronized ElementType getElementType(String name)
    {
        return super.getElementType(name);
    }

    @Override
    public synchronized int getEntity(String name)
    {
        return super.getEntity(name);
    }

    @Override
    public synchronized String getURI()
    {
        return super.getURI();
    }

    @Override
    public synchronized String getPrefix()
    {
        return super.getPrefix();
    }

    @Override
    public synchronized void setURI(String uri)
    {
        super.setURI(uri);
    }

    @Override
    public synchronized void setPrefix(String prefix)
    {
        super.setPrefix(prefix);
    }
}
