/*
 * Copyright (c) 2019-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * General purpose JSON utils
 */
public class JSONUtils
{
    /*
     * Regex to detect ${..} values
     */
    private static final Pattern TOKEN_PATTERN = Pattern.compile("\\$\\{[a-zAZ_0-9 ]*\\}");

    private JSONUtils() {
        // empty on purpose
    }

    /**
     * Checks if the input string is a valid JSON objects
     */
    public static boolean isValidJSONObject(@Nonnull String json)
    {
        try {
            new JSONObject(json);
        }
        catch (JSONException e) {
            return false;
        }
        return true;
    }

    /**
     * Checks if the input string is a valid JSON array
     */
    public static boolean isValidJSONArray(@Nonnull String json)
    {
        try {
            new JSONArray(json);
        }
        catch (JSONException e) {
            return false;
        }
        return true;
    }

    /**
     * Checks if the JSON object only contains supported parameters. If not, a JSONException will be thrown
     */
    public static void checkSupportedNamedParameters(@Nonnull JSONObject json, @Nonnull String ...supportedParameters)
    throws JSONException
    {
        Set<String> supportedNames = CollectionUtils.asLinkedHashSet(supportedParameters);

        Iterator<?> it = json.keys();

        while (it.hasNext())
        {
            Object o = it.next();

            if (!(o instanceof String)) {
                throw new JSONException("key is not a string but a " + o.getClass());
            }

            if (!supportedNames.contains(o)) {
                throw new JSONException("Unsupported parameter " + o);
            }
        }
    }

    /**
     * Merges source with target. The json object must be structurally similar, i.e., if both objects have
     * the same key, the type of the value must be the same. For example if an element is an JSON array, the element
     * of the other JSON object with the same name (and the same part of the tree) should also be an array.
     * <p>
     * Note: if the JSON object is an JSONArray, all values from source will be shallow copied to the target, i.e.,
     * JSON objects withing the JSON array will not be recursively copied.
     *
     * @throws JSONException
     */
    public static JSONObject deepMerge(@Nonnull JSONObject source, @Nonnull JSONObject target)
    throws JSONException
    {
        for (String key: JSONObject.getNames(source))
        {
            Object sourceValue = source.get(key);

            if (sourceValue == null) {
                throw new JSONException("Source value for key " + key + " is null");
            }

            if (!target.has(key)) {
                target.put(key, sourceValue);
            }
            else {
                Object targetValue = target.get(key);

                if (targetValue == null) {
                    throw new JSONException("Target value for key " + key + " is null");
                }

                if (!sourceValue.getClass().equals(targetValue.getClass()))
                {
                    throw new JSONException("Source value is-a " + sourceValue.getClass() + " but target value is-a " +
                            targetValue.getClass());
                }

                if (sourceValue instanceof JSONObject jsonObject) {
                    deepMerge(jsonObject, (JSONObject) targetValue);
                }
                else if (sourceValue instanceof JSONArray sourceArray)
                {
                    /*
                     * Shallow copy all values from source array to target array
                     */
                    JSONArray targetArray = (JSONArray) targetValue;

                    for (int i = 0; i < sourceArray.length(); i++) {
                        targetArray.put(sourceArray.get(i));
                    }
                }
                else {
                    target.put(key, sourceValue);
                }
            }
        }

        return target;
    }

    private static String replaceString(String value, @Nonnull StrSubstitutor strSubstitutor)
    {
        // Note: StrSubstitutor#replace will recursively replace
        String replacedValue = strSubstitutor.replace(value);

        // Remove any non-replaced tokens
        return TOKEN_PATTERN.matcher(replacedValue).replaceAll("");
    }

    private static JSONArray replaceTokens(@Nonnull JSONArray jsonArray, @Nonnull StrSubstitutor strSubstitutor)
    throws JSONException
    {
        JSONArray result = new JSONArray();

        for (int i = 0; i < jsonArray.length(); i++)
        {
            Object object = jsonArray.get(i);

            if (object instanceof String stringObject)
            {
                // Note: StrSubstitutor#replace will recursively replace
                String replaced = replaceString(stringObject, strSubstitutor);

                // If replaced value is blank, remove the JSON value because there was no replacement.
                //
                // Note: this implies that we cannot have values which are empty or whitespace only
                if (StringUtils.isNotBlank(replaced)) {
                    result.put(replaced);
                }
            }
            else if (object instanceof JSONObject jsonObject ) {
                JSONObject replaced = replaceTokens(jsonObject, strSubstitutor);

                if (replaced != null) {
                    result.put(replaced);
                }
            }
            else if (object instanceof JSONArray jsonArrayObject)
            {
                JSONArray replaced = replaceTokens(jsonArrayObject, strSubstitutor);

                if (replaced != null) {
                    result.put(replaced);
                }
            }
        }

        return !result.isEmpty() ? result : null;
    }

    /**
     * Replaces JSON values containing ${...} by the substitution. Any ${..} values which are not substituted will be
     * removed. After substitution, empty JSON objects and empty JSON arrays will be removed
     *
     * Note: It is assumed that StrSubstitutor uses ${..} for the PREFIX and SUFFIX
     *
     * @return JSON Object with values replaced by the substitution. Null if the resulting JSON is empty
     */
    public static JSONObject replaceTokens(JSONObject json, @Nonnull StrSubstitutor strSubstitutor)
    throws JSONException
    {
        if (json == null || json.isEmpty()) {
            return null;
        }

        for (String name : JSONObject.getNames(json))
        {
            Object object = json.get(name);

            if (object instanceof String stringObject)
            {
                // Note: StrSubstitutor#replace will recursively replace
                String replacedValue = replaceString(stringObject, strSubstitutor);

                // If replaced value is blank, remove the JSON value because there was no replacement.
                //
                // Note: this implies that we cannot have values which are empty or whitespace only
                if (StringUtils.isNotBlank(replacedValue)) {
                    json.put(name, replacedValue);
                }
                else {
                    json.remove(name);
                }
            }
            else if (object instanceof JSONObject jsonObject)
            {
                JSONObject replaced = replaceTokens(jsonObject, strSubstitutor);

                if (replaced != null) {
                    json.put(name, replaced);
                }
                else {
                    json.remove(name);
                }
            }
            else if (object instanceof JSONArray jsonArray)
            {
                JSONArray array = replaceTokens(jsonArray, strSubstitutor);

                if (array != null) {
                    json.put(name, array);
                }
                else {
                    json.remove(name);
                }
            }
        }

        return !json.isEmpty() ? json : null;
    }
}
