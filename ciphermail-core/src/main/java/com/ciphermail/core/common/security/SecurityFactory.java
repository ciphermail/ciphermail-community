/*
 * Copyright (c) 2008-2016, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.certificate.X509CertificateBuilder;
import com.ciphermail.core.common.security.crl.X509CRLBuilder;
import com.ciphermail.core.common.security.crypto.RandomGenerator;

import javax.annotation.Nonnull;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.TrustAnchor;
import java.util.Set;

/**
 * This factory interface is used to abstract the actual implementation of certain
 * security related classes. We now have just one place we need to specify
 * which security provider we will use.
 *
 * @author Martijn Brinkers
 *
 */
public interface SecurityFactory
{
    /**
     * Creates a new CertificateFactory instance for the given certificate type.
     * Example: "X.509"
     * The provider uses depends on the SecurityFactory implementation used
     * @param certificateType
     * @return a new CertificateFactory instance
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    CertificateFactory createCertificateFactory(@Nonnull String certificateType)
    throws CertificateException, NoSuchProviderException;

    /**
     * Creates a new MessageDigest instance for the given digest type.
     * Example: "SHA1".
     * @param digestType
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    MessageDigest createMessageDigest(@Nonnull String digestType)
    throws NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates a new X509CertificateBuilder instance.
     * @return
     */
    X509CertificateBuilder createX509CertificateBuilder();

    /**
     * Creates a new KeyPairGenerator instance for the given algorithm
     * Example: "RSA"
     * @param algorithm
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    KeyPairGenerator createKeyPairGenerator(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates a new SecretKeyFactory for the given algorithm
     * @param algorithm
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    SecretKeyFactory createSecretKeyFactory(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates a new KeyFactory for the given algorithm
     * @param algorithm
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    KeyFactory createKeyFactory(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates a new certPathBuilder implementing the algorithm.
     * @param algorithm
     * @return
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     */
    CertPathBuilder createCertPathBuilder(@Nonnull String algorithm)
    throws NoSuchProviderException, NoSuchAlgorithmException;

    /**
     * Creates a PKIXBuilderParameters instance
     * @param trustAnchors
     * @param targetConstraints
     * @return
     * @throws InvalidAlgorithmParameterException
     */
    PKIXBuilderParameters createPKIXBuilderParameters(@Nonnull Set<TrustAnchor> trustAnchors,
            CertSelector targetConstraints)
    throws InvalidAlgorithmParameterException;

    /**
     * Creates a SecureRandom instance using the given algorithm.
     * Example: "SHA1PRNG"
     * @param algorithm
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    SecureRandom createSecureRandom(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates and returns a SecureRandom instance with default algorithm.
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    SecureRandom createSecureRandom()
    throws NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates the given key store
     * @param keyStoreType
     * @return
     */
    KeyStore createKeyStore(@Nonnull String keyStoreType)
    throws KeyStoreException, NoSuchProviderException;

    /**
     * Creates a certstore
     * @param certStoreType
     * @param certStoreParameters
     * @return
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    CertStore createCertStore(@Nonnull String certStoreType, CertStoreParameters certStoreParameters)
    throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates a new cipher instance for the given algorithm
     * @param algorithm
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws NoSuchPaddingException
     */
    Cipher createCipher(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException;

    /**
     * Creates a new RandomGenerator
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    RandomGenerator createRandomGenerator()
    throws NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates a MAC instance
     *
     * @param algorithm
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    Mac createMAC(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates a Signature instance
     */
    Signature createSignature(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException;

    /**
     * Creates a X509CRLBuilder instance
     */
    X509CRLBuilder createX509CRLBuilder();

    /**
     * Returns the name of the provider to use for non-sensitive operations. Non sensitive operations are
     * operations that do not need to be protected with key material stored on an HSM or smartcard.
     *
     * @return the name of the provider
     */
    String getNonSensitiveProvider();

    /**
     * Returns the name of the provider to use for sensitive operations. Sensitive operations are
     * operations that should be protected with key material stored on an HSM or smartcard.
     *
     * @return the name of the provider
     */
    String getSensitiveProvider();
}
