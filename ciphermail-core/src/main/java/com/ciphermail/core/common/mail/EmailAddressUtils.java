/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.locale.DefaultLocale;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Some general purpose utilities for email address validation etc.
 *
 * @author Martijn Brinkers
 *
 */
public class EmailAddressUtils
{
    private static final Logger logger = LoggerFactory.getLogger(EmailAddressUtils.class);

    /**
     * Email address to use when an email address is invalid
     */
    public static final String INVALID_EMAIL = "invalid@invalid.tld";

    /**
     * Email address pattern should not be used for the validation of email addresses because they are not
     * in line with the one from Javamail. Only use this pattern for creating visual mark-ups etc.
     */
    public static final String EMAIL_REG_EXPR = "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";

    /**
     * The pattern used for matching email addresses (non strict matching!). Should be used for matching email
     * addresses for visual feedback (highlighting of email addresses etc.)
     */
    public static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REG_EXPR);

    private EmailAddressUtils() {
        // empty on purpose
    }

    /**
     * Removes quotes from an email address. Email addresses can be quoted. This
     * however can be problematic when we need to use the email address as a key.
     * For example "test"@example.com is in principle the same email address as
     * test@example.com. This function will strip away any quotes from email
     * addresses.
     * <p>
     * Outlook sometimes will single quote an email address for email addresses
     * in headers.
     */
    public static String stripQuotes(String email)
    {
        if (email == null) {
            return null;
        }

        String unquotedEmail = email.trim();

        // First check if the whole email address is single quoted
        // (Outlook sometimes adds single quotes to headers)
        // example: 'test@example.com'
        unquotedEmail = MiscStringUtils.unquote(unquotedEmail, '\'');
        unquotedEmail = MiscStringUtils.unquote(unquotedEmail, '"');

        // the local part of an email address can be quoted.
        int atIndex = unquotedEmail.lastIndexOf('@');

        // did the email address contain the @ symbol?
        if (atIndex > -1)
        {
            String localPart  = unquotedEmail.substring(0, atIndex).trim();
            String domainPart = unquotedEmail.substring(atIndex).trim();

            localPart = MiscStringUtils.unquote(localPart, '"');

            unquotedEmail = localPart + domainPart;
        }

        return unquotedEmail;
    }

    /**
     * Checks whether the given email address is a valid email address. If strict is true, the email address must be
     * a fully qualified address (i.e., including domain)
     * @param email
     * @return email address if email is valid, null if email is invalid
     */
    public static String validate(String email, boolean strict)
    {
        // email size must be smaller than max domain + max local
        if (email != null && email.length() < (255 + 65))
        {
            try {
                InternetAddress validated = new InternetAddress(email, strict);

                return validated.getAddress();
            }
            catch (AddressException ignored) {
                logger.debug("Email address \"{}\" is not a valid email address", email);
            }
        }
        return null;
    }

    /**
     * Checks whether the given email address is a valid email address with strict checking enabled.
     * @param email
     * @return email address if email is valid, null if email is invalid
     */
    public static String validate(String email) {
        return validate(email, true /* strict checking */);
    }

    /**
     * Returns true if the email is a valid email address. If strict is true, the email address must be
     * a fully qualified address (i.e., including domain)
     */
    public static boolean isValid(String email, boolean strict) {
        return validate(email, strict) != null;
    }

    /**
     * Returns true if the email is a valid email address with strict checking enabled.
     */
    public static boolean isValid(String email) {
        return validate(email) != null;
    }

    /**
     * Returns true if the email is a valid email address. If strict is true, the email address must be
     * a fully qualified address (i.e., including domain)
     */
    public static boolean isValid(InternetAddress address, boolean strict) {
        return address != null && validate(address.getAddress(), strict) != null;
    }

    /**
     * Returns true if the email is a valid email address with strict checking enabled.
     */
    public static boolean isValid(InternetAddress address) {
        return address != null && validate(address.getAddress()) != null;
    }

    /**
     * Transform the given email address in a standard form so we can use it as a key using the following steps.
     * When we need to use an email address as an identifier we need to transform an email address to a standard
     * form ie. we need to canonicalize the email address because the same email address can be specified in
     * different ways. In principle email addresses are case sensitive however in practice email addresses are almost
     * always not case sensitive. We therefore will transform the email address to lowercase. The local part of an
     * email address can be quoted to support characters that are normally not allowed for the local part.
     * Examples: "has space"@example.com, "end."@example.com. These email addresses would, in principle, not be valid
     * without the quotes. But, "test"@example.com is equal to test@example.com so we must transform these email
     * addresses to a standard form. The quotes will only be removed if the resulting email address is not invalid.
     * 1. remove start/end quotes
     * 2. trim
     * 3. make all characters lower case
     * @param email
     * @return
     */
    public static String canonicalize(String email)
    {
        if (email == null) {
            return null;
        }

        String canonical = email.toLowerCase(DefaultLocale.getDefaultLocale());

        String unquoted = stripQuotes(canonical);

        // We need to know whether the email address becomes invalid after removing the quotes. If so keep the quotes
        if (isValid(unquoted)) {
            canonical = unquoted;
        }

        return canonical;
    }

    /**
     * Canonicalizes and validates the given email address. If nullIfInvalid is true and email address is not a valid email address
     * null is returned. If nullIfInvalid is false and email is invalid INVALID_EMAIL is returned.
     */
    public static String canonicalizeAndValidate(String email, boolean nullIfInvalid)
    {
        String normalized = canonicalize(email);

        normalized = validate(normalized);

        if (normalized == null && !nullIfInvalid) {
            return INVALID_EMAIL;
        }

        return normalized;
    }

    /**
     * Canonicalizes and validates the given collection of email addresses and returns a Set
     * of email addresses (duplicates will therefore be removed)
     * @return canonicalized and valid emails. Never null.
     */
    public static Set<String> canonicalizeAndValidate(Collection<String> emails, boolean skipIfInvalid)
    {
        Set<String> normalizedEmails = new HashSet<>();

        if (emails != null)
        {
            for (String email : emails)
            {
                String normalized = canonicalizeAndValidate(email, skipIfInvalid);

                if (normalized != null) {
                    normalizedEmails.add(normalized);
                }
            }
        }

        return normalizedEmails;
    }

    /**
     * Canonicalizes and validates the given collection of email addresses and returns a Set
     * of email addresses (duplicates will therefore be removed)
     * @return canonicalized and valid emails. Never null.
     */
    public static Set<String> canonicalizeAndValidate(String[] emails, boolean skipIfInvalid)
    {
        return emails != null ? canonicalizeAndValidate(Arrays.asList(emails), skipIfInvalid) :
                canonicalizeAndValidate((Collection<String>) null, skipIfInvalid);
    }

    /**
     * Parse comma seperated email addresses.
     * <p>
     * Note: the email addresses are not canonicalized
     */
    public static List<String> parseAddressList(String addressList)
    throws AddressException
    {
        addressList = StringUtils.trimToNull(addressList);

        if (addressList == null) {
            return Collections.emptyList();
        }

        List<String> addresses = Arrays.stream(InternetAddress.parse(addressList)).map(
                InternetAddress::getAddress).toList();

        for (String address : addresses)
        {
            if (!isValid(address)) {
                throw new AddressException(addressList + " contains an invalid email address");
            }
        }

        return addresses;
    }

    /**
     * Returns the domain part ie. everything after the @. If there is no @ null is returned. The email address is
     * not normalized or validated. It's up to the caller to validate the email address.
     */
    public static String getDomain(String email)
    {
        String domain = StringUtils.substringAfterLast(email, "@");

        if (StringUtils.isEmpty(domain)) {
            domain = null;
        }

        return domain;
    }

    /**
     * See: getDomain(email)
     */
    public static String getDomain(InternetAddress email)
    {
        if (email == null) {
            return null;
        }

        return getDomain(email.getAddress());
    }

    /**
     * Returns the local part of an email address ie. everything before the @. If there is no @ the complete string is
     * returned. The email address is not normalized or validated. It's up to the caller to validate the email address.
     * If there is no local part null is returned.
     */
    public static String getLocalPart(String email)
    {
        String local = StringUtils.substringBeforeLast(email, "@");

        if (StringUtils.isEmpty(local)) {
            local = null;
        }

        return local;
    }

    /**
     * See getLocalPart(String email)
     */
    public static String getLocalPart(InternetAddress email)
    {
        if (email == null) {
            return null;
        }

        return getLocalPart(email.getAddress());
    }

    /**
     * If the local part contains any characters which are not allowed in the localpart
     * and the localpart is not yet quoted, the local part will be quoted.
     */
    public static String quoteLocalPart(String email)
    {
        if (email == null) {
            return null;
        }

        String localPart = getLocalPart(email);

        if (localPart == null) {
            return email;
        }

        if (!MiscStringUtils.isQuoted(localPart, '"') && (!isValid(email)))
        {
            String quoted = "\"" + localPart + "\"@" + getDomain(email);

            if (isValid(quoted)) {
                email = quoted;
            }
        }

        return email;
    }

    /**
     * Returns the email part if address is an InternetAddress, if not toString() is called in the Address.
     */
    public static String getEmailAddress(Address address)
    {
        String email;

        if (address == null) {
            return null;
        }

        if (address instanceof InternetAddress internetAddress) {
            email = internetAddress.getAddress();
        }
        else {
            email = address.toString();
        }

        return email;
    }

    /**
     * Converts email address to an InternetAddress. If the email address is invalid, null will be returned.
     */
    public static InternetAddress toInternetAddress(String email)
    {
        InternetAddress address = null;

        if (email != null)
        {
            try {
                address = new InternetAddress(email);
            }
            catch (AddressException e) {
                logger.debug("Email address {} is invalid", email);
            }
        }

        return address;
    }

    /**
     * Converts the Address to an InternetAddress. If the email address is invalid, null will be returned.
     */
    public static InternetAddress toInternetAddress(Address address)
    {
        InternetAddress internetAddress = null;

        if (address == null) {
            return null;
        }

        if (address instanceof InternetAddress) {
            internetAddress = ((InternetAddress) address);
        }
        else
        {
            try {
                internetAddress = new InternetAddress(address.toString());
            }
            catch (AddressException e) {
                logger.debug("Email address {} is invalid", address);
            }
        }

        return internetAddress;
    }

    /**
     * Returns a List of InternetAddress for the email addresses. The email addresses are canonicalized and
     * validated. If an email address is invalid or null, it will be skipped.
     */
    public static List<InternetAddress> toInternetAddresses(String... emails) {
        return toInternetAddresses(true, emails != null ? Arrays.asList(emails) : null);
    }

    /**
     * Returns a List of InternetAddress for the email addresses. The email addresses are canonicalized and
     * validated. If an email address is invalid or null, it will be skipped.
     */
    public static List<InternetAddress> toInternetAddresses(Collection<String> emails) {
        return toInternetAddresses(true, emails);
    }

    /**
     * Returns a List of InternetAddress for the email addresses. The email addresses are canonicalized and
     * validated. If an email address is invalid or null, IllegalArgumentException will be thrown
     */
    public static List<InternetAddress> toInternetAddressesStrict(String... emails) {
        return toInternetAddresses(false, emails != null ? Arrays.asList(emails) : null);
    }

    /**
     * Returns a List of InternetAddress for the email addresses. The email addresses are canonicalized and
     * validated. If an email address is invalid or null, IllegalArgumentException will be thrown
     */
    public static List<InternetAddress> toInternetAddressesStrict(Collection<String> emails) {
        return toInternetAddresses(false, emails);
    }

    private static List<InternetAddress> toInternetAddresses(boolean skipIfInvalid, Collection<String> emails)
    {
        List<InternetAddress> addresses = new LinkedList<>();

        if (emails != null)
        {
            for (String email : emails)
            {
                String canonicalizeEmail = canonicalizeAndValidate(email, true);

                if (canonicalizeEmail == null)
                {
                    if (!skipIfInvalid) {
                        throw new IllegalArgumentException(email + " is not a valid email address");
                    }

                    continue;
                }

                try {
                    addresses.add(new InternetAddress(canonicalizeEmail));
                }
                catch (AddressException e) {
                    // Should not happen since it's canonicalized. Skip
                }
            }
        }

        return addresses;
    }

    /**
     * Returns the first address from addresses. Null if addresses is null or addresses.length == 0.
     */
    public static Address getAddress(Address[] addresses)
    {
        if (addresses == null || addresses.length == 0) {
            return null;
        }

        return addresses[0];
    }

    /**
     * Returns the Address's as an array of strings. If decode is true, the address will be mime decoded.
     * Returns null if addresses is null. Decode is used to decode the user part (if encoded) of an
     * email address. The user part will always be double quoted (i.e., between ""). Because the user
     * part will be decoded, the resulting string is not always a valid address for an email.
     * <p>
     * WARNING: The returned string includes the user part!
     */
    public static String[] addressesToStrings(Address[] addresses, boolean decode)
    {
        if (addresses == null) {
            return null;
        }

        List<String> list = addressesToStrings(Arrays.asList(addresses), decode);

        return list != null ? list.toArray(new String[]{}) : null;
    }

    /**
     * Returns the Address's as List of strings. If decode is true, the address will be mime decoded.
     * Returns null if addresses is null. Decode is used to decode the user part (if encoded) of an
     * email address. The user part will always be double quoted (i.e., between ""). Because the user
     * part will be decoded, the resulting string is not always a valid address for an email.
     *
     * WARNING: The returned string includes the user part!
     */
    public static List<String> addressesToStrings(Collection<? extends Address> addresses, boolean decode)
    {
        if (addresses == null) {
            return null;
        }

        ArrayList<String> result = new ArrayList<>(addresses.size());

        for (Address address : addresses)
        {
            if (address != null)
            {
                if (decode)
                {
                    if (address instanceof InternetAddress internetAddress)
                    {
                        String decoded = StringUtils.trimToNull(HeaderUtils.decodeTextQuietly(
                                internetAddress.getPersonal()));

                        decoded = decoded != null ? "\"" + decoded + "\" <" + internetAddress.getAddress() + ">" :
                                internetAddress.getAddress();

                        result.add(decoded);
                    }
                    else {
                        result.add(HeaderUtils.decodeTextQuietly(address.toString()));
                    }
                }
                else {
                    result.add(address.toString());
                }
            }
        }

        return result;
    }

    /**
     * Returns true of search is contained in emails. The emails are canonicalized and checked
     * for validity before being compared.
     */
    public static boolean containsEmail(String search, Collection<String> emails)
    {
        String filteredSearch = canonicalizeAndValidate(search, true);

        if (filteredSearch == null || emails == null) {
            return false;
        }

        Set<String> filteredEmails = canonicalizeAndValidate(emails, true);

        return filteredEmails.contains(filteredSearch);
    }

    /**
     * Returns true if email is equal to INVALID_EMAIL (invalid@invalid.tld)
     */
    public static boolean isInvalidDummyAddress(String email) {
        return INVALID_EMAIL.equals(email);
    }

    /**
     * Returns true if email is equal to INVALID_EMAIL (invalid@invalid.tld)
     */
    public static boolean isInvalidDummyAddress(InternetAddress email)
    {
        if (email == null) {
            return false;
        }

        return INVALID_EMAIL.equals(email.getAddress());
    }

    /**
     * Returns the Reply-To not throwing any exception. If the Reply-To header cannot be retrieved, null will be
     * returned.
     */
    public static Address[] getReplyToQuietly(MimeMessage message)
    {
        Address[] replyTos = null;

        if (message != null)
        {
            try {
                replyTos = message.getReplyTo();
            }
            catch (Exception e) {
                logger.debug("Reply-To is not valid", e);
            }
        }

        return replyTos;
    }

    /**
     * Returns the From not throwing any exception. If the From header cannot be retrieved, null will be
     * returned.
     */
    public static Address[] getFromQuietly(MimeMessage message)
    {
        Address[] froms = null;

        if (message != null)
        {
            try {
                froms = message.getFrom();
            }
            catch (Exception e) {
                logger.debug("From is not valid", e);
            }
        }

        return froms;
    }

    /**
     * Returns the recipients with relaxed address validation.
     *
     * Note: Only RecipientType.To and RecipientType.Cc are supported
     *
     * @throws MessagingException
     */
    public static List<InternetAddress> getRecipientsNonStrict(MimeMessage message,
            @Nonnull Message.RecipientType recipientType)
    throws MessagingException
    {
        String headerName;

        if (Message.RecipientType.TO.equals(recipientType)) {
            headerName = "To";
        }
        else if (Message.RecipientType.CC.equals(recipientType)) {
            headerName = "Cc";
        }
        else if (Message.RecipientType.BCC.equals(recipientType)) {
            headerName = "Bcc";
        }
        else {
            throw new IllegalArgumentException("Unsupported recipientType " + recipientType);
        }

        return getAddressHeaderNonStrict(message, headerName);
    }

    /**
     * Returns the email addresses from the header if the header exists. The email addresses are not strictly checked,
     * i.e., invalid email addresses might be returned
     */
    public static List<InternetAddress> getAddressHeaderNonStrict(MimeMessage message, String headerName)
    throws MessagingException
    {
        List<InternetAddress> addresses = null;

        if (message != null)
        {
            String headerValue = StringUtils.trimToNull(message.getHeader(headerName, ","));

            if (headerValue != null)
            {
                try {
                    addresses = Arrays.asList(InternetAddress.parseHeader(headerValue, false /* non-strict */));
                }
                catch (StringIndexOutOfBoundsException e) {
                    // With some invalid headers, this can result in an StringIndexOutOfBoundsException. We will
                    // catch this and re-throw an AddressException
                    throw new AddressException(e.getMessage());
                }
            }
        }

        return addresses;
    }

    /**
     * Returns the from address. If from address is not set, the sender address will be returned. The email addresses
     * are not strictly checked, i.e., invalid email addresses might be returned
     */
    public static List<InternetAddress> getFromNonStrict(MimeMessage message)
    throws MessagingException
    {
        List<InternetAddress> addresses = getAddressHeaderNonStrict(message, "From");

        if (addresses == null || addresses.isEmpty()) {
            addresses = getAddressHeaderNonStrict(message, "Sender");
        }

        return addresses;
    }

    /**
     * Returns the Reply-To address. If Reply-To address is not set, from will be returned. The email addresses
     * are not strictly checked, i.e., invalid email addresses might be returned. If useFromIfEmpty is true,
     * the From header will be used if the Reply-To header is not set or is empty.
     *
     */
    public static List<InternetAddress> getReplyToNonStrict(MimeMessage message, boolean useFromIfEmpty)
    throws MessagingException
    {
        List<InternetAddress> addresses = getAddressHeaderNonStrict(message, "Reply-To");

        if ((addresses == null || addresses.isEmpty()) && useFromIfEmpty) {
            addresses = getFromNonStrict(message);
        }

        return addresses;
    }

    /**
     * Returns the Reply-To address. If Reply-To address is not set, from will be returned. The email addresses
     * are not strictly checked, i.e., invalid email addresses might be returned. If useFromIfEmpty is true,
     * the From header will be used if the Reply-To header is not set or is empty.
     * <p>
     * Note: this method functions differently from {@link Message#getReplyTo()}. If the Reply-To header is not set,
     * null will be returned, i.e., the From header will not be used
     *
     */
    public static List<InternetAddress> getReplyToNonStrict(MimeMessage message)
    throws MessagingException
    {
        return getReplyToNonStrict(message, false);
    }
}
