/*
 * Copyright (c) 2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.notification.NotificationMessage;
import org.apache.commons.lang.text.StrBuilder;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Implementation of NotificationMessage sent when one or more new PGP keyrings are imported
 *
 * @author Martijn Brinkers
 *
 */
public class PGPPublicKeyRingNotificationMessage implements NotificationMessage
{
    private static final String DEFAULT_MESSAGE = "A keyring with master key ID %s was imported.";

    /*
     * The text part of the notification
     */
    private final String message;

    public PGPPublicKeyRingNotificationMessage(@Nonnull Collection<PGPPublicKeyRing> keyRings) {
        this(keyRings, DEFAULT_MESSAGE);
    }

    public PGPPublicKeyRingNotificationMessage(@Nonnull Collection<PGPPublicKeyRing> keyRings, String message)
    {
        StrBuilder sb = new StrBuilder();

        for (PGPPublicKeyRing keyRing : keyRings)
        {
            if (keyRing == null) {
                continue;
            }

            PGPPublicKey masterKey = keyRing.getPublicKey();

            String keyID = masterKey != null ? PGPUtils.getKeyIDHex(masterKey.getKeyID()) : "unknown";

            sb.appendSeparator('\n');
            sb.append(String.format(message, keyID));
        }

        this.message = sb.toString();
    }

    @Override
    public String getMessage() {
        return message;
    }
}
