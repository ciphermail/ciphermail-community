/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.RequiredByJames;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.UUID;

/**
 * The CertificateRequestStore will store pending certificate requests.
 *
 * @author Martijn Brinkers
 *
 */
@RequiredByJames
public interface CertificateRequestStore
{
    /**
     * Returns a CertificateRequest from the store which should be handled. Typically, the request with the oldest
     * nextUpdate will be returned. The caller is responsible for updating the nextUpdate value. If not the same
     * request will be returned the next call.
     */
    CertificateRequest getNextRequest();

    /**
     * Adds a new certificate request to the store
     */
    void addRequest(@Nonnull CertificateRequest request);

    /**
     * Returns the certificate request with the given ID. Null if no request found with ID.
     */
    CertificateRequest getRequest(@Nonnull UUID id);

    /**
     * Remove the certificate request with the given ID.
     */
    void deleteRequest(@Nonnull UUID id);

    /**
     * Returns all the pending certificate requests for the given email address
     */
    List<? extends CertificateRequest> getRequestsByEmail(@Nonnull String email, Match match,
            Integer firstResult, Integer maxResults);

    /**
     * Returns all the pending certificate requests
     */
    List<? extends CertificateRequest> getAllRequests(Integer firstResult, Integer maxResults);

    /**
     * Returns an iterator that iterates over all the pending certificate requests
     */
    CloseableIterator<? extends CertificateRequest> getAllRequests();

    /**
     * Returns the number of certificate requests
     */
    long getSize();

    /**
     * Returns the number of certificate requests with the given email
     */
    long getSizeByEmail(@Nonnull String email, Match match);

    /**
     * Returns the CertificateRequestHandler that created the request. Returns null if the CertificateRequestHandler
     * is no longer available.
     */
    CertificateRequestHandler getCertificateRequestHandler(@Nonnull CertificateRequest request);
}
