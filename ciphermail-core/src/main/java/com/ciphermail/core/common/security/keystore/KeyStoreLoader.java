/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore;

import com.ciphermail.core.common.security.PEMToPKCS12Builder;
import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.security.KeyStore;

/*
 * The KeyStoreLoader class is responsible for loading a KeyStore from an input stream with support for PKC#S12 and PEM
 */
public class KeyStoreLoader
{
    /*
     * The provider to use. If empty, the default JDK provider will be used
     */
    private String provider = SecurityFactoryBouncyCastle.PROVIDER_NAME;

    public @Nonnull KeyStore loadKeyStore(@Nonnull InputStream input, @Nonnull String password)
    throws IOException, GeneralSecurityException
    {
        // detect whether the input is PEM encoded or not
        // the input therefore need to be resettable
        ByteArrayInputStream resettableInput = input instanceof ByteArrayInputStream byteArrayInputStream ?
                byteArrayInputStream : new ByteArrayInputStream(IOUtils.toByteArray(input));

        // try to convert the input from PEM to PKCS12. If that fails, try to import as a PKCS12
        PEMToPKCS12Builder pemToPKCS12Builder = new PEMToPKCS12Builder();

        pemToPKCS12Builder.addPEM(new InputStreamReader(resettableInput), StringUtils.defaultString(password).toCharArray());

        KeyStore keyStore;

        if (!pemToPKCS12Builder.getCertificates().isEmpty())
        {
            // at least one PEM encoded certificate was found
            keyStore = pemToPKCS12Builder.buildPKCS12(password.toCharArray());
        }
        else {
            // try PKCS12
            resettableInput.reset();
            keyStore = KeyStore.getInstance("PKCS12", provider);
            keyStore.load(resettableInput, password.toCharArray());
        }

        return  keyStore;
    }

    public String getProvider() {
        return provider;
    }

    public KeyStoreLoader setProvider(String provider)
    {
        this.provider = provider;
        return this;
    }
}
