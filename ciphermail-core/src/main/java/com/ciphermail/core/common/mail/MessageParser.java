/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.mail.PartScanner.PartListener;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Class that extracts the body and attachments from a message
 *
 * @author Martijn Brinkers
 *
 */
public class MessageParser
{
    /*
     * Max recursion depth
     */
    private static final int MAX_DEPTH = 8;

    /*
     * The attachments of the message
     */
    private final List<Part> attachments = new LinkedList<>();

    /*
     * The text parts (i.e.m, parts with text/plain content type) of the message
     */
    private final List<Part> inlineTextParts = new LinkedList<>();

    /*
     * Is used to iterate  through all the message parts
     */
    private final PartScanner partScanner;

    /*
     * Filters parts
     */
    private PartFilter partFilter;

    /*
     * If an inline text exceeds the maxInlineSize, the text will be treated as an attachment instead of
     * an inline text. There will be no maximum if set to a negative value,
     */
    private int maxInlineSize = -1;

    /**
     * Event fired for every new part. This can be used to filter which parts will be accepted or not
     */
    public interface PartFilter
    {
        /**
         * If true, the part will be accepted
         */
        boolean isAccepted(Part parent, Part part, Object context)
        throws PartException;
    }

    /**
     * @param deepscan if true, the complete message will be scanned including alternative parts
     */
    public MessageParser(boolean deepscan)
    {
        PartListener partListener = MessageParser.this::onPart;

        partScanner = deepscan ? new PartScanner(partListener, MAX_DEPTH) :
            new AlternativePartScanner(partListener, MAX_DEPTH, AlternativePart.TEXT);
    }

    private boolean isInlineTextPartTooLarge(@Nonnull Part part)
    throws MessagingException
    {
        return maxInlineSize >= 0 && part.getSize() > maxInlineSize;
    }

    private boolean onPart(Part parent, @Nonnull Part part, Object context)
    throws PartException
    {
        if (partFilter == null || partFilter.isAccepted(parent, part, context))
        {
            try {
                if (part.isMimeType("text/plain") && MimeUtils.isInline(part, false /* default if error */) &&
                        !isInlineTextPartTooLarge(part))
                {
                    inlineTextParts.add(part);
                }
                else {
                    attachments.add(part);
                }
            }
            catch (MessagingException e) {
                throw new PartException(e);
            }
        }

        return true;
    }

    public void parseMessage(@Nonnull MimeMessage message)
    throws MessagingException, IOException, PartException
    {
        attachments.clear();
        inlineTextParts.clear();

        partScanner.scanPart(message);
    }

    public List<Part> getAttachments() {
        return attachments;
    }

    public List<Part> getInlineTextParts() {
        return inlineTextParts;
    }

    public PartFilter getPartFilter() {
        return partFilter;
    }

    public void setPartFilter(PartFilter partFilter) {
        this.partFilter = partFilter;
    }

    public int getMaxInlineSize() {
        return maxInlineSize;
    }

    public void setMaxInlineSize(int maxInlineSize) {
        this.maxInlineSize = maxInlineSize;
    }
}
