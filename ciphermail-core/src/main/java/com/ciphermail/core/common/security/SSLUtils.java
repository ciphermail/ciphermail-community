/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.certificate.ExtendedKeyUsageType;
import com.ciphermail.core.common.security.certificate.KeyUsageType;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;

import javax.annotation.Nonnull;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * General utility methods for SSL/TLS
 *
 * @author Martijn Brinkers
 *
 */
public class SSLUtils
{
    private SSLUtils() {
        // empty on purpose
    }

    /**
     * Returns true if the certificate can be used for protecting a server with SSL/TLS
     * @throws CertificateParsingException
     */
    public static boolean isValidSSLServerCertificate(X509Certificate certificate)
    throws CertificateParsingException
    {
        if (certificate == null) {
            return false;
        }

        try {
            boolean validForSSL = true;

            X509CertificateInspector inspector = new X509CertificateInspector(certificate);

            Set<ExtendedKeyUsageType> extendedKeyUsage = inspector.getExtendedKeyUsage();

            if (extendedKeyUsage != null && !(extendedKeyUsage.contains(ExtendedKeyUsageType.ANYKEYUSAGE) ||
                    extendedKeyUsage.contains(ExtendedKeyUsageType.SERVERAUTH)))
            {
                validForSSL = false;
            }

            Set<KeyUsageType> keyUsage = inspector.getKeyUsage();

            if (keyUsage != null && !(keyUsage.contains(KeyUsageType.KEYAGREEMENT) ||
                    keyUsage.contains(KeyUsageType.KEYENCIPHERMENT)))
            {
                validForSSL = false;
            }

            return validForSSL;
        }
        catch(IOException e) {
            throw new CertificateParsingException(e);
        }
    }

    /**
     * Returns all trusted system root certificates
     * @throws KeyStoreException
     */
    public static @Nonnull List<X509Certificate> getSystemTrustedRoots()
    throws KeyStoreException
    {
        TrustManagerFactory trustManagerFactory;

        try {
            trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        }
        catch (NoSuchAlgorithmException e) {
            throw new KeyStoreException(e);
        }

        List<X509Certificate> roots = new LinkedList<>();

        trustManagerFactory.init((KeyStore)null);

        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();

        if (trustManagers != null)
        {
            for (TrustManager trustManager : trustManagers)
            {
                if (trustManager instanceof X509TrustManager x509TrustManager) {
                    roots.addAll(Arrays.asList(x509TrustManager.getAcceptedIssuers()));
                }
            }
        }

        return roots;
    }
}
