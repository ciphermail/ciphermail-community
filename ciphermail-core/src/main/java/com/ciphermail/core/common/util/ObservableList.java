/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * A List, wrapping an existing List, that sends out events on add, remove and set.

 * @author Martijn Brinkers
 *
 * @param <E>
 */
public class ObservableList<E> implements List<E>
{
    private static final Logger logger = LoggerFactory.getLogger(ObservableList.class);

    public interface Event<T>
    {
        boolean event(int index, T item)
        throws ObservableRuntimeException;
    }

    private final List<E> source;

    private Event<E> addEvent;
    private Event<Object> removeEvent;
    private Event<E> setEvent;


    public ObservableList(List<E> source) {
        this.source = source;
    }

    public void setAddEvent(Event<E> event) {
        this.addEvent = event;
    }

    public void setRemoveEvent(Event<Object> event) {
        this.removeEvent = event;
    }

    public void setSetEvent(Event<E> event) {
        this.setEvent = event;
    }

    @Override
    public boolean add(E item)
    {
        boolean added = fireAddEvent(-1, item);

        if (added) {
            added = source.add(item);

            if (!added) {
                logger.warn("Collections out of sync (add).");
            }
        }

        return added;
    }

    @Override
    public boolean addAll(Collection<? extends E> items)
    {
        boolean added = false;

        for (E item : items)
        {
            if (add(item)) {
                added = true;
            }
        }

        return added;
    }

    @Override
    public void clear()
    {
        Iterator<E> it = iterator();

        while (it.hasNext())
        {
            it.next();
            it.remove();
        }
    }

    @Override
    public boolean contains(Object item) {
        return source.contains(item);
    }

    @Override
    public boolean containsAll(@Nonnull Collection<?> items)
    {
        return source.containsAll(items);
    }

    @Override
    public boolean isEmpty() {
        return source.isEmpty();
    }

    @Override
    public Iterator<E> iterator() {
        return new ItemIterator(source.iterator());
    }

    @Override
    public boolean remove(Object item)
    {
        boolean removed = fireRemoveEvent(-1, item);

        if (removed) {
            removed = source.remove(item);

            if (!removed) {
                logger.warn("Collections out of sync (remove).");
            }
        }
        return removed;
    }

    @Override
    public boolean removeAll(Collection<?> items)
    {
        boolean removed = false;

        for (Object item : items)
        {
            if (remove(item)) {
                removed = true;
            }
        }

        return removed;
    }

    @Override
    public boolean retainAll(@Nonnull Collection<?> items)
    {
        boolean removed = false;

        Iterator<E> it = iterator();

        while (it.hasNext())
        {
            E item = it.next();

            if (!items.contains(item))
            {
                it.remove();

                removed = true;
            }
        }

        return removed;
    }

    @Override
    public int size() {
        return source.size();
    }

    @Override
    public Object[] toArray() {
        return source.toArray();
    }

    @Override
    public <T> T[] toArray(@Nonnull T[] items) {
        return source.toArray(items);
    }

    @Override
    public void add(int index, E item)
    {
        boolean added = fireAddEvent(index, item);

        if (added) {
            source.add(index, item);
        }
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> items)
    {
        boolean added = false;

        for (E item : items)
        {
            add(index, item);

            added = true;

            index++;
        }

        return added;
    }

    @Override
    public E get(int index)
    {
        return source.get(index);
    }

    @Override
    public int indexOf(Object item)
    {
        return source.indexOf(item);
    }

    @Override
    public int lastIndexOf(Object item)
    {
        return source.lastIndexOf(item);
    }

    @Override
    public ListIterator<E> listIterator()
    {
        return new ItemListIterator(source.listIterator());
    }

    @Override
    public ListIterator<E> listIterator(int index)
    {
        return new ItemListIterator(source.listIterator(index));
    }

    @Override
    public E remove(int index)
    {
        boolean removed = fireRemoveEvent(index, null);

        E removedItem = null;

        if (removed) {
            removedItem = source.remove(index);
        }

        return removedItem;
    }

    @Override
    public E set(int index, E item)
    {
        boolean set = fireSetEvent(index, item);

        E previous = null;

        if (set) {
            previous = source.set(index, item);
        }

        return previous;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex)
    {
        ObservableList<E> subList = new ObservableList<>(source.subList(fromIndex, toIndex));

        subList.setAddEvent(this.addEvent);
        subList.setRemoveEvent(this.removeEvent);

        return subList;
    }

    private boolean fireAddEvent(int index, E item)
    {
        boolean added = true;

        if (addEvent != null) {
            added = addEvent.event(index, item);
        }

        return added;
    }

    private boolean fireRemoveEvent(int index, Object item)
    {
        boolean removed = true;

        if (removeEvent != null) {
            removed = removeEvent.event(index, item);
        }

        return removed;
    }

    private boolean fireSetEvent(int index, E item)
    {
        boolean set = true;

        if (setEvent != null) {
            set = setEvent.event(index, item);
        }

        return set;
    }

    private class ItemIterator implements Iterator<E>
    {
        private final Iterator<E> source;

        private E next;

        public ItemIterator(Iterator<E> source) {
            this.source = source;
        }

        @Override
        public boolean hasNext() {
            return source.hasNext();
        }

        @Override
        public E next()
        {
            next = source.next();

            return next;
        }

        @Override
        public void remove()
        {
            boolean removed = fireRemoveEvent(-1, next);

            if (removed) {
                source.remove();
            }
        }
    }

    private class ItemListIterator implements ListIterator<E>
    {
        private final ListIterator<E> source;

        private E current;

        public ItemListIterator(ListIterator<E> source) {
            this.source = source;
        }

        @Override
        public void add(E item)
        {
            int index = getCurrentIndex();

            boolean added = fireAddEvent(index, item);

            if (added) {
                source.add(item);
            }

        }

        @Override
        public boolean hasNext() {
            return source.hasNext();
        }

        @Override
        public boolean hasPrevious() {
            return source.hasPrevious();
        }

        @Override
        public E next()
        {
            current = source.next();

            return current;
        }

        @Override
        public int nextIndex() {
            return source.nextIndex();
        }

        @Override
        public E previous()
        {
            current = source.previous();

            return current;
        }

        @Override
        public int previousIndex() {
            return source.previousIndex();
        }

        @Override
        public void remove()
        {
            int index = getCurrentIndex();

            boolean removed = fireRemoveEvent(index, current);

            if (removed) {
                source.remove();
            }
        }

        @Override
        public void set(E item)
        {
            int index = getCurrentIndex();

            boolean set = fireSetEvent(index, item);

            if (set) {
                source.set(item);
            }
        }

        private int getCurrentIndex()
        {
            int index = nextIndex() - 1;

            if (index < 0) {
                index = 0;
            }

            return index;
        }
    }
}
