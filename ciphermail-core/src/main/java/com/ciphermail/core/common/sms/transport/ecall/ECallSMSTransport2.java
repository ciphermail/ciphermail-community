/*
 * Copyright (c) 2016-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.transport.ecall;

import com.ciphermail.core.common.http.AbstractVoidBinResponseConsumer;
import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactory;
import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HttpClientContextFactory;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.sms.SMSTransport;
import com.ciphermail.core.common.util.LimitReachedException;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.PhoneNumberUtils;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleRequestBuilder;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ConnectionClosedException;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.nio.support.BasicRequestProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * SMSTransport implementation for new eCall API (https://www.ecall.ch)
 *
 * @author Martijn Brinkers
 *
 */
public class ECallSMSTransport2 implements SMSTransport
{
    private static final Logger logger = LoggerFactory.getLogger(ECallSMSTransport2.class);

    private static final String TIMEOUT_ERROR = "A timeout has occurred connecting to: ";
    private static final String MAX_RESPOSNE_SIZE_EXCEEDED_ERROR = "Max HTTP response size exceeded.";

    /*
     * Maximum allowed size of a response from the API.
     */
    private int maxResponseSize = SizeUtils.KB * 100;

    /*
     * Max time the request may take
     */
    private long totalTimeout = DateUtils.MILLIS_PER_MINUTE;

    /*
     * Creates CloseableHttpAsyncClient instances
     */
    private final CloseableHttpAsyncClientFactory httpClientFactory;

    /*
     * Creates HttpClientContextFactory instances
     */
    private final HttpClientContextFactory httpClientContextFactory;

    /*
     * Provides the eCall API settings
     */
    private final ECallPropertiesProvider propertiesProvider;

    /*
     * The HTTP client instance responsible for HTTP(s) communication
     */
    private CloseableHttpAsyncClient sharedHttpClient;

    /*
     * The URL for sending SMS messages. The URL should have the following form:
     *
     * https://url.ecall.ch/api/sms?address=xxx&username=xxx&password=xxx&message=test
     */
    private String url = "https://url.ecall.ch/api/sms";

    /*
     * Additional HTTP parameters which will be added to the URL
     */
    private final Map<String, String> additionalParameters = Collections.synchronizedMap(new HashMap<>());

    public ECallSMSTransport2(
            @Nonnull CloseableHttpAsyncClientFactory httpClientFactory,
            @Nonnull HttpClientContextFactory httpClientContextFactory,
            @Nonnull ECallPropertiesProvider propertiesProvider)
    {
        this.httpClientFactory = Objects.requireNonNull(httpClientFactory);
        this.httpClientContextFactory = Objects.requireNonNull(httpClientContextFactory);
        this.propertiesProvider = Objects.requireNonNull(propertiesProvider);
    }

    @Override
    public String getName() {
        return "eCall2";
    }

    private synchronized CloseableHttpAsyncClient getHTTPClient()
    throws IOException, GeneralSecurityException
    {
        if (sharedHttpClient == null) {
            sharedHttpClient = httpClientFactory.createClient();
        }

        return sharedHttpClient;
    }

    private void parseResponse(byte[] response, String phoneNumber, AbstractVoidBinResponseConsumer responseConsumer)
    throws IOException
    {
        int httpStatusCode = responseConsumer.getHttpStatusCode();

        String responseBody = MiscStringUtils.toStringFromUTF8Bytes(response);

        logger.debug("{} response: {}", getName(), responseBody);

        if (httpStatusCode >= HttpStatus.SC_CLIENT_ERROR)
        {
            String httpStatusReasonPhrase = responseConsumer.getHttpStatusReasonPhrase();

            logger.error("{} Error sending SMS. Status: {}, Reason: {}", getName(), httpStatusCode,
                    httpStatusReasonPhrase);

            throw new IOException(getName() + " error sending SMS. " +
                    "Status: " + httpStatusCode + ", " +
                    "Reason: " + httpStatusReasonPhrase);
        }

        if (StringUtils.isNotEmpty(responseBody))
        {
            logger.info("SMS successfully delivered to {} using {}. Response: {}", getName(), phoneNumber,
                    responseBody);
        }
        else {
            throw new IOException("Response body is empty. Status: " + httpStatusCode);
        }
    }

    @Override
    public void sendSMS(String phoneNumber, String message)
    throws IOException
    {
        if (StringUtils.isEmpty(phoneNumber)) {
            throw new IOException("phoneNumber is not set");
        }

        if (StringUtils.isEmpty(message)) {
            throw new IOException("message is not set");
        }

        phoneNumber = PhoneNumberUtils.normalizeAndValidatePhoneNumber(phoneNumber);

        if (StringUtils.isEmpty(phoneNumber)) {
            throw new IOException("phoneNumber is not valid");
        }

        try {
            ECallProperties properties = propertiesProvider.getProperties();

            String accountName = properties.getAccountName();

            if (StringUtils.isEmpty(accountName)) {
                throw new IOException("Account Name is not set");
            }

            String accountPassword = properties.getAccountPassword();

            if (StringUtils.isEmpty(accountPassword)) {
                throw new IOException("Account Password is not set");
            }

            SimpleRequestBuilder requestBuilder = SimpleRequestBuilder.get(url)
                    .addParameter("message", message)
                    .addParameter("address", phoneNumber)
                    .addParameter("username", accountName)
                    .addParameter("password", accountPassword);

            // Add additional parameters (if set)
            for (Entry<String, String>  additionalParameter : additionalParameters.entrySet()) {
                requestBuilder.addParameter(additionalParameter.getKey(), additionalParameter.getValue());
            }

            SimpleHttpRequest request = requestBuilder.build();

            CloseableHttpAsyncClient httpClient = getHTTPClient();

            HttpClientContext clientContext = httpClientContextFactory.createHttpClientContext();

            ByteArrayOutputStream response = new ByteArrayOutputStream();

            AbstractVoidBinResponseConsumer consumer;

            try(
                WritableByteChannel outputChannel = Channels.newChannel(new SizeLimitedOutputStream(response,
                        maxResponseSize)))
            {
                consumer = new AbstractVoidBinResponseConsumer()
                {
                    @Override
                    protected void data(ByteBuffer src, boolean endOfStream)
                    throws IOException
                    {
                        try {
                            outputChannel.write(src);
                        }
                        catch (LimitReachedException e) {
                            // If the limit was reached, we do not want HTTPClient to retry. The
                            // @DefaultHttpRequestRetryStrategy will not retry if the exception is a
                            // ConnectionClosedException
                            throw new ConnectionClosedException(MAX_RESPOSNE_SIZE_EXCEEDED_ERROR, e);
                        }
                    }
                };

                Future<Void> future = httpClient.execute(new BasicRequestProducer(request, null), consumer, clientContext,
                        null);

                future.get(totalTimeout, TimeUnit.MILLISECONDS);
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();

                throw new IOException(e);
            }
            catch (ExecutionException e) {
                throw new IOException(ExceptionUtils.getRootCause(e));
            }
            catch (TimeoutException e) {
                throw new IOException(TIMEOUT_ERROR + url);
            }

            parseResponse(response.toByteArray(), phoneNumber, consumer);
        }
        catch (HierarchicalPropertiesException | GeneralSecurityException e) {
            throw new IOException(getName() + " error sending SMS. " + e.getMessage(), e);
        }
    }

    public int getMaxResponseSize() {
        return maxResponseSize;
    }

    public void setMaxResponseSize(int maxResponseSize) {
        this.maxResponseSize = maxResponseSize;
    }

    public long getTotalTimeout() {
        return totalTimeout;
    }

    public void setTotalTimeout(long totalTimeout) {
        this.totalTimeout = totalTimeout;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static void main(String[] args)
    throws Exception
    {
        StaticECallPropertiesProvider propertiesProvider = new StaticECallPropertiesProvider();

        if (args == null || args.length != 3) {
            throw new IllegalArgumentException("username, password and phone nr required");
        }

        String accountName = StringUtils.trim(args[0]);
        String accountPassword = StringUtils.trim(args[1]);
        String phoneNumber = StringUtils.trim(args[2]);

        if (!phoneNumber.startsWith("+")) {
            phoneNumber = "+" + phoneNumber;
        }

        propertiesProvider.setAccountName(accountName);
        propertiesProvider.setAccountPassword(accountPassword);

        ECallSMSTransport2 transport = new ECallSMSTransport2(new CloseableHttpAsyncClientFactoryImpl(null),
                new HttpClientContextFactoryImpl(null),
                propertiesProvider);

        transport.sendSMS(phoneNumber, "test API2-2" + System.currentTimeMillis());
    }
}
