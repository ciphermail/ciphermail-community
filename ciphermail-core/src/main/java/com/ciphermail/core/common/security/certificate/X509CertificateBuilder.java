/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import org.bouncycastle.asn1.x509.GeneralNames;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

public interface X509CertificateBuilder
{
    void setVersion(CertificateVersion version);
    CertificateVersion getVersion();

    void setSerialNumber(BigInteger serialNumber);
    BigInteger getSerialNumber();

    void setSubject(X500Principal subject);
    X500Principal getSubject();

    void setIssuer(X500Principal issuer);
    X500Principal getIssuer();

    void setNotBefore(Date date);
    Date getNotBefore();

    void setNotAfter(Date date);
    Date getNotAfter();

    void setSignatureAlgorithm(String signatureAlgorithm);
    String getSignatureAlgorithm();

    void setAltNames(GeneralNames altNames, boolean critical);
    GeneralNames getAltNames();
    boolean isAltNamesCritical();

    void setKeyUsage(Set<KeyUsageType> keyUsage, boolean critical);
    Set<KeyUsageType> getKeyUsage();
    boolean isKeyUsageCritical();

    void setExtendedKeyUsage(Set<ExtendedKeyUsageType> keyUsage, boolean critical);
    Set<ExtendedKeyUsageType> getExtendedKeyUsage();
    boolean isExtendedKeyUsageCritical();

    void setIsCA(boolean isCA, boolean critical);
    boolean isCA();
    boolean isCACritical();

    void addSubjectKeyIdentifier(boolean add);
    boolean isAddSubjectKeyIdentifier();

    void addAuthorityKeyIdentifier(boolean add);
    boolean isAddAuthorityKeyIdentifier();

    void setPathLengthConstraint(Integer pathLengthConstraint);
    Integer getPathLengthConstraint();

    void setPublicKey(PublicKey publicKey);
    PublicKey getPublicKey();

    void setCRLDistributionPoints(Collection<String> uris);

    /**
     * Generates a certificate with issuerCertificate as issuer. If issuerCertificate is null
     * Issuer property is used
     *
     * @param issuerPrivateKey
     * @param issuerCertificate
     * @return
     */
    X509Certificate generateCertificate(@Nonnull PrivateKey issuerPrivateKey, X509Certificate issuerCertificate)
    throws CertificateBuilderException;
}
