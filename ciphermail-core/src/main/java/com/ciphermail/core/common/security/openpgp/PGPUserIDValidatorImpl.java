/*
 * Copyright (c) 2014-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of PGPUserIDValidator
 *
 * @author Martijn Brinkers
 *
 */
public class PGPUserIDValidatorImpl implements PGPUserIDValidator
{
    private static final Logger logger = LoggerFactory.getLogger(PGPUserIDValidatorImpl.class);

    /*
     * Is used to validate the User ID signature(s)
     */
    private final PGPSignatureValidator signatureValidator;

    public PGPUserIDValidatorImpl(@Nonnull PGPSignatureValidator signatureValidator) {
        this.signatureValidator = Objects.requireNonNull(signatureValidator);
    }

    private boolean isUserIDSignatureValid(PGPPublicKey publicKey, byte[] userID, PGPSignature signature)
    {
        boolean valid = false;

        try {
            valid = signatureValidator.validateUserIDSignature(publicKey, userID, signature);
        }
        catch (Exception e)
        {
            logger.debug("Error validating User ID signature of User ID {} for key with Key ID {}",
                         userID, PGPUtils.getKeyIDHex(publicKey.getKeyID()), e);
        }

        return valid;
    }

    private boolean isUserIDRevocationSignatureValid(PGPPublicKey publicKey, byte[] userID, PGPSignature signature)
    {
        boolean valid = false;

        try {
            valid = signatureValidator.validateUserIDRevocationSignature(publicKey, userID, signature);
        }
        catch (Exception e)
        {
            logger.debug("Error validating User ID revocation signature of User ID {} for key with Key ID {}",
                    userID, PGPUtils.getKeyIDHex(publicKey.getKeyID()), e);
        }

        return valid;
    }

    @Override
    public boolean validateUserID(@Nonnull byte[] userID, @Nonnull PGPPublicKey publicKey)
    throws PGPUserIDRevokedException
    {
        String keyIDHex = PGPUtils.getKeyIDHex(publicKey.getKeyID());
        String userIDString = PGPUtils.userIDToString(userID);

        boolean valid = false;

        List<PGPSignature> signatures = PGPSignatureInspector.getSignaturesForUserID(publicKey, userID);

        if (CollectionUtils.isNotEmpty(signatures))
        {
            for (PGPSignature signature : signatures)
            {
                if (valid) {
                    break;
                }

                if (!ArrayUtils.contains(PGPSignatureInspector.USER_ID_CERTIFICATION_TYPES,
                        signature.getSignatureType()))
                {
                    // Do not log a User ID revocation signature since this is a normal signature for a User ID
                    if (signature.getSignatureType() != PGPSignature.CERTIFICATION_REVOCATION)
                    {
                        logger.warn("Signature with signature type {} from Key with ID {} is not a User ID signature.",
                                signature.getSignatureType(), keyIDHex);
                    }

                    continue;
                }

                if (isUserIDSignatureValid(publicKey, userID, signature))
                {
                    logger.debug("Signature with signature type {} from Key with ID {} is valid.",
                            signature.getSignatureType(), keyIDHex);

                    // Signature is valid. Now check whether the User ID is not revoked
                    List<PGPSignature> userIDSigs = PGPSignatureInspector.getSignaturesForUserID(publicKey, userID);

                    if (CollectionUtils.isNotEmpty(userIDSigs))
                    {
                        for (PGPSignature userIDSig : userIDSigs)
                        {
                            if (userIDSig.getSignatureType() == PGPSignature.CERTIFICATION_REVOCATION)
                            {
                                if (isUserIDRevocationSignatureValid(publicKey, userID, userIDSig))
                                {
                                    logger.warn("User ID '{}' from Key with ID {} is revoked.",
                                            userIDString, keyIDHex);

                                    throw new PGPUserIDRevokedException(userIDString, publicKey.getKeyID(),
                                            "User ID '" + userIDString + "' from Key with ID " + keyIDHex +
                                            " is revoked.");
                                }
                                else {
                                    logger.debug("User ID '{}' revocation signature from Key with ID {} is not valid.",
                                            userIDString, keyIDHex);
                                }
                            }
                        }

                        valid = true;
                    }
                    else {
                        // Should never happen since PGPSignatureValidator#validateUserIDSignature also needs a
                        // user ID signature
                        logger.warn("No User ID signatures found.");
                    }
                }
                else {
                    logger.debug("User ID '{}' with signature type {} from Key with ID {} is not valid.",
                            userIDString, signature.getSignatureType(), keyIDHex);
                }
            }
        }
        else {
            logger.warn("No signatures found for User ID '{}' from Key with ID {}.", userIDString, keyIDHex);
        }

        return valid;
    }
}
