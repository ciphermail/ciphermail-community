/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.pdf;

import com.lowagie.text.pdf.PdfWriter;

public enum ViewerPreference
{
    NONE                                   (0,                                          "none"),
    PAGE_LAYOUT_SINGLE_PAGE                (PdfWriter.PageLayoutSinglePage,             "pageLayoutSinglePage"),
    PAGE_LAYOUT_ONE_COLUMN                 (PdfWriter.PageLayoutOneColumn,              "pageLayoutOneColumn"),
    PAGE_LAYOUT_TWO_COLUMN_LEFT            (PdfWriter.PageLayoutTwoColumnLeft,          "pageLayoutTwoColumnLeft"),
    PAGE_LAYOUT_TWO_COLUMN_RIGHT           (PdfWriter.PageLayoutTwoColumnRight,         "pageLayoutTwoColumnRight"),
    PAGE_LAYOUT_TWO_PAGE_LEFT              (PdfWriter.PageLayoutTwoPageLeft,            "pageLayoutTwoPageLeft"),
    PAGE_LAYOUT_TWO_PAGE_RIGHT             (PdfWriter.PageLayoutTwoPageRight,           "pageLayoutTwoPageRight"),
    PAGE_MODE_USE_NONE                     (PdfWriter.PageModeUseNone,                  "pageModeUseNone"),
    PAGE_MODE_USE_OUTLINES                 (PdfWriter.PageModeUseOutlines,              "pageModeUseOutlines"),
    PAGE_MODE_USE_THUMBS                   (PdfWriter.PageModeUseThumbs,                "pageModeUseThumbs"),
    PAGE_MODE_FULL_SCREEN                  (PdfWriter.PageModeFullScreen,               "pageModeFullScreen"),
    PAGE_MODE_USEOC                        (PdfWriter.PageModeUseOC,                    "pageModeUseOC"),
    PAGE_MODE_USE_ATTACHMENTS              (PdfWriter.PageModeUseAttachments,           "pageModeUseAttachments"),
    HIDE_TOOLBAR                           (PdfWriter.HideToolbar,                      "hideToolbar"),
    HIDE_MENUBAR                           (PdfWriter.HideMenubar,                      "hideMenubar"),
    HIDE_WINDOWUI                          (PdfWriter.HideWindowUI,                     "hideWindowUI"),
    FIT_WINDOW                             (PdfWriter.FitWindow,                        "fitWindow"),
    CENTER_WINDOW                          (PdfWriter.CenterWindow,                     "centerWindow"),
    DISPLAY_DOC_TITLE                      (PdfWriter.DisplayDocTitle,                  "displayDocTitle"),
    NON_FULL_SCREEN_PAGE_MODE_USE_NONE     (PdfWriter.NonFullScreenPageModeUseNone,     "nonFullScreenPageModeUseNone"),
    NON_FULL_SCREEN_PAGE_MODE_USE_OUTLINES (PdfWriter.NonFullScreenPageModeUseOutlines, "nonFullScreenPageModeUseOutlines"),
    NON_FULL_SCREEN_PAGE_MODE_USE_THUMBS   (PdfWriter.NonFullScreenPageModeUseThumbs,   "nonFullScreenPageModeUseThumbs"),
    NON_FULL_SCREEN_PAGE_MODE_USE_OC       (PdfWriter.NonFullScreenPageModeUseOC,       "nonFullScreenPageModeUseOC"),
    DIRECTION_L2R                          (PdfWriter.DirectionL2R,                     "directionL2R"),
    DIRECTION_R2L                          (PdfWriter.DirectionR2L,                     "directionR2L"),
    PRINT_SCALING_NONE                     (PdfWriter.PrintScalingNone,                 "printScalingNone");

    private final int intPreference;
    private final String friendlyName;

    ViewerPreference(int intPreference, String friendlyName)
    {
        this.intPreference = intPreference;
        this.friendlyName = friendlyName;
    }

    public int intValue() {
        return intPreference;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
