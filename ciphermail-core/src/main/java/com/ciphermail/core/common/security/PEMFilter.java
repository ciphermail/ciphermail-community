/*
 * Copyright (c) 2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.Objects;

/**
 * Utility class which can be used to only return supported PEM blocks from the input. This can be used to filter
 * additional text from a file which contains PEM encoded certificates etc.
 *
 * The PEM filter will return the content between the supported start and end header. The content of the PEM blocks
 * will not be validated (for example it will not be checked if the PEM block is base64 encoded). All returned lines
 * from a PEM block will also be trimmed.
 */
public class PEMFilter
{
    public static class HeaderFooter
    {
        private final String header;
        private final String footer;

        public HeaderFooter(String type)
        {
            this.header = "-----BEGIN " + type + "-----";
            this.footer = "-----END " + type + "-----";
        }
    }

    /**
     * The default supported PEM header/footers for certificates
     */
    public static final HeaderFooter[] DEFAULT_CERTIFICATE_HEADER_FOOTERS = new HeaderFooter[] {
            new HeaderFooter("CERTIFICATE"),
            new HeaderFooter("X509 CERTIFICATE"),
            new HeaderFooter("PKCS7"),
    };

    private final HeaderFooter[] supportedHeaderFooters;

    public PEMFilter(@Nonnull HeaderFooter[] supportedHeaderFooters) {
        this.supportedHeaderFooters = Objects.requireNonNull(supportedHeaderFooters);
    }

    private HeaderFooter getSupportedHeader(String line)
    {
        for (HeaderFooter headerFooter : supportedHeaderFooters)
        {
            if (headerFooter.header.equals(line)) {
                return headerFooter;
            }
        }

        return null;
    }

    /**
     * Returns supported PEM parts found in the input stream. All non PEM characters or PEM parts which are not
     * supported will not be included
     * @throws IOException
     */
    public String readPEM(@Nonnull InputStream input, String charsetName)
    throws IOException
    {
        return readPEM(new InputStreamReader(input, charsetName));
    }

    /**
     * Returns supported PEM parts found in the input stream. All non PEM characters or PEM parts which are not
     * supported will not be included
     * @throws IOException
     */
    public String readPEM(@Nonnull Reader input)
    throws IOException
    {
        LineNumberReader lineReader = new LineNumberReader(input);

        StrBuilder pem = new StrBuilder();

        HeaderFooter currentHeaderFooter = null;

        String line;

        while ((line = lineReader.readLine()) != null)
        {
            line = StringUtils.trimToNull(line);

            if (line == null) {
                continue;
            }

            // Check if we are in a supported PEM block
            if (currentHeaderFooter == null)
            {
                currentHeaderFooter = getSupportedHeader(line);

                if (currentHeaderFooter != null) {
                    pem.appendln(line);
                }
            }
            else {
                // We are inside a supported PEM block. Check if end footer is found
                if (currentHeaderFooter.footer.equals(line)) {
                    // end found
                    pem.appendln(line);

                    currentHeaderFooter = null;
                }
                else {
                    // no so add line
                    pem.appendln(line);
                }
            }
        }

        return pem.toString();
    }
}
