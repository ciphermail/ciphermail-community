/*
 * Copyright (c) 2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.keyserver;

import com.ciphermail.core.common.security.openpgp.PGPPublicKeyAlgorithm;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;

/**
 * Information about a key result returned from a indexed search on a key server.
 *
 * @author Martijn Brinkers
 *
 */
public interface KeyServerKeyInfo
{
    /**
     * The URL of the server that returned the result
     */
    @Nonnull String getServerURL();

    /**
     * this is either the fingerprint or the key ID of the key.  Either the 16-digit or 8-digit key IDs are
     * acceptable, but obviously the fingerprint is best.  A keyserver should use the most specific of the key IDs
     * that it has available.  Since it is not possible to calculate the key ID from a V3 key fingerprint, for V3
     * keys this should be either the 16-digit or 8-digit key ID only.
     */
    @Nonnull String getKeyID();

    /**
     * The Key algorithm
     */
    PGPPublicKeyAlgorithm getAlgorithm();

    /**
     * the key length (i.e. 1024, 2048, 4096, etc.)
     */
    int getKeyLength();

    /**
     * creation date of the key in standard RFC-4880 form (i.e. number of seconds since 1/1/1970 UTC time)
     */
    Date getCreationDate();

    /**
     * expiration date of the key in standard RFC-4880 form (i.e. number of seconds since 1/1/1970 UTC time)
     */
    Date getExpirationDate();

    /**
     * letter codes to indicate details of the key, if any. Flags may be in any order.  The meaning of "disabled"
     * is implementation-specific.  Note that individual flags may be unimplemented, so the absence of a given
     * flag does not necessarily mean the absence of the detail.
     * <p>
     * r == revoked
     * d == disabled
     * e == expired
     */
    String getFlags();

    /**
     * The User IDs on the key
     */
    @Nonnull List<String> getUserIDs();
}
