/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certificate.X509ExtensionInspector;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilder;
import com.ciphermail.core.common.security.certstore.BasicCertStore;
import com.ciphermail.core.common.security.certstore.CertStoreUtils;
import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CollectionUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.URI;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.security.cert.X509Extension;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of CRLStoreUpdater.
 *
 * Note: this class manages it's own transactions. The main reason for this is that downloading CRLs can be time
 * and memory consuming and doing this in one transaction is not recommended.
 */
public class CRLStoreUpdaterImpl implements CRLStoreUpdater
{
    private static final Logger logger = LoggerFactory.getLogger(CRLStoreUpdaterImpl.class);

    /*
     * All the relevant stores, parameters etc. required for getting the CRLs dist points
     * and for downloading all CRLs
     */
    private final CRLStoreUpdaterParameters updaterParameters;

    /*
     * Helper for executing methods wrapped in a database transaction
     */
    private final TransactionOperations transactionOperations;

    public CRLStoreUpdaterImpl(@Nonnull CRLStoreUpdaterParameters updaterParameters,
            TransactionOperations transactionOperations)
    {
        this.updaterParameters = Objects.requireNonNull(updaterParameters);
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
    }

    /*
     * Check if the certificate is trusted up to a Trust Anchor.
     */
    private boolean isTrusted(@Nonnull X509Certificate certificate)
    throws NoSuchProviderException
    {
        boolean trusted = false;

        try {
            CertificatePathBuilder pathBuilder = updaterParameters.getCertificatePathBuilderFactory().
                    createCertificatePathBuilder();

            // add the certificate to the PathBuilder to make sure it is found by the X509CertSelector
            try {
                pathBuilder.addCertStore(CertStoreUtils.createCertStore(certificate));
            }
            catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException e) {
                throw new CertPathBuilderException(e);
            }

            CertPathBuilderResult result = pathBuilder.buildPath(certificate);

            if (result == null) {
                throw new CertPathBuilderException("No valid CertPath found.");
            }

            trusted = true;
        }
        catch (CertPathBuilderException e) {
            // CertPathBuilderException is thrown for a lot of reasons so we will try to extract the reason.
            // Because this exception can happen frequently under 'normal' circumstances
            // (for example when a root is not installed) we will log the messages with the WARN level.
            Throwable rootCause = ExceptionUtils.getRootCause(e);

            Throwable cause = (rootCause != null ? rootCause : e);

            if (cause instanceof CertificateExpiredException)
            {
                logger.warn("Certificate is expired. Certificate: {}. Message: {}",
                        X509CertificateInspector.toString(certificate), cause.getMessage());
            }
            else {
                String errorMessage = "Error while building path for certificate. Certificate: " +
                        X509CertificateInspector.toString(certificate);

                if (logger.isDebugEnabled()) {
                    logger.warn(errorMessage, cause);
                }
                else {
                    logger.warn("{}. Message: {}", errorMessage, cause.getMessage());
                }
            }
        }

        return trusted;
    }

    /*
     * Check if the CRL is trusted up to a Trust Anchor.
     */
    private boolean isTrusted(@Nonnull X509CRL crl)
    {
        boolean trusted = false;

        try {
            updaterParameters.getCRLPathBuilderFactory().createCRLPathBuilder().buildPath(crl);

            trusted = true;
        }
        catch (CertPathBuilderException e) {
            // CertPathBuilderException is thrown for a lot of reasons so we will try to extract the reason.
            // Because this exception can happen frequently under 'normal' circumstances
            // (for example when a root is not installed) we will log the messages with the WARN level.
            Throwable rootCause = ExceptionUtils.getRootCause(e);

            Throwable cause = (rootCause != null ? rootCause : e);

            if (cause instanceof CertificateExpiredException) {
                logger.warn("CRL is expired. CRL: {}. Message: {}", X509CRLInspector.toString(crl), cause.getMessage());
            }
            else {
                String errorMessage = "Error while building path for CRL. CRL: " + X509CRLInspector.toString(crl);

                if (logger.isDebugEnabled()) {
                    logger.warn(errorMessage, cause);
                }
                else {
                    logger.warn("{}. Message: {}", errorMessage, cause.getMessage());
                }
            }
        }
        catch (CRLStoreException e) {
            logger.error("Error while building path for CRL.", e);
        }

        return trusted;
    }

    private boolean isTrusted(@Nonnull X509Extension extension)
    throws NoSuchProviderException
    {
        if (extension instanceof X509Certificate x509Certificate) {
            return isTrusted(x509Certificate);
        }
        else if (extension instanceof X509CRL x509CRL) {
            return isTrusted(x509CRL);
        }
        else {
            throw new IllegalArgumentException("Unsupported extension.");
        }
    }

    private void addURIsFromCRLDistPoint(@Nonnull X509Extension extension, CRLDistPoint crlDistPoint, Set<URI> uris)
    throws NoSuchProviderException, CRLException
    {
        if (crlDistPoint != null)
        {
            Set<URI> certificateURIs = CRLUtils.getAllDistributionPointURIs(crlDistPoint);

            for (URI uri : certificateURIs)
            {
                if (!uris.contains(uri))
                {
                    // if it's a new uri we must check the validity of the object
                    if (updaterParameters.checkTrust() && !isTrusted(extension)) {
                        logger.debug("Certificate or CRL is not trusted so CRL will not be downloaded. URI: {}", uri);
                    }
                    else {
                        uris.add(uri);
                    }
                }
            }
        }
    }

    private void addURIsFromExtension(@Nonnull X509Extension extension, Set<URI> uris)
    throws NoSuchProviderException, CRLException
    {
        try {
            CRLDistPoint crlDistPoint = X509ExtensionInspector.getCRLDistibutionPoints(extension);

            addURIsFromCRLDistPoint(extension, crlDistPoint, uris);
        }
        catch (IOException e) {
            logger.error("Error getting CRL Distibution Points for:" +
                    SystemUtils.LINE_SEPARATOR + extension , e);
        }

        try {
            CRLDistPoint crlDistPoint = X509ExtensionInspector.getFreshestCRL(extension);

            addURIsFromCRLDistPoint(extension, crlDistPoint, uris);
        }
        catch (IOException e) {
            logger.error("Error getting Freshest CRL distibution Points for:" +
                    SystemUtils.LINE_SEPARATOR + extension , e);
        }
    }

    private void addURIsFromCertificates(@Nonnull CloseableIterator<? extends Certificate> iterator, Set<URI> uris)
    throws NoSuchProviderException, CloseableIteratorException
    {
        while (iterator.hasNext())
        {
            try {
                Certificate certificate = iterator.next();

                if (!(certificate instanceof X509Extension))
                {
                    logger.warn("Certificate is not a X509Extension.");

                    continue;
                }

                addURIsFromExtension((X509Extension) certificate, uris);
            }
            catch(Exception e) {
                logger.error("Error getting URIs from certificate. Skipping certificate.", e);
            }
        }
    }

    private void addURIsFromCRLs(@Nonnull CloseableIterator<? extends CRL> iterator, Set<URI> uris)
    throws NoSuchProviderException, CloseableIteratorException
    {
        while (iterator.hasNext())
        {
            try {
                CRL crl = iterator.next();

                if (!(crl instanceof X509Extension))
                {
                    logger.warn("CRL is not a X509Extension.");

                    continue;
                }

                addURIsFromExtension((X509Extension) crl, uris);
            }
            catch(Exception e) {
                logger.error("Error getting URIs from CRL. Skipping CRL.", e);
            }
        }
    }

    private int downloadCRLs(@Nonnull Set<URI> uris)
    {
        int crlsAdded = 0;

        for (URI uri : uris)
        {
            if (uri == null)
            {
                logger.warn("URL is null.");

                continue;
            }

            if (uri.getScheme() == null) {
                logger.warn("Missing scheme. {}", uri);
            }

            logger.debug("Trying to download CRL from {}", uri);

            try {
                Collection<? extends CRL> downloadedCrls = updaterParameters.getCRLDownloader().downloadCRLs(uri);

                if (CollectionUtils.isNotEmpty(downloadedCrls)) {
                    logger.info("Successfully downloaded CRL from {}", uri);
                }

                // add new CRL within a new transaction
                Integer newlyAdded = transactionOperations.execute(tx ->
                {
                    int added = 0;

                    try {
                        added = updaterParameters.getCRLStoreMaintainer().addCRLs(downloadedCrls);
                    }
                    catch (CRLStoreException e) {
                        logger.error("Error adding CRL. URI: {}", uri, e);
                    }

                    return added;
                });

                crlsAdded = crlsAdded + ObjectUtils.defaultIfNull(newlyAdded, 0);
            }
            catch (CRLException e) {
                logger.error("Error handling CRL. URI: {}", uri, e);
            }
            catch (IOException e)
            {
                // log WARN level because downloading CRLs often result in IOException's because the CRL
                // distribution point is no longer available for a lot of roots.
                logger.warn("IO Exception downloading CRL. URI: {}. Message: {}", uri, e.getMessage());

                if (logger.isDebugEnabled()) {
                    logger.debug("More info.", e);
                }
            }
            catch (Exception e) {
                // Catch all exceptions to make sure that other CRLs are downloaded.
                logger.error("Error while downloading CRL.  URI: {}", uri, e);
            }
        }

        return crlsAdded;
    }

    @Override
    public void update()
    throws CRLStoreException
    {
        Set<URI> allURIs = transactionOperations.execute(tx ->
        {
            try {
                Set<URI> uris = new HashSet<>();

                Collection<? extends BasicCertStore> stores = updaterParameters.getCertStores();

                for (BasicCertStore store : stores) {
                    // Add CRL distribution points from the certificates in the CertStore
                    CloseableIterator<? extends Certificate> certificateIterator = store.
                            getCertificateIterator(null);

                    try {
                        addURIsFromCertificates(certificateIterator, uris);
                    }
                    finally {
                        certificateIterator.close();
                    }

                    // Add CRL distribution points from the CRLs in the CRLStore
                    CloseableIterator<? extends CRL> crlIterator = updaterParameters.
                            getCRLStore().getCRLIterator(null);

                    try {
                        addURIsFromCRLs(crlIterator, uris);
                    }
                    finally {
                        crlIterator.close();
                    }
                }

                return uris;
            }
            catch (NoSuchProviderException | CloseableIteratorException | CertStoreException | CRLStoreException e) {
                throw new UnhandledException(e);
            }
        });

        if (allURIs == null)
        {
            // should never happen
            logger.warn("allURIs is null");

            return;
        }

        logger.info("{} CRL distibution points found.", allURIs.size());

        int crlsAdded = downloadCRLs(allURIs);

        logger.info("{} new CRLs added to the CRL store.", crlsAdded);
    }
}