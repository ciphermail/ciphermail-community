/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.repository.hibernate;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.AbstractScrollableResultsIterator;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.mail.repository.MailRepositorySearchField;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;

/**
 * DAO for MailRepository.
 *
 * @author Martijn Brinkers
 *
 */
public class MailRepositoryDAO extends GenericHibernateDAO
{
    private static final String ENTITY_NAME = MailRepositoryEntity.ENTITY_NAME;

    private static class MailRepositoryItemEntityIterator extends AbstractScrollableResultsIterator<MailRepositoryEntity>
    {
        public MailRepositoryItemEntityIterator(ScrollableResults<MailRepositoryEntity> results) {
            super(results);
        }

        @Override
        protected boolean hasMatch(MailRepositoryEntity element) {
            return element != null;
        }
    }

    private MailRepositoryDAO(@Nonnull SessionAdapter session) {
        super(session);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static MailRepositoryDAO getInstance(@Nonnull SessionAdapter session) {
        return new MailRepositoryDAO(session);
    }

    /**
     * Returns a list of all repositories
     */
    public List<String> getRepositoryNames()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<String> criteriaQuery = criteriaBuilder.createQuery(String.class);

        Root<MailRepositoryEntity> repositoryPath = criteriaQuery.from(MailRepositoryEntity.class);

        criteriaQuery.select(repositoryPath.get(MailRepositoryEntity.REPOSITORY_COLUMN_NAME));

        return createQuery(criteriaQuery).stream().distinct().toList();
    }

    /**
     * Returns the number of items in the MailRepository.
     */
    public long getItemCount(@Nonnull String repository)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<MailRepositoryEntity> rootEntity = criteriaQuery.from(MailRepositoryEntity.class);

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(MailRepositoryEntity.REPOSITORY_COLUMN_NAME),
                repository));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    private Query<MailRepositoryEntity> createGetItemsQuery(@Nonnull String repository, Integer firstResult,
            Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<MailRepositoryEntity> criteriaQuery = criteriaBuilder.createQuery(
                MailRepositoryEntity.class);

        Root<MailRepositoryEntity> rootEntity = criteriaQuery.from(MailRepositoryEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(MailRepositoryEntity.REPOSITORY_COLUMN_NAME),
                repository));

        criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(MailRepositoryEntity.CREATED_COLUMN_NAME)));

        Query<MailRepositoryEntity> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query;
    }

    public List<MailRepositoryEntity> getItems(@Nonnull String repository, Integer firstResult, Integer maxResults) {
        return createGetItemsQuery(repository, firstResult, maxResults).getResultList();
    }

    public CloseableIterator<MailRepositoryEntity> getItemsIterator(@Nonnull String repository,
            Integer firstResult, Integer maxResults)
    {
        return new MailRepositoryItemEntityIterator(createGetItemsQuery(repository,
                firstResult, maxResults).scroll(ScrollMode.FORWARD_ONLY));
    }

    public List<MailRepositoryEntity> getItemsBefore(@Nonnull String repository, @Nonnull Date before,
            Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<MailRepositoryEntity> criteriaQuery = criteriaBuilder.createQuery(
                MailRepositoryEntity.class);

        Root<MailRepositoryEntity> rootEntity = criteriaQuery.from(MailRepositoryEntity.class);

        criteriaQuery.where(criteriaBuilder.and(
                criteriaBuilder.equal(rootEntity.get(MailRepositoryEntity.REPOSITORY_COLUMN_NAME), repository),
                criteriaBuilder.lessThan(rootEntity.get(MailRepositoryEntity.CREATED_COLUMN_NAME), before)));

        criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(MailRepositoryEntity.CREATED_COLUMN_NAME)));

        Query<MailRepositoryEntity> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    private List<MailRepositoryEntity> searchForRecipients(@Nonnull String repository, @Nonnull String key,
            Integer firstResult, Integer maxResults)
    {
        Query<MailRepositoryEntity> query = createQuery(
                "select distinct c from " + ENTITY_NAME + " c join c.recipients r "
                    + "where r like lower(:key) and c.repository = :repository order by c.created",
                MailRepositoryEntity.class);

        query.setParameter("key", StringUtils.lowerCase(key));
        query.setParameter("repository", repository);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.list();
    }

    private Predicate createSearchFieldPredicate(
            @Nonnull CriteriaBuilder bld,
            @Nonnull Root<MailRepositoryEntity> root,
            @Nonnull MailRepositorySearchField searchField,
            @Nonnull String key)
    {
        key = StringUtils.lowerCase(key);

        return switch (searchField) {
            case MESSAGE_ID -> bld.like(bld.lower(root.get(MailRepositoryEntity.MESSAGE_ID_COLUMN_NAME)), key);
            case SUBJECT    -> bld.like(bld.lower(root.get(MailRepositoryEntity.SUBJECT_COLUMN_NAME)), key);
            case SENDER     -> bld.like(bld.lower(root.get(MailRepositoryEntity.SENDER_COLUMN_NAME)), key);
            case FROM       -> bld.like(bld.lower(root.get(MailRepositoryEntity.FROM_HEADER_COLUMN_NAME)), key);
            default -> throw new IllegalArgumentException("Unsupported searchField " + searchField);
        };
    }

    public List<MailRepositoryEntity> searchItems(@Nonnull String repository,
            @Nonnull MailRepositorySearchField searchField, @Nonnull String key, Integer firstResult,
            Integer maxResults)
    {
        // We need to handle the search for recipients somewhat differently because the
        // recipients are stored in a separate table
        if (searchField == MailRepositorySearchField.RECIPIENTS)
        {
            return searchForRecipients(repository, key, firstResult,
                    maxResults);
        }
        else {
            CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

            CriteriaQuery<MailRepositoryEntity> criteriaQuery = criteriaBuilder.createQuery(
                    MailRepositoryEntity.class);

            Root<MailRepositoryEntity> rootEntity = criteriaQuery.from(MailRepositoryEntity.class);

            criteriaQuery.where(criteriaBuilder.and(
                    criteriaBuilder.equal(rootEntity.get(MailRepositoryEntity.REPOSITORY_COLUMN_NAME), repository),
                    createSearchFieldPredicate(criteriaBuilder, rootEntity, searchField, key)));

            criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(MailRepositoryEntity.CREATED_COLUMN_NAME)));

            Query<MailRepositoryEntity> query = createQuery(criteriaQuery);

            if (firstResult != null) {
                query.setFirstResult(firstResult);
            }

            if (maxResults != null) {
                query.setMaxResults(maxResults);
            }

            return query.getResultList();
        }
    }

    private long getSearchCountRecipients(@Nonnull String repository, @Nonnull String key)
    {
        Query<Long> query = createQuery("select count (distinct c) from " + ENTITY_NAME + " c join c.recipients r where "
            + "r like :key and c.repository = :repository", Long.class);

        query.setParameter("key", key);
        query.setParameter("repository", repository);

        return query.uniqueResultOptional().orElse(0L);
    }

    public long getSearchCount(@Nonnull String repository, @Nonnull MailRepositorySearchField searchField,
            @Nonnull String key)
    {
        // We need to handle the search for recipients somewhat differently because the
        // recipients are stored in a separate table
        if (searchField == MailRepositorySearchField.RECIPIENTS) {
            return (int) getSearchCountRecipients(repository, key);
        }
        else {
            CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

            CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

            Root<MailRepositoryEntity> rootEntity = criteriaQuery.from(MailRepositoryEntity.class);

            criteriaQuery.select(criteriaBuilder.count(rootEntity));

            criteriaQuery.where(criteriaBuilder.and(
                    criteriaBuilder.equal(rootEntity.get(MailRepositoryEntity.REPOSITORY_COLUMN_NAME), repository),
                    createSearchFieldPredicate(criteriaBuilder, rootEntity, searchField, key)));

            return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
        }
    }

    public void deleteAll(@Nonnull String repository)
    {
        CloseableIterator<MailRepositoryEntity> iterator = getItemsIterator(repository,
                null, null);

        try {
            try {
                while (iterator.hasNext())
                {
                    MailRepositoryEntity entry = iterator.next();

                    // Need to flush before evict to prevent the following exception when deleting while adding
                    // MailRepositoryEntity's at the same time. See https://forum.hibernate.org/viewtopic.php?p=2424890
                    //
                    // java.util.concurrent.ExecutionException: org.hibernate.AssertionFailure: possible
                    // non-threadsafe access to session
                    this.flush();
                    // evict the entry to save memory
                    this.evict(entry);
                    this.delete(entry);
                }
            }
            finally {
                // we must close the iterator
                iterator.close();
            }
        }
        catch (CloseableIteratorException e) {
            throw new UnhandledException(e);
        }
    }
}
