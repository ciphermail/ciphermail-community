/*
 * Copyright (c) 2014-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.openpgp.PGPKeyRingParser.KeyEventListener;
import com.ciphermail.core.common.security.password.PasswordProvider;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of PGPKeyRingImporter
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyRingImporterImpl implements PGPKeyRingImporter
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyRingImporterImpl.class);

    /*
     * If true, and a key cannot be imported because it is corrupt or invalid, the key will be skipped. This is the
     * default value to use if a method without the ignoreParsingErrors parameter is used.
     */
    private boolean defaultIgnoreParsingErrors = true;

    /*
     * The keyring to which the keyrings are imported to
     */
    private final PGPKeyRing keyRing;


    public PGPKeyRingImporterImpl(@Nonnull PGPKeyRing keyRing) {
        this.keyRing = Objects.requireNonNull(keyRing);
    }

    @Override
    public List<PGPKeyRingEntry> importKeyRing(@Nonnull InputStream input, PasswordProvider passwordProvider,
            boolean ignoreParsingErrors)
    throws IOException, PGPException
    {
        final List<PGPKeyRingEntry> imported = new LinkedList<>();

        KeyEventListener keyEventListener = (publicKey, privateKey) ->
        {
            if (publicKey != null)
            {
                PGPKeyRingEntry entry = privateKey != null ? keyRing.addKeyPair(
                        new PGPKeyRingPairImpl(publicKey, privateKey)) : keyRing.addPGPPublicKey(publicKey);

                if (entry != null) {
                    imported.add(entry);
                }
            }
            else {
                // Should never happen
                logger.warn("PublicKey is null");
            }
        };

        PGPKeyRingParser parser = new PGPKeyRingParser();

        parser.setKeyEventListener(keyEventListener);

        parser.setIgnoreParsingErrors(ignoreParsingErrors);

        parser.parseKeyRing(input, passwordProvider);

        return imported;
    }

    @Override
    public List<PGPKeyRingEntry> importKeyRing(@Nonnull InputStream input, PasswordProvider passwordProvider)
    throws IOException, PGPException
    {
        return importKeyRing(input, passwordProvider, defaultIgnoreParsingErrors);
    }

    @Override
    public List<PGPKeyRingEntry> importKeyRing(PrivateKeysAndPublicKeyRing privateKeysAndPublicKeyRing)
    throws IOException, PGPException
    {
        List<PGPKeyRingEntry> imported = new LinkedList<>();

        if (privateKeysAndPublicKeyRing != null)
        {
            PGPPublicKeyRing publicKeyRing = privateKeysAndPublicKeyRing.getPublicKeyRing();

            Iterator<?> keyIterator = publicKeyRing.getPublicKeys();

            while (keyIterator.hasNext())
            {
                Object o = keyIterator.next();

                if (!(o instanceof PGPPublicKey publicKey)) {
                    continue;
                }

                PGPPrivateKey privateKey = privateKeysAndPublicKeyRing.getPrivateKey(publicKey);

                PGPKeyRingEntry entry = privateKey != null ? keyRing.addKeyPair(
                        new PGPKeyRingPairImpl(publicKey, privateKey)) : keyRing.addPGPPublicKey(publicKey);

                if (entry != null) {
                    imported.add(entry);
                }
            }
        }

        return imported;
    }

    public boolean isDefaultIgnoreParsingErrors() {
        return defaultIgnoreParsingErrors;
    }

    public void setDefaultIgnoreParsingErrors(boolean defaultIgnoreParsingErrors) {
        this.defaultIgnoreParsingErrors = defaultIgnoreParsingErrors;
    }
}
