/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.otp;

import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.HexUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * TOTPImpl implementation (<a href="https://tools.ietf.org/html/rfc6238">...</a>)
 */
public class TOTPImpl implements TOTP
{
    /*
     * The HMAC algorithm to use (Google Authenticator only supports HmacSHA1)
     */
    private String algorithm = "HmacSHA1";

    /*
     * The number of digits to use for the otp (Google Authenticator only supports 6)
     */
    private int nrOfDigits = 6;

    /*
     * The base time (Google Authenticator only supports 0)
     */
    private long t0 = 0;

    /*
     * The interval (in seconds) for which a generated TOTP is valid
     */
    private long timestep = 30;

    /*
     * The number of timesteps before and after the current time are checked (i.e., is delayWindow is 2, 5 times steps
     * are checked, two before the current time, the current time and two after the current time). This is done to
     * work around time delays and time differences between devices
     */
    private int delayWindow = 2;

    private Mac createMAC(byte[] secret)
    throws IOException
    {
        try {
            Mac mac = SecurityFactoryFactory.getSecurityFactory().createMAC(algorithm);

            SecretKeySpec keySpec = new SecretKeySpec(secret, "raw");

            mac.init(keySpec);

            return mac;
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException e) {
            throw new IOException(e);
        }
    }

    /**
     * Generates a TOTPImpl using the key and current time
     */
    @Override
    public String generateOTP(byte[] key)
    throws IOException
    {
        return generateOTP(key, System.currentTimeMillis());
    }

    /**
     * Generate TOTPImpl using the key and time in milliseconds
     */
    public String generateOTP(byte[] key, long time)
    throws IOException
    {
        return generateOTPFromTimestep(key, getCurrentTimestep(time));
    }

    private long getCurrentTimestep(long time) {
        return (time / DateUtils.MILLIS_PER_SECOND - t0) / timestep;
    }

    /*
     * Generate TOTP using the current timestep interval
     */
    private String generateOTPFromTimestep(byte[] key, long timestep)
    throws IOException
    {
        try {
            // Pad to make sure that it has 16 hex characters (HOTP rfc4226 requires that the counter be 8 bytes)
            String hexTime = StringUtils.leftPad(Long.toHexString(timestep).toUpperCase(), 16, '0');

            byte[] toSign = HexUtils.hexDecode(hexTime);

            Mac mac = createMAC(key);

            byte[] hash = mac.doFinal(toSign);

            int offset = hash[hash.length - 1] & 0xf;

            int binary = ((hash[offset] & 0x7f) << 24) |
                    ((hash[offset + 1] & 0xff) << 16) |
                    ((hash[offset + 2] & 0xff) << 8) |
                    (hash[offset + 3] & 0xff);

            long otp = binary % (BigInteger.valueOf(10L).pow(nrOfDigits)).longValue();

            // The result should be padded to the correct length
            return StringUtils.leftPad(Long.toString(otp), nrOfDigits, '0');
        }
        catch (DecoderException e) {
            throw new IOException(e);
        }
    }

    /**
     * Verifies the otp against the current time and key
     */
    @Override
    public void verifyOTP(byte[] key, String otp)
    throws TOTPValidationFailureException, IOException
    {
        verifyOTP(key, otp, System.currentTimeMillis());
    }

    /**
     * Verifies the otp against the provided time and key
     */
    public void verifyOTP(byte[] key, String otp, long time)
    throws TOTPValidationFailureException, IOException
    {
        long localTimestep = (time / DateUtils.MILLIS_PER_SECOND - t0) / this.timestep;

        for (long t = localTimestep - delayWindow; t <= (localTimestep + delayWindow); t++)
        {
            if (otp.equals(generateOTPFromTimestep(key, t))) {
                return;
            }
        }

        throw new TOTPValidationFailureException();
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public TOTPImpl setAlgorithm(String algorithm)
    {
        this.algorithm = algorithm;
        return this;
    }

    public int getNrOfDigits() {
        return nrOfDigits;
    }

    public TOTPImpl setNrOfDigits(int nrOfDigits)
    {
        this.nrOfDigits = nrOfDigits;
        return this;
    }

    public long getT0() {
        return t0;
    }

    public TOTPImpl setT0(long t0)
    {
        this.t0 = t0;
        return this;
    }

    public long getTimestep() {
        return timestep;
    }

    public TOTPImpl setTimestep(long timestep)
    {
        this.timestep = timestep;
        return this;
    }

    public int getDelayWindow() {
        return delayWindow;
    }

    public TOTPImpl setDelayWindow(int delayWindow)
    {
        this.delayWindow = delayWindow;
        return this;
    }
}
