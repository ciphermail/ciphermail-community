/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certpath;

import com.ciphermail.core.common.security.certstore.BasicCertStore;
import com.ciphermail.core.common.security.certstore.jce.BasicCertStoreParameters;
import com.ciphermail.core.common.security.provider.CipherMailProvider;

import javax.annotation.Nonnull;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.util.Objects;

public class DefaultCertificatePathBuilderFactory implements CertificatePathBuilderFactory
{
    /*
     * The Used to get the trusted anchors (AKA Roots).
     */
    private final TrustAnchorBuilder trustAnchorBuilder;

    /*
     * The certificate stores to add to the path builder
     */
    private final BasicCertStore[] certStores;

    public DefaultCertificatePathBuilderFactory(@Nonnull TrustAnchorBuilder trustAnchorBuilder,
            @Nonnull BasicCertStore... certStores)
    {
        this.trustAnchorBuilder = Objects.requireNonNull(trustAnchorBuilder);
        this.certStores = Objects.requireNonNull(certStores);
    }

    /*
     * convert the BasicCertStore to a JCE CertStore because CertificatePathBuilder requires
     * a CertStore.
     */
    private void addCertStore(@Nonnull CertificatePathBuilder pathBuilder, BasicCertStore certStore)
    throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException
    {
        CertStoreParameters certStoreParams = new BasicCertStoreParameters(certStore);

        CertStore jceCertStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, certStoreParams,
                CipherMailProvider.PROVIDER);
        pathBuilder.addCertStore(jceCertStore);
    }

    @Override
    public CertificatePathBuilder createCertificatePathBuilder()
    throws CertPathBuilderException
    {
        CertificatePathBuilder pathBuilder = new TrustAnchorBuilderPKIXCertificatePathBuilder(
                trustAnchorBuilder);

        // do not use the built-in revocation checker. Revocation checking will be done separately.
        pathBuilder.setRevocationEnabled(false);

        try {
            for (BasicCertStore certStore : certStores) {
                addCertStore(pathBuilder, certStore);
            }

            // Add checks for critical extension we know how to handle.
            pathBuilder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
            pathBuilder.addCertPathChecker(new CRLDistPointCertPathChecker());
        }
        catch(InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new CertPathBuilderException(e);
        }

        return pathBuilder;
    }
}
