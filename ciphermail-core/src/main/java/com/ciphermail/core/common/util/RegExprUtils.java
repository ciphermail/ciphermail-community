/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Some general purpose regular expression utilities
 */
public class RegExprUtils
{
    /*
     * Pattern used for splitting up the input into a reg expr and value (see splitRegExp)
     */
    private static final Pattern SPLIT_REG_EXP_PATTERN = Pattern.compile("\\s*/([^/]*)/(?:\\s(.*))?");

    /**
     * Escapes all regular expression special chars from the input.
     */
    public static String escape(String input) {
        return Pattern.quote(input);
    }

    private RegExprUtils() {
        // empty on purpose
    }

    /**
     * Splits the input into the reg expression and value. The input should be provided as
     * <p>
     *  /REGEX/ VALUE
     * <p>
     * Returns null if input is null or if input does not match the above pattern
     * <p>
     * Example:
     * input: /^test$/ abc def -> [0]=^test$ [1]=abc def
     */
    public static String[] splitRegExp(String input)
    {
        if (StringUtils.isEmpty(input)) {
            return null;
        }

        Matcher matcher = SPLIT_REG_EXP_PATTERN.matcher(input);

        String[] result = null;

        if (matcher.matches()) {
            result = new String[]{matcher.group(1), matcher.group(2)};
        }

        return result;
    }

    /**
     * Returns true if input is a valid regular expression pattern
     */
    public static boolean isValidRegExp(String input)
    {
        if (input == null) {
            return false;
        }

        boolean valid = false;

        try {
            Pattern.compile(input);

            valid = true;
        }
        catch (PatternSyntaxException e) {
            // Pattern is not valid
        }

        return valid;
    }
}
