/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.extractor.impl;

import com.ciphermail.core.common.extractor.DetectedMimeType;
import com.ciphermail.core.common.extractor.TextExtractorContext;
import com.ciphermail.core.common.util.ThreadSafeContextImpl;

/**
 * Default implementation of TextExtractorContext;
 *
 * @author Martijn Brinkers
 *
 */
public class TextExtractorContextImpl extends ThreadSafeContextImpl implements TextExtractorContext
{
    /*
     * The default keys
     */
    private static final String NAME_KEY = "name";
    private static final String ENCODING_KEY = "encoding";
    private static final String MIME_TYPE_KEY = "mimeType";

    @Override
    public void setName(String defaultName) {
        set(NAME_KEY, defaultName);
    }

    @Override
    public String getName() {
        return get(NAME_KEY, String.class);
    }

    @Override
    public void setEncoding(String encoding) {
        set(ENCODING_KEY, encoding);
    }

    @Override
    public String getEncoding() {
        return get(ENCODING_KEY, String.class);
    }

    @Override
    public void setMimeType(DetectedMimeType mimeType) {
        set(MIME_TYPE_KEY, mimeType);
    }

    @Override
    public DetectedMimeType getMimeType() {
        return get(MIME_TYPE_KEY, DetectedMimeType.class);
    }
    /**
     * Creates a copy of this TextExtractorContextImpl.
     *
     * Note: The context values are not cloned (i.e., it's a shallow clone)
     */
    @Override
    public TextExtractorContextImpl clone()
    {
        TextExtractorContextImpl clone = new TextExtractorContextImpl();

        clone.context.putAll(this.context);

        return clone;
    }
}
