/*
 * Copyright (c) 2016-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.transport.twilio;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

import java.io.IOException;

/**
 * Implementation of TwilioPropertiesProvider which returns static values for the TwilioProperties.
 *
 * @author Martijn Brinkers
 *
 */
public class StaticTwilioPropertiesProvider implements TwilioPropertiesProvider
{
    private String from;
    private String accountSid;
    private String authToken;
    private String apiKeySid;
    private String apiSecretKey;

    @Override
    public TwilioProperties getProperties()
    throws IOException
    {
        return new TwilioProperties()
        {
            @Override
            public void setFrom(String from) {
                // Empty on purpose
            }

            @Override
            public String getFrom() {
                return from;
            }

            @Override
            public void setAccountSid(String accountSid) {
                // Empty on purpose
            }

            @Override
            public String getAccountSid() {
                return accountSid;
            }

            @Override
            public void setAuthToken(String authToken) {
                // Empty on purpose
            }

            @Override
            public String getAuthToken() {
                return authToken;
            }

            @Override
            public void setAPIKeySid(String apiKeySid) {
                // Empty on purpose
            }

            @Override
            public String getAPIKeySid() {
                return apiKeySid;
            }

            @Override
            public void setAPISecretKey(String apiSecretKey) {
                // Empty on purpose
            }

            @Override
            public String getAPISecretKey() {
                return apiSecretKey;
            }

            @Override
            public void copyTo(TwilioProperties other)
            throws HierarchicalPropertiesException
            {
                if (other == null) {
                    return;
                }

                other.setFrom(from);
                other.setAccountSid(accountSid);
                other.setAuthToken(authToken);
                other.setAPIKeySid(apiKeySid);
                other.setAPISecretKey(apiSecretKey);
            }
        };
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getAccountSid() {
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getApiKeySid() {
        return apiKeySid;
    }

    public void setApiKeySid(String apiKeySid) {
        this.apiKeySid = apiKeySid;
    }

    public String getApiKeySecret() {
        return apiSecretKey;
    }

    public void setApiKeySecret(String apiSecretKey) {
        this.apiSecretKey = apiSecretKey;
    }
}
