/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.repository;

import com.ciphermail.core.common.util.ThreadUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class MailRepositoryMaintainer
{
    private static final Logger logger = LoggerFactory.getLogger(MailRepositoryMaintainer.class);

    /*
     * The name of the maintainer thread.
     */
    private static final String THREAD_NAME = "Mail Repository Maintainer";

    /*
     * The MailRepository's which should be maintained
     */
    private final Collection<MailRepository> repositories;

    /*
     * Is used to tell listeners that something has happened for on the mailRepository
     */
    private final MailRepositoryEventListener mailRepositoryEventListener;

    /*
     * Used for executing database actions within a transaction
     */
    private final TransactionOperations transactionOperations;

    /*
     * Time (in milliseconds) a mail will be stored in quarantine until expiration
     */
    private long expirationTime = DateUtils.MILLIS_PER_DAY * 5;

    /*
     * Time (in milliseconds) the thread will sleep if there are no items which expire
     */
    private long sleepTime = DateUtils.MILLIS_PER_MINUTE * 5;

    /*
     * Time (in milliseconds) to settle down at start before the maintainer thread will start
     */
    private long settleTime = DateUtils.MILLIS_PER_SECOND * 10;

    /*
     * The maximum number of MailRepository's items that will be removed in obe batch
     */
    private int maxBatchSize = 50;

    /*
     * True if the thread should stop
     */
    private boolean stopped;

    /*
     * Thread that periodically checks for expired items
     */
    private MaintainThread thread;

    public MailRepositoryMaintainer(
            @Nonnull Collection<MailRepository> repositories,
            @Nonnull MailRepositoryEventListener mailRepositoryEventListener,
            @Nonnull TransactionOperations transactionOperations)
    {
        this.repositories = Objects.requireNonNull(repositories);
        this.mailRepositoryEventListener = Objects.requireNonNull(mailRepositoryEventListener);
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
    }

    public long getExpirationTime() {
        return expirationTime;
    }

    /**
     * Sets the expirationTime (in milliseconds).
     * <p>
     * Note: should not be called after start has been called.
     */
    public void setExpirationTime(long expirationTime)
    {
        if (expirationTime < 0) {
            throw new IllegalArgumentException("expirationTime must be > 0");
        }

        this.expirationTime = expirationTime;
    }

    public long getSleepTime() {
        return sleepTime;
    }

    /**
     * Sets the updateInterval (in milliseconds).
     * <p>
     * Note: should not be called after start has been called.
     */
    public void setSleepTime(long sleepTime)
    {
        if (sleepTime < 0) {
            throw new IllegalArgumentException("sleepTime must be > 0");
        }

        this.sleepTime = sleepTime;
    }

    /**
     * Sets the maximum nr of MailRepository's items that will be maintained at the same time (i.e, handled
     * in one transaction)
     * <p>
     * Note: should not be called after start has been called.
     */
    public void setMaxBatchSize(int maxBatchSize) {
        this.maxBatchSize = maxBatchSize;
    }

    /**
     * The maximum nr of MailRepository's items that will be maintained at the same time (i.e, handled
     * in one transaction)
     * <p>
     * Note: should not be called after start has been called.
     */
    public int getMaxBatchSize() {
        return maxBatchSize;
    }

    /**
     * set the time (in milliseconds) to settle down at start before the maintainer thread will start.
     */
    public void setSettleTime(long settleTime) {
        this.settleTime = settleTime;
    }

    /**
     * Time (in milliseconds) to settle down at start before the maintainer thread will start.
     */
    public long getSettleTime() {
        return settleTime;
    }

    /**
     * Tries to stop the maintainer thread
     */
    public synchronized void start()
    {
        if (thread == null)
        {
            stopped = false;

            thread = new MaintainThread();
            thread.setDaemon(true);
            thread.start();
        }
    }

    /**
     * Tries to stop the maintainer thread
     */
    public synchronized void stop()
    {
        if (thread != null)
        {
            stopped = true;

            thread.interrupt();
            thread = null;
        }
    }

    /*
     * Removes items from the MailRepository which are no longer valid because they expired.
     */
    private boolean maintainRepositoryTransacted(MailRepository repository)
    {
        List<? extends MailRepositoryItem> expired = repository.getItemsBefore(DateUtils.addMilliseconds(new Date(),
            (int) -expirationTime), 0, maxBatchSize);

        for (MailRepositoryItem item : expired)
        {
            repository.deleteItem(item.getID());

            mailRepositoryEventListener.onExpired(repository.getName(), item);
        }

        // If the maximum nr of items reached, assume there are more items
        return expired.size() == maxBatchSize;
    }

    /*
     * Starts a transaction and maintains the repository
     */
    private boolean maintainRepository(final MailRepository repository)
    {
        boolean more = false;

        logger.debug("Maintaining Mail repository {}", repository.getName());

        try {
            more = Boolean.TRUE.equals(transactionOperations.execute(status -> maintainRepositoryTransacted(repository)));
        }
        catch(Exception e) {
            logger.error("Error while maintaining repository {}", repository.getName());
        }

        return more;
    }

    private class MaintainThread extends Thread
    {
        public MaintainThread() {
            super(THREAD_NAME);
        }

        @Override
        public void run()
        {
            logger.info("Starting thread {}. Expiration time: {}, Sleep time: {}, Settle time: {}",
                    THREAD_NAME, expirationTime, sleepTime, settleTime);

            ThreadUtils.sleepQuietly(settleTime);

            do {
                try {
                    boolean more = false;

                    for (MailRepository repository : repositories)
                    {
                        if (maintainRepository(repository)) {
                            more = true;
                        }
                    }

                    if (!more) {
                        ThreadUtils.sleepQuietly(sleepTime);
                    }
                }
                catch(Exception e) {
                    logger.error("Error in thread {} thread.", THREAD_NAME, e);
                }
            }
            while(!stopped);
        }
    }
}
