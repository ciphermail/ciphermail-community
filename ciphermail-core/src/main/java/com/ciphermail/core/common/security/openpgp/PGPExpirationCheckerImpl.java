/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.CurrentDateProvider;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.time.DateUtils;
import org.bouncycastle.bcpg.SignatureSubpacket;
import org.bouncycastle.bcpg.SignatureSubpacketTags;
import org.bouncycastle.bcpg.sig.KeyExpirationTime;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureSubpacketVector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of PGPExpirationChecker
 *
 * @author Martijn Brinkers
 *
 */
public class PGPExpirationCheckerImpl implements PGPExpirationChecker
{
    private static final Logger logger = LoggerFactory.getLogger(PGPExpirationCheckerImpl.class);

    /*
     * For validating key signatures
     */
    private final PGPSignatureValidator signatureValidator;

    public PGPExpirationCheckerImpl(@Nonnull PGPSignatureValidator signatureValidator) {
        this.signatureValidator = Objects.requireNonNull(signatureValidator);
    }

    private boolean isUserIDSignatureValid(PGPPublicKey key, byte[] userID, PGPSignature signature)
    {
        boolean valid = false;

        try {
            valid = signatureValidator.validateUserIDSignature(key, userID, signature);
        }
        catch (Exception e) {
            logger.debug("Error validating User ID signature", e);
        }

        return valid;
    }

    private boolean isSubKeyBindingSignatureValid(PGPPublicKey masterKey, PGPPublicKey subKey, PGPSignature signature)
    {
        boolean valid = false;

        try {
            valid = signatureValidator.validateSubkeyBindingSignature(masterKey, subKey, signature);
        }
        catch (Exception e) {
            logger.error("Error validating sub key binding signature", e);
        }

        return valid;
    }

    private Long getKeyValiditySeconds(PGPSignature signature)
    {
        Long result = null;

        // only check hashed sub packets for the expiration time. Storing it in the unhashed sub packets
        // makes no sense since anyone can add them
        PGPSignatureSubpacketVector subPackets = signature.getHashedSubPackets();

        if (subPackets != null)
        {
            SignatureSubpacket packet = subPackets.getSubpacket(SignatureSubpacketTags.KEY_EXPIRE_TIME);

            if (packet != null) {
                result = ((KeyExpirationTime)packet).getTime();
            }
        }

        return result;
    }

    private long getKeyValidSeconds(@Nonnull PGPPublicKey key, PGPPublicKey masterKey)
    {
        // Number of seconds the key is valid. 0 means it will always be valid (i.e., never expires)
        long validSeconds = -1;

        Date latestSigDate = null;

        if (key.getVersion() <= 3) {
            validSeconds = key.getPublicKeyPacket().getValidDays() * 24L * 60 * 60;
        }
        else if (key.isMasterKey())
        {
            // The expiration time is stored on one of the certification signatures
            List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(key);

            for (byte[] userID : userIDs)
            {
                List<PGPSignature> signatures = PGPSignatureInspector.getSignaturesForUserID(key, userID);

                for (PGPSignature signature : signatures)
                {
                    if (ArrayUtils.contains(PGPSignatureInspector.USER_ID_CERTIFICATION_TYPES,
                            signature.getSignatureType()))
                    {
                        if (isUserIDSignatureValid(key, userID, signature))
                        {
                            Long thisValidSeconds = getKeyValiditySeconds(signature);

                            Date thisSigDate = signature.getCreationTime();

                            if (latestSigDate == null || thisSigDate.after(latestSigDate))
                            {
                                latestSigDate = thisSigDate;

                                validSeconds = thisValidSeconds != null ? thisValidSeconds : 0;
                            }
                        }
                    }
                }
            }
        }
        else {
            if (masterKey == null) {
                throw new IllegalArgumentException("masterkey is null");
            }

            // Sub key expiration should be stored in the sub key binding signatures
            List<PGPSignature> signatures = PGPPublicKeyInspector.getSubkeyBindingSignatures(key);

            for (PGPSignature signature : signatures)
            {
                if (isSubKeyBindingSignatureValid(masterKey, key, signature))
                {
                    Long thisValidSeconds = getKeyValiditySeconds(signature);

                    Date thisSigDate = signature.getCreationTime();

                    if (latestSigDate == null || thisSigDate.after(latestSigDate))
                    {
                        latestSigDate = thisSigDate;

                        validSeconds = thisValidSeconds != null ? thisValidSeconds : 0;
                    }
                }
            }
        }

        if (validSeconds == -1) {
            validSeconds = 0;
        }

        return validSeconds;
    }

    @Override
    public Date getKeyExpirationDate(@Nonnull PGPPublicKey key, PGPPublicKey masterKey)
    {
        Date expirationDate = null;

        // Number of seconds the key is valid. 0 means it will always be valid (i.e., never expires)
        long validSeconds = getKeyValidSeconds(key, masterKey);

        if (validSeconds > 0)
        {
            if (validSeconds > Integer.MAX_VALUE) {
                validSeconds = Integer.MAX_VALUE;
            }

            Date creationDate = key.getCreationTime();

            if (creationDate != null) {
                expirationDate = DateUtils.addSeconds(creationDate, (int) validSeconds);
            }
        }

        return expirationDate;
    }

    @Override
    public boolean isKeyExpired(@Nonnull PGPPublicKey key, PGPPublicKey masterKey)
    {
        boolean expired = false;

        Date expirationDate = getKeyExpirationDate(key, masterKey);

        if (expirationDate != null) {
            expired = CurrentDateProvider.getNow().after(expirationDate);
        }

        return expired;
    }
}
