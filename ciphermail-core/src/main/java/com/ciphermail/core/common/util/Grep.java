/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.collections.buffer.CircularFifoBuffer;
import org.apache.commons.io.IOUtils;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Grep can be used to search for patterns in files.
 *
 * @author Martijn Brinkers
 *
 */
public class Grep
{
    /*
     * The files to read the lines from
     */
    private final Collection<File> files;

    /*
     * If context > 0, the lines before and after a match will be returned
     */
    private int context;

    /*
     * The regular expression filter
     */
    private Pattern pattern;

    /*
     * List implementation which only counts the number of items added without actually storing
     */
    @SuppressWarnings("serial")
    private static class CountOnlyList extends LinkedList<String>
    {
        private int count;

        @Override
        public boolean add(String s)
        {
            count++;
            return true;
        }

        @Override
        public int size() {
            return count;
        }
    }

    public Grep(File file) {
        this(Collections.singleton(file));
    }

    public Grep(@Nonnull Collection<File> files)
    {
        if (files.isEmpty()) {
            throw new IllegalArgumentException("files need to contain at least one File.");
        }

        this.files = files;
    }

    private LineNumberReader createLineNumberReader()
    throws FileNotFoundException
    {
        Collection<Reader> readers = new ArrayList<>(files.size());

        for (File file : files) {
            readers.add(new FileReader(file));
        }

        return new LineNumberReader(new MetaReader(readers));
    }

    /**
     * Can be overridden to add line filtering
     */
    protected boolean isMatch(String line)
    {
        boolean match = true;

        if (pattern != null)
        {
            Matcher matcher = pattern.matcher(line);

            match = matcher.find();
        }

        return match;
    }

    /**
     * returns the number of lines that match
     * @return
     * @throws IOException
     */
    public int getLineCount()
    throws IOException
    {
        CountOnlyList countOnlyList = new CountOnlyList();

        readLines(0,  Integer.MAX_VALUE, countOnlyList);

        return countOnlyList.count;
    }

    /**
     * Returns max lines starting from start line.
     *
     * Note: settings context > 0 is only sensible when a filter Pattern is set.
     */
    public List<String> readLines(int startLine, int maxLines)
    throws IOException
    {
        if (startLine < 0) {
            throw new IllegalArgumentException("startLine must be >= 0");
        }

        if (maxLines < 0) {
            throw new IllegalArgumentException("maxLines must be >= 0");
        }

        List<String> lines = new LinkedList<>();

        readLines(startLine, maxLines, lines);

        return lines;
    }

    private void readLines(int startLine, int maxLines, @Nonnull List<String> lines)
    throws IOException
    {
        CircularFifoBuffer contextLines = null;

        int context = this.context;

        // If the pattern is not specified, there is no need for using a context
        if (pattern == null) {
            context = 0;
        }

        if (context > 0) {
            contextLines = new CircularFifoBuffer(context);
        }

        LineNumberReader lineNumberReader = createLineNumberReader();

        try {
            int currentLine = 0;

            // Keeps track of the lines we need to add after the match (only if context > 0)
            int afterMatchCount = 0;

            while (lines.size() <  maxLines)
            {
                String line = lineNumberReader.readLine();

                if (line == null) {
                    break;
                }

                if (isMatch(line))
                {
                    afterMatchCount = context;

                    // If we have any lines before the match we need to add those lines first
                    if (contextLines != null)
                    {
                        currentLine = currentLine + contextLines.size();

                        Iterator<?> it = contextLines.iterator();

                        while (it.hasNext())
                        {
                            String contentLine = (String) it.next();

                            if (currentLine > startLine) {
                                lines.add(contentLine);
                            }

                            if (lines.size() >=  maxLines) {
                                // Max number of lines reached.
                                return;
                            }
                        }

                        contextLines.clear();
                    }

                    currentLine++;

                    if (currentLine > startLine) {
                        lines.add(line);
                    }
                }
                else {
                    if (afterMatchCount > 0)
                    {
                        currentLine++;

                        if (currentLine > startLine) {
                            lines.add(line);
                        }

                        afterMatchCount--;

                        if (afterMatchCount < 0) {
                            afterMatchCount = 0;
                        }
                    }
                    else if (contextLines != null) {
                        contextLines.add(line);
                    }
                }
            }
        }
        finally {
            IOUtils.closeQuietly(lineNumberReader);
        }
    }

    public int getContext() {
        return context;
    }

    public void setContext(int context)
    {
        if (context < 0) {
            context = 0;
        }

        this.context = context;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }
}
