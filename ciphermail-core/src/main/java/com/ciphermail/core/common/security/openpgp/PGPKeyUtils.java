/*
 * Copyright (c) 2013-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.password.PasswordProvider;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cryptlib.CryptlibObjectIdentifiers;
import org.bouncycastle.asn1.gnu.GNUObjectIdentifiers;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import org.bouncycastle.bcpg.BCPGKey;
import org.bouncycastle.bcpg.ECPublicBCPGKey;
import org.bouncycastle.bcpg.PublicKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.KeyFingerPrintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;

import javax.annotation.Nonnull;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class PGPKeyUtils
{
    /*
     * For calculating the fingerprint of a public key
     */
    private static final KeyFingerPrintCalculator keyFingerPrintCalculator = new JcaKeyFingerprintCalculator();

    private PGPKeyUtils() {
        // empty on purpose
    }

    /**
     * Returns the first public key found from the input.
     */
    public static PGPPublicKey decodePublicKey(@Nonnull InputStream input)
    throws IOException
    {
        Object object = new PGPObjectFactory(PGPUtil.getDecoderStream(input), keyFingerPrintCalculator).nextObject();

        if (object instanceof PGPPublicKeyRing pgpPublicKeyRing) {
            return pgpPublicKeyRing.getPublicKey();
        }

        if (object instanceof PGPPublicKey pgpPublicKey) {
            return pgpPublicKey;
        }

        return null;
    }

    /**
     * Returns the first public key found from the input.
     */
    public static PGPPublicKey decodePublicKey(@Nonnull byte[] encodedPublicKey)
    throws IOException
    {
        return decodePublicKey(new ByteArrayInputStream(encodedPublicKey));
    }

    /**
     * Returns the first public key found from the input. The input should be a binary input, i.e., not ASCII armored.
     */
    public static PGPPublicKey decodePublicKey(@Nonnull File file)
    throws IOException
    {
        InputStream input = new BufferedInputStream(new FileInputStream(file));

        try {
            return decodePublicKey(input);
        }
        finally {
            IOUtils.closeQuietly(input);
        }
    }

    /**
     * Returns all the public keys it can find from the input
     */
    public static List<PGPPublicKey> readPublicKeys(@Nonnull InputStream input)
    throws IOException
    {
        List<PGPPublicKey> keys = new LinkedList<>();

        PGPObjectFactory objectFactory = new PGPObjectFactory(PGPUtil.getDecoderStream(input),
                keyFingerPrintCalculator);

        Object pgpObject;

        while ((pgpObject = objectFactory.nextObject()) != null)
        {
            if (pgpObject instanceof PGPPublicKeyRing ring)
            {
                Iterator<?> iterator = ring.getPublicKeys();

                while (iterator.hasNext())
                {
                    Object o = iterator.next();

                    if (o instanceof PGPPublicKey pgpPublicKey) {
                        keys.add(pgpPublicKey);
                    }
                }
            }
            else if (pgpObject instanceof PGPPublicKey pgpPublicKey) {
                keys.add(pgpPublicKey);
            }
        }

        return keys;
    }

    /**
     * Returns all the public keys it can find from the input
     */
    public static List<PGPPublicKey> readPublicKeys(@Nonnull byte[] input)
    throws IOException
    {
        return readPublicKeys(new ByteArrayInputStream(input));
    }

    /**
     * Returns all the public keys it can find from the input
     */
    public static List<PGPPublicKey> readPublicKeys(@Nonnull File file)
    throws IOException
    {
        InputStream input = new BufferedInputStream(new FileInputStream(file));

        try {
            return readPublicKeys(input);
        }
        finally {
            IOUtils.closeQuietly(input);
        }
    }

    /**
     * Returns all the PGP secret keys it can find from the input
     */
    public static List<PGPSecretKey> readSecretKeys(@Nonnull InputStream input)
    throws IOException
    {
        List<PGPSecretKey> keys = new LinkedList<>();

        PGPObjectFactory objectFactory = new PGPObjectFactory(PGPUtil.getDecoderStream(input),
                keyFingerPrintCalculator);

        Object pgpObject;

        while ((pgpObject = objectFactory.nextObject()) != null)
        {
            if (pgpObject instanceof PGPSecretKeyRing ring)
            {
                Iterator<?> iterator = ring.getSecretKeys();

                while (iterator.hasNext())
                {
                    Object o = iterator.next();

                    if (o instanceof PGPSecretKey pgpSecretKey) {
                        keys.add(pgpSecretKey);
                    }
                }
            }
        }

        return keys;
    }

    /**
     * Returns all the PGP secret keys it can find from the input
     */
    public static List<PGPSecretKey> readSecretKeys(@Nonnull byte[] input)
    throws IOException
    {
        return readSecretKeys(new ByteArrayInputStream(input));
    }

    /**
     * Returns all the PGP secret keys it can find from the input
     */
    public static List<PGPSecretKey> readSecretKeys(@Nonnull File file)
    throws IOException
    {
        InputStream input = new BufferedInputStream(new FileInputStream(file));

        try {
            return readSecretKeys(input);
        }
        finally {
            IOUtils.closeQuietly(input);
        }
    }

    /**
     * Extracts the private key from the secret key. Returns null if the secret key does not contain a private key
     * @throws IOException
     * @throws PGPException
     */
    public static PGPPrivateKey extractPrivateKey(@Nonnull PGPSecretKey secretKey, PasswordProvider passwordProvider)
    throws PGPException, IOException
    {
        return secretKey.extractPrivateKey(getSecretKeyDecryptorBuilder().build(
                passwordProvider != null ? passwordProvider.getPassword() : null));
    }

    private static JcePBESecretKeyDecryptorBuilder getSecretKeyDecryptorBuilder()
    {
        JcePBESecretKeyDecryptorBuilder decryptorBuilder = new JcePBESecretKeyDecryptorBuilder();
        // Private keys stored on an HSM are directly returned by JcaPGPKeyConverter. JcaPGPKeyConverter is only
        // used for converting soft keys so use non-sensitive provider.
        decryptorBuilder.setProvider(PGPSecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());

        return decryptorBuilder;
    }

    /**
     * Parses the input and returns revocation certificate (which is basically a PGPSignatureList). Null if the first
     * packet is not a PGPSignature.
     */
    public static PGPSignatureList readRevocationCertificate(InputStream input)
    throws IOException
    {
        PGPSignatureList result = null;

        PGPObjectFactory objectFactory = new PGPObjectFactory(PGPUtil.getDecoderStream(input),
                keyFingerPrintCalculator);

        Object pgpObject = objectFactory.nextObject();

        if (pgpObject instanceof PGPSignatureList pgpSignatureList) {
            result = pgpSignatureList;
        }

        return result;
    }

    /**
     * Parses the input and returns revocation certificate (which is basically a PGPSignatureList). Null if the first
     * packet is not a PGPSignature.
     */
    public static PGPSignatureList readRevocationCertificate(File file)
    throws IOException
    {
        InputStream input = new BufferedInputStream(new FileInputStream(file));

        try {
            return readRevocationCertificate(input);
        }
        finally {
            IOUtils.closeQuietly(input);
        }
    }

    /**
     * Returns true if the PGP public keys are equal by comparing the SHA256 fingerprints of the PGP public keys.
     */
    public static boolean isEquals(PGPPublicKey one, PGPPublicKey other)
    throws IOException
    {
        if (one == null && other == null) {
            return true;
        }

        if (one == null || other == null) {
            return false;
        }

        try {
            return ArrayUtils.isEquals(PGPPublicKeyInspector.getSHA256Fingerprint(one),
                    PGPPublicKeyInspector.getSHA256Fingerprint(other));
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new IOException(e);
        }
    }

    /**
     * Returns all the public keys from the PGPPublicKeyRing
     */
    public static List<PGPPublicKey> getPublicKeys(PGPPublicKeyRing publicKeyRing)
    {
        List<PGPPublicKey> keys = new LinkedList<>();

        if (publicKeyRing != null)
        {
            Iterator<?> iterator = publicKeyRing.getPublicKeys();

            while (iterator.hasNext())
            {
                Object o = iterator.next();

                if (o instanceof PGPPublicKey pgpPublicKey) {
                    keys.add(pgpPublicKey);
                }
            }
        }

        return keys;
    }

    /**
     * Returns a friendly name for the algorithm. If a friendly name is not available, the algorithm int will be
     * returned as a string.
     */
    public static @Nonnull String getAlgorithmFriendly(@Nonnull PGPPublicKey publicKey)
    {
        int algorithm = publicKey.getAlgorithm();

        StrBuilder friendlyNameBuilder = new StrBuilder();

        switch (algorithm) {
            case PublicKeyAlgorithmTags.RSA_GENERAL -> friendlyNameBuilder.append("RSA");
            case PublicKeyAlgorithmTags.RSA_ENCRYPT -> friendlyNameBuilder.append("RSA-encrypt");
            case PublicKeyAlgorithmTags.RSA_SIGN -> friendlyNameBuilder.append("RSA-sign");
            case PublicKeyAlgorithmTags.ELGAMAL_ENCRYPT -> friendlyNameBuilder.append("Elgamal-encrypt");
            case PublicKeyAlgorithmTags.DSA -> friendlyNameBuilder.append("DSA");
            case PublicKeyAlgorithmTags.ECDH -> friendlyNameBuilder.append("ECDH");
            case PublicKeyAlgorithmTags.ECDSA -> friendlyNameBuilder.append("ECDSA");
            case PublicKeyAlgorithmTags.ELGAMAL_GENERAL -> friendlyNameBuilder.append("Elgamal");
            case PublicKeyAlgorithmTags.DIFFIE_HELLMAN -> friendlyNameBuilder.append("DH");
            case PublicKeyAlgorithmTags.EDDSA_LEGACY -> friendlyNameBuilder.append("EDDSA");
            case PublicKeyAlgorithmTags.X25519 -> friendlyNameBuilder.append("X25519");
            case PublicKeyAlgorithmTags.X448 -> friendlyNameBuilder.append("X448");
            case PublicKeyAlgorithmTags.Ed25519 -> friendlyNameBuilder.append("Ed25519");
            case PublicKeyAlgorithmTags.Ed448 -> friendlyNameBuilder.append("Ed448");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_1 -> friendlyNameBuilder.append("Experimental-1");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_2 -> friendlyNameBuilder.append("Experimental-2");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_3 -> friendlyNameBuilder.append("Experimental-3");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_4 -> friendlyNameBuilder.append("Experimental-4");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_5 -> friendlyNameBuilder.append("Experimental-5");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_6 -> friendlyNameBuilder.append("Experimental-6");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_7 -> friendlyNameBuilder.append("Experimental-7");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_8 -> friendlyNameBuilder.append("Experimental-8");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_9 -> friendlyNameBuilder.append("Experimental-9");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_10 -> friendlyNameBuilder.append("Experimental-10");
            case PublicKeyAlgorithmTags.EXPERIMENTAL_11 -> friendlyNameBuilder.append("Experimental-11");
            default -> friendlyNameBuilder.append(algorithm);
        }

        BCPGKey bcPGPKey = publicKey.getPublicKeyPacket().getKey();

        String algoFriendlyName = null;

        if (bcPGPKey instanceof ECPublicBCPGKey ecPublicBCPGKey)
        {
            ASN1ObjectIdentifier curveOID = ecPublicBCPGKey.getCurveOID();

            if (curveOID != null)
            {
                algoFriendlyName = ECNamedCurveTable.getName(curveOID);

                if (algoFriendlyName == null)
                {
                    if (curveOID.equals(GNUObjectIdentifiers.Ed25519)) {
                        algoFriendlyName = "Ed25519";
                    }
                    else if (curveOID.equals(CryptlibObjectIdentifiers.curvey25519)) {
                        algoFriendlyName = "curvey25519";
                    }
                }
            }
        }

        if (algoFriendlyName != null) {
            friendlyNameBuilder.append(":").append(algoFriendlyName);
        }

        return friendlyNameBuilder.toString();
    }
}
