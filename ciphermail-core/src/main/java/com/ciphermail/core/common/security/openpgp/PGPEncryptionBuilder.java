/*
 * Copyright (c) 2013-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.util.DirectAccessByteArrayOutputStream;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * Encrypts (and compresses) binary input
 *
 * @author Martijn Brinkers
 *
 */
public class PGPEncryptionBuilder
{
    /*
     * The global security factory
     */
    private final SecurityFactory securityFactory = PGPSecurityFactoryFactory.getSecurityFactory();

    /*
     * The compression algorithm to use
     */
    private PGPCompressionAlgorithm compressionAlgorithm = PGPCompressionAlgorithm.ZLIB;

    /*
     * The encryption algorithm to use
     */
    private PGPEncryptionAlgorithm encryptionAlgorithm = PGPEncryptionAlgorithm.AES_128;

    /*
     * The filename used for the literal compressed packet
     */
    private String compressedFilename = "";

    /*
     * The filename used for the literal data when no compression is used
     */
    private String literalFilename = "";

    /*
     * If true, an integrity packet will be added
     */
    private boolean addIntegrityPacket = true;

    /*
     * If true, a literal data packet will be added
     */
    private boolean addLiteralPacket = true;

    /*
     * If true, the signed content will be ASCII armored
     */
    private boolean armor = true;

    /*
     * Additional armor headers to add if armor is true
     */
    private final Map<String, String> additionalArmorHeaders = new HashMap<>();

    /*
     * If true, session keys will be obfuscated. Session key obfuscation should be disabled by default because
     * this is only supported by recent gnupg versions. Besides, session key obfuscation does not provide a lot of
     * additional security because it is only enabled when using EC keys which require either AES128 or AES256
     *
     * More info:
     *
     * https://github.com/bcgit/bc-java/issues/749
     * https://dev.gnupg.org/T3763
     */
    private boolean sessionKeyObfuscation;

    public void encrypt(@Nonnull InputStream input, @Nonnull OutputStream output,
            @Nonnull Collection<PGPPublicKey> encryptionKeys)
    throws IOException, PGPException
    {
        try {
            DirectAccessByteArrayOutputStream mimeOutput = new DirectAccessByteArrayOutputStream();

            IOUtils.copy(input, mimeOutput);

            mimeOutput = compress(mimeOutput.getInputStream());

            ByteArrayInputStream mimeInput = mimeOutput.getInputStream();

            SecureRandom secureRandom = securityFactory.createSecureRandom();

            JcePGPDataEncryptorBuilder encryptorBuilder = new JcePGPDataEncryptorBuilder(encryptionAlgorithm.getTag());

            encryptorBuilder.setWithIntegrityPacket(addIntegrityPacket);
            encryptorBuilder.setProvider(securityFactory.getNonSensitiveProvider());
            encryptorBuilder.setSecureRandom(secureRandom);

            PGPEncryptedDataGenerator encryptedGenerator = new PGPEncryptedDataGenerator(encryptorBuilder);

            for (PGPPublicKey encryptionKey : encryptionKeys)
            {
                JcePublicKeyKeyEncryptionMethodGenerator encryptionMethod = new JcePublicKeyKeyEncryptionMethodGenerator(
                        encryptionKey);

                encryptionMethod.setProvider(securityFactory.getNonSensitiveProvider());
                encryptionMethod.setSecureRandom(secureRandom);

                // Session key obfuscation should be disabled
                //
                // For details see BC issue #749 (https://github.com/bcgit/bc-java/issues/749)
                encryptionMethod.setSessionKeyObfuscation(sessionKeyObfuscation);

                encryptedGenerator.addMethod(encryptionMethod);
            }

            mimeOutput = new DirectAccessByteArrayOutputStream();

            Hashtable<String, String> armorHeaders = PGPUtils.createArmorHeaders(); //NOSONAR

            armorHeaders.putAll(additionalArmorHeaders);

            OutputStream armored = armor ? new ArmoredOutputStream(mimeOutput, armorHeaders) : mimeOutput;

            try {
                OutputStream encryptor = encryptedGenerator.open(armored, mimeInput.available());

                try {
                    IOUtils.copy(mimeInput, encryptor);
                }
                finally {
                    encryptor.close();
                }
            }
            finally {
                armored.close();
            }

            IOUtils.copy(mimeOutput.getInputStream(), output);
        }
        catch (NoSuchProviderException | NoSuchAlgorithmException e) {
            throw new IOException(e);
        }
    }

    private DirectAccessByteArrayOutputStream compress(@Nonnull ByteArrayInputStream input)
    throws IOException
    {
        DirectAccessByteArrayOutputStream compressed = new DirectAccessByteArrayOutputStream();

        if (compressionAlgorithm != PGPCompressionAlgorithm.UNCOMPRESSED)
        {
            PGPCompressedDataGenerator compressedGenerator = new PGPCompressedDataGenerator(compressionAlgorithm.getTag());

            OutputStream compressorOutput = compressedGenerator.open(compressed);

            try {
                if (addLiteralPacket)
                {
                    PGPLiteralDataGenerator literalGenerator = new PGPLiteralDataGenerator();

                    OutputStream output = literalGenerator.open(compressorOutput, PGPLiteralData.BINARY, compressedFilename,
                            input.available(), new Date());
                    try {
                        IOUtils.copy(input, output);
                    }
                    finally {
                        output.close();
                    }
                }
                else {
                    IOUtils.copy(input, compressorOutput);
                }
            }
            finally {
                compressorOutput.close();
            }
        }
        else {
            if (addLiteralPacket)
            {
                PGPLiteralDataGenerator literalGenerator = new PGPLiteralDataGenerator();

                OutputStream output = literalGenerator.open(compressed, PGPLiteralData.BINARY, literalFilename,
                        input.available(), new Date());
                try {
                    IOUtils.copy(input, output);
                }
                finally {
                    output.close();
                }
            }
            else {
                IOUtils.copy(input, compressed);
            }
        }

        return compressed;
    }

    public PGPEncryptionAlgorithm getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }

    public void setEncryptionAlgorithm(PGPEncryptionAlgorithm encryptionAlgorithm) {
        this.encryptionAlgorithm = encryptionAlgorithm;
    }

    public PGPCompressionAlgorithm getCompressionAlgorithm() {
        return compressionAlgorithm;
    }

    public void setCompressionAlgorithm(PGPCompressionAlgorithm compressionAlgorithm) {
        this.compressionAlgorithm = compressionAlgorithm;
    }

    public String getCompressedFilename() {
        return compressedFilename;
    }

    public void setCompressedFilename(String compressedFilename) {
        this.compressedFilename = compressedFilename;
    }

    public String getLiteralFilename() {
        return literalFilename;
    }

    public void setLiteralFilename(String literalFilename) {
        this.literalFilename = literalFilename;
    }

    public boolean isAddIntegrityPacket() {
        return addIntegrityPacket;
    }

    public void setAddIntegrityPacket(boolean addIntegrityPacket) {
        this.addIntegrityPacket = addIntegrityPacket;
    }

    public boolean isAddLiteralPacket() {
        return addLiteralPacket;
    }

    public void setAddLiteralPacket(boolean addLiteralPacket) {
        this.addLiteralPacket = addLiteralPacket;
    }

    public boolean isArmor() {
        return armor;
    }

    public void setArmor(boolean armor) {
        this.armor = armor;
    }

    public void setAdditionalArmorHeaders(Map<String, String> additionalArmorHeaders) {
        this.additionalArmorHeaders.putAll(additionalArmorHeaders);
    }

    public boolean isSessionKeyObfuscation() {
        return sessionKeyObfuscation;
    }

    public void setSessionKeyObfuscation(boolean sessionKeyObfuscation) {
        this.sessionKeyObfuscation = sessionKeyObfuscation;
    }
}
