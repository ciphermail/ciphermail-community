/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.locale.DefaultLocale;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class PGPKeyRingEntityDAO extends GenericHibernateDAO
{
    /*
     * The name of the key ring we are managing
     */
    private final String keyRingName;

    private PGPKeyRingEntityDAO(@Nonnull SessionAdapter sessionAdapter, @Nonnull String keyRingName)
    {
        super(sessionAdapter);

        this.keyRingName = Objects.requireNonNull(keyRingName);
    }

    /**
     * Creates a new DAO instance
     * @param sessionAdapter a Hibernate database session
     * @return new DAO instance
     */
    public static PGPKeyRingEntityDAO getInstance(@Nonnull SessionAdapter sessionAdapter, @Nonnull String keyRingName) {
        return new PGPKeyRingEntityDAO(sessionAdapter, keyRingName);
    }

    /*
     * The table columns
     */
    private enum Column
    {
        ID                 ("id"),
        KEY_RING_NAME      ("keyRingName"),
        KEY_ID             ("keyID"),
        PARENT_KEY_ID      ("parentKeyID"),
        CREATION_DATE      ("creationDate"),
        EXPIRATION_DATE    ("expirationDate"),
        INSERTION_DATE     ("insertionDate"),
        ENCODED_PUBLIC_KEY ("encodedPublicKey"),
        PRIVATE_KEY_ALIAS  ("privateKeyAlias"),
        FINGERPRINT        ("fingerprint"),
        SHA256_FINGERPRINT ("sha256Fingerprint"),
        MASTER             ("master"),
        KEY_ID_HEX         ("keyIDHex");

        private final String name;

        Column(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private ScrollableResults<PGPKeyRingEntity> findWithRestrictionsScrollable(
            @Nonnull CriteriaBuilder cb,
            @Nonnull CriteriaQuery<PGPKeyRingEntity> criteriaQuery,
            @Nonnull Root<PGPKeyRingEntity> root,
            @Nonnull List<Predicate> restrictions,
            Integer firstResult,
            Integer maxResults)
    {
        // make sure we only get results for just one store
        restrictions.add(cb.equal(root.get(Column.KEY_RING_NAME.getName()), keyRingName));

        criteriaQuery.where(CollectionUtils.toArray(restrictions, Predicate.class));

        // Sort on insertion date
        criteriaQuery.orderBy(cb.asc(root.get(Column.INSERTION_DATE.getName())));

        Query<PGPKeyRingEntity> query = createQuery(criteriaQuery);

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxReslts to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.scroll(ScrollMode.FORWARD_ONLY);
    }

    private <T> Query<T> createEmailQuery(@Nonnull String baseQuery, String email,
            @Nonnull PGPSearchParameters searchParameters, Date date, @Nonnull Class<T> clazz)
    {
        if (email != null)
        {
            baseQuery = baseQuery + " join c.email e where e";
            baseQuery = baseQuery + (searchParameters.getMatch() == Match.EXACT ? " = :email" : " like :email");
        }
        else {
            // if email is null we want to get all the entries without an email address
            baseQuery = baseQuery + " left join c.email e where e is null";
        }

        if (searchParameters.getExpired() == Expired.MATCH_UNEXPIRED_ONLY) {
            baseQuery = baseQuery + " and (:time <= c.expirationDate or c.expirationDate is null)";
        }
        else if (searchParameters.getExpired() == Expired.MATCH_EXPIRED_ONLY) {
            baseQuery = baseQuery + " and :time > c.expirationDate";
        }

        if (searchParameters.getMissingKeyAlias() == MissingKeyAlias.NOT_ALLOWED) {
            baseQuery = baseQuery + " and c.privateKeyAlias is not null";
        }

        if (searchParameters.getKeyType() == PGPKeyType.MASTER_KEY) {
            baseQuery = baseQuery + " and c.master = true";
        }
        else if (searchParameters.getKeyType() == PGPKeyType.SUB_KEY) {
            baseQuery = baseQuery + " and c.master = false";
        }

        baseQuery = baseQuery + " and c.keyRingName = :keyRingName";

        Query<T> query = createQuery(baseQuery, clazz);

        if (email != null)
        {
            email = email.toLowerCase().trim();
            query.setParameter("email", email);
        }

        if (searchParameters.getExpired() == Expired.MATCH_EXPIRED_ONLY ||
            searchParameters.getExpired() == Expired.MATCH_UNEXPIRED_ONLY )
        {
            query.setParameter("time", date);
        }

        query.setParameter(Column.KEY_RING_NAME.getName(), keyRingName);

        return query;
    }

    private CloseableIterator<PGPKeyRingEntity> searchEmail(@Nonnull String email,
            @Nonnull PGPSearchParameters searchParameters, Date date, Integer firstResult, Integer maxResults)
    {
        // we need to get distinct results using the distinct keyword. This is not supported by Criteria
        // AFAIK so we will need to use HQL
        String baseQuery = "select distinct c from " + PGPKeyRingEntity.ENTITY_NAME + " c";

        Query<PGPKeyRingEntity> query = createEmailQuery(baseQuery, email, searchParameters, date,
                PGPKeyRingEntity.class);

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxReslts to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return new PGPKeyRingEntityIterator(query.scroll(ScrollMode.FORWARD_ONLY));
    }

    /**
     * returns the number of records that getByEmail would return
     */
    private long searchEmailCount(@Nonnull String email, @Nonnull PGPSearchParameters searchParameters, Date date)
    {
        // we need to get distinct results using the distinct keyword. This is not supported by Criteria
        // AFAIK so we will need to use HQL
        String baseQuery = "select count (distinct c) from " + PGPKeyRingEntity.ENTITY_NAME  + " c";

        return createEmailQuery(baseQuery, email, searchParameters, date, Long.class)
                .uniqueResultOptional().orElse(0L);
    }

    private <T> Query<T> createUserIDQuery(@Nonnull String baseQuery, String userID,
            @Nonnull PGPSearchParameters searchParameters, Date date, @Nonnull Class<T> clazz)
    {
        // We need to search with lowercase and lower() in SQL otherwise it will do strange things when searching
        // for user-ids. Somehow it did not match the first character is the first character in the database was
        // uppercase
        userID = StringUtils.lowerCase(userID);

        if (userID != null)
        {
            baseQuery = baseQuery + " join c.userID e where lower(e)";
            baseQuery = baseQuery + (searchParameters.getMatch() == Match.EXACT ? " = :userID" : " like :userID");
        }
        else {
            // if userID is null we want to get all the entries without an userID
            baseQuery = baseQuery + " left join c.userID e where e is null";
        }

        if (searchParameters.getExpired() == Expired.MATCH_UNEXPIRED_ONLY) {
            baseQuery = baseQuery + " and (:time <= c.expirationDate or c.expirationDate is null)";
        }
        else if (searchParameters.getExpired() == Expired.MATCH_EXPIRED_ONLY) {
            baseQuery = baseQuery + " and :time > c.expirationDate";
        }

        if (searchParameters.getMissingKeyAlias() == MissingKeyAlias.NOT_ALLOWED) {
            baseQuery = baseQuery + " and c.privateKeyAlias is not null";
        }

        if (searchParameters.getKeyType() == PGPKeyType.MASTER_KEY) {
            baseQuery = baseQuery + " and c.master = true";
        }
        else if (searchParameters.getKeyType() == PGPKeyType.SUB_KEY) {
            baseQuery = baseQuery + " and c.master = false";
        }

        baseQuery = baseQuery + " and c.keyRingName = :keyRingName";

        Query<T> query = createQuery(baseQuery, clazz);

        if (userID != null)
        {
            userID = userID.toLowerCase().trim();
            query.setParameter("userID", userID);
        }

        if (searchParameters.getExpired() == Expired.MATCH_EXPIRED_ONLY ||
            searchParameters.getExpired() == Expired.MATCH_UNEXPIRED_ONLY )
        {
            query.setParameter("time", date);
        }

        query.setParameter(Column.KEY_RING_NAME.getName(), keyRingName);

        return query;
    }

    private CloseableIterator<PGPKeyRingEntity> searchUserID(@Nonnull String userID,
            @Nonnull PGPSearchParameters searchParameters, Date date, Integer firstResult, Integer maxResults)
    {
        // we need to get distinct results using the distinct keyword. This is not supported by Criteria
        // AFAIK so we will need to use HQL
        String baseQuery = "select distinct c from " + PGPKeyRingEntity.ENTITY_NAME + " c";

        Query<PGPKeyRingEntity> query = createUserIDQuery(baseQuery, userID, searchParameters, date,
                PGPKeyRingEntity.class);

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxReslts to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return new PGPKeyRingEntityIterator(query.scroll(ScrollMode.FORWARD_ONLY));
    }

    /**
     * returns the number of records that searchUserID would return
     */
    private long searchUserIDCount(@Nonnull String userID, @Nonnull PGPSearchParameters searchParameters, Date date)
    {
        // we need to get distinct results using the distinct keyword. This is not supported by Criteria
        // AFAIK so we will need to use HQL
        String baseQuery = "select count (distinct c) from " + PGPKeyRingEntity.ENTITY_NAME  + " c";

        return createUserIDQuery(baseQuery, userID, searchParameters, date, Long.class)
                .uniqueResultOptional().orElse(0L);
    }

    private List<Predicate> createBaseRestrictions(@Nonnull CriteriaBuilder cb, @Nonnull Root<PGPKeyRingEntity> root,
            @Nonnull PGPSearchParameters searchParameters, Date date)
    {
        List<Predicate> restrictions = new LinkedList<>();

        if (searchParameters.getExpired() == Expired.MATCH_UNEXPIRED_ONLY)
        {
            restrictions.add(cb.or(
                cb.greaterThan(root.get(Column.EXPIRATION_DATE.getName()), date),
                cb.isNull(root.get(Column.EXPIRATION_DATE.getName()))));
        }
        else if (searchParameters.getExpired() == Expired.MATCH_EXPIRED_ONLY) {
            restrictions.add(cb.lessThan(root.get(Column.EXPIRATION_DATE.getName()), date));
        }

        if (searchParameters.getMissingKeyAlias() == MissingKeyAlias.NOT_ALLOWED) {
            restrictions.add(cb.isNotNull(root.get(Column.PRIVATE_KEY_ALIAS.getName())));
        }

        if (searchParameters.getKeyType() == PGPKeyType.MASTER_KEY) {
            restrictions.add(cb.equal(root.get(Column.MASTER.getName()), true));
        }
        else if (searchParameters.getKeyType() == PGPKeyType.SUB_KEY) {
            restrictions.add(cb.equal(root.get(Column.MASTER.getName()), false));
        }

        return restrictions;
    }

    private List<Predicate> createKeyIDRestrictions(@Nonnull CriteriaBuilder cb, @Nonnull Root<PGPKeyRingEntity> root,
            String keyID, @Nonnull PGPSearchParameters searchParameters, Date date)
    {
        keyID = StringUtils.upperCase(keyID);

        List<Predicate> restrictions = createBaseRestrictions(cb, root, searchParameters, date);

        if (searchParameters.getMatch() == Match.EXACT) {
            restrictions.add(cb.equal(root.get(Column.KEY_ID_HEX.getName()), keyID));
        }
        else {
            restrictions.add(cb.like(root.get(Column.KEY_ID_HEX.getName()), keyID));
        }

        return restrictions;
    }

    private List<Predicate> createFingerprintRestrictions(@Nonnull CriteriaBuilder cb,
            @Nonnull Root<PGPKeyRingEntity> root, String fingerprint, @Nonnull PGPSearchParameters searchParameters,
            Date date)
    {
        fingerprint = StringUtils.upperCase(fingerprint);

        List<Predicate> restrictions = createBaseRestrictions(cb, root, searchParameters, date);

        if (searchParameters.getMatch() == Match.EXACT) {
            restrictions.add(cb.equal(root.get(Column.FINGERPRINT.getName()), fingerprint));
        }
        else {
            restrictions.add(cb.like(root.get(Column.FINGERPRINT.getName()), fingerprint));
        }

        return restrictions;
    }

    private CloseableIterator<PGPKeyRingEntity> searchKeyID(@Nonnull String keyID,
            @Nonnull PGPSearchParameters searchParameters, Date date, Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<PGPKeyRingEntity> criteriaQuery = cb.createQuery(PGPKeyRingEntity.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        List<Predicate> restrictions = createKeyIDRestrictions(cb, root, keyID, searchParameters, date);

        return new PGPKeyRingEntityIterator(findWithRestrictionsScrollable(cb, criteriaQuery, root, restrictions,
                firstResult, maxResults));
    }

    private long searchKeyIDCount(@Nonnull String keyID, @Nonnull PGPSearchParameters searchParameters, Date date)
    {
        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = cb.createQuery(Long.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        List<Predicate> restrictions = createKeyIDRestrictions(cb, root, keyID, searchParameters, date);

        restrictions.add(cb.equal(root.get(Column.KEY_RING_NAME.getName()), keyRingName));

        criteriaQuery.where(CollectionUtils.toArray(restrictions, Predicate.class));

        criteriaQuery.select(cb.count(root));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    private CloseableIterator<PGPKeyRingEntity> searchFingerprint(@Nonnull String fingerprint,
            @Nonnull PGPSearchParameters searchParameters, Date date, Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<PGPKeyRingEntity> criteriaQuery = cb.createQuery(PGPKeyRingEntity.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        List<Predicate> restrictions = createFingerprintRestrictions(cb, root, fingerprint, searchParameters, date);

        return new PGPKeyRingEntityIterator(findWithRestrictionsScrollable(cb, criteriaQuery, root, restrictions,
                firstResult, maxResults));
    }

    private long searchFingerprintCount(@Nonnull String fingerprint, @Nonnull PGPSearchParameters searchParameters,
            Date date)
    {
        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = cb.createQuery(Long.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        List<Predicate> restrictions = createFingerprintRestrictions(cb, root, fingerprint, searchParameters, date);

        restrictions.add(cb.equal(root.get(Column.KEY_RING_NAME.getName()), keyRingName));

        criteriaQuery.where(CollectionUtils.toArray(restrictions, Predicate.class));

        criteriaQuery.select(cb.count(root));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    /**
     * Searches for PGPKeyRingEntity's based on the search parameters
     */
    public CloseableIterator<PGPKeyRingEntity> search(@Nonnull PGPSearchField searchField, @Nonnull String searchValue,
            PGPSearchParameters searchParameters, Date date, Integer firstResult, Integer maxResults)
    {
        // Use default PGPSearchParameters if searchParameters is not set
        if (searchParameters == null) {
            searchParameters = new PGPSearchParameters();
        }

        return switch (searchField) {
            case EMAIL       -> searchEmail(searchValue, searchParameters, date, firstResult, maxResults);
            case USER_ID     -> searchUserID(searchValue, searchParameters, date, firstResult, maxResults);
            case KEY_ID      -> searchKeyID(searchValue, searchParameters, date, firstResult, maxResults);
            case FINGERPRINT -> searchFingerprint(searchValue, searchParameters, date, firstResult, maxResults);
            default -> throw new IllegalArgumentException("Unsupported search field " + searchField);
        };
    }

    /**
     * Returns the number of results from a Search for PGPKeyRingEntity's based on the search parameters
     */
    public long searchCount(@Nonnull PGPSearchField searchField, @Nonnull String searchValue,
            PGPSearchParameters searchParameters, Date date)
    {
        // Use default PGPSearchParameters if searchParameters is not set
        if (searchParameters == null) {
            searchParameters = new PGPSearchParameters();
        }

        return switch (searchField) {
            case EMAIL       -> searchEmailCount(searchValue, searchParameters, date);
            case USER_ID     -> searchUserIDCount(searchValue, searchParameters, date);
            case KEY_ID      -> searchKeyIDCount(searchValue, searchParameters, date);
            case FINGERPRINT -> searchFingerprintCount(searchValue, searchParameters, date);
            default -> throw new IllegalArgumentException("Unsupported search field " + searchField);
        };
    }

    /**
     * Returns PGPKeyRingEntity's with the provided key id. Normally this should only return one entry. However
     * a key id in principle is not unique and a key collision can occur.
     */
    public CloseableIterator<PGPKeyRingEntity> getByKeyID(long keyID, MissingKeyAlias missingKeyAlias)
    {
        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<PGPKeyRingEntity> criteriaQuery = cb.createQuery(PGPKeyRingEntity.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        List<Predicate> restrictions = new LinkedList<>();

        restrictions.add(cb.equal(root.get(Column.KEY_ID.getName()), keyID));

        if (missingKeyAlias == MissingKeyAlias.NOT_ALLOWED) {
            restrictions.add(cb.isNotNull(root.get(Column.PRIVATE_KEY_ALIAS.getName())));
        }

        return new PGPKeyRingEntityIterator(findWithRestrictionsScrollable(cb, criteriaQuery, root, restrictions,
                null, null));
    }

    /**
     * Returns PGPKeyRingEntity's with the provided parent key id. Multiple "subkeys" can be returned.
     */
    public CloseableIterator<PGPKeyRingEntity> getByParentKeyID(long parentKeyID, Integer firstResult,
            Integer maxResults)
    {
        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<PGPKeyRingEntity> criteriaQuery = cb.createQuery(PGPKeyRingEntity.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        List<Predicate> restrictions = new LinkedList<>();

        restrictions.add(cb.equal(root.get(Column.PARENT_KEY_ID.getName()), parentKeyID));

        return new PGPKeyRingEntityIterator(findWithRestrictionsScrollable(cb, criteriaQuery, root, restrictions,
                firstResult, maxResults));
    }

    /**
     * Returns the number of PGPKeyRingEntity's returned by getByParentKeyID
     */
    public long getByParentKeyIDCount(long parentKeyID)
    {
        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = cb.createQuery(Long.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        criteriaQuery.where(cb.equal(root.get(Column.PARENT_KEY_ID.getName()), parentKeyID));

        criteriaQuery.select(cb.count(root));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    public PGPKeyRingEntity getBySHA256Fingerprint(@Nonnull String sha256Fingerprint)
    {
        sha256Fingerprint = sha256Fingerprint.toUpperCase(DefaultLocale.getDefaultLocale());

        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<PGPKeyRingEntity> criteriaQuery = cb.createQuery(PGPKeyRingEntity.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        criteriaQuery.where(
                cb.equal(root.get(Column.SHA256_FINGERPRINT.getName()), sha256Fingerprint),
                cb.equal(root.get(Column.KEY_RING_NAME.getName()), keyRingName));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }

    public PGPKeyRingEntity getByPublicKey(@Nonnull PGPPublicKey publicKey)
    throws PGPException
    {
        try {
            return getBySHA256Fingerprint(PGPPublicKeyInspector.getSHA256FingerprintHex(publicKey));
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException | IOException e) {
            throw new PGPException("Error getting public key", e);
        }
    }

    public long getSize()
    {
        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = cb.createQuery(Long.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        criteriaQuery.where(cb.equal(root.get(Column.KEY_RING_NAME.getName()), keyRingName));

        criteriaQuery.select(cb.count(root));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    public long getCount(PGPSearchParameters searchParameters, Date date)
    {
        // Use default PGPSearchParameters if searchParameters is not set
        if (searchParameters == null) {
            searchParameters = new PGPSearchParameters();
        }

        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = cb.createQuery(Long.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        List<Predicate> restrictions = createBaseRestrictions(cb, root, searchParameters, date);

        restrictions.add(cb.equal(root.get(Column.KEY_RING_NAME.getName()), keyRingName));

        criteriaQuery.where(CollectionUtils.toArray(restrictions, Predicate.class));

        criteriaQuery.select(cb.count(root));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    public CloseableIterator<PGPKeyRingEntity> getIterator(PGPSearchParameters searchParameters,
            Date date, Integer firstResult, Integer maxResults)
    {
        // Use default PGPSearchParameters if searchParameters is not set
        if (searchParameters == null) {
            searchParameters = new PGPSearchParameters();
        }

        CriteriaBuilder cb = getCriteriaBuilder();

        CriteriaQuery<PGPKeyRingEntity> criteriaQuery = cb.createQuery(PGPKeyRingEntity.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        return new PGPKeyRingEntityIterator(findWithRestrictionsScrollable(cb, criteriaQuery, root,
                createBaseRestrictions(cb, root, searchParameters, date), firstResult, maxResults));
    }

    public void deleteAll()
    throws IOException
    {
        CloseableIterator<PGPKeyRingEntity> iterator = getIterator(new PGPSearchParameters(),
                null, null, null);

        try {
            try {
                while (iterator.hasNext())
                {
                    PGPKeyRingEntity entry = iterator.next();

                    this.delete(entry);
                }
            }
            finally {
                // need to close the iterator
                iterator.close();
            }
        }
        catch (CloseableIteratorException e) {
            throw new IOException("error deleting all entries", e);
        }
    }

    /**
     * Returns an iterator over all keys of the given keyring. If keyRingName is null, all keys of all keyrings
     * will be returned
     */
    public static CloseableIterator<PGPKeyRingEntity> getAllKeys(@Nonnull SessionAdapter sessionAdapter,
            String keyRingName)
    {
        PGPKeyRingEntityDAO dao = PGPKeyRingEntityDAO.getInstance(sessionAdapter,
                StringUtils.defaultString(keyRingName));

        CriteriaBuilder cb = dao.getCriteriaBuilder();

        CriteriaQuery<PGPKeyRingEntity> criteriaQuery = cb.createQuery(PGPKeyRingEntity.class);

        Root<PGPKeyRingEntity> root = criteriaQuery.from(PGPKeyRingEntity.class);

        if (keyRingName != null) {
            criteriaQuery.where(cb.equal(root.get(Column.KEY_RING_NAME.getName()), keyRingName));
        }

        return new PGPKeyRingEntityIterator(dao.createQuery(criteriaQuery).scroll(ScrollMode.FORWARD_ONLY));
    }

    public String getKeyRingName() {
        return keyRingName;
    }
}
