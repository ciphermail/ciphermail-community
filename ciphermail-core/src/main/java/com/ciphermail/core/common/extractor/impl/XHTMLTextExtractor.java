/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.extractor.impl;

import java.io.IOException;
import java.io.Writer;

import javax.activation.MimeTypeParseException;

import org.apache.commons.io.input.CloseShieldInputStream;
import org.ccil.cowan.tagsoup.HTMLSchema;
import org.ccil.cowan.tagsoup.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.ext.DefaultHandler2;

import com.ciphermail.core.common.extractor.TextExtractorContext;
import com.ciphermail.core.common.mail.MimeUtils;
import com.ciphermail.core.common.util.RewindableInputStream;

/**
 *
 * XHTML text extractor. The extractor can cope with malformed HTML and converts any HTML to XHTML
 * while scanning for text.
 *
 * @author Martijn Brinkers
 *
 */
public class XHTMLTextExtractor extends AbstractXMLBaseTextExtractor
{
    private static final Logger logger = LoggerFactory.getLogger(XHTMLTextExtractor.class);

    /*
     * some HTML tags and attributes
     */
    private static final String HEAD_TAG = "head";
    private static final String BODY_TAG = "body";
    private static final String META_TAG = "meta";
    private static final String HTTP_EQUIV_ATTR = "http-equiv";
    private static final String CONTENT_ATTR = "content";

    /*
     * Creating a HTMLSchema is a heavy action so we will create a HTMLSchema
     * which can be reused in a thread safe manner.
     */
    private static final HTMLSchema CACHED_HTML_SCHEMA = new ThreadSafeHTMLSchema();

    /*
     * If true, only the HTML body is scanned
     */
    private boolean onlyScanBody;

    /*
     * SAX event handler that looks for <meta http-equiv="Content-Type" content="">
     * tags in the header section and tries to retrieve the encoding.
     */
    private static class FindEncodingHandler extends DefaultHandler2
    {
        /*
         * Some sanity check
         */
        private static final int MAX_ATTRIBUTES = 64;

        /*
         * True if we found the head tag
         */
        private boolean headFound;

        /*
         * The content encoding
         */
        private String encoding;

        private String parseContentType(String contentType)
        {
            String result = null;

            try {
                result = MimeUtils.getCharsetFromContentType(contentType);
            }
            catch (MimeTypeParseException e) {
                logger.debug("The following Content-Type could not be parsed: {}", contentType);
            }

            return result;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes)
        throws SAXException
        {
            if (HEAD_TAG.equalsIgnoreCase(qName))
            {
                headFound = true;

                return;
            }

            if (headFound && META_TAG.equalsIgnoreCase(qName))
            {
                if (attributes != null)
                {
                    String contentType = null;

                    boolean httpEquivFound = false;

                    int attrToCheck = attributes.getLength();

                    /*
                     * Limit the number of attributes as a sanity check
                     */
                    if (attrToCheck > MAX_ATTRIBUTES)
                    {
                        logger.warn("Number of attributes exceeds MAX_ATTRIBUTES " + MAX_ATTRIBUTES);

                        attrToCheck = MAX_ATTRIBUTES;
                    }

                    for (int i = 0; i < attrToCheck; i++)
                    {
                        String attrName = attributes.getQName(i);

                        if (HTTP_EQUIV_ATTR.equalsIgnoreCase(attrName) &&
                                "content-type".equalsIgnoreCase(attributes.getValue(i)))
                        {
                            httpEquivFound = true;

                            if (contentType != null) {
                                break;
                            }
                        }

                        if (CONTENT_ATTR.equalsIgnoreCase(attrName))
                        {
                            contentType = attributes.getValue(i);

                            if (httpEquivFound) {
                                break;
                            }
                        }

                    }

                    if (httpEquivFound && contentType != null)
                    {
                        this.encoding = parseContentType(contentType);

                        /*
                         * We are done scanning so force a stop
                         */
                        throw new StopParsingException();
                    }
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
        throws SAXException
        {
            if (headFound && HEAD_TAG.equalsIgnoreCase(qName)) {
                /*
                 * Done looking for encoding. We assume that the meta tags are within
                 * the header section
                 */
                throw new StopParsingException();
            }
        }

        public String getEncoding() {
            return encoding;
        }
    }

    /*
     * Extension of ExtractTextHandler with additional functionality
     */
    private static class ExtractTextHandlerExt extends ExtractTextHandler
    {
        /*
         * If true, only the body of the HTML will be scanned
         */
        private final boolean onlyScanBody;

        /*
         * True if the parser is within the body of the HTML
         */
        private boolean inBody;

        public ExtractTextHandlerExt(Writer writer, boolean skipComments, boolean onlyScanBody)
        {
            super(writer, skipComments);

            this.onlyScanBody = onlyScanBody;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes)
        throws SAXException
        {
            if (BODY_TAG.equals(qName)) {
                inBody = true;
            }

            super.startElement(uri, localName, qName, attributes);
        }

        @Override
        public void endElement(String uri, String localName, String qName)
        throws SAXException
        {
            if (BODY_TAG.equals(qName)) {
                inBody = false;
            }

            super.endElement(uri, localName, qName);
        }

        @Override
        protected void writeText(String text)
        throws SAXException
        {
            if (!onlyScanBody || inBody) {
                super.writeText(text);
            }
        }
    }

    private Parser createParser()
    throws SAXNotRecognizedException, SAXNotSupportedException
    {
        Parser parser = new Parser();

        /*
         * Creating a HTMLSchema is a heavy action so reuse the HTMLSchema
         */
        parser.setProperty(Parser.schemaProperty, CACHED_HTML_SCHEMA);

        return parser;
    }

    @Override
    protected String getEncodingFromDocument(RewindableInputStream input, TextExtractorContext context)
    throws IOException, SAXException
    {
        String encoding = null;

        long savedPosition = input.getPosition();

        try {
            /*
             * Parse the HTML and start looking for <meta http-equiv="Content-Type" content="">
             * in the header section of the XHTML.
             */
            Parser parser = createParser();

            /*
             * Wrap input in a CloseShieldInputStream to prevent the parser from closing the input
             */
            InputSource inputSource = new InputSource(CloseShieldInputStream.wrap(input));

            FindEncodingHandler findEncodingHandler = new FindEncodingHandler();

            parser.setContentHandler(findEncodingHandler);

            try {
                parser.parse(inputSource);
            }
            catch(StopParsingException e) {
                /*
                 * content encoding was found.
                 */
            }

            encoding = findEncodingHandler.getEncoding();
        }
        finally {
            input.setPosition(savedPosition);
        }

        return encoding;
    }

    @Override
    protected void parse(RewindableInputStream input, TextExtractorContext context, Writer writer)
    throws IOException, SAXException
    {
        Parser parser = createParser();

        InputSource inputSource = new InputSource(CloseShieldInputStream.wrap(input));

        inputSource.setEncoding(getEncoding(input, context));

        ExtractTextHandler parserHandler = new ExtractTextHandlerExt(writer, isSkipComments(), onlyScanBody);

        parser.setContentHandler(parserHandler);
        /*
         * Make sure comments are extracted as well
         */
        parser.setProperty(Parser.lexicalHandlerProperty, parserHandler);

        parser.parse(inputSource);
    }

    public XHTMLTextExtractor(int threshold, long maxPartSize) {
        super(threshold, maxPartSize);
    }

    public boolean isOnlyScanBody() {
        return onlyScanBody;
    }

    public void setOnlyScanBody(boolean onlyScanBody) {
        this.onlyScanBody = onlyScanBody;
    }
}
