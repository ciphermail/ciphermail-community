/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

/**
 * An OutputStream that throws an IOException when the number of bytes written exceeds the set maximum.
 *
 * This OutputStream is NOT thread safe
 *
 * @author Martijn Brinkers
 *
 */
public class SizeLimitedOutputStream extends OutputStream
{
    /*
     * The number of bytes written
     */
    private long byteCount;

    /*
     * the max number of items written
     */
    private final long threshold;

    /*
     * OutputStream to delegate to
     */
    private final OutputStream delegate;

    /*
     * If true an exception will be thrown when more than the limit is written, if false the OutputStream works like
     * a 'bitsink'
     */
    private final boolean throwExceptionOnLimitExceeded;

    /*
     * If strict is true, the number of bytes written will not be larger than the max. If false,
     * the number of bytes written may at max be the limit + 4096. Default is false because it's faster.
     */
    private final boolean strict;

    public SizeLimitedOutputStream(
            @Nonnull OutputStream delegate,
            long threshold,
            boolean throwExceptionOnLimitExceeded,
            boolean strict)
    {
        this.delegate = Objects.requireNonNull(delegate);
        this.threshold = threshold;
        this.throwExceptionOnLimitExceeded = throwExceptionOnLimitExceeded;
        this.strict = strict;
    }

    public SizeLimitedOutputStream(
            @Nonnull OutputStream delegate,
            long threshold,
            boolean throwExceptionOnLimitExceeded)
    {
        this(delegate, threshold, throwExceptionOnLimitExceeded, false);
    }

    public SizeLimitedOutputStream(OutputStream delegate, long threshold)
    {
        this(delegate, threshold, true);
    }

    public boolean isLimitReached()
    {
        return byteCount >= threshold;
    }

    @Override
    public void write(int i)
    throws IOException
    {
        if (!isLimitReached())
        {
            delegate.write(i);

            byteCount++;
        }
        else {
            if (throwExceptionOnLimitExceeded) {
                throw new LimitReachedException("Limit reached");
            }
        }
    }

    @Override
    public void write(@Nonnull byte[] b, int offset, int length)
    throws IOException
    {
        if (strict) {
            super.write(b, offset, length);
        }
        else {
            if(offset < 0 || offset > b.length || length < 0 || offset + length > b.length || offset + length < 0) {
                throw new IndexOutOfBoundsException();
            }

            if(length == 0) {
                return;
            }

            int nextOffset = offset;

            while(length > 0)
            {
                if (isLimitReached())
                {
                    if (throwExceptionOnLimitExceeded) {
                        throw new LimitReachedException("Limit reached");
                    }

                    break;
                }

                int nextLength = Math.min(length, 4096);

                delegate.write(b, nextOffset, nextLength);

                length = length - nextLength;
                nextOffset = nextOffset + nextLength;

                byteCount = byteCount + nextLength;
            }
        }
    }

    @Override
    public void close()
    throws IOException
    {
        delegate.close();
    }

    @Override
    public void flush()
    throws IOException
    {
        delegate.flush();
    }

    public long getByteCount() {
        return byteCount;
    }
}
