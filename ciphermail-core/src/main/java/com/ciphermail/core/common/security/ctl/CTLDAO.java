/*
 * Copyright (c) 2009-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ctl;

import com.google.common.annotations.VisibleForTesting;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

public class CTLDAO extends GenericHibernateDAO
{
    /*
     * The name of the CTL this DAO works for. If null, the null store name will be used
     */
    private final String name;

    /**
     * Creates a DAO that access the CTL store.
     */
    private CTLDAO(@Nonnull SessionAdapter session, @Nonnull String name)
    {
        super(session);

        this.name = Objects.requireNonNull(name);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static CTLDAO getInstance(@Nonnull SessionAdapter session, @Nonnull String name) {
        return new CTLDAO(session, name);
    }

    public @Nonnull CTLEntity createCTLEntry(@Nonnull String thumbprint) {
        return new CTLEntity(name, thumbprint);
    }

    public List<CTLEntity> getCTLs(Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<CTLEntity> criteriaQuery = criteriaBuilder.createQuery(CTLEntity.class);

        Root<CTLEntity> rootEntity = criteriaQuery.from(CTLEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(CTLEntity.NAME_COLUMN), name));

        Query<CTLEntity> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    /**
     * Returns ALL entries irrespective of store name. Only use this if you really need all CTL entries.
     */
    public List<CTLEntity> getAllCTLs(Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<CTLEntity> criteriaQuery = criteriaBuilder.createQuery(CTLEntity.class);

        criteriaQuery.from(CTLEntity.class);

        Query<CTLEntity> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    public long size()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<CTLEntity> rootEntity = criteriaQuery.from(CTLEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(CTLEntity.NAME_COLUMN), name));

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    public CTLEntity getCTL(@Nonnull String thumbprint)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<CTLEntity> criteriaQuery = criteriaBuilder.createQuery(CTLEntity.class);

        Root<CTLEntity> rootEntity = criteriaQuery.from(CTLEntity.class);

        criteriaQuery.where(
                criteriaBuilder.equal(rootEntity.get(CTLEntity.NAME_COLUMN), name),
                criteriaBuilder.equal(rootEntity.get(CTLEntity.THUMBPRINT_COLUMN), thumbprint));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }

    /**
     * Delete the entries from the named CTL
     */
    public void delete()
    {
        List<CTLEntity> all = getCTLs(null, null);

        for (CTLEntity entry : all) {
            delete(entry);
        }
    }

    /**
     * Deletes all the entries irrespective of name (used while testing to make
     * sure that all entries are removed)
     */
    @VisibleForTesting
    public static void deleteAllEntries(SessionAdapter sessionAdapter)
    {
        CTLDAO dao = CTLDAO.getInstance(sessionAdapter, "ignored");

        List<CTLEntity> all = dao.findAll(CTLEntity.class);

        for (CTLEntity entry : all) {
            dao.delete(entry);
        }
    }
}
