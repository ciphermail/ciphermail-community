/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1IA5String;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.security.cert.CRLException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CRLDistributionPointsInspector
{
    private static final Logger logger = LoggerFactory.getLogger(CRLDistributionPointsInspector.class);

    private final CRLDistPoint crlDistPoint;

    public CRLDistributionPointsInspector(@Nonnull CRLDistPoint crlDistPoint) {
        this.crlDistPoint = Objects.requireNonNull(crlDistPoint);
    }

    /*
     * Returns a set of URIs in (in String form) from the distribution point names. Some certificates
     * contain illegal URIs (for example they contain spaces).
     */
    public Set<String> getURIDistributionPointNames()
    throws CRLException
    {
        return getURIDistributionPointNames(crlDistPoint);
    }

    /*
     * Returns a set of URIs in (in String form) from the distribution point names. Some certificates
     * contain illegal URIs (for example they contain spaces).
     */
    public static @Nonnull Set<String> getURIDistributionPointNames(CRLDistPoint crlDistPoint)
    throws CRLException
    {
        try {
            Set<String> uris = new HashSet<>();

            if (crlDistPoint == null) {
                return uris;
            }

            DistributionPoint[] distributionPoints = crlDistPoint.getDistributionPoints();

            if (distributionPoints != null)
            {
                for (DistributionPoint distributionPoint : distributionPoints)
                {
                    if (distributionPoint == null)
                    {
                        logger.debug("Distributionpoint is null.");
                        continue;
                    }

                    DistributionPointName distributionPointName = distributionPoint.getDistributionPoint();

                    // only return full names containing URIs
                    if (distributionPointName != null && distributionPointName.getType() ==
                                DistributionPointName.FULL_NAME)
                    {
                        ASN1Encodable name = distributionPointName.getName();

                        if (name != null)
                        {
                            GeneralName[] names = GeneralNames.getInstance(name).getNames();

                            for (GeneralName generalName : names)
                            {
                                if (generalName != null && generalName.getTagNo() == GeneralName.uniformResourceIdentifier &&
                                        generalName.getName() != null)
                                {
                                    String uri = ASN1IA5String.getInstance(generalName.getName()).getString();

                                    uris.add(uri);
                                }
                            }
                        }
                    }
                }
            }

            return uris;
        }
        catch(IllegalArgumentException e) {
            // Can be thrown when the CRL dist. point contains illegal ASN1.
            throw new CRLException("Error getting the CRL distribution point names.", e);
        }
    }
}
