/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.certificate.validator.PKITrustCheckCertificateValidatorFactory;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilderFactory;
import com.ciphermail.core.common.security.certpath.TrustAnchorBuilder;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crl.CRLPathBuilderFactory;
import com.ciphermail.core.common.security.crl.RevocationChecker;
import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.util.RequiredByJames;

@RequiredByJames
public interface PKISecurityServices
{
    /**
     * Returns the store which stores certificates and keys.
     */
    KeyAndCertStore getKeyAndCertStore();

    /**
     * Returns the RootStore.
     */
    X509CertStoreExt getRootStore();

    /**
     * Returns the CRLStore.
     */
    X509CRLStoreExt getCRLStore();

    /**
     * Returns a factory to create a path builder used to check the trust of a certificate.
     * Most likely the CertificatePathBuilder returned from the factory is not thread safe and
     * should therefore be created on-demand and not shared between threads..
     */
    CertificatePathBuilderFactory getCertificatePathBuilderFactory();

    /**
     * Returns a factory to create a CRL path builder used to check the trust of a CRL.
     * Most likely the CRLPathBuilder returned from the factory is not thread safe and
     * should therefore be created on-demand and not shared between threads..
     */
    CRLPathBuilderFactory getCRLPathBuilderFactory();

    /**
     * Creates a PKITrustCheckCertificateValidatorFactory which can be used to check the trust
     * status of a certificate (not revoked, not expired, signed by a trusted root etc.).
     * Most likely the PKITrustCheckCertificateValidator created is not thread safe and should
     * therefore be created on-demand and not shared between threads.
     */
    PKITrustCheckCertificateValidatorFactory getPKITrustCheckCertificateValidatorFactory();

    /**
     * Returns the CRL revocation checker.
     */
    RevocationChecker getCRLStoreRevocationChecker()
    throws CRLStoreException;

    /**
     * Returns the associated TrustAnchorBuilder.
     */
    TrustAnchorBuilder getTrustAnchorBuilder();
}
