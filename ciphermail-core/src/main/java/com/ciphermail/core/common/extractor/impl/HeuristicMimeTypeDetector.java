/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.extractor.impl;

import com.ciphermail.core.common.extractor.DetectedMimeType;
import com.ciphermail.core.common.extractor.MimeTypeDetector;
import com.ciphermail.core.common.util.RewindableInputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.apache.tika.mime.MediaType;

import java.io.IOException;
import java.util.Optional;

/**
 * Implementation of MimeTypeDetector that uses Tika's MIME type detection to detect the mime type.
 *
 * @author Martijn Brinkers
 *
 */
public class HeuristicMimeTypeDetector implements MimeTypeDetector
{
    /*
     * Tika is used for detecting Mime/Media types
     */
    private final Tika tika = new Tika();

    @Override
    public DetectedMimeType detectMimeType(RewindableInputStream input, String resourceName)
    throws IOException
    {
        MediaType type;

        long savedPosition = input.getPosition();

        try {
            type = Optional.ofNullable(tika.detect(input)).map(MediaType::parse).orElse(null);
        }
        finally {
            input.setPosition(savedPosition);
        }

        if ((type == null || MediaType.OCTET_STREAM.equals(type)) && StringUtils.isNotEmpty(resourceName))
        {
            // Try to detect using the resourceName
            type = Optional.ofNullable(tika.detect(resourceName)).map(MediaType::parse).orElse(null);
        }

        if (type == null) {
            type = MediaType.OCTET_STREAM;
        }

        return new DetectedMimeTypeImpl(type);
    }
}
