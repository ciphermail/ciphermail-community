/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties.hibernate;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.util.CloseableIterator;
import org.hibernate.ScrollMode;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.List;

public class NamedBlobDAO extends GenericHibernateDAO
{
    private static final String ENTITY_NAME = NamedBlobEntity.ENTITY_NAME;

    private NamedBlobDAO(@Nonnull SessionAdapter session) {
        super(session);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static NamedBlobDAO getInstance(@Nonnull SessionAdapter session) {
        return new NamedBlobDAO(session);
    }

    public NamedBlobEntity getNamedBlob(@Nonnull String category, @Nonnull String name)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<NamedBlobEntity> criteriaQuery = criteriaBuilder.createQuery(NamedBlobEntity.class);

        Root<NamedBlobEntity> rootEntity = criteriaQuery.from(NamedBlobEntity.class);

        criteriaQuery.where(criteriaBuilder.and(
                criteriaBuilder.equal(rootEntity.get(NamedBlobEntity.CATEGORY_COLUMN), category),
                criteriaBuilder.equal(rootEntity.get(NamedBlobEntity.NAME_COLUMN), name)));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }

    public List<NamedBlobEntity> getByCategory(@Nonnull String category, Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<NamedBlobEntity> criteriaQuery = criteriaBuilder.createQuery(NamedBlobEntity.class);

        Root<NamedBlobEntity> rootEntity = criteriaQuery.from(NamedBlobEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(NamedBlobEntity.CATEGORY_COLUMN), category));

        criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(NamedBlobEntity.NAME_COLUMN)));

        Query<NamedBlobEntity> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    public long getByCategoryCount(@Nonnull String category)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<NamedBlobEntity> rootEntity = criteriaQuery.from(NamedBlobEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(NamedBlobEntity.CATEGORY_COLUMN), category));

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    /*
     * A NamedBlobEntity can reference other NamedBlobEntity's. This method returns the number
     * of times the NamedBlobEntity is referenced by other NamedBlobEntity's.
     */
    public long getReferencedByCount(@Nonnull NamedBlobEntity entity)
    {
        String hql = "select count(*) from " + ENTITY_NAME + " u where :other in (select c from u.namedBlobs c)";

        Query<Long> query = createQuery(hql, Long.class);

        query.setParameter("other", entity);

        return query.uniqueResultOptional().orElse(0L);
    }

    /*
     * A NamedBlobEntity can reference other NamedBlobEntity's. This method returns all the NamedBlobEntity's
     * that references the NamedBlobEntity.
     */
    public List<NamedBlobEntity> getReferencedBy(@Nonnull NamedBlobEntity entity, Integer firstResult,
            Integer maxResults)
    {
        String hql = "select u from " + ENTITY_NAME + " u where :other in (select c from u.namedBlobs c)";

        Query<NamedBlobEntity> query = createQuery(hql, NamedBlobEntity.class);

        query.setParameter("other", entity);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.list();
    }

    public void deleteNamedBlob(@Nonnull String category, @Nonnull String name)
    {
        NamedBlobEntity entity = getNamedBlob(category, name);

        if (entity != null) {
            delete(entity);
        }
    }

    public void deleteAll()
    {
        List<NamedBlobEntity> blobs = findAll(NamedBlobEntity.class);

        for (NamedBlobEntity blob : blobs) {
            delete(blob);
        }
    }

    public CloseableIterator<NamedBlobEntity> getNamedBlobIterator()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<NamedBlobEntity> criteriaQuery = criteriaBuilder.createQuery(NamedBlobEntity.class);

        criteriaQuery.select(criteriaQuery.from(NamedBlobEntity.class));

        return new NamedBlobIterator(createQuery(criteriaQuery).scroll(ScrollMode.FORWARD_ONLY));
    }
}
