/*
 * Copyright (c) 2021-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.http;

import org.apache.commons.lang.time.DateUtils;
import org.apache.hc.client5.http.config.ConnectionConfig;
import org.apache.hc.client5.http.config.TlsConfig;
import org.apache.hc.client5.http.impl.DefaultHttpRequestRetryStrategy;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.async.HttpAsyncClientBuilder;
import org.apache.hc.client5.http.impl.async.HttpAsyncClients;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManager;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManagerBuilder;
import org.apache.hc.client5.http.ssl.ClientTlsStrategyBuilder;
import org.apache.hc.core5.http.nio.ssl.TlsStrategy;
import org.apache.hc.core5.http2.HttpVersionPolicy;
import org.apache.hc.core5.reactor.IOReactorConfig;
import org.apache.hc.core5.ssl.SSLContextBuilder;
import org.apache.hc.core5.util.TimeValue;
import org.apache.hc.core5.util.Timeout;

import java.security.GeneralSecurityException;
import java.util.LinkedList;
import java.util.List;

public class CloseableHttpAsyncClientFactoryImpl implements CloseableHttpAsyncClientFactory,
    ClientTlsStrategyBuilderBuilder, SSLContextBuilderBuilder, PoolingAsyncClientConnectionManagerBuilderBuilder,
    IOReactorConfigBuilderBuilder
{
    /*
     * Period after inactivity, in milliseconds, after which persistent connections must be checked to ensure they are
     * still valid.
     */
    private long validateAfterInactivity = DateUtils.MILLIS_PER_SECOND;

    /*
     * Maximum time, in milliseconds, persistent connections can stay idle while kept alive  in the connection pool.
     * Connections whose inactivity period exceeds this value will get closed and evicted from the pool.
     */
    private long evictIdleConnectionsMaxIdleTime = 5 * DateUtils.MILLIS_PER_SECOND;

    /*
     *  The default socket timeout in milliseconds value for non-blocking I/O operations.
     */
    private long soTimeout = 30 * DateUtils.MILLIS_PER_SECOND;

    /*
     * Determines the timeout until a new connection is fully established
     */
    private long connectTimeout = 30 * DateUtils.MILLIS_PER_SECOND;

    /*
     * The default HTTP user agent
     */
    private String httpUserAgent = HTTPUtils.HTTP_USER_AGENT;

    /*
     * The HTTP version to use.
     */
    private HttpVersionPolicy versionPolicy = HttpVersionPolicy.NEGOTIATE;

    /*
     * The registered ClientTlsStrategyBuilderConfigurer's which post configures the ClientTlsStrategyBuilder
     */
    private List<ClientTlsStrategyBuilderConfigurer> additionalClientTlsStrategyBuilderConfigurers = new LinkedList<>();

    /*
     * The registered SSLContextBuilderConfigurer's which post configures the SSLContextBuilder
     */
    private List<SSLContextBuilderConfigurer> additionalSSLContextBuilderConfigurers = new LinkedList<>();

    /*
     * The registered PoolingAsyncClientConnectionManagerBuilderConfigurer's which post configures the
     * PoolingAsyncClientConnectionManagerBuilder
     */
    private List<PoolingAsyncClientConnectionManagerBuilderConfigurer>
            additionalPoolingAsyncClientConnectionManagerBuilderConfigurers = new LinkedList<>();

    /*
     * The registered IOReactorConfigBuilderConfigurer's which post configures the IOReactorConfig.Builder
     */
    private List<IOReactorConfigBuilderConfigurer> additionalIOReactorConfigBuilderConfigurers = new LinkedList<>();

    /**
     * Provides the proxy details
     */
    private final HTTPClientProxyProvider httpClientProxyProvider;

    public CloseableHttpAsyncClientFactoryImpl(HTTPClientProxyProvider httpClientProxyProvider) {
        this.httpClientProxyProvider = httpClientProxyProvider;
    }

    @Override
    public CloseableHttpAsyncClient createClient()
    throws GeneralSecurityException
    {
        ClientTlsStrategyBuilder tlsStrategyBuilder = ClientTlsStrategyBuilder.create();

        configureDefaultClientTlsStrategyBuilder(tlsStrategyBuilder);

        if (!additionalSSLContextBuilderConfigurers.isEmpty())
        {
            SSLContextBuilder sslContextBuilder = SSLContextBuilder.create();

            for (SSLContextBuilderConfigurer configurer : additionalSSLContextBuilderConfigurers) {
                configurer.configureBuilder(sslContextBuilder);
            }

            tlsStrategyBuilder.setSslContext(sslContextBuilder.build());
        }

        for (ClientTlsStrategyBuilderConfigurer configurer : additionalClientTlsStrategyBuilderConfigurers) {
            configurer.configureBuilder(tlsStrategyBuilder);
        }

        TlsStrategy tlsStrategy = tlsStrategyBuilder.build();

        PoolingAsyncClientConnectionManagerBuilder connectionManagerBuilder = PoolingAsyncClientConnectionManagerBuilder
                    .create();

        configureDefaultPoolingAsyncClientConnectionManagerBuilder(connectionManagerBuilder, tlsStrategy);

        for (PoolingAsyncClientConnectionManagerBuilderConfigurer configurer :
                additionalPoolingAsyncClientConnectionManagerBuilderConfigurers)
        {
            configurer.configureBuilder(connectionManagerBuilder);
        }

        PoolingAsyncClientConnectionManager poolingAsyncClientConnectionManager = connectionManagerBuilder.build();

        IOReactorConfig.Builder ioReactorConfigBuilder = IOReactorConfig.custom();

        configureDefaultIOReactorConfigBuilder(ioReactorConfigBuilder);

        for (IOReactorConfigBuilderConfigurer configurer : additionalIOReactorConfigBuilderConfigurers) {
            configurer.configureBuilder(ioReactorConfigBuilder);
        }

        IOReactorConfig ioReactorConfig = ioReactorConfigBuilder.build();

        HttpAsyncClientBuilder clientBuilder = HttpAsyncClients.custom();

        configureDefaultHttpAsyncClientBuilder(clientBuilder, ioReactorConfig, poolingAsyncClientConnectionManager);

        if (httpClientProxyProvider != null) {
            clientBuilder.setProxy(httpClientProxyProvider.getProxyHost());
        }

        CloseableHttpAsyncClient client = clientBuilder.build();

        client.start();

        return client;
    }

    protected void configureDefaultClientTlsStrategyBuilder(ClientTlsStrategyBuilder clientTlsStrategyBuilder) {
        clientTlsStrategyBuilder.useSystemProperties();
    }

    protected void configureDefaultPoolingAsyncClientConnectionManagerBuilder(
            PoolingAsyncClientConnectionManagerBuilder connectionManagerBuilder,
            TlsStrategy tlsStrategy)
    {
        connectionManagerBuilder
            .setTlsStrategy(tlsStrategy)
            .setDefaultTlsConfig(TlsConfig.custom().setVersionPolicy(versionPolicy).build())
            .setDefaultConnectionConfig(ConnectionConfig.custom()
                .setValidateAfterInactivity(TimeValue.ofMilliseconds(validateAfterInactivity))
                .setConnectTimeout(Timeout.ofMilliseconds(connectTimeout))
                .build());
    }

    protected void configureDefaultIOReactorConfigBuilder(IOReactorConfig.Builder ioReactorConfigBuilder) {
        ioReactorConfigBuilder.setSoTimeout(Timeout.ofMilliseconds(soTimeout));
    }

    protected void configureDefaultHttpAsyncClientBuilder(HttpAsyncClientBuilder httpAsyncClientBuilder,
            IOReactorConfig ioReactorConfig,
            PoolingAsyncClientConnectionManager poolingAsyncClientConnectionManager)
    {
        httpAsyncClientBuilder
            .useSystemProperties()
            .setIOReactorConfig(ioReactorConfig)
            .setConnectionManager(poolingAsyncClientConnectionManager)
            .setUserAgent(httpUserAgent)
            .setRetryStrategy(DefaultHttpRequestRetryStrategy.INSTANCE)
            .evictExpiredConnections()
            .evictIdleConnections(Timeout.ofMilliseconds(evictIdleConnectionsMaxIdleTime));
    }

    public long getValidateAfterInactivity() {
        return validateAfterInactivity;
    }

    public void setValidateAfterInactivity(long validateAfterInactivity) {
        this.validateAfterInactivity = validateAfterInactivity;
    }

    public long getEvictIdleConnectionsMaxIdleTime() {
        return evictIdleConnectionsMaxIdleTime;
    }

    public void setEvictIdleConnectionsMaxIdleTime(long evictIdleConnectionsMaxIdleTime) {
        this.evictIdleConnectionsMaxIdleTime = evictIdleConnectionsMaxIdleTime;
    }

    public long getSoTimeout() {
        return soTimeout;
    }

    public void setSoTimeout(long soTimeout) {
        this.soTimeout = soTimeout;
    }

    public long getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(long connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public String getHttpUserAgent() {
        return httpUserAgent;
    }

    public void setHttpUserAgent(String httpUserAgent) {
        this.httpUserAgent = httpUserAgent;
    }

    public HttpVersionPolicy getVersionPolicy() {
        return versionPolicy;
    }

    public void setVersionPolicy(HttpVersionPolicy versionPolicy) {
        this.versionPolicy = versionPolicy;
    }

    @Override
    public List<ClientTlsStrategyBuilderConfigurer> getAdditionalClientTlsStrategyBuilderConfigurers() {
        return additionalClientTlsStrategyBuilderConfigurers;
    }

    @Override
    public void setAdditionalClientTlsStrategyBuilderConfigurers(
            List<ClientTlsStrategyBuilderConfigurer> additionalClientTlsStrategyBuilderConfigurers)
    {
        this.additionalClientTlsStrategyBuilderConfigurers = additionalClientTlsStrategyBuilderConfigurers;
    }

    @Override
    public List<SSLContextBuilderConfigurer> getAdditionalSSLContextBuilderConfigurers() {
        return additionalSSLContextBuilderConfigurers;
    }

    @Override
    public void setAdditionalSSLContextBuilderConfigurers(
            List<SSLContextBuilderConfigurer> additionalSSLContextBuilderConfigurers)
    {
        this.additionalSSLContextBuilderConfigurers = additionalSSLContextBuilderConfigurers;
    }

    @Override
    public List<PoolingAsyncClientConnectionManagerBuilderConfigurer> getAdditionalPoolingAsyncClientConnectionManagerBuilderConfigurers() {
        return additionalPoolingAsyncClientConnectionManagerBuilderConfigurers;
    }

    @Override
    public void setAdditionalPoolingAsyncClientConnectionManagerBuilderConfigurers(
            List<PoolingAsyncClientConnectionManagerBuilderConfigurer> additionalPoolingAsyncClientConnectionManagerBuilderConfigurers)
    {
        this.additionalPoolingAsyncClientConnectionManagerBuilderConfigurers = additionalPoolingAsyncClientConnectionManagerBuilderConfigurers;
    }

    @Override
    public List<IOReactorConfigBuilderConfigurer> getAdditionalIOReactorConfigBuilderConfigurers() {
        return additionalIOReactorConfigBuilderConfigurers;
    }

    @Override
    public void setAdditionalIOReactorConfigBuilderConfigurers(
            List<IOReactorConfigBuilderConfigurer> additionalIOReactorConfigBuilderConfigurers)
    {
        this.additionalIOReactorConfigBuilderConfigurers = additionalIOReactorConfigBuilderConfigurers;
    }
}
