/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password.validator;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import org.passay.PasswordData;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * PasswordData wrapper that returns the local part of an email address as the username if the username
 * is an email address.
 *
 * @author Martijn Brinkers
 *
 */
public class PasswordDataEmailWrapper extends PasswordData
{
    /*
     * The Password data to delegate all calls to
     */
    private final PasswordData delegate;

    public PasswordDataEmailWrapper(@Nonnull PasswordData delegate) {
        this.delegate = Objects.requireNonNull(delegate);
    }

    /**
     * Sets the password.
     *
     * @param  password
     */
    @Override
    public void setPassword(String password) {
        delegate.setPassword(password);
    }

    /**
     * Returns the password.
     *
     * @return  password
     */
    @Override
    public String getPassword() {
      return delegate.getPassword();
    }

    /**
     * Sets the username.
     *
     * @param  s  username
     */
    @Override
    public void setUsername(String s) {
        delegate.setUsername(s);
    }

    /**
     * Returns the username.
     *
     * @return  username
     */
    @Override
    public String getUsername()
    {
        String username = delegate.getUsername();

        String email = EmailAddressUtils.canonicalizeAndValidate(username, true);

        if (email != null) {
            // username is a valid email address. Extract local part.
            username = EmailAddressUtils.getLocalPart(username);
        }

        return username;
    }

    /**
     * Returns a string representation of this object.
     *
     * @return  string representation
     */
    @Override
    public String toString() {
        return delegate.toString();
    }
}
