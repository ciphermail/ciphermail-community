/*
 * Copyright (c) 2008-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.handler;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MimeUtils;
import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.mail.PartScanner;
import com.ciphermail.core.common.mail.PartScanner.PartListener;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.NotHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.ProtectedContentHeaderNameMatcher;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Sometimes S/MIME messages are modified by content scanners or disclaimer services (SMTP servers that add a disclaimer).
 * Most non S/MIME aware disclaimer servers add a banner to the message and change the content-type of the message to
 * a multipart/mixed. The encrypted part gets added as an attachment. The message is no longer a valid S/MIME message and
 * SMTP clients (like Outlook) are no longer able to digest the message. AttachedSMIMEHandler tries to create a valid
 * S/MIME message from the attached encrypted blob by changing the content-type of the attachment and attach the blob
 * as an encrypted message (a RFC 822 attachment). Now Outlook and other SMTP clients are able to read the S/MIME message.
 *
 *
 * @author Martijn Brinkers
 *
 */
public class AttachedSMIMEHandler
{
    private static final Logger logger = LoggerFactory.getLogger(AttachedSMIMEHandler.class);

    private final PartHandler partHandler;

    /*
     * Name of the attachment given to the attached RFC822 message when a S/MIME blob
     * is converted to an attached message (RFC822 attachment)
     */
    private String messageFilename = "attached message.eml";

    /*
     * Collection of S/MIME parts found in the source message
     */
    private final List<MimeMessage> sMIMEMessages = new LinkedList<>();

    /*
     * Collection of non S/MIME parts found in the source message
     */
    private final List<Part> nonSMIMEParts = new LinkedList<>();

    /*
     * Possible S/MIME filename
     */
    private String[] smimeFilenames = new String[] {"smime.p7m", "smime.p7s", "smime.p7z"};

    public static interface PartHandler {
        MimeMessage handlePart(Part source)
        throws SMIMEHandlerException;
    }

    public AttachedSMIMEHandler(@Nonnull PartHandler partHandler) {
        this.partHandler = Objects.requireNonNull(partHandler);
    }

    /*
     * Sets the name of the rfc822 file that gets attached when the message contains more than one
     * body item.
     */
    public void setMessageFilename(String filename) {
        this.messageFilename = filename;
    }

    @SuppressWarnings("unchecked")
    private void copyNonContentHeaders(@Nonnull Part source, @Nonnull  Part target)
    throws MessagingException
    {
        // only copy headers from source to target that are not already in the target
        String[] protectedHeaders = HeaderUtils.getMatchingHeaderNames(target.getAllHeaders(),
                source.getAllHeaders());

        HeaderMatcher contentMatcher = new ProtectedContentHeaderNameMatcher(protectedHeaders);

        HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);

        // copy all non-content headers from source message to the new message
        HeaderUtils.copyHeaders(source, target, nonContentMatcher);
    }

    private boolean isSMIMEFilename(String filename)
    {
        if (smimeFilenames == null) {
            return false;
        }

        filename = StringUtils.trimToNull(filename);

        if (filename == null) {
            return false;
        }

        filename = StringUtils.lowerCase(filename);

        for (String smimeFilename : smimeFilenames)
        {
            if (filename.equals(smimeFilename)) {
                return true;
            }
        }

        return false;
    }

    private boolean onPart(Part parent, @Nonnull Part part)
    throws PartException
    {
        MimeMessage newMessage = null;

        try {
            // if the part is a attached message (rfc822) we should inspect the attached message
            if (part.isMimeType("message/rfc822"))
            {
                try {
                    Part messagePart = BodyPartUtils.extractFromRFC822(part);

                    newMessage = partHandler.handlePart(messagePart);
                }
                catch (MessagingException | IOException e) {
                    logger.warn("Unable to handle attached RFC822 message.", e);
                }
            }
            else {
                String filename = MimeUtils.getFilenameQuietly(part);

                // Only scan for S/MIME parts if multipart or if filename is a supported S/MIME filename
                if (part.isMimeType("multipart/*") || isSMIMEFilename(filename))
                {
                    newMessage = partHandler.handlePart(part);

                    if (newMessage != null)
                    {
                        // The part should have the headers of the parent
                        copyNonContentHeaders(parent, newMessage);
                    }
                }
            }
        }
        catch (MessagingException | SMIMEHandlerException e) {
            throw new PartException(e);
        }

        if (newMessage != null) {
            // part was an S/MIME message.
            sMIMEMessages.add(newMessage);
        }
        else {
            // part was not S/MIME.

            boolean addPart = true;

            try {
                // If the part does not contain relevant data we do not have to add it.
                if (part.isMimeType("text/*"))
                {
                    try {
                        Object content = part.getContent();

                        if (content instanceof String stringContent)
                        {
                            if (stringContent.trim().length() == 0)
                            {
                                logger.debug("Part is empty.");

                                addPart = false;
                            }
                        }
                    } catch(UnsupportedEncodingException e) {
                        // ignore
                    }
                }
                else {
                    if (part.getSize() == 0)
                    {
                        logger.debug("Part size is 0.");

                        addPart = false;
                    }
                }
            }
            catch (IOException | MessagingException e)
            {
                logger.error("Error investigating part.", e);

                addPart = false;
            }

            if (addPart) {
                nonSMIMEParts.add(part);
            }
        }

        return true;
    }

    /*
     * Build a message from S/MIME parts and non S/MIME parts
     */
    private MimeMessage buildMessage(Part parent)
    throws MessagingException, IOException
    {
        if (sMIMEMessages.isEmpty()) {
            throw new IllegalStateException("sMIMEMessages is empty");
        }

        MimeMessage newMessage;

        int bodyCount = sMIMEMessages.size() + nonSMIMEParts.size();

        if (bodyCount > 1)
        {
            // there is more than one S/MIME part so we need to create a multipart message
            Multipart mp = new MimeMultipart();

            for (Part part : nonSMIMEParts)
            {
                MimeBodyPart bodyPart = BodyPartUtils.toMimeBodyPart(part);

                mp.addBodyPart(bodyPart);
            }

            for (MimeMessage message : sMIMEMessages)
            {
                MimeBodyPart bodyPart = BodyPartUtils.toRFC822(message, messageFilename);

                mp.addBodyPart(bodyPart);
            }

            newMessage = new MimeMessage(MailSession.getDefaultSession());

            newMessage.setContent(mp);
        }
        else {
            newMessage = sMIMEMessages.get(0);
        }

        copyNonContentHeaders(parent, newMessage);

        return newMessage;
    }

    public MimeMessage handlePart(Part source)
    throws SMIMEHandlerException
    {
        PartListener partListener = (parent, part, context) -> AttachedSMIMEHandler.this.onPart(parent, part);

        PartScanner partScanner = new PartScanner(partListener, 1);

        MimeMessage newMessage = null;

        try {
            partScanner.scanPart(source);

            // we will only return a message if there was a S/MIME message
            if (!sMIMEMessages.isEmpty()) {
                newMessage = buildMessage(source);
            }
        }
        catch (MessagingException | IOException | PartException e) {
            throw new SMIMEHandlerException(e);
        }

        return newMessage;
    }

    public String[] getSmimeFilenames() {
        return smimeFilenames;
    }

    public void setSmimeFilenames(String[] smimeFilenames) {
        this.smimeFilenames = smimeFilenames;
    }
}
