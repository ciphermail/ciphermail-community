/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Iterator that returned name value pairs.
 *
 * Class is not thread safe.
 *
 * @author Martijn Brinkers
 *
 */
public class NameValueIterator implements Iterator<NameValueIterator.Entry>
{
    // the regular expression used to detect name value pairs
    private static final Pattern nameValuePattern = Pattern.compile("\\s*(\\S*)\\s*=\\s*(\\S*)\\s*");

    private final Matcher matcher;

    private Entry cachedEntry;

    public static class Entry
    {
        private Entry(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        private final String name;
        private final String value;

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }

    public NameValueIterator(@Nonnull String input) {
        matcher = nameValuePattern.matcher(input);
    }

    @Override
    public boolean hasNext()
    {
        if (cachedEntry != null) {
            return true;
        }

        if (matcher.find())
        {
            String name = matcher.group(1);
            String value = matcher.group(2);

            cachedEntry = new Entry(name, value);

            return true;
        }

        return false;
    }

    @Override
    public Entry next()
    {
        if (!hasNext()) {
            throw new NoSuchElementException("No more entries");
        }
        else {
            Entry entry = cachedEntry;

            cachedEntry = null;

            return entry;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove is unsupported.");
    }
}
