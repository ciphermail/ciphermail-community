/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.MimeMessageWithID;
import com.ciphermail.core.common.mail.matcher.ContentHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.NotHeaderNameMatcher;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.activation.DataHandler;
import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;

/**
 * Creates a PGP/MIME signed message
 *
 * @author Martijn Brinkers
 *
 */
public class PGPMIMESignatureBuilder
{
    /*
     * The hash algorithm for the signature
     */
    private PGPHashAlgorithm hashAlgorithm = PGPHashAlgorithm.SHA256;

    /*
     * The signature type to use (Binary or text)
     */
    private PGPDocumentType signatureType = PGPDocumentType.TEXT;

    /*
     * If true, the message id of the source email will be retained
     */
    private boolean retainMessageID = true;

    /*
     * If true and the message contains 8bit parts, the message will be converted
     * to 7bit prior to signing.
     */
    private boolean convertTo7Bit = true;

    public MimeMessage sign(@Nonnull MimeMessage message, @Nonnull PGPPublicKey publicKey,
            @Nonnull PrivateKey privateKey)
    throws IOException, MessagingException, PGPException
    {
        // Any 8bit MIME parts must be converted to 7bit before signing
        if (convertTo7Bit && MailUtils.contains8BitMimePart(message))
        {
            message = retainMessageID ? MailUtils.cloneMessageWithFixedMessageID(message) :
                MailUtils.cloneMessage(message);

            MailUtils.convertTo7Bit(message);

            message.saveChanges();

            // For some reason we need to create a new MIME message from the converted one otherwise getting the raw
            // MIME content of the body (using MimeMessage#getRawInputStream) will not return the re-encoded body
            message = MailUtils.cloneMessageWithFixedMessageID(message);
        }

        ByteArrayOutputStream mimeBuffer = new ByteArrayOutputStream();

        BodyPartUtils.writeMIMEBodyPart(message, new ContentHeaderNameMatcher(), mimeBuffer);

        ByteArrayOutputStream canonicalizedBuffer = new ByteArrayOutputStream();

        // Make sure that all lines end with CR/LF and all trailing whitespace is removed
        // (see RFC 3156 section 5 "OpenPGP signed data")
        InputStream canonicalizer = new PGPMIMECanonicalizerInputStream(new ByteArrayInputStream(
                mimeBuffer.toByteArray()), "US-ASCII");

        IOUtils.copy(canonicalizer, canonicalizedBuffer);

        canonicalizer.close();

        byte[] canonicalizedMIME = canonicalizedBuffer.toByteArray();

        // We need to trim excessive new lines because Javamail will remove excessive empty lines after the last
        // boundary when encoding the message. This then results in a corrupt signature since the message that is
        // signed is different then the message which is sent.
        //
        // Another reason to remove lines is that Enigmail will report a bad signature if the message was signed with
        // more then one newline
        if (message.isMimeType("multipart/*")) {
            canonicalizedMIME = PGPUtils.trimRightExcessiveNewlines(canonicalizedMIME);
        }

        PGPSignatureBuilder signatureBuilder = new PGPSignatureBuilder();

        signatureBuilder.setPGPDocumentType(signatureType);
        signatureBuilder.setClearSign(false);

        ByteArrayOutputStream signature = new ByteArrayOutputStream();

        signatureBuilder.setHashAlgorithm(hashAlgorithm);
        signatureBuilder.sign(new ByteArrayInputStream(canonicalizedMIME), signature, publicKey, privateKey);

        MimeBodyPart bodyPart = new MimeBodyPart(new ByteArrayInputStream(canonicalizedMIME));

        MimeBodyPart signaturePart = new MimeBodyPart();

        signaturePart.setDataHandler(new DataHandler(new ByteArrayDataSource(signature.toByteArray(),
                "application/pgp-signature; name=\"signature.asc\"")));
        signaturePart.setDescription("OpenPGP digital signature");
        signaturePart.setDisposition("attachment; filename=\"signature.asc\"");

        Multipart mp = new MimeMultipart("signed; micalg=" + hashAlgorithm.getMicAlg() +
                "; protocol=\"application/pgp-signature\"");

        mp.addBodyPart(bodyPart);
        mp.addBodyPart(signaturePart);

        MimeMessage signedMessage = retainMessageID ? new MimeMessageWithID(MailSession.getDefaultSession(),
                message.getMessageID()) : new MimeMessage(MailSession.getDefaultSession());

        signedMessage.setContent(mp);

        HeaderMatcher contentMatcher = new ContentHeaderNameMatcher();

        // create a matcher that matches on everything expect content-*
        HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);

        // copy all non-content headers from source message to the new message
        HeaderUtils.copyHeaders(message, signedMessage, nonContentMatcher);

        signedMessage.saveChanges();

        return signedMessage;
    }

    public PGPHashAlgorithm getHashAlgorithm() {
        return hashAlgorithm;
    }

    public void setHashAlgorithm(PGPHashAlgorithm hashAlgorithm) {
        this.hashAlgorithm = hashAlgorithm;
    }

    public PGPDocumentType getSignatureType() {
        return signatureType;
    }

    public void setSignatureType(PGPDocumentType signatureType) {
        this.signatureType = signatureType;
    }

    public boolean isRetainMessageId() {
        return retainMessageID;
    }

    public void setRetainMessageId(boolean retainMessageID) {
        this.retainMessageID = retainMessageID;
    }

    public boolean isConvertTo7Bit() {
        return convertTo7Bit;
    }

    public void setConvertTo7Bit(boolean convertTo7Bit) {
        this.convertTo7Bit = convertTo7Bit;
    }
}
