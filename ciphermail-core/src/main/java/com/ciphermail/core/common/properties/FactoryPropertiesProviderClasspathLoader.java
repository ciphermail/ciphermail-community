/*
 * Copyright (c) 2012-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import org.apache.commons.lang.UnhandledException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * FactoryPropertiesProvider implementation that locates all the FactoryPropertiesProvider's from the classpath (only from the
 * provided packages) and loads all the Properties.
 *
 * @author Martijn Brinkers
 *
 */
public class FactoryPropertiesProviderClasspathLoader implements FactoryPropertiesProvider
{
    private static final Logger logger = LoggerFactory.getLogger(FactoryPropertiesProviderClasspathLoader.class);

    /*
     * The packages to scan
     */
    private final Set<String> packages = new HashSet<>();

    public FactoryPropertiesProviderClasspathLoader() {
        // Empty on purpose
    }

    public FactoryPropertiesProviderClasspathLoader(String... packages) {
        setPackages(packages);
    }

    private LinkedHashSet<Class<?>> findPropertiesProviders(String basePackage)
    throws IOException
    {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);

        LinkedHashSet<Class<?>> propertiesProviders = new LinkedHashSet<>();

        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                                   resolveBasePackage(basePackage) + "/**/*.class";

        Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);

        for (Resource resource : resources)
        {
            if (resource.isReadable())
            {
                try {
                    MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);

                    if (isPropertiesProviderImpl(metadataReader)) {
                        propertiesProviders.add(Class.forName(metadataReader.getClassMetadata().getClassName()));
                    }
                }
                catch (Exception e) {
                    logger.error("Error getting class", e);
                }
            }
        }

        return propertiesProviders;
    }

    private String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage));
    }

    private boolean isPropertiesProviderImpl(MetadataReader metadataReader)
    {
        try {
            ClassMetadata classMetadata = metadataReader.getClassMetadata();

            if (!classMetadata.isConcrete()) {
                // Only concrete implementations can be used
                return false;
            }

            Class<?> c = Class.forName(classMetadata.getClassName());

            // Only accept if it is a FactoryPropertiesProvider and if the FactoryPropertiesProviderAutoLoad annotation is present
            if (FactoryPropertiesProvider.class.isAssignableFrom(c) &&
                c.getAnnotation(FactoryPropertiesProviderAutoLoad.class) != null)
            {
                return true;
            }
        }
        catch(Exception e) {
            logger.error("Error checking whether class is-a FactoryPropertiesProvider", e);
        }

        return false;
    }

    private List<Properties> loadProperties(Class<?> clazz)
    {
        List<Properties> properties = null;

        try {
            logger.info("Found FactoryPropertiesProvider {}. Instantiating class.", clazz.getName());

            properties = ((FactoryPropertiesProvider) clazz.getDeclaredConstructor().newInstance()).getProperties();
        }
        catch (UnhandledException e) {
            throw e;
        }
        catch (Exception e) {
            logger.error("Error creating instance of " + clazz.getName(), e);
        }

        if (properties == null) {
            properties = Collections.emptyList();
        }

        return properties;
    }

    @Override
    public List<Properties> getProperties()
    {
        List<Properties> properties = new LinkedList<>();

        for (String basePackage : packages)
        {
            try {
                LinkedHashSet<Class<?>> classes = findPropertiesProviders(basePackage);

                for (Class<?> clazz : classes) {
                    properties.addAll(loadProperties(clazz));
                }
            }
            catch (IOException e) {
                logger.error("Error loading packages from " + basePackage, e);
            }
        }

        return properties;
    }

    public void setPackages(String... packages)
    {
        if (packages != null) {
            this.packages.addAll(Arrays.asList(packages));
        }
    }

    public void addPackage(String packageName)
    {
        if (packageName != null) {
            packages.add(packageName);
        }
    }
}
