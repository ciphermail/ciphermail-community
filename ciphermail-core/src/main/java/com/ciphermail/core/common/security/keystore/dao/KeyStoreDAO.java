/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore.dao;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.CertificateUserType;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.security.certificate.CertificateInspector;
import com.ciphermail.core.common.security.keystore.hibernate.KeyStoreEntity;
import com.ciphermail.core.common.util.CloseableIterator;
import org.hibernate.ScrollMode;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.List;
import java.util.Objects;

public class KeyStoreDAO extends GenericHibernateDAO
{
    /*
     * The name of the store. If null, the null store name will be used
     */
    private final String storeName;

    /*
     * Name of the Hibernate entity
     */
    private static final String ENTITY_NAME = KeyStoreEntity.ENTITY_NAME;

    /**
     * Creates a DAO that access the key store.
     */
    public KeyStoreDAO(@Nonnull String storeName, @Nonnull Session session) {
        this(storeName, SessionAdapterFactory.create(session));
    }

    private KeyStoreDAO(@Nonnull String storeName, @Nonnull SessionAdapter sessionAdapter)
    {
        super(sessionAdapter);

        this.storeName = Objects.requireNonNull(storeName);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static KeyStoreDAO getInstance(@Nonnull String storeName, @Nonnull Session session) {
        return new KeyStoreDAO(storeName, session);
    }

    public String getStoreName() {
        return storeName;
    }

    public List<String> getAliases()
    {
        String hql = "select k.alias from " + ENTITY_NAME + " k where storeName = :storeName";

        Query<String> query = createQuery(hql, String.class);

        query.setParameter("storeName", storeName);

        return query.list();
    }

    public KeyStoreEntity getEntryByAlias(@Nonnull String alias)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<KeyStoreEntity> criteriaQuery = criteriaBuilder.createQuery(KeyStoreEntity.class);

        Root<KeyStoreEntity> rootEntity = criteriaQuery.from(KeyStoreEntity.class);

        criteriaQuery.where(
                criteriaBuilder.equal(rootEntity.get(KeyStoreEntity.ALIAS_COLUMN), alias),
                criteriaBuilder.equal(rootEntity.get(KeyStoreEntity.STORE_NAME_COLUMN), storeName));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }

    public KeyStoreEntity getEntryByCertificate(@Nonnull Certificate certificate)
    throws CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<KeyStoreEntity> criteriaQuery = criteriaBuilder.createQuery(KeyStoreEntity.class);

        Root<KeyStoreEntity> rootEntity = criteriaQuery.from(KeyStoreEntity.class);

        Path<CertificateUserType> certificateRoot = rootEntity.get(KeyStoreEntity.CERTIFICATE_USER_TYPE_NAME);

        CertificateInspector inspector = new CertificateInspector(certificate);

        criteriaQuery.where(
                criteriaBuilder.equal(rootEntity.get(KeyStoreEntity.STORE_NAME_COLUMN), storeName),
                criteriaBuilder.equal(certificateRoot.get(KeyStoreEntity.THUMBPRINT_COLUMN),
                        inspector.getThumbprint()));

        Query<KeyStoreEntity> query = createQuery(criteriaQuery);

        query.setMaxResults(1);

        List<KeyStoreEntity> result = query.getResultList();

        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    public CloseableIterator<KeyStoreEntity> getEntryIterator()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<KeyStoreEntity> criteriaQuery = criteriaBuilder.createQuery(KeyStoreEntity.class);

        Root<KeyStoreEntity> rootEntity = criteriaQuery.from(KeyStoreEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(KeyStoreEntity.STORE_NAME_COLUMN), storeName));

        return new KeyStoreEntryIterator(createQuery(criteriaQuery).scroll(ScrollMode.FORWARD_ONLY));
    }

    /*
     * Returns ALL entries irrespective of the store name. Only use this if you really need all keys.
     */
    public static CloseableIterator<KeyStoreEntity> getAllEntriesIterator(@Nonnull SessionAdapter session)
    {
        KeyStoreDAO dao = new KeyStoreDAO("ignored", session);

        CriteriaBuilder criteriaBuilder = dao.getCriteriaBuilder();

        CriteriaQuery<KeyStoreEntity> criteriaQuery = criteriaBuilder.createQuery(KeyStoreEntity.class);

        criteriaQuery.from(KeyStoreEntity.class);

        return new KeyStoreEntryIterator(dao.createQuery(criteriaQuery).scroll(ScrollMode.FORWARD_ONLY));
    }

    public long getEntryCount()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<KeyStoreEntity> rootEntity = criteriaQuery.from(KeyStoreEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(KeyStoreEntity.STORE_NAME_COLUMN), storeName));

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }
}
