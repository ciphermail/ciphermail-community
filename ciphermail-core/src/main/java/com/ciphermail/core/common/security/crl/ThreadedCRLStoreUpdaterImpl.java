/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.util.ThreadUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Service that regularly updates the CRL store (find the CRL dist. points and downloads the certificates).
 *
 * @author Martijn Brinkers
 *
 */
public class ThreadedCRLStoreUpdaterImpl implements ThreadedCRLStoreUpdater
{
    private static final Logger logger = LoggerFactory.getLogger(ThreadedCRLStoreUpdaterImpl.class);

    /*
     * The name of the thread
     */
    private static final String THREAD_NAME = "CRL Updater thread";

    /*
     * The service that is responsible for downloading and updating the CRL store
     */
    private final CRLStoreUpdater storeUpdater;

    /*
     * Time between consecutive updates in milliseconds.
     */
    private final long updateInterval;

    /*
     * The thread that updates the CRL store
     */
    private Thread updateThread;

    /*
     * The runnable which is ran by the updateThread.
     */
    private Updater updater;

    /*
     * The runnable responsible for starting the CRL update process
     */
    private class Updater implements Runnable
    {
        /*
         * for waking up the thread
         */
        private final Object wakeupToken = new Object();

        @Override
        public void run()
        {
            // Wait some time to start thread. Add some random time to make it less likely that other
            // background threads start at the same time
            ThreadUtils.sleepQuietly(DateUtils.MILLIS_PER_SECOND * (60 + RandomUtils.nextInt(30)));

            logger.info("Starting " + THREAD_NAME);

            do {
                try {
                    try {
                        storeUpdater.update();
                    }
                    catch (CRLStoreException e) {
                        throw new UnhandledException(e);
                    }
                }
                catch(Throwable e) {
                    logger.error("Error updating the CRL store.", e);
                }

                try {
                    synchronized (wakeupToken) {
                        wakeupToken.wait(updateInterval);
                    }
                }
                catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            while(true);
        }

        public void wakeup()
        {
            synchronized (wakeupToken) {
                wakeupToken.notifyAll();
            }
        }
    }

    public ThreadedCRLStoreUpdaterImpl(@Nonnull CRLStoreUpdater storeUpdater, long updateInterval)
    {
        this.storeUpdater = Objects.requireNonNull(storeUpdater);
        this.updateInterval = updateInterval;
    }

    @Override
    public synchronized void start()
    {
        if (updateThread == null)
        {
            updater = new Updater();

            updateThread = new Thread(updater, THREAD_NAME);

            updateThread.setDaemon(true);
            updateThread.start();
        }
    }

    @Override
    public void update() {
        updater.wakeup();
    }
}