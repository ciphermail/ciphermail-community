/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.trustlist;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyInspector;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.List;
import java.util.Objects;

public class PGPTrustListImpl implements PGPTrustList
{
    private static final Logger logger = LoggerFactory.getLogger(PGPTrustListImpl.class);

    protected static final String NOT_LISTED_MESSAGE = "Public key not found in trust list.";
    protected static final String UNTRUSTED_MESSAGE  = "Public key is not trusted.";
    protected static final String TRUSTED_MESSAGE    = "Public key is trusted.";

    /*
     * The name of this trust list
     */
    private final String name;

    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;

    /*
     * For adding/removing sub keys
     */
    private final PGPKeyRing keyRing;

    public PGPTrustListImpl(@Nonnull String name, @Nonnull SessionManager sessionManager, @Nonnull PGPKeyRing keyRing)
    {
        this.name = Objects.requireNonNull(name);
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.keyRing = Objects.requireNonNull(keyRing);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<? extends PGPTrustListEntry> getEntries(Integer firstResult, Integer maxResults)
    throws PGPTrustListException
    {
        return getDAO().getPGPTrustListEntities(firstResult, maxResults);
    }

    @Override
    public long size()
    throws PGPTrustListException
    {
        return getDAO().size();
    }

    @Override
    public void deleteAll()
    throws PGPTrustListException
    {
        getDAO().delete();
    }

    @Override
    public PGPTrustListEntry getEntry(@Nonnull String sha256Fingerprint)
    throws PGPTrustListException
    {
        return getDAO().getPGPTrustListEntity(sha256Fingerprint);
    }

    @Override
    public PGPTrustListEntry getEntry(@Nonnull PGPPublicKey publicKey)
    throws PGPTrustListException
    {
        try {
            return getEntry(PGPPublicKeyInspector.getSHA256FingerprintHex(publicKey));
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException | IOException e) {
            throw new PGPTrustListException(e);
        }
    }

    @Override
    public PGPTrustListEntry createEntry(@Nonnull String sha256Fingerprint)
    throws PGPTrustListException
    {
        PGPTrustListEntity entry = new PGPTrustListEntity(sha256Fingerprint);

        entry.setName(name);

        return entry;
    }

    @Override
    public PGPTrustListEntry createEntry(@Nonnull PGPPublicKey publicKey)
    throws PGPTrustListException
    {
        try {
            return createEntry(PGPPublicKeyInspector.getSHA256FingerprintHex(publicKey));
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException | IOException e) {
            throw new PGPTrustListException(e);
        }
    }

    @Override
    public void setEntry(@Nonnull PGPTrustListEntry entry, boolean includeSubkeys)
    throws PGPTrustListException
    {
        if (!(entry instanceof PGPTrustListEntity newEntity)) {
            throw new PGPTrustListException("This PGPTrustList only accepts PGPTrustListEntity's.");
        }

        if (StringUtils.isEmpty(entry.getSHA256Fingerprint())) {
            throw new IllegalArgumentException("Fingerprint should not be empty");
        }

        newEntity.setName(name);

        PGPTrustListDAO dao = getDAO();

        // If there is already an entry with the same fingerprint, delete it first
        PGPTrustListEntity existingEntry = dao.getPGPTrustListEntity(newEntity.getSHA256Fingerprint());

        if (existingEntry != null)
        {
            dao.delete(existingEntry);

            // We need to flush to make sure that the deletion is executed before making the new
            // entry persistent. If not we will get a Constraint Violation since Hibernate will
            // execute it in a different order.
            dao.flush();
        }

        dao.persist(newEntity);

        if (includeSubkeys) {
            // Check if there is a key on the key ring for this entry. If so add all sub keys
            PGPKeyRingEntry keyEntry;

            try {
                keyEntry = keyRing.getBySha256Fingerprint(newEntity.getSHA256Fingerprint());
            }
            catch (PGPException | IOException e) {
                throw new PGPTrustListException(e);
            }

            if (keyEntry != null && keyEntry.isMasterKey())
            {
                for (PGPKeyRingEntry subKeyEntry : keyEntry.getSubkeys()) {
                    setEntry(newEntity.clone(subKeyEntry.getSHA256Fingerprint()), false);
                }
            }
        }
    }

    @Override
    public void deleteEntry(@Nonnull PGPTrustListEntry entry, boolean includeSubkeys)
    throws PGPTrustListException
    {
        if (!(entry instanceof PGPTrustListEntity entity)) {
            throw new PGPTrustListException("This PGPTrustList only accepts PGPTrustListEntity's.");
        }

        getDAO().delete(entity);

        if (includeSubkeys) {
            // Check if there is a key on the key ring for this entry. If so delete all sub keys
            PGPKeyRingEntry keyEntry;

            try {
                keyEntry = keyRing.getBySha256Fingerprint(entity.getSHA256Fingerprint());
            }
            catch (PGPException | IOException e) {
                throw new PGPTrustListException(e);
            }

            if (keyEntry != null && keyEntry.isMasterKey())
            {
                for (PGPKeyRingEntry subKeyEntry : keyEntry.getSubkeys())
                {
                    PGPTrustListEntry subTrustEntry = getEntry(subKeyEntry.getSHA256Fingerprint());

                    if (subTrustEntry != null) {
                        deleteEntry(subTrustEntry, false);
                    }
                }
            }
        }
    }

    @Override
    public PGPTrustListValidityResult checkValidity(@Nonnull PGPPublicKey publicKey)
    throws PGPTrustListException
    {
        PGPTrustListEntry entry = getEntry(publicKey);

        if (entry == null)
        {
            logger.debug(NOT_LISTED_MESSAGE);

            return new PGPTrustListValidityResultImpl(PGPTrustListValidity.NOT_LISTED, NOT_LISTED_MESSAGE);
        }

        PGPTrustListStatus status = entry.getStatus();

        if (PGPTrustListStatus.UNTRUSTED == status)
        {
            logger.debug(UNTRUSTED_MESSAGE);

            return new PGPTrustListValidityResultImpl(PGPTrustListValidity.INVALID, UNTRUSTED_MESSAGE);
        }
        else if (PGPTrustListStatus.TRUSTED == status)
        {
            logger.debug(TRUSTED_MESSAGE);

            return new PGPTrustListValidityResultImpl(PGPTrustListValidity.VALID, TRUSTED_MESSAGE);
        }

        throw new IllegalStateException("Unknown PGPTrustListStatus: " + status);
    }

    private PGPTrustListDAO getDAO() {
        return PGPTrustListDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), name);
    }
}
