/*
 * Copyright (c) 2008-2018, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.google.common.net.InetAddresses;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The AddressUtils class provides utility methods for working with IP addresses and networks.
 */
public class AddressUtils
{
    private static final Pattern NETWORK_PATTERN = Pattern.compile("^\\s*(?<literalstart>\\[)?(?<address>[\\p{XDigit}.:]+)(?<literalend>])?(/(?<cidr>\\d+))?\\s*$");

    private AddressUtils() {
        // empty on purpose
    }

    /**
     * Returns true if the address is a valid IP address.
     */
    public static boolean isValidIPv4Address(String address)
    {
        if (address == null) {
            return false;
        }

        boolean valid = false;

        try {
            valid = InetAddresses.forString(address) instanceof Inet4Address;
        }
        catch (IllegalArgumentException e) {
            // ignore
        }

        return valid;
    }

    /**
     * Returns true if the address is a valid IPv6 address.
     *
     * @param address the IPv6 address to check
     * @param requireLiteral if true, the IP address should be a literal address, i.e., enclosed in square brackets
     * @return true if the IP address is a valid IPv6 address
     */
    public static boolean isValidIPv6Address(String address, boolean requireLiteral)
    {
        if (address == null) {
            return false;
        }

        if (requireLiteral)
        {
            if (!address.startsWith("[") || !address.endsWith("]")) {
                return false;
            }

            address = StringUtils.substring(address, 1, -1);
        }

        boolean valid = false;

        try {
            valid = InetAddresses.forString(address) instanceof Inet6Address;
        }
        catch (IllegalArgumentException e) {
            // ignore
        }

        return valid;
    }

    /**
     * Returns true if the address is a valid IPv6 address.
     *
     * @param address the IPv6 address to check
     * @return true if the IP address is a valid IPv6 address
     */
    public static boolean isValidIPv6Address(String address) {
        return isValidIPv6Address(address, false);
    }

    /**
     * Returns true if address is a valid IPv4 or IPv6 address
     */
    public static boolean isValidIPAddress(String address) {
        return isValidIPv4Address(address) || isValidIPv6Address(address);
    }

    /**
     * Returns true if the address is a valid IPv4 or IPv6 network with optional CIDR netmask.
     * Examples:
     * - 192.168.0.0/32
     * - 127.0.0.1
     * <p>
     * if literal is true
     * - [::1]
     *
     * @param network the  IPv4 or IPv6 network
     * @param ipv6RequireLiteral if true, the IPv6 address should be a literal address, i.e., enclosed in square brackets
     * @return true if the IP address is a valid IPv6 address
     */
    public static boolean isValidNetwork(String network, boolean ipv6RequireLiteral)
    {
        if (network == null) {
            return false;
        }

        boolean valid = false;

        Matcher matcher = NETWORK_PATTERN.matcher(network);

        if (!matcher.matches()) {
            return false;
        }

        String address = matcher.group("address");
        String cidr = matcher.group("cidr");
        String literalStart = StringUtils.defaultString(matcher.group("literalstart"));
        String literalEnd = StringUtils.defaultString(matcher.group("literalend"));

        if (StringUtils.isEmpty(literalStart) != StringUtils.isEmpty(literalEnd)) {
            return false;
        }

        if (isValidIPv4Address(address))
        {
            // ipv4 should not be enclosed in square brackets
            if (literalStart.equals("[")) {
                return false;
            }

            valid = true;
        }
        else {
            if (ipv6RequireLiteral && !literalStart.equals("[")) {
                return false;
            }

            if (isValidIPv6Address(address)) {
                valid = true;
            }
        }


        if (valid && StringUtils.isNotEmpty(cidr))
        {
            int bits = NumberUtils.toInt(cidr, -1);

            valid = bits >= 0;
        }

        return valid;
    }

    /**
     * Returns true if the address is a valid IPv4 or IPv6 network with optional CIDR netmask.
     * Examples:
     * 1) 192.168.0.0/32
     * 2) 127.0.0.1
     *
     * @param network the  IPv4 or IPv6 network
     * @return true if the IP address is a valid IPv6 address
     */
    public static boolean isValidNetwork(String network) {
        return isValidNetwork(network, false);
    }

    /**
     * Returns true if the provided address is a local IP address
     */
    public static boolean isLocalIpAddress(InetAddress addr)
    {
        if (addr.isAnyLocalAddress() || addr.isLoopbackAddress()) {
            return true;
        }

        try {
            return NetworkInterface.getByInetAddress(addr) != null;
        }
        catch (SocketException e) {
            return false;
        }
    }

    /**
     * Parses the IPv6 address and returns the 64bit network prefix
     */
    public static String getIPv6NetworkPrefix(String ipv6)
    throws UnknownHostException
    {
        InetAddress address = InetAddresses.forString(Objects.requireNonNull(ipv6, "ipv6 is null"));

        if (!(address instanceof Inet6Address inet6Address)) {
            throw new IllegalArgumentException(String.format("%s is not an IPv6 address", ipv6));
        }

        return getIPv6NetworkPrefix(inet6Address).getHostAddress();
    }

    /**
     * Parses the IPv6 address and returns the 64bit network prefix
     */
    public static Inet6Address getIPv6NetworkPrefix(Inet6Address inet6Address)
    throws UnknownHostException
    {
        byte[] addressBytes = inet6Address.getAddress();

        for (int i = 8; i < addressBytes.length; i++) {
            addressBytes[i] = 0;
        }

        return (Inet6Address) InetAddress.getByAddress(addressBytes);
    }
}
