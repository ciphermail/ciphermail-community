/*
 * Copyright (c) 2012-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import javax.annotation.Nonnull;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimePart;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Objects;

/**
 * Helper class to extract headers from MimeMessage, Part's etc.
 *
 * MimeMessage, Part, MimePart etc. not all support the same methods to extract headers. The HeaderExtractor
 * provides a common header extract interface to the underlying Part extensions. For example MimeMessage (inherited
 * from MimePart) supports the method getAllHeaderLines but Part does not.
 *
 * @author Martijn Brinkers
 *
 */
public class HeaderExtractor
{
    /*
     * The part to extract the headers from
     */
    private final Part part;

    public HeaderExtractor(@Nonnull Part part) {
        this.part = Objects.requireNonNull(part);
    }

    /**
     * Returns all the Headers's.
     */
    public Enumeration<Header> getAllHeaders()
    throws MessagingException
    {
        return getAllHeaders(part);
    }

    /**
     * Returns all the Headers's.
     */
    @SuppressWarnings("unchecked")
    public static Enumeration<Header> getAllHeaders(Part part)
    throws MessagingException
    {
        return part.getAllHeaders();
    }

    /**
     * Returns all the raw headers strings
     */
    public Enumeration<String> getAllHeaderLines()
    throws MessagingException
    {
        return getAllHeaderLines(part);
    }

    /**
     * Returns all the raw headers strings
     */
    @SuppressWarnings("unchecked")
    public static Enumeration<String> getAllHeaderLines(Part part)
    throws MessagingException
    {
        Enumeration<String> headerEnum = null;

        if (part instanceof MimePart mimePart) {
            headerEnum = mimePart.getAllHeaderLines();
        }
        else {
            headerEnum = Collections.enumeration(new ArrayList<String>());
        }

        return headerEnum;
    }

    /**
     * Returns all the raw headers strings
     */
    @SuppressWarnings("unchecked")
    public static Enumeration<String> getAllHeaderLines(InternetHeaders headers) {
        return headers.getAllHeaderLines();
    }
}
