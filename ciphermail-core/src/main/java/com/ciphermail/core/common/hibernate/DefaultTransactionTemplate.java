package com.ciphermail.core.common.hibernate;

import com.ciphermail.core.common.util.RequiredByJames;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Extension of TransactionTemplate. The only purpose of the extension is adding the @{{@link RequiredByJames}} annotation
 */
@RequiredByJames(baseClass = TransactionOperations.class)
public class DefaultTransactionTemplate extends TransactionTemplate
{
    public DefaultTransactionTemplate() {
    }

    public DefaultTransactionTemplate(PlatformTransactionManager transactionManager) {
        super(transactionManager);
    }

    public DefaultTransactionTemplate(PlatformTransactionManager transactionManager,
            TransactionDefinition transactionDefinition)
    {
        super(transactionManager, transactionDefinition);
    }
}
