/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.password.PasswordProvider;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPPrivateKey;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * PGPKeyRingEntry implementation which uses a KeyStore to provide access to the private key.
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyRingEntryImpl implements PGPKeyRingEntry
{
    /*
     * The database entity
     */
    private final PGPKeyRingEntity entity;

    /*
     * The store which contains the private keys
     */
    private final KeyStore privateKeyStore;

    /*
     * Provides the password for the key entries
     */
    private final PasswordProvider passwordProvider;

    /*
     * Used for updating the email addresses associated with a public key
     */
    private final PGPKeyRingEntryEmailUpdater keyRingEntryEmailUpdater;

    public PGPKeyRingEntryImpl(@Nonnull PGPKeyRingEntity entity, KeyStore privateKeyStore,
            PasswordProvider passwordProvider, @Nonnull PGPKeyRingEntryEmailUpdater keyRingEntryEmailUpdater)
    {
        if (privateKeyStore != null) {
            Objects.requireNonNull(passwordProvider, "passwordProvider");
        }

        this.entity = Objects.requireNonNull(entity);
        this.privateKeyStore = privateKeyStore;
        this.passwordProvider = passwordProvider;
        this.keyRingEntryEmailUpdater = Objects.requireNonNull(keyRingEntryEmailUpdater);
    }

    @Override
    public UUID getID() {
        return entity.getID();
    }

    @Override
    public Long getKeyID() {
        return entity.getKeyID();
    }


    @Override
    public Date getCreationDate() {
        return entity.getCreationDate();
    }

    @Override
    public Date getExpirationDate() {
        return entity.getExpirationDate();
    }

    @Override
    public Date getInsertionDate() {
        return entity.getInsertionDate();
    }

    @Override
    public void setPublicKey(@Nonnull PGPPublicKey publicKey)
    throws PGPException, IOException
    {
        entity.setPGPPublicKey(publicKey);

        keyRingEntryEmailUpdater.updateEmail(this, null /* do not add new email addresses */);
    }

    @Override
    public PGPPublicKey getPublicKey()
    throws PGPException, IOException
    {
        return PGPKeyUtils.decodePublicKey(entity.getEncodedPublicKey());
    }

    @Override
    public PGPPrivateKey getPrivateKey()
    throws PGPException, IOException
    {
        PGPPrivateKey result = null;

        if (entity.getPrivateKeyAlias() != null && privateKeyStore != null)
        {
            try {
                PrivateKey privateKey = (PrivateKey) privateKeyStore.getKey(entity.getPrivateKeyAlias(),
                        passwordProvider.getPassword());

                if (privateKey != null) {
                    result = new JcaPGPPrivateKey(getPublicKey(), privateKey);
                }
            }
            catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
                throw new PGPException("Error getting private key", e);
            }
        }

        return result;
    }

    @Override
    public String getPrivateKeyAlias() {
        return entity.getPrivateKeyAlias();
    }

    @Override
    public String getFingerprint() {
        return entity.getFingerprint();
    }

    @Override
    public String getSHA256Fingerprint() {
        return entity.getSHA256Fingerprint();
    }

    @Override
    public Set<String> getUserIDs() {
        return new HashSet<>(entity.getUserIDs());
    }

    @Override
    public Set<String> getEmail() {
        return new HashSet<>(entity.getEmail());
    }

    @Override
    public void setEmail(Set<String> email)
    {
        if (email == null) {
            email = Collections.emptySet();
        }

        entity.getEmail().clear();
        entity.getEmail().addAll(email);
    }

    @Override
    public boolean isMasterKey() {
        return entity.isMasterKey();
    }

    @Override
    public PGPKeyRingEntry getParentKey() {
        return entity.getParentKey() != null ? createPGPKeyRingEntry(entity.getParentKey()) : null;
    }

    @Override
    public Set<PGPKeyRingEntry> getSubkeys()
    {
        Set<PGPKeyRingEntry> subEntries = new LinkedHashSet<>();

        Set<PGPKeyRingEntity> subEntities = entity.getSubkeys();

        if (subEntities != null)
        {
            for (PGPKeyRingEntity subEntity : subEntities) {
                subEntries.add(createPGPKeyRingEntry(subEntity));
            }
        }

        return subEntries;
    }

    @Override
    public String getKeyRingName() {
        return entity.getKeyRingName();
    }

    private PGPKeyRingEntry createPGPKeyRingEntry(@Nonnull PGPKeyRingEntity entity) {
        return new PGPKeyRingEntryImpl(entity, privateKeyStore, passwordProvider, keyRingEntryEmailUpdater);
    }

    public PGPKeyRingEntity getEntity() {
        return entity;
    }

    public KeyStore getPrivateKeyStore() {
        return privateKeyStore;
    }

    public PasswordProvider getPasswordProvider() {
        return passwordProvider;
    }

    @Override
    public int hashCode() {
        return entity.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) { return false; }
        if (obj == this) { return true; }

        if (!(obj instanceof PGPKeyRingEntryImpl rhs)) {
            return false;
        }

        return entity.equals(rhs.entity);
    }
}
