/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.MissingKeyAlias;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.UUID;

public interface PGPKeyRing
{
    /**
     * The key alias of keys from the PGPKeyRing will be prefixed with this string
     */
    String KEY_ALIAS_PREFIX = "PGP:";

    /**
     * Returns the PGPKeyRingEntry's with the given id.
     */
    PGPKeyRingEntry getByID(@Nonnull UUID id)
    throws PGPException, IOException;

    /**
     * Returns the PGPKeyRingEntry's with the given sha256 fingerprint.
     */
    PGPKeyRingEntry getBySha256Fingerprint(@Nonnull String fingerprint)
    throws PGPException, IOException;

    /**
     * Adds the PGP public key to the keyring. Returns the PGPKeyRingEntry that has been added or null if nothing
     * has been added
     */
    PGPKeyRingEntry addPGPPublicKey(@Nonnull PGPPublicKey publicKey)
    throws PGPException, IOException;

    /**
     * Adds the keypair to the keyring. Returns the PGPKeyRingEntry that has been added or null if nothing
     * has been added.
     */
    PGPKeyRingEntry addKeyPair(@Nonnull PGPKeyRingPair keyPair)
    throws PGPException, IOException;

    /**
     * Returns the PGPKeyRingEntry's with the given key id. Normally there should only be one result.
     */
    CloseableIterator<PGPKeyRingEntry> getByKeyID(long keyID)
    throws PGPException, IOException;

    /**
     * Returns the PGPKeyRingEntry's with the given key id. Normally there should only be one result.
     */
    public CloseableIterator<PGPKeyRingEntry> getByKeyID(long keyID, MissingKeyAlias missingKeyAlias)
    throws PGPException, IOException;

    /**
     * Returns PGPKeyRingEntry's with the provided parent key id. Multiple "subkeys" can be returned.
     */
    CloseableIterator<PGPKeyRingEntry> getByParentKeyID(long parentKeyID, Integer firstResult,
            Integer maxResults)
    throws PGPException, IOException;

    /**
     * Returns the number PGPKeyRingEntry's which will be returned by a call to getByParentKeyID
     */
    long getByParentKeyIDCount(long parentKeyID)
    throws PGPException, IOException;

    /**
     * Returns the PGPKeyRingEntry's that match the search parameters
     */
    CloseableIterator<PGPKeyRingEntry> search(PGPSearchField searchField, String searchValue,
            PGPSearchParameters searchParameters, Integer firstResult, Integer maxResults)
    throws PGPException, IOException;

    /**
     * Returns the number of PGPKeyRingEntry's which will be returned by search matching the filters
     */
    long searchCount(PGPSearchField searchField, String searchValue, PGPSearchParameters searchParameters)
    throws PGPException, IOException;

    /**
     * Returns the PGPKeyRingEntry's that match the filters
     */
    CloseableIterator<PGPKeyRingEntry> getIterator(PGPSearchParameters searchParameters, Integer firstResult,
            Integer maxResults)
    throws PGPException, IOException;

    /**
     * Returns the number of PGPKeyRingEntry's which will be returned by getIterator matching the filters
     */
    long getCount(PGPSearchParameters searchParameters)
    throws PGPException, IOException;

    /**
     * Removes the key with the id from the keyring. If a key is a master key, all subkeys will be deleted as well. If
     * an entry with the provided ID does not exist, nothing happens.
     *
     * Note: the id is *not* the keyID but the unique ID of the entry.
     *
     */
    void delete(UUID id)
    throws PGPException, IOException;

    /**
     * Removes all the keys from the keyring
     */
    void deleteAll()
    throws IOException;

    /**
     * Returns the total number of keys in the keyring
     */
    long getSize()
    throws IOException;

    /**
     * Registers a PGPKeyRingEventListener which is called when keys are added, deleted etc.
     */
    void addPGPKeyRingEventListener(PGPKeyRingEventListener listener);
}
