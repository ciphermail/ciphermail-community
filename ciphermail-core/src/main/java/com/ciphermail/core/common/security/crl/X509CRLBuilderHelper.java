/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.HexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class X509CRLBuilderHelper
{
    private static final Logger logger = LoggerFactory.getLogger(X509CRLBuilderHelper.class);

    /*
     * Used for storage and retrieval of CRLs
     */
    private final X509CRLStoreExt crlStore;

    /*
     * Used to build the CRL
     */
    private final X509CRLBuilder crlBuilder;

    /**
     * Creates a new X509CRLBuilderHelper. The provided X509CRLBuilder instance should
     * already be populated with the relevant settings.
     */
    public X509CRLBuilderHelper(@Nonnull X509CRLStoreExt crlStore, @Nonnull X509CRLBuilder crlBuilder)
    {
        this.crlStore = Objects.requireNonNull(crlStore);
        this.crlBuilder = Objects.requireNonNull(crlBuilder);
    }

    /**
     * Adds the serial numbers to the revocation list. The serial number must be in hex form.
     */
    public void addEntries(Collection<String> revokedSerials)
    throws CRLException
    {
        if (revokedSerials != null)
        {
            Date revocationDate = new Date();

            for (String serialHex : revokedSerials)
            {
                if (serialHex == null) {
                    continue;
                }

                if (!HexUtils.isHex(serialHex)) {
                    throw new CRLException(serialHex + " is not a hex number.");
                }

                BigInteger serial = BigIntegerUtils.hexDecode(serialHex.trim());

                crlBuilder.addCRLEntry(serial, revocationDate, 0);
            }
        }
    }

    public @Nonnull X509CRL generateCRL(@Nonnull KeyAndCertificate issuer, boolean updateExistingCRL)
    throws CRLException
    {
        try {
            if (updateExistingCRL)
            {
                CRLLocator locator = new CRLLocator(crlStore);

                List<X509CRL> existingCRLs = locator.findCRLs(issuer.getCertificate());

                for (X509CRL existingCRL : existingCRLs)
                {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Adding existing CRL: {}", existingCRL);
                    }

                    crlBuilder.addCRL(existingCRL);
                }
            }

            return crlBuilder.generateCRL(issuer);
        }
        catch (NoSuchProviderException e) {
            throw new CRLException(e);
        }
    }
}
