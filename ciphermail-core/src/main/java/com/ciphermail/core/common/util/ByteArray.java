/*
 * Copyright (c) 2014-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Wraps a byte array to provide hashCode and equals functionality. This allows a byte[] to be used in a Set or Map.
 *
 * @author Martijn Brinkers
 *
 */
public class ByteArray
{
    private final byte[] bytes;

    public ByteArray(@Nonnull byte[] bytes) {
        this.bytes = Objects.requireNonNull(bytes);
    }

    /**
     * Returns the wrapped array
     */
    @Nonnull public byte[] getBytes() {
        return bytes;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(bytes).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) { return false; }
        if (obj == this) { return true; }

        if (obj.getClass() != getClass()) {
            return false;
        }

        ByteArray rhs = (ByteArray) obj;

        return new EqualsBuilder().append(bytes, rhs.bytes).isEquals();
    }

    /**
     * Return the bytes as a base64 encoded string. This is needed for example when used as a bean in combination with
     * BeanUtils
     */
    @Override
    public String toString() {
        return Base64Utils.encode(bytes);
    }

    /**
     * Return the underlying byte array or null if byteArray is null
     */
    public static byte[] getBytes(ByteArray byteArray) {
        return byteArray != null ? byteArray.getBytes() : null;
    }

    /**
     * Returns the collection of bytes arrays as a List of ByteArray's
     */
    public static List<ByteArray> toList(Collection<byte[]> arrays)
    {
        List<ByteArray> wrappers = new LinkedList<>();

        if (CollectionUtils.isNotEmpty(arrays))
        {
            for (byte[] array : arrays) {
                wrappers.add(new ByteArray(array));
            }
        }

        return wrappers;
    }

    /**
     * Returns the collection of bytes arrays as a set of ByteArray's
     */
    public static Set<ByteArray> toSet(Collection<byte[]> bytes)
    {
        Set<ByteArray> wrappers = new LinkedHashSet<>();

        if (CollectionUtils.isNotEmpty(bytes))
        {
            for (byte[] array : bytes) {
                wrappers.add(new ByteArray(array));
            }
        }

        return wrappers;
    }

    /**
     * Returns the collection of ByteArray's as a List of byte arrays
     */
    public static List<byte[]> toArrayList(Collection<ByteArray> bytes)
    {
        List<byte[]> arrays = new LinkedList<>();

        if (CollectionUtils.isNotEmpty(bytes))
        {
            for (ByteArray wrapper : bytes) {
                arrays.add(wrapper.getBytes());
            }
        }

        return arrays;
    }
}
