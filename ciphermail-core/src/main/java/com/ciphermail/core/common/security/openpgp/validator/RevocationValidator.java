/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.security.openpgp.PGPNotSelfSignedException;
import com.ciphermail.core.common.security.openpgp.PGPSignatureValidator;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.util.Context;
import org.apache.commons.lang.text.StrBuilder;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.Objects;

/**
 * PGPPublicKeyValidator that checks whether a key revoked
 *
 * @author Martijn Brinkers
 *
 */
public class RevocationValidator implements PGPPublicKeyValidator
{
    private static final Logger logger = LoggerFactory.getLogger(RevocationValidator.class);

    /*
     * Some upper limit on the number of revocation signatures to check
     */
    private int maxSignatures = 100;

    /*
     * PGPSignatureValidator is used to check whether the revocations signature is valid
     */
    private final PGPSignatureValidator signatureValidator;

    public RevocationValidator(@Nonnull PGPSignatureValidator signatureValidator) {
        this.signatureValidator = Objects.requireNonNull(signatureValidator);
    }

    @Override
    public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
    {
        // The master key must be stored in the context if the key is not a master key
        PGPPublicKey masterKey = publicKey.isMasterKey() ? publicKey :
            context.get(StandardValidatorContextObjects.MASTER_KEY, PGPPublicKey.class);

        if (masterKey == null)
        {
            return new PGPPublicKeyValidatorResultImpl(this, false, PGPPublicKeyValidatorFailureSeverity.MAJOR,
                    "Master key not available. Unable to check revocation status.");
        }

        PGPPublicKeyValidatorResult result;

        if (publicKey.isMasterKey())
        {
            result = checkRevocation(masterKey, null, publicKey.getSignaturesOfType(PGPSignature.KEY_REVOCATION),
                    masterKey);
        }
        else {
            // Check master key revocation first. If master key is revoked, subkeys are not longer valid
            result = checkRevocation(masterKey, null, masterKey.getSignaturesOfType(PGPSignature.KEY_REVOCATION),
                    masterKey);

            if (result.isValid())
            {
                // Master key not revoked. Check sub key revocation.
                result = checkRevocation(masterKey, publicKey, publicKey.getSignaturesOfType(
                        PGPSignature.SUBKEY_REVOCATION), publicKey);
            }
        }

        return result;
    }

    private PGPPublicKeyValidatorResult checkRevocation(PGPPublicKey masterKey, PGPPublicKey subkey,
        Iterator<?> signatureIterator, PGPPublicKey keyToCheck)
    {
        boolean revoked = false;
        String failureMessage = null;
        PGPPublicKeyValidatorFailureSeverity severity = null;

        int signatureIndex = 0;

        // There might be multiple revocations but, we only check one since one revocation is enough.
        while (!revoked && signatureIterator.hasNext())
        {
            signatureIndex++;

            // Some check to make sure that we do not end up in some loop or DOS when there are a large number
            // of signatures
            if (signatureIndex > maxSignatures)
            {
                failureMessage = "Number of signatures exceeds the maximum";

                severity = PGPPublicKeyValidatorFailureSeverity.CRITICAL;
                /*
                 * Assume revoked
                 */
                revoked = true;

                break;
            }

            logger.debug("Key with key id {} is revoked. Validating signature.",
                    PGPUtils.getKeyIDHex(keyToCheck.getKeyID()));

            boolean signatureCorrect = false;

            PGPSignature signature = (PGPSignature) signatureIterator.next();

            logger.debug("Checking revocation signature for key with key id {}. Signature issuer key id {}",
                    PGPUtils.getKeyIDHex(keyToCheck.getKeyID()), PGPUtils.getKeyIDHex(signature.getKeyID()));

            try {
                signatureCorrect = signatureValidator.validateKeyRevocationSignature(masterKey, subkey, signature);

                if (!signatureCorrect)
                {
                    logger.warn("Revocation signature for key with key id {} does not verify.",
                            PGPUtils.getKeyIDHex(keyToCheck.getKeyID()));
                }
            }
            catch (PGPNotSelfSignedException e) {
                logger.debug("Revocation signature was not self signed", e);
            }
            catch (Exception e) {
                logger.error("Error validating the revocation signature", e);
            }

            if (signatureCorrect)
            {
                revoked = true;

                StrBuilder sb = new StrBuilder();

                sb.append("Key with key id ").append(PGPUtils.getKeyIDHex(keyToCheck.getKeyID()))
                    .append(" is revoked.");

                failureMessage = sb.toString();
                severity = PGPPublicKeyValidatorFailureSeverity.CRITICAL;
            }
        }

        return new PGPPublicKeyValidatorResultImpl(this, !revoked, severity, failureMessage);
    }

    public int getMaxSignatures() {
        return maxSignatures;
    }

    public void setMaxSignatures(int maxSignatures) {
        this.maxSignatures = maxSignatures;
    }
}
