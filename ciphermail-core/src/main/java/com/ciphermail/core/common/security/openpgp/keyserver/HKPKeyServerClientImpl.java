/*
 * Copyright (c) 2014-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.keyserver;

import com.ciphermail.core.common.http.AbstractVoidBinResponseConsumer;
import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactory;
import com.ciphermail.core.common.http.HttpClientContextFactory;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.util.LimitReachedException;
import com.ciphermail.core.common.util.MiscArrayUtils;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.core.common.util.SizeUtils;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.TextExtractor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleRequestBuilder;
import org.apache.hc.client5.http.entity.EntityBuilder;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ConnectionClosedException;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.http.nio.entity.BasicAsyncEntityProducer;
import org.apache.hc.core5.http.nio.support.BasicRequestProducer;
import org.apache.hc.core5.net.URIBuilder;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPPublicKeyRingCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Implementation of KeyServerClient that implements the OpenPGP HTTP key server Protocol (HKP)
 * <p>
 * See: <a href="http://tools.ietf.org/html/draft-shaw-openpgp-hkp-00">...</a>
 *
 * @author Martijn Brinkers
 *
 */
public class HKPKeyServerClientImpl implements HKPKeyServerClient
{
    private static final Logger logger = LoggerFactory.getLogger(HKPKeyServerClientImpl.class);

    private static final String TIMEOUT_ERROR = "A timeout has occurred connecting to: ";
    private static final String MAX_RESPONE_SIZE_EXCEEDED_ERROR = "Max HTTP response size exceeded.";

    /*
     * The max size in bytes a HKP response may be.
     */
    private int maxResponseSize = SizeUtils.MB;

    /*
     * The max time a request might take
     */
    private long totalTimeout = 30 * DateUtils.MILLIS_PER_SECOND;

    /*
     * Creates CloseableHttpAsyncClient instances
     */
    private final CloseableHttpAsyncClientFactory httpClientFactory;

    /*
     * Creates HttpClientContextFactory instances
     */
    private final HttpClientContextFactory httpClientContextFactory;

    /*
     * The HTTP client instance responsible for HTTP(s) communication
     */
    private CloseableHttpAsyncClient sharedHttpClient;

    public HKPKeyServerClientImpl(@Nonnull CloseableHttpAsyncClientFactory httpClientFactory,
            @Nonnull HttpClientContextFactory httpClientContextFactory)
    {
        this.httpClientFactory = Objects.requireNonNull(httpClientFactory);
        this.httpClientContextFactory = Objects.requireNonNull(httpClientContextFactory);
    }

    /*
     * Returns a shared CloseableHttpAsyncClient
     */
    private synchronized CloseableHttpAsyncClient getHTTPClient()
    throws IOException, GeneralSecurityException
    {
        if (sharedHttpClient == null) {
            sharedHttpClient = httpClientFactory.createClient();
        }

        return sharedHttpClient;
    }

    private URI getRequestURI(@Nonnull String serverURL, String requestPath)
    throws URISyntaxException
    {
        URIBuilder uriBuilder = new URIBuilder(serverURL);

        uriBuilder.appendPath(requestPath);

        return uriBuilder.build();
    }

    private String getParam(String[] params, int index) {
        return StringUtils.trimToNull(MiscArrayUtils.safeGet(params, index));
    }

    private Date getDate(String seconds)
    {
        Date date = null;

        long timeInSec = NumberUtils.toLong(StringUtils.trim(seconds), -1L);

        if (timeInSec != -1) {
            date = new Date(timeInSec * 1000);
        }

        return date;
    }

    private KeyServerKeyInfo parseKeyInfo(String[] params, @Nonnull HKPKeyServerClientSettings clientSettings)
    throws IOException
    {
        // Skip first element since this is pub
        String keyID = getParam(params, 1);

        if (keyID == null) {
            throw new IOException("KeyID is missing");
        }

        return new KeyServerKeyInfoImpl(
                clientSettings.getServerURL(),
                keyID,
                PGPPublicKeyAlgorithm.fromTag(NumberUtils.toInt(getParam(params, 2), -1)),
                NumberUtils.toInt(getParam(params, 3), -1),
                getDate(getParam(params, 4)),
                getDate(getParam(params, 5)),
                getParam(params, 6));
    }

    private void addUserID(KeyServerKeyInfo key, String userID)
    {
        userID = StringUtils.trimToNull(userID);

        if (userID != null)
        {
            try {
                // The user ID can be URI encoded to support Unicode
                userID = URLDecoder.decode(userID, StandardCharsets.UTF_8);
            }
            catch (Exception e) {
                logger.error("Erroring decoding userID " + userID, e);
            }

            key.getUserIDs().add(userID);
        }
    }

    private List<KeyServerKeyInfo> handleSearchKeysResponse(@Nonnull byte[] response, Integer maxKeys,
            HKPKeyServerClientSettings clientSettings)
    throws IOException
    {
        List<KeyServerKeyInfo> keys = new LinkedList<>();

        KeyServerKeyInfo currentKey = null;

        try (LineIterator lineIterator = new LineIterator(new InputStreamReader(new ByteArrayInputStream(response),
                StandardCharsets.US_ASCII)))
        {
            while (lineIterator.hasNext())
            {
                String line = StringUtils.trimToNull(lineIterator.next());

                if (line == null) {
                    continue;
                }

                String[] params = StringUtils.splitPreserveAllTokens(line, ':');

                if (params == null || params.length == 0) {
                    continue;
                }

                String command = StringUtils.lowerCase(StringUtils.trimToNull(params[0]));

                if (command == null) {
                    continue;
                }

                if ("info".equals(command)) {
                    // info line. Ignore
                }
                else if ("pub".equals(command))
                {
                    if (currentKey != null) {
                        keys.add(currentKey);
                    }

                    if (maxKeys != null && keys.size() >= maxKeys)
                    {
                        currentKey = null;

                        break;
                    }

                    currentKey = parseKeyInfo(params, clientSettings);
                }
                else if ("uid".equals(command))
                {
                    if (currentKey != null) {
                        addUserID(currentKey, MiscArrayUtils.safeGet(params, 1));
                    }
                    else {
                        logger.warn("uid found without current pub");
                    }
                }
                else {
                    logger.debug("Unsupported line: {}", line);
                }
            }
        }

        if (currentKey != null) {
            keys.add(currentKey);
        }

        return keys;
    }

    private String getErrorMessage(int httpStatusCode, String httpStatusReasonPhrase, @Nonnull byte[] response)
    {
        StrBuilder sb = new StrBuilder();

        try {
            Source source = new Source(new InputStreamReader(new ByteArrayInputStream(response),
                    StandardCharsets.UTF_8));

            // Create a TextExtractor that skips the title and h1
            TextExtractor textExtractor = new TextExtractor(source)
            {
                @Override
                public boolean excludeElement(StartTag startTag)
                {
                    return HTMLElementName.TITLE.equalsIgnoreCase(startTag.getName()) ||
                            HTMLElementName.H1.equalsIgnoreCase(startTag.getName());
                }
            };

            String responseBody = StringUtils.trimToNull(textExtractor.toString());

            if (responseBody != null) {
                sb.append(" ").append(responseBody);
            }

            sb. append(" (").append(httpStatusCode).append(" ").append(httpStatusReasonPhrase).append(")");
        }
        catch (IOException e) {
            logger.warn("Error parsing response");
        }

        return sb.toString().trim();
    }

    @Override
    public List<KeyServerKeyInfo> searchKeys(@Nonnull HKPKeyServerClientSettings clientSettings, String query,
            boolean exact)
    throws IOException, URISyntaxException
    {
        return searchKeys(clientSettings, query, exact, null);
    }

    @Override
    public List<KeyServerKeyInfo> searchKeys(@Nonnull HKPKeyServerClientSettings clientSettings, String query,
            boolean exact, Integer maxKeys)
    throws IOException, URISyntaxException
    {
        URI uri = getRequestURI(clientSettings.getServerURL(), clientSettings.getLookupKeysPath());

        SimpleHttpRequest request = SimpleRequestBuilder.get(uri)
                .addParameter("search", StringUtils.defaultString(query))
                .addParameter("op", "index")
                .addParameter("options", "mr")
                .addParameter("exact", BooleanUtils.toString(exact, "on", "off"))
                .build();

        CloseableHttpAsyncClient httpClient;

        try {
            httpClient = getHTTPClient();
        }
        catch (GeneralSecurityException e) {
            throw new IOException(e);
        }

        HttpClientContext clientContext = httpClientContextFactory.createHttpClientContext();

        ByteArrayOutputStream response = new ByteArrayOutputStream();

        AbstractVoidBinResponseConsumer consumer;

        try(
            WritableByteChannel outputChannel = Channels.newChannel(new SizeLimitedOutputStream(response,
                    maxResponseSize)))
        {
            consumer = new AbstractVoidBinResponseConsumer()
            {
                @Override
                protected void data(ByteBuffer src, boolean endOfStream)
                throws IOException
                {
                    try {
                        outputChannel.write(src);
                    }
                    catch (LimitReachedException e) {
                        // If the limit was reached, we do not want HTTPClient to retry. The
                        // @DefaultHttpRequestRetryStrategy will not retry if the exception is a
                        // ConnectionClosedException
                        throw new ConnectionClosedException(MAX_RESPONE_SIZE_EXCEEDED_ERROR, e);
                    }
                }
            };

            Future<Void> future = httpClient.execute(new BasicRequestProducer(request, null), consumer, clientContext,
                    null);

            future.get(totalTimeout, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();

            throw new IOException(e);
        }
        catch (ExecutionException e) {
            throw new IOException(ExceptionUtils.getRootCause(e));
        }
        catch (TimeoutException e) {
            throw new IOException(TIMEOUT_ERROR + uri);
        }

        if (consumer.getHttpStatusCode() != HttpStatus.SC_OK)
        {
            throw new IOException(getErrorMessage(consumer.getHttpStatusCode(), consumer.getHttpStatusReasonPhrase(),
                    response.toByteArray()));
        }

        return handleSearchKeysResponse(response.toByteArray(), maxKeys, clientSettings);
    }

    @Override
    public String submitKeys(@Nonnull HKPKeyServerClientSettings clientSettings,
            PGPPublicKeyRingCollection keyRingCollection)
    throws IOException, URISyntaxException
    {
        if (keyRingCollection == null) {
            return null;
        }

        ByteArrayOutputStream armoredKeyRing = new ByteArrayOutputStream();

        ArmoredOutputStream armorer = new ArmoredOutputStream(armoredKeyRing, PGPUtils.createArmorHeaders());

        try {
            armorer.write(keyRingCollection.getEncoded());
        }
        finally {
            IOUtils.closeQuietly(armorer);
        }

        URI uri = getRequestURI(clientSettings.getServerURL(), clientSettings.getSubmitKeysPath());

        SimpleHttpRequest request = SimpleRequestBuilder.post(uri).build();

        final EntityBuilder entityBuilder = EntityBuilder.create();

        entityBuilder.setParameters(new BasicNameValuePair("keytext",
                MiscStringUtils.toStringFromASCIIBytes(armoredKeyRing.toByteArray())));

        CloseableHttpAsyncClient httpClient;

        try {
            httpClient = getHTTPClient();
        }
        catch (GeneralSecurityException e) {
            throw new IOException(e);
        }

        HttpClientContext clientContext = httpClientContextFactory.createHttpClientContext();

        ByteArrayOutputStream response = new ByteArrayOutputStream();

        AbstractVoidBinResponseConsumer consumer;

        try(
                WritableByteChannel outputChannel = Channels.newChannel(new SizeLimitedOutputStream(response,
                    maxResponseSize));

                HttpEntity contentEntity = entityBuilder.build();
        )
        {
            consumer = new AbstractVoidBinResponseConsumer()
            {
                @Override
                protected void data(ByteBuffer src, boolean endOfStream)
                throws IOException
                {
                    try {
                        outputChannel.write(src);
                    }
                    catch (LimitReachedException e) {
                        // If the limit was reached, we do not want HTTPClient to retry. The
                        // @DefaultHttpRequestRetryStrategy will not retry if the exception is a
                        // ConnectionClosedException
                        throw new ConnectionClosedException(MAX_RESPONE_SIZE_EXCEEDED_ERROR, e);
                    }
                }
            };

            Future<Void> future = httpClient.execute(
                new BasicRequestProducer(request, new BasicAsyncEntityProducer(IOUtils.toByteArray(
                        contentEntity.getContent()), ContentType.APPLICATION_FORM_URLENCODED)),
                consumer, clientContext, null);

            future.get(totalTimeout, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();

            throw new IOException(e);
        }
        catch (ExecutionException e) {
            throw new IOException(ExceptionUtils.getRootCause(e));
        }
        catch (TimeoutException e) {
            throw new IOException(TIMEOUT_ERROR + uri);
        }

        if (consumer.getHttpStatusCode() != HttpStatus.SC_OK)
        {
            throw new IOException(getErrorMessage(consumer.getHttpStatusCode(), consumer.getHttpStatusReasonPhrase(),
                    response.toByteArray()));
        }

        Source source = new Source(new InputStreamReader(new ByteArrayInputStream(response.toByteArray()),
                StandardCharsets.UTF_8));

        // Create a TextExtractor that skips the title and h1
        TextExtractor textExtractor = new TextExtractor(source)
        {
            @Override
            public boolean excludeElement(StartTag startTag)
            {
                return HTMLElementName.TITLE.equalsIgnoreCase(startTag.getName()) ||
                        HTMLElementName.H1.equalsIgnoreCase(startTag.getName());
            }
        };

        String responseBody = StringUtils.trimToNull(textExtractor.toString());

        if (StringUtils.isNotEmpty(responseBody)) {
            logger.info(responseBody);
        }

        return responseBody;
    }

    @Override
    public PGPPublicKeyRingCollection getKeys(@Nonnull HKPKeyServerClientSettings clientSettings,
            @Nonnull String keyID)
    throws IOException, URISyntaxException
    {
        keyID = StringUtils.trimToNull(keyID);

        if (!StringUtils.startsWithIgnoreCase(keyID, "0x")) {
            keyID = "0x" + keyID;
        }

        URI uri = getRequestURI(clientSettings.getServerURL(), clientSettings.getLookupKeysPath());

        SimpleHttpRequest request = SimpleRequestBuilder.get(uri)
                .addParameter("search", keyID)
                .addParameter("op", "get")
                .addParameter("options", "mr")
                .addParameter("exact", "on")
                .build();

        CloseableHttpAsyncClient httpClient;

        try {
            httpClient = getHTTPClient();
        }
        catch (GeneralSecurityException e) {
            throw new IOException(e);
        }

        HttpClientContext clientContext = httpClientContextFactory.createHttpClientContext();

        ByteArrayOutputStream response = new ByteArrayOutputStream();

        AbstractVoidBinResponseConsumer consumer;

        try(
            WritableByteChannel outputChannel = Channels.newChannel(new SizeLimitedOutputStream(response,
                    maxResponseSize)))
        {
            consumer = new AbstractVoidBinResponseConsumer()
            {
                @Override
                protected void data(ByteBuffer src, boolean endOfStream)
                throws IOException
                {
                    try {
                        outputChannel.write(src);
                    }
                    catch (LimitReachedException e) {
                        // If the limit was reached, we do not want HTTPClient to retry. The
                        // @DefaultHttpRequestRetryStrategy will not retry if the exception is a
                        // ConnectionClosedException
                        throw new ConnectionClosedException(MAX_RESPONE_SIZE_EXCEEDED_ERROR, e);
                    }
                }
            };

            Future<Void> future = httpClient.execute(new BasicRequestProducer(request, null), consumer, clientContext,
                    null);

            future.get(totalTimeout, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();

            throw new IOException(e);
        }
        catch (ExecutionException e) {
            throw new IOException(ExceptionUtils.getRootCause(e));
        }
        catch (TimeoutException e) {
            throw new IOException(TIMEOUT_ERROR + uri);
        }

        if (consumer.getHttpStatusCode() != HttpStatus.SC_OK)
        {
            throw new IOException(getErrorMessage(consumer.getHttpStatusCode(), consumer.getHttpStatusReasonPhrase(),
                    response.toByteArray()));
        }

        try {
            return new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(new ByteArrayInputStream(
                    response.toByteArray())));
        }
        catch (PGPException e) {
            throw new IOException("Response was not a valid PGP keyring", e);
        }
    }

    public int getMaxResponseSize() {
        return maxResponseSize;
    }

    public void setMaxResponseSize(int maxResponseSize) {
        this.maxResponseSize = maxResponseSize;
    }

    public long getTotalTimeout() {
        return totalTimeout;
    }

    public void setTotalTimeout(long totalTimeout) {
        this.totalTimeout = totalTimeout;
    }

    public void shutdown()
    {
        if (sharedHttpClient != null) {
            sharedHttpClient.initiateShutdown();
        }
    }
}
