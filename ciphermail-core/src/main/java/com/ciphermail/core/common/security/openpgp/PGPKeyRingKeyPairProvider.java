/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.MissingKeyAlias;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of PGPKeyPairProvider that reads the keys from a PGPKeyRing.
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyRingKeyPairProvider implements PGPKeyPairProvider
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyRingKeyPairProvider.class);

    /*
     * The key ring to read the keys from
     */
    private final PGPKeyRing keyRing;

    public PGPKeyRingKeyPairProvider(@Nonnull PGPKeyRing keyRing) {
        this.keyRing = Objects.requireNonNull(keyRing);
    }

    @Override
    public List<PGPKeyRingPair> getByKeyID(long keyID)
    throws PGPException, IOException
    {
        List<PGPKeyRingPair> result = new LinkedList<>();

        CloseableIterator<PGPKeyRingEntry> entryIterator = keyRing.getByKeyID(keyID, MissingKeyAlias.NOT_ALLOWED);

        if (entryIterator != null)
        {
            try {
                while (entryIterator.hasNext())
                {
                    PGPKeyRingEntry entry = entryIterator.next();

                    PGPPrivateKey privateKey = entry.getPrivateKey();

                    if (privateKey != null) {
                        result.add(new PGPKeyRingPairImpl(entry.getPublicKey(), privateKey));
                    }
                    else {
                        logger.warn("PGPPrivateKey not available for key KeyID {}",
                                    PGPUtils.getKeyIDHex(entry.getKeyID()));
                    }
                }
            }
            catch (CloseableIteratorException e) {
                throw new IOException(e);
            }
            finally {
                CloseableIteratorUtils.closeQuietly(entryIterator);
            }
        }

        return result;
    }
}
