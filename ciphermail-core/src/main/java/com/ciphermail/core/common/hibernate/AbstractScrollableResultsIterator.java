/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import com.ciphermail.core.common.reflection.ReflectionUtils;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import org.apache.commons.lang.IllegalClassException;
import org.hibernate.ScrollableResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.lang.reflect.ParameterizedType;
import java.util.Objects;

/**
 * {@link CloseableIterator} implementation that can be used as a base class to iterate over entries in
 * a database using a {@link ScrollableResults} as the data source.
 *
 * @author Martijn Brinkers
 *
 * @param <T>
 */
public abstract class AbstractScrollableResultsIterator<T> implements CloseableIterator<T>
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractScrollableResultsIterator.class);

    /*
     * The class over which will be iterated
     */
    private final Class<T> persistentClass;

    /*
     * The ScrollableResults from which the items will be read
     */
    private final ScrollableResults<?> results;

    /*
     * The next element to return when #next is called
     */
    private T nextElement;

    /*
     * Will be true if the CloseableIterator was closed
     */
    private boolean closed;

    @SuppressWarnings("unchecked")
    protected AbstractScrollableResultsIterator(@Nonnull ScrollableResults<?> results)
    {
        this.results = Objects.requireNonNull(results);

        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().
                getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected Object parseElement(Object element)
    {
        if (!ReflectionUtils.isInstanceOf(element.getClass(), persistentClass))
        {
            throw new IllegalClassException("The object returned from the ScrollableResults is not a " +
                    persistentClass.getCanonicalName());
        }

        return element;
    }

    protected abstract boolean hasMatch(T element);

    protected Object get(ScrollableResults<?> results) {
        return results.get();
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean hasNext()
    throws CloseableIteratorException
    {
        if (closed) {
            throw new CloseableIteratorException("This iterator is closed");
        }

        if (nextElement != null) {
            return true;
        }

        T newElement = null;

        boolean hasNext = false;

        while(results.next())
        {
            Object object = get(results);

            object = parseElement(object);

            newElement = (T) object;

            if (!hasMatch(newElement)) {
                continue;
            }

            hasNext = true;
            break;
        }

        if (hasNext) {
            nextElement = newElement;
        }
        else {
            // close if we have arrived at the last entry
            close();
        }

        return hasNext;
    }

    @Override
    public T next()
    throws CloseableIteratorException
    {
        if (closed) {
            throw new CloseableIteratorException("This iterator is closed");
        }

        if (nextElement == null && (!hasNext())) {
                throw new CloseableIteratorException("There is not next entry");
        }

        T copy = nextElement;

        nextElement = null;

        return copy;
    }

    @Override
    public void close()
    throws CloseableIteratorException
    {
        logger.debug("Closing iterator");

        if (!closed)
        {
            results.close();

            closed = true;
        }
    }

    @Override
    public boolean isClosed()
    throws CloseableIteratorException
    {
        return closed;
    }
}