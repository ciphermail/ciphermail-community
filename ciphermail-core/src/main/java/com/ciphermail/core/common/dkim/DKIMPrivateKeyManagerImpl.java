/*
 * Copyright (c) 2016-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dkim;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateBuilderException;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.impl.StandardX509CertificateBuilder;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.common.security.password.PasswordProvider;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of DKIMPrivateKeyManager which stores the private keys into a KeyStore
 *
 * @author Martijn Brinkers
 *
 */
public class DKIMPrivateKeyManagerImpl implements DKIMPrivateKeyManager
{
    private static final Logger logger = LoggerFactory.getLogger(DKIMPrivateKeyManagerImpl.class);

    /*
     * For now only RSA is supported
     */
    private static final String PUBLIC_KEY_ALGORITHM = "RSA";

    /*
     * Key alias prefix
     */
    private static final String KEY_ALIAS_PREFIX = "DKIM:";

    /*
     * Provides access to the key store which is used for storing the DKIM key pairs
     */
    private final KeyStoreProvider keyStoreProvider;

    /*
     * Provides passwords for the keystore
     */
    private final PasswordProvider passwordProvider;

    /*
     * Factory class for creating security object instances
     */
    private final SecurityFactory securityFactory;

    public DKIMPrivateKeyManagerImpl(@Nonnull KeyStoreProvider keyStoreProvider,
            @Nonnull PasswordProvider passwordProvider)
    {
        this.keyStoreProvider = Objects.requireNonNull(keyStoreProvider);
        this.passwordProvider = Objects.requireNonNull(passwordProvider);
        this.securityFactory = DKIMSecurityFactoryFactory.getSecurityFactory();
    }

    private static String createKeyAlias(String keyId)
    {
        keyId = StringUtils.trim(keyId);

        if (keyId == null) {
            throw new IllegalArgumentException("keyId should not be empty");
        }

        return KEY_ALIAS_PREFIX + keyId;
    }

    private static String getKeyId(String alias)
    {
        alias = StringUtils.trim(alias);

        if (alias == null) {
            throw new IllegalArgumentException("alias should not be empty");
        }

        return StringUtils.substringAfter(alias, KEY_ALIAS_PREFIX);
    }

    /*
     * Generates a self-signed certificate. We need this certificate because a KeyStore requires that a certificate
     * is stored together with the key.
     */
    private X509Certificate createSelfSignedCertificate(KeyPair keyPair, String subject)
    throws IOException
    {
        X500PrincipalBuilder principalBuilder = X500PrincipalBuilder.getInstance();

        principalBuilder.setCommonName(subject);

        X500Principal subjectX500 = principalBuilder.buildPrincipal();

        StandardX509CertificateBuilder certificateBuilder = new StandardX509CertificateBuilder(
                securityFactory.getSensitiveProvider(), securityFactory.getNonSensitiveProvider());

        certificateBuilder.setSubject(subjectX500);
        certificateBuilder.setIssuer(subjectX500);
        certificateBuilder.setPublicKey(keyPair.getPublic());
        certificateBuilder.setNotBefore(DateUtils.addDays(new Date(), -1));
        certificateBuilder.setNotAfter(DateUtils.addYears(new Date(), 32));
        certificateBuilder.setSerialNumber(BigInteger.valueOf(1));
        certificateBuilder.setSignatureAlgorithm("SHA256With" + PUBLIC_KEY_ALGORITHM);

        try {
            return certificateBuilder.generateCertificate(keyPair.getPrivate(), null);
        }
        catch (CertificateBuilderException e) {
            throw new IOException(e);
        }
    }

    @Override
    public @Nonnull KeyPair generateKey(@Nonnull String keyId, int keyLength)
    throws IOException, GeneralSecurityException
    {
        KeyPairGenerator keyPairGenerator = SecurityFactoryFactory.getSecurityFactory().createKeyPairGenerator(
                PUBLIC_KEY_ALGORITHM);

        keyPairGenerator.initialize(keyLength);

        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        setKey(keyId, keyPair);

        return keyPair;
    }

    @Override
    public void setKey(@Nonnull String keyId, @Nonnull KeyPair keyPair)
    throws IOException, GeneralSecurityException
    {
        String keyAlias = createKeyAlias(keyId);

        // We need a certificate because a KeyStore requires that a certificate is stored together with the key.
        X509Certificate certificate = createSelfSignedCertificate(keyPair, keyId);

        keyStoreProvider.getKeyStore().setKeyEntry(keyAlias, keyPair.getPrivate(), passwordProvider.getPassword(),
                new Certificate[]{certificate});

        logger.info("DKIM key with key alias {} added to key store", keyAlias);
    }

    @Override
    public KeyPair getKey(@Nonnull String keyId)
    throws IOException, GeneralSecurityException
    {
        KeyStore.PrivateKeyEntry entry = (PrivateKeyEntry) keyStoreProvider.getKeyStore().getEntry(
                createKeyAlias(keyId), new KeyStore.PasswordProtection(passwordProvider.getPassword()));

        KeyPair keyPair = null;

        if (entry != null) {
            keyPair = new KeyPair(entry.getCertificate().getPublicKey(), entry.getPrivateKey());
        }

        return keyPair;
    }

    @Override
    public boolean isValidKey(@Nonnull String keyId)
    throws GeneralSecurityException
    {
        return keyStoreProvider.getKeyStore().isKeyEntry(createKeyAlias(keyId));
    }

    @Override
    public void deleteKey(@Nonnull String keyId)
    throws GeneralSecurityException
    {
        keyStoreProvider.getKeyStore().deleteEntry(createKeyAlias(keyId));
    }

    @Override
    public List<String> getKeyIds()
    throws KeyStoreException
    {
        return Collections.list(keyStoreProvider.getKeyStore().aliases()).stream()
                .map(DKIMPrivateKeyManagerImpl::getKeyId).toList();
    }
}
