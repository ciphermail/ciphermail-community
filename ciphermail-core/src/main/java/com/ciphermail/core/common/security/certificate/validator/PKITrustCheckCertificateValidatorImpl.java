/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate.validator;

import com.ciphermail.core.common.security.certpath.CertificatePathBuilder;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilderFactory;
import com.ciphermail.core.common.security.certstore.CertStoreUtils;
import com.ciphermail.core.common.security.crl.RevocationChecker;
import com.ciphermail.core.common.security.crl.RevocationResult;
import com.ciphermail.core.common.security.crl.RevocationStatus;
import com.ciphermail.core.common.security.ctl.CTL;
import com.ciphermail.core.common.security.ctl.CTLException;
import com.ciphermail.core.common.security.ctl.CTLValidity;
import com.ciphermail.core.common.security.ctl.CTLValidityResult;
import com.ciphermail.core.common.util.CurrentDateProvider;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.Certificate;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * This CertificateValidator is used to check the trust of a certificate (ie. if a valid certificate chain
 * can be build) and that the certificate is not revoked.
 * <p>
 * This class if not thread safe.
 *
 * @author Martijn Brinkers
 *
 */
public class PKITrustCheckCertificateValidatorImpl implements PKITrustCheckCertificateValidator
{
    private static final Logger logger = LoggerFactory.getLogger(PKITrustCheckCertificateValidatorImpl.class) ;

    /*
     * Default acceptable revocation status. This is the default policy for this class.
     * */
    private static final RevocationStatus[] DEFAULT_ACCEPTABLE_REVOCATION_STATUS = {
            RevocationStatus.NOT_REVOKED,
            RevocationStatus.UNKNOWN,
            RevocationStatus.EXPIRED};

    /*
     * Name of this CertificateValidator.
     */
    private final String name;

    /*
     * Used for the creation of Certificate path builders
     */
    private final CertificatePathBuilderFactory certificatePathBuilderFactory;

    /*
     * Used to check the revocation state (using CRL's) of the certificate.
     */
    private final RevocationChecker revocationChecker;

    /*
     * Certificate Trust List
     */
    private final CTL ctl;

    /*
     * The revocation reasons that are accepted as valid ie. flag the certificate as not revoked.
     */
    private final RevocationStatus[] acceptableRevocationStatus;

    /*
     * Certificates that will be added to the path builder
     */
    private Set<Certificate> additionalCertificates;

    /*
     * The date used for path building and revocation checking. If not set the current date will be used.
     */
    private Date date;

    /*
     * True if the last result was valid
     */
    boolean valid;

    /*
     * True if a valid path could be built.
     */
    private boolean trusted;

    /*
     * True if one of the certificates in the chain is considered revoked (based on acceptedRevocationStatus).
     */
    private boolean revoked;

    /*
     * True if one of the certificates in the chain is blacklisted
     */
    private boolean blackListed;

    /*
     * True if the certificate is whitelisted
     */
    private boolean whiteListed;

    /*
     * Gives some info about the cause of the failure (should only contain relevant info when the last call
     * to isValid returned false).
     */
    private String failureMessage = "";

    /*
     * The certificate path of the certificate
     */
    private CertPath certPath;

    /*
     * The TrustAnchor of the certificate
     */
    private TrustAnchor trustAnchor;

    protected PKITrustCheckCertificateValidatorImpl(
            String name,
            @Nonnull CertificatePathBuilderFactory certificatePathBuilderFactory,
            @Nonnull RevocationChecker revocationChecker,
            CTL ctl,
            Collection<? extends Certificate> additionalCertificates)
    {
        this.name = name;
        this.certificatePathBuilderFactory = Objects.requireNonNull(certificatePathBuilderFactory);
        this.revocationChecker = Objects.requireNonNull(revocationChecker);
        this.ctl = ctl;

        this.acceptableRevocationStatus = DEFAULT_ACCEPTABLE_REVOCATION_STATUS;

        if (additionalCertificates != null && !additionalCertificates.isEmpty())
        {
            // Clone the additionalCertificates
            this.additionalCertificates = new HashSet<>(additionalCertificates);
        }
    }

    public PKITrustCheckCertificateValidatorImpl(CertificatePathBuilderFactory certificatePathBuilderFactory,
            RevocationChecker revocationChecker, CTL ctl, Collection<? extends Certificate> additionalCertificates)
    {
        this("PKITrustCheckCertificateValidator", certificatePathBuilderFactory, revocationChecker, ctl,
                additionalCertificates);
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * True if the last time isValid(certificate) returned true, false otherwise. The certificate
     * is valid when a complete trusted path can be build up to a trusted root and the
     * certificate is not revoked.
     */
    @Override
    public boolean isValid() {
        return valid;
    }

    /**
     * True if a path could be build (i.e. up to a trusted root). trusted does not mean the certificate is not
     * revoked, it only means that a complete chain could be built from the certificate to a trusted root.
     */
    @Override
    public boolean isTrusted() {
        return trusted;
    }

    /**
     * True if one of the certificates in the chain is revoked (based on acceptedRevocationStatus). This value is
     * only valid if isTrusted is true. If isTrusted is false isRevoked will always be false. Revocation checking
     * cannot be done when the chain is incomplete. We will therefore return false when the chain in incomplete
     * because we do not know better.
     */
    @Override
    public boolean isRevoked() {
        return revoked;
    }

    /**
     * True if one of the certificates in the chain is blacklisted. Blacklist checking is only done when
     * the certificate is valid.
     */
    @Override
    public boolean isBlackListed() {
        return blackListed;
    }

    /**
     * True if the certificate is whitelisted. A white list check is only done when the certificate is
     * invalid.
     */
    @Override
    public boolean isWhiteListed() {
        return whiteListed;
    }

    @Override
    public String getFailureMessage() {
        return failureMessage;
    }

    /**
     * Sets the date used for path building and revocation checking. If not set the current date will be used.
     */
    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    private Date getDate()
    {
        if (date != null) {
            return date;
        }

        return CurrentDateProvider.getNow();
    }

    private boolean isBlackListed(@Nonnull CertPath certPath)
    throws CTLException
    {
        if (ctl == null) {
            return false;
        }

        List<? extends Certificate> certificates = certPath.getCertificates();

        for (int i = 0; i < certificates.size(); i++)
        {
            Certificate certificate = certificates.get(i);

            if (!(certificate instanceof X509Certificate))
            {
                logger.warn("Only X509Certificates can be black listed.");

                continue;
            }

            CTLValidityResult result = ctl.checkValidity((X509Certificate) certificate);

            if (CTLValidity.INVALID == result.getValidity())
            {
                // If the certificate is the first it's not an intermediate.
                reportFailure(i == 0 ? result.getMessage() : ("Intermediate " + result.getMessage()));

                return true;
            }
        }

        return false;
    }

    private boolean isWhiteListed(@Nonnull X509Certificate certificate)
    throws CTLException
    {
        boolean whiteListed = false;

        if (ctl != null)
        {
            CTLValidityResult result = ctl.checkValidity(certificate);

            switch (result.getValidity())
            {
            case VALID      : whiteListed = true; break;
            case NOT_LISTED : break;
            default:
                reportFailure(result.getMessage());
            }
        }

        return whiteListed;
    }

    @Override
    public boolean isValid(@Nonnull Certificate certificate)
    {
        valid = false;
        trusted = false;
        revoked = false;
        blackListed = false;
        whiteListed = false;

        if (!(certificate instanceof X509Certificate x509Certificate))
        {
            failureMessage = "Certificate is not a X509Certificate";

            return false;
        }

        failureMessage = "";

        try {
            CertPathAndAnchor certPathAndAnchor = getCertPathAndAnchor(x509Certificate);

            certPath = certPathAndAnchor.getCertPath();
            trustAnchor = certPathAndAnchor.getTrustAnchor();

            if (certPath != null && trustAnchor != null)
            {
                trusted = true;

                revoked = isRevoked(certPath, trustAnchor);

                if (!revoked)
                {
                    // Chain is valid, not expired, not revoked. We now need to check
                    // whether a certificate in the chain is not BlackListed.
                    blackListed = isBlackListed(certPath);

                    valid = !blackListed;
                }
                else {
                    valid = false;
                }
            }
            else {
                throw new CertPathBuilderException("A valid CertPath could not be built.");
            }
        }
        catch (CertPathBuilderException e) {
            // A valid certificate chain could not be built. This can happen because of a lot of reasons:
            // an intermediate or root certificate is not trusted, a certificate in the chain has expired,
            // a certificate in the chain is invalid etc. We now check whether the certificate is white listed.
            //
            // Note: Because the chain is not valid we cannot check the revocation status so white listing
            // a certificate should be done with care.
            logger.debug("CertPathBuilderException", e);

            try {
                whiteListed = isWhiteListed(x509Certificate);

                valid = whiteListed;
            }
            catch (CTLException ctle) {
                logger.error("Error checking the CTL.", ctle);
            }

            if (!valid) {
                // We do not want a complete stack trace on a CertPathBuilderException because this exception can be thrown
                // quite often. CertPathBuilderException is also thrown when a path validator exception occurs. We will
                // therefore try to extract the root cause.
                Throwable cause = ExceptionUtils.getRootCause(e);

                if (cause == null) {
                    cause = e;
                }

                reportFailure("Error building certPath. " + cause.getMessage());
            }
        }
        catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchProviderException e) {
            reportFailure("Error building certPath.", e);
        }
        catch (CTLException e) {
            reportFailure("Error checking CTL status.", e);
        }

        if (!valid) {
            logger.debug("Failure message: {}", failureMessage);
        }

        return valid;
    }

    private void reportFailure(String message, Throwable t)
    {
        Throwable cause = ExceptionUtils.getRootCause(t);

        if (cause == null) {
            cause = t;
        }

        failureMessage = StringUtils.isNotBlank(failureMessage) ? failureMessage + "; " + message : message;

        if (cause != null)
        {
            failureMessage = failureMessage + " Exception: " + cause.getMessage();

            logger.error(message, cause);
        }
        else {
            logger.debug(message);
        }
    }

    private void reportFailure(String message) {
        reportFailure(message, null);
    }

    protected void modifyPathBuilder(CertificatePathBuilder pathBuilder)
    {
        // Subclasses can use this to add CertPathChecker's etc.
    }

    private CertPathAndAnchor getCertPathAndAnchor(X509Certificate certificate)
    throws CertPathBuilderException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
            NoSuchProviderException
    {
        CertificatePathBuilder pathBuilder = certificatePathBuilderFactory.createCertificatePathBuilder();

        modifyPathBuilder(pathBuilder);

        // Add the x509Certificate to the stores used for path building to make sure the
        // certificate is found by the path builder.
        pathBuilder.addCertStore(CertStoreUtils.createCertStore(certificate));

        // Add the additional certificates if there are any
        if (additionalCertificates != null) {
            pathBuilder.addCertStore(CertStoreUtils.createCertStore(additionalCertificates));
        }

        pathBuilder.setDate(getDate());

        CertPathBuilderResult pathBuilderResult = pathBuilder.buildPath(certificate);

        CertPath certPath = pathBuilderResult.getCertPath();
        TrustAnchor trustAnchor = null;

        if (pathBuilderResult instanceof PKIXCertPathBuilderResult pkixResult) {

            trustAnchor = pkixResult.getTrustAnchor();
        }

        return new CertPathAndAnchor(certPath, trustAnchor);
    }

    private boolean isRevoked(CertPath certPath, TrustAnchor trustAnchor)
    {
        boolean revoked = true;

        try {
            // check if the certificate is revoked
            RevocationResult revocationResult = revocationChecker.getRevocationStatus(certPath,
                    trustAnchor, getDate());

            if (revocationResult != null)
            {
                // Check if the returned revocation status is acceptable. What an acceptable revocation
                // status is determined by a policy. Currently, we will use a hardcoded policy. See
                // DEFAULT_ACCEPTABLE_REVOCATION_STATUS.
                for (RevocationStatus acceptableStatus : acceptableRevocationStatus)
                {
                    if (acceptableStatus == revocationResult.getStatus())
                    {
                        revoked = false;
                        break;
                    }
                }

                if (revoked) {
                    reportFailure("Certificate not accepted. Revocation status :" + revocationResult.getStatus());
                }
            }
        }
        catch (CRLException e) {
            reportFailure("Error while checking revocation status.", e);
        }

        return revoked;
    }

    @Override
    public CertPath getCertPath() {
        return certPath;
    }

    @Override
    public TrustAnchor getTrustAnchor() {
        return trustAnchor;
    }

    /*
     * Helper class so we can return a CertPath and TrustAnchor
     */
    private static class CertPathAndAnchor
    {
        private final CertPath certPath;
        private final TrustAnchor trustAnchor;

        public CertPathAndAnchor(CertPath certPath, TrustAnchor trustAnchor)
        {
            this.certPath = certPath;
            this.trustAnchor = trustAnchor;
        }

        public CertPath getCertPath() {
            return certPath;
        }

        public TrustAnchor getTrustAnchor() {
            return trustAnchor;
        }
    }
}
