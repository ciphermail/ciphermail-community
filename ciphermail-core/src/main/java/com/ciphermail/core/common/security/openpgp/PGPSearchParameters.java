/*
 * Copyright (c) 2013-2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;

/**
 * Parameters which influence the search the PGP Key Ring
 *
 * @author Martijn Brinkers
 *
 */
public class PGPSearchParameters
{
    /*
     * Match or like search
     */
    private Match match = Match.LIKE;

    /*
     * Also return expired
     */
    private Expired expired = Expired.MATCH_ALL;

    /*
     * Also return keys without private keys
     */
    private MissingKeyAlias missingKeyAlias = MissingKeyAlias.ALLOWED;

    /*
     * Return only master keys, sub keys or both
     */
    private PGPKeyType keyType = PGPKeyType.ALL;

    public PGPSearchParameters() {
        // Default constructor
    }

    public PGPSearchParameters(Match match, Expired expired, MissingKeyAlias missingKeyAlias, PGPKeyType keyType) {
        this.match = match;
        this.expired = expired;
        this.missingKeyAlias = missingKeyAlias;
        this.keyType = keyType;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Expired getExpired() {
        return expired;
    }

    public void setExpired(Expired expired) {
        this.expired = expired;
    }

    public MissingKeyAlias getMissingKeyAlias() {
        return missingKeyAlias;
    }

    public void setMissingKeyAlias(MissingKeyAlias missingKeyAlias) {
        this.missingKeyAlias = missingKeyAlias;
    }

    public PGPKeyType getKeyType() {
        return keyType;
    }

    public void setKeyType(PGPKeyType keyType) {
        this.keyType = keyType;
    }
}
