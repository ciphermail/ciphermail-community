/*
 * Copyright (c) 2010-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.properties.PropertyUtils;

/**
 * general constants for security related functionality
 *
 * @author Martijn Brinkers
 *
 */
public class SecurityConstants
{
    private SecurityConstants() {
        // empty on purpose
    }

    /**
     * The system property which is used to detect whether the Outlook 2010 workaround is enabled. The
     * property value should be true or false.
     */
    public static final String OL2010_SKI_WORKAROUND_ENABLED_PROP_KEY = "ciphermail.smime.outlook-2010-ski-workaround-enable";

    private static final boolean DEFAULT_OUTLOOK2010_SKI_WORKAROUND_ENABLED = PropertyUtils.getBooleanSystemProperty(
            SecurityConstants.OL2010_SKI_WORKAROUND_ENABLED_PROP_KEY, false);

    /*
     * determines whether the SKI workaround for Outlook 2010 is enabled.
     */
    private static boolean outlook2010SKIWorkaroundEnabled = DEFAULT_OUTLOOK2010_SKI_WORKAROUND_ENABLED;

    /**
     * True if the Subject Key Identifier workaround for Outlook 2010 is enabled.
     * <p>
     * For info why this workaround is needed see: <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=559243">...</a> and
     * <a href="http://www.ietf.org/mail-archive/web/smime/current/msg18730.html">...</a>
     */
    public static boolean isOutlook2010SKIWorkaroundEnabled() {
        return outlook2010SKIWorkaroundEnabled;
    }

    public static void setOutlook2010SKIWorkaroundEnabled(boolean enable) {
        outlook2010SKIWorkaroundEnabled = enable;
    }
}
