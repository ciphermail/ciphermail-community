package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.RequiredByJames;

/**
 * Extension of PGPKeySelector with the sole purpose of disambiguating PGPKeySelector implementations
 */
@RequiredByJames
public interface PGPPublicKeySelector extends PGPKeySelector
{
    // empty on purpose
}
