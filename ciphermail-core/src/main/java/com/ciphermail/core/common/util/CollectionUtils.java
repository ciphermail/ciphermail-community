/*
 * Copyright (c) 2008-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.ciphermail.core.common.reflection.ReflectionUtils;
import org.apache.commons.lang.ObjectUtils;

import javax.annotation.Nonnull;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class CollectionUtils
{
    private CollectionUtils() {
        // empty on purpose
    }

    /**
     * Copies elements from the target collection to the source collection only when the elements implements all the
     * clazz classes.
     *
     * @param <T>
     * @param source
     * @param target
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void copyCollectionFiltered(@Nonnull Collection source, @Nonnull Collection target,
            @Nonnull Class<?>... clazz)
    {
        for (Object object : source)
        {
            if (ReflectionUtils.isInstanceOf(object.getClass(), clazz)) {
                target.add(object);
            }
        }
    }

    /**
     * Converts the collection to an array of Strings by calling org.apache.commons.lang.ObjectUtils on each item.
     * If items is null null will be returned. If an item is null the item will be "nullString".
     */
    public static String[] toStringArray(Collection<?> items, String nullString)
    {
        if (items == null) {
            return null;
        }

        String[] result = new String[items.size()];

        int i = 0;

        for (Object obj : items)
        {
            result[i] = ObjectUtils.toString(obj, nullString);

            i++;
        }

        return result;
    }

    /**
     * Converts the collection to an array of Strings by calling org.apache.commons.lang.ObjectUtils on each item.
     * If items is null null will be returned. If an item is null the item will be "" (empty string).
     */
    public static String[] toStringArray(Collection<?> items) {
        return toStringArray(items, "");
    }

    /**
     * Converts the collection to a List of Strings by calling org.apache.commons.lang.ObjectUtils on each item. If
     * items is null an empty List will be returned (i.e., a non-null List instance is always returned). If an item is
     * null the item will be "nullString".
     *
     * @param items
     * @return
     */
    public static List<String> toStringList(Collection<?> items, String nullString)
    {
        int size = items != null ? items.size() : 0;

        ArrayList<String> sl = new ArrayList<>(size);

        if (items != null)
        {
            for (Object obj : items) {
                sl.add(ObjectUtils.toString(obj, nullString));
            }
        }

        return sl;
    }

    /**
     * Converts the collection to a Set of Strings by calling org.apache.commons.lang.ObjectUtils on each item of items.
     * If items is null an empty Set will be returned (i.e., a non-null Set instance is always returned). If an item is
     * null the item will be "nullString".
     *
     * @param items
     * @return
     */
    public static Set<String> toStringSet(Collection<?> items, String nullString)
    {
        int size = items != null ? items.size() : 0;

        LinkedHashSet<String> set = new LinkedHashSet<>(size);

        if (items != null)
        {
            for (Object obj : items) {
                set.add(ObjectUtils.toString(obj, nullString));
            }
        }

        return set;
    }

    /**
     * Returns the array as a set
     */
    public static <T> Set<T> asLinkedHashSet(@Nonnull T[] array) {
        return new LinkedHashSet<>(Arrays.asList(array));
    }

    /**
     * Returns the size of the collection. If the collection is null, 0 will be returned.
     */
    public static int getSize(Collection<?> collection) {
        return collection != null ? collection.size() : 0;
    }

    /**
     * Returns true if the size of the collection is 0 or if the collection is null.
     */
    public static boolean isEmpty(Collection<?> collection) {
        return getSize(collection) == 0;
    }

    /**
     * Returns true if the size of the collection is > 0.
     */
    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    /**
     * Returns an array containing all elements contained in the collection
     */
    public static <T> T[] toArray(Collection<T> collection, Class<T> classOfT)
    {
        if (collection == null) {
            return null;
        }

        int size = collection.size();

        @SuppressWarnings("unchecked")
        T[] newArray = (T[]) Array.newInstance(classOfT, size);

        int i = 0;

        for (T element : collection)
        {
            newArray[i] = element;

            i++;
        }

        return newArray;
    }
}
