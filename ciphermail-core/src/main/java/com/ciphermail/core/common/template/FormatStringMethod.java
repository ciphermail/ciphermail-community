/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.template;

import java.util.IllegalFormatException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateModelException;

/**
 * A function for Freemarker that allows you format a string using String#format
 *
 * @author Martijn Brinkers
 *
 */
public class FormatStringMethod extends TemplateMethodModelBase
{
    private static final Logger logger = LoggerFactory.getLogger(FormatStringMethod.class);

    /*
     * Freemarker method name
     */
    public static final String METHOD_NAME = "format";

    @Override
    public Object exec(@SuppressWarnings("rawtypes") List arguments)
    throws TemplateModelException
    {
        checkMethodArgCount(arguments, 1, Integer.MAX_VALUE);

        String input = argToString(arguments.get(0));

        Object[] parameters = new String[arguments.size() - 1];

        for (int i = 1; i < arguments.size(); i++) {
            parameters[i - 1] =  argToString(arguments.get(i));
        }

        String formatted = null;

        try {
            formatted = String.format(input, parameters);
        }
        catch (IllegalFormatException e) {
            logger.error("Error formatting the string. Message: {}", e.getMessage());
        }

        return new SimpleScalar(formatted != null ? formatted : input);
    }
}