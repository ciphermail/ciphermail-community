/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.security.cms.CMSAdapterFactory;
import com.ciphermail.core.common.security.cms.CMSSignedDataAdapter;
import com.ciphermail.core.common.security.cms.CMSSignedInspector;
import com.ciphermail.core.common.security.cms.CMSSignedInspectorImpl;
import com.ciphermail.core.common.security.cms.CryptoMessageSyntaxException;
import com.ciphermail.core.common.security.cms.SignerInfo;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.mail.smime.CMSProcessableBodyPartInbound;
import org.bouncycastle.mail.smime.SMIMEException;
import org.bouncycastle.mail.smime.SMIMESigned;

import javax.annotation.Nonnull;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.OutputStream;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Objects;

public class SMIMESignedInspectorImpl implements SMIMESignedInspector
{
    private final CMSSignedInspector signedInspector;

    private MimeBodyPart unsignedContent;

    /*
     * CMSProcessable that writes the body without any additional coding except CR-LF conversion. We will use this
     * version by default because BC's version sometimes results in 'corrupt' signatures.
     */
    private static class WriteRawCMSProcessable implements CMSProcessable
    {
        private final BodyPart bodyPart;

        public WriteRawCMSProcessable(@Nonnull BodyPart bodyPart) {
            this.bodyPart = Objects.requireNonNull(bodyPart);
        }

        @Override
        public Object getContent() {
            return bodyPart;
        }

        @Override
        public void write(OutputStream output)
        throws IOException, CMSException
        {
            try {
                SMIMEUtils.writeBodyPart(bodyPart, output, "7bit");
            }
            catch (MessagingException e) {
                throw new CMSException("Error writing BodyPart.", e);
            }
        }
    }

    /*
     * A private version which will allow us to explicitly set the message part and
     * signed part.
     */
    private static class SMIMEClearSigned extends CMSSignedData
    {
        public SMIMEClearSigned(@Nonnull CMSProcessable processable, @Nonnull BodyPart signaturePart)
        throws CMSException, IOException, MessagingException
        {
            super(processable, signaturePart.getInputStream());
        }
    }

    public SMIMESignedInspectorImpl(Part signedPart, String nonSensitiveProvider, String sensitiveProvider)
    throws SMIMEInspectorException, MessagingException, IOException
    {
        this(signedPart, nonSensitiveProvider, sensitiveProvider, true);
    }

    public SMIMESignedInspectorImpl(@Nonnull Part signedPart, String nonSensitiveProvider, String sensitiveProvider,
            boolean useRaw)
    throws SMIMEInspectorException, MessagingException, IOException
    {
        try {
            signedInspector = new CMSSignedInspectorImpl(getSignedDataAdapter(signedPart, useRaw),
                    nonSensitiveProvider, sensitiveProvider);
        }
        catch (CMSException | SMIMEException e) {
            throw new SMIMEInspectorException(e);
        }
    }

    protected CMSSignedDataAdapter getSignedDataAdapter(@Nonnull Part signedPart, boolean useRaw)
    throws MessagingException, CMSException, IOException, SMIMEInspectorException, SMIMEException
    {
        CMSSignedData signed;

        Object content = signedPart.getContent();

        if (content instanceof MimeMultipart multipart)
        {
            // extract the message part and signature part
            BodyPart[] parts = SMIMEUtils.dissectSigned(multipart);

            if (parts == null) {
                throw new SMIMEInspectorException("Multipart is not valid clear signed.");
            }

            CMSProcessable processable = useRaw ? new WriteRawCMSProcessable(parts[0]) :
                new CMSProcessableBodyPartInbound(parts[0]);

            signed = new SMIMEClearSigned(processable, parts[1]);

            unsignedContent = (MimeBodyPart) parts[0];
        }
        else {
            signed = new SMIMESigned(signedPart);

            unsignedContent = ((SMIMESigned) signed).getContent();
        }

        return CMSAdapterFactory.createAdapter(signed);
    }

    @Override
    public List<SignerInfo> getSigners()
    throws CryptoMessageSyntaxException
    {
        return signedInspector.getSigners();
    }

    @Override
    public int getVersion() {
        return signedInspector.getVersion();
    }

    @Override
    public List<X509Certificate> getCertificates()
    throws CryptoMessageSyntaxException
    {
        return signedInspector.getCertificates();
    }

    @Override
    public List<X509CRL> getCRLs()
    throws CryptoMessageSyntaxException
    {
        return signedInspector.getCRLs();
    }

    @Override
    public MimeBodyPart getContent() {
        return unsignedContent;
    }

    @Override
    public MimeMessage getContentAsMimeMessage()
    throws MessagingException, IOException
    {
        return BodyPartUtils.toMessage(getContent());
    }
}
