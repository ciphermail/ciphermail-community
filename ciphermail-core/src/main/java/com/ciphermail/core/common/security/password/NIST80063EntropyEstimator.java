/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password;

import org.apache.commons.lang.StringUtils;

/**
 * Class that estimates the entropy of a string using the algorithm from NIST Special Publication 800-63.
 *
 * Note: this password strength calculation algorithm is not without flaws. However this is the best general
 * method I have found so far which also works when using very long passwords (passphrases) without requiring
 * special characters
 *
 * @author Martijn Brinkers
 *
 */
public class NIST80063EntropyEstimator
{
    private static final String UPPERCASE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWERCASE_CHARS = "abcdefghijklmnopqrstuvwxyz";
    private static final String NUMBER_CHARS = "0123456789";
    private static final String NORMAL_CHARS = UPPERCASE_CHARS + LOWERCASE_CHARS + NUMBER_CHARS + " ";

    private NIST80063EntropyEstimator() {
        // empty on purpose
    }

    private static int countMatchingChars(String input, String characters)
    {
        int count = 0;

        for (int i = 0; i < input.length(); i++)
        {
            if (characters.indexOf(input.charAt(i)) > -1) {
                count++;
            }
        }

        return count;
    }

    /**
     * Calculates the entropy of the input using the algorithm from NIST Special Publication 800-63.
     */
    public static float estimateEntropy(String input)
    {
        float entropy = 0;

        if (StringUtils.isEmpty(input)) {
            return 0;
        }

        entropy = 4;

        if (input.length() > 1)
        {
            entropy = entropy + Math.min(7, input.length() - 1) * 2;

            if (input.length() >= 9)
            {
                entropy = entropy + Math.min(12, input.length() - 8) * 1.5f;

                if (input.length() >= 21) {
                    entropy = entropy + (input.length() - 20);
                }
            }
        }

        int uppercaseCount = countMatchingChars(input, UPPERCASE_CHARS);
        int lowercaseCount = countMatchingChars(input, LOWERCASE_CHARS);
        int specialCount = input.length() - countMatchingChars(input, NORMAL_CHARS);

        if (uppercaseCount > 0 && lowercaseCount > 0 && specialCount > 0) {
            entropy = entropy + 6;
        }

        return entropy;
    }
}
