/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.asn1.x500.style.RFC4519Style;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Class used to get info on X500Principal instances.
 *
 * @author Martijn Brinkers
 *
 */
public class X500PrincipalInspector
{
    /*
     * The X500Name version of the X500Principal
     */
    private final X500Name x500Name;

    public X500PrincipalInspector(@Nonnull X500Name x500Name) {
        this.x500Name = Objects.requireNonNull(x500Name);
    }

    public X500PrincipalInspector(@Nonnull X500Principal principal)
    {
        // Convert the X500Principal to a Bouncycastle x500Name because
        // we will need some functionality of x509Principal
        this.x500Name = Objects.requireNonNull(X500PrincipalUtils.toX500Name(principal));
    }

    private String rDNToString(RDN rdn)
    {
        String result = null;

        if (rdn.isMultiValued())
        {
            // We currently do not support multi-value RDNs so if multivalued, combine them into one
            // string with +
            AttributeTypeAndValue[] values = rdn.getTypesAndValues();

            StrBuilder sb = new StrBuilder();

            for (AttributeTypeAndValue value : values)
            {
                sb.appendSeparator('+');
                sb.append(IETFUtils.valueToString(value.getValue()));

                result = sb.toString();
            }
        }
        else {
            result = IETFUtils.valueToString(rdn.getFirst().getValue());
        }

        return StringUtils.defaultString(result);
    }

    /*
     * Returns the DERObjectIdentifier element as a list of strings
     */
    public List<String> asStrings(ASN1ObjectIdentifier identifier)
    {
        List<String> result = new LinkedList<>();

        RDN[] rdns = x500Name.getRDNs(identifier);

        if (rdns != null)
        {
            for (RDN rdn : rdns)
            {
                if (rdn != null) {
                    result.add(rDNToString(rdn));
                }
            }
        }

        return result;
    }

    /**
     * Returns the X500Name object representing the name of this X500PrincipalInspector.
     *
     * @return the X500Name object representing the name of this X500PrincipalInspector
     */
    public X500Name getX500Name() {
        return x500Name;
    }

    /**
     * Returns all the EmailAddress items from the subject DN.
     * Duplicate items are possible and there is no guarantee that the
     * email addresses are valid email addresses.
     *
     * @return a list of EmailAddress strings
     */
    public List<String> getEmail() {
        return asStrings(PKCSObjectIdentifiers.pkcs_9_at_emailAddress);
    }

    /**
     * Returns a list of all organization elements from this principal.
     * All elements are returned so the list can contain duplicate elements*/
    public List<String> getOrganisation() {
        return asStrings(RFC4519Style.o);
    }

    /**
     * Returns a list of all organizational unit elements from this principal.
     * All elements are returned so the list can contain duplicate elements
     */
    public List<String> getOrganisationalUnit() {
        return asStrings(RFC4519Style.ou);
    }

    /**
     * Returns a list of all country code elements from this principal.
     * Country codes are two-letter codes (see ISO 3166)
     * All elements are returned so the list can contain duplicate elements
     */
    public List<String> getCountryCode() {
        return asStrings(RFC4519Style.c);
    }

    /**
     * Returns a list of all State elements from this principal.
     * All elements are returned so the list can contain duplicate elements
     */
    public List<String> getState() {
        return asStrings(RFC4519Style.st);
    }

    /**
     * Returns a list of all locality elements from this principal.
     * All elements are returned so the list can contain duplicate elements
     */
    public List<String> getLocality() {
        return asStrings(RFC4519Style.l);
    }

    /**
     * Returns a list of all common name elements from this principal.
     * All elements are returned so the list can contain duplicate elements
     */
    public List<String> getCommonName() {
        return asStrings(RFC4519Style.cn);
    }

    /**
     * Returns a list of all given name elements from this principal.
     * All elements are returned so the list can contain duplicate elements
     */
    public List<String> getGivenName() {
        return asStrings(RFC4519Style.givenName);
    }

    /**
     * Returns a list of all surname elements from this principal.
     * All elements are returned so the list can contain duplicate elements
     */
    public List<String> getSurname() {
        return asStrings(RFC4519Style.sn);
    }

    /**
     * Returns the Canonical String version of the X500Principal
     */
    public static String getCanonical(X500Principal principal)
    {
        if (principal == null) {
            return null;
        }

        return principal.getName(X500Principal.CANONICAL);
    }

    /**
     * Returns the friendly String version of the X500Principal
     */
    public static String getFriendly(X500Principal principal)
    {
        if (principal == null) {
            return null;
        }

        return principal.toString();
    }
}
