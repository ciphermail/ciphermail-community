/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.cms;

import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.RecipientInformationStore;

import java.io.IOException;
import java.security.AlgorithmParameters;

/**
 * Bouncycastle's CMSEnvelopedData and CMSEnvelopedDataParser do not share common interface.
 * This adapter will be used to create a wrapper around these classes to give them a common
 * interface
 *
 * @author Martijn Brinkers
 *
 */
public interface CMSEnvelopedDataAdapter
{
    /**
     * Return the object identifier for the content encryption algorithm.
     */
    String getEncryptionAlgOID();

    /**
     * Returns additional encryption algorithm parameters if available (for example key length)
     */
    byte[] getEncryptionAlgParams();

    /**
     * Return the content encryption algorithm details for the data in this object.
     */
    AlgorithmParameters getEncryptionAlgorithmParameters()
    throws CMSException;

    /**
     * Return a store of the intended recipients for this message
     */
    RecipientInformationStore getRecipientInfos();

    /**
     * Return a table of the unprotected attributes indexed by
     * the OID of the attribute.
     */
    AttributeTable getUnprotectedAttributes()
    throws IOException;
}
