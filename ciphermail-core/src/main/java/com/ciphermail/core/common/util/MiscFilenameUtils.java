/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.CharSet;

/**
 * Filename utility methods not found in Apache commons FilenameUtils
 *
 * @author Martijn Brinkers
 *
 */
public class MiscFilenameUtils
{
    private static final int MAX_FILENAME_LENGTH = 255;

    /*
     * The valid chars in a filename
     */
    private static final String ALLOWED_FILENAME_CHARS = "a-zA-Z0-9._ @=-";

    /*
     * The set with valid chars in a filename
     */
    private static final CharSet ALLOWED_FILENAME_CHARSET = CharSet.getInstance(ALLOWED_FILENAME_CHARS);

    /*
     * The chars in a filename which are forbidden
     */
    private static final String FORBIDDEN_FILENAME_CHARS = "\\/:*?\"<>|";

    /*
     * The set with forbidden chars in a filename
     */
    private static final CharSet FORBIDDEN_FILENAME_CHARSET = CharSet.getInstance(FORBIDDEN_FILENAME_CHARS);

    private MiscFilenameUtils() {
        // empty on purpose
    }

    /**
     * Allows characters for a filename which are considered safe for a filename. The white list is very
     * conservative in what it considers to be a safe char. If a char is not safe, it's encoded as %HEX
     *
     * If truncate is true and the final filename is larger then 255 characters, the filename will be truncated to 255
     * characters. If truncate is false and the final filename is larger then 255 characters, a IllegalArgumentException
     * will be thrown.
     */
    public static String safeEncodeFilenameWhitelist(String filename, boolean truncate)
    {
        if (filename == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < filename.length(); i++)
        {
            char c = filename.charAt(i);

            // If char is not from allowed charset or the first character is . encode the character
            if (!ALLOWED_FILENAME_CHARSET.contains(c) || (i == 0 && c == '.')) {
                sb.append("%").append(Integer.toHexString(c).toUpperCase());
            }
            else {
                sb.append(c);
            }
        }

        String encoded = sb.toString();

        if (!truncate && encoded.length() > MAX_FILENAME_LENGTH) {
            throw new IllegalArgumentException("Filename exceeds " + MAX_FILENAME_LENGTH);
        }

        return encoded.substring(0, Math.min(encoded.length(), MAX_FILENAME_LENGTH));
    }

    /**
     * Encodes characters from the filename which are considered not to be safe for a filename. The black list
     * is is less conservative than the white list. If a char is not safe, it's encoded as %HEX
     *
     * If truncate is true and the final filename is larger then 255 characters, the filename will be truncated to 255
     * characters. If truncate is false and the final filename is larger then 255 characters, a IllegalArgumentException
     * will be thrown.
     */
    public static String safeEncodeFilenameBlacklist(String filename, boolean truncate)
    {
        if (filename == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < filename.length(); i++)
        {
            char c = filename.charAt(i);

            // If char is forbidden, is a control char or the first character is . encode the character
            if (Character.isISOControl(c) || FORBIDDEN_FILENAME_CHARSET.contains(c) || (i == 0 && c == '.')) {
                sb.append("%").append(Integer.toHexString(c).toUpperCase());
            }
            else {
                sb.append(c);
            }
        }

        String encoded = sb.toString();

        if (!truncate && encoded.length() > MAX_FILENAME_LENGTH) {
            throw new IllegalArgumentException("Filename exceeds " + MAX_FILENAME_LENGTH);
        }

        return encoded.substring(0, Math.min(encoded.length(), MAX_FILENAME_LENGTH));
    }
}
