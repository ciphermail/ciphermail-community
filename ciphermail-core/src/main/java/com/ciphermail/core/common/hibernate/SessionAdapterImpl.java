/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * {@link SessionAdapter} implementation for a Hibernate {@link Session}.
 *
 * @author Martijn Brinkers
 *
 */
public class SessionAdapterImpl implements SessionAdapter
{
    /*
     * The Hibernate session
     */
    private final Session session;

    public SessionAdapterImpl(@Nonnull Session session) {
        this.session = Objects.requireNonNull(session);
    }

    @Override
    public <T> T get(@Nonnull Object id, @Nonnull Class<T> entityType) {
        return session.get(entityType, id);
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return session.getCriteriaBuilder();
    }

    @Override
    public <T> Query<T> createQuery(@Nonnull String query, @Nonnull Class<T> entityType) {
        return session.createQuery(query, entityType);
    }

    @Override
    public <T> Query<T> createQuery(@Nonnull CriteriaQuery<T> criteriaQuery) {
        return session.createQuery(criteriaQuery);
    }

    @Override
    public void persist(@Nonnull Object entity) {
        session.persist(entity);
    }

    @Override
    public void delete(@Nonnull Object entity) {
        session.remove(entity);
    }

    @Override
    public void evict(@Nonnull Object entity) {
        session.evict(entity);
    }

    @Override
    public void flush() {
        session.flush();
    }

    @Override
    public void clear() {
        session.clear();
    }

    @Override
    public void close() {
        session.close();
    }
}
