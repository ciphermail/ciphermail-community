/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.asn1.ASN1Encoder;
import com.ciphermail.core.common.security.asn1.DERUtils;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import org.bouncycastle.asn1.ASN1EncodableVector;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.Collections;

public class CRLEncoder
{
    private CRLEncoder() {
        // empty on purpose
    }

    /*
     * Converts to CRL to a byte array
     */
    public static byte[] encode(@Nonnull X509CRL crl, @Nonnull ObjectEncoding encoding)
    throws CRLException, IOException
    {
        return switch (encoding) {
            case DER -> crl.getEncoded();
            case PEM -> ASN1Encoder.encodePEM(Collections.singletonList(crl));
            default -> throw new IllegalArgumentException("Unknown encoding: " + encoding);
        };
    }

    /**
     * Convert the collection of CRLs to a byte array using the specified encoding.
     */
    public static byte[] encode(@Nonnull Collection<X509CRL> crls, @Nonnull ObjectEncoding encoding)
    throws CRLException, IOException
    {
        if (crls.isEmpty()) {
            throw new IllegalArgumentException("CRLs are missing");
        }

        return switch (encoding) {
            case DER -> encodeDER(crls);
            case PEM -> ASN1Encoder.encodePEM(crls);
            default -> throw new IllegalArgumentException("Unknown encoding: " + encoding);
        };
    }

    private static byte[] encodeDER(@Nonnull Collection<X509CRL> crls)
    throws IOException, CRLException
    {
        if (crls.size() > 1) {
            // if there are multiple CRLs, PKC7 encode otherwise single encoded
            ASN1EncodableVector asn1CRLs = new ASN1EncodableVector();

            for (X509CRL crl : crls) {
                asn1CRLs.add(DERUtils.toDERObject(crl));
            }

            return ASN1Encoder.encodePKCS7(null /* no certs */, asn1CRLs);
        }
        else {
            return crls.iterator().next().getEncoded();
        }
    }
}
