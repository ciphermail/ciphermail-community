/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.mail.matcher.ContentHeaderNameMatcher;
import com.ciphermail.core.common.util.FileConstants;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.DeferredFileOutputStream;
import org.apache.commons.lang.CharSetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;
import javax.mail.util.ByteArrayDataSource;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

public class MailUtils
{
    private static final Logger logger = LoggerFactory.getLogger(MailUtils.class);

    public static final byte[] CRLF_BYTES = new byte[] {'\r', '\n'};

    /*
     * Remove all control chars but keep CR, LF and TAB
     */
    /*
     * Characters which will be removed from the subject. Remove all control chars but keep CR, LF and TAB
     */
    private static final String MAIL_UNSAFE_CHARS = "\u0000-\u0008\u000B\u000C\u000E-\u001F";

    /*
     * Throw away OutputStream
     */
    private static class Bitsink extends OutputStream
    {
        @Override
        public void write(int ignore) {}
    }

    private static class Invalid7BitCharsException extends IOException
    {
        // Empty on purpose. Used as a marker Exception
    }

    /*
     * OutputStream which checks if the input is valid for 7 bit email
     */
    private static class CheckForInvalid7BitCharsOutputStream extends OutputStream
    {
        @Override
        public void write(int b)
        throws IOException
        {
            if (b > 127) {
                throw new Invalid7BitCharsException();
            }
            if (b < 32 && b != '\t' && b != '\n' && b != '\r') {
                throw new Invalid7BitCharsException();
            }
        }
    }

    public static void writeMessageRaw(@Nonnull MimePart part, @Nonnull OutputStream output)
    throws IOException, MessagingException
    {
        @SuppressWarnings("rawtypes")
        Enumeration lines = part.getAllHeaderLines();

        // step through all header lines
        while (lines.hasMoreElements())
        {
            String header = (String)lines.nextElement();

            output.write(MiscStringUtils.getBytesASCII(header));
            output.write(CRLF_BYTES);
        }

        output.write(CRLF_BYTES);

        InputStream input;

        // If message is created from a stream Javamail will return the raw stream. If
        // the message is created from 'scratch' getRawInputStream throws a
        // MessagingException. We will therefore first try the raw version and if
        // that fails we will try getInputStream.
        //
        // Only MimeMessage has getRawInputStream so we check whether the part
        // is a MimeMessage
        if (part instanceof MimeMessage mimeMessage)
        {
            try {
                input = mimeMessage.getRawInputStream();
            }
            catch(MessagingException e) {
                // Could be that we are using Javamail 1.3 which does not support
                // getRawInputStream. Use the inputStream instead
                input = part.getInputStream();
            }
        }
        else {
            input = part.getInputStream();
        }

        IOUtils.copy(input, output);
    }

    /**
     * Helper method to convert the Part to a MIME encoded String
     */
    public static String partToMimeString(@Nonnull Part part)
    throws MessagingException, IOException
    {
       ByteArrayOutputStream bos = new ByteArrayOutputStream();

       writeMessage(part, bos);

       return MiscStringUtils.toStringFromUTF8Bytes(bos.toByteArray());
    }

    /**
     * Saves the MimePart to the output stream
     * @param part
     * @param output
     * @throws IOException
     * @throws MessagingException
     */
    public static void writeMessage(@Nonnull Part part, @Nonnull OutputStream output)
    throws IOException, MessagingException
    {
        // we need to store the result in a temporary buffer so we can fall back
        // on a different procedure when writeTo fails. If not, MimePart#writeTo might
        // fail halfway and some data has already been written to output.
        DeferredFileOutputStream buffer = DeferredFileOutputStream.builder()
                .setThreshold(SizeUtils.MB)
                .setPrefix(FileConstants.TEMP_FILE_PREFIX).get();

        // First try the writeTo method. Sometimes this will fail when the message uses
        // an unsupported or corrupted encoding. For example, the content encoding says that
        // the message is base64 encoded, but it's not.
        try {
            part.writeTo(buffer);

            // Need to close the DeferredFileOutputStream before we can write
            buffer.close();

            // writeTo was successful so we can copy the result to the final output
            buffer.writeTo(output);
        }
        catch(IOException | MessagingException e)
        {
            if (part instanceof MimePart mimePart) {
                writeMessageRaw(mimePart, output);
            }
            else {
                throw e;
            }
        }
        finally {
            // Delete any possible temp file used by DeferredFileOutputStream.
            FileUtils.deleteQuietly(buffer.getFile());
        }
    }

    public static void writeMessage(@Nonnull MimePart part, @Nonnull File outputFile)
    throws IOException, MessagingException
    {
        FileOutputStream output = new FileOutputStream(outputFile);

        try {
            writeMessage(part, output);
        }
        finally {
            output.close();
        }
    }

    /**
     * Loads a message from the given file using the default mail session
     * @param file file containing the RFC822 message source
     * @return the loaded file
     * @throws FileNotFoundException
     * @throws MessagingException
     */
    public static MimeMessage loadMessage(@Nonnull File file)
    throws FileNotFoundException, MessagingException
    {
        InputStream input = new BufferedInputStream(new FileInputStream(file));

        try {
            return loadMessage(input);
        }
        finally {
            IOUtils.closeQuietly(input);
        }
    }

    /**
     * Loads a message from the input stream using the default mail session
     * @param input
     * @return
     * @throws MessagingException
     */
    public static MimeMessage loadMessage(@Nonnull InputStream input)
    throws MessagingException
    {
        return new MimeMessage(MailSession.getDefaultSession(), input);
    }

    /**
     * Checks whether the message can be converted to RFC2822 raw message source. Messages that
     * contain unsupported encoding types, corrupts content transfer encoding etc. will result
     * in a MessagingException or IOException.
     * @param message
     * @throws MessagingException
     * @throws IOException
     */
    public static void validateMessage(@Nonnull MimeMessage message)
    throws MessagingException, IOException
    {
        message.writeTo(new Bitsink());
    }

    /**
     * Return false if the message is not a valid message
     */
    public static boolean isValidMessage(@Nonnull MimeMessage message)
    {
        try {
            message.writeTo(new Bitsink());

            return true;
        }
        catch (IOException | MessagingException e) {
            // Ignore
        }

        return false;
    }

    /**
     * Creates a new MimeMessage which is a duplicate of the source message.
     */
    public static MimeMessage cloneMessage(@Nonnull MimeMessage sourceMessage)
    throws MessagingException
    {
        return new MimeMessage(sourceMessage);
    }

    /**
     * Creates a new MimeMessage which is a duplicate of the source message but the resulting message will
     * always use the message-id (i.e., saving the clone does not result in a new message-id)
     */
    public static MimeMessage cloneMessageWithFixedMessageID(@Nonnull MimeMessage sourceMessage)
    throws MessagingException
    {
        return new MimeMessageWithID(sourceMessage, sourceMessage.getMessageID());
    }

    /**
     * Returns the Part as a byte array. The Part is converted to a byte array using MimeMessage.writeTo.
     * If a message contains a corrupt body (like incorrect base64) writeTo can throw an exception.
     */
    public static byte[] partToByteArray(@Nonnull Part part)
    throws IOException, MessagingException
    {
        // create a buffer with some 'guess' of the size plus some constant value. If size is unknown size = -1 so
        // the buffer will be 1023.
        ByteArrayOutputStream bos = new ByteArrayOutputStream(part.getSize() + 1024);

        part.writeTo(bos);

        return bos.toByteArray();
    }

    /**
     * Converts the bytes to a MimeMessage
     * @throws MessagingException
     */
    public static MimeMessage byteArrayToMessage(@Nonnull byte[] bytes)
    throws MessagingException
    {
        return loadMessage(new ByteArrayInputStream(bytes));
    }

    /**
     * Checks if the MimeMessage contains 8-bit characters or characters which are not valid in normal emails.
     * Returns true if the message contains character outside the range [#09 (TAB), #10 (LF), #13 (CR), #32-#126]
     * @throws MessagingException
     * @throws IOException
     *
     */
    public static boolean containsInvalid7BitChars(@Nonnull MimeMessage message)
    throws IOException, MessagingException
    {
        boolean result = false;

        try {
            message.writeTo(new CheckForInvalid7BitCharsOutputStream());
        }
        catch (Invalid7BitCharsException e) {
            result = true;
        }

        return result;
    }

    /**
     * Converts any 8bit encoded parts to 7bit. Returns true if a part (or all parts) were converted from
     * 8bit to 7bit.
     *
     * @param part
     * @throws MessagingException
     */
    public static boolean convertTo7Bit(@Nonnull MimePart part)
    throws MessagingException, IOException
    {
        boolean converted = false;

        if (part.isMimeType("multipart/*"))
        {
            Multipart parts = (Multipart) part.getContent();

            int count = parts.getCount();

            for (int i = 0; i < count; i++)
            {
                boolean partConverted = convertTo7Bit((MimePart) parts.getBodyPart(i));

                if (partConverted) {
                    converted = true;
                }
            }
        }
        else if ("8bit".equalsIgnoreCase(part.getEncoding()))
        {
            logger.debug("Converting part from 8bit to 7bit");

            String encoding = part.isMimeType("text/*") ? "quoted-printable" : "base64";

            // We need to use a ByteArrayDataSource to make sure it will always be encoded
            part.setDataHandler(new DataHandler(new ByteArrayDataSource(part.getInputStream(),
                    part.getContentType())));

            part.setHeader("Content-Transfer-Encoding", encoding);
            part.addHeader("X-MIME-Autoconverted", "from 8bit to 7bit by CipherMail");

            converted = true;
        }

        return converted;
    }

    /**
     * Return True if a MIME part is 8bit encoded
     *
     * @param part
     * @throws MessagingException
     */
    public static boolean contains8BitMimePart(@Nonnull MimePart part)
    throws MessagingException, IOException
    {
        if (part.isMimeType("multipart/*"))
        {
            Multipart parts = (Multipart) part.getContent();

            int count = parts.getCount();

            for (int i = 0; i < count; i++)
            {
                if (contains8BitMimePart((MimePart) parts.getBodyPart(i))) {
                    return true;
                }
            }
        }
        else if ("8bit".equalsIgnoreCase(part.getEncoding()))
        {
            logger.debug("MIME part is 8bit encoded");

            return true;
        }

        return false;
    }

    /**
     * Adds the messageToAdd to message as an RFC822 attachment.
     *
     * @return a new MimeMessage instance withe the messageToAdd attached
     * @throws MessagingException
     * @throws IOException
     */
    public static MimeMessage attachMessageAsRFC822(@Nonnull MimeMessage message, @Nonnull MimeMessage messageToAdd,
        @Nonnull String nameOfAttachedMessage)
    throws MessagingException, IOException
    {
        MimeMessage newMessage = new MimeMessageWithID(message, message.getMessageID());

        MimeBodyPart sourceRFC822 = BodyPartUtils.toRFC822(messageToAdd, nameOfAttachedMessage, Part.ATTACHMENT);

        Multipart mainPart = new MimeMultipart();

        if (message.isMimeType("multipart/mixed"))
        {
            // Message is already multipart/mixed so we need to add the existing bodyparts
            Multipart sourcePart = (Multipart) message.getContent();

            for (int i = 0; i < sourcePart.getCount(); i++) {
                mainPart.addBodyPart(sourcePart.getBodyPart(i));
            }
        }
        else {
            // Message it not multipart/mixed so we need to create a bodypart from the message and add
            mainPart.addBodyPart(BodyPartUtils.makeContentBodyPart(message, new ContentHeaderNameMatcher()));
        }

        mainPart.addBodyPart(sourceRFC822);

        newMessage.setContent(mainPart);

        newMessage.saveChanges();

        return newMessage;
    }

    /**
     * Returns the message-id without throwing and exception. If the message-id cannot be retrieved or not available
     * defaultValue will be returned.
     */
    public static String getMessageIDQuietly(MimeMessage message, String defaultValue)
    {
        String messageID = null;

        try {
            if (message != null) {
                messageID = message.getMessageID();
            }
        }
        catch (Exception e) {
            // Ignore
        }

        if (messageID == null) {
            messageID = defaultValue;
        }

        return messageID;
    }

    /**
     * Returns the message-id without throwing and exception. If the message-id cannot be retrieved or not available
     * null will be returned.
     */
    public static String getMessageIDQuietly(MimeMessage message) {
        return getMessageIDQuietly(message, null);
    }

    /**
     * Returns the subject with null and ESC chars removed
     * @throws MessagingException
     */
    public static String getSafeSubject(MimeMessage message)
    throws MessagingException
    {
        if (message == null) {
            return null;
        }

        return deleteUnsafeChars(message.getSubject());
    }

    /**
     * Removes chars from the input which are not considered to be safe for mail, i.e., null value, control chars
     */
    public static String deleteUnsafeChars(String input) {
        return CharSetUtils.delete(input, MAIL_UNSAFE_CHARS);
    }
}
