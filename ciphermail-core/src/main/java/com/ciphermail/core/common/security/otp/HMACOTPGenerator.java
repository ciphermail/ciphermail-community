/*
 * Copyright (c) 2011-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.otp;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.Base32Utils;
import org.apache.commons.lang.ArrayUtils;

import javax.annotation.Nonnull;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.util.Objects;

/**
 * HMAC based OTPGenerator implementation
 *
 * @author Martijn Brinkers
 *
 */
public class HMACOTPGenerator implements OTPGenerator
{
    /*
     * The HMAC algorithm to use
     */
    private final String algorithm;

    /*
     * Factory for creating security class instances
     */
    private final SecurityFactory securityFactory;

    public HMACOTPGenerator(@Nonnull String algorithm)
    {
        this.algorithm = Objects.requireNonNull(algorithm);

        securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }

    private Mac createMAC(byte[] secret)
    throws GeneralSecurityException
    {
        Mac mac = securityFactory.createMAC(algorithm);

        SecretKeySpec keySpec = new SecretKeySpec(secret, "raw");

        mac.init(keySpec);

        return mac;
    }

    @Override
    public @Nonnull String generate(@Nonnull byte[] secret, @Nonnull byte[] counter, int byteLength)
    throws GeneralSecurityException
    {
        Mac mac = createMAC(secret);

        // Convert the password to base32 to make it easier for end users to read and
        // limit the number of bytes to make the password not too long
        return Base32Utils.base32Encode(ArrayUtils.subarray(mac.doFinal(counter), 0, byteLength));
    }
}
