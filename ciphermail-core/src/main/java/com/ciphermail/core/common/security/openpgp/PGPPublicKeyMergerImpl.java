/*
 * Copyright (c) 2014-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.ByteArray;
import org.apache.commons.lang.ArrayUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Implementation of PGPPublicKeyMerger
 *
 * @author Martijn Brinkers
 *
 */
public class PGPPublicKeyMergerImpl implements PGPPublicKeyMerger
{
    private static final Logger logger = LoggerFactory.getLogger(PGPPublicKeyMergerImpl.class);

    /*
     * If true, user IDs will not be merged.
     */
    private boolean skipUserIDs;

    @Override
    public PGPPublicKey merge(PGPPublicKey existingKey, PGPPublicKey newKey)
    throws PGPException
    {
        if (existingKey == null || newKey == null) {
            // Nothing to add
            return null;
        }

        // Only keys with matching key IDs can be merged
        if (existingKey.getKeyID() != newKey.getKeyID())
        {
            throw new PGPException("Key ID " + PGPUtils.getKeyIDHex(existingKey.getKeyID()) +
                    " does not match Key ID " + PGPUtils.getKeyIDHex(newKey.getKeyID()));
        }

        // We will store the existing signatures in a set so we can check whether there are any
        // new signatures to add
        Set<ByteArray> encodedSignatures = getExistingEncodedSignatures(existingKey);

        List<PGPSignature> keySignaturesToAdd = new LinkedList<>();

        List<PGPSignature> signatures =  PGPSignatureInspector.getSignatures(newKey);

        for (PGPSignature signature : signatures)
        {
            logger.debug("New signature type {}", signature.getSignatureType());

            try {
                if (!encodedSignatures.contains(new ByteArray(signature.getEncoded())))
                {
                    logger.info("New signature found for key with key ID {}. Signature type {}",
                            PGPUtils.getKeyIDHex(existingKey.getKeyID()), signature.getSignatureType());

                    if (ArrayUtils.contains(PGPSignatureInspector.KEY_SIGNATURES, signature.getSignatureType())) {
                        keySignaturesToAdd.add(signature);
                    }
                }
                else {
                    logger.debug("Signature was already part of existing key.");
                }
            }
            catch (Exception e) {
                logger.error("Error encoding signature", e);
            }
        }

        // Map from User ID to signatures to add
        Map<ByteArray, List<PGPSignature>> userIDSignaturesToAdd = new LinkedHashMap<>();

        if (!skipUserIDs)
        {
            // Merge User IDs
            for (byte[] userID : PGPPublicKeyInspector.getUserIDs(newKey))
            {
                signatures = PGPSignatureInspector.getSignaturesForUserID(newKey, userID);

                for (PGPSignature signature : signatures)
                {
                    if (ArrayUtils.contains(PGPSignatureInspector.USER_ID_CERTIFICATION_WITH_REVOCATION_TYPES,
                            signature.getSignatureType()))
                    {
                        try {
                            if (!encodedSignatures.contains(new ByteArray(signature.getEncoded())))
                            {
                                // A new User ID signature
                                List<PGPSignature> sigsForUserID = userIDSignaturesToAdd.get(
                                        new ByteArray(userID));

                                if (sigsForUserID == null)
                                {
                                    sigsForUserID = new LinkedList<>();
                                    userIDSignaturesToAdd.put(new ByteArray(userID), sigsForUserID);
                                }

                                sigsForUserID.add(signature);
                            }
                        }
                        catch (Exception e) {
                            logger.error("Error encoding signature", e);
                        }
                    }
                }
            }
        }

        PGPPublicKey merged = null;

        // Add the new key signatures.
        if (!keySignaturesToAdd.isEmpty())
        {
            merged = existingKey;

            for (PGPSignature signature : keySignaturesToAdd) {
                merged = PGPPublicKey.addCertification(merged, signature);
            }
        }

        // Add the new User ID signatures.
        if (userIDSignaturesToAdd.size() > 0)
        {
            merged = existingKey;

            for (Entry<ByteArray, List<PGPSignature>> entry : userIDSignaturesToAdd.entrySet())
            {
                for (PGPSignature signature : entry.getValue()) {
                    merged = PGPPublicKey.addCertification(merged, entry.getKey().getBytes(), signature);
                }
            }
        }

        // Merging should not change the key ID
        if (merged != null && existingKey.getKeyID() != merged.getKeyID())
        {
            throw new PGPException("Key ID " + PGPUtils.getKeyIDHex(existingKey.getKeyID()) +
                    " does not match the merged Key ID " + PGPUtils.getKeyIDHex(merged.getKeyID()));
        }

        return merged;
    }

    @Override
    public PGPPublicKey merge(PGPPublicKey existingKey, PGPSignatureList signatureList)
    throws PGPException
    {
        if (existingKey == null || signatureList == null || signatureList.size() == 0) {
            // Nothing to add
            return null;
        }

        // store the existing signatures in a set so we can check whether there are any
        // new signatures to add
        Set<ByteArray> encodedSignatures = getExistingEncodedSignatures(existingKey);

        List<PGPSignature> keySignaturesToAdd = new LinkedList<>();

        for (int i = 0; i < signatureList.size(); i++)
        {
            PGPSignature signature = signatureList.get(i);

            logger.debug("New signature type {}", signature.getSignatureType());

            try {
                // only accept the signature if it was signed by the key
                if (existingKey.getKeyID() == signature.getKeyID())
                {
                    if (!encodedSignatures.contains(new ByteArray(signature.getEncoded())))
                    {
                        logger.info("New signature found for key with key ID {}.",
                                PGPUtils.getKeyIDHex(existingKey.getKeyID()));

                        // Only accept key signatures
                        if (ArrayUtils.contains(PGPSignatureInspector.KEY_SIGNATURES, signature.getSignatureType())) {
                            keySignaturesToAdd.add(signature);
                        }
                    }
                    else {
                        logger.debug("Signature was already part of existing key.");
                    }
                }
                else {
                    logger.warn("Signature was signed by a key with key ID {} and not by a key with Key ID {}.",
                            PGPUtils.getKeyIDHex(signature.getKeyID()), PGPUtils.getKeyIDHex(existingKey.getKeyID()));
                }
            }
            catch (Exception e) {
                logger.error("Error encoding signature", e);
            }
        }

        PGPPublicKey merged = null;

        // Add the new key signatures.
        if (!keySignaturesToAdd.isEmpty())
        {
            merged = existingKey;

            for (PGPSignature signature : keySignaturesToAdd) {
                merged = PGPPublicKey.addCertification(merged, signature);
            }
        }

        // Merging should not change the key ID
        if (merged != null && existingKey.getKeyID() != merged.getKeyID())
        {
            throw new PGPException("Key ID " + PGPUtils.getKeyIDHex(existingKey.getKeyID()) +
                    " does not match the merged Key ID " + PGPUtils.getKeyIDHex(merged.getKeyID()));
        }

        return merged;
    }

    private Set<ByteArray> getExistingEncodedSignatures(PGPPublicKey key)
    {
        // store the existing signatures in a set so we can check whether there are any
        // * new signatures to add
        Set<ByteArray> encodedSignatures = new HashSet<>();

        List<PGPSignature> signatures =  PGPSignatureInspector.getSignatures(key);

        for (PGPSignature signature : signatures)
        {
            logger.debug("Existing signature type {}", signature.getSignatureType());

            try {
                encodedSignatures.add(new ByteArray(signature.getEncoded()));
            }
            catch (Exception e) {
                logger.error("Error encoding signature", e);
            }
        }

        return encodedSignatures;
    }

    public boolean isSkipUserIDs() {
        return skipUserIDs;
    }

    public void setSkipUserIDs(boolean skipUserIDs) {
        this.skipUserIDs = skipUserIDs;
    }
}
