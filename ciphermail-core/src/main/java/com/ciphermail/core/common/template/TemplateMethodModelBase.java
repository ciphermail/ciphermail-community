/*
 * Copyright (c) 2017-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.template;

import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Can be used as a base to create a new TemplateMethodModelEx
 */
public abstract class TemplateMethodModelBase implements TemplateMethodModelEx
{
    /*
     * The date format to use when converting from date to String
     */
    private static final String DATE_FORMAT = "dd-MMM-yyyy HH:mm:ss z";

    protected final void checkMethodArgCount(List<?> args, int expectedCnt)
    throws TemplateModelException
    {
        checkMethodArgCount(args != null ? args.size() : 0, expectedCnt);
    }

    protected final void checkMethodArgCount(int argCnt, int expectedCnt)
    throws TemplateModelException
    {
        if (argCnt != expectedCnt)
        {
            throw new TemplateModelException("Expected " + expectedCnt + " arguments but got " + argCnt +
                    " arguments");
        }
    }

    protected final void checkMethodArgCount(List<?> args, int minCnt, int maxCnt)
    throws TemplateModelException
    {
        checkMethodArgCount(args != null ? args.size() : 0, minCnt, maxCnt);
    }

    protected final void checkMethodArgCount(int argCnt, int minCnt, int maxCnt)
    throws TemplateModelException
    {
        if (argCnt < minCnt || argCnt > maxCnt)
        {
            throw new TemplateModelException("Expected min " + minCnt + " or max " + maxCnt + " arguments but got " +
                    argCnt + " arguments");
        }
    }

    protected String dateToString(Date date) {
        return new SimpleDateFormat(DATE_FORMAT).format(date);
    }

    protected String argToString(Object arg)
    throws TemplateModelException
    {
        if (arg instanceof TemplateScalarModel templateScalarModel) {
            return templateScalarModel.getAsString();
        }
        else if (arg instanceof TemplateNumberModel templateNumberModel) {
            return templateNumberModel.getAsNumber().toString();
        }
        else if (arg instanceof TemplateBooleanModel templateBooleanModel) {
            return BooleanUtils.toStringTrueFalse((templateBooleanModel).getAsBoolean());
        }
        else if (arg instanceof TemplateDateModel templateDateModel) {
            return dateToString((templateDateModel).getAsDate());
        }

        return ObjectUtils.toString(arg);
    }

    protected int argToInt(Object arg)
    throws TemplateModelException
    {
        try {
            return Integer.parseInt(argToString(arg));
        }
        catch (NumberFormatException e) {
            throw new TemplateModelException(arg + " is not an Integer but a  " + arg.getClass());
        }
    }
}
