/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.provider;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.certstore.jce.X509CertStoreProvider;
import com.ciphermail.core.common.security.keystore.jce.KeyStoreHibernate;
import com.ciphermail.core.common.security.openpgp.PGPCertificateFactory;
import com.ciphermail.core.common.security.password.PBEncryptionImpl;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;

/**
 * A Java security provider so we can register our own extensions like the DatabaseKeyStore.
 *
 * This providers name is "ciphermail".
 *
 * @author Martijn Brinkers
 *
 */
public class CipherMailProvider extends Provider
{
    public static final String PROVIDER             = "ciphermail";
    public static final String DATABASE_KEYSTORE    = "DatabaseKeyStore";
    public static final String DATABASE_CERTSTORE   = "DatabaseCertStore";
    public static final String PGP_CERTIFICATE_TYPE = "PGP";

    /*
     * The global CipherMailProvider instance
     */
    private static CipherMailProvider cipherMailProvider;

    /*
     * Manages database sessions
     */
    private static SessionManager sessionManager;

    /*
     * Local extension of KeyStoreHibernate because it requires the session source
     * and a password encryptor.
     */
    public static class KeyStoreHibernateImpl extends KeyStoreHibernate
    {
        public KeyStoreHibernateImpl()
        throws NoSuchAlgorithmException, NoSuchProviderException
        {
            super(sessionManager, new PBEncryptionImpl());
        }
    }

    public CipherMailProvider()
    {
        super(PROVIDER, "1.0", "CipherMail JCE provider.");

        // register the JCE classes
        put("KeyStore."  + DATABASE_KEYSTORE, KeyStoreHibernateImpl.class.getName());
        put("CertStore." + DATABASE_CERTSTORE, X509CertStoreProvider.class.getName());

        put("CertificateFactory.PGP", PGPCertificateFactory.class.getName());
        put("Alg.Alias.CertificateFactory.PGP", PGP_CERTIFICATE_TYPE);
    }

    /**
     * Initializes this provider. Must be called once before use.
     *
     * @param sessionSource
     */
    public static synchronized CipherMailProvider initialize(SessionManager sessionManager)
    {
        setSessionManager(sessionManager);

        if (cipherMailProvider == null)
        {
            cipherMailProvider = new CipherMailProvider();

            Security.addProvider(cipherMailProvider);
        }

        return cipherMailProvider;
    }

    public static void setSessionManager(SessionManager sessionManager)
    {
        if (sessionManager != null) {
            CipherMailProvider.sessionManager = sessionManager;
        }
    }

    public static Provider getProvider() {
        return cipherMailProvider;
    }
}
