/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.ArrayUtils;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * Array utility functions.
 *
 * @author Martijn Brinkers
 *
 */
public class MiscArrayUtils
{
    private MiscArrayUtils() {
        // empty on purpose
    }

    /**
     * Compares the two arrays. If the sizes are different it is assumed that the shortest array
     * is padded with 0's.
     * @param a
     * @param b
     * @return
     */
    public static int compareArray(@Nonnull byte[] a, @Nonnull byte[] b)
    {
         if (a.length < b.length)
         {
             for (int i = 0; i != a.length; i++)
             {
                 int    l = a[i] & 0xff;
                 int    r = b[i] & 0xff;

                 if (r > l)
                 {
                     return -1;
                 }
                 else if (l > r)
                 {
                     return 1;
                 }
             }

             return -1;
         }
         else
         if (a.length > b.length)
         {
             for (int i = 0; i != b.length; i++)
             {
                 int    l = a[i] & 0xff;
                 int    r = b[i] & 0xff;

                 if (r > l)
                 {
                     return -1;
                 }
                 else if (l > r)
                 {
                     return 1;
                 }
             }

             return 1;
         }
         else {
             for (int i = 0; i != b.length; i++)
             {
                 int    l = a[i] & 0xff;
                 int    r = b[i] & 0xff;

                 if (r > l)
                 {
                     return -1;
                 }
                 else if (l > r)
                 {
                     return 1;
                 }
             }

             return 0;
         }
    }

    /**
     * Encodes the byte array to a String consisting of all readable characters.
     */
    public static String toMaxRadix(@Nonnull  byte[] bytes)
    {
        // We need to make sure that the BigInteger will be positive and that any starting zero (0) bytes
        // are not removed.
        byte[] pos = ArrayUtils.addAll(new byte[]{1}, bytes);

        BigInteger bigInt = new BigInteger(pos);

        return bigInt.toString(Character.MAX_RADIX);
    }

    /**
     * Converts the input string which is encoded in MAX_RADIX to a byte array (this function
     * is the complement of ArrayUtils#toMaxRadix)
     */
    public static byte[] fromMaxRadix(@Nonnull String inputMaxRadix)
    {
        BigInteger bigInt = new BigInteger(inputMaxRadix, Character.MAX_RADIX);

        byte[] bytes = bigInt.toByteArray();

        // We need to remove the first byte added by toMaxRadix.
        return ArrayUtils.subarray(bytes, 1, bytes.length);
    }

    /**
     * Clears the byte array (can be used for example to clear out passwords)
     */
    public static char[] clear(char[] array)
    {
        if (array != null) {
            Arrays.fill(array, (char) 0);
        }

        return array;
    }

    /**
     * Returns the object at the given index. If the index is out of bounds null will be returned.
     */
    public static <T> T safeGet(T[] array, int index)
    {
        if (array == null) {
            return null;
        }

        if (index < 0 || index > array.length - 1) {
            return null;
        }

        return array[index];
    }
}
