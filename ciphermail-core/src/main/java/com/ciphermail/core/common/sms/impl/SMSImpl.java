/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.impl;

import com.ciphermail.core.common.sms.SMS;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * Default implementation of an SMS.
 *
 * @author Martijn Brinkers
 *
 */
public class SMSImpl implements SMS
{
    /*
     * The unique ID of the SMS
     */
    private final UUID id;

    /*
     * The telephone number to which the SMS should be delivered
     */
    private final String phoneNumber;

    /*
     * The message payload of the SMS
     */
    private final String message;

    /*
     * The date the SMS was created
     */
    private final Date created;

    /*
     * The date the last time a attempt was made to deliver the SMS
     */
    private final Date lastTry;

    /*
     * The error message if an error occurred while delivering the message
     */
    private final String lastError;

    public SMSImpl(@Nonnull UUID id, @Nonnull String phoneNumber, @Nonnull String message, Date created,
            Date lastTry, String lastError)
    {
        this.id = id;
        this.phoneNumber = Objects.requireNonNull(phoneNumber);
        this.message = Objects.requireNonNull(message);
        this.created = created;
        this.lastTry = lastTry;
        this.lastError = lastError;
    }

    @Override
    public UUID getID() {
        return id;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public Date getDateCreated() {
        return created;
    }

    @Override
    public Date getDateLastTry() {
        return lastTry;
    }

    @Override
    public String getLastError() {
        return lastError;
    }
}
