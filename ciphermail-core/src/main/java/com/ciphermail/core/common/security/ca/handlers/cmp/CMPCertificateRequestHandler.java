/*
 * Copyright (c) 2021-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.handlers.cmp;

import com.ciphermail.core.common.http.AbstractVoidBinResponseConsumer;
import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactory;
import com.ciphermail.core.common.http.HttpClientContextFactory;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.KeyAndCertificateWithAdditonalCertsImpl;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.ca.CAException;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.CertificateRequestHandler;
import com.ciphermail.core.common.security.certificate.X500PrincipalUtils;
import com.ciphermail.core.common.security.crypto.RandomGenerator;
import com.ciphermail.core.common.util.LimitReachedException;
import com.ciphermail.core.common.util.MiscClassUtils;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleRequestBuilder;
import org.apache.hc.client5.http.entity.EntityBuilder;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ConnectionClosedException;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.nio.entity.BasicAsyncEntityProducer;
import org.apache.hc.core5.http.nio.support.BasicRequestProducer;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.cmp.CMPCertificate;
import org.bouncycastle.asn1.cmp.CertOrEncCert;
import org.bouncycastle.asn1.cmp.CertRepMessage;
import org.bouncycastle.asn1.cmp.CertResponse;
import org.bouncycastle.asn1.cmp.CertifiedKeyPair;
import org.bouncycastle.asn1.cmp.ErrorMsgContent;
import org.bouncycastle.asn1.cmp.PKIBody;
import org.bouncycastle.asn1.cmp.PKIFreeText;
import org.bouncycastle.asn1.cmp.PKIStatusInfo;
import org.bouncycastle.asn1.crmf.CertReqMessages;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.cmp.CMPException;
import org.bouncycastle.cert.cmp.GeneralPKIMessage;
import org.bouncycastle.cert.cmp.ProtectedPKIMessage;
import org.bouncycastle.cert.cmp.ProtectedPKIMessageBuilder;
import org.bouncycastle.cert.crmf.CRMFException;
import org.bouncycastle.cert.crmf.CertificateRequestMessage;
import org.bouncycastle.cert.crmf.CertificateRequestMessageBuilder;
import org.bouncycastle.cert.crmf.PKMACBuilder;
import org.bouncycastle.cert.crmf.jcajce.JcePKMACValuesCalculator;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.MacCalculator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CMPCertificateRequestHandler implements CertificateRequestHandler
{
    private static final Logger logger = LoggerFactory.getLogger(CMPCertificateRequestHandler.class);

    private static final String TIMEOUT_ERROR = "A timeout has occurred connecting to: ";
    private static final String MAX_RESPONE_SIZE_EXCEEDED_ERROR = "Max HTTP response size exceeded.";

    private static final String REQUEST_HANDLER_NAME = "CMP";

    /*
     * Maximum allowed size of a response from the API.
     */
    private int maxResponseSize = SizeUtils.MB * 5;

    /*
     * Max time the request may take
     */
    private long totalTimeout = 5 * DateUtils.MILLIS_PER_MINUTE;

    /*
     * Creates CloseableHttpAsyncClient instances
     */
    private final CloseableHttpAsyncClientFactory httpClientFactory;

    /*
     * Creates HttpClientContextFactory instances
     */
    private final HttpClientContextFactory httpClientContextFactory;

    /*
     * The HTTP client instance responsible for HTTP(s) communication
     */
    private CloseableHttpAsyncClient sharedHttpClient;

    /*
     * Provides the settings (like URL, password etc.) for the CMP request handler.
     */
    private final CMPSettingsProvider settingsProvider;

    /*
     * Creates security related instances
     */
    private final SecurityFactory securityFactory;

    public CMPCertificateRequestHandler(
            @Nonnull CMPSettingsProvider settingsProvider,
            @Nonnull CloseableHttpAsyncClientFactory httpClientFactory,
            @Nonnull HttpClientContextFactory httpClientContextFactory)
    {
        this.settingsProvider = Objects.requireNonNull(settingsProvider);
        this.httpClientFactory = Objects.requireNonNull(httpClientFactory);
        this.httpClientContextFactory = Objects.requireNonNull(httpClientContextFactory);

        this.securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }

    private synchronized CloseableHttpAsyncClient getHTTPClient()
    throws IOException, GeneralSecurityException
    {
        if (sharedHttpClient == null) {
            sharedHttpClient = httpClientFactory.createClient();
        }

        return sharedHttpClient;
    }

    private CMPSettings getCMPSettings()
    throws CAException
    {
        try {
            return settingsProvider.getSettings();
        }
        catch (IOException e) {
            throw new CAException(e);
        }
    }

    @Override
    public boolean isEnabled()
    {
        boolean enabled = false;

        CMPSettings settings;

        try {
            settings = getCMPSettings();

            enabled = settings.getCMPServiceURL() != null && settings.getIssuerDN() != null &&
                    settings.getMessageProtectionPassword() != null;
        }
        catch (CAException e) {
            logger.error("Error getting settings", e);
        }

        return enabled;
    }

    @Override
    public boolean isInstantlyIssued() {
        return true;
    }

    @Override
    public String getCertificateHandlerName() {
        return REQUEST_HANDLER_NAME;
    }

    private KeyPair generateKeyPair(int keyLength)
    throws CAException
    {
        try {
            KeyPairGenerator keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");

            keyPairGenerator.initialize(keyLength, securityFactory.createSecureRandom());

            return keyPairGenerator.generateKeyPair();
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new CAException(e);
        }
    }

    private String pkiFreeTextToString(PKIFreeText pkiFreeText)
    {
        StrBuilder sb = new StrBuilder();

        if (pkiFreeText != null)
        {
            for (int i = 0; i < pkiFreeText.size(); i++) {
                sb.appendSeparator(", ").append(pkiFreeText.getStringAtUTF8(i).getString());
            }
        }

        return sb.toString();
    }

    private X509Certificate cmpCertificateToX509Certificate(CMPCertificate cmpCertificate)
    throws CAException, CertificateException
    {
        if (cmpCertificate == null) {
            throw new CAException("cmpCertificate is null");
        }

        Certificate certificate = cmpCertificate.getX509v3PKCert();

        if (certificate == null) {
            throw new CAException("certificate is null");
        }

        return new JcaX509CertificateConverter().
                setProvider(securityFactory.getNonSensitiveProvider()).
                getCertificate(new X509CertificateHolder(certificate));
    }

    private byte[] sendCMPRequest(ProtectedPKIMessage message, CMPSettings cmpSettings)
    throws IOException, CAException
    {
        byte[] encodedMessage = message.toASN1Structure().getEncoded(ASN1Encoding.DER);

        String url = cmpSettings.getCMPServiceURL();

        SimpleHttpRequest request = SimpleRequestBuilder.post(url).build();

        EntityBuilder entityBuilder = EntityBuilder.create();

        entityBuilder.setBinary(encodedMessage);

        CloseableHttpAsyncClient httpClient;

        try {
            httpClient = getHTTPClient();
        }
        catch (GeneralSecurityException e) {
            throw new CAException(e);
        }

        HttpClientContext clientContext = httpClientContextFactory.createHttpClientContext();

        ByteArrayOutputStream response = new ByteArrayOutputStream();

        AbstractVoidBinResponseConsumer consumer;

        try(
            WritableByteChannel outputChannel = Channels.newChannel(new SizeLimitedOutputStream(response,
                    maxResponseSize));

            HttpEntity contentEntity = entityBuilder.build();
        )
        {
            consumer = new AbstractVoidBinResponseConsumer()
            {
                @Override
                protected void data(ByteBuffer src, boolean endOfStream)
                throws IOException
                {
                    try {
                        outputChannel.write(src);
                    }
                    catch (LimitReachedException e) {
                        // If the limit was reached, we do not want HTTPClient to retry. The
                        // @DefaultHttpRequestRetryStrategy will not retry if the exception is a
                        // ConnectionClosedException
                        throw new ConnectionClosedException(MAX_RESPONE_SIZE_EXCEEDED_ERROR, e);
                    }
                }
            };

            Future<Void> future = httpClient.execute(
                    new BasicRequestProducer(request, new BasicAsyncEntityProducer(IOUtils.toByteArray(
                            contentEntity.getContent()), ContentType.create("application/pkixcmp"))),
                    consumer, clientContext, null);

            future.get(totalTimeout, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();

            throw new IOException(e);
        }
        catch (ExecutionException e) {
            throw new IOException(ExceptionUtils.getRootCause(e));
        }
        catch (TimeoutException e) {
            throw new IOException(TIMEOUT_ERROR + url);
        }

        int httpStatusCode = consumer.getHttpStatusCode();
        String httpStatusReasonPhrase = consumer.getHttpStatusReasonPhrase();

        if (httpStatusCode != HttpStatus.SC_OK)
        {
            logger.error("CMP response error. Status: {}, Reason: {}", httpStatusCode, httpStatusReasonPhrase);

            throw new CAException("CMP response error. " +
                    "Status: " + httpStatusCode + ", " +
                    "Reason: " + httpStatusReasonPhrase);
        }

        return response.toByteArray();
    }

    @Override
    public KeyAndCertificate handleRequest(CertificateRequest request)
    throws CAException
    {
        if (!getCertificateHandlerName().equalsIgnoreCase(request.getCertificateHandlerName())) {
            // Should never happen
            throw new IllegalArgumentException("Handler mismatch. Expected " + getCertificateHandlerName() +
                    " but got " + request.getCertificateHandlerName());
        }

        if (!isEnabled()) {
            throw new CAException(getCertificateHandlerName() + " is not enabled");
        }

        try {
            KeyPair keyPair = generateKeyPair(request.getKeyLength());

            CMPSettings cmpSettings = getCMPSettings();

            RandomGenerator randomGenerator = securityFactory.createRandomGenerator();

            BigInteger certReqId = BigInteger.valueOf(1);

            CertificateRequestMessageBuilder msgbuilder = new CertificateRequestMessageBuilder(certReqId);

            X500Name issuerDN = new X500NameBuilder(RFC4519Style.INSTANCE).addRDN(
                    RFC4519Style.cn, cmpSettings.getIssuerDN()).build();

            X500Name subjectDN = X500PrincipalUtils.toX500Name(request.getSubject());

            msgbuilder.setIssuer(issuerDN);
            msgbuilder.setSubject(subjectDN);

            ASN1Sequence ansn1Sequence = ASN1Sequence.getInstance(keyPair.getPublic().getEncoded());

            AlgorithmIdentifier algId = AlgorithmIdentifier.getInstance(ansn1Sequence.getObjectAt(0));
            DERBitString keyData = (DERBitString) ansn1Sequence.getObjectAt(1);

            SubjectPublicKeyInfo keyInfo = new SubjectPublicKeyInfo(algId, keyData.getBytes());

            msgbuilder.setPublicKey(keyInfo);

            GeneralName sender = new GeneralName(subjectDN);
            //  RAVerified POP
            msgbuilder.setProofOfPossessionRaVerified();

            CertificateRequestMessage msg = msgbuilder.build();

            ProtectedPKIMessageBuilder pbuilder = new ProtectedPKIMessageBuilder(sender, new GeneralName(issuerDN));

            pbuilder.setMessageTime(new Date());
            pbuilder.setSenderNonce(randomGenerator.generateRandom(16));

            String senderKeyId = StringUtils.trimToNull(cmpSettings.getSenderKID());

            if (senderKeyId != null) {
                pbuilder.setSenderKID(MiscStringUtils.getBytesUTF8(senderKeyId));
            }

            CertReqMessages msgs = new CertReqMessages(msg.toASN1Structure());

            PKIBody pkibody = new PKIBody(PKIBody.TYPE_INIT_REQ, msgs);

            pbuilder.setBody(pkibody);

            JcePKMACValuesCalculator pkmacCalc = new JcePKMACValuesCalculator();

            AlgorithmIdentifier digAlg = new AlgorithmIdentifier(new ASN1ObjectIdentifier(
                    cmpSettings.getMessageProtectionDigestAlgorithm()));

            AlgorithmIdentifier macAlg = new AlgorithmIdentifier(new ASN1ObjectIdentifier(
                    cmpSettings.getMessageProtectionHMACAlgorithm()));

            pkmacCalc.setup(digAlg, macAlg);

            PKMACBuilder macbuilder = new PKMACBuilder(pkmacCalc);

            MacCalculator macCalculator = macbuilder.build(cmpSettings.getMessageProtectionPassword().toCharArray());

            ProtectedPKIMessage message = pbuilder.build(macCalculator);

            byte[] encodedResponse = sendCMPRequest(message, cmpSettings);

            GeneralPKIMessage responseMessage = new GeneralPKIMessage(encodedResponse);

            PKIBody responseBody = responseMessage.getBody();

            if (responseBody.getType() == PKIBody.TYPE_ERROR)
            {
                ErrorMsgContent errorMsgContent = (ErrorMsgContent) responseBody.getContent();

                StrBuilder errorMessageBulder = new StrBuilder();

                errorMessageBulder.append("Error code: ").append(errorMsgContent.getErrorCode())
                  .append(", Error details: ").append(pkiFreeTextToString(errorMsgContent.getErrorDetails()));

                PKIStatusInfo pkiStatusInfo = errorMsgContent.getPKIStatusInfo();

                if (pkiStatusInfo != null)
                {
                    errorMessageBulder.append(", Status: ").append(pkiStatusInfo.getStatus())
                      .append(", Status string: ").append(pkiFreeTextToString(pkiStatusInfo.getStatusString()))
                      .append(", Fail info: ").append(pkiStatusInfo.getFailInfo());
                }

                logger.warn("CMP request failed. Details: {}", errorMessageBulder);

                throw new CAException("CMP request failed. See logs for more information");
            }
            else if (responseBody.getType() == PKIBody.TYPE_INIT_REP)
            {
                if (!responseMessage.hasProtection()) {
                    throw new CAException("CMP response is not protected");
                }

                ProtectedPKIMessage protectedResponseMessage = new ProtectedPKIMessage(responseMessage);

                if (!protectedResponseMessage.hasPasswordBasedMacProtection())
                {
                    // Certificate signing protection.
                    //
                    // Use the first available certificate
                    X509Certificate cmpResponseSigningCert = new JcaX509CertificateConverter().
                            setProvider(securityFactory.getNonSensitiveProvider()).
                            getCertificate(protectedResponseMessage.getCertificates()[0]);

                    ContentVerifierProvider verifierProvider = new JcaContentVerifierProviderBuilder().
                            setProvider(securityFactory.getNonSensitiveProvider()).
                            build(cmpResponseSigningCert.getPublicKey());

                    if (!protectedResponseMessage.verify(verifierProvider)) {
                        throw new CAException("CMP response verification failed");
                    }
                }
                else {
                    // MAC based protection
                    if (!protectedResponseMessage.verify(macbuilder, cmpSettings.getMessageProtectionPassword().
                            toCharArray()))
                    {
                        throw new CAException("CMP response MAC verification failed");
                    }
                }

                if (!(responseBody.getContent() instanceof CertRepMessage certRepMessage))
                {
                    throw new CAException(CertRepMessage.class.getName() + " expected CertRepMessage but got " +
                            MiscClassUtils.getClassName(responseBody.getContent()));
                }

                CertResponse[] certResponses = certRepMessage.getResponse();

                if (ArrayUtils.isEmpty(certResponses)) {
                    throw new CAException("CMP response does not contain any certificates");
                }

                if (certResponses.length > 1) {
                    throw new CAException("One certificate expected but got " + certResponses.length);
                }

                CertResponse certResponse = certResponses[0];

                if (certResponse.getCertReqId() == null) {
                    throw new CAException("CertReqId of the response is not set");
                }

                if (!certReqId.equals(certResponse.getCertReqId().getValue())) {
                    throw new CAException("Expected CertReqId " + certReqId + " but got " + certResponse.getCertReqId());
                }

                CertifiedKeyPair certifiedKeyPair = certResponse.getCertifiedKeyPair();

                if (certifiedKeyPair == null) {
                    throw new CAException("certifiedKeyPair is null");
                }

                CertOrEncCert certOrEncCert = certifiedKeyPair.getCertOrEncCert();

                if (certOrEncCert == null) {
                    throw new CAException("certOrEncCert is null");
                }

                X509Certificate issuedCertificate = cmpCertificateToX509Certificate(certOrEncCert.getCertificate());

                CMPCertificate[] caCertificates = certRepMessage.getCaPubs();

                List<X509Certificate> additionalCertificates = new LinkedList<>();

                if (caCertificates != null)
                {
                    for (CMPCertificate caCertificate : caCertificates)
                    {
                        X509Certificate caX509Certificate = cmpCertificateToX509Certificate(caCertificate);

                        logger.debug("CA certificate {}", caX509Certificate);

                        additionalCertificates.add(caX509Certificate);
                    }
                }

                return new KeyAndCertificateWithAdditonalCertsImpl(keyPair.getPrivate(), issuedCertificate,
                        additionalCertificates);
            }
            else {
                throw new CAException("Unsupported response type " + responseBody.getType());
            }
        }
        catch (CRMFException | CMPException | NoSuchAlgorithmException | NoSuchProviderException | IOException |
                CertificateException | OperatorCreationException e)
        {
            throw new CAException(e);
        }
    }

    @Override
    public KeyAndCertificate handleRequest(CertificateRequest request, X509Certificate certificate)
    {
        // Not supported by this handler
        return null;
    }

    @Override
    public void cleanup(CertificateRequest request) {
        // Not supported by this handler
    }

    public int getMaxResponseSize() {
        return maxResponseSize;
    }

    public void setMaxResponseSize(int maxResponseSize) {
        this.maxResponseSize = maxResponseSize;
    }

    public long getTotalTimeout() {
        return totalTimeout;
    }

    public void setTotalTimeout(long totalTimeout) {
        this.totalTimeout = totalTimeout;
    }
}
