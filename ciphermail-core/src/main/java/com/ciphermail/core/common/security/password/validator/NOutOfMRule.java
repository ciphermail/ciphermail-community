/*
 * Copyright (c) 2021-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password.validator;

import org.passay.PasswordData;
import org.passay.Rule;
import org.passay.RuleResult;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Password rule which validates if at least N out M validations succeed
 */
public class NOutOfMRule implements Rule
{
    public static final String ERROR_CODE = "N/M-MIN-REQUIRED-NOT-REACHED";

    /*
     * The minimal number of rules to succeed for this rule to succeed
     */
    private final int minimumRequired;

    private final List<WeightedRule> weightedRules;

    public NOutOfMRule(int minimumRequired, WeightedRule... weightedRules) {
        this(minimumRequired, Arrays.asList(weightedRules));
    }

    public NOutOfMRule(int minimumRequired, @Nonnull Collection<WeightedRule> weightedRules)
    {
        this.minimumRequired = minimumRequired;
        this.weightedRules = new LinkedList<>(weightedRules);
    }

    @Override
    public RuleResult validate(PasswordData passwordData)
    {
        int successCount = 0;

        RuleResult result = new RuleResult();

        for (WeightedRule weightedRule : weightedRules)
        {
            RuleResult ruleResult = weightedRule.getRule().validate(passwordData);

            if (!ruleResult.isValid()) {
                result.getDetails().addAll(ruleResult.getDetails());
            }
            else {
                successCount = successCount + weightedRule.getWeight();
            }

            result.getMetadata().merge(ruleResult.getMetadata());
        }

        if (successCount < minimumRequired)
        {
            result.setValid(false);

            Map<String, Object> params = new HashMap<>();

            params.put("successCount", successCount);
            params.put("minimumRequired", minimumRequired);
            params.put("ruleCount", weightedRules.size());

            result.addError(ERROR_CODE, params);
        }

        return result;
    }
}
