/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

/**
 * All the possible PGP encryption algorithms
 *
 * @author Martijn Brinkers
 *
 */
public enum PGPEncryptionAlgorithm
{
    PLAIN_TEXT ("Plaintext", 0),
    IDEA       ("IDEA" ,     1),
    TRIPLE_DES ("3DES",      2),
    CAST5      ("CAST5",     3),
    BLOWFISH   ("Blowfish",  4),
    SAFER      ("Safer",     5),
    DES        ("DES",       6),
    AES_128    ("AES-128",   7),
    AES_192    ("AES-192",   8),
    AES_256    ("AES-256",   9),
    TWOFISH    ("Twofish",  10);

    private final String friendlyName;
    private final int tag;

    PGPEncryptionAlgorithm(String friendlyName, int tag)
    {
        this.friendlyName = friendlyName;
        this.tag = tag;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public int getTag() {
        return tag;
    }

    public static PGPEncryptionAlgorithm fromName(String name)
    {
        for (PGPEncryptionAlgorithm type : PGPEncryptionAlgorithm.values())
        {
            if (type.name().equalsIgnoreCase(name)) {
                return type;
            }
        }

        return null;
    }

    public static PGPEncryptionAlgorithm fromTag(int tag)
    {
        for (PGPEncryptionAlgorithm type : PGPEncryptionAlgorithm.values())
        {
            if (tag == type.tag) {
                return type;
            }
        }

        return null;
    }
}
