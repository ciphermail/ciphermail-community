/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Objects;

/**
 * PGPPublicKeyWrapper wraps a PGPPublicKey to provide hashCode and equals so a PGPPublicKey can be used in a Set etc.
 *
 * Note: this wrapper assumes that the encoded form of the public key does not change once it is wrapped.
 *
 * @author Martijn Brinkers
 *
 */
public class PGPPublicKeyWrapper
{
    /*
     * The wrapped public key
     */
    private final PGPPublicKey publicKey;

    /*
     * The encoded public key
     */
    private final byte[] encoded;

    public PGPPublicKeyWrapper(@Nonnull PGPPublicKey publicKey)
    throws IOException
    {
        this.publicKey = Objects.requireNonNull(publicKey);
        this.encoded = publicKey.getEncoded();
    }

    public PGPPublicKey getPublicKey() {
        return publicKey;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) { return false; }
        if (obj == this) { return true; }

        if (obj.getClass() != getClass()) {
            return false;
        }

        PGPPublicKeyWrapper other = (PGPPublicKeyWrapper) obj;

        return new EqualsBuilder()
            .append(encoded, other.encoded)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(encoded).toHashCode();
    }
}
