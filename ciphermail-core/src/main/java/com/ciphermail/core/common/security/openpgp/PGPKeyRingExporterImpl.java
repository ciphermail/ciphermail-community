/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.password.PasswordProvider;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.operator.PBESecretKeyEncryptor;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPDigestCalculatorProviderBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPKeyConverter;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPPrivateKey;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyEncryptorBuilder;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Implementation of PGPKeyRingExporter
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyRingExporterImpl implements PGPKeyRingExporter
{
    /*
     * The security factory to use
     */
    private final SecurityFactory securityFactory;

    /*
     * For converting PrivateKey's to PGPPrivateKey's and vice versa
     */
    private final JcaPGPKeyConverter keyConverter;

    /*
     * Use for exporting the private key
     */
    private final PGPDigestCalculator digestCalculator;

    public PGPKeyRingExporterImpl()
    throws PGPException
    {
        this.securityFactory = PGPSecurityFactoryFactory.getSecurityFactory();

        this.keyConverter = new JcaPGPKeyConverter().
                setProvider(securityFactory.getNonSensitiveProvider());

        this.digestCalculator = new JcaPGPDigestCalculatorProviderBuilder().
                setProvider(securityFactory.getNonSensitiveProvider()).build().get(
                HashAlgorithmTags.SHA1);
    }

    @Override
    public boolean exportPublicKeys(@Nonnull Collection<PGPKeyRingEntry> keyRingEntries, @Nonnull OutputStream output)
    throws IOException, PGPException
    {
        Set<PGPKeyRingEntry> masterKeys = getMasterKeys(keyRingEntries);

        List<PGPPublicKey> publicKeys = new LinkedList<>();

        for (PGPKeyRingEntry masterEntry : masterKeys)
        {
            publicKeys.add(masterEntry.getPublicKey());

            Set<PGPKeyRingEntry> subKeyEntries = masterEntry.getSubkeys();

            if (subKeyEntries != null)
            {
                for (PGPKeyRingEntry subKeyEntry : subKeyEntries) {
                    publicKeys.add(subKeyEntry.getPublicKey());
                }
            }
        }

        if (!publicKeys.isEmpty())
        {
            ArmoredOutputStream armor = new ArmoredOutputStream(output, PGPUtils.createArmorHeaders());

            for (PGPPublicKey publicKey : publicKeys) {
                publicKey.encode(armor);
            }

            armor.close();
        }

        return !publicKeys.isEmpty();
    }

    private PGPPrivateKey getPGPPrivateKey(PGPKeyRingEntry keyEntry)
    throws PGPException, IOException
    {
        PGPPrivateKey privateKey = keyEntry.getPrivateKey();

        if (privateKey instanceof JcaPGPPrivateKey jcaPGPPrivateKey)
        {
            // A JcaPGPPrivateKey is not a full PGPPrivateKey so we need to convert it before we can export
            // it. This will naturally only work when an HSM is not used.
            privateKey = keyConverter.getPGPPrivateKey(keyEntry.getPublicKey(),
                    jcaPGPPrivateKey.getPrivateKey());

        }

        return privateKey;
    }

    @Override
    public boolean exportSecretKeys(@Nonnull Collection<PGPKeyRingEntry> keyRingEntries, @Nonnull OutputStream output,
        PasswordProvider exportPasswordProvider)
    throws IOException, PGPException
    {
        try {
            Set<PGPKeyRingEntry> masterKeys = getMasterKeys(keyRingEntries);

            List<PGPPublicKey> publicKeys = new LinkedList<>();
            List<PGPSecretKey> secretKeys = new LinkedList<>();

            char[] password = exportPasswordProvider != null ? exportPasswordProvider.getPassword() : null;

            JcePBESecretKeyEncryptorBuilder secretKeyEncryptorBuilder = new JcePBESecretKeyEncryptorBuilder(
                    password != null ? SymmetricKeyAlgorithmTags.AES_128 : SymmetricKeyAlgorithmTags.NULL);

            secretKeyEncryptorBuilder.setProvider(securityFactory.getNonSensitiveProvider());
            secretKeyEncryptorBuilder.setSecureRandom(securityFactory.createSecureRandom());

            PBESecretKeyEncryptor secretKeyEncryptor = secretKeyEncryptorBuilder.build(
                    password != null ? password : new char[]{});

            for (PGPKeyRingEntry masterEntry : masterKeys)
            {
                publicKeys.add(masterEntry.getPublicKey());

                PGPPrivateKey privateMasterKey = getPGPPrivateKey(masterEntry);

                if (privateMasterKey != null)
                {
                    secretKeys.add(new PGPSecretKey(privateMasterKey, masterEntry.getPublicKey(),
                            digestCalculator, true /* master key */, secretKeyEncryptor));
                }

                Set<PGPKeyRingEntry> subKeyEntries = masterEntry.getSubkeys();

                if (subKeyEntries != null)
                {
                    for (PGPKeyRingEntry subKeyEntry : subKeyEntries)
                    {
                        publicKeys.add(subKeyEntry.getPublicKey());

                        PGPPrivateKey privateSubKey = getPGPPrivateKey(subKeyEntry);

                        if (privateSubKey != null)
                        {
                            secretKeys.add(new PGPSecretKey(privateSubKey, subKeyEntry.getPublicKey(),
                                    digestCalculator, false /* sub key */, secretKeyEncryptor));
                        }
                    }
                }
            }

            if (!publicKeys.isEmpty())
            {
                ArmoredOutputStream armor = new ArmoredOutputStream(output, PGPUtils.createArmorHeaders());

                for (PGPPublicKey publicKey : publicKeys) {
                    publicKey.encode(armor);
                }

                armor.close();
            }

            if (!secretKeys.isEmpty())
            {
                ArmoredOutputStream armor = new ArmoredOutputStream(output, PGPUtils.createArmorHeaders());

                for (PGPSecretKey secretKey : secretKeys) {
                    secretKey.encode(armor);
                }

                armor.close();
            }

            return (secretKeys.size() + publicKeys.size()) > 0;
        }
        catch (NoSuchProviderException | NoSuchAlgorithmException e) {
            throw new IOException(e);
        }
    }

    private Set<PGPKeyRingEntry> getMasterKeys(Collection<PGPKeyRingEntry> keyRingEntries)
    {
        Set<PGPKeyRingEntry> masterKeyEntries = new LinkedHashSet<>();

        if (keyRingEntries != null)
        {
            for (PGPKeyRingEntry entry : keyRingEntries)
            {
                if (entry == null) {
                    continue;
                }

                if (!entry.isMasterKey()) {
                    entry = entry.getParentKey();
                }

                if (entry != null) {
                    masterKeyEntries.add(entry);
                }
            }
        }

        return masterKeyEntries;
    }
}
