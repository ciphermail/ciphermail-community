/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.repository.hibernate;

import com.google.common.annotations.VisibleForTesting;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.mail.repository.MailRepository;
import com.ciphermail.core.common.mail.repository.MailRepositoryItem;
import com.ciphermail.core.common.mail.repository.MailRepositorySearchField;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Implementation of MailRepository that stores MailRepositoryItem's in the database.
 *
 * @author Martijn Brinkers
 *
 */
public class MailRepositoryImpl implements MailRepository
{
    /*
     * The name of this repository to use
     */
    private final String repositoryName;

    /*
     * Manages database sessions
     */
    private final SessionManager sessionManager;

    public MailRepositoryImpl(@Nonnull SessionManager sessionManager,
            @Nonnull String repositoryName)
    {
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.repositoryName = Objects.requireNonNull(repositoryName);
    }

    @Override
    public String getName() {
        return repositoryName;
    }

    @Override
    public @Nonnull MailRepositoryItem createItem(@Nonnull MimeMessage message)
    throws MessagingException, IOException
    {
        return new MailRepositoryEntity(repositoryName, message);
    }

    @VisibleForTesting
    public @Nonnull MailRepositoryItem createItem(@Nonnull MimeMessage message, @Nonnull Date created)
    throws MessagingException, IOException
    {
        return new MailRepositoryEntity(repositoryName, message, created);
    }

    @Override
    public void addItem(@Nonnull MailRepositoryItem item)
    {
        if (!(item instanceof MailRepositoryEntity mailRepositoryEntity)) {
            throw new IllegalArgumentException("The item is-not-a MailRepositoryEntity.");
        }
        getDAO().persist(mailRepositoryEntity);
    }

    @Override
    public void deleteItem(@Nonnull UUID id)
    {
        MailRepositoryDAO dao = getDAO();

        MailRepositoryEntity item = dao.findById(id, MailRepositoryEntity.class);

        if (item != null) {
            dao.delete(item);
        }
    }

    @Override
    public MailRepositoryItem getItem(@Nonnull UUID id) {
        return getDAO().findById(id, MailRepositoryEntity.class);
    }

    @Override
    public long getItemCount() {
        return getDAO().getItemCount(repositoryName);
    }

    @Override
    public List<? extends MailRepositoryItem> getItems(Integer firstResult, Integer maxResults) {
        return getDAO().getItems(repositoryName, firstResult, maxResults);
    }

    @Override
    public List<? extends MailRepositoryItem> getItemsBefore(@Nonnull Date before, Integer firstResult,
            Integer maxResults)
    {
        return getDAO().getItemsBefore(repositoryName, before, firstResult, maxResults);
    }

    @Override
    public long getSearchCount(@Nonnull MailRepositorySearchField searchField, @Nonnull String key) {
        return getDAO().getSearchCount(repositoryName, searchField, key);
    }

    @Override
    public List<? extends MailRepositoryItem> searchItems(@Nonnull MailRepositorySearchField searchField,
            @Nonnull String key, Integer firstResult, Integer maxResults)
    {
        return getDAO().searchItems(repositoryName, searchField, key, firstResult, maxResults);
    }

    @Override
    public void deleteAll() {
        getDAO().deleteAll(repositoryName);
    }

    private MailRepositoryDAO getDAO() {
        return MailRepositoryDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }
}
