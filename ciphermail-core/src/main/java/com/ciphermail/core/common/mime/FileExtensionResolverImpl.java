/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mime;

import org.apache.commons.lang.StringUtils;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MediaTypeRegistry;
import org.apache.tika.mime.MimeTypes;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of FileExtensionResolver
 *
 * @author Martijn Brinkers
 *
 */
public class FileExtensionResolverImpl implements FileExtensionResolver
{
    private static final String DEFAULT_EXTENSION = "bin";
    private static final int MAX_DEPTH = 10;

    /*
     * Mapping from content type to extension
     */
    private final Map<MediaType, String> mediaTypeToExtension = Collections.synchronizedMap(new HashMap<>());

    /*
     * Is used to get a base mime type
     */
    private final MediaTypeRegistry mediaTypeRegistry;

    public FileExtensionResolverImpl()
    {
        this.mediaTypeRegistry = MimeTypes.getDefaultMimeTypes().getMediaTypeRegistry();

        addDefaultMappings();
    }

    public void addMapping(@Nonnull String contentType, @Nonnull String extension)
    {
        contentType = StringUtils.trimToNull(contentType);

        if (contentType == null) {
            throw new IllegalArgumentException("contentType is empty");
        }

        extension = StringUtils.trimToNull(extension);

        if (extension == null) {
            throw new IllegalArgumentException("extension is empty");
        }

        mediaTypeToExtension.put(MediaType.parse(contentType), extension);
    }

    public void addDefaultMappings()
    {
        addMapping("application/octet-stream", "bin");
        addMapping("image/gif", "gif");
        addMapping("image/jpeg", "jpg");
        addMapping("image/png", "png");
        addMapping("image/svg+xml", "svg");
        addMapping("image/tiff", "tiff");
        addMapping("text/css", "css");
        addMapping("text/csv", "csv");
        addMapping("text/plain", "txt");
        addMapping("text/html", "htm");
        addMapping("text/xml", "xml");
        addMapping("application/pdf", "pdf");
        addMapping("application/xml", "xml");
        addMapping("application/zip", "zip");
        addMapping("text/calendar", "ics");
        addMapping("application/msword", "doc");
        addMapping("application/vnd.ms-powerpoint", "ppt");
        addMapping("application/vnd.ms-excel", "xls");
    }

    @Override
    public String getExtensionFromContentType(String contentType)
    {
        String extension = null;

        MediaType mediaType = MediaType.parse(contentType);

        if (mediaType == null) {
            mediaType = MediaType.OCTET_STREAM;
        }

        // If the depth exceeds the max exit (prevents endless loop in case something is wrong with mediaTypeRegistry
        int count = 0;

        while (extension == null && mediaType != null && count++ < MAX_DEPTH)
        {
            extension = mediaTypeToExtension.get(mediaType);

            if (extension == null)
            {
                // The type might be an alias
                MediaType mainType = mediaTypeRegistry.normalize(mediaType);

                if (mainType != null) {
                    extension = mediaTypeToExtension.get(mainType);
                }

                if (extension == null) {
                    mediaType = mediaTypeRegistry.getSupertype(mediaType);
                }
            }
        }

        if (extension == null) {
            extension = DEFAULT_EXTENSION;
        }

        return extension;
    }
}
