/*
 * Copyright (c) 2008-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.tools;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.KeyIdentifier;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.asn1.ASN1Utils;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.cms.KeyNotFoundException;
import com.ciphermail.core.common.security.cms.RecipientInfo;
import com.ciphermail.core.common.security.cms.SignerInfo;
import com.ciphermail.core.common.security.cms.SignerInfoException;
import com.ciphermail.core.common.security.keystore.BasicKeyStore;
import com.ciphermail.core.common.security.keystore.KeyStoreKeyProvider;
import com.ciphermail.core.common.security.smime.SMIMEBuilder;
import com.ciphermail.core.common.security.smime.SMIMEBuilderImpl;
import com.ciphermail.core.common.security.smime.SMIMECapabilitiesInspector;
import com.ciphermail.core.common.security.smime.SMIMECapabilityInfo;
import com.ciphermail.core.common.security.smime.SMIMECompressedInspector;
import com.ciphermail.core.common.security.smime.SMIMEEncryptionAlgorithm;
import com.ciphermail.core.common.security.smime.SMIMEEnvelopedInspector;
import com.ciphermail.core.common.security.smime.SMIMEInspector;
import com.ciphermail.core.common.security.smime.SMIMEInspectorImpl;
import com.ciphermail.core.common.security.smime.SMIMESignMode;
import com.ciphermail.core.common.security.smime.SMIMESignedInspector;
import com.ciphermail.core.common.security.smime.SMIMESigningAlgorithm;
import com.ciphermail.core.common.security.smime.SMIMEType;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.mail.smime.SMIMEException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertSelector;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings({"java:S106", "java:S112"})
public class SMIME
{
    private static final String COMMAND_NAME = SMIME.class.getName();

    private static final Logger logger = LoggerFactory.getLogger(SMIME.class);

    private static final SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

    private static final String HELP_OPTION_SHORT = "h";
    private static final String HELP_OPTION_LONG = "help";

    private static final String IN_FILE_OPTION = "in-file";
    private static final String OUT_FILE_OPTION = "out-file";
    private static final String KEYSTORE_FILE_OPTION = "keystore-file";
    private static final String PASSWORD_OPTION = "password";
    private static final String P7M_IN_OPTION = "p7m-in";
    private static final String CONVERT_P7M_TO_MIME_OPTION = "convert-p7m-to-mime";
    private static final String BINARY_OPTION = "binary";
    private static final String CER_OUTPUT_OPTION = "cer-out";
    private static final String READ_OPTION = "read";
    private static final String SIGN_OPTION = "sign";
    private static final String KEY_ALIAS_OPTION = "key-alias";
    private static final String PRINT_KEY_ALIASES_OPTION = "print-key-aliases";
    private static final String DIGEST_OPTION = "digest";
    private static final String USE_ALL_KEYS_OPTION = "use-all-keys";

    /*
     * BasicKeyStore implementation which does not use the provided KeyIdentifier but always returns all keys from
     * the provided key store. This is for testing purposes so we can analyze an encrypted message (for example when
     * analyzing OAEP) without requiring the correct key.
     */
    private static class UseAllKeysKeyBasicKeyStore implements BasicKeyStore
    {
        private final KeyStore keyStore;
        private final String password;

        UseAllKeysKeyBasicKeyStore(KeyStore keyStore, String password)
        {
            this.keyStore = keyStore;
            this.password = password;
        }

        @Override
        public Collection<? extends Key> getMatchingKeys(KeyIdentifier keyIdentifier)
        throws KeyStoreException
        {
            List<Key> keys = new LinkedList<>();

            Enumeration<String> aliases = keyStore.aliases();

            while (aliases.hasMoreElements())
            {
                String alias = aliases.nextElement();

                if (keyStore.isKeyEntry(alias))
                {
                    try {
                        keys.add(keyStore.getKey(alias, password != null ? password.toCharArray() : null));
                    }
                    catch (UnrecoverableKeyException e) {
                        logger.error("Error getting Key", e);
                    }
                    catch (NoSuchAlgorithmException e) {
                        logger.error("Algorithm not supported", e);
                    }
                }
            }

            return keys;
        }
    }

    private Options createCommandLineOptions()
    {
        Options options = new Options();

        options.addOption(Option.builder(HELP_OPTION_SHORT).longOpt(HELP_OPTION_LONG).desc("Show help").build());

        options.addOption(Option.builder().longOpt(IN_FILE_OPTION).argName("file").hasArg().
                desc("input file").build());

        options.addOption(Option.builder().longOpt(OUT_FILE_OPTION).argName("file").hasArg().
                desc("output file").build());

        options.addOption(Option.builder().longOpt(KEYSTORE_FILE_OPTION).argName("file").hasArg().
                desc("PKCS#12 key store file").build());

        options.addOption(Option.builder().longOpt(PASSWORD_OPTION).argName("password").hasArg().
                desc("PKCS#12 key store password").build());

        options.addOption(Option.builder().longOpt(P7M_IN_OPTION).
                desc("enable this option if input file is a p7m file").build());

        options.addOption(Option.builder().longOpt(CONVERT_P7M_TO_MIME_OPTION).
                desc("if set, the p7m file will be converted to a MIME file").build());

        options.addOption(Option.builder().longOpt(BINARY_OPTION).
                desc("enable this option if input file is binary instead of base64").build());

        options.addOption(Option.builder().longOpt(CER_OUTPUT_OPTION).argName("file").hasArg().
                desc("save the certificates from the email to file in PKCS#7 format").build());

        options.addOption(Option.builder().longOpt(READ_OPTION).
                desc("read S/MIME message from input file").build());

        options.addOption(Option.builder().longOpt(SIGN_OPTION).
                desc("sign input file").build());

        options.addOption(Option.builder().longOpt(KEY_ALIAS_OPTION).argName("alias").hasArg().
                desc("key alias").build());

        options.addOption(Option.builder().longOpt(PRINT_KEY_ALIASES_OPTION).
                desc("list all key aliases").build());

        options.addOption(Option.builder().longOpt(DIGEST_OPTION).argName("algorithm").hasArg().
                desc("digest algorithm used for signing").build());

        options.addOption(Option.builder().longOpt(USE_ALL_KEYS_OPTION).
                desc("if set, all keys from the key store will be used for decryption").build());

        return options;
    }

    private static MimeMessage loadMessage(String filename)
    throws FileNotFoundException, MessagingException
    {
        return MailUtils.loadMessage(new File(filename).getAbsoluteFile());
    }

    private static MimeMessage loadp7m(String filename, boolean binary)
    throws Exception
    {
        File p7mFile = new File(filename);

        p7mFile = p7mFile.getAbsoluteFile();

        FileInputStream fis = new FileInputStream(p7mFile);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        OutputStreamWriter sw = new OutputStreamWriter(bos);

        sw.append("Content-Type: application/pkcs7-mime; name=\"smime.p7m\"\r\n");
        sw.append("Content-Transfer-Encoding: base64\r\n");
        sw.append("\r\n\r\n");

        byte[] content = IOUtils.toByteArray(fis);

        if (binary) {
            content = Base64.encodeBase64Chunked(content);
        }

        String base64Content = MiscStringUtils.toStringFromASCIIBytes(content);

        sw.append(base64Content);

        sw.flush();

        return MailUtils.byteArrayToMessage(bos.toByteArray());
    }

    private static KeyStore loadKeyStore(String keyFile, String password)
    throws Exception
    {
        File file = new File(keyFile);

        file = file.getAbsoluteFile();

        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(file), password != null ? password.toCharArray() : null);

        return keyStore;
    }

    private static void inspectEnveloped(SMIMEEnvelopedInspector inspector)
    throws Exception
    {
        System.err.println("==============================");
        System.err.println("Encrypted message");
        System.err.println();

        String algorithmName = inspector.getEncryptionAlgorithmOID();

        SMIMEEncryptionAlgorithm encryptionAlg = SMIMEEncryptionAlgorithm.fromOID(algorithmName);

        if (encryptionAlg != null) {
            algorithmName = encryptionAlg.toString();
        }

        System.err.print("Encryption Algorithm: " + algorithmName);

        if (encryptionAlg != null) {
            System.err.print(", Key size: " + SMIMEEncryptionAlgorithm.getKeySize(encryptionAlg));
        }

        System.err.println();
        System.err.println();

        List<RecipientInfo> recipients = inspector.getRecipients();

        for (int i = 0; i < recipients.size(); i++)
        {
            RecipientInfo recipient = recipients.get(i);

            System.err.println("*** Recipient " + i);
            System.err.println();
            System.err.println(recipient.toString());
            System.err.println();
        }

        System.err.println();
        System.err.println("Unprotected attributes:");
        System.err.println(ASN1Utils.dump(inspector.getUnprotectedAttributes()));

        System.err.println("*** Decrypted message:");
        System.err.println();

        try {
            inspector.getContentAsMimeMessage().writeTo(System.out);
        }
        catch(KeyNotFoundException e) {
            System.err.println("Decryption key could not be found.");
        }
    }

    private static void dumpSMIMECapabilities(AttributeTable attributes)
    {
        List<SMIMECapabilityInfo> capabilities = SMIMECapabilitiesInspector.inspect(attributes);

        System.err.println("SMIME capabilities");
        System.err.println();

        for (SMIMECapabilityInfo capabilityInfo : capabilities)
        {
            System.err.println(capabilityInfo);
        }

        System.err.println();
    }

    private static void inspectSigned(SMIMESignedInspector inspector, String cerOut)
    throws Exception
    {
        System.err.println("==============================");
        System.err.println("Signed message");
        System.err.println();

        System.err.println("CMSVersion: " + inspector.getVersion());
        System.err.println();

        List<SignerInfo> signers = inspector.getSigners();

        List<X509Certificate> certificates = inspector.getCertificates();

        for (int i = 0; i < signers.size(); i++)
        {
            SignerInfo signer = signers.get(i);

            System.err.println("*** [Signer " + i + "] ***");
            System.err.println();
            System.err.println(signer);
            System.err.println();

            dumpSMIMECapabilities(signer.getSignedAttributes());

            CertSelector selector = signer.getSignerId().getSelector();

            List<X509Certificate> signingCerts = CertificateUtils.getMatchingCertificates(certificates, selector);

            if (!signingCerts.isEmpty())
            {
                // there could be more certificates but get the first one
                X509Certificate certificate = signingCerts.get(0);

                try {
                    if (signer.verify(certificate.getPublicKey())) {
                        System.err.println("Verification OK.");
                    }
                    else {
                        System.err.println("* WARNING: verification failed.");
                    }
                }
                catch(SignerInfoException e) {
                    System.err.println("* WARNING: verification failed. Message: " + e.getMessage());
                }
            }
            else {
                System.err.println("* WARNING: Signing certificate not found so unable to verify signature *");
            }
        }

        System.err.println("==============================");
        System.err.println("Certificates:");
        System.err.println();

        for (int i = 0; i < certificates.size(); i++)
        {
            X509Certificate certificate = certificates.get(i);

            System.err.println("*** Certificate " + i);
            System.err.println();
            System.err.println(certificate);
            System.err.println();

            System.err.println("Extra information:");
            System.err.println();

            X509CertificateInspector certInspector = new X509CertificateInspector(certificate);

            System.err.println("SubjectKeyIdentifier: " + certInspector.getSubjectKeyIdentifierHex());
            System.err.println("Email: " + certInspector.getEmail());
            System.err.println();
        }

        if (cerOut != null)
        {
            File outfile = new File(cerOut);

            CertificateUtils.writeCertificates(certificates, new FileOutputStream(outfile));
        }

        List<X509CRL> crls = inspector.getCRLs();

        System.err.println("==============================");
        System.err.println("CRLs:");
        System.err.println();

        for (int i = 0; i < crls.size(); i++)
        {
            X509CRL crl = crls.get(i);

            System.err.println("*** CRL " + i);
            System.err.println();
            System.err.println(crl);
            System.err.println();
        }


        System.err.println("*** Unsigned message:");
        System.err.println();

        inspector.getContentAsMimeMessage().writeTo(System.out);
    }

    private static void inspectCompressed(SMIMECompressedInspector inspector)
    throws Exception
    {
        System.err.println("==============================");
        System.err.println("Compressed message");
        System.err.println();
        System.err.println("Decompressed message:");
        System.err.println();

        inspector.getContentAsMimeMessage().writeTo(System.out);
    }

    private static void inspectMessage(MimeMessage message, BasicKeyStore basicKeyStore, String cerOut)
    throws Exception
    {
        if (basicKeyStore == null) {
            basicKeyStore = keyIdentifier -> Collections.emptyList();
        }

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, basicKeyStore,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        if (inspector.getSMIMEType() == SMIMEType.NONE)
        {
            System.err.println("Message is not a S/MIME message.");

            return;
        }

        switch (inspector.getSMIMEType())
        {
            case ENCRYPTED  : inspectEnveloped(inspector.getEnvelopedInspector());
                break;
            case AUTH_ENCRYPTED  : inspectEnveloped(inspector.getEnvelopedInspector());
                break;
            case SIGNED     : inspectSigned(inspector.getSignedInspector(), cerOut);
                break;
            case COMPRESSED : inspectCompressed(inspector.getCompressedInspector());
                break;
            case NONE:
                break;
        }
    }

    private static void printKeystoreAliases(KeyStore keyStore)
    throws KeyStoreException
    {
        Enumeration<String> aliases = keyStore.aliases();

        System.out.println("**** BEGIN KEY ALIASES ***");

        while (aliases.hasMoreElements()) {
            System.out.println(aliases.nextElement());
        }

        System.out.println("**** END KEY ALIASES ***");
    }

    private static void sign(MimeMessage source, KeyStore keyStore, String alias, String password, String digestAlgo,
            String outFile)
    throws Exception
    {
        if (StringUtils.isEmpty(alias)) {
            throw new MissingArgumentException("alias is missing.");
        }

        KeyStore.Entry entry = keyStore.getEntry(alias, new KeyStore.PasswordProtection(password.toCharArray()));

        if (!(entry instanceof KeyStore.PrivateKeyEntry privateKeyEntry)) {
            throw new KeyStoreException("Key is not a PrivateKeyEntry.");
        }

        X509Certificate certificate = (X509Certificate) privateKeyEntry.getCertificate();

        if (certificate == null) {
            throw new KeyStoreException("Entry does not have a certificate.");
        }

        PrivateKey key = privateKeyEntry.getPrivateKey();

        if (key == null) {
            throw new KeyStoreException("Entry does not have a private key.");
        }

        SMIMESigningAlgorithm signingAlgorithm;

        if (StringUtils.isNotEmpty(digestAlgo))
        {
            signingAlgorithm = SMIMESigningAlgorithm.fromName(digestAlgo);

            if (signingAlgorithm == null) {
                throw new IllegalArgumentException(digestAlgo + " is not a valid digest.");
            }
        }
        else {
            signingAlgorithm = SMIMESigningAlgorithm.SHA1_WITH_RSA;
        }

        SMIMEBuilder builder = new SMIMEBuilderImpl(source);

        builder.addCertificates(certificate);
        builder.addSigner(key, certificate, signingAlgorithm);

        builder.sign(SMIMESignMode.CLEAR);

        MimeMessage signed = builder.buildMessage();

        if (signed == null) {
            throw new SMIMEException("Message could not be signed");
        }

        MailUtils.writeMessage(signed, new File(outFile));
    }

    private void handleCommandline(String[] args)
    throws Exception
    {
        InitializeBouncycastle.initialize();

        CommandLineParser parser = new DefaultParser();

        Options options = createCommandLineOptions();

        HelpFormatter formatter = new HelpFormatter();

        CommandLine commandLine;

        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e)
        {
            formatter.printHelp(COMMAND_NAME, options, true);

            throw e;
        }

        String inFile   = commandLine.getOptionValue(IN_FILE_OPTION);
        String keyFile  = commandLine.getOptionValue(KEYSTORE_FILE_OPTION);
        String password = commandLine.getOptionValue(PASSWORD_OPTION);
        boolean binary  = commandLine.hasOption(BINARY_OPTION);
        String cerOut   = commandLine.getOptionValue(CER_OUTPUT_OPTION);
        String alias    = commandLine.getOptionValue(KEY_ALIAS_OPTION);
        String digest   = commandLine.getOptionValue(DIGEST_OPTION);
        String outFile  = commandLine.getOptionValue(OUT_FILE_OPTION);

        if (commandLine.getOptions().length == 0 || commandLine.hasOption(HELP_OPTION_SHORT) || commandLine.hasOption(HELP_OPTION_LONG))
        {
            formatter.printHelp(COMMAND_NAME, options, true);

            return;
        }

        KeyStore keyStore = null;

        if (keyFile != null) {
            keyStore = loadKeyStore(keyFile, password);
        }

        if (commandLine.hasOption(PRINT_KEY_ALIASES_OPTION))
        {
            if (keyStore == null) {
                throw new MissingArgumentException(KEYSTORE_FILE_OPTION);
            }

            printKeystoreAliases(keyStore);
        }

        MimeMessage message;

        if (commandLine.hasOption(READ_OPTION))
        {
            if (inFile == null) {
                throw new MissingArgumentException(IN_FILE_OPTION);
            }

            if (commandLine.hasOption(P7M_IN_OPTION))
            {
                message = loadp7m(inFile, binary);

                if (commandLine.hasOption(OUT_FILE_OPTION)) {
                    MailUtils.writeMessage(message, new File(outFile));
                }
            }
            else {
                message = loadMessage(inFile);
            }

            BasicKeyStore basicKeyStore = null;

            if (keyStore != null)
            {
                if (!commandLine.hasOption(USE_ALL_KEYS_OPTION))
                {
                    basicKeyStore = new KeyStoreKeyProvider(keyStore, password);

                    ((KeyStoreKeyProvider)basicKeyStore).setUseOL2010Workaround(true);
                }
                else {
                    basicKeyStore = new UseAllKeysKeyBasicKeyStore(keyStore, password);
                }
            }

            inspectMessage(message, basicKeyStore, cerOut);
        }
        else if (commandLine.hasOption(SIGN_OPTION))
        {
            if (inFile == null) {
                throw new MissingArgumentException(IN_FILE_OPTION);
            }

            if (outFile == null) {
                throw new MissingArgumentException(OUT_FILE_OPTION);
            }

            message = loadMessage(inFile);

            sign(message, keyStore, alias, password, digest, outFile);
        }
    }

    public static void main(String[] args)
    throws Exception
    {
        SMIME tool = new SMIME();

        try {
            tool.handleCommandline(args);
        }
        catch (MissingArgumentException | MissingOptionException e)
        {
            System.err.println("Some required parameters are missing: " + e.getMessage());

            System.exit(2);
        }
        catch (UnrecognizedOptionException e)
        {
            System.err.println(e.getMessage());

            System.exit(3);
        }
        catch (ParseException e)
        {
            System.err.println("Command line parsing error. " + e);

            System.exit(4);
        }
    }

}
