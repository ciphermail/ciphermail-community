/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.security.openpgp.PGPKeyFlagChecker;
import com.ciphermail.core.common.security.openpgp.PGPNotSelfSignedException;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyInspector;
import com.ciphermail.core.common.security.openpgp.PGPSignatureValidator;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.util.Context;
import org.apache.commons.collections.CollectionUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPSignatureSubpacketVector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

/**
 * PGPPublicKeyValidator that checks whether a key is a valid subkey (i.e., whether the subkey has a binding
 * signature signed by the master key)
 *
 * @author Martijn Brinkers
 *
 */
public class SubKeyBindingSignatureValidator implements PGPPublicKeyValidator
{
    private static final Logger logger = LoggerFactory.getLogger(SubKeyBindingSignatureValidator.class);

    /*
     * PGPSignatureValidator is used to check whether the revocations signature is valid
     */
    private final PGPSignatureValidator signatureValidator;

    /*
     * We need to know whether the key is valid for signing if the key is a sub key
     */
    private final PGPKeyFlagChecker keyFlagChecker;

    /*
     * If a key is a sub key and the sub key is valid for signing, a valid primary subkey binding signature should be
     * embedded. This requirement was added later to the RFC so there might be keys which do not have a primary subkey
     * binding. Checking the primary sub key binding sig can be disabled by setting checkPrimaryKeyBindingSignature to
     * false
     */
    private boolean checkPrimaryKeyBindingSignature = true;

    public SubKeyBindingSignatureValidator(@Nonnull PGPSignatureValidator signatureValidator,
            @Nonnull PGPKeyFlagChecker keyFlagChecker)
    {
        this.signatureValidator = Objects.requireNonNull(signatureValidator);
        this.keyFlagChecker = Objects.requireNonNull(keyFlagChecker);
    }

    @Override
    public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
    {
        boolean valid = false;
        String failureMessage = null;
        PGPPublicKeyValidatorFailureSeverity severity = null;

        // Subkey binding check is only relevant for sub keys
        if (!publicKey.isMasterKey())
        {
            // The master key must be stored in the context
            PGPPublicKey masterKey = context.get(StandardValidatorContextObjects.MASTER_KEY, PGPPublicKey.class);

            if (masterKey == null)
            {
                return new PGPPublicKeyValidatorResultImpl(this, false, PGPPublicKeyValidatorFailureSeverity.MAJOR,
                        "Master key not available. Unable to check subkey binding signature.");
            }

            try {
                List<PGPSignature> signatures = PGPPublicKeyInspector.getSubkeyBindingSignatures(publicKey);

                if (CollectionUtils.isNotEmpty(signatures))
                {
                    for (PGPSignature signature : signatures)
                    {
                        if(signatureValidator.validateSubkeyBindingSignature(masterKey, publicKey, signature))
                        {
                            // If the key is a subkey and it's a signing key, it must contain a primary key binding
                            // signature
                            //
                            // From RFC 4880 11.1 "
                            //
                            // For subkeys that can issue signatures, the subkey binding signature MUST contain an
                            // Embedded Signature subpacket with a primary key binding signature (0x19) issued by the
                            // subkey on the top-level key.
                            //
                            // For reasons why this is important:
                            //
                            // "it is possible for an attacker to take a signing subkey from a victim's key and attach
                            // this signing subkey to the attacker's own key.  The attacker does this by issuing a new
                            // binding signature on the subkey from his own primary key.  The end result is that a
                            // signature issued by this signing subkey may be claimed to be from either the attacker
                            // or the victim."
                            //
                            // See Back-signatures, part II http://www.imc.org/ietf-openpgp/mail-archive/msg04690.html
                            // and http://www.imc.org/ietf-openpgp/mail-archive/msg04794.html
                            //
                            // Note: since this requirement was later added to the RFC, there might be signing sub keys
                            // without a valid primary key binding signature. Checking for primary key binding sig can
                            // therefore be disabled
                            if (checkPrimaryKeyBindingSignature &&
                                    keyFlagChecker.isSubKeyValidForSigning(masterKey, publicKey))
                            {
                                // It's a signing key so we must check the primary key binding signature
                                PGPSignatureSubpacketVector unhashedSubPackets = signature.getUnhashedSubPackets();

                                if (unhashedSubPackets != null)
                                {
                                    PGPSignatureList embeddedSignatures = unhashedSubPackets.getEmbeddedSignatures();

                                    if (embeddedSignatures != null && !embeddedSignatures.isEmpty())
                                    {
                                        for (int i = 0; i < embeddedSignatures.size(); i++)
                                        {
                                            PGPSignature embeddedSignature = embeddedSignatures.get(i);

                                            if (embeddedSignature != null && embeddedSignature.getSignatureType() ==
                                                    PGPSignature.PRIMARYKEY_BINDING)
                                            {
                                                if(signatureValidator.validatePrimaryKeyBindingSignature(masterKey,
                                                        publicKey, embeddedSignature))
                                                {
                                                    valid = true;

                                                    // We found a valid primary key binding so we can stop
                                                    break;
                                                }
                                                else {
                                                    logger.warn("Primary key binding signature for key with key id " +
                                                            "{} is not valid",
                                                            PGPUtils.getKeyIDHex(publicKey.getKeyID()));
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        logger.warn("key with key id {} does not contain any embedded signatures ",
                                                PGPUtils.getKeyIDHex(publicKey.getKeyID()));
                                    }
                                }
                            }
                            else {
                                // Not a signing key or checkPrimaryKeyBindingSignature is false
                                valid = true;
                            }
                        }

                        if (valid) {
                            break;
                        }
                    }
                }
                else {
                    logger.warn("key with key id {} does not contain a subkey binding signature",
                            PGPUtils.getKeyIDHex(publicKey.getKeyID()));
                }
            }
            catch (PGPNotSelfSignedException e) {
                logger.debug("Subkey binding signature was not self signed", e);
            }
            catch (Exception e) {
                logger.error("Error validating the signature", e);
            }

            if (!valid)
            {
                severity = PGPPublicKeyValidatorFailureSeverity.CRITICAL;

                failureMessage = "Key with key id " + PGPUtils.getKeyIDHex(publicKey.getKeyID()) +
                        " is not a valid sub key of " + PGPUtils.getKeyIDHex(masterKey.getKeyID());
            }
        }
        else {
            // Subkey binding check is only relevant for sub keys
            valid = true;
        }

        return new PGPPublicKeyValidatorResultImpl(this, valid, severity, failureMessage);
    }

    public boolean isCheckPrimaryKeyBindingSignature() {
        return checkPrimaryKeyBindingSignature;
    }

    public void setCheckPrimaryKeyBindingSignature(boolean checkPrimaryKeyBindingSignature) {
        this.checkPrimaryKeyBindingSignature = checkPrimaryKeyBindingSignature;
    }
}
