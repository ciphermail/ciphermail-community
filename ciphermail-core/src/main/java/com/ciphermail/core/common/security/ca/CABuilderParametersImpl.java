/*
 * Copyright (c) 2009-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import java.util.Collection;

import javax.security.auth.x500.X500Principal;

public class CABuilderParametersImpl implements CABuilderParameters
{
    private int rootKeyLength = 2048;
    private int intermediateKeyLength = 2048;

    private X500Principal rootSubject;
    private X500Principal intermediateSubject;

    private int rootValidity = 3650;
    private int intermediateValidity = 3650;

    /*
     * Algorithm for signing the certificates
     */
    private String signatureAlgorithm = "SHA256WithRSAEncryption";

    /*
     * URIs of the CRL distribution points
     */
    private Collection<String> crlDistributionPoints;

    @Override
    public void setRootSubject(X500Principal subject) {
        this.rootSubject = subject;
    }

    @Override
    public X500Principal getRootSubject() {
        return rootSubject;
    }

    @Override
    public void setIntermediateSubject(X500Principal subject) {
        this.intermediateSubject = subject;
    }

    @Override
    public X500Principal getIntermediateSubject() {
        return intermediateSubject;
    }

    @Override
    public void setRootValidity(int days)
    {
        if (days <= 0) {
            throw new IllegalArgumentException("days must be > 0");
        }

        rootValidity = days;
    }

    @Override
    public int getRootValidity() {
        return rootValidity;
    }

    @Override
    public void setIntermediateValidity(int days)
    {
        if (days <= 0) {
            throw new IllegalArgumentException("days must be > 0");
        }

        intermediateValidity = days;
    }

    @Override
    public int getIntermediateValidity() {
        return intermediateValidity;
    }

    @Override
    public void setRootKeyLength(int bits)
    {
        if (bits < 1024) {
            throw new IllegalArgumentException("Key length must be >= 1024");
        }

        rootKeyLength = bits;
    }

    @Override
    public int getRootKeyLength() {
        return rootKeyLength;
    }

    @Override
    public void setIntermediateKeyLength(int bits)
    {
        if (bits < 1024) {
            throw new IllegalArgumentException("Key length must be >= 1024");
        }

        intermediateKeyLength = bits;
    }

    @Override
    public int getIntermediateKeyLength() {
        return intermediateKeyLength;
    }

    @Override
    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    @Override
    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }


    @Override
    public void setCRLDistributionPoints(Collection<String> uris) {
        this.crlDistributionPoints = uris;
    }

    @Override
    public Collection<String> getCRLDistributionPoints() {
        return crlDistributionPoints;
    }
}
