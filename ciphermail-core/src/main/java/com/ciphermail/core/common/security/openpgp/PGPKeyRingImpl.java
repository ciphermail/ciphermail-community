/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.certificate.CertificateBuilderException;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.impl.StandardX509CertificateBuilder;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.common.security.password.PasswordProvider;
import com.ciphermail.core.common.util.ByteArray;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.CurrentDateProvider;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.common.util.Result2;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPKeyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * Implementation of PGPKeyRing which will stores the public key material in the database and the private key
 * material in a KeyStore.
 *
 * Note: because this implementation will store public key material into a database, all calls must be wrapped in
 * a database transaction.
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyRingImpl implements PGPKeyRing
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyRingImpl.class);

    /*
     * Some sanity upper bound on the max number of sub keys to search for
     */
    private static final int MAX_SUB_KEYS = 1000;

    /*
     * The name of this key ring
     */
    private final String keyRingName;

    /*
     * The store which contains the private keys
     */
    private final KeyStoreProvider keyStoreProvider;

    /*
     * Provides the password for the key entries
     */
    private final PasswordProvider passwordProvider;

    /*
     * Handles the database sessions
     */
    private final SessionManager sessionManager;

    /*
     * Merges a new key with an existing key
     */
    private final PGPPublicKeyMerger keyMerger;

    /*
     * Used for updating the email addresses associated with a public key
     */
    private final PGPKeyRingEntryEmailUpdater keyRingEntryEmailUpdater;

    /*
     * For converting PGPPrivateKey to PrivateKey
     */
    private final JcaPGPKeyConverter keyConverter;

    /*
     * The registered PGPKeyRingEventListener's
     */
    private final List<PGPKeyRingEventListener> eventListeners = Collections.synchronizedList(new LinkedList<>());

    /*
     * If true, a dummy X509 certificate will be used for the certificate chain of the key entry
     */
    private boolean useDummyCertificate;

    public PGPKeyRingImpl(
            @Nonnull String keyRingName,
            @Nonnull KeyStoreProvider keyStoreProvider,
            @Nonnull PasswordProvider passwordProvider,
            @Nonnull SessionManager sessionManager,
            @Nonnull PGPPublicKeyMerger keyMerger,
            @Nonnull PGPKeyRingEntryEmailUpdater keyRingEntryEmailUpdater)
    {
        this.keyRingName = Objects.requireNonNull(keyRingName);
        this.keyStoreProvider = Objects.requireNonNull(keyStoreProvider);
        this.passwordProvider = Objects.requireNonNull(passwordProvider);
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.keyMerger = Objects.requireNonNull(keyMerger);
        this.keyRingEntryEmailUpdater = Objects.requireNonNull(keyRingEntryEmailUpdater);

        this.keyConverter = new JcaPGPKeyConverter();

        // Private keys stored on an HSM are directly returned by JcaPGPKeyConverter. JcaPGPKeyConverter is only
        // used for converting soft keys
        this.keyConverter.setProvider(PGPSecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());
    }

    private Result2<PGPKeyRingEntryImpl, Boolean> internalAddPGPPublicKey(@Nonnull PGPPublicKey publicKey)
    throws PGPException, IOException, KeyStoreException
    {
        PGPKeyRingEntityDAO dao = createDAO();

        PGPKeyRingEntity entity = dao.getByPublicKey(publicKey);

        PGPKeyRingEntryImpl entry;

        boolean add = false;

        // Skip if there was already an entity for the public key (based on fingerprint).
        if (entity == null)
        {
            entity = new PGPKeyRingEntity(publicKey, keyRingName);

            entry = createPGPKeyRingEntry(entity);

            // Set the email addresses of the newly created entry
            keyRingEntryEmailUpdater.updateEmail(entry, PGPPublicKeyInspector.getUserIDs(publicKey));

            // If the key is a master key, look if there are any sub keys already in the keyring and vice versa
            if (publicKey.isMasterKey())
            {
                CloseableIterator<PGPKeyRingEntity> subKeyIterator = dao.getByParentKeyID(publicKey.getKeyID(),
                        0, MAX_SUB_KEYS);

                try {
                    while (subKeyIterator.hasNext())
                    {
                        PGPKeyRingEntity subKeyEntity = subKeyIterator.next();

                        subKeyEntity.setParentKey(entity);
                        entity.getSubkeys().add(subKeyEntity);
                    }
                }
                catch (CloseableIteratorException e) {
                    throw new PGPException("Error iterating subkeys", e);
                }
                finally {
                    CloseableIteratorUtils.closeQuietly(subKeyIterator);
                }

                add = true;
            }
            else {
                // The key is a sub key. Get the ID of the master key.
                //
                // Note: at this stage, the signature is not checked. This is done when the key is used
                Long parentKeyID = PGPPublicKeyInspector.getSubkeyBindingSignatureKeyID(publicKey);

                if (parentKeyID != null)
                {
                    // Search for a master key that signed the sub key. In theory there can be more hits but this is
                    // extremely unlikely
                    CloseableIterator<PGPKeyRingEntity> masterKeyIterator = dao.getByKeyID(parentKeyID,
                            MissingKeyAlias.ALLOWED);

                    try {
                        while (masterKeyIterator.hasNext())
                        {
                            PGPKeyRingEntity masterKeyEntity = masterKeyIterator.next();

                            entity.setParentKey(masterKeyEntity);
                            masterKeyEntity.getSubkeys().add(entity);
                        }
                    }
                    catch (CloseableIteratorException e) {
                        throw new PGPException("Error iterating subkeys", e);
                    }
                    finally {
                        CloseableIteratorUtils.closeQuietly(masterKeyIterator);
                    }

                    add = true;
                }
                else {

                    logger.atWarn().setMessage("Subkey with key ID {} has no subkey binding signature key ID")
                            .addArgument(PGPUtils.getKeyIDHex(publicKey.getKeyID()))
                            .log();
                }
            }

            if (add)
            {
                dao.persist(entity);

                fireKeyRingEntryAddedEvent(entity);
            }
        }
        else {
            // Key is already in the key ring. We need to check whether there are signatures in the new key
            // which are not yet part of the existing key
            entry = createPGPKeyRingEntry(entity);

            PGPPublicKey merged = keyMerger.merge(entry.getPublicKey(), publicKey);

            if (merged != null)
            {
                // Additional email addresses should not be overwritten we a new (merged) PGP is set. We assume that
                // merging a PGP key only results in adding new User IDs (User IDs should never be removed, only
                // revoked)
                Set<ByteArray> newUserIDs = ByteArray.toSet(
                        PGPPublicKeyInspector.getUserIDs(merged));

                // Remove existing User IDs to get the newly added User IDs
                ByteArray.toList(PGPPublicKeyInspector.getUserIDs(
                        entry.getPublicKey())).forEach(newUserIDs::remove);

                entity.setPGPPublicKey(merged);

                keyRingEntryEmailUpdater.updateEmail(entry, ByteArray.toArrayList(newUserIDs));

                fireKeyRingEntryUpdatedEvent(entity);

                add = true;
            }
        }

        return new Result2<>(entry, add);
    }

    @Override
    public PGPKeyRingEntry getByID(@Nonnull UUID id)
    throws IOException
    {
        try {
            PGPKeyRingEntity entity = createDAO().findById(id, PGPKeyRingEntity.class);

            return entity != null ? createPGPKeyRingEntry(entity) : null;
        }
        catch (KeyStoreException e) {
            throw new IOException(e);
        }
    }

    @Override
    public PGPKeyRingEntry getBySha256Fingerprint(@Nonnull String fingerprint)
    throws IOException
    {
        fingerprint = StringUtils.trimToNull(fingerprint);

        if (fingerprint == null) {
            return null;
        }

        try {
            PGPKeyRingEntity entity = createDAO().getBySHA256Fingerprint(fingerprint);

            return entity != null ? createPGPKeyRingEntry(entity) : null;
        }
        catch (KeyStoreException e) {
            throw new IOException(e);
        }
    }

    @Override
    public PGPKeyRingEntry addPGPPublicKey(@Nonnull PGPPublicKey publicKey)
    throws PGPException, IOException
    {
        try {
            Result2<PGPKeyRingEntryImpl, Boolean> result = internalAddPGPPublicKey(publicKey);

            return Boolean.TRUE.equals(result.getValue2()) ? result.getValue1() : null;
        }
        catch (KeyStoreException e) {
            throw new IOException(e);
        }
    }

    protected X509Certificate generateX509Certificate(@Nonnull PGPKeyRingPair pgpKeyPair)
    throws IOException
    {
        try {
            SecurityFactory securityFactory = PGPSecurityFactoryFactory.getSecurityFactory();

            // Since the dummy cert is not sensitive, we will generate soft keys
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA",
                    securityFactory.getNonSensitiveProvider());

            keyPairGenerator.initialize(1024, securityFactory.createSecureRandom());

            KeyPair certificateKeyPair = keyPairGenerator.generateKeyPair();

            StandardX509CertificateBuilder certificateBuilder = new StandardX509CertificateBuilder(
                    securityFactory.getNonSensitiveProvider(), securityFactory.getNonSensitiveProvider());

            X500PrincipalBuilder issuerBuilder = X500PrincipalBuilder.getInstance();

            issuerBuilder.setCommonName("Certificate for PGP key with key ID " +
                    PGPUtils.getKeyIDHex(pgpKeyPair.getPublicKey().getKeyID()));

            X500Principal issuer = issuerBuilder.buildPrincipal();

            certificateBuilder.setSubject(issuer);
            certificateBuilder.setIssuer(issuer);
            certificateBuilder.setPublicKey(certificateKeyPair.getPublic());
            certificateBuilder.setNotBefore(DateUtils.addDays(new Date(), -1));
            certificateBuilder.setNotAfter(DateUtils.addYears(new Date(), 32));
            certificateBuilder.setSerialNumber(new BigInteger(Long.toString(pgpKeyPair.getPublicKey().getKeyID())));
            certificateBuilder.setSignatureAlgorithm("SHA1WithRSA");

            return certificateBuilder.generateCertificate(certificateKeyPair.getPrivate(), null);
        }
        catch (NoSuchProviderException | CertificateBuilderException | NoSuchAlgorithmException e) {
            throw new IOException(e);
        }
    }

    /*
     * Creates a certificate for the PGP public key to be used by the key entry (a private key entry must contain a
     * certificate chain).
     */
    protected Certificate[] createCertificateChain(@Nonnull PGPKeyRingPair pgpKeyPair)
    throws PGPException, IOException
    {
        // Although a KeyStore should support the generic Certificate class, the IAIK provider only supports
        // X509Certificate's. We will therefore generate a self-signed X509Certificate if useDummyCertificate is set.
        // The downside of this is that all PGP key material is not stored in the key store. This is not a problem per
        // se because all PGP public keys will be stored separately. This is only a problem if the complete PGP public
        // key should be restored from the KeyStore.
        return new Certificate[]{useDummyCertificate ? generateX509Certificate(pgpKeyPair) :
                new PGPCertificate(pgpKeyPair.getPublicKey())};
    }

    @Override
    public PGPKeyRingEntry addKeyPair(@Nonnull PGPKeyRingPair keyPair)
    throws PGPException, IOException
    {
        String keyAlias = null;

        try {
            if (keyPair.getPrivateKey() != null)
            {
                PrivateKey privateKey = keyConverter.getPrivateKey(keyPair.getPrivateKey());

                keyAlias = getKeyAlias(keyPair.getPublicKey());

                getKeyStore().setKeyEntry(keyAlias, privateKey, passwordProvider.getPassword(),
                        createCertificateChain(keyPair));
            }

            Result2<PGPKeyRingEntryImpl, Boolean> result = internalAddPGPPublicKey(keyPair.getPublicKey());

            PGPKeyRingEntryImpl entry = result.getValue1();

            PGPKeyRingEntity entity = entry.getEntity();

            boolean added = result.getValue2();

            // Only set the alias if the alias is not null. We do not want to set the alias to null if we import an
            // updated public key. Also check whether the key alias has changed so we know whether a new key has been
            // imported
            if (keyAlias != null && !keyAlias.equals(entity.getPrivateKeyAlias()))
            {
                entity.setPrivateKeyAlias(keyAlias);

                added = true;
            }

            return added ? result.getValue1() : null;
        }
        catch (KeyStoreException e) {
            throw new IOException(e);
        }
    }

    @Override
    public CloseableIterator<PGPKeyRingEntry> getByKeyID(long keyID)
    throws PGPException, IOException
    {
        return getByKeyID(keyID, MissingKeyAlias.ALLOWED);
    }

    @Override
    public CloseableIterator<PGPKeyRingEntry> getByKeyID(long keyID, MissingKeyAlias missingKeyAlias)
    throws PGPException, IOException
    {
        return new PGPKeyRingEntryIterator(createDAO().getByKeyID(keyID, missingKeyAlias), keyStoreProvider,
                passwordProvider, keyRingEntryEmailUpdater);
    }

    @Override
    public CloseableIterator<PGPKeyRingEntry> getByParentKeyID(long parentKeyID, Integer firstResult,
            Integer maxResults)
    {
        return new PGPKeyRingEntryIterator(createDAO().getByParentKeyID(parentKeyID, firstResult, maxResults),
                keyStoreProvider, passwordProvider, keyRingEntryEmailUpdater);
    }

    @Override
    public long getByParentKeyIDCount(long parentKeyID)
    {
        return createDAO().getByParentKeyIDCount(parentKeyID);
    }

    @Override
    public CloseableIterator<PGPKeyRingEntry> search(@Nonnull PGPSearchField searchField, @Nonnull String searchValue,
            PGPSearchParameters searchParameters, Integer firstResult, Integer maxResults)
    throws PGPException, IOException
    {
        return new PGPKeyRingEntryIterator(createDAO().search(searchField, searchValue, searchParameters, CurrentDateProvider.getNow(),
                firstResult, maxResults), keyStoreProvider, passwordProvider, keyRingEntryEmailUpdater);
    }

    @Override
    public long searchCount(@Nonnull PGPSearchField searchField, @Nonnull String searchValue,
            PGPSearchParameters searchParameters)
    {
        return createDAO().searchCount(searchField, searchValue, searchParameters, CurrentDateProvider.getNow());
    }

    @Override
    public CloseableIterator<PGPKeyRingEntry> getIterator(PGPSearchParameters searchParameters, Integer firstResult,
            Integer maxResults)
    throws PGPException, IOException
    {
        return new PGPKeyRingEntryIterator(createDAO().getIterator(searchParameters, CurrentDateProvider.getNow(),
                firstResult, maxResults), keyStoreProvider, passwordProvider, keyRingEntryEmailUpdater);
    }

    @Override
    public long getCount(PGPSearchParameters searchParameters)
    throws PGPException, IOException
    {
        return createDAO().getCount(searchParameters, CurrentDateProvider.getNow());
    }

    @Override
    public void delete(UUID id)
    throws PGPException, IOException
    {
        PGPKeyRingEntityDAO dao = createDAO();

        PGPKeyRingEntity entity = dao.findById(id, PGPKeyRingEntity.class);

        if (entity != null)
        {
            if (!entity.isMasterKey()) {
                // if this is a sub-key, we need to remove the key from the master key (parent). If not, hibernate
                // re-saves the entity
                entity.getParentKey().getSubkeys().remove(entity);
            }

            dao.delete(entity);

            fireKeyRingEntryDeletedEvent(entity);

            // We need to delete any associated private keys (including sub keys). The sub keys themselves are
            // deleted automatically because of CascadeType.ALL
            deletePrivateKey(entity, new HashSet<>());
        }
    }

    /*
     * Recursively deletes all the private keys from the entity and sub key entities
     */
    private void deletePrivateKey(@Nonnull PGPKeyRingEntity entity, @Nonnull Set<UUID> handled)
    throws IOException
    {
        if (handled.contains(entity.getID()))
        {
            logger.warn("Loop detected. Exiting");

            return;
        }

        handled.add(entity.getID());

        if (entity.getPrivateKeyAlias() != null)
        {
            try {
                getKeyStore().deleteEntry(entity.getPrivateKeyAlias());
            }
            catch (KeyStoreException e) {
                logger.error("Error delting associated private key for key with key ID " + entity.getKeyID(), e);
            }
        }

        // Step through all sub keys and recursively delete all private keys
        Set<PGPKeyRingEntity> subKeys = entity.getSubkeys();

        if (subKeys != null)
        {
            for (PGPKeyRingEntity subKeyEntity : subKeys)
            {
                deletePrivateKey(subKeyEntity, handled);

                // Since the sub keys are automatically deleted because of CascadeType.ALL, we need to
                // fire the deletion event for all sub keys
                fireKeyRingEntryDeletedEvent(subKeyEntity);
            }
        }
    }

    @Override
    public void deleteAll()
    throws IOException
    {
        PGPKeyRingEntityDAO dao = createDAO();

        // We need to delete any associated private keys
        CloseableIterator<PGPKeyRingEntity> iterator = dao.getIterator(new PGPSearchParameters(),
                null, null, null);

        try {
            try {
                while (iterator.hasNext())
                {
                    PGPKeyRingEntity entity = iterator.next();

                    // Only delete master entries. The master entry will auto delete the childs. If we also delete
                    // the child's, we get a org.hibernate.StaleStateException
                    if (entity.isMasterKey()) {
                        dao.delete(entity);
                    }

                    fireKeyRingEntryDeletedEvent(entity);

                    if (entity.getPrivateKeyAlias() != null)
                    {
                        try {
                            getKeyStore().deleteEntry(entity.getPrivateKeyAlias());
                        }
                        catch (KeyStoreException e)
                        {
                            logger.error("Error delting associated private key for key with key ID " +
                                    entity.getKeyID(), e);
                        }
                    }
                }
            }
            finally {
                // must close the iterator
                iterator.close();
            }
        }
        catch (CloseableIteratorException e) {
            throw new IOException("error deleting private keys", e);
        }
    }

    @Override
    public long getSize() {
        return createDAO().getSize();
    }

    private KeyStore getKeyStore()
    throws KeyStoreException
    {
        return keyStoreProvider.getKeyStore();
    }

    private PGPKeyRingEntryImpl createPGPKeyRingEntry(@Nonnull PGPKeyRingEntity entity)
    throws KeyStoreException
    {
        return new PGPKeyRingEntryImpl(entity, getKeyStore(), passwordProvider, keyRingEntryEmailUpdater);
    }

    private PGPKeyRingEntityDAO createDAO() {
        return PGPKeyRingEntityDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), keyRingName);
    }

    private String getKeyAlias(@Nonnull PGPPublicKey publicKey)
    throws IOException
    {
        // The key alias will be based on the SHA256 fingerprint
        try {
            return KEY_ALIAS_PREFIX + PGPPublicKeyInspector.getSHA256FingerprintHex(publicKey);
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void addPGPKeyRingEventListener(PGPKeyRingEventListener listener)
    {
        if (listener != null) {
            eventListeners.add(listener);
        }
    }

    private void fireKeyRingEntryDeletedEvent(PGPKeyRingEntity entity)
    throws IOException
    {
        if (entity != null)
        {
            logger.atInfo().setMessage("Key with key id {} deleted")
                    .addArgument(PGPUtils.getKeyIDHex(entity.getKeyID()))
                    .log();

            try {
                PGPKeyRingEntry entry = createPGPKeyRingEntry(entity);

                // Note: we assume that registerPGPKeyRingEventListener is called at startup of the application and not
                // during the use of the KeyRing. We will therefore not clone the eventListeners for performance reasons.
                // If registerPGPKeyRingEventListener needs to be called during normal runtime,  we need to either clone
                // the eventListeners or synchronize to prevent any possible ConcurrentModificationException's.
                for (PGPKeyRingEventListener listener : eventListeners) {
                    listener.onKeyRingEntryDeleted(entry);
                }
            }
            catch (KeyStoreException e) {
                throw new IOException(e);
            }
        }
    }

    private void fireKeyRingEntryAddedEvent(PGPKeyRingEntity entity)
    throws IOException
    {
        if (entity != null)
        {
            logger.atInfo().setMessage("Key with key id {} added")
                    .addArgument(PGPUtils.getKeyIDHex(entity.getKeyID()))
                    .log();

            try {
                PGPKeyRingEntry entry = createPGPKeyRingEntry(entity);

                // Note: we assume that registerPGPKeyRingEventListener is called at startup of the application and not
                // during the use of the KeyRing. We will therefore not clone the eventListeners for performance reasons.
                // If registerPGPKeyRingEventListener needs to be called during normal runtime,  we need to either clone
                // the eventListeners or synchronize to prevent any possible ConcurrentModificationException's.
                for (PGPKeyRingEventListener listener : eventListeners) {
                    listener.onKeyRingEntryAdded(entry);
                }
            }
            catch (KeyStoreException e) {
                throw new IOException(e);
            }
        }
    }

    private void fireKeyRingEntryUpdatedEvent(PGPKeyRingEntity entity)
    throws IOException
    {
        if (entity != null)
        {
            logger.atInfo().setMessage("Key with key id {} updated")
                    .addArgument(PGPUtils.getKeyIDHex(entity.getKeyID()))
                    .log();

            try {
                PGPKeyRingEntry entry = createPGPKeyRingEntry(entity);

                // Note: we assume that registerPGPKeyRingEventListener is called at startup of the application and not
                // during the use of the KeyRing. We will therefore not clone the eventListeners for performance reasons.
                // If registerPGPKeyRingEventListener needs to be called during normal runtime,  we need to either clone
                // the eventListeners or synchronize to prevent any possible ConcurrentModificationException's.
                for (PGPKeyRingEventListener listener : eventListeners) {
                    listener.onKeyRingEntryUpdated(entry);
                }
            }
            catch (KeyStoreException e) {
                throw new IOException(e);
            }
        }
    }

    public boolean isUseDummyCertificate() {
        return useDummyCertificate;
    }

    public void setUseDummyCertificate(boolean useDummyCertificate) {
        this.useDummyCertificate = useDummyCertificate;
    }
}
