/*
 * Copyright (c) 2014-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.password.PasswordProvider;
import com.ciphermail.core.common.util.EOFInputStream;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

/**
 * Parses PGP key rings
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyRingParser
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyRingParser.class);

    /**
     * Event listener which is called when a key (or keypair) is found
     */
    public interface KeyEventListener
    {
        /**
         * Called when a key (or key pair) is parsed. publicKey is never null. privateKey is null if there is
         * no private key
         */
        void handleKey(PGPPublicKey publicKey, PGPPrivateKey privateKey)
        throws IOException, PGPException;
    }

    /**
     * Event listener which is called when a keyring is found
     */
    public interface KeyRingEventListener
    {
        /**
         * Called when a secret key ring is found
         */
        void handleSecretKeyRing(PGPSecretKeyRing secretKeyRing, PasswordProvider passwordProvider)
        throws IOException, PGPException;

        /**
         * Called when a public key ring is found
         */
        void handlePublicKeyRing(PGPPublicKeyRing publicKeyRing)
        throws IOException, PGPException;
    }

    /*
     * If true, and a key cannot be imported because it is corrupt or invalid, the key will be skipped
     */
    private boolean ignoreParsingErrors = true;

    /*
     * Called whenever a key (or key pair) is found
     */
    private KeyEventListener keyEventListener;

    /*
     * Called whenever a keyring is found
     */
    private KeyRingEventListener keyRingEventListener;

    /*
     * The total number of key rings that were parsed
     */
    private int parsed;

    /*
     * The total number of key rings that failed to parse
     */
    private int parseFailures;

    /*
     * Note: this method is private because importing a PGPSecretKeyRing which is not created from an encoded
     * secret key ring can fail. If a PGPSecretKeyRing is created at runtime, the returned public keys might loose
     * signed sub packets if they are encoded.
     */
    private void parseSecretKeyRing(@Nonnull PGPSecretKeyRing secretKeyRing, PasswordProvider passwordProvider)
    throws IOException, PGPException
    {
        if (keyRingEventListener != null) {
            keyRingEventListener.handleSecretKeyRing(secretKeyRing, passwordProvider);
        }

        if (keyEventListener != null)
        {
            Iterator<?> secretkeyRingIterator = secretKeyRing.getSecretKeys();

            if (secretkeyRingIterator != null)
            {
                while (secretkeyRingIterator.hasNext())
                {
                    Object obj = secretkeyRingIterator.next();

                    if (obj instanceof PGPSecretKey secretKey)
                    {
                        String keyID = null;

                        try {
                            PGPPublicKey publicKey = secretKey.getPublicKey();

                            keyID = PGPUtils.getKeyIDHex(publicKey.getKeyID());

                            keyEventListener.handleKey(publicKey, PGPKeyUtils.extractPrivateKey(secretKey,
                                    passwordProvider));
                        }
                        catch (Exception e)
                        {
                            logger.error("Error parsing PGP keypair with key ID {}", keyID, e);

                            if (!ignoreParsingErrors) {
                                throw new IOException("Error parsing PGP keypair with key ID " + keyID, e);
                            }
                        }
                    }
                }
            }
        }
    }

    private void parsePublicKeyRing(@Nonnull PGPPublicKeyRing publicKeyRing)
    throws IOException, PGPException
    {
        if (keyRingEventListener != null) {
            keyRingEventListener.handlePublicKeyRing(publicKeyRing);
        }

        if (keyEventListener != null)
        {
            Iterator<?> publicRingIterator = publicKeyRing.getPublicKeys();

            if (publicRingIterator != null)
            {
                while (publicRingIterator.hasNext())
                {
                    Object obj = publicRingIterator.next();

                    if (obj instanceof PGPPublicKey publicKey)
                    {
                        String keyID = null;

                        try {
                            keyID = PGPUtils.getKeyIDHex(publicKey.getKeyID());

                            keyEventListener.handleKey(publicKey, null);
                        }
                        catch (Exception e)
                        {
                            logger.error("Error parsing PGP keypair with key ID {}", keyID, e);

                            if (!ignoreParsingErrors) {
                                throw new IOException("Error parsing PGP public key with key ID " + keyID, e);
                            }
                        }
                    }
                }
            }
        }
    }

    public void rethrow(Exception e)
    throws IOException, PGPException
    {
        if (e instanceof IOException ioException) {
            throw ioException;
        }
        else if (e instanceof PGPException pgpException) {
            throw pgpException;
        }
        else {
            throw new IOException(e);
        }
    }

    public void parseKeyRing(@Nonnull InputStream input, PasswordProvider passwordProvider)
    throws IOException, PGPException
    {
        parsed = 0;
        parseFailures = 0;

        try {
            if (!input.markSupported()) {
                input = new BufferedInputStream(input);
            }

            EOFInputStream eofInputStream = new EOFInputStream(input);

            while (!eofInputStream.isEOF())
            {
                PGPObjectFactory factory = new JcaPGPObjectFactory(PGPUtil.getDecoderStream(eofInputStream));

                Object obj;

                try {
                    while ((obj = factory.nextObject()) != null)
                    {
                        if (obj instanceof PGPPublicKeyRing pgpPublicKeyRing)
                        {
                            parsePublicKeyRing(pgpPublicKeyRing);

                            parsed++;
                        }
                        else if (obj instanceof PGPSecretKeyRing pgpSecretKeyRing)
                        {
                            parseSecretKeyRing(pgpSecretKeyRing, passwordProvider);

                            parsed++;
                        }
                    }
                }
                catch (Exception e)
                {
                    parseFailures++;

                    logger.error("Error PGPObjectFactory#nextObject", e);

                    if (!ignoreParsingErrors) {
                        rethrow(e);
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Error parsing keyring", e);

            if (!ignoreParsingErrors) {
                rethrow(e);
            }
        }
    }

    public boolean isIgnoreParsingErrors() {
        return ignoreParsingErrors;
    }

    public void setIgnoreParsingErrors(boolean ignoreParsingErrors) {
        this.ignoreParsingErrors = ignoreParsingErrors;
    }

    public KeyEventListener getKeyEventListener() {
        return keyEventListener;
    }

    public void setKeyEventListener(KeyEventListener keyEventListener) {
        this.keyEventListener = keyEventListener;
    }

    public KeyRingEventListener getKeyRingEventListener() {
        return keyRingEventListener;
    }

    public void setKeyRingEventListener(KeyRingEventListener keyRingEventListener) {
        this.keyRingEventListener = keyRingEventListener;
    }

    public int getParsed() {
        return parsed;
    }

    public int getParseFailures() {
        return parseFailures;
    }
}
