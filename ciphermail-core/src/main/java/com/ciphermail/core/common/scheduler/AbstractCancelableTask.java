/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractCancelableTask implements CancelableTask
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractCancelableTask.class);

    private final String name;
    private final Object lock = new Object();

    private boolean canceled;
    private boolean hasRun;
    private CancelEvent cancelEvent;

    protected AbstractCancelableTask()
    {
        this.name = "AbstractCancelableTask";
    }

    protected AbstractCancelableTask(String name)
    {
        this.name = name;
    }

    @Override
    public void run()
    {
        logger.warn("Task '{}' started.", name);

        synchronized (lock) {
            hasRun = true;
        }

        try {
            doRun();
        }
        catch(Exception e) {
            logger.error("Uncaught exception.", e);
        }
    }

    public abstract void doRun()
    throws Exception;

    @Override
    public boolean cancel()
    {
        synchronized (lock) {
            canceled = true;
        }

        if (cancelEvent != null) {
            return cancelEvent.doCancel();
        }

        return false;
    }

    @Override
    public boolean isCanceled()
    {
        synchronized (lock) {
            return canceled;
        }
    }

    @Override
    public boolean hasRun()
    {
        synchronized (lock) {
            return hasRun;
        }
    }

    @Override
    public void setCancelListener(CancelEvent event) {
        this.cancelEvent = event;
    }

    @Override
    public String getName() {
        return name;
    }
}
