/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.repository;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * MailRepository stores MailRepositoryItem's.
 *
 * @author Martijn Brinkers
 *
 */
public interface MailRepository
{
    /**
     * The name of the repository
     */
    String getName();

    /**
     * Creates, but not adds, a new MailRepositoryItem.
     * @throws IOException
     * @throws MessagingException
     */
    @Nonnull MailRepositoryItem createItem(@Nonnull MimeMessage message)
    throws MessagingException, IOException;

    /**
     * Adds the MailRepositoryItem. The ID of the MailRepositoryItem should not already been
     * added (i.e. the ID should be unique).
     */
    void addItem(@Nonnull MailRepositoryItem item);

    /**
     * Deletes the item with the given id.
     */
    void deleteItem(@Nonnull UUID id);

    /**
     * Returns the item with the id. Null if item is not found.
     */
    MailRepositoryItem getItem(@Nonnull UUID id);

    /**
     * Returns maxResults items starting at firstResult sorted on creation date.
     */
    List<? extends MailRepositoryItem> getItems(Integer firstResult, Integer maxResults);

    /**
     * Returns items that were created before the given date sorted on creation date
     */
    List<? extends MailRepositoryItem> getItemsBefore(@Nonnull Date before, Integer firstResult, Integer maxResults);

    /**
     * Returns maxResults items starting at firstResult with the given search sorted on creation date.
     */
    List<? extends MailRepositoryItem> searchItems(@Nonnull MailRepositorySearchField searchField, @Nonnull String key,
            Integer firstResult, Integer maxResults);

    /**
     * Returns the number of items in the repository
     */
    long getItemCount();

    /**
     * Returns the number of items in the repository with the given search.
     */
    long getSearchCount(@Nonnull MailRepositorySearchField searchField, @Nonnull String key);

    /**
     * Deletes all items.
     */
    void deleteAll();
}
