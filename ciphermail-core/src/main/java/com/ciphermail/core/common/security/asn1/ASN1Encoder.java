/*
 * Copyright (c) 2008-2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.asn1;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.pkcs.ContentInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.SignedData;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collection;

/**
 * Some general ASN1 encoding functions
 *
 * @author Martijn Brinkers
 *
 */
public class ASN1Encoder
{
    private ASN1Encoder() {
        // empty on purpose
    }

    /**
     * Taken from org.bouncycastle.jce.provider.PKIXCertPath.
     * <p>
     * See ftp://ftp.rsasecurity.com/pub/pkcs/ascii/pkcs-7.asc for info on PKCS#7 encoding
     */
    public static byte[] encodePKCS7(ASN1EncodableVector certificatesVector, ASN1EncodableVector crlsVector)
    throws IOException
    {
        ContentInfo dataContentInfo = new ContentInfo(PKCSObjectIdentifiers.data, null);

        ASN1Integer version = new ASN1Integer(1);
        ASN1Set digestAlgorithms = new DERSet();
        ASN1Set signerInfos = new DERSet();
        ASN1Set crls = null;
        ASN1Set certificates = null;

        if (certificatesVector != null)
        {
            // pre-sort the asn1Certificates vector with a much faster method then DERSet uses
            ASN1EncodableVector sortedASN1Certificates = DERUtils.sortASN1EncodableVector(certificatesVector);
            certificates = new DERSet(sortedASN1Certificates);
        }

        if (crlsVector != null)
        {
            // pre-sort the asn1Certificates vector with a much faster method then DERSet uses
            ASN1EncodableVector sortedASN1CRLs = DERUtils.sortASN1EncodableVector(crlsVector);
            crls = new DERSet(sortedASN1CRLs);
        }

        SignedData signedData = new SignedData(version, digestAlgorithms, dataContentInfo,
                certificates, crls, signerInfos);

        ContentInfo signedContentInfo = new ContentInfo(PKCSObjectIdentifiers.signedData, signedData);

        return DERUtils.toByteArray(signedContentInfo);
    }

    /**
     * Encodes a collection using PEM encoding
     */
    public static byte[] encodePEM(@Nonnull Collection<?> elements)
    throws IOException
    {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        JcaPEMWriter writer = new JcaPEMWriter(new OutputStreamWriter(output));

        for (Object element : elements) {
            writer.writeObject(element);
        }

        writer.close();

        return output.toByteArray();
    }
}
