/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore.hibernate;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import com.ciphermail.core.common.hibernate.CertPathUserType;
import com.ciphermail.core.common.hibernate.X509CertificateUserType;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.CompositeType;
import org.hibernate.annotations.UuidGenerator;

import java.io.IOException;
import java.security.cert.CertPath;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * Entity class for storing a certificate with a possible CertPath and key alias into the database.
 *
 * @author Martijn Brinkers
 *
 */
@Entity(name = X509CertStoreEntity.ENTITY_NAME)
@Table(
uniqueConstraints = {@UniqueConstraint(columnNames={X509CertStoreEntity.STORE_NAME_COLUMN,
        X509CertStoreEntity.THUMBPRINT_COLUMN})},
indexes = {
    @Index(name = "certificate_store_name_index",     columnList = X509CertStoreEntity.STORE_NAME_COLUMN),
    @Index(name = "certificate_issuer_index",         columnList = X509CertStoreEntity.ISSUER_COLUMN),
    @Index(name = "certificate_serial_index",         columnList = X509CertStoreEntity.SERIAL_COLUMN),
    @Index(name = "certificate_subject_key_id_index", columnList = X509CertStoreEntity.SUBJECT_KEY_IDENTIFIER_COLUMN),
    @Index(name = "certificate_subject_index",        columnList = X509CertStoreEntity.SUBJECT_COLUMN),
    @Index(name = "certificate_key_alias_index",      columnList = X509CertStoreEntity.KEY_ALIAS_COLUMN),
    @Index(name = "certificate_thumbprint_index",     columnList = X509CertStoreEntity.THUMBPRINT_COLUMN),
    @Index(name = "certificate_creationdate_index",   columnList = X509CertStoreEntity.CREATION_DATE_COLUMN)
})
public class X509CertStoreEntity implements X509CertStoreEntry
{
    public static final String ENTITY_NAME = "Certificate";

    /**
     * Name of columns which are referenced from other classes
     */
    public static final String STORE_NAME_COLUMN = "storeName";
    public static final String NOT_BEFORE_COLUMN = X509CertificateUserType.NOT_BEFORE_COLUMN_NAME;
    public static final String NOT_AFTER_COLUMN = X509CertificateUserType.NOT_AFTER_COLUMN_NAME;
    public static final String ISSUER_COLUMN = X509CertificateUserType.ISSUER_COLUMN_NAME;
    public static final String ISSUER_FRIENDLY_COLUMN = X509CertificateUserType.ISSUER_FRIENDLY_COLUMN_NAME;
    public static final String SERIAL_COLUMN = X509CertificateUserType.SERIAL_NUMBER_COLUMN_NAME;
    public static final String SUBJECT_KEY_IDENTIFIER_COLUMN = X509CertificateUserType.SUBJECT_KEY_IDENT_COLUMN_NAME;
    public static final String SUBJECT_COLUMN = X509CertificateUserType.SUBJECT_COLUMN_NAME;
    public static final String SUBJECT_FRIENDLY_COLUMN = X509CertificateUserType.SUBJECT_FRIENDLY_COLUMN_NAME;
    public static final String THUMBPRINT_COLUMN = X509CertificateUserType.THUMBPRINT_COLUMN_NAME;
    public static final String CREATION_DATE_COLUMN = "creationDate";
    public static final String KEY_ALIAS_COLUMN = "keyAlias";

    /*
     * Name of the certificate X509CertificateUserType
     */
    public static final String CERTIFICATE_USER_TYPE_NAME = "certificate";

    /*
     * Maximum length of an email address.
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_EMAIL_LENGTH = 64 + 1 + 255;

    /*
     * 256K for a certificate should be more than plenty
     */
    private static final int MAX_CERTIFICATE_SIZE = SizeUtils.KB * 256;

    @Id
    @Column(name = "id")
    @UuidGenerator
    private UUID id;

    /**
     * The store name will be used to store multiple certificate stores in the database
     */
    @Column (name = STORE_NAME_COLUMN, length = 255, unique = false, nullable = true)
    private String storeName;

    @Embedded
    @AttributeOverride(name = X509CertificateUserType.CERTIFICATE_COLUMN_NAME, column = @Column(length = MAX_CERTIFICATE_SIZE))
    @AttributeOverride(name = ISSUER_COLUMN, column = @Column(length = SizeUtils.KB * 32))
    @AttributeOverride(name = ISSUER_FRIENDLY_COLUMN, column = @Column(length = SizeUtils.KB * 32))
    // The maximum length of a serial number. Should be at least 20 octets
    // (see http://www.ietf.org/rfc/rfc3280.txt appendix B)
    @AttributeOverride(name = SERIAL_COLUMN, column = @Column(length = SizeUtils.KB))
    @AttributeOverride(name = SUBJECT_COLUMN, column = @Column(length = SizeUtils.KB * 32))
    @AttributeOverride(name = SUBJECT_FRIENDLY_COLUMN, column = @Column(length = SizeUtils.KB * 32))
    @AttributeOverride(name = SUBJECT_KEY_IDENTIFIER_COLUMN, column = @Column(length = SizeUtils.KB))
    @AttributeOverride(name = THUMBPRINT_COLUMN, column = @Column(length = 255))
    @CompositeType(X509CertificateUserType.class)
    private X509Certificate certificate;

    /*
     * The certificate chain.
     */
    @Embedded
    @AttributeOverride(name = CertPathUserType.CERT_PATH_COLUMN_NAME, column = @Column(length = MAX_CERTIFICATE_SIZE * 10, nullable = true))
    @AttributeOverride(name = CertPathUserType.CERT_PATH_TYPE_COLUMN_NAME, column = @Column(length = 255, unique = false, nullable = true))
    @CompositeType(CertPathUserType.class)
    private CertPath certificatePath;

    /*
     * The date the chain was updated
     */
    @Column (name = "datePathUpdated", unique = false, nullable = true)
    private Date datePathUpdated;

    /*
     * Date this entry was created
     */
    @Column (name = CREATION_DATE_COLUMN, unique = false, nullable = true)
    private Date creationDate;

    /*
     * A collection of email addresses from the certificate.
     */
    @ElementCollection
    @JoinTable(name = "CertificatesEmail",
            joinColumns = @JoinColumn(name = "CertificatesId"))
    @Column(name = "email", length = MAX_EMAIL_LENGTH)
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<String> email = new HashSet<>();

    /*
     * If this certificate has an associated private key this will be the alias
     */
    @Column (name = KEY_ALIAS_COLUMN, length = 1024, unique = false, nullable = true)
    private String keyAlias;

    public X509CertStoreEntity(X509Certificate certificate, String keyAlias, String storeName, Date creationDate)
    throws IOException
    {
        this.certificate = Objects.requireNonNull(certificate);
        this.keyAlias = keyAlias;
        this.storeName = storeName;
        this.creationDate = creationDate;

        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        List<String> unfilteredEmail = inspector.getEmail();

        // filter and normalize the email addresses to make sure that we only add
        // valid email addresses
        email.addAll(EmailAddressUtils.canonicalizeAndValidate(unfilteredEmail, true));
    }

    protected X509CertStoreEntity() {
        // Hibernate requires a default constructor
    }

    public String getStoreName() {
        return storeName;
    }

    @Override
    public X509Certificate getCertificate() {
        return certificate;
    }

    @Override
    public void setCertificatePath(CertPath certificatePath) {
        this.certificatePath = certificatePath;
    }

    @Override
    public CertPath getCertificatePath() {
        return certificatePath;
    }

    @Override
    public void setDatePathUpdated(Date date) {
        this.datePathUpdated = date;
    }

    @Override
    public Date getDatePathUpdated() {
        return datePathUpdated;
    }

    @Override
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public Date getCreationDate() {
        return creationDate;
    }

    @Override
    public Set<String> getEmail() {
        return email;
    }

    @Override
    public void setKeyAlias(String keyAlias) {
        this.keyAlias = keyAlias;
    }

    @Override
    public String getKeyAlias() {
        return keyAlias;
    }

    @Override
    public String toString() {
        return certificate.toString();
    }

    /**
     * X509CertStoreEntry is equal if and only if the certificates and storeNames are equal
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof X509CertStoreEntity rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(certificate, rhs.certificate)
            .append(storeName, rhs.storeName)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(certificate)
            .append(storeName)
            .toHashCode();
    }
}
