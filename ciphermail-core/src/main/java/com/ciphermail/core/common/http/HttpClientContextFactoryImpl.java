/*
 * Copyright (c) 2021-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.http;

import org.apache.commons.lang.time.DateUtils;
import org.apache.hc.client5.http.auth.CredentialsStore;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.util.Timeout;

public class HttpClientContextFactoryImpl implements HttpClientContextFactory
{
    private long connectionRequestTimeout = 30 * DateUtils.MILLIS_PER_SECOND;
    private long responseTimeout = 30 * DateUtils.MILLIS_PER_SECOND;

    /*
     * The store containing credentials for authenticating. This is also used for the proxy credentials
     */
    private final CredentialsStore credentialsStore;

    public HttpClientContextFactoryImpl(CredentialsStore credentialsStore) {
        this.credentialsStore = credentialsStore;
    }

    public HttpClientContextFactoryImpl() {
        this(null);
    }

    @Override
    public HttpClientContext createHttpClientContext()
    {
        HttpClientContext clientContext = HttpClientContext.create();

        if (credentialsStore != null) {
            clientContext.setCredentialsProvider(credentialsStore);
        }

        RequestConfig.Builder requestConfigBuilder = RequestConfig.custom();

        requestConfigBuilder.setConnectionRequestTimeout(Timeout.ofMilliseconds(connectionRequestTimeout))
            .setResponseTimeout(Timeout.ofMilliseconds(responseTimeout));

        clientContext.setRequestConfig(requestConfigBuilder.build());

        return clientContext;
    }

    @Override
    public CredentialsStore getCredentialsStore() {
        return credentialsStore;
    }

    public long getConnectionRequestTimeout() {
        return connectionRequestTimeout;
    }

    public void setConnectionRequestTimeout(long connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public long getResponseTimeout() {
        return responseTimeout;
    }

    public void setResponseTimeout(long responseTimeout) {
        this.responseTimeout = responseTimeout;
    }
}
