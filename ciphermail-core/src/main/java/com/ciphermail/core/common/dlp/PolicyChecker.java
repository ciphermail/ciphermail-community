/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp;


import com.ciphermail.core.common.util.RequiredByJames;

import javax.annotation.Nonnull;

/**
 * A PolicyChecker checks whether a policy is violated. A PolicyChecker might be called multiple times with
 * the same context instance but possibly different context content. If state should be kept, any state can be stored
 * in the context. For example a PolicyChecker can be called multiple times when the input to check is too large to
 * check at once.
 *
 * Note: A PolicyChecker implementation should be thread safe. If the PolicyChecker needs to store data during
 * init or update, it should store the data in the provided PolicyCheckerContext instance.
 *
 * @author Martijn Brinkers
 *
 */
@RequiredByJames
public interface PolicyChecker
{
    /**
     * The name of this PolicyChecker
     */
    String getName();

    /**
     * init is called the first time the PolicyChecker is used to give the PolicyChecker a chance to
     * initialize itself.
     */
    void init(@Nonnull PolicyCheckerContext context);

    /**
     * Provides the PolicyChecker with possible new data. The context will contain the data required by the
     * PolicyChecker implementation to check the policy (for example the message content and the regular expressions
     * to check). Update can be called multiple times with updated context content. The context will be the same
     * instance for every call to update until finish is called. The context can be used to temporarily store state
     * between consecutive calls to update.
     *
     * @Throws PolicyViolationException if this policy is violated.
     */
    void update(@Nonnull PolicyCheckerContext context);

    /**
     * Finish is called when all data, via calls to update, is provided. Finish will check the policy and throws
     * a PolicyViolationException when the policy has been violated.
     *
     * @Throws PolicyViolationException if this policy is violated.
     */
    void finish(@Nonnull PolicyCheckerContext context)
    throws PolicyViolationException;
}
