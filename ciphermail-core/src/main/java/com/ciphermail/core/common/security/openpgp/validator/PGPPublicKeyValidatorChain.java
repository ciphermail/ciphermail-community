/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.util.Context;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Chain of PGPPublicKeyValidator's
 *
 * @author Martijn Brinkers
 *
 */
public class PGPPublicKeyValidatorChain implements PGPPublicKeyValidator
{
    /*
     * The chain of PGPPublicKeyValidator's which are being called one after the other until a failure
     */
    private final List<PGPPublicKeyValidator> chain = new LinkedList<>();

    public PGPPublicKeyValidatorChain() {
        // empty on purpose
    }

    public PGPPublicKeyValidatorChain(List<PGPPublicKeyValidator> validators)
    {
        if (validators != null) {
            chain.addAll(validators);
        }
    }

    public void addValidators(@Nonnull PGPPublicKeyValidator... validators) {
        chain.addAll(Arrays.asList(validators));
    }

    @Override
    public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
    {
        PGPPublicKeyValidatorResult result = null;

        for (PGPPublicKeyValidator validator : chain)
        {
            PGPPublicKeyValidatorResult newResult = validator.validate(publicKey, context);

            if (!newResult.isValid())
            {
                // Check if the severity of the new result is higher than the stored one
                if (result == null || newResult.getFailureSeverity().compareTo(result.getFailureSeverity()) > 0) {
                    result = newResult;
                }

                if (result.getFailureSeverity() == PGPPublicKeyValidatorFailureSeverity.CRITICAL) {
                    // We found a critical severity so we can stop
                    break;
                }
            }
        }

        if (result == null) {
            result = new PGPPublicKeyValidatorResultImpl(this, true);
        }

        return result;
    }
}
