/*
 * Copyright (c) 2008-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.http.AbstractVoidBinResponseConsumer;
import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactory;
import com.ciphermail.core.common.http.HttpClientContextFactory;
import com.ciphermail.core.common.util.LimitReachedException;
import com.ciphermail.core.common.util.ReadableOutputStreamBuffer;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleRequestBuilder;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ConnectionClosedException;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.nio.support.BasicRequestProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.security.GeneralSecurityException;
import java.security.NoSuchProviderException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class HTTPCRLDownloadHandler implements CRLDownloadHandler
{
    private static final Logger logger = LoggerFactory.getLogger(HTTPCRLDownloadHandler.class);

    private static final String TIMEOUT_ERROR = "A timeout has occurred while downloading CRL from: ";

    /*
     * The default size at which a downloaded CRL will be streamed to disk
     */
    private static final int DEFAULT_MEM_THRESHOLD = SizeUtils.MB * 5;

    /*
     * Creates CloseableHttpAsyncClient instances
     */
    private final CloseableHttpAsyncClientFactory httpClientFactory;

    /*
     * Creates HttpClientContextFactory instances
     */
    private final HttpClientContextFactory httpClientContextFactory;

    /*
     * The HTTP client instance responsible for HTTP(s) communication
     */
    private CloseableHttpAsyncClient sharedHttpClient;

    /*
     * The size at which a downloaded CRL will be streamed to disk
     */
    private int memThreshold = DEFAULT_MEM_THRESHOLD;

    /*
     * The max size of a downloaded CRL in bytes.
     */
    private int maxCRLSize = SizeUtils.MB * 25;

    /*
     * The max time a downloaded CRL may take in milliseconds.
     */
    private long totalTimeout = 15 * DateUtils.MILLIS_PER_MINUTE;

    public HTTPCRLDownloadHandler(@Nonnull CloseableHttpAsyncClientFactory httpClientFactory,
            @Nonnull HttpClientContextFactory httpClientContextFactory)
    {
        this.httpClientFactory = Objects.requireNonNull(httpClientFactory);
        this.httpClientContextFactory = Objects.requireNonNull(httpClientContextFactory);
    }

    private synchronized CloseableHttpAsyncClient getHTTPClient()
    throws IOException, GeneralSecurityException
    {
        if (sharedHttpClient == null) {
            sharedHttpClient = httpClientFactory.createClient();
        }

        return sharedHttpClient;
    }

    @Override
    public boolean canHandle(@Nonnull URI uri)
    {
        String scheme = uri.getScheme();

        return scheme != null && (scheme.equalsIgnoreCase("http") || scheme.equalsIgnoreCase("https"));
    }

    @Override
    public Collection<? extends CRL> downloadCRLs(@Nonnull URI uri)
    throws IOException, CRLException
    {
        // sanity check
        if (!canHandle(uri)) {
            throw new IllegalArgumentException("The uri cannot be handled by this handler.");
        }

        Collection<? extends CRL> crls;

        CloseableHttpAsyncClient httpCLient;

        try {
            httpCLient = getHTTPClient();
        }
        catch (GeneralSecurityException e) {
            throw new IOException(e);
        }

        HttpClientContext clientContext = httpClientContextFactory.createHttpClientContext();

        SimpleHttpRequest request = SimpleRequestBuilder.get(uri).build();

        try(
            ReadableOutputStreamBuffer output = new ReadableOutputStreamBuffer(memThreshold);

            WritableByteChannel outputChannel = Channels.newChannel(new SizeLimitedOutputStream(output,
                    maxCRLSize)))
        {
            AbstractVoidBinResponseConsumer consumer = new AbstractVoidBinResponseConsumer()
            {
                @Override
                protected void data(ByteBuffer src, boolean endOfStream)
                throws IOException
                {
                    try {
                        outputChannel.write(src);
                    }
                    catch (LimitReachedException e) {
                        // If the limit was reached, we do not want HTTPClient to retry. The
                        // @DefaultHttpRequestRetryStrategy will not retry if the exception is a
                        // ConnectionClosedException
                        throw new ConnectionClosedException("Max CRL size reached.", e);
                    }
                }
            };

            Future<Void> future = httpCLient.execute(new BasicRequestProducer(request, null), consumer, clientContext,
                    null);

            future.get(totalTimeout, TimeUnit.MILLISECONDS);

            if (consumer.getHttpStatusCode() != HttpStatus.SC_OK) {
                throw new IOException("Connection failed. HTTP status code: " + consumer.getHttpStatusCode());
            }

            InputStream input = output.getInputStream();

            try {
                crls = CRLUtils.readCRLs(input);
            }
            catch (NoSuchProviderException e) {
                // Should never happen
                throw new UnhandledException(e);
            }
            catch (CertificateException  e) {
                throw new CRLException("CRL was invalid", e);
            }
            finally {
                IOUtils.closeQuietly(input);
            }

            if (crls == null || crls.isEmpty()) {
                logger.debug("No CRLs found in the downloaded stream.");
            }
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();

            throw new IOException(e);
        }
        catch (ExecutionException e) {
            throw new IOException(ExceptionUtils.getRootCause(e));
        }
        catch (TimeoutException e) {
            throw new IOException(TIMEOUT_ERROR + uri);
        }

        return crls;
    }

    public int getMemThreshold() {
        return memThreshold;
    }

    public void setMemThreshold(int memThreshold) {
        this.memThreshold = memThreshold;
    }

    public int getMaxCRLSize() {
        return maxCRLSize;
    }

    public void setMaxCRLSize(int maxCRLSize) {
        this.maxCRLSize = maxCRLSize;
    }

    public long getTotalTimeout() {
        return totalTimeout;
    }

    public void setTotalTimeout(long totalTimeout) {
        this.totalTimeout = totalTimeout;
    }

    public void shutdown()
    {
        if (sharedHttpClient != null) {
            sharedHttpClient.initiateShutdown();
        }
    }
}
