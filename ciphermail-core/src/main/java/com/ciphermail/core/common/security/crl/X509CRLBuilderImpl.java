/*
 * Copyright (c) 2009-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.certificate.X500PrincipalUtils;
import org.apache.commons.collections.CollectionUtils;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509v2CRLBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CRLConverter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of X509CRLBuilder for builder X509 V2 CRLs.
 *
 * Note: this class is *not* thread safe
 *
 * @author Martijn Brinkers
 *
 */
public class X509CRLBuilderImpl implements X509CRLBuilder
{
    /*
     * The provider used for signing the CRL
     */
    private final String sigingProvider;

    /*
     * The provider used for creating the CRL
     */
    private final String crlProvider;

    /*
     * The date of the CRL
     */
    private Date thisUpdate;

    /*
     * The next update of the CRL
     */
    private Date nextUpdate;

    /*
     * The algorithm used for signing the CRL
     */
    private String signatureAlgorithm;

    /*
     * The entries of the CRLs will be added to the CRL to be created
     */
    private List<X509CRL> crls;

    /*
     * container class for userCertificate, revocationDate and reason
     */
    private static class Entry
    {
        private final BigInteger serialNumber;
        private final Date revocationDate;
        private final int reason;

        Entry(BigInteger serialNumber, Date revocationDate, int reason)
        {
            this.serialNumber = serialNumber;
            this.revocationDate = revocationDate;
            this.reason = reason;
        }
    }

    /*
     * The entries to add
     */
    private List<Entry> entries;

    public X509CRLBuilderImpl(String sigingProvider, String crlProvider)
    {
        this.sigingProvider = sigingProvider;
        this.crlProvider = crlProvider;
    }

    @Override
    public void setThisUpdate(Date date) {
        this.thisUpdate = date;
    }

    @Override
    public void setNextUpdate(Date date) {
        this.nextUpdate = date;
    }

    @Override
    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    @Override
    public void addCRLEntry(@Nonnull BigInteger serialNumber, Date revocationDate, int reason)
    {
        if (entries == null) {
            entries = new LinkedList<>();
        }

        entries.add(new Entry(serialNumber, revocationDate, reason));
    }

    @Override
    public void addCRL(@Nonnull X509CRL other)
    throws CRLException
    {
        if (crls == null) {
            crls = new LinkedList<>();
        }

        crls.add(other);
    }

    private ContentSigner getContentSigner(@Nonnull PrivateKey privateKey)
    throws OperatorCreationException
    {
        JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder(signatureAlgorithm);

        contentSignerBuilder.setProvider(sigingProvider);

        return contentSignerBuilder.build(privateKey);
    }

    private X509CRL getX509CRL(@Nonnull X509CRLHolder holder)
    throws CRLException
    {
        JcaX509CRLConverter converter = new JcaX509CRLConverter();

        converter.setProvider(crlProvider);

        return converter.getCRL(holder);
    }

    @Override
    public @Nonnull X509CRL generateCRL(@Nonnull KeyAndCertificate issuer)
    throws CRLException
    {
        if (thisUpdate == null) {
            throw new IllegalStateException("thisUpdate it not set");
        }

        try {
            X509v2CRLBuilder builder = new X509v2CRLBuilder(X500PrincipalUtils.toX500Name(
                    issuer.getCertificate().getSubjectX500Principal()), thisUpdate);

            if (CollectionUtils.isNotEmpty(crls))
            {
                for (X509CRL crl : crls) {
                    builder.addCRL(new X509CRLHolder(crl.getEncoded()));
                }
            }

            if (CollectionUtils.isNotEmpty(entries))
            {
                for (Entry entry : entries) {
                    builder.addCRLEntry(entry.serialNumber, entry.revocationDate, entry.reason);
                }
            }

            if (nextUpdate != null) {
                builder.setNextUpdate(nextUpdate);
            }

            return getX509CRL(builder.build(getContentSigner(issuer.getPrivateKey())));
        }
        catch (IllegalStateException | IOException | OperatorCreationException e) {
            throw new CRLException(e);
        }
    }
}
