/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore.dao;

import com.google.common.annotations.VisibleForTesting;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.hibernate.X509CertificateUserType;
import com.ciphermail.core.common.locale.DefaultLocale;
import com.ciphermail.core.common.security.certificate.X500PrincipalInspector;
import com.ciphermail.core.common.security.certificate.X509CertSelectorInspector;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certificate.X509ExtensionInspector;
import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreEntity;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class X509CertStoreDAO extends GenericHibernateDAO
{
    /*
     * Name of the Certificate Store. This can be used to have multiple distinct certificate stores in the database
     * like for example a certificate store (end-user and intermediate) and a root store
     */
    private final String storeName;

    /*
     * Hibernate entity name
     */
    private static final String ENTITY_NAME = X509CertStoreEntity.ENTITY_NAME;

    private X509CertStoreDAO(@Nonnull SessionAdapter session, @Nonnull String storeName)
    {
        super(session);

        this.storeName = Objects.requireNonNull(storeName);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static X509CertStoreDAO getInstance(@Nonnull SessionAdapter session, @Nonnull String storeName) {
        return new X509CertStoreDAO(session, storeName);
    }

    private <T> Query<T> createByEmailQuery(@Nonnull String baseQuery, String email, Match match,
            Expired expired, MissingKeyAlias missingKeyAlias, Date date, @Nonnull Class<T> clazz)
    {
        if (email != null) {
            baseQuery = baseQuery + " join c.email e where e";
            baseQuery = baseQuery + (match == Match.EXACT ? " = :email" : " like :email");
        }
        else {
            // if email is null we want to get all the entries without an email address
            baseQuery = baseQuery + " left join c.email e where e is null";
        }

        if (expired == Expired.MATCH_UNEXPIRED_ONLY) {
            baseQuery = baseQuery + " and :time between c.certificate.notBefore and c.certificate.notAfter";
        }
        else if (expired == Expired.MATCH_EXPIRED_ONLY) {
            baseQuery = baseQuery + " and (:time < c.certificate.notBefore or :time > c.certificate.notAfter)";
        }

        if (missingKeyAlias == MissingKeyAlias.NOT_ALLOWED) {
            baseQuery = baseQuery + " and c.keyAlias is not null";
        }

        baseQuery = baseQuery + " and c.storeName = :storeName";

        Query<T> query = createQuery(baseQuery, clazz);

        if (email != null)
        {
            email = email.toLowerCase().trim();
            query.setParameter("email", email);
        }

        if (expired == Expired.MATCH_UNEXPIRED_ONLY || expired == Expired.MATCH_EXPIRED_ONLY) {
            query.setParameter("time", date);
        }

        query.setParameter(X509CertStoreEntity.STORE_NAME_COLUMN, storeName);

        return query;
    }

    public CloseableIterator<X509CertStoreEntity> getByEmail(String email, Match match, Expired expired,
            MissingKeyAlias missingKeyAlias, Date date, Integer firstResult, Integer maxResults)
    {
        // we need to get distinct results using the distinct keyword. This is not supported by Criteria
        // AFAIK so we will need to use HQL
        String baseQuery = "select distinct c from " + ENTITY_NAME + " c";

        Query<X509CertStoreEntity> query = createByEmailQuery(baseQuery, email, match, expired,
                missingKeyAlias, date, X509CertStoreEntity.class);

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxReslts to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return new X509CertStoreEntryIterator(query.scroll(ScrollMode.FORWARD_ONLY));
    }

    /**
     * returns the number of records that getByEmail would return
     */
    public long getByEmailCount(String email, Match match, Expired expired, MissingKeyAlias missingKeyAlias, Date date)
    {
        // we need to get distinct results using the distinct keyword. This is not supported by Criteria
        // AFAIK so we will need to use HQL
        String baseQuery = "select count (distinct c) from " + ENTITY_NAME + " c";

        return createByEmailQuery(baseQuery, email, match, expired, missingKeyAlias,
                date, Long.class).uniqueResultOptional().orElse(0L);
    }

    /**
     * Searches the subject friendly name using ILIKE.
     */
    public CloseableIterator<X509CertStoreEntity> searchBySubject(@Nonnull String subject,
            Expired expired, MissingKeyAlias missingKeyAlias, Date date, Integer firstResult, Integer maxResults)
    {
        return searchCertificateColumn(X509CertStoreEntity.SUBJECT_FRIENDLY_COLUMN, subject, expired,
                missingKeyAlias, date, firstResult, maxResults);
    }

    public long getSearchBySubjectCount(@Nonnull String subject, Expired expired, MissingKeyAlias missingKeyAlias,
            Date date)
    {
        return getSearchCertificateColumnCount(X509CertStoreEntity.SUBJECT_FRIENDLY_COLUMN, subject, expired,
                missingKeyAlias, date);
    }

    /**
     * Searches the issuer friendly name using ILIKE.
     */
    public CloseableIterator<X509CertStoreEntity> searchByIssuer(String issuer, Expired expired,
            MissingKeyAlias missingKeyAlias, Date date, Integer firstResult, Integer maxResults)
    {
        return searchCertificateColumn(X509CertStoreEntity.ISSUER_FRIENDLY_COLUMN, issuer, expired,
                missingKeyAlias, date, firstResult, maxResults);
    }

    public long getSearchByIssuerCount(String issuer, Expired expired, MissingKeyAlias missingKeyAlias, Date date)
    {
        return getSearchCertificateColumnCount(X509CertStoreEntity.ISSUER_FRIENDLY_COLUMN, issuer, expired,
                missingKeyAlias, date);
    }

    /*
     * Helper function that searches the field using like.
     */
    private CloseableIterator<X509CertStoreEntity> searchCertificateColumn(@Nonnull String column,
            @Nonnull String search, Expired expired, MissingKeyAlias missingKeyAlias, Date date,
            Integer firstResult, Integer maxResults)
    {
        search = StringUtils.lowerCase(search);

        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<X509CertStoreEntity> criteriaQuery = criteriaBuilder.createQuery(
                X509CertStoreEntity.class);

        Root<X509CertStoreEntity> rootEntity = criteriaQuery.from(X509CertStoreEntity.class);

        Path<X509CertificateUserType> certificatePath = rootEntity.get(
                X509CertStoreEntity.CERTIFICATE_USER_TYPE_NAME);

        List<Predicate> restrictions = new LinkedList<>();

        restrictions.add(criteriaBuilder.equal(rootEntity.get(X509CertStoreEntity.STORE_NAME_COLUMN),
                storeName));

        restrictions.add(criteriaBuilder.like(criteriaBuilder.lower(certificatePath.get(column)), search));

        if (expired == Expired.MATCH_UNEXPIRED_ONLY)
        {
            restrictions.add(criteriaBuilder.lessThan(certificatePath.get(
                    X509CertStoreEntity.NOT_BEFORE_COLUMN), date));

            restrictions.add(criteriaBuilder.greaterThan(certificatePath.get(
                    X509CertStoreEntity.NOT_AFTER_COLUMN), date));
        }
        else if (expired == Expired.MATCH_EXPIRED_ONLY)
        {
            restrictions.add(criteriaBuilder.or(
                    criteriaBuilder.greaterThan(certificatePath.get(X509CertStoreEntity.NOT_BEFORE_COLUMN),
                            date),
                    criteriaBuilder.lessThan(certificatePath.get(X509CertStoreEntity.NOT_AFTER_COLUMN),
                            date)));
        }

        if (missingKeyAlias == MissingKeyAlias.NOT_ALLOWED) {
            restrictions.add(criteriaBuilder.isNotNull(rootEntity.get(X509CertStoreEntity.KEY_ALIAS_COLUMN)));
        }

        criteriaQuery.where(CollectionUtils.toArray(restrictions, Predicate.class));

        criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(X509CertStoreEntity.CREATION_DATE_COLUMN)));

        Query<X509CertStoreEntity> query = createQuery(criteriaQuery);

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxReslts to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return new X509CertStoreEntryIterator(query.scroll(ScrollMode.FORWARD_ONLY));
    }

    /*
     * Helper function that returns the number of records that searchField would return.
     */
    private long getSearchCertificateColumnCount(@Nonnull String column, @Nonnull String search,
            Expired expired, MissingKeyAlias missingKeyAlias, Date date)
    {
        search = StringUtils.lowerCase(search);

        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<X509CertStoreEntity> rootEntity = criteriaQuery.from(X509CertStoreEntity.class);

        Path<X509CertificateUserType> certificatePath = rootEntity.get(
                X509CertStoreEntity.CERTIFICATE_USER_TYPE_NAME);

        List<Predicate> restrictions = new LinkedList<>();

        restrictions.add(criteriaBuilder.equal(rootEntity.get(X509CertStoreEntity.STORE_NAME_COLUMN),
                storeName));

        restrictions.add(criteriaBuilder.like(criteriaBuilder.lower(certificatePath.get(column)), search));

        if (expired == Expired.MATCH_UNEXPIRED_ONLY)
        {
            restrictions.add(criteriaBuilder.lessThan(certificatePath.get(
                    X509CertStoreEntity.NOT_BEFORE_COLUMN), date));

            restrictions.add(criteriaBuilder.greaterThan(certificatePath.get(
                    X509CertStoreEntity.NOT_AFTER_COLUMN), date));
        }
        else if (expired == Expired.MATCH_EXPIRED_ONLY)
        {
            restrictions.add(criteriaBuilder.or(
                    criteriaBuilder.greaterThan(certificatePath.get(X509CertStoreEntity.NOT_BEFORE_COLUMN),
                            date),
                    criteriaBuilder.lessThan(certificatePath.get(X509CertStoreEntity.NOT_AFTER_COLUMN),
                            date)));
        }

        if (missingKeyAlias == MissingKeyAlias.NOT_ALLOWED) {
            restrictions.add(criteriaBuilder.isNotNull(rootEntity.get(X509CertStoreEntity.KEY_ALIAS_COLUMN)));
        }

        criteriaQuery.where(CollectionUtils.toArray(restrictions, Predicate.class));

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    public X509CertStoreEntity getByThumbprint(@Nonnull String thumbprint)
    {
        thumbprint = thumbprint.toUpperCase(DefaultLocale.getDefaultLocale());

        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<X509CertStoreEntity> criteriaQuery = criteriaBuilder.createQuery(
                X509CertStoreEntity.class);

        Root<X509CertStoreEntity> rootEntity = criteriaQuery.from(X509CertStoreEntity.class);

        Path<X509CertificateUserType> certificatePath = rootEntity.get(
                X509CertStoreEntity.CERTIFICATE_USER_TYPE_NAME);

        criteriaQuery.where(criteriaBuilder.and(
                criteriaBuilder.equal(certificatePath.get(X509CertStoreEntity.THUMBPRINT_COLUMN),
                        thumbprint),
                criteriaBuilder.equal(rootEntity.get(X509CertStoreEntity.STORE_NAME_COLUMN), storeName)));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }

    public X509CertStoreEntity getByCertificate(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        String thumbprint;

        try {
            thumbprint = X509CertificateInspector.getThumbprint(certificate);
        }
        catch (CertificateEncodingException | NoSuchAlgorithmException e) {
            throw new CertStoreException(e);
        }
        catch (NoSuchProviderException e) {
            throw new UnhandledException(e);
        }

        return getByThumbprint(thumbprint);
    }

    public long getRowCount()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<X509CertStoreEntity> rootEntity = criteriaQuery.from(X509CertStoreEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(X509CertStoreEntity.STORE_NAME_COLUMN),
                storeName));

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    public long getRowCount(Expired expired, MissingKeyAlias missingKeyAlias, Date date)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<X509CertStoreEntity> rootEntity = criteriaQuery.from(X509CertStoreEntity.class);

        List<Predicate> restrictions = new LinkedList<>();

        restrictions.add(criteriaBuilder.equal(rootEntity.get(X509CertStoreEntity.STORE_NAME_COLUMN),
                storeName));

        Path<X509CertificateUserType> certificatePath = rootEntity.get(
                X509CertStoreEntity.CERTIFICATE_USER_TYPE_NAME);

        if (expired == Expired.MATCH_UNEXPIRED_ONLY)
        {
            restrictions.add(criteriaBuilder.lessThan(certificatePath.get(
                    X509CertStoreEntity.NOT_BEFORE_COLUMN), date));

            restrictions.add(criteriaBuilder.greaterThan(certificatePath.get(
                    X509CertStoreEntity.NOT_AFTER_COLUMN), date));
        }
        else if (expired == Expired.MATCH_EXPIRED_ONLY)
        {
            restrictions.add(criteriaBuilder.or(
                    criteriaBuilder.greaterThan(certificatePath.get(X509CertStoreEntity.NOT_BEFORE_COLUMN),
                            date),
                    criteriaBuilder.lessThan(certificatePath.get(X509CertStoreEntity.NOT_AFTER_COLUMN),
                            date)));
        }

        if (missingKeyAlias == MissingKeyAlias.NOT_ALLOWED) {
            restrictions.add(criteriaBuilder.isNotNull(rootEntity.get(X509CertStoreEntity.KEY_ALIAS_COLUMN)));
        }

        criteriaQuery.where(CollectionUtils.toArray(restrictions, Predicate.class));

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    public CloseableIterator<X509CertStoreEntity> getCertStoreIterator(Expired expired,
            MissingKeyAlias missingKeyAlias, Date date, Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<X509CertStoreEntity> criteriaQuery = criteriaBuilder.createQuery(
                X509CertStoreEntity.class);

        Root<X509CertStoreEntity> rootEntity = criteriaQuery.from(X509CertStoreEntity.class);

        List<Predicate> restrictions = new LinkedList<>();

        restrictions.add(criteriaBuilder.equal(rootEntity.get(X509CertStoreEntity.STORE_NAME_COLUMN),
                storeName));

        Path<X509CertificateUserType> certificatePath = rootEntity.get(
                X509CertStoreEntity.CERTIFICATE_USER_TYPE_NAME);

        if (expired == Expired.MATCH_UNEXPIRED_ONLY)
        {
            restrictions.add(criteriaBuilder.lessThan(certificatePath.get(
                    X509CertStoreEntity.NOT_BEFORE_COLUMN), date));

            restrictions.add(criteriaBuilder.greaterThan(certificatePath.get(
                    X509CertStoreEntity.NOT_AFTER_COLUMN), date));
        }
        else if (expired == Expired.MATCH_EXPIRED_ONLY)
        {
            restrictions.add(criteriaBuilder.or(
                    criteriaBuilder.greaterThan(certificatePath.get(X509CertStoreEntity.NOT_BEFORE_COLUMN),
                            date),
                    criteriaBuilder.lessThan(certificatePath.get(X509CertStoreEntity.NOT_AFTER_COLUMN),
                            date)));
        }

        if (missingKeyAlias == MissingKeyAlias.NOT_ALLOWED) {
            restrictions.add(criteriaBuilder.isNotNull(rootEntity.get(X509CertStoreEntity.KEY_ALIAS_COLUMN)));
        }

        criteriaQuery.where(criteriaBuilder.and(CollectionUtils.toArray(restrictions, Predicate.class)));

        criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(X509CertStoreEntity.CREATION_DATE_COLUMN)));

        Query<X509CertStoreEntity> query = createQuery(criteriaQuery);

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxReslts to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return new X509CertStoreEntryIterator(query.scroll(ScrollMode.FORWARD_ONLY));
    }

    public @Nonnull X509CertStoreEntity addCertificate(@Nonnull X509Certificate certificate, String keyAlias)
    throws CertStoreException
    {
        Date creationDate = new Date();

        X509CertStoreEntity entry;

        try {
            entry = new X509CertStoreEntity(certificate, keyAlias, storeName,
                    creationDate);
        }
        catch (IOException e) {
            throw new CertStoreException(e);
        }

        persist(entry);

        return entry;
    }

    public void removeCertificate(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        X509CertStoreEntity entry = getByCertificate(certificate);

        if (entry != null) {
            this.delete(entry);
        }
    }

    public void removeAllEntries()
    throws CertStoreException
    {
        // I tried using HQL and delete but that did not work because of a constraint violation.
        // Using delete seems not to cascade to the certificate_email table.
        CloseableIterator<X509CertStoreEntity> iterator = getCertStoreIterator(null,
                MissingKeyAlias.ALLOWED, null, null);

        try {
            try {
                while (iterator.hasNext())
                {
                    X509CertStoreEntity entry = iterator.next();

                    // Need to flush before evict to prevent the following exception when deleting while adding
                    // certificates at the same time. See https://forum.hibernate.org/viewtopic.php?p=2424890
                    //
                    // java.util.concurrent.ExecutionException: org.hibernate.AssertionFailure: possible
                    // nonthreadsafe access to session
                    this.flush();
                    // evict the entry to save memory
                    this.evict(entry);
                    this.delete(entry);
                }
            }
            finally {
                // need to close the iterator
                iterator.close();
            }
        }
        catch (CloseableIteratorException e) {
            throw new CertStoreException(e);
        }
    }

    public Collection<X509Certificate> getCertificates(CertSelector certSelector, MissingKeyAlias missingKeyAlias,
            Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        Collection<X509Certificate> foundCertificates = new LinkedList<>();

        CloseableIterator<X509Certificate> iterator = getCertificateIterator(certSelector, missingKeyAlias, firstResult,
                maxResults);

        try {
            try {
                while (iterator.hasNext())
                {
                    X509Certificate certificate = iterator.next();

                    if (certificate != null && (certSelector == null || certSelector.match(certificate))) {
                        foundCertificates.add(certificate);
                    }
                }
            }
            finally {
                // need to close the iterator
                iterator.close();
            }
        }
        catch(CloseableIteratorException e) {
            throw new CertStoreException(e);
        }

        return foundCertificates;
    }

    public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    {
        return new X509CertStoreCertificateIterator(
                getPreFilteredEntriesScrollable(certSelector, missingKeyAlias, firstResult, maxResults),
                certSelector);
    }

    public CloseableIterator<X509CertStoreEntity> getCertStoreIterator(CertSelector certSelector,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    {
        return new X509CertStoreEntryIterator(
                getPreFilteredEntriesScrollable(certSelector, missingKeyAlias, firstResult, maxResults),
                certSelector);
    }

    /*
     * Creates a collection of DatabaseCriterions used for pre-filtering based on the
     * CertSelector. This speeds up the search process because the database will
     * filter all relevant entries using database queries.
     */
    private List<Predicate> createRestrictionsForCertSelector(@Nonnull CriteriaBuilder criteriaBuilder,
            @Nonnull Root<X509CertStoreEntity> rootEntity, @Nonnull X509CertSelector certSelector)
    {
        List<Predicate> restrictions = new LinkedList<>();

        X500Principal issuer    = certSelector.getIssuer();
        X500Principal subject   = certSelector.getSubject();
        BigInteger serialNumber = certSelector.getSerialNumber();
        Date certificateValid   = certSelector.getCertificateValid();
        // CertSelector stored the subjectKeyIdentifier DER encoded
        byte[] subjectKeyIdentifier = X509CertSelectorInspector.getSubjectKeyIdentifier(certSelector);

        // if a certificate is specified we need to get the properties from the
        // certificate
        if (certSelector.getCertificate() != null)
        {
            X509Certificate certificate = certSelector.getCertificate();

            if (issuer == null) {
                issuer = certificate.getIssuerX500Principal();
            }

            if (subject == null) {
                subject = certificate.getSubjectX500Principal();
            }

            if (serialNumber == null) {
                serialNumber = certificate.getSerialNumber();
            }

            if (subjectKeyIdentifier == null) {
                subjectKeyIdentifier = X509ExtensionInspector.getSubjectKeyIdentifier(certificate);
            }
        }

        Path<X509CertificateUserType> certificatePath = rootEntity.get(
                X509CertStoreEntity.CERTIFICATE_USER_TYPE_NAME);

        if (issuer != null)
        {
            restrictions.add(criteriaBuilder.equal(
                    certificatePath.get(X509CertStoreEntity.ISSUER_COLUMN),
                    X500PrincipalInspector.getCanonical(issuer)));
        }

        if (subject != null)
        {
            restrictions.add(criteriaBuilder.equal(
                    certificatePath.get(X509CertStoreEntity.SUBJECT_COLUMN),
                    X500PrincipalInspector.getCanonical(subject)));
        }

        if (serialNumber != null)
        {
            restrictions.add(criteriaBuilder.equal(
                    certificatePath.get(X509CertStoreEntity.SERIAL_COLUMN),
                    BigIntegerUtils.hexEncode(serialNumber)));
        }

        if (subjectKeyIdentifier != null)
        {
            restrictions.add(criteriaBuilder.equal(
                    certificatePath.get(X509CertStoreEntity.SUBJECT_KEY_IDENTIFIER_COLUMN),
                    HexUtils.hexEncode(subjectKeyIdentifier)));
        }

        if (certificateValid != null)
        {
            restrictions.add(criteriaBuilder.lessThan(certificatePath.get(
                    X509CertStoreEntity.NOT_BEFORE_COLUMN), certificateValid));

            restrictions.add(criteriaBuilder.greaterThan(certificatePath.get(
                    X509CertStoreEntity.NOT_AFTER_COLUMN), certificateValid));
        }

        return restrictions;
    }

    /*
     * Return a pre-filtered collection of X509CertStoreEntry object. The filtering is done using
     * database queries.
     */
    @VisibleForTesting
    ScrollableResults<X509CertStoreEntity> getPreFilteredEntriesScrollable(CertSelector certSelector,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<X509CertStoreEntity> criteriaQuery = criteriaBuilder.createQuery(
                X509CertStoreEntity.class);

        Root<X509CertStoreEntity> rootEntity = criteriaQuery.from(X509CertStoreEntity.class);

        List<Predicate> restrictions = new LinkedList<>();

        restrictions.add(criteriaBuilder.equal(rootEntity.get(X509CertStoreEntity.STORE_NAME_COLUMN),
                storeName));

        if (missingKeyAlias == MissingKeyAlias.NOT_ALLOWED) {
            restrictions.add(criteriaBuilder.isNotNull(rootEntity.get(X509CertStoreEntity.KEY_ALIAS_COLUMN)));
        }

        // If the selector is a X509CertSelector we can do some pre-filtering to speed the search process
        if (certSelector instanceof X509CertSelector x509CertSelector) {
            restrictions.addAll(createRestrictionsForCertSelector(criteriaBuilder, rootEntity, x509CertSelector));
        }

        criteriaQuery.where(criteriaBuilder.and(CollectionUtils.toArray(restrictions, Predicate.class)));

        criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(X509CertStoreEntity.CREATION_DATE_COLUMN)));

        Query<X509CertStoreEntity> query = createQuery(criteriaQuery);

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxResults to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.scroll(ScrollMode.FORWARD_ONLY);
    }
}
