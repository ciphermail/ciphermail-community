/*
 * Copyright (c) 2009-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

import javax.annotation.Nonnull;

public interface CAProperties
{
    /**
     * The thumbprint of the certificate that will sign the issued certificates.
     */
    void setIssuerThumbprint(String thumbprint)
    throws HierarchicalPropertiesException;

    String getIssuerThumbprint()
    throws HierarchicalPropertiesException;

    /**
     * The default common name used when issuing certificates
     */
    void setDefaultCommonName(String commonName)
    throws HierarchicalPropertiesException;

    String getDefaultCommonName()
    throws HierarchicalPropertiesException;

    /**
     * The default number of days the certificate should be valid. Must be > 0.
     */
    void setCertificateValidityDays(Integer days)
    throws HierarchicalPropertiesException;

    @Nonnull Integer getCertificateValidityDays()
    throws HierarchicalPropertiesException;

    /**
     * The default length of the RSA key in bites (must >= 1024)
     */
    void setKeyLength(Integer length)
    throws HierarchicalPropertiesException;

    @Nonnull Integer getKeyLength()
    throws HierarchicalPropertiesException;

    /**
     * The default signature algorithm
     */
    void setSignatureAlgorithm(CACertificateSignatureAlgorithm signatureAlgorithm)
    throws HierarchicalPropertiesException;

    @Nonnull CACertificateSignatureAlgorithm getSignatureAlgorithm()
    throws HierarchicalPropertiesException;

    /**
     * The CRL distribution point added to the end user certificate
     */
    void setCRLDistributionPoint(String url)
    throws HierarchicalPropertiesException;

    String getCRLDistributionPoint()
    throws HierarchicalPropertiesException;

    /**
     * The default certificate request handler to use
     */
    void setDefaultCertificateRequestHandler(String certificateRequestHandler)
    throws HierarchicalPropertiesException;

    String getDefaultCertificateRequestHandler()
    throws HierarchicalPropertiesException;
}
