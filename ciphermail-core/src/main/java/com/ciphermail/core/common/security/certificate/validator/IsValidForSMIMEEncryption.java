/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate.validator;

import com.ciphermail.core.common.security.certificate.ExtendedKeyUsageType;
import com.ciphermail.core.common.security.certificate.KeyUsageType;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;

import javax.annotation.Nonnull;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Set;

/**
 * This CertificateValidator checks if a certificate can be used for S/MIME encryption.
 *
 * If a certificate has a key usage the key usage should contain keyEncipherment.
 * If a certificate has an extended key usage the extended key usage should contain anyExtendedKeyUsage
 * and/or id-kp-emailProtection.
 *
 * This class is not thread safe.
 *
 * @author Martijn Brinkers
 *
 */
public class IsValidForSMIMEEncryption implements CertificateValidator
{
    private final String name;

    private String failureMessage = "";

    public IsValidForSMIMEEncryption(String name) {
        this.name = name;
    }

    public IsValidForSMIMEEncryption() {
        this(null);
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * Returns true if the certificate allows KEYENCIPHERMENT (keyUsage extension) and allows
     * EMAILPROTECTION (extended keyUsage extension).
     */
    @Override
    public boolean isValid(@Nonnull Certificate certificate)
    throws CertificateException
    {
        if (!(certificate instanceof X509Certificate x509Certificate))
        {
            failureMessage = "Certificate is not a X509Certificate";

            return false;
        }

        failureMessage = "";

        Set<KeyUsageType> keyUsage = X509CertificateInspector.getKeyUsage(x509Certificate);

        boolean valid = (keyUsage == null) || (keyUsage.contains(KeyUsageType.KEYENCIPHERMENT));

        if (!valid) {
            failureMessage = "Key usage does not allow " + KeyUsageType.KEYENCIPHERMENT;
        }
        else {
            Set<ExtendedKeyUsageType> extendedKeyUsage = X509CertificateInspector.getExtendedKeyUsage(x509Certificate);

            valid = (extendedKeyUsage == null || extendedKeyUsage.contains(ExtendedKeyUsageType.ANYKEYUSAGE) ||
                    extendedKeyUsage.contains(ExtendedKeyUsageType.EMAILPROTECTION));

            if (!valid) {
                failureMessage = "Extended key usage does not allow " + ExtendedKeyUsageType.EMAILPROTECTION;
            }
        }

        return valid;
    }

    @Override
    public String getFailureMessage() {
        return failureMessage;
    }
}
