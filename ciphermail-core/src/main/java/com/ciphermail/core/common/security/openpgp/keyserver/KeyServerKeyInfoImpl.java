/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.keyserver;

import com.ciphermail.core.common.security.openpgp.PGPPublicKeyAlgorithm;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of KeyServerKeyInfo
 *
 * @author Martijn Brinkers
 *
 */
public class KeyServerKeyInfoImpl implements KeyServerKeyInfo
{
    /*
     * The URL of the server that returned the result
     */
    private final String serverURL;

    /*
     * The key ID of the key
     */
    private final String keyID;

    /*
     * The public key algorithm
     */
    private final PGPPublicKeyAlgorithm algorithm;

    /*
     * The length of the public key
     */
    private final int keyLength;

    /*
     * The date when the key was created
     */
    private final Date creationDate;

    /*
     * The date the key will expire
     */
    private final Date expirationDate;

    /*
     * Details of the key
     */
    private final String flags;

    /*
     * The User IDs associated with the key
     */
    private final List<String> userIDs = new LinkedList<>();

    public KeyServerKeyInfoImpl(@Nonnull String serverURL, @Nonnull String keyID, PGPPublicKeyAlgorithm algorithm,
            int keyLength, Date creationDate, Date expirationDate, String flags)
    {
        this.serverURL = Objects.requireNonNull(serverURL);
        this.keyID = Objects.requireNonNull(keyID);
        this.algorithm = algorithm;
        this.keyLength = keyLength;
        this.creationDate = creationDate;
        this.expirationDate = expirationDate;
        this.flags = flags;
    }

    @Override
    public @Nonnull String getServerURL() {
        return serverURL;
    }

    @Override
    public @Nonnull String getKeyID() {
        return keyID;
    }

    @Override
    public PGPPublicKeyAlgorithm getAlgorithm() {
        return algorithm;
    }

    @Override
    public int getKeyLength() {
        return keyLength;
    }

    @Override
    public Date getCreationDate() {
        return creationDate;
    }

    @Override
    public Date getExpirationDate() {
        return expirationDate;
    }

    @Override
    public String getFlags() {
        return flags;
    }

    @Override
    public @Nonnull List<String> getUserIDs() {
        return userIDs;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof KeyServerKeyInfo rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(keyID, rhs.getKeyID())
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().
            append(keyID).
            toHashCode();
    }
}
