/*
 * Copyright (c) 2008-2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore.jce;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.SecurityFactory;

import java.security.KeyStore;
import java.security.KeyStore.ProtectionParameter;

/**
 * KeyStore parameters used by KeyStoreHibernate
 *
 * @author Martijn Brinkers
 *
 */
public class DatabaseKeyStoreLoadStoreParameter implements KeyStore.LoadStoreParameter
{
    /*
     * The name of the key store
     */
    private final String storeName;

    /*
     * Manager for database sessions
     */
    private final SessionManager sessionManager;

    /*
     * The security factory to use to create the key from the raw key material. If null, the default security factory
     * will be used.
     */
    private SecurityFactory securityFactory;

    public DatabaseKeyStoreLoadStoreParameter(String storeName, SessionManager sessionManager)
    {
        this.storeName = storeName;
        this.sessionManager = sessionManager;
    }

    @Override
    public ProtectionParameter getProtectionParameter() {
        // not used
        return null;
    }

    public String getStoreName() {
        return storeName;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public SecurityFactory getSecurityFactory() {
        return securityFactory;
    }

    public void setSecurityFactory(SecurityFactory securityFactory) {
        this.securityFactory = securityFactory;
    }
}
