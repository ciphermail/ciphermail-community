/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certpath;

import com.ciphermail.core.common.security.certstore.BasicCertStore;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * This TrustAnchorBuilder will build it's TrustAnchor set from a provider X509CertStoreExt certStore.
 * The returned trustAnchor set should not be stored because certificates added to the store after
 * the call to getTrustAnchors will not be available to that set.
 *
 * @author Martijn Brinkers
 *
 */
public class CertStoreTrustAnchorBuilder extends SimpleTrustAnchorBuilder
{
    private static final Logger logger = LoggerFactory.getLogger(CertStoreTrustAnchorBuilder.class);

    /*
     * Keep a private list of TrustAnchors. The list in SimpleTrustAnchorBuilder will be used
     * to store trustAnchors added by the user.
     */
    private Set<TrustAnchor> trustAnchors = Collections.synchronizedSet(Collections.unmodifiableSet(
            new HashSet<>()));

    /*
     * The store containing the trusted Certificates.
     */
    private final BasicCertStore rootStore;

    /*
     * Time in milliseconds between checks for cache dirty.
     */
    private final long updateCheckInterval;

    /*
     * If true the cached trustAnchors set will be updated.
     */
    private boolean forceUpdate;

    /*
     * Time in milliseconds the last check for dirty cache was done.
     */
    private long timeLastUpdateCheck;

    public CertStoreTrustAnchorBuilder(@Nonnull BasicCertStore rootStore, long updateCheckInterval)
    {
        this.rootStore = Objects.requireNonNull(rootStore);
        this.updateCheckInterval = updateCheckInterval;
    }

    private boolean updateNeeded()
    {
        if (forceUpdate)
        {
            forceUpdate = false;

            return true;
        }

        boolean updateNeeded = false;

        long now = System.currentTimeMillis();

        // calculate the time in milliseconds between the last check and the current time.
        long intervalLastCheck = now - timeLastUpdateCheck;

        // checking the record count and last entry takes a lot of time so we only want to
        // check this after some time has elapsed. Check if intervalLastCheck < 0, if so
        // the global time of the system must have been set to a earlier date.
        if (timeLastUpdateCheck == 0 || intervalLastCheck >= updateCheckInterval || intervalLastCheck < 0)
        {
            timeLastUpdateCheck = now;

            updateNeeded = true;
        }

        return updateNeeded;
    }

    @Override
    public synchronized void addCertificates(@Nonnull Collection<? extends Certificate> certificates)
    throws CertStoreException
    {
        super.addCertificates(certificates);

        forceUpdate = true;
    }

    /**
     * The returned trustAnchor set should not be stored because certificates added to the store after
     * the call to getTrustAnchors will not be available to that set.
     */
    @Override
    public synchronized Set<TrustAnchor> getTrustAnchors()
    throws CertStoreException
    {
        if (updateNeeded())
        {
            logger.info("Rebuilding trust anchor cache.");

            // refresh list. Initialize with trust anchors from super
            Set<TrustAnchor> newTrustAnchors = new HashSet<>(super.getTrustAnchors());

            CloseableIterator<? extends Certificate> iterator = rootStore.getCertificateIterator(null);

            try {
                try {
                    while(iterator.hasNext())
                    {
                        Certificate certificate = iterator.next();

                        // only X509Certificate's are supported
                        if (certificate instanceof X509Certificate x509Certificate)
                        {
                            TrustAnchor trustAnchor = new TrustAnchor(x509Certificate, null);

                            newTrustAnchors.add(trustAnchor);
                        }
                    }

                }
                finally {
                    iterator.close();
                }
            }
            catch(CloseableIteratorException e) {
                throw new CertStoreException(e);
            }

            // trustAnchors will be set the new instance so we won't interfere with existing references of
            // the previous trustAnchors.
            trustAnchors = Collections.synchronizedSet(Collections.unmodifiableSet(newTrustAnchors));
        }

        return trustAnchors;
    }

    @Override
    public synchronized void refresh() {
        forceUpdate = true;
    }
}
