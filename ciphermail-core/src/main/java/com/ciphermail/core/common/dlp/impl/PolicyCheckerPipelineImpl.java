/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import com.ciphermail.core.common.dlp.PolicyChecker;
import com.ciphermail.core.common.dlp.PolicyCheckerContext;
import com.ciphermail.core.common.dlp.PolicyCheckerPipeline;
import com.ciphermail.core.common.dlp.PolicyViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Default implementation of PolicyCheckerPipeline.
 *
 * Note: fastFail should only be changed before this PolicyChecker is used in a multi-threaded environment
 * because access to fastFail is not thread safe.
 *
 * @author Martijn Brinkers
 *
 */
public class PolicyCheckerPipelineImpl implements PolicyCheckerPipeline
{
    private static final Logger logger = LoggerFactory.getLogger(PolicyCheckerPipelineImpl.class);

    /*
     * All the registered PolicyChecker's.
     */
    private final List<PolicyChecker> policies = Collections.synchronizedList(new LinkedList<PolicyChecker>());

    /*
     * If fastfail is set, checkPolicy will immediately stop checking other policies if a policy throws a
     * PolicyViolationException. If fastfail is false, checkPolicy will continue checking the other policies. All policy
     * violations will be stored in the PolicyViolationException.
     */
    private boolean fastFail = false;

    public PolicyCheckerPipelineImpl() {
        // Empty on purpose
    }

    public PolicyCheckerPipelineImpl(Collection<PolicyChecker> policyCheckers)
    {
        if (policyCheckers != null)
        {
            for (PolicyChecker policyChecker : policyCheckers) {
                addPolicyChecker(policyChecker);
            }
        }
    }

    @Override
    public void addPolicyChecker(@Nonnull PolicyChecker policyChecker) {
        policies.add(Objects.requireNonNull(policyChecker));
    }

    @Override
    public String getName() {
        return "PolicyCheckerPipelineImpl";
    }

    @Override
    public void init(@Nonnull PolicyCheckerContext context)
    {
        for (PolicyChecker policyChecker : policies) {
            policyChecker.init(context);
        }
    }

    @Override
    public void update(@Nonnull PolicyCheckerContext context)
    {
        for (PolicyChecker policyChecker : policies) {
            policyChecker.update(context);
        }
    }

    @Override
    public void finish(@Nonnull PolicyCheckerContext context)
    throws PolicyViolationException
    {
        // The 'master' exception when fastfail is false
        PolicyViolationException exception = null;

        for (PolicyChecker policyChecker : policies)
        {
            try {
                policyChecker.finish(context);
            }
            catch(PolicyViolationException e)
            {
                logger.debug("Policy violation.", e);

                if (fastFail) {
                    throw e;
                }


                if (exception == null)
                {
                    // Use the root cause of the first exception as the base message
                    exception = new PolicyViolationException(e.getMessage());
                }

                exception.addViolations(e.getViolations());
            }
        }

        if (exception != null) {
            throw exception;
        }
    }

    @Override
    public boolean isFastFail() {
        return fastFail;
    }

    @Override
    public void setFastFail(boolean fastFail) {
        this.fastFail = fastFail;
    }
}
