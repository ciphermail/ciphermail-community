/*
 * Copyright (c) 2013-2019 CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import javax.mail.MessagingException;
import javax.mail.Part;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class to get all (sub) parts of a mail part
 *
 * @author Martijn Brinkers
 *
 */
public class PartFlattener
{
    /*
     * If the MIME depth exceeds this value, scanning will stop
     */
    private int maxDepth = 32;

    /*
     * If true, and the maxDepth is reached a MaxDepthReachedException exception will be thrown.
     */
    private boolean exceptionOnMaxDepthReached = false;

    private static class PartContext
    {
        /*
         * The collected parts
         */
        final List<Part> parts = new LinkedList<>();
    }

    /**
     * Returns all the (sub) parts of the part. If the part is not a multipart, the part will be returned as-is.
     */
    public List<Part> flatten(Part part)
    throws MessagingException, IOException, PartException
    {
        PartContext context = new PartContext();

        PartScanner partScanner = new PartScanner((parent, part1, context1) ->
        {
            ((PartContext) context1).parts.add(part1);

            return true;
        }, maxDepth);

        partScanner.scanPart(part, context);

        return context.parts;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    public boolean isExceptionOnMaxDepthReached() {
        return exceptionOnMaxDepthReached;
    }

    public void setExceptionOnMaxDepthReached(boolean exceptionOnMaxDepthReached) {
        this.exceptionOnMaxDepthReached = exceptionOnMaxDepthReached;
    }
}
