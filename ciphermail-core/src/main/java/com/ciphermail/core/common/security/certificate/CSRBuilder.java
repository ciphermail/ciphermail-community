/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.ExtensionsGenerator;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.LinkedList;
import java.util.List;

public class CSRBuilder extends TLSKeyPairBuilder
{
    /*
     * The generated keypair
     */
    private KeyPair keyPair;

    /*
     * Subject of the CSR
     */
    private X500Name subject;

    /*
     * Domain names to add to the CSR
     */
    private final List<String> domains = new LinkedList<>();

    private CSRBuilder() {
        // use CSRBuilder#getInstance
    }

    public static CSRBuilder getInstance() {
        return new CSRBuilder();
    }

    private Extensions createSubjectAlternativeNameExtension()
    throws IOException
    {
        List<GeneralName> namesList = new LinkedList<>();

        for (String domain : domains) {
            namesList.add(new GeneralName(GeneralName.dNSName, domain));
        }

        if (namesList.isEmpty()) {
            return null;
        }

        GeneralNames subjectAltNames = new GeneralNames(namesList.toArray(new GeneralName[] {}));

        ExtensionsGenerator extGen = new ExtensionsGenerator();

        extGen.addExtension(Extension.subjectAlternativeName, false, subjectAltNames);

        return extGen.generate();
    }

    public PKCS10CertificationRequest buildPKCS10()
    throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException,
            OperatorCreationException, IOException
    {
        if (subject == null) {
            throw new IllegalArgumentException("Subject is not set");
        }

        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        ContentSigner signer;

        switch (getKeyAlgorithm().getJcaAlgorithm()) {
            case "RSA" -> {
                keyPair = buildKeyPair();

                String signatureAlgorithm;

                if (getKeyAlgorithm().getKeySize() >= 4096) {
                    signatureAlgorithm = "SHA512withRSA";
                }
                else if (getKeyAlgorithm().getKeySize() >= 3072) {
                    signatureAlgorithm = "SHA384withRSA";
                }
                else {
                    signatureAlgorithm = "SHA256withRSA";
                }

                signer = new JcaContentSignerBuilder(signatureAlgorithm)
                        .setProvider(securityFactory.getNonSensitiveProvider())
                        .build(keyPair.getPrivate());
            }
            case "EC" -> {
                keyPair = buildKeyPair();

                String signatureAlgorithm;

                if (getKeyAlgorithm().getKeySize() >= 521) {
                    signatureAlgorithm = "SHA512withECDSA";
                }
                else if (getKeyAlgorithm().getKeySize() >= 384) {
                    signatureAlgorithm = "SHA384withECDSA";
                }
                else {
                    signatureAlgorithm = "SHA256withECDSA";
                }

                signer = new JcaContentSignerBuilder(signatureAlgorithm)
                        .setProvider(securityFactory.getNonSensitiveProvider())
                        .build(keyPair.getPrivate());
            }
            default -> throw new IllegalArgumentException("Unsupported algorithm " + getKeyAlgorithm().getJcaAlgorithm());
        }

        PKCS10CertificationRequestBuilder requestBuilder = new JcaPKCS10CertificationRequestBuilder(subject,
                keyPair.getPublic());

        Extensions subjectAlternativeNameExtension = createSubjectAlternativeNameExtension();

        if (subjectAlternativeNameExtension != null)
        {
            requestBuilder.addAttribute(PKCSObjectIdentifiers.pkcs_9_at_extensionRequest,
                    subjectAlternativeNameExtension);
        }

        return requestBuilder.build(signer);
    }

    public KeyPair getKeyPair() {
        return keyPair;
    }

    public X500Name getSubject() {
        return subject;
    }

    public CSRBuilder setSubject(X500Name subject)
    {
        this.subject = subject;
        return this;
    }

    public CSRBuilder addDomain(@Nonnull String domain)
    {
        domains.add(domain);
        return this;
    }
}
