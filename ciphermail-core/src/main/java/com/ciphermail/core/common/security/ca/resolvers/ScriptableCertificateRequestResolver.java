/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.resolvers;

import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.CertificateRequestResolverException;
import com.ciphermail.core.common.util.ProcessRunner;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * ReplaceSubjectTokensCertificateRequestResolver extension which executes an external script which can modify
 * the subject.
 */
public class ScriptableCertificateRequestResolver extends ReplaceSubjectTokensCertificateRequestResolver
{
    private static final Logger logger = LoggerFactory.getLogger(ScriptableCertificateRequestResolver.class);

    /*
     * The maximum time in milliseconds a command may run after which it is destroyed
     */
    private long timeout = 30 * DateUtils.MILLIS_PER_SECOND;

    /*
     * Max output size we are interested in
     */
    private int maxOutputSize = SizeUtils.KB * 1000;

    /*
     * Max error output size we are interested in
     */
    private int maxErrorSize = SizeUtils.KB * 10;

    /*
     * If true, the postfix command will be run with sudo
     */
    private boolean requireSudo;

    /*
     * The executable to use to resolve the subject
     */
    private String baseCommand;

    @Override
    public void resolve(String email, CertificateRequest certificateRequest)
    throws CertificateRequestResolverException
    {
        super.resolve(email, certificateRequest);

        // skip if command is not set
            if (StringUtils.isBlank(baseCommand)) {
            return;
        }

        ProcessRunner processRunner = new ProcessRunner();

        processRunner.setThrowExceptionOnErrorExitCode(true);
        processRunner.setRequireSudo(isRequireSudo());

        List<String> cmd = new LinkedList<>(ProcessRunner.splitCommandLine(baseCommand));

        String subject = Optional.ofNullable(certificateRequest.getSubject()).map(X500Principal::getName).orElse("");

        cmd.add("--email");
        cmd.add(email);
        cmd.add("--subject");
        cmd.add(subject);

        StringWriter outputWriter = new StringWriter();
        StringWriter errorWriter = new StringWriter();

        try {
            try(WriterOutputStream output = WriterOutputStream.builder()
                    .setWriter(outputWriter).setCharset(StandardCharsets.UTF_8).get();
                WriterOutputStream error = WriterOutputStream.builder()
                    .setWriter(errorWriter).setCharset(StandardCharsets.UTF_8).get())
            {
                processRunner.setOutput(output);
                processRunner.setError(error);

                processRunner.run(cmd);
            }

            String result = StringUtils.trimToNull(outputWriter.toString());

            if (result != null)
            {
                // extract the first line
                LineIterator lineIterator = new LineIterator(new StringReader(result));

                String replacedSubject = lineIterator.next();

                if (replacedSubject != null && !StringUtils.equals(subject, replacedSubject))
                {
                    logger.debug("Subject {}, replaced by {}", subject, replacedSubject);

                    certificateRequest.setSubject(new X500Principal(replacedSubject));
                }
            }
        }
        catch(IOException e) {
            throw new CertificateRequestResolverException(errorWriter.toString(), e);
        }
    }

    public long getTimeout() {
        return timeout;
    }

    public ScriptableCertificateRequestResolver setTimeout(long timeout)
    {
        this.timeout = timeout;
        return this;
    }

    public int getMaxOutputSize() {
        return maxOutputSize;
    }

    public ScriptableCertificateRequestResolver setMaxOutputSize(int maxOutputSize)
    {
        this.maxOutputSize = maxOutputSize;
        return this;
    }

    public int getMaxErrorSize() {
        return maxErrorSize;
    }

    public ScriptableCertificateRequestResolver setMaxErrorSize(int maxErrorSize)
    {
        this.maxErrorSize = maxErrorSize;
        return this;
    }

    public boolean isRequireSudo() {
        return requireSudo;
    }

    public ScriptableCertificateRequestResolver setRequireSudo(boolean requireSudo)
    {
        this.requireSudo = requireSudo;
        return this;
    }

    public String getBaseCommand() {
        return baseCommand;
    }

    public ScriptableCertificateRequestResolver setBaseCommand(String baseCommand)
    {
        this.baseCommand = baseCommand;
        return this;
    }
}
