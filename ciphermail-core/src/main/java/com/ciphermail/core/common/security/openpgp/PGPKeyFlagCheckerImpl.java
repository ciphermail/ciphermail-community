/*
 * Copyright (c) 2014-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.apache.commons.collections.CollectionUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of PGPKeyFlagChecker
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyFlagCheckerImpl implements PGPKeyFlagChecker
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyFlagCheckerImpl.class);

    /*
     * For validating signatures
     */
    private final PGPSignatureValidator signatureValidator;

    /*
     * For validating the user IDs
     */
    private final PGPUserIDValidator userIDValidator;

    /*
     * If true (the default), signatures are validates
     */
    private boolean validateSignatures = true;

    public PGPKeyFlagCheckerImpl(@Nonnull PGPSignatureValidator signatureValidator,
            @Nonnull PGPUserIDValidator userIDValidator)
    {
        this.signatureValidator = Objects.requireNonNull(signatureValidator);
        this.userIDValidator = Objects.requireNonNull(userIDValidator);
    }

    private boolean isValidForEncryption(PGPPublicKey masterKey, PGPPublicKey subKey)
    throws PGPException
    {
        if (masterKey == null) {
            throw new PGPException("master key parameter is not set.");
        }

        if (!masterKey.isMasterKey()) {
            throw new PGPException("master key parameter is not a master key but a sub key.");
        }

        if (subKey != null && subKey.isMasterKey()) {
            throw new PGPException("subkey parameter is not a sub key but a master key.");
        }

        PGPPublicKey publicKey = subKey;

        if (publicKey == null) {
            publicKey = masterKey;
        }

        PGPPublicKeyAlgorithm algorithm = PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm());

        boolean valid = false;

        // Only specific public key algorithms are supported for encryption
        if (algorithm == PGPPublicKeyAlgorithm.RSA ||
            algorithm == PGPPublicKeyAlgorithm.RSA_ENCRYPT ||
            algorithm == PGPPublicKeyAlgorithm.ECDH ||
            algorithm == PGPPublicKeyAlgorithm.ELGAMAL ||
            algorithm == PGPPublicKeyAlgorithm.ELGAMAL_GENERAL ||
            algorithm == PGPPublicKeyAlgorithm.DIFFIE_HELLMAN)
        {
            // Check if the key flags allows encryption but only for version > 3
            if (publicKey.getVersion() <= 3) {
                valid = true;
            }
            else {
                valid = hasKeyFlags(masterKey, subKey, PGPKeyFlags.ENCRYPT_COMMS, PGPKeyFlags.ENCRYPT_STORAGE);
            }
        }

        return valid;
    }

    private boolean isValidForSigning(PGPPublicKey masterKey, PGPPublicKey subKey)
    throws PGPException
    {
        if (masterKey == null) {
            throw new PGPException("master key parameter is not set.");
        }

        if (!masterKey.isMasterKey()) {
            throw new PGPException("master key parameter is not a master key but a sub key.");
        }

        if (subKey != null && subKey.isMasterKey()) {
            throw new PGPException("subkey parameter is not a sub key but a master key.");
        }

        PGPPublicKey publicKey = subKey;

        if (publicKey == null) {
            publicKey = masterKey;
        }

        PGPPublicKeyAlgorithm algorithm = PGPPublicKeyAlgorithm.fromTag(publicKey.getAlgorithm());

        boolean valid = false;

        // Only specific public key algorithms are supported for encryption
        //
        // Note: ELGAMAL_GENERAL (tag 0x20 should no longer be used for signatures since this is insecure. See RFC 4880
        // section "13.8. Reserved Algorithm Numbers".
        if (algorithm == PGPPublicKeyAlgorithm.RSA ||
            algorithm == PGPPublicKeyAlgorithm.RSA_SIGN ||
            algorithm == PGPPublicKeyAlgorithm.DSA ||
            algorithm == PGPPublicKeyAlgorithm.ECDSA ||
            algorithm == PGPPublicKeyAlgorithm.EDDSA)
        {
            // Check if the key flags allows signing but only for version > 3
            if (publicKey.getVersion() <= 3) {
                valid = true;
            }
            else {
                valid = hasKeyFlags(masterKey, subKey , PGPKeyFlags.SIGN_DATA);
            }
        }

        return valid;
    }

    private List<PGPSignature> getValidSignatures(PGPPublicKey masterKey, PGPPublicKey subKey,
            List<PGPSignature> signatures, List<byte[]> userIDs)
    {
        List<PGPSignature> validSignatures = new LinkedList<>();

        if (subKey == null)
        {
            // For a master key, we need to check the User ID signatures
            for (PGPSignature signature : signatures)
            {
                for (byte[] userID : userIDs)
                {
                    try {
                        if (signatureValidator.validateUserIDSignature(masterKey, userID, signature))
                        {
                            validSignatures.add(signature);

                            // Try next signature
                            break;
                        }
                    }
                    catch (Exception e) {
                        // Since we do not know which signature belongs to which userID, we will log only in debug
                        // mode because if there are a large number of User ID signatures, there will be a lot of
                        // failures if the signer is not trusted (or unknown).
                        logger.debug("Error validating the User ID signature.", e);
                    }
                }
            }
        }
        else {
            // For a sub key, we need to check the sub key binding signatures
            for (PGPSignature signature : signatures)
            {
                try {
                    if (signatureValidator.validateSubkeyBindingSignature(masterKey, subKey, signature)) {
                        validSignatures.add(signature);
                    }
                }
                catch (Exception e) {
                    logger.debug("Error validating the sub key binding signature.", e);
                }
            }
        }

        return validSignatures;
    }

    private List<byte[]> getValidUserIDs(PGPPublicKey publicKey)
    {
        List<byte[]> validUserIDs = new LinkedList<>();

        List<byte[]> allUserIDs = PGPPublicKeyInspector.getUserIDs(publicKey);

        for (byte[] userID : allUserIDs)
        {
            String userIDString = PGPUtils.userIDToString(userID);

            if (validateSignatures)
            {
                try {
                    if (userIDValidator.validateUserID(userID, publicKey)) {
                        validUserIDs.add(userID);
                    }
                }
                catch (PGPException e)
                {
                    logger.debug("Error validating user ID " + userIDString + " for key with Key ID " +
                            PGPUtils.getKeyIDHex(publicKey.getKeyID()), e);
                }
                catch (Exception e)
                {
                    logger.error("Error validating user ID " + userIDString + " for key with Key ID " +
                            PGPUtils.getKeyIDHex(publicKey.getKeyID()), e);
                }
            }
            else {
                validUserIDs.add(userID);
            }
        }

        return validUserIDs;
    }

    /*
     * Returns true if the public key has has any of the key flags
     */
    private boolean hasKeyFlags(PGPPublicKey masterKey, PGPPublicKey subKey, PGPKeyFlags... flags)
    {
        if (flags == null || flags.length == 0) {
            return false;
        }

        if (subKey == null)
        {
            boolean hasKeyFlagsSubPacket = false;

            // For a master key we need to check if any of the User ID certifications allow the flags. We will only
            // use the User ID if the User ID is valid
            List<byte[]> userIDs = getValidUserIDs(masterKey);

            if (CollectionUtils.isNotEmpty(userIDs))
            {
                for (int certificationType : PGPSignatureInspector.USER_ID_CERTIFICATION_TYPES)
                {
                    List<PGPSignature> signatures = PGPSignatureInspector.getSignaturesOfType(masterKey,
                            certificationType);

                    if (PGPSignatureInspector.hasKeyFlagsSubPacket(signatures))
                    {
                        hasKeyFlagsSubPacket = true;

                        List<PGPSignature> validSignatures = validateSignatures ? getValidSignatures(masterKey, subKey,
                                signatures, userIDs) : signatures;

                        if (CollectionUtils.isNotEmpty(validSignatures) && PGPSignatureInspector.hasKeyFlags(signatures,
                                flags))
                        {
                            return true;
                        }
                    }
                }

                if (!hasKeyFlagsSubPacket) {
                    // If there were no key flags sub packets, assume the key can be used. This is how Enigmail works
                    // when a key does not contain a key flags sub packet so we will match the behavior
                    return true;
                }
            }
            else {
                logger.warn("There were no valid User IDs for key with Key ID {}",
                        PGPUtils.getKeyIDHex(masterKey.getKeyID()));
            }
        }
        else
        {
            boolean hasKeyFlagsSubPacket = false;

            // For a subkey we need to check the subkey binding signature
            List<PGPSignature> signatures = PGPSignatureInspector.getSignaturesOfType(subKey,
                    PGPSignature.SUBKEY_BINDING);

            if (PGPSignatureInspector.hasKeyFlagsSubPacket(signatures))
            {
                hasKeyFlagsSubPacket = true;

                List<PGPSignature> validSignatures = validateSignatures ? getValidSignatures(masterKey, subKey,
                        signatures, null) : signatures;

                if (CollectionUtils.isNotEmpty(validSignatures) && PGPSignatureInspector.hasKeyFlags(signatures,
                        flags))
                {
                    return true;
                }
            }

            if (!hasKeyFlagsSubPacket) {
                // If there were no key flags sub packets, assume the key can be used. This is how Enigmail works
                // when a key does not contain a key flags sub packet so we will match the behavior
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isMasterKeyValidForEncryption(@Nonnull PGPPublicKey masterKey)
    throws PGPException
    {
        return isValidForEncryption(masterKey, null);
    }

    @Override
    public boolean isSubKeyValidForEncryption(@Nonnull  PGPPublicKey masterKey, @Nonnull PGPPublicKey subKey)
    throws PGPException
    {
        return isValidForEncryption(masterKey, subKey);
    }

    @Override
    public boolean isMasterKeyValidForSigning(@Nonnull PGPPublicKey masterKey)
    throws PGPException
    {
        return isValidForSigning(masterKey, null);
    }

    @Override
    public boolean isSubKeyValidForSigning(@Nonnull PGPPublicKey masterKey, @Nonnull PGPPublicKey subKey)
    throws PGPException
    {
        return isValidForSigning(masterKey, subKey);
    }

    public boolean isValidateSignatures() {
        return validateSignatures;
    }

    public void setValidateSignatures(boolean validateSignatures) {
        this.validateSignatures = validateSignatures;
    }
}
