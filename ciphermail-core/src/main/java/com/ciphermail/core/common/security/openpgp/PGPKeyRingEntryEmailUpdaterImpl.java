/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.ByteArray;
import org.apache.commons.collections.CollectionUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of PGPKeyRingEntryEmailUpdater that checks whether the provided userIDs are valid and whether
 * there are any user IDs revoked and if so, removes the email addresses which are revoked,
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyRingEntryEmailUpdaterImpl implements PGPKeyRingEntryEmailUpdater
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyRingEntryEmailUpdaterImpl.class);

    /*
     * whether new email addresses from the User IDs will be added
     */
    private boolean updateEmailAddresses = true;

    /*
     * Is used for checking whether the User IDs are valid
     */
    private final PGPUserIDValidator userIDValidator;

    /*
     * Is used for checking whether any existing User IDs are revoked
     */
    private final PGPUserIDRevocationChecker userIDRevocationChecker;

    public PGPKeyRingEntryEmailUpdaterImpl(@Nonnull PGPUserIDValidator userIDValidator,
            @Nonnull PGPUserIDRevocationChecker userIDRevocationChecker)
    {
        this.userIDValidator = Objects.requireNonNull(userIDValidator);
        this.userIDRevocationChecker = Objects.requireNonNull(userIDRevocationChecker);
    }

    private Set<ByteArray> getRevokedUserIDs(@Nonnull PGPPublicKey publicKey)
    {
        List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(publicKey);

        Set<ByteArray> revokedUserIDs = new HashSet<>();

        for (byte[] userID : userIDs)
        {
            boolean revoked = true;

            try {
                revoked = userIDRevocationChecker.isRevoked(userID, publicKey);
            }
            catch (Exception e)
            {
                logger.error("Error checking revocation of User ID {} for key with Key ID {}",
                        PGPUtils.userIDToString(userID), PGPUtils.getKeyIDHex(publicKey.getKeyID()));
            }

            if (revoked) {
                revokedUserIDs.add(new ByteArray(userID));
            }
        }

        return revokedUserIDs;
    }

    private Set<ByteArray> getValidUserIDs(Collection<ByteArray> userIDs, PGPPublicKey publicKey)
    {
        Set<ByteArray> userIDsToCheck = userIDs != null ? new HashSet<>(userIDs) : new HashSet<>();

        // Only use User IDs which are part of the key
        userIDsToCheck.retainAll(ByteArray.toList(PGPPublicKeyInspector.getUserIDs(publicKey)));

        Set<ByteArray> validUserIDs = new HashSet<>();

        for (ByteArray userIDWrapper : userIDsToCheck)
        {
            boolean valid = false;

            try {
                valid = userIDValidator.validateUserID(userIDWrapper.getBytes(), publicKey);
            }
            catch (Exception e)
            {
                logger.error("Error checking validity of User ID {} for key with Key ID {}",
                        PGPUtils.userIDToString(userIDWrapper.getBytes()), PGPUtils.getKeyIDHex(publicKey.getKeyID()));
            }

            if (valid) {
                validUserIDs.add(userIDWrapper);
            }
        }

        return validUserIDs;
    }

    @Override
    public void updateEmail(@Nonnull PGPKeyRingEntry entry, Collection<byte[]> newUserIDs)
    throws IOException, PGPException
    {
        if (entry.isMasterKey())
        {
            Set<String> emails = new HashSet<>(entry.getEmail());

            PGPPublicKey publicKey = entry.getPublicKey();

            Set<ByteArray> validNewUserIDs = getValidUserIDs(ByteArray.toList(newUserIDs),
                    publicKey);

            if (isUpdateEmailAddresses() && CollectionUtils.isNotEmpty(validNewUserIDs))
            {
                for (ByteArray validNewUserIDWrapper : validNewUserIDs)
                {
                    emails.addAll(PGPUtils.getEmailAddressesFromUserID(PGPUtils.userIDToString(
                            validNewUserIDWrapper.getBytes())));
                }
            }

            // Get all the revoked User IDs
            Set<ByteArray> revokedUserIDs = getRevokedUserIDs(publicKey);

            for (ByteArray revokedUserIDWrapper : revokedUserIDs)
            {
                // The User ID is revoked so we need to remove the email address from the list of email addresses
                emails.removeAll(PGPUtils.getEmailAddressesFromUserID(PGPUtils.userIDToString(
                        revokedUserIDWrapper.getBytes())));
            }

            entry.setEmail(emails);
        }
        else {
            logger.debug("Only master keys will have email addresses");
        }
    }

    public boolean isUpdateEmailAddresses() {
        return updateEmailAddresses;
    }

    public void setUpdateEmailAddresses(boolean updateEmailAddresses) {
        this.updateEmailAddresses = updateEmailAddresses;
    }
}
