/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.util.HexUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.time.DateUtils;
import org.bouncycastle.bcpg.SignatureSubpacket;
import org.bouncycastle.bcpg.SignatureSubpacketTags;
import org.bouncycastle.bcpg.sig.KeyExpirationTime;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureSubpacketVector;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class PGPPublicKeyInspector
{
    private PGPPublicKeyInspector() {
        // empty on purpose
    }

    /**
     * Returns the fingerprint of the public key in hex form
     */
    public static String getFingerprintHex(@Nonnull PGPPublicKey publicKey) {
        return HexUtils.hexEncode(publicKey.getFingerprint());
    }

    /**
     * Returns the fingerprint of the public key calculated with V4 format but using SHA256 instead of SHA1
     *
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static byte[] getSHA256Fingerprint(@Nonnull PGPPublicKey publicKey)
    throws NoSuchAlgorithmException, NoSuchProviderException, IOException
    {
        MessageDigest digest = PGPSecurityFactoryFactory.getSecurityFactory().createMessageDigest(
                Digest.SHA256.getFactoryName());

        byte[] encodedPublicKey = publicKey.getPublicKeyPacket().getEncodedContents();

        digest.update((byte)0x99);
        digest.update((byte)(encodedPublicKey.length >> 8));
        digest.update((byte)encodedPublicKey.length);
        digest.update(encodedPublicKey);

        return digest.digest();
    }

    /**
     * Returns the fingerprint (in hex) of the public key calculated with V4 format but using SHA256 instead of SHA1
     *
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static String getSHA256FingerprintHex(@Nonnull PGPPublicKey publicKey)
    throws NoSuchAlgorithmException, NoSuchProviderException, IOException
    {
        return HexUtils.hexEncode(getSHA256Fingerprint(publicKey));
    }

    /**
     * Returns all the UserIDs of the public key
     */
    public static Set<String> getUserIDsAsStrings(@Nonnull PGPPublicKey publicKey)
    {
        Set<String> strings = new LinkedHashSet<>();

        List<byte[]> userIDs = getUserIDs(publicKey);

        for (byte[] userID : userIDs) {
            strings.add(PGPUtils.userIDToString(userID));
        }

        return strings;
    }

    /**
     * Returns all the UserIDs of the public key
     */
    public static List<byte[]> getUserIDs(@Nonnull PGPPublicKey publicKey)
    {
        List<byte[]> ids = new LinkedList<>();

        Iterator<?> it = publicKey.getRawUserIDs();

        if (it != null)
        {
            while (it.hasNext())
            {
                Object next = it.next();

                if (next instanceof byte[] nextBytes) {
                    ids.add(nextBytes);
                }
            }
        }

        return ids;
    }

    private static Long getKeyValiditySeconds(@Nonnull PGPSignature signature)
    {
        Long result = null;

        // We only check hashed sub packets for the expiration time. Storing it in the unhashed sub packets
        // makes no sense since anyone can add them
        PGPSignatureSubpacketVector subPackets = signature.getHashedSubPackets();

        if (subPackets != null)
        {
            SignatureSubpacket packet = subPackets.getSubpacket(SignatureSubpacketTags.KEY_EXPIRE_TIME);

            if (packet != null) {
                result = ((KeyExpirationTime)packet).getTime();
            }
        }

        return result;
    }

    /**
     * Returns the number of seconds the key is valid from the creation time
     *
     * WARNING: THIS METHOD DOES NOT VALIDATE THE SIGNATURES AND SHOULD THEREFORE ONLY BE USED AS ROUGH WAY TO GET
     * THE EXPIRATION DATE
     */
    public static long getKeyValidSeconds(@Nonnull PGPPublicKey key)
    {
        // Number of seconds the key is valid. 0 means it will always be valid (i.e., never expires)
        long validSeconds = -1;

        Date latestSigDate = null;

        if (key.getVersion() <= 3) {
            validSeconds = key.getPublicKeyPacket().getValidDays() * 24L * 60 * 60;
        }
        else if (key.isMasterKey())
        {
            // The expiration time is stored on one of the certification signatures
            List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(key);

            for (byte[] userID : userIDs)
            {
                List<PGPSignature> signatures = PGPSignatureInspector.getSignaturesForUserID(key, userID);

                for (PGPSignature signature : signatures)
                {
                    if (ArrayUtils.contains(PGPSignatureInspector.USER_ID_CERTIFICATION_TYPES,
                            signature.getSignatureType()))
                    {
                        Long thisValidSeconds = getKeyValiditySeconds(signature);

                        Date thisSigDate = signature.getCreationTime();

                        if (latestSigDate == null || thisSigDate.after(latestSigDate))
                        {
                            latestSigDate = thisSigDate;

                            validSeconds = thisValidSeconds != null ? thisValidSeconds : 0;
                        }
                    }
                }
            }
        }
        else {
            // Sub key expiration should be stored in the sub key binding signatures
            List<PGPSignature> signatures = PGPPublicKeyInspector.getSubkeyBindingSignatures(key);

            for (PGPSignature signature : signatures)
            {
                Long thisValidSeconds = getKeyValiditySeconds(signature);

                Date thisSigDate = signature.getCreationTime();

                if (latestSigDate == null || thisSigDate.after(latestSigDate))
                {
                    latestSigDate = thisSigDate;

                    validSeconds = thisValidSeconds != null ? thisValidSeconds : 0;
                }
            }
        }

        if (validSeconds == -1) {
            validSeconds = 0;
        }

        return validSeconds;
    }

    /**
     * Returns the expiration date of the key. Null if the key never expires.
     *
     * WARNING: THIS METHOD DOES NOT VALIDATE THE SIGNATURES AND SHOULD THEREFORE ONLY BE USED AS ROUGH WAY TO GET
     * THE EXPIRATION DATE
     */
    public static Date getExpirationDate(@Nonnull PGPPublicKey publicKey)
    {
        long validSeconds = getKeyValidSeconds(publicKey);

        if (validSeconds < 0) {
            validSeconds = 0;
        }

        if (validSeconds > Integer.MAX_VALUE) {
            validSeconds = Integer.MAX_VALUE;
        }

        Date creationDate = publicKey.getCreationTime();

        return creationDate != null && validSeconds != 0 ? DateUtils.addSeconds(creationDate,
                (int) validSeconds) : null;
    }

    /**
     * Returns the 0x18 Subkey Binding Signatures if available
     */
    public static List<PGPSignature> getSubkeyBindingSignatures(@Nonnull PGPPublicKey publicKey)
    {
        List<PGPSignature> allSignatures = PGPSignatureInspector.getSignatures(publicKey);

        List<PGPSignature> subKeyBindingSignatures = new LinkedList<>();

        for (PGPSignature signature : allSignatures)
        {
            if (signature.getSignatureType() == PGPSignature.SUBKEY_BINDING) {
                subKeyBindingSignatures.add(signature);
            }
        }

        return subKeyBindingSignatures;
    }

    /**
     * Returns the key id of the master key that "owns" this key. The key id from a "0x18: Subkey Binding Signature"
     * signature packet is returned.
     *
     * Note: only the first sub key binding signaure's key id is returned.
     */
    public static Long getSubkeyBindingSignatureKeyID(@Nonnull PGPPublicKey publicKey)
    {
        List<PGPSignature> signatures = getSubkeyBindingSignatures(publicKey);

        return  CollectionUtils.isNotEmpty(signatures) ? signatures.get(0).getKeyID() : null;
    }
}
