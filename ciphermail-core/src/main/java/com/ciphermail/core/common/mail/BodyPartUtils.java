/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.locale.CharacterEncoding;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.util.CRLFInputStream;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimePart;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

public class BodyPartUtils
{
    private static final Logger logger = LoggerFactory.getLogger(BodyPartUtils.class);

    private BodyPartUtils() {
        // empty on purpose
    }

    private static final byte[] CRLF = new byte[]{'\r', '\n'};

    public static MimeBodyPart makeContentBodyPart(@Nonnull Part sourcePart, @Nonnull HeaderMatcher matcher)
    throws MessagingException, IOException
    {
        MimeBodyPart newBodyPart = new MimeBodyPart();

        newBodyPart.setContent(sourcePart.getContent(), sourcePart.getContentType());

        HeaderUtils.copyHeaders(sourcePart, newBodyPart, matcher);

        return newBodyPart;
    }

    public static MimeBodyPart makeContentBodyPartRaw(@Nonnull MimeBodyPart sourceMessage,
            @Nonnull HeaderMatcher matcher)
    throws MessagingException
    {
        // getRawInputStream() can throw a MessagingException when the source message is a MimeMessage
        // that is created in code (ie. not from a stream)
        InputStream messageStream = sourceMessage.getRawInputStream();

        MimeBodyPart newBodyPart = new MimeBodyPart(messageStream);

        HeaderUtils.copyHeaders(sourceMessage, newBodyPart, matcher);

        return newBodyPart;
    }

    public static MimeBodyPart makeContentBodyPartRaw(@Nonnull MimeMessage sourceMessage,
            @Nonnull HeaderMatcher matcher)
    throws MessagingException
    {
        // getRawInputStream() can throw a MessagingException when the source message is a MimeMessage
        // that is created in code (ie. not from a stream)
        InputStream messageStream = sourceMessage.getRawInputStream();

        MimeBodyPart newBodyPart = new MimeBodyPart(messageStream);

        HeaderUtils.copyHeaders(sourceMessage, newBodyPart, matcher);

        return newBodyPart;
    }

    /**
     * This is the only way I know of to create a new MimeBodyPart from another MimeBodyPart which is safe
     * for signed email. All other methods break the signature when quoted printable soft breaks are used.
     *
     * example of quoted printable soft breaks:
     *
     * Content-Transfer-Encoding: quoted-printable

     * soft break example =
     * another line =
     *
     * All other methods will re-encode and removes the soft breaks.
     *
     * @param sourceMessage
     * @param matcher
     * @return
     * @throws IOException
     * @throws MessagingException
     */
    @SuppressWarnings("unchecked")
    public static MimeBodyPart makeContentBodyPartRawBytes(@Nonnull MimeBodyPart sourceMessage,
            @Nonnull HeaderMatcher matcher)
    throws IOException, MessagingException
    {
        // getRawInputStream() can throw a MessagingException when the source message is a MimeMessage
        // that is created in code (ie. not from a stream)
        InputStream messageStream = sourceMessage.getRawInputStream();

        byte[] rawMessage = IOUtils.toByteArray(messageStream);

        InternetHeaders destinationHeaders = new InternetHeaders();

        Enumeration<Header> sourceHeaders = sourceMessage.getAllHeaders();

        HeaderUtils.copyHeaders(sourceHeaders, destinationHeaders, matcher);

        return new MimeBodyPart(destinationHeaders, rawMessage);
    }

    /**
     * This is the only way I know of to create a new MimeBodyPart from another Message which is safe
     * for signed email. All other methods break the signature when quoted printable soft breaks are used.
     *
     * example of quoted printable soft breaks:
     *
     * Content-Transfer-Encoding: quoted-printable

     * soft break example =
     * another line =
     *
     * All other methods will re-encode and removes the soft breaks.
     *
     * @param sourceMessage
     * @param matcher
     * @return
     * @throws IOException
     * @throws MessagingException
     */
    @SuppressWarnings("unchecked")
    public static MimeBodyPart makeContentBodyPartRawBytes(@Nonnull MimeMessage sourceMessage,
            @Nonnull HeaderMatcher matcher)
    throws IOException, MessagingException
    {
        // getRawInputStream() can throw a MessagingException when the source message is a MimeMessage
        // that is created in code (ie. not from a stream)
        InputStream messageStream = sourceMessage.getRawInputStream();

        byte[] rawMessage = IOUtils.toByteArray(messageStream);

        InternetHeaders destinationHeaders = new InternetHeaders();

        Enumeration<Header> sourceHeaders = sourceMessage.getAllHeaders();

        HeaderUtils.copyHeaders(sourceHeaders, destinationHeaders, matcher);

        return new MimeBodyPart(destinationHeaders, rawMessage);
    }

    public static MimeBodyPart makeContentBodyPart(@Nonnull MimeBodyPart sourcePart, @Nonnull HeaderMatcher matcher)
    throws MessagingException, IOException
    {
        MimeBodyPart mimeBodyPart = null;

        try {
            mimeBodyPart = makeContentBodyPartRawBytes(sourcePart, matcher);
        }
        catch (IOException e) {
            logger.error("makeContentBodyPartRaw failed. Trying makeContentBodyPartStandard.", e);
        }
        catch (MessagingException e) {
            // makeContentBodyPartRaw is not supported if the sourcePart is a MimeMessage that was
            // created in code ie. not from a stream
            logger.debug("makeContentBodyPartRaw failed. Trying makeContentBodyPartStandard.", e);
        }

        if (mimeBodyPart == null) {
            mimeBodyPart = makeContentBodyPart((MimePart)sourcePart, matcher);
        }

        return mimeBodyPart;
    }

    public static MimeBodyPart makeContentBodyPart(@Nonnull MimeMessage sourcePart, @Nonnull HeaderMatcher matcher)
    throws MessagingException, IOException
    {
        MimeBodyPart mimeBodyPart = null;

        try {
            mimeBodyPart = makeContentBodyPartRawBytes(sourcePart, matcher);
        }
        catch (IOException e) {
            logger.error("makeContentBodyPartRaw failed. Trying makeContentBodyPartStandard.", e);
        }
        catch (MessagingException e) {
            // makeContentBodyPartRaw is not supported if the sourcePart is a MimeMessage that was
            // created in code ie. not from a stream
            logger.debug("makeContentBodyPartRaw failed. Trying makeContentBodyPartStandard.", e);
        }

        if (mimeBodyPart == null) {
            mimeBodyPart = makeContentBodyPart((MimePart)sourcePart, matcher);
        }

        return mimeBodyPart;
    }

    /**
     * Extracts the MIME from the input message
     */
    public static void writeMIMEBodyPart(@Nonnull MimeMessage sourceMessage, @Nonnull HeaderMatcher matcher,
            @Nonnull OutputStream output)
    throws IOException, MessagingException
    {
        InputStream mime = null;

        // getRawInputStream() can throw a MessagingException when the source message is a MimeMessage
        // that is created in code (ie. not from a stream)
        try {
            mime = new CRLFInputStream(sourceMessage.getRawInputStream());

            InternetHeaders destinationHeaders = new InternetHeaders();

            @SuppressWarnings("unchecked")
            Enumeration<Header> sourceHeaders = sourceMessage.getAllHeaders();

            HeaderUtils.copyHeaders(sourceHeaders, destinationHeaders, matcher);

            @SuppressWarnings("unchecked")
            Enumeration<String> headerLines = destinationHeaders.getAllHeaderLines();

            while (headerLines.hasMoreElements())
            {
                output.write(MiscStringUtils.getBytesASCII(headerLines.nextElement()));
                output.write(CRLF);
            }

            output.write(CRLF);

            IOUtils.copy(mime, output);
        }
        catch (MessagingException e) {
            // #getRawInputStream can fail if the MimeMessage was not created from MIME
            MimeBodyPart part = makeContentBodyPart(sourceMessage, matcher);

            part.writeTo(output);
        }
    }

    /**
     * Creates a MimeMessage from a Part. If part is an instance of MimeMessage the part is returned as a new
     * MimeMessage if clone is true. If clone is false the Part is cast to MimeMessage and returned.
     */
    public static MimeMessage toMessage(@Nonnull Part part, boolean clone)
    throws MessagingException, IOException
    {
        if (part instanceof MimeMessage mimeMessage) {
            return clone ? MailUtils.cloneMessage(mimeMessage) : (MimeMessage) part;
        }

        return MailUtils.byteArrayToMessage(MailUtils.partToByteArray(part));
    }

    public static MimeMessage toMessage(@Nonnull Part part)
    throws MessagingException, IOException
    {
        return toMessage(part, false /* do not clone */);
    }

    /**
     * Creates a MimeBodyPart from a Part. If part is an instance of MimeBodyPart the part is returned without
     * any modification.
     */
    public static MimeBodyPart toMimeBodyPart(@Nonnull Part part)
    throws MessagingException, IOException
    {
        if (part instanceof MimeBodyPart mimeBodyPart) {
            return mimeBodyPart;
        }

        return new MimeBodyPart(new ByteArrayInputStream(MailUtils.partToByteArray(part)));
    }

    /**
     * Creates a MimeBodyPart with the provided message attached as a RFC822 attachment.
     */
    public static MimeBodyPart toRFC822(@Nonnull MimeMessage message, String filename, String disposition)
    throws MessagingException
    {
        MimeBodyPart bodyPart = new MimeBodyPart();

        bodyPart.setContent(message, "message/rfc822");
        /* somehow the content-Type header is not set so we need to set it ourselves */
        bodyPart.setHeader("Content-Type", "message/rfc822");
        bodyPart.setDisposition(disposition);

        if (filename != null) {
            bodyPart.setFileName(filename);
        }

        return bodyPart;
    }

    /**
     * Creates a MimeBodyPart with the provided message attached as a RFC822 attachment with the disposition set
     * to Part.INLINE.
     */
    public static MimeBodyPart toRFC822(@Nonnull MimeMessage message, String filename)
    throws MessagingException
    {
        return toRFC822(message, filename, Part.INLINE);
    }

    /**
     * Extracts the message from the RFC822 attachment.
     * @throws MessagingException
     * @throws IOException
     */
    public static MimeMessage extractFromRFC822(@Nonnull Part rfc822)
    throws IOException, MessagingException
    {
        if (!rfc822.isMimeType("message/rfc822")) {
            throw new MessagingException("Part is-not-a message/rfc822 but " + rfc822.getContentType());
        }

        return new MimeMessage(MailSession.getDefaultSession(), rfc822.getInputStream());
    }

    /**
     * Searches for an embedded RFC822 messages. Returns the first embedded RFC822 it finds.
     * Only one level deep is searched.
     *
     * Returns null if there are no embedded message.
     */
    public static MimeMessage searchForRFC822(@Nonnull MimeMessage message)
    throws MessagingException, IOException
    {
        // Fast fail. Only multipart mixed messages are supported.
        if (!message.isMimeType("multipart/mixed")) {
            return null;
        }

        Multipart mp;

        try {
            mp = (Multipart) message.getContent();
        }
        catch (IOException e) {
            throw new MessagingException("Error getting message content.", e);
        }

        MimeMessage embeddedMessage = null;

        for (int i=0; i < mp.getCount(); i++)
        {
            BodyPart part = mp.getBodyPart(i);

            if (part.isMimeType("message/rfc822"))
            {
                embeddedMessage = BodyPartUtils.extractFromRFC822(part);

                break;
            }
        }

        return embeddedMessage;
    }

    public static String charsetForBody(String body)
    {
        // Return replyBodyCharset if the body contains non-ascii characters
        return MiscStringUtils.isPrintableAscii(body) ? CharacterEncoding.US_ASCII : CharacterEncoding.UTF_8;
    }
}
