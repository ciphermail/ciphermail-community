/*
 * Copyright (c) 2016-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.transport.twilio;

import com.ciphermail.core.app.properties.SMSTransportsCategory;
import com.ciphermail.core.app.properties.UserPropertiesType;
import com.ciphermail.core.app.properties.UserProperty;
import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

import javax.annotation.Nonnull;

/**
 * Implementation of TwilioProperties that delegates property setter/getter to a HierarchicalProperties instance.
 *
 * @author Martijn Brinkers
 *
 */
@UserPropertiesType(name = "twilio", displayName = "Twilio", parent = SMSTransportsCategory.NAME, order = 30)
public class TwilioPropertiesImpl extends DelegatedHierarchicalProperties implements TwilioProperties
{
    /**
     * Property names
     */
    public static final String FROM = "sms-transport-twilio-from";
    public static final String ACCOUNT_SID = "sms-transport-twilio-account-sid";
    public static final String AUTH_TOKEN = "sms-transport-twilio-auth-token";
    public static final String API_KEY_SID = "sms-transport-twilio-api-key-sid";
    public static final String API_KEY_SECRET = "sms-transport-twilio-api-key-secret";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public TwilioPropertiesImpl(@Nonnull HierarchicalProperties delegatedProperties) {
        super(delegatedProperties);
    }

    public static TwilioPropertiesImpl getInstance(@Nonnull HierarchicalProperties delegatedProperties) {
        return new TwilioPropertiesImpl(delegatedProperties);
    }

    @Override
    @UserProperty(name = FROM,
            user = false, domain = false
    )
    public void setFrom(String from)
    throws HierarchicalPropertiesException
    {
        setProperty(FROM, from);
    }

    @Override
    @UserProperty(name = FROM,
            user = false, domain = false,
            order = 10,
            allowNull = true
    )
    public String getFrom()
    throws HierarchicalPropertiesException
    {
        return getProperty(FROM);
    }

    @Override
    @UserProperty(name = ACCOUNT_SID,
            user = false, domain = false
    )
    public void setAccountSid(String accountSid)
    throws HierarchicalPropertiesException
    {
        setProperty(ACCOUNT_SID, accountSid);
    }

    @Override
    @UserProperty(name = ACCOUNT_SID,
            user = false, domain = false,
            order = 20,
            allowNull = true
    )
    public String getAccountSid()
    throws HierarchicalPropertiesException
    {
        return getProperty(ACCOUNT_SID);
    }

    @Override
    @UserProperty(name = AUTH_TOKEN,
            user = false, domain = false
    )
    public void setAuthToken(String authToken)
    throws HierarchicalPropertiesException
    {
        setProperty(AUTH_TOKEN, authToken);
    }

    @Override
    @UserProperty(name = AUTH_TOKEN,
            user = false, domain = false,
            order = 30,
            allowNull = true
    )
    public String getAuthToken()
    throws HierarchicalPropertiesException
    {
        return getProperty(AUTH_TOKEN);
    }

    @Override
    @UserProperty(name = API_KEY_SID,
            user = false, domain = false
    )
    public void setAPIKeySid(String apiKeySid)
    throws HierarchicalPropertiesException
    {
        setProperty(API_KEY_SID, apiKeySid);
    }

    @Override
    @UserProperty(name = API_KEY_SID,
            user = false, domain = false,
            order = 40,
            allowNull = true
    )
    public String getAPIKeySid()
    throws HierarchicalPropertiesException
    {
        return getProperty(API_KEY_SID);
    }

    @Override
    @UserProperty(name = API_KEY_SECRET,
            user = false, domain = false
    )
    public void setAPISecretKey(String apiSecretKey)
    throws HierarchicalPropertiesException
    {
        setProperty(API_KEY_SECRET, apiSecretKey);
    }

    @Override
    @UserProperty(name = API_KEY_SECRET,
            user = false, domain = false,
            order = 50,
            allowNull = true
    )
    public String getAPISecretKey()
    throws HierarchicalPropertiesException
    {
        return getProperty(API_KEY_SECRET);
    }

    @Override
    public void copyTo(TwilioProperties other)
    throws HierarchicalPropertiesException
    {
        if (other == null) {
            return;
        }

        other.setFrom(getFrom());
        other.setAccountSid(getAccountSid());
        other.setAuthToken(getAuthToken());
        other.setAPIKeySid(getAPIKeySid());
        other.setAPISecretKey(getAPISecretKey());
    }
}
