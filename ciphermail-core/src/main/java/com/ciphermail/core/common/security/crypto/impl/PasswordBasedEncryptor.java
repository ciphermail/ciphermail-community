/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crypto.impl;

import com.ciphermail.core.common.security.crypto.Encryptor;
import com.ciphermail.core.common.security.crypto.EncryptorException;
import com.ciphermail.core.common.security.password.PBEncryption;
import com.ciphermail.core.common.security.password.PBEncryptionImpl;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Objects;

/**
 * Implementation of Encryptor that encrypts and decrypts with a password using AES128 (@See PBEncryption)
 * The
 *
 * @author Martijn Brinkers
 *
 */
public class PasswordBasedEncryptor implements Encryptor
{
    private static final String DEFAULT_ALGORITHM = "PBEWITHSHA256AND128BITAES-CBC-BC";

    /*
     * larger iteration count improves security because "brute force" password cracking takes
     * longer. Disadvantage, naturally, is that it takes longer to decrypt the key.
     */
    private static final int DEFAULT_ITERATION_COUNT = 256;

    /*
     * default salt length in bytes (16 = 128 bits/8)
     */
    private static final int DEFAULT_SALT_LENGTH = 16;

    private final String password;

    /*
     * The class that does the encryption/decryption.
     */
    private final PBEncryption pbEncryption;

    public PasswordBasedEncryptor(@Nonnull String password)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        this(password, DEFAULT_ITERATION_COUNT, DEFAULT_SALT_LENGTH, DEFAULT_ALGORITHM);
    }

    public PasswordBasedEncryptor(@Nonnull String password, int iterationCount, int saltLength,
            @Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        this.password = Objects.requireNonNull(password);

        this.pbEncryption = new PBEncryptionImpl(iterationCount, saltLength, Objects.requireNonNull(algorithm));
    }

    @Override
    public byte[] encrypt(@Nonnull byte[] data)
    throws EncryptorException
    {
        try {
            return pbEncryption.encrypt(data, password.toCharArray());
        }
        catch (GeneralSecurityException | IOException e) {
            throw new EncryptorException(e);
        }
    }

    @Override
    public byte[] decrypt(@Nonnull byte[] data)
    throws EncryptorException
    {
        try {
            return pbEncryption.decrypt(data, password.toCharArray());
        }
        catch (GeneralSecurityException | IOException e) {
            throw new EncryptorException(e);
        }
    }
}
