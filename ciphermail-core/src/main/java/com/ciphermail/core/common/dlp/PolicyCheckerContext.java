/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp;

import com.ciphermail.core.common.util.Context;

import java.util.Collection;

/**
 * Context used by the PolicyChecker's.
 *
 * @author Martijn Brinkers
 *
 */
public interface PolicyCheckerContext extends Context
{
    /**
     * Returns the content stored in the context. The content can be a partial content.
     * Null if there is no content.
     */
    void setContent(String content);
    String getContent();

    /**
     * True if the content is a partial content. Content is partial if the complete text
     * is too big for one time. The content is then split into separate parts. When
     * the content is partial, an overlap might be added to make sure that the boundary
     * is scanned correctly (for example a CC number might be split in two parts, to find the
     * CC number, the two parts should be scanned as one).
     */
    void setPartial(boolean partial);
    boolean isPartial();

    /**
     * The PolicyPattern's stored in the context. Can be null.
     */
    void setPatterns(Collection<PolicyPattern> patterns);
    Collection<PolicyPattern> getPatterns();
}
