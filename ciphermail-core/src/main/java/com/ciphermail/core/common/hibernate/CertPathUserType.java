/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import com.ciphermail.core.common.security.SecurityFactoryFactory;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metamodel.spi.ValueAccess;
import org.hibernate.usertype.CompositeUserType;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.security.NoSuchProviderException;
import java.security.cert.CertPath;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

/**
 * Hibernate CompositeUserType that can be used to persist {@link CertPath}.
 *
 * @author Martijn Brinkers
 *
 */
public class CertPathUserType implements CompositeUserType<CertPath>
{
    private static final int CERT_PATH_COLUMN_INDEX      = 0;
    private static final int CERT_PATH_TYPE_COLUMN_INDEX = 1;

    public static final String CERT_PATH_COLUMN_NAME      = "certPath";
    public static final String CERT_PATH_TYPE_COLUMN_NAME = "certPathType";

    public static class BinaryContentWithTypeMapper {
        // in sorted order
        byte[] certPath;
        String certPathType;
    }

    /*
     * This is used for serializing/deserializing CertPath
     */
    public static class EncodedCertPathAndType implements Serializable
    {
        EncodedCertPathAndType(byte[] encodedCertPath, String certPathType)
        {
            this.encodedCertPath = encodedCertPath;
            this.certPathType = certPathType;
        }

        private final byte[] encodedCertPath;
        private final String certPathType;
    }

    @Override
    public Object getPropertyValue(CertPath certPath, int propertyIndex)
    throws HibernateException
    {
        try {
            return switch (propertyIndex) {
                case CERT_PATH_COLUMN_INDEX      -> certPath.getEncoded();
                case CERT_PATH_TYPE_COLUMN_INDEX -> certPath.getType();
                default -> null;
            };
        }
        catch (CertificateEncodingException e) {
            throw new HibernateException(e);
        }
    }

    private CertPath decodeCertPath(byte[] encodedCertPath, String certPathType)
    {
        CertPath certPath = null;

        if (encodedCertPath != null)
        {
            try {
                CertificateFactory factory = SecurityFactoryFactory.getSecurityFactory().
                        createCertificateFactory(certPathType);

                certPath = factory.generateCertPath(new ByteArrayInputStream(encodedCertPath));
            }
            catch (CertificateException | NoSuchProviderException e) {
                throw new HibernateException(e);
            }
        }

        return certPath;
    }

    @Override
    public CertPath instantiate(ValueAccess valueAccess, SessionFactoryImplementor sessionFactoryImplementor)
    {
        return decodeCertPath(
                valueAccess.getValue(CERT_PATH_COLUMN_INDEX, byte[].class),
                valueAccess.getValue(CERT_PATH_TYPE_COLUMN_INDEX, String.class));
    }

    @Override
    public Class<?> embeddable() {
        return BinaryContentWithTypeMapper.class;
    }

    @Override
    public Class<CertPath> returnedClass() {
        return CertPath.class;
    }

    @Override
    public boolean equals(CertPath certPath1, CertPath certPath2)
    {
        if (certPath1 == certPath2) {
            return true;
        }

        if (certPath1 == null || certPath2 == null) {
            return false;
        }

        return certPath1.equals(certPath2);
    }

    @Override
    public int hashCode(CertPath certPath) {
        return certPath.hashCode();
    }

    @Override
    public CertPath deepCopy(CertPath certPath)
    {
        // According to javadoc:
        // It is not necessary to copy immutable objects, or null values, in which case it is safe to simply return the
        // argument.
        return certPath;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(CertPath certPath)
    {
        try {
            return certPath != null ? new EncodedCertPathAndType(certPath.getEncoded(), certPath.getType())
                    : null;
        }
        catch (CertificateEncodingException e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public CertPath assemble(Serializable cached, Object owner)
    {
        EncodedCertPathAndType encodedCertPathAndType = (EncodedCertPathAndType) cached;

        return decodeCertPath(encodedCertPathAndType.encodedCertPath, encodedCertPathAndType.certPathType);
    }

    @Override
    public CertPath replace(CertPath detached, CertPath managed, Object owner)
    {
        // According to javadoc:
        // For immutable objects, or null values, it is safe to simply return the first parameter.
        return detached;
    }
}
