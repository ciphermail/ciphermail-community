/*
 * Copyright (c) 2008-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.locale.CharacterEncoding;
import com.ciphermail.core.common.security.PEMFilter;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchProviderException;
import java.security.cert.CertSelector;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CertificateUtils
{
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(CertificateUtils.class);

    private CertificateUtils() {
        // empty on purpose
    }

    /**
     * Loads certificates from the specified input
     * @param input
     * @return
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static Collection<? extends Certificate> readCertificates(@Nonnull InputStream input)
    throws CertificateException, NoSuchProviderException
    {
        CertificateFactory fac = SecurityFactoryFactory.getSecurityFactory().
                createCertificateFactory("X.509");

        // Because of BC issue #758 (https://github.com/bcgit/bc-java/issues/758), we want to filter PEM input to make
        // sure we are backwards compatible, i.e., we can read PEM certificates which could be read by older BC releases
        // We therefore need to fully read the input otherwise we cannot check whether the input is PEM and filter
        try {
            ByteArrayInputStream bais = input instanceof ByteArrayInputStream byteArrayInputStream ?
                    byteArrayInputStream : new ByteArrayInputStream(IOUtils.toByteArray(input));

            bais.mark(1);

            int tag = bais.read();

            if (tag == -1) {
                // Return empty collection
                return new LinkedList<>();
            }

            bais.reset();

            if (tag != 0x30)
            {
                // Assume PEM encoding. Filter the PEM
                PEMFilter pemFilter = new PEMFilter(PEMFilter.DEFAULT_CERTIFICATE_HEADER_FOOTERS);

                String pem = StringUtils.trimToNull(pemFilter.readPEM(bais, CharacterEncoding.UTF_8));

                if (pem == null)
                {
                    // Return empty collection
                   return new LinkedList<>();
                }

                bais = new ByteArrayInputStream(MiscStringUtils.getBytesUTF8(pem));
            }

            return fac.generateCertificates(bais);
        }
        catch (IOException e) {
            throw new CertificateException(e);
        }
    }

    /**
     * Loads certificates from the specified input. Only X509certificates are returned.
     * @param input
     * @return
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static @Nonnull List<X509Certificate> readX509Certificates(@Nonnull InputStream input)
    throws CertificateException, NoSuchProviderException
    {
        Collection<? extends Certificate> certificates = readCertificates(input);

        List<X509Certificate> x509Certificates = new LinkedList<>();

        CollectionUtils.copyCollectionFiltered(certificates, x509Certificates, X509Certificate.class);

        return x509Certificates;
    }

    /**
     * Loads the certificate from the specified file
     * @param file
     * @return
     * @throws CertificateException
     * @throws NoSuchProviderException
     * @throws FileNotFoundException
     */
    public static Collection<? extends Certificate> readCertificates(@Nonnull File file)
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        FileInputStream input = new FileInputStream(file);
        BufferedInputStream buffered = new BufferedInputStream(input);

        try {
            return readCertificates(buffered);
        }
        finally {
            try {
                buffered.close();
            }
            catch(IOException e) {
                // log and ignore
                logger.error(e.getMessage(), e);
            }
            try {
                input.close();
            }
            catch(IOException e) {
                // log and ignore
                logger.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Loads the certificate from the specified file. Only X509Certificates are returned.
     * @param file
     * @return
     * @throws CertificateException
     * @throws NoSuchProviderException
     * @throws FileNotFoundException
     */
    public static @Nonnull List<X509Certificate> readX509Certificates(@Nonnull File file)
    throws CertificateException, NoSuchProviderException, FileNotFoundException
    {
        Collection<? extends Certificate> certificates = readCertificates(file);

        List<X509Certificate> x509Certificates = new LinkedList<>();

        CollectionUtils.copyCollectionFiltered(certificates, x509Certificates, X509Certificate.class);

        return x509Certificates;
    }


    /**
     * Writes the certificate to the output stream
     * @param certificate
     * @param output
     * @throws CertificateEncodingException
     * @throws IOException
     */
    public static void writeCertificate(@Nonnull Certificate certificate, @Nonnull OutputStream output)
    throws CertificateEncodingException, IOException
    {
        output.write(certificate.getEncoded());
    }

    /**
     * Writes the certificate to the specified file
     * @param certificate
     * @param file
     * @throws CertificateEncodingException
     * @throws IOException
     */
    public static void writeCertificate(@Nonnull Certificate certificate, @Nonnull File file)
    throws CertificateEncodingException, IOException
    {
        FileOutputStream outputStream = new FileOutputStream(file);
        BufferedOutputStream buffered = new BufferedOutputStream(outputStream);

        try {
            writeCertificate(certificate, buffered);
        }
        finally {
            try {
                buffered.close();
            }
            catch(IOException e) {
                // log and ignore
                logger.error(e.getMessage(), e);
            }
            try {
                outputStream.close();
            }
            catch(IOException e) {
                // log and ignore
                logger.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Writes the certificates to a stream in the provided format
     * @param certificates
     * @param output
     * @param encoding
     * @throws IOException
     * @throws CertificateEncodingException
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static void writeCertificates(@Nonnull Collection<? extends Certificate> certificates,
            @Nonnull OutputStream output, @Nonnull ObjectEncoding encoding)
    throws CertificateEncodingException, IOException
    {
        output.write(CertificateEncoder.encode(certificates, encoding));
    }

    /**
     * Writes the certificates to a stream in PKCS#7 format
     * @param certificates
     * @param output
     * @throws IOException
     * @throws CertificateEncodingException
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static void writeCertificates(@Nonnull Collection<? extends Certificate> certificates,
            @Nonnull OutputStream output)
    throws CertificateEncodingException, IOException
    {
        writeCertificates(certificates, output, ObjectEncoding.DER);
    }

    /**
     * Writes the certificates to the specified file in the provided format
     * @param certificates
     * @param file
     * @param encoding
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static void writeCertificates(@Nonnull Collection<? extends Certificate> certificates, @Nonnull File file,
            @Nonnull ObjectEncoding encoding)
    throws IOException, CertificateException
    {
        FileOutputStream outputStream = new FileOutputStream(file);
        BufferedOutputStream buffered = new BufferedOutputStream(outputStream);

        try {
            writeCertificates(certificates, buffered, encoding);
        }
        finally {
            try {
                buffered.close();
            }
            catch(IOException e) {
                // log and ignore
                logger.error(e.getMessage(), e);
            }
            try {
                outputStream.close();
            }
            catch(IOException e) {
                // log and ignore
                logger.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Writes the certificates to the specified file in PKCS#7 format
     * @param certificates
     * @param file
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchProviderException
     */
    public static void writeCertificates(@Nonnull Collection<? extends Certificate> certificates, @Nonnull File file)
    throws IOException, CertificateException
    {
        writeCertificates(certificates, file, ObjectEncoding.DER);
    }


    /**
     * Returns a list of certificates matching the selector
     * @param <T>
     * @param certificates
     * @param selector
     * @return
     */
    public static @Nonnull <T extends Certificate> List<T> getMatchingCertificates(Collection<T> certificates,
            CertSelector selector)
    {
        List<T> selected = new LinkedList<>();

        if (certificates != null && selector != null)
        {
            for (T certificate : certificates)
            {
                if (selector.match(certificate)) {
                    selected.add(certificate);
                }
            }
        }

        return selected;
    }

    /**
     * Returns an array of all the X509Certificates from the input. Never null.
     *
     * @param certificates
     * @return
     */
    public static @Nonnull X509Certificate[] getX509Certificates(Certificate... certificates)
    {
        X509Certificate[] result;

        if (certificates != null)
        {
            List<X509Certificate> x509Certs = new ArrayList<>(certificates.length);

            for (Certificate certificate : certificates)
            {
                if (certificate instanceof X509Certificate x509Certificate) {
                    x509Certs.add(x509Certificate);
                }
            }

            result = new X509Certificate[x509Certs.size()];

            result = x509Certs.toArray(result);
        }
        else {
            result = new X509Certificate[]{};
        }

        return result;
    }

    /**
     * Returns a List of all the X509Certificates from the input. Never null.
     *
     * @param certificates
     * @return
     */
    public static @Nonnull List<X509Certificate> getX509Certificates(Collection<? extends Certificate> certificates)
    {
        List<X509Certificate> result;

        if (certificates != null)
        {
            result = new ArrayList<>(certificates.size());

            for (Certificate certificate : certificates)
            {
                if (certificate instanceof X509Certificate x509Certificate) {
                    result.add(x509Certificate);
                }
            }
        }
        else {
            result = new ArrayList<>();
        }

        return result;
    }

    /**
     * Tries to find the issuer of the certificate from the collection of certificates. The issuer if found if the
     * issuer certificate's subject is equal to the issuer of the target certificate and if the public key of the
     * issuer certificate signed the target certificate.
     * <p>
     * Note: the issuer is not validated and not checked whether the issuer is allowed to issue certificates etc.
     */
    public @Nonnull static List<X509Certificate> findIssuers(X509Certificate targetCertificate,
            Collection<X509Certificate> certificates)
    {
        if (targetCertificate == null) {
            return new LinkedList<>();
        }

        Set<X509Certificate> issuers = new LinkedHashSet<>();

        // assume that if the public key of a certificate has signed a certificate, that we have found
        // the issuer
        X500Principal issuerPrincipal = targetCertificate.getIssuerX500Principal();

        for (X509Certificate other : certificates)
        {
            // Only check the signature if the issuer is correct
            if (ObjectUtils.equals(issuerPrincipal, other.getSubjectX500Principal()))
            {
                try {
                    targetCertificate.verify(other.getPublicKey());

                    // Signature was correct. We have found an issuer
                    issuers.add(other);
                }
                catch (Exception e) {
                    // Signature was not correct
                }
            }
        }

        return new LinkedList<>(issuers);
    }

    /**
     * Tries to find the chain of the certificate from the collection of certificates. The issuer if found if the
     * issuer certificate's subject is equal to the issuer of the target certificate and if the public key of the
     * issuer certificate signed the target certificate.
     * Only the first issuer of a certificate will be used, i.e., only one chain is tried.
     * <p>
     * NOTE: do NOT use this method for security sensitive operations because the chain is not validated. Use this
     * method for example to allow an admin to find the issuer of a certificate if the chain is incomplete because
     * when the chain is incomplete, a proper chain builder will not return a result.
     */
    public static @Nonnull List<X509Certificate> findChain(X509Certificate targetCertificate,
            Collection<X509Certificate> certificates)
    {
        // keep track of certs in order to remove any duplicates
        Set<X509Certificate> all = new HashSet<>(certificates);

        Set<X509Certificate> chain = new LinkedHashSet<>();

        if (targetCertificate == null) {
            return new LinkedList<>();
        }

        X509Certificate issuer = targetCertificate;

        while (issuer != null)
        {
            if (chain.contains(issuer)) {
                // the certificates was already handled, so we either have a self-signed certificate or found a loop
                break;
            }

            chain.add(issuer);

            List<X509Certificate> issuers = findIssuers(issuer, all);

            issuer = CollectionUtils.isNotEmpty(issuers) ? issuers.get(0) : null;
        }

        return new LinkedList<>(chain);
    }
}
