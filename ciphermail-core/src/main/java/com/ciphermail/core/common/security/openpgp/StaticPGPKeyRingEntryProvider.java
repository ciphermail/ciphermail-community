/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.annotation.Nonnull;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Implementation of PGPKeyRingEntryProvider that returns the keys read from a public keyring
 * <p>
 * Note: PGPKeyRingEntryProvider does not validate signatures. Child entries are only matched using the keyID which
 * might not be correct. The main purpose for this class is for unit tests.
 * <p>
 */
public class StaticPGPKeyRingEntryProvider implements PGPKeyRingEntryProvider
{
    /*
     * Map from keyID to PGPKeyRingEntry
     */
    private final Map<Long, StaticPGPKeyRingEntry> mappedEntries = new HashMap<>();

    public StaticPGPKeyRingEntryProvider(@Nonnull File file)
    throws IOException
    {
        List<PGPPublicKey> keys;

        InputStream input = new BufferedInputStream(new FileInputStream(file));

        try {
            do {
                keys = PGPKeyUtils.readPublicKeys(input);

                for (PGPPublicKey key : keys) {
                    mappedEntries.put(key.getKeyID(), new StaticPGPKeyRingEntry(UUID.randomUUID(), key));
                }
            }
            while (CollectionUtils.isNotEmpty(keys));

            // try to find all sub-keys and bind them to the master key

            for (StaticPGPKeyRingEntry entry : mappedEntries.values())
            {
                if (!entry.isMasterKey())
                {
                    Long parentKeyID = PGPPublicKeyInspector.getSubkeyBindingSignatureKeyID(entry.publicKey);

                    if (parentKeyID != null)
                    {
                        for (StaticPGPKeyRingEntry subEntry : mappedEntries.values())
                        {
                            if (subEntry.isMasterKey() && subEntry.getKeyID().equals(parentKeyID))
                            {
                                subEntry.subKeys.add(entry);
                                entry.setParentKey(subEntry);
                            }
                        }
                    }
                }
            }
        }
        finally {
            IOUtils.closeQuietly(input);
        }
    }

    @Override
    public StaticPGPKeyRingEntry getByKeyID(long keyID)
    throws PGPException, IOException
    {
        return mappedEntries.get(keyID);
    }

    public List<PGPKeyRingEntry> getPGPKeyRingEntries() {
        return mappedEntries.values().stream().map(t -> (PGPKeyRingEntry)t).toList();
    }
}
