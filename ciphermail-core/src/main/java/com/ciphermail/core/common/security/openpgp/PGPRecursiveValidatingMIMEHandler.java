/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailConstants;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.MimeUtils;
import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.mail.PartFlattener;
import com.ciphermail.core.common.util.LFInputStream;
import com.ciphermail.core.common.util.MiscClassUtils;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * PGP handler that recursively handles PGP encrypted/signed email and validates the signatures.
 *
 * Note: this class is not thread safe and should only be used once (i.e., a new instance should be created after
 * calling handleMessage)
 *
 * @author Martijn Brinkers
 *
 */
public class PGPRecursiveValidatingMIMEHandler
{
    private static final Logger logger = LoggerFactory.getLogger(PGPRecursiveValidatingMIMEHandler.class);

    /*
     * If certain subject tags (signed, encrypted etc.) should not be added, this special tag value can be used.
     * Settings the subject tag value to an empty string "" (in the GUI) will not work because that will result
     * in inheriting the value from the system factory settings. If the subject tag value is equal to <empty> we will
     * skip adding the tag
     */
    private static final String SKIP_SUBJECT_TAG = "<skip>";

    /**
     * PGP specific headers added to the message if a message is PGP signed and/or encrypted
     */
    private static final String BASE = MailConstants.CIPHERMAIL_HEADER_PREFIX + "-Info-PGP-";
    public static final String PGP_ENCODING = BASE + "Encoding";
    public static final String PGP_SIGNED = BASE + "Signed";
    public static final String PGP_SIGNER_KEY_ID = BASE + "Signer-KeyID";
    public static final String PGP_SIGNATURE_VALID = BASE + "Signature-Valid";
    public static final String PGP_SIGNATURE_FAILURE = BASE + "Signature-Failure";
    public static final String PGP_ENCRYPTED = BASE + "Encrypted";
    public static final String PGP_ENCRYPTION_ALGORITHM = BASE + "Encryption-Algorithm";
    public static final String PGP_DECRYPTION_KEY_NOT_FOUND = BASE + "Decryption-Key-Not-Found";
    public static final String PGP_SIGNER_MISMATCH = BASE + "Signer-Mismatch";
    public static final String PGP_SIGNER_EMAIL = BASE + "Signer-Email";
    public static final String PGP_MIXED_CONTENT = BASE + "Mixed-Content";

    /*
     * The maximum length of a header. A long header will always be folded. This maximum is the maximum
     * length of the header including folding.
     */
    private int maxHeaderLength = 4096;

    /*
     * maximum recursive depth for MIME parts.
     */
    private int maxMimeDepth = 8;

    /*
     * The maximum number of PGP objects. If the number of PGP objects read exceeds the maximum, a PGP
     * exception will be thrown.
     */
    private int maxObjects = 64;

    /*
     * The maximum number of times the message will be handled
     */
    private int maxRecursion = 4;

    /*
     * Provides PGP key pair based on key id
     */
    private final PGPKeyPairProvider keyPairProvider;

    /*
     * Listener which is called when a PGP keyring is found
     */
    private PGPPublicKeyRingListener publicKeyRingListener;

    /*
     * For validating the signature
     */
    private final PGPContentSignatureValidator signatureValidator;

    /*
     * The maximum number of bytes read for decompression. This is done to protect against "ZIP bombs".
     * The default value will be set to 10% of Runtime.getRuntime().maxMemory()
     */
    private int decompressionUpperlimit = (int) (Runtime.getRuntime().maxMemory() * 0.1);

    /*
     * If true, PGP inline is supported
     */
    private boolean inlinePGPEnabled = true;

    /*
     * If true, binary attachments, i.e., other than text/plain or text/html, that do not have a PGP type of extension
     * (.pgp, .asc etc.) will be skipped.
     */
    private boolean skipNonPGPExtensions = true;

    /*
     * If true, HTML parts are scanned for PGP by converting the HTML to text before scanning for PGP
     */
    private boolean scanHTMLForPGP = true;

    /*
     * If true (the default), the PGP blob will be decrypted if it is encrypted and a suitable key is available
     */
    private boolean decrypt = true;

    /*
     * If true, any PGP signature will be removed
     */
    private boolean removeSignature = true;

    /*
     * If true the original Message-ID will be used for the handled message
     */
    private boolean retainMessageID = true;

    /*
     * If true, x-pgp-encoding-format header will be used (if available) to detect whether a message is PGP/MIME
     */
    private boolean enablePGPUniversalWorkaround = true;

    /*
     * The subject template used when adding security info to the subject. The first parameter is replaced with the
     * current subject and the second parameter is replaced with the encryption or signing tag.
     *
     *  Example subject:
     *
     *  %1$s %2$s
     */
    private String subjectTemplate = "%1$s %2$s";

    /*
     * If true, security info will be added to the subject of the final message (see decryptedTag etc.)
     */
    private boolean addSecurityInfoToSubject;

    /*
     * The tag to add to the subject when the message was decrypted
     */
    private String decryptedTag;

    /*
     * The tag to add to the subject when the message was signed and valid
     */
    private String signedValidTag;

    /*
     * The tag to add to the subject when the message was signed and valid but signed using a different
     * email address then the sender
     */
    private String signedByValidTag;

    /*
     * The tag to add to the subject when the message was signed but not valid.
     */
    private String signedInvalidTag;

    /*
     * The tag to add to the subject if the message has mixed content (i.e., some but not all parts are encrypted and/or
     * signed
     */
    private String mixedContentTag;

    /*
     * True if the message was decrypted
     */
    private boolean decrypted;

    /*
     * True if a part was encrypted but the decryption key could not be found
     */
    private boolean decryptionKeyNotFound;

    /*
     * The mail ID of the message (if set). This is purely used for logging
     */
    private String mailID;

    public PGPRecursiveValidatingMIMEHandler(@Nonnull PGPKeyPairProvider keyPairProvider,
            @Nonnull PGPContentSignatureValidator signatureValidator)
    {
        this.keyPairProvider = Objects.requireNonNull(keyPairProvider);
        this.signatureValidator = Objects.requireNonNull(signatureValidator);
    }

    private PGPMIMEHandler createPGPMIMEHandler(boolean inlinePGPEnabled)
    {
        PGPMIMEHandler handler = new PGPMIMEHandler(keyPairProvider);

        handler.setInlinePGPEnabled(inlinePGPEnabled);
        handler.setSkipNonPGPExtensions(skipNonPGPExtensions);
        handler.setScanHTMLForPGP(scanHTMLForPGP);
        handler.setDecrypt(decrypt);
        handler.setRemoveSignature(removeSignature);
        handler.setRetainMessageID(retainMessageID);
        handler.setDecompressionUpperlimit(decompressionUpperlimit);
        handler.setMaxMimeDepth(maxMimeDepth);
        handler.setMaxObjects(maxObjects);
        handler.setPublicKeyRingListener(publicKeyRingListener);

        return handler;
    }

    /*
     * Makes sure the header is encoded when it contains non ASCII characters is folded and has a
     * maximum length.
     */
    private void setHeader(String name, String value, @Nonnull Part part)
    {
        if (value == null) {
            value = "";
        }

        value = MiscStringUtils.restrictLength(value, maxHeaderLength);

        try {
            // make sure the header only contains ASCII characters and is folded
            // when necessary
            value = HeaderUtils.encodeHeaderValue(name, value);
        }
        catch (UnsupportedEncodingException e) {
            logger.warn("Header value cannot be encoded.", e);
        }

        try {
            part.setHeader(name, value);
        }
        catch (MessagingException e) {
            logger.error("Error adding header", e);
        }
    }

    private void addTagToSubject(@Nonnull MimeMessage message, String tag)
    {

        if (SKIP_SUBJECT_TAG.equals(tag))
        {
            logger.debug("Skip tag detected. Skip adding the subject tag.");

            return;
        }

        try {
            String currentSubject = MailUtils.getSafeSubject(message);

            String newSubject = String.format(subjectTemplate, StringUtils.defaultString(currentSubject),
                    StringUtils.defaultString(tag));

            message.setSubject(newSubject);
        }
        catch (Exception e) {
            logger.error("Error while appending text to subject");
        }
    }

    private String getSignerKeyID(PGPSignatureLayerPart signaturePart)
    {
        String keyID = null;

        if (signaturePart != null)
        {
            PGPSignatureList signatureList = signaturePart.getPGPSignatureList();

            if (signatureList != null && signatureList.size() > 0) {
                // Get the first signature. We currently do not support multiple signers
                keyID = PGPUtils.getKeyIDHex(signatureList.get(0).getKeyID());
            }
        }

        return StringUtils.defaultIfEmpty(keyID, "Unknown");
    }

    private boolean isSignerMismatch(MimeMessage message, Set<String> allowedAddresses)
    {
        // Get the first From header and canonicalize it
        String canonicalizedFrom = EmailAddressUtils.canonicalize(EmailAddressUtils.getEmailAddress(
                EmailAddressUtils.getAddress(EmailAddressUtils.getFromQuietly(message))));

        if (StringUtils.isNotEmpty(canonicalizedFrom) && allowedAddresses != null)
        {
            for (String allowedEmailAddress : allowedAddresses)
            {
                String canonicalizedAllowed = EmailAddressUtils.canonicalize(allowedEmailAddress);

                if (StringUtils.equals(canonicalizedFrom, canonicalizedAllowed)) {
                    // Email address match, so no mismatch
                    return false;
                }

                // Check domain. The allowedEmailAddresses *can* be a domain only
                if (StringUtils.equals(EmailAddressUtils.getDomain(canonicalizedFrom), canonicalizedAllowed)) {
                    // Domain match, so no mismatch
                    return false;
                }
            }
        }

        return true;
    }

    private Set<String> getSignerEmails(PGPKeyRingEntry signer)
    {
        Set<String> emails = null;

        if (signer != null)
        {
            // We need to get the emails from the master key
            if (!signer.isMasterKey() && (signer.getParentKey() != null)) {
                signer = signer.getParentKey();
            }

            emails = signer.getEmail();
        }

        if (emails == null) {
            emails = new HashSet<>();
        }

        return emails;
    }

    private void addSignerInfoToSubject(MimeMessage message, boolean valid, boolean senderMismatch,
        Set<String> signerEmail)
    {
        String tag;

        if (valid) {
            // If there is a mismatch between "sender" and email address from certificate, show the
            // email address(es) of the signer(s).
            if (senderMismatch)
            {
                // There is a mismatch between the from and the signers. We will therefore add the signers
                // to the subject
                try {
                    tag = String.format(getSignedByValidTag(), StringUtils.defaultString(
                            StringUtils.join(signerEmail, ", ")));
                }
                catch (Exception e)
                {
                    logger.error("Invalid format string", e);

                    tag = "<invalid format string>";
                }
            }
            else {
                tag = getSignedValidTag();
            }
        }
        else {
            tag = getSignedInvalidTag();
        }

        addTagToSubject(message, tag);
    }

    private void validatePGPMIMESignature(MimeMessage message, PGPMIMEHandler handler)
    {
        // For PGP/MIME we only have one part (i.e., the message) so get the first one (index 0)
        List<PGPHandler> pgpHandlers = handler.getHandlers().get(0);

        if (CollectionUtils.isNotEmpty(pgpHandlers))
        {
            PGPHandler pgpHandler = pgpHandlers.get(pgpHandlers.size() - 1);

            List<PGPLayer> layers = pgpHandler.getPGPLayers();

            if (CollectionUtils.isNotEmpty(layers))
            {
                PGPLayer layer = layers.get(layers.size() - 1);

                // The last layer should contain a a literal and signature layer
                List<PGPLayerPart> parts = layer.getParts();

                if (parts.size() == 2)
                {
                    if (parts.get(0) instanceof PGPLiteralLayerPart literalPart &&
                        parts.get(1) instanceof PGPSignatureLayerPart signaturePart)
                    {
                        // The final data blob was signed. Now validate the signature

                        setHeader(PGP_SIGNED, "True", message);
                        setHeader(PGP_SIGNER_KEY_ID, getSignerKeyID(signaturePart), message);

                        PGPSignature signature = PGPSignatureInspector.getFirstSignature(
                                signaturePart.getPGPSignatureList());

                        if (signature != null)
                        {
                            InputStream contentInput = literalPart.getLiteralContent();

                            // Make sure that all lines end with CR/LF and all trailing whitespace is removed
                            // (see RFC 3156 section 5 "OpenPGP signed data") if it is a CANONICAL_TEXT_DOCUMENT
                            if (signature.getSignatureType() == PGPSignature.CANONICAL_TEXT_DOCUMENT)
                            {
                                try {
                                    contentInput = new PGPMIMECanonicalizerInputStream(contentInput, "US-ASCII");
                                }
                                catch (UnsupportedEncodingException e)
                                {
                                    // should never happen with US-ASCII
                                    logger.error("Unsupported Encoding", e);
                                }
                            }

                            PGPSignatureValidatorResult validatorResult = signatureValidator.validateSignature(
                                    contentInput, signature);

                            PGPKeyRingEntry signer = validatorResult.getSigner();

                            Set<String> signerEmails = getSignerEmails(signer);

                            if (validatorResult.isValid() && signer != null)
                            {
                                logger.info("PGP/MIME signature was valid; MailID: {}", mailID);

                                setHeader(PGP_SIGNATURE_VALID, "True", message);

                                // Signature is valid but there can be a sender mismatch (i.e., the from does not match
                                // an email address of the signer entry
                                boolean signerMismatch = isSignerMismatch(message, signerEmails);

                                if (signerMismatch)
                                {
                                    setHeader(PGP_SIGNER_MISMATCH, "True", message);
                                    setHeader(PGP_SIGNER_EMAIL, StringUtils.join(signerEmails, ", "), message);
                                }

                                if (addSecurityInfoToSubject) {
                                    addSignerInfoToSubject(message, true, signerMismatch, signerEmails);
                                }
                            }
                            else {
                                logger.warn("PGP/MIME signature was not valid; Failure message: {}; MailID: {}",
                                        validatorResult.getFailureMessage(), mailID);

                                setHeader(PGP_SIGNATURE_VALID, "False", message);
                                setHeader(PGP_SIGNATURE_FAILURE, validatorResult.getFailureMessage(), message);

                                if (addSecurityInfoToSubject) {
                                    addSignerInfoToSubject(message, false, false, signerEmails);
                                }
                            }
                        }
                        else {
                            logger.warn("Signature is null");
                        }
                    }
                    else {
                        logger.debug("Incorrect layer parts. Part 1 is a {} and part 2 is a {} ",
                                MiscClassUtils.getClassName(parts.get(0)), MiscClassUtils.getClassName(parts.get(1)));
                    }
                }
                else {
                    logger.debug("Layer contains {} parts instead of 2", parts.size());
                }
            }
            else {
                logger.debug("There are no PGPLayer's");
            }
        }
    }

    private PGPEncryptionLayerPart getEncryptionLayer(@Nonnull List<PGPMIMEHandler> handlers)
    {
        // Step through all handlers and through all layers to see whether there is an encryption layer
        for (PGPMIMEHandler handler : handlers)
        {
            // For PGP/MIME we only have one part (i.e., the message) so get the first one (index 0)
            List<PGPHandler> pgpHandlers = handler.getHandlers().get(0);

            if (CollectionUtils.isNotEmpty(pgpHandlers))
            {
                for (PGPHandler pgpHandler : pgpHandlers)
                {
                    List<PGPLayer> layers = pgpHandler.getPGPLayers();

                    if (CollectionUtils.isNotEmpty(layers))
                    {
                        for (PGPLayer layer : layers)
                        {
                            List<PGPLayerPart> layerParts = layer.getParts();

                            if (CollectionUtils.isNotEmpty(layerParts))
                            {
                                for (PGPLayerPart layerPart : layerParts)
                                {
                                    if (layerPart instanceof PGPEncryptionLayerPart pgpEncryptionLayerPart) {
                                        return pgpEncryptionLayerPart;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return null;
    }

    private void checkPGPMIMEEncrypted(@Nonnull MimeMessage message, List<PGPMIMEHandler> handlers)
    {
        PGPEncryptionLayerPart encryptionLayerPart = getEncryptionLayer(handlers);

        if (encryptionLayerPart != null) {
            // The message was encrypted
            decrypted = true;

            setHeader(PGP_ENCRYPTED, "True", message);

            PGPEncryptionAlgorithm encryptionAlgorithm = encryptionLayerPart.getEncryptionAlgorithm();

            if (encryptionAlgorithm != null) {
                setHeader(PGP_ENCRYPTION_ALGORITHM, encryptionAlgorithm.getFriendlyName(), message);
            }

            if (addSecurityInfoToSubject) {
                addTagToSubject(message, getDecryptedTag());
            }
        }
    }

    private void checkPGPInlineEncrypted(MimeMessage message, List<PGPMIMEHandler> mimeHandlers,
        MutableBoolean mixedContent)
    {
        try {
            PartFlattener flattener = new PartFlattener();

            List<Part> parts = flattener.flatten(message);

            // Check if there are the same amount of handlers as parts and that all parts are encrypted. We currently
            // only support encryption of the first layer of the first handler
            PGPMIMEHandler firstMIMEHandler = mimeHandlers.get(0);

            // If only some parts of the message are encrypted, we call this mixed content
            if (firstMIMEHandler.isMixedContent()) {
                mixedContent.setValue(true);
            }

            // In the first later, all PGPHandlers must be encrypted to mark the email as being encrypted and
            // the number of parts should match
            List<ArrayList<PGPHandler>> handlersPerPart = firstMIMEHandler.getHandlers();

            if (handlersPerPart.size() != parts.size())
            {
                // Should not happen
                logger.warn("Mismatch between parts of the message and handled parts");

                return;
            }

            Set<PGPEncryptionAlgorithm> encryptionAlgorithms = new HashSet<>();

            boolean encrypted = false;
            boolean notAllPartsEncrypted = false;

            for (ArrayList<PGPHandler> handlers : handlersPerPart)
            {
                if (CollectionUtils.isNotEmpty(handlers))
                {
                    for (PGPHandler handler : handlers)
                    {
                        boolean thisEncrypted = false;

                        List<PGPLayer> layers = handler.getPGPLayers();

                        // Check if the first layer is encrypted
                        if (CollectionUtils.isNotEmpty(layers))
                        {
                            PGPLayer firstLayer = layers.get(0);

                            List<PGPLayerPart> layerParts = firstLayer.getParts();

                            if (CollectionUtils.isNotEmpty(layerParts))
                            {
                                PGPLayerPart firstLayerPart = layerParts.get(0);

                                if (firstLayerPart instanceof PGPEncryptionLayerPart pgpEncryptionLayerPart)
                                {
                                    thisEncrypted = true;
                                    encrypted = true;

                                    PGPEncryptionAlgorithm encryptionAlgorithm = pgpEncryptionLayerPart.
                                            getEncryptionAlgorithm();

                                    if (encryptionAlgorithm != null) {
                                        encryptionAlgorithms.add(encryptionAlgorithm);
                                    }
                                }
                            }
                        }

                        if (!thisEncrypted) {
                            notAllPartsEncrypted = true;
                        }
                    }
                }
                else {
                    notAllPartsEncrypted = true;
                }
            }

            if (encrypted)
            {
                // There was at least one part encrypted.
                decrypted = true;

                setHeader(PGP_ENCRYPTED, "True", message);

                if (!encryptionAlgorithms.isEmpty()) {
                    // add friendly names of algorithms as a comma seperated string
                    setHeader(PGP_ENCRYPTION_ALGORITHM, encryptionAlgorithms.stream().map(
                            PGPEncryptionAlgorithm::getFriendlyName)
                            .collect(Collectors.joining (",")), message);
                }

                if (addSecurityInfoToSubject) {
                    addTagToSubject(message, getDecryptedTag());
                }

                // Check if all parts were encrypted or whether only some parts were encrypted
                if (notAllPartsEncrypted) {
                    mixedContent.setValue(true);
                }
            }
        }
        catch (PartException | MessagingException | IOException  e) {
            logger.error("Error post handling PGP/INLINE message");
        }
    }

    private Part findPartWithFilename(@Nonnull List<Part> parts, String filename)
    {
        for (Part part : parts)
        {
            if (StringUtils.equals(filename, HeaderUtils.decodeTextQuietly(MimeUtils.getFilenameQuietly(part)))) {
                return part;
            }
        }

        return null;
    }

    private MimeMessage removeDetachedSignatures(@Nonnull MimeMessage message,
            @Nonnull List<Part> detachedSignaturePartsToRemove)
    throws MessagingException
    {
        boolean partRemoved = false;

        try {
            for (Part part : detachedSignaturePartsToRemove)
            {
                if (part instanceof MimeBodyPart bodyPart)
                {
                    Multipart parent = bodyPart.getParent();

                    parent.removeBodyPart(bodyPart);

                    partRemoved = true;
                }
                else {
                    logger.warn("Detached signature cannot be removed since it was not a BodyPart");
                }
            }
        }
        catch (Exception e) {
            logger.error("Error while trying to remove detached signature", e);
        }

        if (partRemoved) {
            message.saveChanges();
        }

        return message;

    }

    private PGPSignatureValidatorResult validateDetachedSignature(@Nonnull Part signedPart,
            @Nonnull PGPSignature signature)
    throws IOException, MessagingException
    {
        PGPSignatureValidatorResult result = signatureValidator.
                validateSignature(signedPart.getInputStream(), signature);

        if (result.isTampered())
        {
            // On Linux, text attachments are signed with LF. If the attachment is not base64 encoded when sent
            // by email, SMTP will transform the line ending into CR/LF. This will break the signature. We therefore
            // will retry to verify with LF endings (but only if the part is not Base64 encoded).
            if (!StringUtils.equalsIgnoreCase("base64", MimeUtils.getContentTransferEncoding(signedPart)))
            {
                // Retry with line endings converted to LF
                PGPSignatureValidatorResult retry = signatureValidator.
                        validateSignature(new LFInputStream(signedPart.getInputStream()), signature);

                if (!retry.isTampered()) {
                    result = retry;
                }
            }
        }

        return result;
    }

    private MimeMessage validatePGPInlineSignatures(MimeMessage message, @Nonnull List<PGPMIMEHandler> mimeHandlers,
        @Nonnull MutableBoolean mixedContent)
    {
        try {
            PartFlattener flattener = new PartFlattener();

            List<Part> parts = flattener.flatten(message);

            // The last PGPMIMEHandler should contain the signatures (we assume that the message is first encrypted,
            // then signed)
            PGPMIMEHandler lastMIMEHandler = mimeHandlers.get(mimeHandlers.size() - 1);

            // If only some parts of the message are signed, we call this mixed content
            if (lastMIMEHandler.isMixedContent()) {
                mixedContent.setValue(true);
            }

            List<ArrayList<PGPHandler>> handlersPerPart = lastMIMEHandler.getHandlers();

            if (handlersPerPart.size() != parts.size())
            {
                // Should not happen
                logger.warn("Mismatch between parts of the message and handled parts");

                return message;
            }

            boolean signed = false;
            boolean signatureValid = true;
            boolean signerMismatch = false;

            int unsigned = 0;

            Set<String> signerKeyIDs = new HashSet<>();
            Set<String> allSignerEmails = new HashSet<>();
            Set<String> signatureFailureMessages = new HashSet<>();

            // Detached signature parts to remove
            List<Part> detachedSignaturePartsToRemove = new LinkedList<>();

            for (ArrayList<PGPHandler> handlers : handlersPerPart)
            {
                if (CollectionUtils.isNotEmpty(handlers))
                {
                    for (PGPHandler handler : handlers)
                    {
                        boolean thisSigned = false;

                        List<PGPLayer> layers = handler.getPGPLayers();

                        if (CollectionUtils.isNotEmpty(layers))
                        {
                            PGPLayer layer = layers.get(layers.size() - 1);

                            List<PGPLayerPart> layerParts = layer.getParts();

                            if (layerParts.size() == 1 && handler.getFilename() != null &&
                                    layerParts.get(0) instanceof PGPSignatureLayerPart signatureLayerPart)
                            {
                                // This might be a detached signature. Check whether there is an attachment with
                                // the signature filename. We need to remove any possible .pgp extension from the
                                // filename
                                String filename = PGPUtils.removePGPFilenameExtension(handler.getFilename());

                                if (StringUtils.isNotEmpty(filename))
                                {
                                    Part signedPart = findPartWithFilename(parts, filename);

                                    if (signedPart != null)
                                    {
                                        Part signaturePart = findPartWithFilename(parts, handler.getFilename());

                                        if (signaturePart != null) {
                                            detachedSignaturePartsToRemove.add(signaturePart);
                                        }

                                        thisSigned = true;

                                        // Validate the part
                                        signerKeyIDs.add(getSignerKeyID(signatureLayerPart));

                                        PGPSignature signature = PGPSignatureInspector.getFirstSignature(
                                                signatureLayerPart.getPGPSignatureList());

                                        if (signature != null)
                                        {
                                            // Since we found a detached signature, we have handled a part which
                                            // has been flagged as unsigned
                                            unsigned--;

                                            signed = true;

                                            PGPSignatureValidatorResult validatorResult = validateDetachedSignature(
                                                    signedPart, signature);

                                            PGPKeyRingEntry signer = validatorResult.getSigner();

                                            if (!validatorResult.isValid())
                                            {
                                                signatureValid = false;

                                                signatureFailureMessages.add(validatorResult.getFailureMessage());
                                            }
                                            else if (signer != null)
                                            {
                                                Set<String> signerEmails = getSignerEmails(signer);

                                                // There can be a sender mismatch (i.e., the from does not match an
                                                // email address of the signer entry
                                                if (isSignerMismatch(message, signerEmails)) {
                                                    signerMismatch = true;
                                                }

                                                allSignerEmails.addAll(signerEmails);
                                            }
                                        }
                                        else {
                                            logger.warn("Signature is null");
                                        }
                                    }
                                    else {
                                        logger.warn("Signed part not found");
                                    }
                                }
                            }
                            else if (layerParts.size() == 2)
                            {
                                // If the message was inline clear signed, the literal and signature layer should be
                                // the last two
                                if (layerParts.get(0) instanceof PGPLiteralLayerPart literalPart &&
                                    layerParts.get(1) instanceof PGPSignatureLayerPart signaturePart)
                                {
                                    thisSigned = true;

                                    /*
                                     * The final data blob was signed. Now validate the signature
                                     */
                                    signerKeyIDs.add(getSignerKeyID(signaturePart));

                                    PGPSignature signature = PGPSignatureInspector.getFirstSignature(
                                            signaturePart.getPGPSignatureList());

                                    if (signature != null)
                                    {
                                        signed = true;

                                        PGPSignatureValidatorResult validatorResult = signatureValidator.
                                                validateSignature(literalPart.getLiteralContent(), signature);

                                        PGPKeyRingEntry signer = validatorResult.getSigner();

                                        if (!validatorResult.isValid())
                                        {
                                            signatureValid = false;

                                            signatureFailureMessages.add(validatorResult.getFailureMessage());
                                        }
                                        else if (signer != null)
                                        {
                                            Set<String> signerEmails = getSignerEmails(signer);

                                            // There can be a sender mismatch (i.e., the from does not match an
                                            // email address of the signer entry
                                            if (isSignerMismatch(message, signerEmails)) {
                                                signerMismatch = true;
                                            }

                                            allSignerEmails.addAll(signerEmails);
                                        }
                                    }
                                    else {
                                        logger.warn("Signature is null");
                                    }
                                }
                            }
                        }

                        if (!thisSigned) {
                            // It might be that this is a binary part which has an external signature or this part might
                            // be a signature. This will only happen with signed only parts
                            unsigned++;
                        }
                    }
                }
                else {
                    // Should normally not happen
                    unsigned++;
                }
            }

            if (signed)
            {
                setHeader(PGP_SIGNED, "True", message);
                setHeader(PGP_SIGNER_KEY_ID, StringUtils.join(signerKeyIDs, ", "), message);

                if (signatureValid)
                {
                    logger.info("PGP/INLINE signature was valid; MailID: {}", mailID);

                    setHeader(PGP_SIGNATURE_VALID, "True", message);

                    if (signerMismatch)
                    {
                        setHeader(PGP_SIGNER_MISMATCH, "True", message);
                        setHeader(PGP_SIGNER_EMAIL, StringUtils.join(allSignerEmails, ", "), message);
                    }

                    if (addSecurityInfoToSubject) {
                        addSignerInfoToSubject(message, true, signerMismatch, allSignerEmails);
                    }
                }
                else {
                    logger.warn("PGP/INLINE signature was not valid; Failure message: {}; MailID: {}",
                            StringUtils.join(signatureFailureMessages, ", "), mailID);

                    setHeader(PGP_SIGNATURE_VALID, "False", message);
                    setHeader(PGP_SIGNATURE_FAILURE, StringUtils.join(signatureFailureMessages, ", "), message);

                    if (addSecurityInfoToSubject) {
                        addSignerInfoToSubject(message, false, false, allSignerEmails);
                    }
                }

                // Check if all parts were signed or whether only some parts were signed
                if (unsigned > 0) {
                    mixedContent.setValue(true);
                }

                if (removeSignature && !detachedSignaturePartsToRemove.isEmpty()) {
                    message = removeDetachedSignatures(message, detachedSignaturePartsToRemove);
                }
            }
        }
        catch (PartException | MessagingException | IOException e) {
            logger.error("Error post handling PGP/INLINE message");
        }

        return message;
    }

    public MimeMessage handleMessage(@Nonnull MimeMessage message)
    throws MessagingException, IOException
    {
        return handleMessage(message, new LinkedList<>());
    }

    public MimeMessage handleMessage(@Nonnull MimeMessage message, @Nonnull List<PGPMIMEHandler> handlers)
    throws MessagingException, IOException
    {
        MimeMessage result = null;

        int i = 0;

        boolean localInlinePGPEnabled = this.inlinePGPEnabled;
        boolean localEnablePGPUniversalWorkaround = this.enablePGPUniversalWorkaround;

        boolean done = false;

        do {
            PGPMIMEHandler handler = createPGPMIMEHandler(localInlinePGPEnabled);

            // Set PGP Universal workaround for the first layer if enabled
            handler.setEnablePGPUniversalWorkaround(localEnablePGPUniversalWorkaround);

            message = handler.handleMessage(message);

            if (handler.isDecryptionKeyNotFound()) {
                decryptionKeyNotFound = true;
            }

            // Should be disabled after handling the first layer since the PGP universal headers are copied to the
            // message after the message has been handled. After handling the first layer, the PGP universal headers
            // should not trigger the work around for the new part.
            localEnablePGPUniversalWorkaround = false;

            if (message != null && handler.isPGP())
            {
                result = message;

                handlers.add(handler);

                if (localInlinePGPEnabled && handler.getEncoding() == PGPEncoding.PGP_MIME) {
                    // Disable PGP/INLINE if the message is PGP/MIME. For optimization, we do not support mixed
                    // PGP/MIME and PGP/INLINE
                    localInlinePGPEnabled = false;
                }
            }

            done = (message == null) || !handler.isChanged();
        }
        while (!done && i++ < maxRecursion);

        if (i >= maxRecursion) {
            logger.warn("PGP max recursion reached");
        }

        if (result != null && !handlers.isEmpty())
        {
            PGPMIMEHandler lastHandler = handlers.get(handlers.size() - 1);

            setHeader(PGP_ENCODING, lastHandler.getEncoding().getFriendlyName(), result);

            if (lastHandler.getEncoding() == PGPEncoding.PGP_MIME)
            {
                // Check whether the message was encrypted
                checkPGPMIMEEncrypted(result, handlers);

                // Check whether the message was signed and if so check that the signature is valid
                validatePGPMIMESignature(result, lastHandler);
            }
            else if (lastHandler.getEncoding() == PGPEncoding.PGP_INLINE) {
                // Keep track if the PGP/INLINE message has mixed content
                MutableBoolean mixedContent = new MutableBoolean();

                // Check whether the message was encrypted
                checkPGPInlineEncrypted(result, handlers, mixedContent);

                // Check whether the message was signed and if so check that the signature is valid
                result = validatePGPInlineSignatures(result, handlers, mixedContent);

                if (mixedContent.booleanValue())
                {
                    if (addSecurityInfoToSubject) {
                        addTagToSubject(result, getMixedContentTag());
                    }

                    logger.warn("PGP/INLINE signed message contained mixed content; MailID: {}", mailID);

                    setHeader(PGP_MIXED_CONTENT, "True", result);
                }
            }
        }

        return result;
    }

    public boolean isInlinePGPEnabled() {
        return inlinePGPEnabled;
    }

    public void setInlinePGPEnabled(boolean inlinePGPEnabled) {
        this.inlinePGPEnabled = inlinePGPEnabled;
    }

    public boolean isSkipNonPGPExtensions() {
        return skipNonPGPExtensions;
    }

    public void setSkipNonPGPExtensions(boolean skipNonPGPExtensions) {
        this.skipNonPGPExtensions = skipNonPGPExtensions;
    }

    public boolean isScanHTMLForPGP() {
        return scanHTMLForPGP;
    }

    public void setScanHTMLForPGP(boolean scanHTMLForPGP) {
        this.scanHTMLForPGP = scanHTMLForPGP;
    }

    public boolean isDecrypt() {
        return decrypt;
    }

    public void setDecrypt(boolean decrypt) {
        this.decrypt = decrypt;
    }

    public boolean isRemoveSignature() {
        return removeSignature;
    }

    public void setRemoveSignature(boolean removeSignature) {
        this.removeSignature = removeSignature;
    }

    public boolean isRetainMessageID() {
        return retainMessageID;
    }

    public void setRetainMessageID(boolean retainMessageID) {
        this.retainMessageID = retainMessageID;
    }

    public boolean isEnablePGPUniversalWorkaround() {
        return enablePGPUniversalWorkaround;
    }

    public void setEnablePGPUniversalWorkaround(boolean enablePGPUniversalWorkaround) {
        this.enablePGPUniversalWorkaround = enablePGPUniversalWorkaround;
    }

    public int getDecompressionUpperlimit() {
        return decompressionUpperlimit;
    }

    public void setDecompressionUpperlimit(int decompressionUpperlimit) {
        this.decompressionUpperlimit = decompressionUpperlimit;
    }

    public int getMaxRecursion() {
        return maxRecursion;
    }

    public void setMaxRecursion(int maxRecursion) {
        this.maxRecursion = maxRecursion;
    }

    public int getMaxHeaderLength() {
        return maxHeaderLength;
    }

    public void setMaxHeaderLength(int maxHeaderLength) {
        this.maxHeaderLength = maxHeaderLength;
    }

    public boolean isAddSecurityInfoToSubject() {
        return addSecurityInfoToSubject;
    }

    public void setAddSecurityInfoToSubject(boolean addSecurityInfoToSubject) {
        this.addSecurityInfoToSubject = addSecurityInfoToSubject;
    }

    public String getSubjectTemplate() {
        return subjectTemplate;
    }

    public void setSubjectTemplate(String subjectTemplate) {
        this.subjectTemplate = subjectTemplate;
    }

    public String getDecryptedTag() {
        return decryptedTag;
    }

    public void setDecryptedTag(String decryptedTag) {
        this.decryptedTag = decryptedTag;
    }

    public String getSignedValidTag() {
        return signedValidTag;
    }

    public void setSignedValidTag(String signedValidTag) {
        this.signedValidTag = signedValidTag;
    }

    public String getSignedByValidTag() {
        return signedByValidTag;
    }

    public void setSignedByValidTag(String signedByValidTag) {
        this.signedByValidTag = signedByValidTag;
    }

    public String getSignedInvalidTag() {
        return signedInvalidTag;
    }

    public void setSignedInvalidTag(String signedInvalidTag) {
        this.signedInvalidTag = signedInvalidTag;
    }

    public String getMixedContentTag() {
        return mixedContentTag;
    }

    public void setMixedContentTag(String mixedContentTag) {
        this.mixedContentTag = mixedContentTag;
    }

    public int getMaxMimeDepth() {
        return maxMimeDepth;
    }

    public void setMaxMimeDepth(int maxMimeDepth) {
        this.maxMimeDepth = maxMimeDepth;
    }

    public int getMaxObjects() {
        return maxObjects;
    }

    public void setMaxObjects(int maxObjects) {
        this.maxObjects = maxObjects;
    }

    public boolean isDecrypted() {
        return decrypted;
    }

    public boolean isDecryptionKeyNotFound() {
        return decryptionKeyNotFound;
    }

    public PGPPublicKeyRingListener getPublicKeyRingListener() {
        return publicKeyRingListener;
    }

    public void setPublicKeyRingListener(PGPPublicKeyRingListener publicKeyRingListener) {
        this.publicKeyRingListener = publicKeyRingListener;
    }

    public String getMailID() {
        return mailID;
    }

    public void setMailID(String mailID) {
        this.mailID = mailID;
    }
}
