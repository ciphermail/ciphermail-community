/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Thrown when a policy is violated.
 * <p>
 * Note: PolicyViolationException is not thread safe
 */
public class PolicyViolationException extends Exception
{
    /*
     * List of embedded violation exceptions.
     */
    private final List<PolicyViolation> violations = new LinkedList<>();

    public PolicyViolationException(String message) {
        super(message);
    }

    /**
     * A violation message can be added to this PolicyViolationException. This will be used when multiple
     * policies are violated.
     */
    public void addViolation(@Nonnull PolicyViolation violation) {
        violations.add(violation);
    }

    /**
     * Violation messages can be added to this PolicyViolationException. This will be used when multiple
     * policies are violated.
     */
    public void addViolations(@Nonnull Collection<PolicyViolation> violations) {
        this.violations.addAll(violations);
    }

    /**
     * @Return all violated policies as an immutable list. Null if there are no embedded violations.
     */
    public List<PolicyViolation> getViolations() {
        return violations.isEmpty() ? null : Collections.unmodifiableList(violations);
    }
}
