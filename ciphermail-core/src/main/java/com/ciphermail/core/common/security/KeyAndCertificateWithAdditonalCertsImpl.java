/*
 * Copyright (c) 2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of KeyAndCertificateWithAdditonalCerts
 */
public class KeyAndCertificateWithAdditonalCertsImpl extends KeyAndCertificateImpl
    implements KeyAndCertificateWithAdditonalCerts
{
    /*
     * Additional certificates (for example intermediate certificates)
     */
    private List<X509Certificate> additionalCertificates;

    public KeyAndCertificateWithAdditonalCertsImpl(PrivateKey privateKey, X509Certificate certificate)
    {
        super(privateKey, certificate);

        setAdditionalCertificates(null);
    }

    public KeyAndCertificateWithAdditonalCertsImpl(PrivateKey privateKey, X509Certificate certificate,
            Collection<X509Certificate> additionalCertificates)
    {
        super(privateKey, certificate);

        setAdditionalCertificates(additionalCertificates);
    }

    private void setAdditionalCertificates(Collection<X509Certificate> additionalCertificates)
    {
        /*
         * Make a shallow copy
         */
        this.additionalCertificates = additionalCertificates != null ? new LinkedList<>(additionalCertificates) :
            new LinkedList<>();
    }

    @Override
    public Collection<X509Certificate> getAdditionalCertificates() {
        return additionalCertificates;
    }

    /*
     * Fallback to super class. The identity of the object is defined by the key and certificate and not whether or not
     * there are additional certificates.
     *
     * @see mitm.common.security.KeyAndCertificateImpl#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /*
     * Fallback to super class. The identity of the object is defined by the key and certificate and not whether or not
     * there are additional certificates.
     *
     * @see mitm.common.security.KeyAndCertificateImpl#equals(java.lang.Object)
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
