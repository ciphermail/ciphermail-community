/*
 * Copyright (c) 2021-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password.validator;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class NOutOfMRuleJSONParser
{
    private static final String MINIMUM_REQUIRED_JSON_PARAM = "m";
    private static final String RULES_JSON_PARAM = "r";
    private static final String WEIGHT_JSON_PARAM = "w";
    private static final String REGEX_JSON_PARAM = "p";

    private NOutOfMRuleJSONParser() {
        // empty on purpose
    }

    public static class WeightedRuleDef
    {
        private final int weight;
        private final String regex;

        WeightedRuleDef(int weight, @Nonnull String regex)
        {
            this.weight = weight;
            this.regex = Objects.requireNonNull(regex);
        }

        public int getWeight() {
            return weight;
        }

        public String getRegex() {
            return regex;
        }
    }

    public static class NOutOfMRuleDef
    {
        private final int minimumRequired;
        private final List<WeightedRuleDef> weightedRules;

        public NOutOfMRuleDef(int minimumRequired, @Nonnull List<WeightedRuleDef> weightedRules)
        {
            this.minimumRequired = minimumRequired;
            this.weightedRules = Objects.requireNonNull(weightedRules);
        }

        public int getMinimumRequired() {
            return minimumRequired;
        }

        public List<WeightedRuleDef> getWeightedRules() {
            return weightedRules;
        }
    }

    /**
     * Parses the provided json and returns the rule definition.
     * <p>
     * The JSON should be formed as follows:
     * <p>
     * {m: INT, r:[{w: INT, p:STRING}, {w: INT, p:STRING}, .... ]}
     * <p>
     * m and w are optional (if not set, default value will be 1). There should be one ore more "rules"
     * {w: INT, p:STRING}
     * <p>
     * Examples:
     * <p>
     * {m:10, r:[{w:1, p:'(?U)^(?=.*[0-9]).{8,}$'},{p:'(?U)^(?=.*[a-z]).{8,}$'}]}
     * {r:[{p:'(?U)^(?=.*[0-9]).{8,}$'}]}
     */
    public static NOutOfMRuleDef parseJSON(@Nonnull String jsonString)
    throws JSONException
    {
        try {
            jsonString = StringUtils.trimToNull(jsonString);

            if (jsonString == null) {
                throw new JSONException("the rule defintion cannot be empty");
            }

            JSONObject json;

            try {
                json = new JSONObject(jsonString);
            }
            catch (JSONException e) {
                throw new JSONException("the rule defintion should be a valid JSON string");
            }

            String value = StringUtils.trimToNull(json.optString(MINIMUM_REQUIRED_JSON_PARAM));

            int minimumRequired;

            try {
                minimumRequired = value != null ? Integer.parseInt(value) : 1;
            }
            catch (NumberFormatException e) {
                throw new JSONException("minimal required value (m) is not a valid integer");
            }

            Object jsonObject = json.opt(RULES_JSON_PARAM);

            if (!(jsonObject instanceof JSONArray rules)) {
                throw new JSONException("rule value (r) is not a JSON array");
            }

            if (rules.isEmpty()) {
                throw new JSONException("rule value (r) is empty");
            }

            List<WeightedRuleDef> weightedRules = new LinkedList<>();

            for (int i = 0; i < rules.length(); i++)
            {
                JSONObject ruleJSON = rules.getJSONObject(i);

                value = StringUtils.trimToNull(ruleJSON.optString(WEIGHT_JSON_PARAM));

                int weight;

                try {
                    weight = value != null ? Integer.parseInt(value) : 1;
                }
                catch (NumberFormatException e) {
                    throw new JSONException("weight value (w) is not a valid integer");
                }

                String regEx = StringUtils.trimToNull(ruleJSON.optString(REGEX_JSON_PARAM));

                if (regEx == null) {
                    throw new JSONException("patterns (p) are not defined");
                }

                // Validate regular expression
                try {
                    Pattern.compile(regEx);
                }
                catch (PatternSyntaxException e) {
                    throw new JSONException("pattern (p) is not a valid regular expression");
                }

                weightedRules.add(new WeightedRuleDef(weight, regEx));
            }

            return new NOutOfMRuleDef(minimumRequired, weightedRules);
        }
        catch (Exception e) {
            throw new JSONException(e);
        }
    }
}
