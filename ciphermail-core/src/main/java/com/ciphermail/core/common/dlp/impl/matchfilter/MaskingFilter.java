/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl.matchfilter;

import org.apache.commons.lang.StringUtils;

import com.ciphermail.core.common.dlp.MatchFilter;

import javax.annotation.Nonnull;

/**
 * Filter that replaces part of the input with * chars.
 *
 * @author Martijn Brinkers
 *
 */
public class MaskingFilter implements MatchFilter
{
    /*
     * Name of the filter
     */
    public static final String NAME = "Mask";

    /*
     * description of the filter
     */
    private String description = "Replaces the input with * chars";

    @Override
    public String filter(String input)
    {
        if (input != null) {
            input = StringUtils.repeat("*", input.length());
        }

        return input;
    }

    @Override
    public @Nonnull String getName() {
        return NAME;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public @Nonnull String getDescription() {
        return description;
    }
}
