/*
 * Copyright (c) 2009-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import javax.security.auth.x500.X500Principal;

public interface RequestParameters
{
    /**
     * The subject for the certificate
     */
    void setSubject(X500Principal subject);
    X500Principal getSubject();

    /**
     * Email will be added as an AltName
     */
    void setEmail(String email);
    String getEmail();

    /**
     * The number of days the certificate should be valid. Must be > 0.
     */
    void setValidity(int days);
    int getValidity();

    /**
     * The length of the RSA key in bytes (must >= 1024)
     */
    void setKeyLength(int length);
    int getKeyLength();

    /**
     * The signature algorithm used to sign the certificate
     */
    void setSignatureAlgorithm(String signatureAlgorithm);
    String getSignatureAlgorithm();

    /**
     * The CRL distribution point to add
     */
    void setCRLDistributionPoint(String crlDistributionPoint);
    String getCRLDistributionPoint();

    /**
     * The name of the certificate request handler that will handle this request
     */
    void setCertificateRequestHandler(String certificateRequestHandler);
    String getCertificateRequestHandler();
}
