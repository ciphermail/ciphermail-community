/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.ParseException;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class HeaderUtils
{
    private static final Logger logger = LoggerFactory.getLogger(HeaderUtils.class);

    private HeaderUtils() {
        // empty on purpose
    }

    /*
     * Helper class that will copy source headers when the headers match
     */
    private abstract static class HeaderCopier
    {
        public void copyHeaders(@Nonnull Enumeration<Header> sourceHeaders, @Nonnull HeaderMatcher matcher)
        throws MessagingException
        {
            // Javamail reverses Received and Return-Path headers. Because the headers are
            // already reversed in the source the Received and Return-Path headers are added
            // in the wrong order. We therefore need to add them reversed. Javamail knows
            // about the order of the Received and Return-Path headers so adding them
            // afterwards does not matter.
            List<Header> reversed = null;

            while (sourceHeaders.hasMoreElements())
            {
                Header header = sourceHeaders.nextElement();

                if (matcher.isMatch(header))
                {
                    String name = header.getName();

                    if ("Received".equalsIgnoreCase(name) || "Return-Path".equalsIgnoreCase(name))
                    {
                        if (reversed == null) {
                            reversed = new LinkedList<>();
                        }
                        // Add in reversed order
                        reversed.add(0, header);
                    }
                    else {
                        addHeader(name, header.getValue());
                    }
                }
            }

            if (reversed != null)
            {
                // Add the Received and Return-Path headers in reversed order
                for (Header header : reversed) {
                    addHeader(header.getName(), header.getValue());
                }
            }
        }

        public abstract void addHeader(String name, String value)
        throws MessagingException;
    }

    public static void copyHeaders(@Nonnull Enumeration<Header> sourceHeaders, @Nonnull Part destination,
            @Nonnull HeaderMatcher matcher)
    throws MessagingException
    {
        HeaderCopier headerCopier = new HeaderCopier()
        {
            @Override
            public void addHeader(String name, String value)
            throws MessagingException
            {
                destination.addHeader(name, value);
            }
        };

        headerCopier.copyHeaders(sourceHeaders, matcher);
    }

    public static void copyHeaders(@Nonnull Enumeration<Header> sourceHeaders, @Nonnull InternetHeaders destination,
            @Nonnull HeaderMatcher matcher)
    throws MessagingException
    {
        HeaderCopier headerCopier = new HeaderCopier()
        {
            @Override
            public void addHeader(String name, String value)
            throws MessagingException
            {
                destination.addHeader(name, value);
            }
        };

        headerCopier.copyHeaders(sourceHeaders, matcher);
    }

    @SuppressWarnings("unchecked")
    public static void copyHeaders(@Nonnull Part sourcePart, @Nonnull Part destination, @Nonnull HeaderMatcher matcher)
    throws MessagingException
    {
        copyHeaders(sourcePart.getAllHeaders(), destination, matcher);
    }

    @SuppressWarnings("unchecked")
    public static void removeHeaders(@Nonnull Part part, @Nonnull HeaderMatcher matcher)
    throws MessagingException
    {
        Enumeration<Header> headers = part.getAllHeaders();

        while (headers.hasMoreElements())
        {
            Header header = headers.nextElement();

            if (matcher.isMatch(header)) {
                part.removeHeader(header.getName());
            }
        }
    }

    /**
     * Returns an array of headers names that do not exist in the existingHeaders. The header names returned are
     * always lowercase and matching is case insensitive
     */
    public static String[] getNonMatchingHeaderNames(@Nonnull Enumeration<Header> existingHeaders,
            @Nonnull String... headersToMatch)
    {
        Set<String> set = new HashSet<>();

        for (String headerName : headersToMatch)
        {
            if (headerName != null)
            {
                set.add(headerName.toLowerCase());
            }
        }

        while (existingHeaders.hasMoreElements())
        {
            Header header = existingHeaders.nextElement();

            if (header.getName() != null)
            {
                String headerName = header.getName().toLowerCase();

                set.remove(headerName);
            }
        }

        String[] result = new String[set.size()];

        return set.toArray(result);
    }

    /**
     * Returns an array of headers names that do not exist in the existingHeaders. The header names returned are
     * always lowercase and matching is case-insensitive
     */
    public static String[] getNonMatchingHeaderNames(@Nonnull Enumeration<Header> existingHeaders,
            @Nonnull Enumeration<Header> headersToMatch)
    {
        Set<String> headerNames = new HashSet<>();

        while (headersToMatch.hasMoreElements())
        {
            Header header = headersToMatch.nextElement();

            if (header.getName() != null)
            {
                String headerName = header.getName().toLowerCase();

                headerNames.add(headerName);
            }
        }

        String[] result = new String[headerNames.size()];

        return getNonMatchingHeaderNames(existingHeaders, headerNames.toArray(result));
    }

    /**
     * Returns an array of headers names from headersToMatch that do exist in the existingHeaders. The header names returned are
     * always lowercase and matching is case insensitive
     */
    public static String[] getMatchingHeaderNames(@Nonnull Enumeration<Header> existingHeaders,
            @Nonnull String... headersToMatch)
    {
        List<String> matching = new LinkedList<>();

        Set<String> set = new HashSet<>();

        for (String headerName : headersToMatch)
        {
            if (headerName != null)
            {
                set.add(headerName.toLowerCase());
            }
        }

        while (existingHeaders.hasMoreElements())
        {
            Header header = existingHeaders.nextElement();

            if (header.getName() != null)
            {
                String headerName = header.getName().toLowerCase();

                if (set.contains(headerName)) {
                    matching.add(headerName);
                }
            }
        }

        String[] result = new String[matching.size()];

        return matching.toArray(result);
    }

    /**
     * Returns an array of headers names from headersToMatch that do exist in the existingHeaders. The header names returned are
     * always lowercase and matching is case-insensitive
     */
    public static String[] getMatchingHeaderNames(@Nonnull Enumeration<Header> existingHeaders,
            @Nonnull Enumeration<Header> headersToMatch)
    {
        Set<String> headerNames = new HashSet<>();

        while (headersToMatch.hasMoreElements())
        {
            Header header = headersToMatch.nextElement();

            if (header.getName() != null)
            {
                String headerName = header.getName().toLowerCase();

                headerNames.add(headerName);
            }
        }

        String[] result = new String[headerNames.size()];

        return getMatchingHeaderNames(existingHeaders, headerNames.toArray(result));
    }

    /**
     * Encodes the value so it only contains ASCII characters (RFC 2047).
     */
    public static String encodeHeaderQuietly(String value)
    {
        if (value == null) {
            return null;
        }

        String encoded;

        try {
        	encoded = MimeUtility.encodeText(value);
		}
        catch (UnsupportedEncodingException e) {
        	encoded = value;
		}

        return encoded;
    }

    /**
     * Encodes and folds the header value so it only contains ASCII characters (RFC 2047).
     *
     * @param headerName Headername is only used to calculate where to fold
     * @param headerValue The header value that need to be folded and converted to ASCII
     * @return the header value folded and RFC 2047 encoded
     * @throws UnsupportedEncodingException
     */
    public static String encodeHeaderValue(String headerName, String headerValue)
    throws UnsupportedEncodingException
    {
        if (headerValue == null) {
            return null;
        }

        int nameLength = (headerName == null ? 0 : headerName.length());

        return MimeUtility.fold(nameLength, MimeUtility.encodeText(headerValue));
    }

    /**
     * Unfolds and decodes the header value
     *
     * @param headerValue
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String decodeHeaderValue(String headerValue)
    throws UnsupportedEncodingException
    {
        if (headerValue == null) {
            return null;
        }

        return MimeUtility.decodeText(MimeUtility.unfold(headerValue));
    }

    /**
     * Adds the headerline at the top of the headers. The rationale for this method
     * is that Javamail has no direct way to add a header at the start of the headers.
     * <p>
     * Note 1: It's assume that the header line is already correctly encoded
     * Note 2: If saveMessage is called on the resuling message, the headers are resorted again
     */
    public static void prependHeaderLine(@Nonnull MimeMessage message, @Nonnull String... headerLines)
    throws MessagingException
    {
        List<String> original = new LinkedList<>();

        Enumeration<?> headerEnum = message.getAllHeaderLines();

        while (headerEnum.hasMoreElements()) {
            original.add((String) headerEnum.nextElement());
        }

        // remove all existing headers
        headerEnum = message.getAllHeaders();

        while (headerEnum.hasMoreElements())
        {
            Header header = (Header) headerEnum.nextElement();

            message.removeHeader(header.getName());
        }

        for (String headerLine : headerLines) {
            // add the new header line and then add the original headers
            message.addHeaderLine(headerLine);
        }

        for (String line : original) {
            message.addHeaderLine(line);
        }
    }

    /**
     * Returns all the header lines
     * @throws MessagingException
     */
    public static String[] getHeaderLines(MimeMessage message)
    throws MessagingException
    {
        if (message == null) {
            return null;
        }

        Enumeration<String> headers = HeaderExtractor.getAllHeaderLines(message);

        List<String> lines = new LinkedList<>();

        while (headers.hasMoreElements()) {
            lines.add(headers.nextElement());
        }

        return CollectionUtils.toStringArray(lines);
    }

    /**
     * Returns all the header lines
     * @throws MessagingException
     */
    public static String[] getHeaderLines(InternetHeaders headers)
    {
        if (headers == null) {
            return null;
        }

        Enumeration<String> headerLines = HeaderExtractor.getAllHeaderLines(headers);

        List<String> lines = new LinkedList<>();

        while (headerLines.hasMoreElements()) {
            lines.add(headerLines.nextElement());
        }

        return CollectionUtils.toStringArray(lines);
    }

    /**
     * Calls MimeUtility#decodeText for the input. When the input cannot be decoded the input will
     * be returned as-is (ie. no exception will be thrown).
     */
    public static String decodeTextQuietly(String input)
    {
        if (input == null) {
            return null;
        }

        String decoded;

        try {
            decoded = MimeUtility.decodeText(input);
        }
        catch (UnsupportedEncodingException e) {
            decoded = input;
        }

        return decoded;
    }

    /**
     * Calls MimeUtility#encodeText for the input. When the input cannot be encoded the input will
     * be returned as-is (ie. no exception will be thrown).
     */
    public static String encodeTextQuietly(String input, String charset)
    {
        if (input == null) {
            return null;
        }

        String encoded;

        try {
            encoded = MimeUtility.encodeText(input, charset, null);
        }
        catch (UnsupportedEncodingException e) {
            encoded = input;
        }

        return encoded;
    }

    /**
     * Skip headers. The input stream's current position is positioned at the start of the body. It is assumed that
     * the input uses CR/LF pairs. The headers are written to the headers output stream (skipped if set to null)
     */
    public static void skipHeaders(@Nonnull InputStream input, OutputStream headers)
    throws IOException
    {
        // A crude but fast way to detect the header body separator. We assume that
        // the input uses CR/LF pairs
        int newLineCount = 0;

        int i;

        try {
            while((i = input.read()) != -1)
            {
                if (headers != null) {
                    headers.write(i);
                }

                if (i == '\r' || i == '\n') {
                    newLineCount++;
                }
                else {
                    newLineCount = 0;
                }

                if (newLineCount == 4) {
                    break;
                }
            }
        }
        catch (EOFException ioe) {
            // ignore
        }
    }

    /**
     * Skip headers. The input stream's current position is positioned at the start of the body. It is assumed that
     * the input uses CR/LF pairs.
     */
    public static void skipHeaders(@Nonnull InputStream input)
    throws IOException
    {
        skipHeaders(input, null);
    }

    /**
     * Removes the angle brackets ("<" and ">") from the input.
     * <p>
     * Note: the input is not trim'd before checking whether the input is between brackets, i.e.,
     * <p>
     * removeAngleBrackets(" <test> ") does not remove the brackets
     */
    public static String removeAngleBrackets(String input)
    {
        if (input == null) {
            return null;
        }

        // if length < 2 the input cannot contain the brackets
        if (input.length() < 2) {
            return input;
        }

        String unquoted = input;

        if (unquoted.charAt(0) == '<' && unquoted.charAt(unquoted.length() - 1) == '>')
        {
            unquoted = unquoted.substring(1, unquoted.length() - 1);
        }

        return unquoted;
    }

    /**
     * Return the charset from the content type. If the content type does not contain the charset parameter,
     * or the charset cannot be read, the defaultCharset will be returned.
     */
    public static String getCharsetFromContentType(@Nonnull String contentTypeValue, String defaultCharset)
    {
        String charset = null;

        try {
            ContentType ct = new ContentType(contentTypeValue);

            charset = ct.getParameter("charset");
        }
        catch (ParseException e) {
            // ignore
        }

        if (charset == null) {
            charset = defaultCharset;
        }

        if (charset != null)
        {
            charset = MimeUtility.javaCharset(charset);

            if (!Charset.isSupported(charset))
            {
                logger.warn("Charset {} is not a supported charset.", charset);

                charset = defaultCharset;
            }
        }

        return charset;
    }

    /**
     * Return the parameter from the content type. If the content type does not contain the parameter, null
     * will be returned.
     */
    public static String getContentTypeParameter(@Nonnull String contentTypeValue, String parameter)
    throws ParseException
    {
        ContentType ct = new ContentType(contentTypeValue);

        return ct.getParameter(parameter);
    }

    /**
     * Sets the content type parameter to the given value.
     */
    public static String setContentTypeParameter(@Nonnull String contentTypeValue, String parameter, String value)
    throws ParseException
    {
        ContentType ct = new ContentType(contentTypeValue);

        ct.setParameter(parameter, value);

        return ct.toString();
    }

    /**
     * Validates if the given header name is valid according to RFC822
     *
     * @param headerName The header name to validate.
     * @return true if the header name is valid, false otherwise.
     */
    public static boolean isValidHeaderName(@Nonnull String headerName) {
        return headerName.matches("[a-zA-Z0-9_-]+");
    }
}
