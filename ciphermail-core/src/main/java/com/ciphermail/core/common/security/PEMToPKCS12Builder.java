/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.certificate.CertificateInspector;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.util.CollectionUtils;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Builder utility which can be used to convert PEM files to a PFX.
 * <p>
 * Only X509 certificates and Private keys are supported
 *
 * @author Martijn Brinkers
 *
 */
public class PEMToPKCS12Builder
{
    /*
     * Some static text we will encrypted to test whether a public key belongs to a private key
     */
    private static final byte[] PLAIN_TEXT = "plain text".getBytes();

    /*
     * The certificates that will be added to the PKCS12 stream
     */
    private final Set<X509Certificate> certificates = new HashSet<>();

    /*
     * The keys that will be added to the PKCS12 stream
     */
    private final Set<PrivateKey> keys = new HashSet<>();

    /*
     * Factory for security objects
     */
    private final SecurityFactory securityFactory;

    /*
     * For converting PEM objects to JCE objects
     */
    private final JcaPEMKeyConverter keyConverter;

    /*
     * For decrypting password protected PEM objects
     */
    private final JcePEMDecryptorProviderBuilder decryptorProviderBuilder;

    /*
     * For decrypting password protected PEM objects
     */
    private final JceOpenSSLPKCS8DecryptorProviderBuilder pKCS8DecryptorProviderBuilder;

    /*
     * For converting an X509CertificateHolder into an X509Certificate
     */
    private final JcaX509CertificateConverter certificateConverter;

    public PEMToPKCS12Builder()
    {
        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        keyConverter = new JcaPEMKeyConverter();

        // Since we are importing PEM, we will use the non-sensitive provider because any private material
        // will not be stored on HSM
        keyConverter.setProvider(securityFactory.getNonSensitiveProvider());

        decryptorProviderBuilder = new JcePEMDecryptorProviderBuilder();
        decryptorProviderBuilder.setProvider(securityFactory.getNonSensitiveProvider());

        pKCS8DecryptorProviderBuilder = new JceOpenSSLPKCS8DecryptorProviderBuilder();
        pKCS8DecryptorProviderBuilder.setProvider(securityFactory.getNonSensitiveProvider());

        certificateConverter = new JcaX509CertificateConverter();
        certificateConverter.setProvider(securityFactory.getNonSensitiveProvider());
    }

    /**
     * Adds PEM encoded certificate(s) or key(s) to be added to the PKCS12.
     *
     * @param pem      a {@link Reader} containing the PEM encoded certificate(s) or key(s).
     * @param password the password to decrypt the encrypted key(s).
     * @return the {@link PEMToPKCS12Builder} instance.
     * @throws IOException if an I/O error occurs.
     */
    public PEMToPKCS12Builder addPEM(String pem, char[] password)
    throws IOException
    {
        addPEM(new StringReader(pem), password);

        return this;
    }

    /**
     * Adds PEM encoded certificate(s) or key(s) to be added to the PKCS12.
     *
     * @param pem      a {@link Reader} containing the PEM encoded certificate(s) or key(s).
     * @param password the password to decrypt the encrypted key(s).
     * @return the {@link PEMToPKCS12Builder} instance.
     * @throws IOException if an I/O error occurs.
     */
    public PEMToPKCS12Builder addPEM(Reader pem, char[] password)
    throws IOException
    {
        PEMParser parser = new PEMParser(pem);

        Object object = null;

        while ((object = parser.readObject()) != null)
        {
            if (object instanceof X509CertificateHolder x509CertificateHolder)
            {
                try {
                    certificates.add(certificateConverter.getCertificate(x509CertificateHolder));
                }
                catch (CertificateException e) {
                    throw new IOException(e);
                }
            }
            else if (object instanceof PEMKeyPair pemKeyPair)
            {
                // We are only interested in private keys and not in "stand-alone" public keys
                keys.add(keyConverter.getKeyPair(pemKeyPair).getPrivate());
            }
            else if (object instanceof PrivateKeyInfo privateKeyInfo) {
                keys.add(keyConverter.getPrivateKey(privateKeyInfo));
            }
            else if (object instanceof PKCS8EncryptedPrivateKeyInfo pkcs8EncryptedPrivateKeyInfo)
            {
                try {
                    PrivateKeyInfo privateKeyInfo = (pkcs8EncryptedPrivateKeyInfo).decryptPrivateKeyInfo(
                            pKCS8DecryptorProviderBuilder.build(password));

                    if (privateKeyInfo != null) {
                        keys.add(keyConverter.getPrivateKey(privateKeyInfo));
                    }
                }
                catch (OperatorCreationException | PKCSException e) {
                    throw new IOException(e);
                }
            }
            else if (object instanceof PEMEncryptedKeyPair encryptedKeyPair)
            {
                PEMKeyPair keyPair = encryptedKeyPair.decryptKeyPair(decryptorProviderBuilder.build(password));

                if (keyPair != null)
                {
                    // We are only interested in private keys and not in "stand-alone" public keys
                    keys.add(keyConverter.getKeyPair(keyPair).getPrivate());
                }
            }
        }

        return this;
    }

    /**
     * Builds a PKCS12 KeyStore with the provided password.
     *
     * @param password the password to protect the KeyStore
     * @return the built PKCS12 KeyStore
     * @throws IOException              if an I/O error occurs
     * @throws KeyStoreException        if an error occurs while creating the KeyStore
     * @throws CertificateException     if an error occurs while working with certificates
     */
    public KeyStore buildPKCS12(char[] password)
    throws IOException, KeyStoreException, CertificateException
    {
        if (certificates.isEmpty()) {
            throw new IOException("There are no certificates");
        }

        List<KeyAndCertificate> keyAndCertificates = new LinkedList<>();

        // Step through all keys and try to find the corresponding certificate
        try {
            for (PrivateKey key : keys)
            {
                for (X509Certificate certificate : certificates)
                {
                    Cipher cipher = securityFactory.createCipher("RSA/None/PKCS1Padding");

                    try {
                        cipher.init(Cipher.ENCRYPT_MODE, key);
                    }
                    catch (InvalidKeyException e) {
                        // Ignore this key
                        break;
                    }

                    byte[] cipherText;

                    try {
                        cipherText = cipher.doFinal(PLAIN_TEXT);
                    }
                    catch (IllegalBlockSizeException | BadPaddingException e) {
                        // Ignore this key
                        break;
                    }

                    try {
                        cipher.init(Cipher.DECRYPT_MODE, certificate);

                        byte[] plainText = cipher.doFinal(cipherText);

                        if (Arrays.areEqual(PLAIN_TEXT, plainText)) {
                            // We have found the correct certificate
                            keyAndCertificates.add(new KeyAndCertificateImpl(key, certificate));

                            break;
                        }
                    }
                    catch (Exception e) {
                        // ignore this certificate
                    }
                }
            }

            KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

            keyStore.load(null);

            // Keep track of certificates that are not part of a chain. These will be added as certificate
            Set<X509Certificate> notInChain = new HashSet<>(certificates);

            // Now find the certificate chains for all the key/cert pairs
            for (KeyAndCertificate keyAndCertificate : keyAndCertificates)
            {
                List<X509Certificate> chain = CertificateUtils.findChain(keyAndCertificate.getCertificate(),
                        certificates);

                if (CollectionUtils.isNotEmpty(chain))
                {
                    chain.forEach(notInChain::remove);

                    keyStore.setKeyEntry(CertificateInspector.getThumbprint(keyAndCertificate.getCertificate()),
                            keyAndCertificate.getPrivateKey(), password,
                            CollectionUtils.toArray(chain, X509Certificate.class));
                }
            }

            // Now add all certificates that are not part of any chain as certificate entry
            for (X509Certificate certificate : notInChain) {
                keyStore.setCertificateEntry(CertificateInspector.getThumbprint(certificate), certificate);
            }

            return keyStore;
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e) {
            throw new IOException(e);
        }
    }

    public Set<PrivateKey> getKeys() {
        return keys;
    }

    public Set<X509Certificate> getCertificates() {
        return certificates;
    }
}
