/*
 * Copyright (c) 2016-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption pro.
 */
package com.ciphermail.core.common.security.ca.handlers.pkcs10;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.KeyAndCertificateImpl;
import com.ciphermail.core.common.security.KeyEncoder;
import com.ciphermail.core.common.security.KeyEncoderException;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.ca.CAException;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.CertificateRequestHandler;
import com.ciphermail.core.common.security.ca.EncodedRequestProvider;
import com.ciphermail.core.common.security.certificate.CertificateBuilderException;
import com.ciphermail.core.common.security.certificate.SerialNumberGenerator;
import com.ciphermail.core.common.security.certificate.X500PrincipalUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certificate.impl.StandardX509CertificateBuilder;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.common.security.password.PasswordProvider;
import org.apache.commons.lang.time.DateUtils;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.openssl.jcajce.JcaMiscPEMGenerator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.util.io.pem.PemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Objects;

/**
 * CertificateRequestHandler which generates a PKCS#10 CSR. The CSR can then be uploaded to a CA. When the certificate
 * is issued, the certificate can be imported (via the GUI)
 */
public class PKCS10CertificateRequestHandler implements CertificateRequestHandler, EncodedRequestProvider
{
    private static final Logger logger = LoggerFactory.getLogger(PKCS10CertificateRequestHandler.class);

    static final String NAME = "CSR";

    /*
     * The alias of keys stored in the key store will have an alias with this prefix
     */
    private static final String KEY_ALIAS_PREFIX = "PKCS10-CERTIFICATE-REQUEST-HANDLER:";

    /*
     * The message stored in the request when the request was successfully created
     */
    private static final String REQUEST_SUCCESS_MESSAGE = "PKCS#10 request generated. Waiting for certificate.";

    /*
     * Provides access to the key store which is used for storing the key pair of a certificate request
     */
    private final KeyStoreProvider keyStoreProvider;

    /*
     * Provides passwords for the keystore
     */
    private final PasswordProvider passwordProvider;

    /*
     * For generating serial numbers for a certificate
     */
    private final SerialNumberGenerator serialNumberGenerator;

    /*
     * Factory class for creating security object instances
     */
    private final SecurityFactory securityFactory;

    /*
     * The algorithm used for signing the PKCS#10 request
     */
    private String signingAlgorithm = "SHA256WithRSA";

    public PKCS10CertificateRequestHandler(
            @Nonnull KeyStoreProvider keyStoreProvider,
            @Nonnull PasswordProvider passwordProvider,
            @Nonnull SerialNumberGenerator serialNumberGenerator)
    {
        this.keyStoreProvider = Objects.requireNonNull(keyStoreProvider);
        this.passwordProvider = Objects.requireNonNull(passwordProvider);
        this.serialNumberGenerator = Objects.requireNonNull(serialNumberGenerator);

        this.securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isInstantlyIssued() {
        return false;
    }

    @Override
    public String getCertificateHandlerName() {
        return NAME;
    }

    private X509Certificate createSelfSignedCertificate(KeyPair keyPair, X500Principal subject)
    throws CertificateBuilderException, IOException
    {
        StandardX509CertificateBuilder certificateBuilder = new StandardX509CertificateBuilder(
                securityFactory.getSensitiveProvider(), securityFactory.getNonSensitiveProvider());

        certificateBuilder.setSubject(subject);
        certificateBuilder.setIssuer(subject);
        certificateBuilder.setPublicKey(keyPair.getPublic());
        certificateBuilder.setNotBefore(DateUtils.addDays(new Date(), -1));
        certificateBuilder.setNotAfter(DateUtils.addYears(new Date(), 32));
        certificateBuilder.setSerialNumber(serialNumberGenerator.generate());
        certificateBuilder.setSignatureAlgorithm(signingAlgorithm);

        return certificateBuilder.generateCertificate(keyPair.getPrivate(), null);
    }

    @Override
    public KeyAndCertificate handleRequest(@Nonnull CertificateRequest request)
    throws CAException
    {
        if (!NAME.equalsIgnoreCase(request.getCertificateHandlerName())) {
            // Should never happen
            throw new IllegalArgumentException("Handler mismatch. Expected " + NAME + " but got " +
                    request.getCertificateHandlerName());
        }

        if (request.getData() != null)
        {
            logger.debug("The PKCS#10 request was already created");

            // check whether the message in the request is still the default message. It can happen
            // that the certificate request handler was not available. An error message "A Certificate Request Handler
            // with name CSR (PKCS#10) was not found". We need to reset this message back to the successful message
            //
            // Note: in a normal setup this should not happen because this handler should always be available. The only
            // time this can happen is during development if you add a request and then later start the back-end
            // with the provider not enabled.
            if (!REQUEST_SUCCESS_MESSAGE.equals(request.getLastMessage())) {
                request.setLastMessage(REQUEST_SUCCESS_MESSAGE);
            }

            return null;
        }

        try {
            KeyPairGenerator keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");

            SecureRandom randomSource = securityFactory.createSecureRandom();

            keyPairGenerator.initialize(request.getKeyLength(), randomSource);

            KeyPair keyPair = keyPairGenerator.generateKeyPair();

            PKCS10CertificationRequestBuilder requestBuilder = new PKCS10CertificationRequestBuilder(
                    X500PrincipalUtils.toX500Name(request.getSubject()), SubjectPublicKeyInfo.getInstance(
                    keyPair.getPublic().getEncoded()));

            JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder(signingAlgorithm);

            contentSignerBuilder.setProvider(securityFactory.getSensitiveProvider());

            PKCS10CertificationRequest pkcs10 = requestBuilder.build(contentSignerBuilder.build(keyPair.getPrivate()));

            // store the private key in the key store because the key store might be an HSM. We might use
            // key wrapping but this might not be supported by all HSM providers or not supported in a general way
            // without having to rely on different wrapping/unwrapping methods for every HSM provider. The downside
            // of storing the key is that if the request is removed, we need to remove the key. Storing the key
            // inside the key store also requires a certificate. We will therefore generate a dummy certificate.
            KeyStore keyStore = keyStoreProvider.getKeyStore();

            X509Certificate certificate = createSelfSignedCertificate(keyPair, request.getSubject());

            String keyAlias = KEY_ALIAS_PREFIX + X509CertificateInspector.getThumbprint(certificate);

            keyStore.setKeyEntry(KEY_ALIAS_PREFIX + X509CertificateInspector.getThumbprint(certificate),
                    keyPair.getPrivate(), passwordProvider.getPassword(), new X509Certificate[]{certificate});

            logger.info("PKCS#10 with subject {} is generated for email {}. Key is stored in the keystore with "
                    + "alias {}", request.getSubject(), request.getEmail(), keyAlias);

            request.setLastMessage(REQUEST_SUCCESS_MESSAGE);

            StringWriter pemEncodedPKCS10Request = new StringWriter();

            try (PemWriter pemWriter = new PemWriter(pemEncodedPKCS10Request)) {
                pemWriter.writeObject(new JcaMiscPEMGenerator(pkcs10));
            }

            // store the data in the CertificateRequest
            request.setData(new DataWrapper(pemEncodedPKCS10Request.toString(), KeyEncoder.encode(keyPair.getPublic()),
                    keyAlias).encode());

            // Since #handleRequest never generates a KeyAndCertificate directly, we always return null
            return null;
        }
        catch (NoSuchProviderException | NoSuchAlgorithmException | OperatorCreationException | KeyStoreException |
                CertificateBuilderException | IOException | CertificateEncodingException | KeyEncoderException e)
        {
            throw new CAException(e);
        }
    }

    private DataWrapper getDataWrapper(CertificateRequest request)
    throws CAException
    {
        if (request == null) {
            return null;
        }

        byte[] data = request.getData();

        if (data == null) {
            return null;
        }

        try {
            return DataWrapper.decode(data);
        }
        catch (IOException e) {
            throw new CAException(e);
        }
    }

    /*
     * Checks if the uploaded certificate matches the request and if so the certificate and private key will be combined
     * and returned.
     */
    @Override
    public KeyAndCertificate handleRequest(CertificateRequest request, X509Certificate certificate)
    throws CAException
    {
        try {
            KeyAndCertificate keyAndCertificate = null;

            DataWrapper wrapper = getDataWrapper(request);

            if (wrapper != null)
            {
                // Check if the certificate belongs to this request
                PublicKey publicKey = (PublicKey) KeyEncoder.decode(wrapper.getEncodedPublicKey());

                if (certificate.getPublicKey().equals(publicKey))
                {
                    PrivateKey privateKey = (PrivateKey) keyStoreProvider.getKeyStore().getKey(wrapper.getKeyAlias(),
                            passwordProvider.getPassword());

                    if (privateKey != null) {
                        keyAndCertificate = new KeyAndCertificateImpl(privateKey, certificate);
                    }
                    else {
                        logger.warn("Key with alias {} not found", wrapper.getKeyAlias());
                    }
                }
            }

            return keyAndCertificate;
        }
        catch (IOException | UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException |
                KeyEncoderException e)
        {
            throw new CAException(e);
        }
    }

    /*
     * Returns the PEM encoded PKCS#10 request.
     */
    @Override
    public String getPemEncodedRequest(CertificateRequest request)
    throws CAException
    {
        DataWrapper wrapper = getDataWrapper(request);

        return  (wrapper != null && wrapper.getPemEncodedPKCS10Request() != null) ?
                wrapper.getPemEncodedPKCS10Request() : null;
    }

    @Override
    public void cleanup(CertificateRequest request)
    throws CAException
    {
        DataWrapper wrapper = getDataWrapper(request);

        if (wrapper != null)
        {
            // We need to remove the temporary key from the key store
            try {
                keyStoreProvider.getKeyStore().deleteEntry(wrapper.getKeyAlias());
            }
            catch (KeyStoreException e) {
                logger.error("Unable to delete key entry from key store with alias {}", wrapper.getKeyAlias());
            }
        }
    }

    public String getSigningAlgorithm() {
        return signingAlgorithm;
    }

    public void setSigningAlgorithm(String signingAlgorithm) {
        this.signingAlgorithm = signingAlgorithm;
    }

    /*
     * Will be used to persistently store data in the CertificateRequest
     */
    static class DataWrapper
    {
        /*
         * The generated PKCS#10 request in encoded form
         */
        private String pemEncodedPKCS10Request;

        /*
         * The generated public key of the request in encoded form
         */
        private byte[] encodedPublicKey;

        /*
         * The alias under which the private key is stored
         */
        private String keyAlias;

        DataWrapper() {
            // jackson requires a default constructor
        }

        DataWrapper(String pemEncodedPKCS10Request, byte[] encodedPublicKey, String keyAlias)
        {
            this.pemEncodedPKCS10Request = pemEncodedPKCS10Request;
            this.encodedPublicKey = encodedPublicKey;
            this.keyAlias = keyAlias;
        }

        public byte[] encode()
        throws IOException
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            JacksonUtil.getObjectMapper().writeValue(bos, this);

            return bos.toByteArray();
        }

        public static DataWrapper decode(byte[] encoded)
        throws IOException
        {
            return JacksonUtil.getObjectMapper().readValue(encoded, DataWrapper.class);
        }

        public String getPemEncodedPKCS10Request() {
            return pemEncodedPKCS10Request;
        }

        public DataWrapper setPemEncodedPKCS10Request(String pemEncodedPKCS10Request)
        {
            this.pemEncodedPKCS10Request = pemEncodedPKCS10Request;
            return this;
        }

        public byte[] getEncodedPublicKey() {
            return encodedPublicKey;
        }

        public DataWrapper setEncodedPublicKey(byte[] encodedPublicKey)
        {
            this.encodedPublicKey = encodedPublicKey;
            return this;
        }

        public String getKeyAlias() {
            return keyAlias;
        }

        public DataWrapper setKeyAlias(String keyAlias)
        {
            this.keyAlias = keyAlias;
            return this;
        }
    }
}
