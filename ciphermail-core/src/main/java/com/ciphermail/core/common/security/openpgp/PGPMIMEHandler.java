/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.HeaderExtractor;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.MimeMessageWithID;
import com.ciphermail.core.common.mail.MimeUtils;
import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.mail.PartScanner;
import com.ciphermail.core.common.mail.PartScanner.PartListener;
import com.ciphermail.core.common.mail.matcher.ContentHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.NotHeaderNameMatcher;
import com.ciphermail.core.common.util.MutableString;
import net.htmlparser.jericho.Source;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.bcpg.BCPGInputStream;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.annotation.Nonnull;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Handles PGP/INLINE and PGP/MIME. Only one layer is handled by this class (i.e., if a message is signed and then
 * encrypted, PGPMIMEHandler should be called two times).
 *
 * Note: This class of *not* thread safe
 *
 * @author Martijn Brinkers
 *
 */
public class PGPMIMEHandler
{
    private static final Logger logger = LoggerFactory.getLogger(PGPMIMEHandler.class);

    /*
     * Provides PGP key pair based on key id
     */
    private final PGPKeyPairProvider keyPairProvider;

    /*
     * Listener which is called when a PGP keyring is found
     */
    private PGPPublicKeyRingListener publicKeyRingListener;

    /*
     * If true, PGP inline is supported
     */
    private boolean inlinePGPEnabled = true;

    /*
     * If true, binary attachments, i.e., other than text/plain or text/html, that do not have a PGP type of extension
     * (.pgp, .asc etc.) will be skipped.
     */
    private boolean skipNonPGPExtensions = true;

    /*
     * If true, HTML parts are scanned for PGP by converting the HTML to text before scanning for PGP
     */
    private boolean scanHTMLForPGP = true;

    /*
     * maximum recursive depth for MIME parts.
     */
    private int maxMimeDepth = 8;

    /*
     * If true, and the MIME maxDepth is reached a TextExtractorException exception will be thrown.
     */
    private boolean exceptionOnMaxDepthReached = true;

    /*
     * The maximum number of bytes read for decompression. This is done to protect against "ZIP bombs".
     * The default value will be set to 10% of Runtime.getRuntime().maxMemory()
     */
    private int decompressionUpperlimit = (int) (Runtime.getRuntime().maxMemory() * 0.1);

    /*
     * The maximum number of PGP objects. If the number of PGP objects read exceeds the maximum, a PGP
     * exception will be thrown.
     */
    private int maxObjects = 64;

    /*
     * If true (the default), the PGP blob will be decrypted if it is encrypted and a suitable key is available
     */
    private boolean decrypt = true;

    /*
     * If true, any PGP signature will be removed
     */
    private boolean removeSignature = true;

    /*
     * If true the original Message-ID will be used for the handled message
     */
    private boolean retainMessageID = true;

    /*
     * If true, x-pgp-encoding-format header will be used (if available) to detect whether a message is PGP/MIME
     */
    private boolean enablePGPUniversalWorkaround;

    /*
     * Listener called for each MIME part in the message
     */
    private final PartListener partListener = new PartListenerImpl();

    /*
     * The encoding (PGP/INLINE, PGP/MIME) of the message
     */
    private PGPEncoding encoding;

    /*
     * True if the message contained pgp content
     */
    private boolean pgp;

    /*
     * True if the message was changed (decrypted, signature removed etc.)
     */
    private boolean changed;

    /*
     * True if a part was encrypted but the decryption key could not be found
     */
    private boolean decryptionKeyNotFound;

    /*
     * True if there is extra data other the empty lines before or after the PGP part
     */
    private boolean mixedContent;

    /*
     * All the PGPHandler's used per part during handling of the message
     */
    private final List<ArrayList<PGPHandler>> handlers = new LinkedList<>();

    /*
     * The original message which is handled
     */
    private MimeMessage sourceMessage;

    /*
     * Used by PartScanner
     */
    class PartListenerImpl implements PartScanner.PartListener
    {
        @Override
        public boolean onPart(Part parent, Part part, Object context)
        throws PartException
        {
            return PGPMIMEHandler.this.onInlinePart(part);
        }
    }

    public PGPMIMEHandler(@Nonnull PGPKeyPairProvider keyPairProvider) {
        this.keyPairProvider = Objects.requireNonNull(keyPairProvider);
    }

    /**
     * Handles the message. Returns a message if the message was changed because it contained some PGP content.
     * Returns null if the message was not changed.
     */
    public MimeMessage handleMessage(MimeMessage message)
    throws MessagingException, IOException
    {
        // Clear state
        sourceMessage = message;
        encoding = null;
        pgp = false;
        changed = false;
        mixedContent = false;
        handlers.clear();

        PGPMIMEType type = PGPMIMEUtils.getPGPMIMEType(message, enablePGPUniversalWorkaround);

        MimeMessage result = null;

        if (type == null) {
            // The mail is not PGP/MIME. Could be PGP inline though.
            //
            // Note: the message will be cloned because the message might be changed "in-place"
            if (inlinePGPEnabled)
            {
                result = retainMessageID ? MailUtils.cloneMessageWithFixedMessageID(message) :
                    MailUtils.cloneMessage(message);

                handlePGPInline(result);

                if (changed) {
                    result.saveChanges();
                }

                if (pgp) {
                    encoding = PGPEncoding.PGP_INLINE;
                }
            }
        }
        else if (type == PGPMIMEType.ENCRYPTED)
        {
            encoding = PGPEncoding.PGP_MIME;

            result = handlePGPMIMEEncrypted(message);
        }
        else if (type == PGPMIMEType.SIGNED)
        {
            encoding = PGPEncoding.PGP_MIME;

            result = handlePGPMIMESigned(message);
        }

        return result;
    }

    private PGPHandler createPGPHandler()
    {
        PGPHandler handler = new PGPHandler(keyPairProvider);

        handler.setDecrypt(decrypt);
        handler.setDecompressionUpperlimit(decompressionUpperlimit);
        handler.setMaxObjects(maxObjects);
        handler.setPublicKeyRingListener(publicKeyRingListener);

        return handler;
    }

    private PGPInlineHandler createPGPInlineHandler()
    {
        PGPInlineHandler handler = new PGPInlineHandler(keyPairProvider);

        handler.setDecrypt(decrypt);
        handler.setDecompressionUpperlimit(decompressionUpperlimit);
        handler.setMaxObjects(maxObjects);
        handler.setRemoveSignature(removeSignature);

        return handler;
    }

    private void handlePGPInline(MimeMessage message)
    throws MessagingException
    {
        try {
            PartScanner partScanner = new PartScanner(partListener, maxMimeDepth);

            partScanner.setExceptionOnMaxDepthReached(exceptionOnMaxDepthReached);

            partScanner.scanPart(message);
        }
        catch (PartException | IOException e) {
            throw new MessagingException("Error handling part", e);
        }
    }

    private boolean isScanBinaryPart(Part part)
    {
        boolean scan = false;

        if (skipNonPGPExtensions)
        {
            String filename = HeaderUtils.decodeTextQuietly(MimeUtils.getFilenameQuietly(part));

            if (filename != null && (PGPUtils.isPGPFilenameExtension(filename))) {
                scan = true;
            }
        }
        else {
            scan = true;
        }

        return scan;
    }

    /*
     * Is called for every MIME part of the message
     */
    private boolean onInlinePart(Part part)
    throws PartException
    {
        try {
            // Part should normally never by a multipart since PartScanner does not call this for multipart (
            // unless exceptionOnMaxDepthReached is false). Skip if it is a multipart
            if (!part.isMimeType("multipart/*"))
            {
                handlers.add(new ArrayList<>());

                if (part.isMimeType("text/plain")) {
                    handleInlineTextPart(part);
                }
                else if (scanHTMLForPGP && part.isMimeType("text/html")) {
                    handleInlineHTMLPart(part);
                }
                else {
                    if (isScanBinaryPart(part)) {
                        handleInlineBinaryPart(part);
                    }
                }
            }
            return true;
        }
        catch (MessagingException e) {
            throw new PartException(e);
        }
    }

    private void handleInlineBinaryPart(Part part)
    {
        try {
            PGPHandler handler = createPGPHandler();

            handlers.get(handlers.size() - 1).add(handler);

            ByteArrayOutputStream mime = new ByteArrayOutputStream();

            handler.handle(part.getInputStream(), mime);

            // Check if any content was written
            if (mime.size() > 0)
            {
                // Check if there is a filename in the literal packet
                //
                // See http://binblog.info/2008/03/12/know-your-pgp-implementation/ for some info
                // and http://www.imc.org/ietf-openpgp/mail-archive/msg05251.html for more info
                String filename = null;

                List<PGPLayer> pgpLayers = handler.getPGPLayers();

                if (CollectionUtils.isNotEmpty(pgpLayers))
                {
                    // Check if the last packet is a literal packet with a filename
                    PGPLayer lastLayer = pgpLayers.get(pgpLayers.size() - 1);

                    List<PGPLayerPart> layerParts = lastLayer.getParts();

                    if (CollectionUtils.isNotEmpty(layerParts))
                    {
                        PGPLayerPart layerPart = layerParts.get(0);

                        if (layerPart instanceof PGPLiteralLayerPart pgpLiteralLayerPart) {
                            filename = pgpLiteralLayerPart.getPGPLiteralData().getFileName();
                        }
                    }
                }

                String contentType = part.getContentType();
                String contentTypeName = HeaderUtils.getContentTypeParameter(contentType, "name");

                if (StringUtils.isEmpty(filename))
                {
                    // No filename from literal packet so use the headers
                    filename = HeaderUtils.decodeTextQuietly(MimeUtils.getFilenameQuietly(part));

                    // No filename, use name parameter
                    if (StringUtils.isEmpty(filename) && (StringUtils.isNotEmpty(contentTypeName))) {
                            filename = contentTypeName;

                    }

                    // We need to remove any possible .pgp extension from the filename
                    filename = PGPUtils.removePGPFilenameExtension(filename);
                }

                if (StringUtils.isNotEmpty(filename))
                {
                    // Set filename but only if original has content-disposition header set
                    String disposition = part.getDisposition();

                    if (disposition != null)
                    {
                        // The content-disposition header might contain filenames encoded with filename*0*, filename*1*
                        // etc. To make sure we have a "clean" content-disposition we will remove the original
                        // first before setting a new one.
                        //
                        // Note: Setting the disposition without deleting it first will not remove anny additional
                        // parameters (like the filename*0* parameters) so we need to set it to null first
                        part.setDisposition(null);
                        part.setDisposition(disposition);

                        // Now set the filename (this will also set the filename in the disposition)
                        part.setFileName(HeaderUtils.encodeTextQuietly(filename, CharEncoding.UTF_8));
                    }

                    if (StringUtils.isNotEmpty(contentTypeName)) {
                        // There was a content-type name header so set name to the filename
                        contentType = HeaderUtils.setContentTypeParameter(contentType, "name", filename);
                    }
                }

                part.setDataHandler(new DataHandler(new ByteArrayDataSource(mime.toByteArray(), contentType)));

                changed = true;
                pgp = true;
            }
            else {
                // No content. It might be a detached signature. Store the filename of the part in the PGPHandler so
                // we can lookup the part later
                handler.setFilename(HeaderUtils.decodeTextQuietly(MimeUtils.getFilenameQuietly(part)));
            }
        }
        catch (PGPDecryptionDisabledException e)
        {
            logger.debug("Message is PGP encrypted but decryption is disabled; Recipient Key IDs: [{}]; " +
                    "Message-ID: {}", StringUtils.join(e.getKeyIDs(), ", "),
                    MailUtils.getMessageIDQuietly(sourceMessage));
        }
        catch (PGPDecryptionKeyNotFoundException e)
        {
            logger.warn("PGP decryption key not found; Recipient Key IDs: [{}]; Message-ID: {}",
                    StringUtils.join(e.getKeyIDs(), ", "), MailUtils.getMessageIDQuietly(sourceMessage));

            decryptionKeyNotFound = true;
        }
        catch (Exception e) {
            // PGPHandler#handle throws an exception when the part is not a PGP part. We therefore will log
            // only if debug is enabled.
            logger.debug("Error handling inline binary part. This is probably not a PGP part.", e);
        }
    }

    private void handleInlineTextPart(Part part)
    {
        try {
            PGPInlineHandler handler = createPGPInlineHandler();

            ByteArrayOutputStream mime = new ByteArrayOutputStream();

            String charsetFromContentType = HeaderUtils.getCharsetFromContentType(part.getContentType(), null);

            MutableString charset = new MutableString(charsetFromContentType != null ?
                    charsetFromContentType : CharEncoding.UTF_8);

            boolean isPGP = handler.handle(part.getInputStream(), charset, mime);

            if (isPGP) {
                // The message contained PGP content. Since there can be multiple "BEGIN PGP MESSAGE" blocks in a part,
                // there can be multiple handlers.
                handlers.get(handlers.size() - 1).addAll(handler.getHandlers());

                if (!mixedContent) {
                    mixedContent = handler.isMixedContent();
                }

                // Only use new MIME if new MIME content was created
                if (handler.isChanged())
                {
                    part.setDataHandler(new DataHandler(new ByteArrayDataSource(mime.toByteArray(),
                           "text/plain; charset=" + MimeUtils.fromJavaCharsetToMimeCharset(charset.getValue()))));

                    changed = true;
                }

                pgp = true;
            }
        }
        catch (PGPDecryptionDisabledException e)
        {
            logger.debug("Message is PGP encrypted but decryption is disabled; Recipient Key IDs: [{}]; " +
                    "Message-ID: {}", StringUtils.join(e.getKeyIDs(), ", "),
                    MailUtils.getMessageIDQuietly(sourceMessage));
        }
        catch (PGPDecryptionKeyNotFoundException e)
        {
            logger.warn("PGP decryption key not found; Recipient Key IDs: [{}]; Message-ID: {}",
                    StringUtils.join(e.getKeyIDs(), ", "), MailUtils.getMessageIDQuietly(sourceMessage));

            decryptionKeyNotFound = true;
        }
        catch (Exception e) {
            logger.error("Error handling inline text part.", e);
        }
    }

    private void handleInlineHTMLPart(Part part)
    {
        try {
            PGPInlineHandler handler = createPGPInlineHandler();

            ByteArrayOutputStream mime = new ByteArrayOutputStream();

            String charsetFromContentType = HeaderUtils.getCharsetFromContentType(part.getContentType(), null);

            MutableString charset = new MutableString(charsetFromContentType != null ?
                    charsetFromContentType : CharEncoding.UTF_8);

            // Convert HTML to text. For example PGP desktop (now Symantec Desktop Encryption) and Enigmail (when HTML
            // is enabled) will embed PGP into the HTML part. The embedded HTML however seems to be text only strangely
            // enough
            Source htmlSource = new Source(new InputStreamReader(part.getInputStream(), charset.getValue()));

            boolean isPGP = handler.handle(IOUtils.toInputStream(htmlSource.getRenderer().toString(),
                    StandardCharsets.UTF_8), charset, mime);

            if (isPGP) {
                // The message contained PGP content. Since there can be multiple "BEGIN PGP MESSAGE" blocks in a part,
                // there can be multiple handlers.
                handlers.get(handlers.size() - 1).addAll(handler.getHandlers());

                if (!mixedContent) {
                    mixedContent = handler.isMixedContent();
                }

                // Only use new MIME if new MIME content was created
                if (handler.isChanged())
                {
                    part.setDataHandler(new DataHandler(new ByteArrayDataSource(mime.toByteArray(),
                           "text/plain; charset=" + MimeUtils.fromJavaCharsetToMimeCharset(charset.getValue()))));

                    changed = true;
                }

                pgp = true;
            }
        }
        catch (PGPDecryptionDisabledException e)
        {
            logger.debug("Message is PGP encrypted but decryption is disabled; Recipient Key IDs: [{}]; " +
                    "Message-ID: {}", StringUtils.join(e.getKeyIDs(), ", "),
                    MailUtils.getMessageIDQuietly(sourceMessage));
        }
        catch (PGPDecryptionKeyNotFoundException e)
        {
            logger.warn("PGP decryption key not found; Recipient Key IDs: [{}]; Message-ID: {}",
                    StringUtils.join(e.getKeyIDs(), ", "), MailUtils.getMessageIDQuietly(sourceMessage));

            decryptionKeyNotFound = true;
        }
        catch (Exception e) {
            logger.error("Error handling inline HTML part.", e);
        }
    }

    private MimeMessage createNewMessageFromMIME(MimeMessage source, byte[] mime)
    throws MessagingException
    {
        MimeMessage newMessage = retainMessageID ?  new MimeMessageWithID(MailSession.getDefaultSession(),
                new ByteArrayInputStream(mime), source.getMessageID()) :
                new MimeMessage(MailSession.getDefaultSession(), new ByteArrayInputStream(mime));

        // copy all non-content headers from the source message to new decrypted mime. We do
        // not want the content-type to be the first header so we need some "magic" to make sure that the
        // original headers are read before the content-type headers
        HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(new ContentHeaderNameMatcher());

        InternetHeaders headers = new InternetHeaders();

        // Copy the non-content-* headers from source message
        HeaderUtils.copyHeaders(HeaderExtractor.getAllHeaders(source), headers, nonContentMatcher);

        HeaderUtils.prependHeaderLine(newMessage, HeaderUtils.getHeaderLines(headers));

        newMessage.saveChanges();

        return newMessage;
    }

    private MimeMessage handlePGPMIMEEncrypted(MimeMessage message)
    throws MessagingException, IOException
    {
        MimeMessage newMessage = null;

        try {
            // There should be two body parts, one "application/pgp-encrypted" and one "application/octet-stream"
            //
            // See RFC 3156 section 4
            Multipart mp = (Multipart) message.getContent();

            Part encryptedPart = null;

            for (int i = 0; i < mp.getCount(); i++)
            {
                encryptedPart = mp.getBodyPart(i);

                if (encryptedPart.isMimeType("application/octet-stream")) {
                    break;
                }

                encryptedPart = null;
            }

            if (encryptedPart == null) {
                throw new MessagingException("application/octet-stream not found in PGP/MIME encrypted part.");
            }

            PGPHandler handler = createPGPHandler();

            handlers.add(new ArrayList<>());
            handlers.get(handlers.size() - 1).add(handler);

            ByteArrayOutputStream decrypted = new ByteArrayOutputStream();

            try {
                handler.handle(encryptedPart.getInputStream(), decrypted);

                // Check if any content was written
                if (decrypted.size() > 0)
                {
                    newMessage = createNewMessageFromMIME(message, decrypted.toByteArray());

                    changed = true;
                }

                pgp = true;
            }
            catch (PGPDecryptionDisabledException e)
            {
                logger.debug("Message is PGP encrypted but decryption is disabled; Recipient Key IDs: [{}]; " +
                		"Message-ID: {}", StringUtils.join(e.getKeyIDs(), ", "),
                		MailUtils.getMessageIDQuietly(sourceMessage));
            }
            catch (PGPDecryptionKeyNotFoundException e)
            {
                logger.warn("PGP decryption key not found; Recipient Key IDs: [{}]; Message-ID: {}",
                        StringUtils.join(e.getKeyIDs(), ", "), MailUtils.getMessageIDQuietly(sourceMessage));

                decryptionKeyNotFound = true;
            }

            return newMessage;
        }
        catch (PGPException e) {
            throw new IOException(e);
        }
    }

    private MimeMessage handlePGPMIMESigned(MimeMessage message)
    throws MessagingException, IOException
    {
        Multipart mp = (Multipart) message.getContent();

        // Extract the signed content and the signature
        BodyPart[] parts = PGPMIMEUtils.dissectPGPMIMESigned(mp);

        ByteArrayOutputStream signature = new ByteArrayOutputStream();

        parts[1].writeTo(signature);

        PGPHandler handler = createPGPHandler();

        handlers.add(new ArrayList<>());
        handlers.get(handlers.size() - 1).add(handler);

        // Since this is a detached signature, there should be no literal content layer, only a signature layer.
        // We will therefore need to add a literal layer so we use the same validation method for encrypted/signed
        // content.
        PGPLiteralDataGenerator generator = new PGPLiteralDataGenerator();

        ByteArrayOutputStream literalDataPacket = new ByteArrayOutputStream();

        ByteArrayOutputStream mimeStream = new ByteArrayOutputStream();

        parts[0].writeTo(mimeStream);

        byte[] mime = mimeStream.toByteArray();

        OutputStream output = generator.open(literalDataPacket, PGPLiteralData.TEXT, "", mime.length, new Date());

        IOUtils.write(mime, output);

        PGPLiteralData literalData = new PGPLiteralData(new BCPGInputStream(new ByteArrayInputStream(
                literalDataPacket.toByteArray())));

        try {
            handler.handle(new ByteArrayInputStream(signature.toByteArray()), NullOutputStream.INSTANCE);
        }
        catch (PGPException e) {
            throw new MessagingException("Error handling PGP/MIME signature", e);
        }

        List<PGPLayer> layers = handler.getPGPLayers();

        // Insert the literal layer part to the first layer as the first part.
        //
        // Note: normally there should only be one layer with one part (the signature part)
        if (CollectionUtils.isNotEmpty(layers)) {
            layers.get(0).getParts().add(0, new PGPLiteralLayerPartImpl(null, literalData, mimeStream));
        }

        pgp = true;

        MimeMessage result;

        if (removeSignature)
        {
            result = createNewMessageFromMIME(message, mime);

            changed = true;
        }
        else {
            result = retainMessageID ? MailUtils.cloneMessageWithFixedMessageID(message) :
                    MailUtils.cloneMessage(message);
        }

        return result;
    }

    public boolean isInlinePGPEnabled() {
        return inlinePGPEnabled;
    }

    public void setInlinePGPEnabled(boolean inlinePGPEnabled) {
        this.inlinePGPEnabled = inlinePGPEnabled;
    }

    public boolean isSkipNonPGPExtensions() {
        return skipNonPGPExtensions;
    }

    public void setSkipNonPGPExtensions(boolean skipNonPGPExtensions) {
        this.skipNonPGPExtensions = skipNonPGPExtensions;
    }

    public boolean isScanHTMLForPGP() {
        return scanHTMLForPGP;
    }

    public void setScanHTMLForPGP(boolean scanHTMLForPGP) {
        this.scanHTMLForPGP = scanHTMLForPGP;
    }

    public int getMaxMimeDepth() {
        return maxMimeDepth;
    }

    public void setMaxMimeDepth(int maxMimeDepth) {
        this.maxMimeDepth = maxMimeDepth;
    }

    public boolean isExceptionOnMaxDepthReached() {
        return exceptionOnMaxDepthReached;
    }

    public void setExceptionOnMaxDepthReached(boolean exceptionOnMaxDepthReached) {
        this.exceptionOnMaxDepthReached = exceptionOnMaxDepthReached;
    }

    public int getDecompressionUpperlimit() {
        return decompressionUpperlimit;
    }

    public void setDecompressionUpperlimit(int decompressionUpperlimit) {
        this.decompressionUpperlimit = decompressionUpperlimit;
    }

    public int getMaxObjects() {
        return maxObjects;
    }

    public void setMaxObjects(int maxObjects) {
        this.maxObjects = maxObjects;
    }

    public PGPEncoding getEncoding() {
        return encoding;
    }

    public List<ArrayList<PGPHandler>> getHandlers() {
        return handlers;
    }

    public boolean isDecrypt() {
        return decrypt;
    }

    public void setDecrypt(boolean decrypt) {
        this.decrypt = decrypt;
    }

    public boolean isRemoveSignature() {
        return removeSignature;
    }

    public void setRemoveSignature(boolean removeSignature) {
        this.removeSignature = removeSignature;
    }

    public boolean isRetainMessageID() {
        return retainMessageID;
    }

    public void setRetainMessageID(boolean retainMessageID) {
        this.retainMessageID = retainMessageID;
    }

    public boolean isEnablePGPUniversalWorkaround() {
        return enablePGPUniversalWorkaround;
    }

    public void setEnablePGPUniversalWorkaround(boolean enablePGPUniversalWorkaround) {
        this.enablePGPUniversalWorkaround = enablePGPUniversalWorkaround;
    }

    public boolean isPGP() {
        return pgp;
    }

    public boolean isChanged() {
        return changed;
    }

    public boolean isDecryptionKeyNotFound() {
        return decryptionKeyNotFound;
    }

    public boolean isMixedContent() {
        return mixedContent;
    }

    public PGPPublicKeyRingListener getPublicKeyRingListener() {
        return publicKeyRingListener;
    }

    public void setPublicKeyRingListener(PGPPublicKeyRingListener publicKeyRingListener) {
        this.publicKeyRingListener = publicKeyRingListener;
    }
}
