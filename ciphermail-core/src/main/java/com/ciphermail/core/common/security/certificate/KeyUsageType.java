/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import java.util.Set;

import org.bouncycastle.asn1.x509.KeyUsage;

public enum KeyUsageType
{
    /*
     * See http://www.ietf.org/rfc/rfc3280.txt
     *
     * KeyUsage ::= BIT STRING {
     *      digitalSignature        (0),
     *      nonRepudiation          (1),
     *      keyEncipherment         (2),
     *      dataEncipherment        (3),
     *      keyAgreement            (4),
     *      keyCertSign             (5),
     *      cRLSign                 (6),
     *      encipherOnly            (7),
     *      decipherOnly            (8) }
    */
    DIGITALSIGNATURE (0, KeyUsage.digitalSignature, "digitalSignature"),
    NONREPUDIATION   (1, KeyUsage.nonRepudiation,   "nonRepudiation"),
    KEYENCIPHERMENT  (2, KeyUsage.keyEncipherment,  "keyEncipherment"),
    DATAENCIPHERMENT (3, KeyUsage.dataEncipherment, "dataEncipherment"),
    KEYAGREEMENT     (4, KeyUsage.keyAgreement,     "keyAgreement"),
    KEYCERTSIGN      (5, KeyUsage.keyCertSign,      "keyCertSign"),
    CRLSIGN          (6, KeyUsage.cRLSign,          "CRLSign"),
    ENCIPHERONLY     (7, KeyUsage.encipherOnly,     "encipherOnly"),
    DECIPHERONLY     (8, KeyUsage.decipherOnly,     "decipherOnly");

    private final int tag;
    private final int bitValue;
    private final String friendlyName;

    KeyUsageType(int tag, int bitValue, String friendlyName)
    {
        this.tag = tag;
        this.bitValue = bitValue;
        this.friendlyName = friendlyName;
    }

    /**
     * Returns the tag value of the key usage flag
     * @return
     */
    public int getTag() {
        return tag;
    }

    /**
     * Returns the bit value of the key usage flag
     * @return
     */
    public int getBitValue() {
        return bitValue;
    }

    public static KeyUsageType fromTag(int tag)
    {
        for (KeyUsageType keyUsage : KeyUsageType.values())
        {
            if (keyUsage.getTag() == tag) {
                return keyUsage;
            }
        }

        return null;
    }

    /**
     * Returns a boolean array with the correct entries set which can be used for setKeyUsage(boolean[])
     */
    public static boolean[] getKeyUsageArray(Set<KeyUsageType> usages)
    {
        boolean[] usageArray = new boolean[DECIPHERONLY.ordinal() + 1];

        for (KeyUsageType type : usages) {
            usageArray[type.ordinal()] = true;
        }

        return usageArray;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
