/*
 * Copyright (c) 2010-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.tools;

import com.ciphermail.core.common.security.certificate.CertificateInspector;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.UUID;


/**
 * Tool which can be used to manage PKCS#12 files
 *
 * @author Martijn Brinkers
 *
 */
@SuppressWarnings({"java:S106", "java:S112"})
public class P12Tool
{
    private static final String COMMAND_NAME = P12Tool.class.getName();

    private String inPassword;
    private String destPassword;
    private String caPassword;
    private String inFile;
    private String destFile;
    private String caFile;
    private String keyAlias;
    private int daysValid;
    private boolean retainAliases;

    /*
     * Provides access to the command line options
     */
    private CommandLine commandLine;

    /*
     * The CLI option for showing help
     */
    private static final String HELP_OPTION_SHORT = "h";
    private static final String HELP_OPTION_LONG = "help";


    private static final String IN_PASSWORD_OPTION = "in-password";
    private static final String OUT_PASSWORD_OPTION = "out-password";
    private static final String CA_PASSWORD_OPTION = "ca-password";
    private static final String IN_FILE_OPTION = "in-file";
    private static final String OUT_FILE_OPTION = "out-file";
    private static final String CA_FILE_OPTION = "ca-file";
    private static final String KEY_ALIAS_OPTION = "key-alias";
    private static final String MERGE_OPTION = "merge";
    private static final String LIST_OPTION = "list";
    private static final String RENEW_OPTION = "renew";
    private static final String RETAIN_ALIASES_OPTION = "retain-aliases";
    private static final String DAYS_VALID_OPTION = "days-valid";

    private Options createCommandLineOptions()
    {
        Options options = new Options();

        options.addOption(Option.builder(HELP_OPTION_SHORT).longOpt(HELP_OPTION_LONG).desc("Show help").build());

        options.addOption(Option.builder().longOpt(IN_PASSWORD_OPTION).argName("password").hasArg().
                desc("Password for the input pkcs#12 file").build());

        options.addOption(Option.builder().longOpt(OUT_PASSWORD_OPTION).argName("password").hasArg().
                desc("Password for the destination pkcs#12 file").build());

        options.addOption(Option.builder().longOpt(CA_PASSWORD_OPTION).argName("password").hasArg().
                desc("Password for the CA pkcs#12 file").build());

        options.addOption(Option.builder().longOpt(IN_FILE_OPTION).argName("file").hasArg().
                desc("pkcs#12 input file").build());

        options.addOption(Option.builder().longOpt(OUT_FILE_OPTION).argName("file").hasArg().
                desc("pkcs#12 output file").build());

        options.addOption(Option.builder().longOpt(CA_FILE_OPTION).argName("file").hasArg().
                desc("pkcs#12 CA file").build());

        options.addOption(Option.builder().longOpt(KEY_ALIAS_OPTION).argName("alias").hasArg().
                desc("The key alias").build());

        options.addOption(Option.builder().longOpt(MERGE_OPTION).
                desc("Merges the input pkcs#12 with the output pkcs#12 file").build());

        options.addOption(Option.builder().longOpt(LIST_OPTION).
                desc("Lists all items from the input pkcs#12 file").build());

        options.addOption(Option.builder().longOpt(RENEW_OPTION).
                desc("Reads all certificates with keys from in-file file, renews the "
                     + "certificates by issuing the certificates with the CA (ca-file) wrting the resulting file to "
                     + "out-file").build());

        options.addOption(Option.builder().longOpt(DAYS_VALID_OPTION).argName("days").hasArg().
                desc("The number of days the certificate should be valid").build());

        options.addOption(Option.builder().longOpt(RETAIN_ALIASES_OPTION).
                desc("If enabled, aliases from the input file will be uses instead of generating " +
                     "new aliases").build());

        return options;
    }

    private static KeyStore loadKeyStore(String keyFile, boolean shouldExist, String password)
    throws Exception
    {
        File file = new File(keyFile);

        file = file.getAbsoluteFile();

        KeyStore keyStore = KeyStore.getInstance("PKCS12");

        if (shouldExist && !file.exists()) {
            throw new FileNotFoundException(keyFile + " pkcs#12 file not found.");
        }

        InputStream input = null;

        try {
            input = file.exists() ? new FileInputStream(file) : null;

            keyStore.load(input, password != null ? password.toCharArray() : null);
        }
        finally {
            IOUtils.closeQuietly(input);
        }

        return keyStore;
    }

    private void checkInFileOption()
    throws MissingOptionException
    {
        if (StringUtils.isEmpty(inFile)) {
            throw new MissingOptionException(IN_FILE_OPTION);
        }
    }

    private void checkInPasswordOption()
    throws MissingOptionException
    {
        if (StringUtils.isEmpty(inPassword)) {
            throw new MissingOptionException(IN_PASSWORD_OPTION);
        }
    }

    private void checkDestFileOption()
    throws MissingOptionException
    {
        if (StringUtils.isEmpty(destFile)) {
            throw new MissingOptionException(OUT_FILE_OPTION);
        }
    }

    private void checkDestPasswordOption()
    throws MissingOptionException
    {
        if (StringUtils.isEmpty(destPassword)) {
            throw new MissingOptionException(OUT_PASSWORD_OPTION);
        }
    }

    private void checkCAFileOption()
    throws MissingOptionException
    {
        if (StringUtils.isEmpty(caFile)) {
            throw new MissingOptionException(CA_FILE_OPTION);
        }
    }

    private void checkCAPasswordOption()
    throws MissingOptionException
    {
        if (StringUtils.isEmpty(caPassword)) {
            throw new MissingOptionException(CA_PASSWORD_OPTION);
        }
    }

    private void checkKeyAliasOption()
    throws MissingOptionException
    {
        if (StringUtils.isEmpty(keyAlias)) {
            throw new MissingOptionException(KEY_ALIAS_OPTION);
        }
    }

    private void checkDaysValidOption()
    throws MissingOptionException
    {
        if (!commandLine.hasOption(DAYS_VALID_OPTION)) {
            throw new MissingOptionException(DAYS_VALID_OPTION);
        }

        if (daysValid <= 0) {
            throw new IllegalArgumentException(DAYS_VALID_OPTION + " must be > 0");
        }
    }

    private String generateUniqueAlias(String alias) {
        return UUID.randomUUID() + "_" + alias;
    }

    private void mergePKCS12()
    throws Exception
    {
        checkInFileOption();
        checkInPasswordOption();
        checkDestFileOption();
        checkDestPasswordOption();

        KeyStore inStore = loadKeyStore(inFile, true, inPassword);
        KeyStore destStore = loadKeyStore(destFile, false, destPassword);

        Enumeration<String> aliases = inStore.aliases();

        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();

            String destAlias = retainAliases ? alias : generateUniqueAlias(alias);

            if (inStore.isKeyEntry(alias))
            {
                KeyStore.Entry entry = inStore.getEntry(alias, new KeyStore.PasswordProtection(inPassword.toCharArray()));

                destStore.setEntry(destAlias, entry, new KeyStore.PasswordProtection(destPassword.toCharArray()));
            }
            else {
                Certificate certificate = inStore.getCertificate(alias);

                destStore.setCertificateEntry(destAlias, certificate);
            }
        }

        destStore.store(new FileOutputStream(destFile), destPassword.toCharArray());
    }

    private static void printKeystoreDetails(KeyStore keyStore)
    throws KeyStoreException
    {
        Enumeration<String> aliases = keyStore.aliases();

        int count = 0;

        System.out.println("**** BEGIN ENTRIES ***");

        while (aliases.hasMoreElements())
        {
            count++;

            String alias = aliases.nextElement();

            StrBuilder sb = new StrBuilder();

            sb.append("Alias: ").append(alias).append(", key entry: ").append(keyStore.isKeyEntry(alias));

            System.out.println(sb.toString());
        }

        System.out.println("**** END ENTRIES ***");
        System.out.println("Nr of entries: " + count);
    }

    private void listPKCS12()
    throws Exception
    {
        checkInFileOption();
        checkInPasswordOption();

        KeyStore keyStore = loadKeyStore(inFile, true, inPassword);

        printKeystoreDetails(keyStore);
    }

    private void renewPKCS12()
    throws Exception
    {
        checkInFileOption();
        checkInPasswordOption();
        checkDestFileOption();
        checkDestPasswordOption();
        checkCAFileOption();
        checkCAPasswordOption();
        checkKeyAliasOption();
        checkDaysValidOption();

        KeyStore inKeyStore = loadKeyStore(inFile, true, inPassword);
        KeyStore caKeyStore = loadKeyStore(caFile, true, caPassword);
        KeyStore destStore = loadKeyStore(destFile, false, destPassword);

        PrivateKey caKey = (PrivateKey) caKeyStore.getKey(keyAlias, caPassword.toCharArray());

        if (caKey == null) {
            throw new IllegalArgumentException("There is no key with alias " + keyAlias);
        }

        X509Certificate caCertificate = (X509Certificate) caKeyStore.getCertificate(keyAlias);

        if (caCertificate == null) {
            throw new IllegalArgumentException("There is no certificate with alias " + keyAlias);
        }

        X509CertificateHolder caCertificateHolder = new X509CertificateHolder(caCertificate.getEncoded());

        KeyStore.PasswordProtection inPasswordProtection = new KeyStore.PasswordProtection(
                inPassword.toCharArray());

        final Date newExpirationDate = DateUtils.addDays(new Date(), daysValid);

        Enumeration<String> aliases = inKeyStore.aliases();

        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();

            KeyStore.Entry entry = inKeyStore.getEntry(alias, inPasswordProtection);

            if (entry instanceof KeyStore.PrivateKeyEntry privateKeyEntry)
            {
                X509Certificate certificate = (X509Certificate) privateKeyEntry.getCertificate();

                X509CertificateHolder certificateHolder = new X509CertificateHolder(certificate.getEncoded())
                {
                    @Override
                    public Date getNotAfter() {
                        return newExpirationDate;
                    }

                    @Override
                    public X500Name getIssuer() {
                        return caCertificateHolder.getSubject();
                    }
                };

                X509v3CertificateBuilder certificateBuilder = new X509v3CertificateBuilder(certificateHolder);

                X509CertificateHolder renewedCertificateHolder = certificateBuilder.build(new JcaContentSignerBuilder(
                        certificate.getSigAlgName()).build(caKey));

                JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter();

                X509Certificate renewedCertificate = certificateConverter.getCertificate(renewedCertificateHolder);

                destStore.setKeyEntry(alias, privateKeyEntry.getPrivateKey(), destPassword.toCharArray(),
                        new Certificate[] {renewedCertificate, caCertificate});

                StrBuilder sb = new StrBuilder();

                sb.append(CertificateInspector.getThumbprint(certificate))
                    .append(" => ").append(CertificateInspector.getThumbprint(renewedCertificate));

                System.out.println(sb);
            }
        }

        FileOutputStream destOutput = new FileOutputStream(destFile);

        try {
            destStore.store(destOutput, destPassword.toCharArray());
        }
        finally {
            IOUtils.closeQuietly(destOutput);
        }
    }

    private void handleCommandline(String[] args)
    throws Exception
    {
        CommandLineParser parser = new DefaultParser();

        Options options = createCommandLineOptions();

        HelpFormatter formatter = new HelpFormatter();

        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, true);

            throw e;
        }

        if (commandLine.getOptions().length == 0 || commandLine.hasOption(HELP_OPTION_SHORT) ||
            commandLine.hasOption(HELP_OPTION_LONG))
        {
            formatter.printHelp(COMMAND_NAME, options, true);

            return;
        }

        inPassword = commandLine.getOptionValue(IN_PASSWORD_OPTION);
        destPassword = commandLine.getOptionValue(OUT_PASSWORD_OPTION);
        caPassword = commandLine.getOptionValue(CA_PASSWORD_OPTION);
        inFile = commandLine.getOptionValue(IN_FILE_OPTION);
        destFile = commandLine.getOptionValue(OUT_FILE_OPTION);
        caFile = commandLine.getOptionValue(CA_FILE_OPTION);
        keyAlias = commandLine.getOptionValue(KEY_ALIAS_OPTION);
        daysValid = NumberUtils.toInt(commandLine.getOptionValue(DAYS_VALID_OPTION));
        retainAliases = commandLine.hasOption(RETAIN_ALIASES_OPTION);

        if (commandLine.hasOption(MERGE_OPTION)) {
            mergePKCS12();
        }
        else if (commandLine.hasOption(LIST_OPTION)) {
            listPKCS12();
        }
        else if (commandLine.hasOption(RENEW_OPTION)) {
            renewPKCS12();
        }
    }

    public static void main(String[] args)
    throws Exception
    {
        P12Tool tool = new P12Tool();

        try {
            tool.handleCommandline(args);
        }
        catch (MissingArgumentException | MissingOptionException e)
        {
            System.err.println("Some required parameters are missing: " + e.getMessage());

            System.exit(2);
        }
        catch (UnrecognizedOptionException e)
        {
            System.err.println(e.getMessage());

            System.exit(3);
        }
    }
}
