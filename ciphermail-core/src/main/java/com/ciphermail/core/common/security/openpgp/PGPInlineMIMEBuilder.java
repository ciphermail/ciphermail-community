/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.MimeUtils;
import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.mail.PartScanner;
import com.ciphermail.core.common.mail.PartScanner.PartListener;
import com.ciphermail.core.common.mail.matcher.ContentHeaderNameMatcher;
import com.ciphermail.core.common.mime.FileExtensionResolver;
import com.ciphermail.core.common.util.DirectAccessByteArrayOutputStream;
import net.htmlparser.jericho.Source;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.annotation.Nonnull;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.util.Collection;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

/**
 * Builds a PGP/INLINE message using the PGP partitioned format
 * (See http://archive.cert.uni-stuttgart.de/openpgp/2005/03/msg00000.html or
 * http://www.imc.org/ietf-openpgp/mail-archive/msg05251.html)
 *
 * @author Martijn Brinkers
 *
 */
public class PGPInlineMIMEBuilder
{
    private static final Logger logger = LoggerFactory.getLogger(PGPInlineMIMEBuilder.class);

    /*
     * Listener called for each MIME part in the message
     */
    private final PartListener partListener = new PartListenerImpl();

    /*
     * maximum recursive depth for MIME parts.
     */
    private int maxMimeDepth = 8;

    /*
     * If true the original Message-ID will be used for the new message
     */
    private boolean retainMessageID = true;

    /*
     * If true and the message contains 8bit parts, the message will be converted
     * to 7bit prior to signing.
     */
    private boolean convertTo7Bit = true;

    /*
     * The public PGP key of the signer
     */
    private PGPPublicKey signerPublicKey;

    /*
     * The private key of the signer
     */
    private PrivateKey signerPrivateKey;

    /*
     * The encryption keys
     */
    private Collection<PGPPublicKey> encryptionKeys;

    /*
     * The hash algorithm for the signature
     */
    private PGPHashAlgorithm hashAlgorithm = PGPHashAlgorithm.SHA256;

    /*
     * The compression algorithm to use
     */
    private PGPCompressionAlgorithm compressionAlgorithm = PGPCompressionAlgorithm.ZLIB;

    /*
     * The encryption algorithm to use
     */
    private PGPEncryptionAlgorithm encryptionAlgorithm = PGPEncryptionAlgorithm.AES_128;

    /*
     * The filename used for the literal compressed packet
     */
    private String compressedFilename = "";

    /*
     * The filename used for the literal data when no compression is used
     */
    private String literalFilename = "";

    /*
     * If true, an integrity packet will be added
     */
    private boolean addIntegrityPacket = true;

    /*
     * If true, a HTML part is converted to text and added as an additional part
     */
    private boolean convertHTMLToText = true;

    /*
     * Set to true if the message should signed
     */
    private boolean sign;

    /*
     * Set to true if the message should encrypted
     */
    private boolean encrypt;

    /*
     * Resolves the extension of an attachment based on content type of there is no filename set
     */
    private final FileExtensionResolver fileExtensionResolver;

    /*
     * If true, session keys will be obfuscated.
     */
    private boolean sessionKeyObfuscation;

    /*
     * Used by PartScanner
     */
    class PartListenerImpl implements PartScanner.PartListener
    {
        @Override
        public boolean onPart(Part parent, Part part, Object context)
        throws PartException
        {
            return PGPInlineMIMEBuilder.this.onPart(parent, part, (PartScannerContext) context);
        }
    }

    private static class PartWrapper
    {
        PartWrapper(Part part, boolean isTextPart)
        {
            this.part = part;
            this.isTextPart = isTextPart;
        }

        final Part part;
        final boolean isTextPart;
    }

    /*
     * Context used by PartScanner
     */
    private static class PartScannerContext
    {
        /*
         * Attachment counter
         */
        final MutableInt attachmentCounter = new MutableInt();

        /*
         * The body parts that should be added after scanning. We cannot add them during the scan since
         * that can result in weird errors
         */
        final Map<Part, List<PartWrapper>> parts = new IdentityHashMap<>();

        /*
         * Multiparts that should be flattened
         */
        final List<Multipart> flatten = new LinkedList<>();

        /*
         * True if an alternative part was found
         */
        boolean alternativePartFound;

        void addPart(Part parent, Part part, boolean isTextPart)
        {
            List<PartWrapper> parentParts = parts.get(parent);

            if (parentParts == null)
            {
                parentParts = new LinkedList<>();

                parts.put(parent, parentParts);
            }

            parentParts.add(new PartWrapper(part, isTextPart));
        }
    }

    public PGPInlineMIMEBuilder(@Nonnull FileExtensionResolver fileExtensionResolver) {
        this.fileExtensionResolver = Objects.requireNonNull(fileExtensionResolver);
    }

    private DirectAccessByteArrayOutputStream sign(InputStream input, PGPDocumentType type, String filename)
    throws IOException, PGPException
    {
        PGPSignatureBuilder builder = new PGPSignatureBuilder();

        builder.setHashAlgorithm(hashAlgorithm);
        builder.setPGPDocumentType(type);
        builder.setDetached(!encrypt);
        builder.setClearSign(!encrypt);
        builder.setLiteralFilename(literalFilename);
        builder.setArmor(!encrypt);

        if (StringUtils.isNotEmpty(filename)) {
            builder.setLiteralFilename(filename);
        }

        DirectAccessByteArrayOutputStream signed = new DirectAccessByteArrayOutputStream();

        builder.sign(input, signed, signerPublicKey, signerPrivateKey);

        return signed;
    }

    private DirectAccessByteArrayOutputStream encrypt(InputStream input, String filename, boolean armor)
    throws IOException, PGPException
    {
        PGPEncryptionBuilder builder = new PGPEncryptionBuilder();

        builder.setEncryptionAlgorithm(encryptionAlgorithm);
        builder.setCompressionAlgorithm(compressionAlgorithm);
        builder.setAddIntegrityPacket(addIntegrityPacket);
        builder.setCompressedFilename(compressedFilename);
        builder.setLiteralFilename(literalFilename);
        builder.setArmor(armor);
        builder.setSessionKeyObfuscation(sessionKeyObfuscation);

        Map<String, String> additionalArmorHeaders = new HashMap<>();

        // Since we converted it to UTF-8, we must add the charset to the armor
        additionalArmorHeaders.put("Charset", CharEncoding.UTF_8);

        builder.setAdditionalArmorHeaders(additionalArmorHeaders);

        // If the message is also signed, we do not need to add a literal packet
        builder.setAddLiteralPacket(!sign);

        if (StringUtils.isNotEmpty(filename))
        {
            builder.setCompressedFilename(filename);
            builder.setLiteralFilename(filename);
        }

        DirectAccessByteArrayOutputStream encrypted = new DirectAccessByteArrayOutputStream();

        builder.encrypt(input, encrypted, encryptionKeys);

        return encrypted;
    }

    private String signAndEncryptText(InputStream input)
    throws PGPException, IOException
    {
        DirectAccessByteArrayOutputStream mime = null;

        if (sign)
        {
            mime = sign(input, PGPDocumentType.TEXT, null);

            input = mime.getInputStream();
        }

        if (encrypt) {
            mime = encrypt(input, null, true /* use ASCII armor */ );
        }

        if (mime == null) {
            throw new PGPException("The message was not signed or encrypted");
        }

        return IOUtils.toString(mime.getInputStream(), StandardCharsets.UTF_8);
    }

    private String signAndEncryptTextPart(Part part)
    throws PGPException, IOException, MessagingException
    {
        Object content = part.getContent();

        InputStream input;

        if (content instanceof String stringContent) {
            input = IOUtils.toInputStream(stringContent, StandardCharsets.UTF_8);
        }
        else {
            // Should normally not happen. Log and fallback to "raw" InputStream
            logger.warn("Part content-type is {}, but content is not a string but a {}", part.getContentType(),
                    content.getClass());

            input = part.getInputStream();
        }

        return signAndEncryptText(input);
    }

    private void handleTextPart(Part part)
    throws PGPException, IOException, MessagingException
    {
        part.setText(signAndEncryptTextPart(part));
    }

    private void handleHTMLPart(Part parent, Part part, PartScannerContext context)
    throws PGPException, IOException, MessagingException
    {
        Object originalContent = part.getContent();

        // It's an HTML part and part of a multipart/alternative. We need to convert it in a special way
        part.setHeader("X-Content-PGP-Universal-Saved-Content-Type", "text/html; charset=UTF-8");
        // For *all* HTML parts, this must be here (for PGP desktop)
        part.setHeader("X-PGP-MIME-Structure", "alternative");

        part.setDisposition(Part.ATTACHMENT);

        part.setContent(signAndEncryptTextPart(part), encrypt ? "application/octet-stream; name=\"PGPexch.htm.pgp\"" :
            "application/octet-stream; name=\"PGPexch.htm.asc\"");

        if (parent.isMimeType("multipart/alternative"))
        {
            // We need to make the parent multipart/mixed and add then sign
            Multipart parentMultipart = (Multipart) parent.getContent();

            MimeMultipart mixed = new MimeMultipart();

            for (int i = 0; i < parentMultipart.getCount(); i++) {
                mixed.addBodyPart(parentMultipart.getBodyPart(i));
            }

            parent.setContent(mixed);

            // This multipart must be flattened for PGP desktop support
            context.flatten.add(parentMultipart);
            context.alternativePartFound = true;
        }
        else {
            // It's an HTML part only. Convert HTML to text and add the converted text as an extra inline part so
            // non-PGP clients and client which do not understand the encoding can read the converted part
            if (convertHTMLToText)
            {
                if (originalContent instanceof String originalContentStringContent)
                {
                    try {
                        Source source = new Source(originalContentStringContent);

                        MimeBodyPart textPart = new MimeBodyPart();

                        textPart.setText(signAndEncryptText(IOUtils.toInputStream(source.getRenderer().toString(),
                                StandardCharsets.UTF_8)));

                        context.addPart(parent, textPart, true);
                    }
                    catch (Exception e) {
                        logger.error("An error orcurred converting HTML to text.", e);
                    }
                }
                else {
                    logger.warn("HTML part is not a String but a {}", originalContent.getClass());
                }
            }
        }
    }

    private void handleBinaryPart(Part parent, Part part, PartScannerContext context)
    throws PGPException, IOException, MessagingException
    {
        InputStream input = part.getInputStream();

        String realFilename = HeaderUtils.decodeTextQuietly(MimeUtils.getFilenameQuietly(part));

        String newFilename = realFilename;

        if (StringUtils.isEmpty(newFilename))
        {
            context.attachmentCounter.increment();

            newFilename = "Attachment" + context.attachmentCounter.intValue() + "." + fileExtensionResolver.
                    getExtensionFromContentType(part.getContentType());
        }

        DirectAccessByteArrayOutputStream mime = null;

        if (sign)
        {
            mime = sign(input, PGPDocumentType.BINARY, realFilename);

            input = mime.getInputStream();
        }

        if (encrypt)
        {
            mime = encrypt(input, realFilename, false /* don't use ASCII armor */);

            // replace the current part with the encrypted content
            part.setHeader("X-Content-PGP-Universal-Saved-Content-Type", part.getContentType());

            newFilename = newFilename + ".pgp";

            part.setFileName(HeaderUtils.encodeTextQuietly(newFilename, CharEncoding.UTF_8));
            part.setDisposition(Part.ATTACHMENT);

            part.setDataHandler(new DataHandler(new ByteArrayDataSource(mime.toByteArray(),
                    "application/octet-stream")));
        }
        else {
            // The message was signed only. We therefore need to create a new .sig part. We can however not add
            // it directly since that would interfere with the mime part scanner
            MimeBodyPart signaturePart = new MimeBodyPart();

            newFilename = newFilename + ".sig";

            signaturePart.setFileName(HeaderUtils.encodeTextQuietly(newFilename, CharEncoding.UTF_8));
            signaturePart.setDisposition(Part.ATTACHMENT);

            if (mime == null) {
                // should not happen. Sign or encrypt should always be true and mime should therefore be non null.
                throw new PGPException("mime is null");
            }

            signaturePart.setDataHandler(new DataHandler(new ByteArrayDataSource(mime.toByteArray(),
                    "application/octet-stream")));

            context.addPart(parent, signaturePart, false);
        }
    }

    /*
     * Is called for every MIME part of the message
     */
    private boolean onPart(Part parent, Part part, PartScannerContext context)
    throws PartException
    {
        try {
            // Part should normally never by a multipart since PartScanner does not call this for multipart (
            // unless exceptionOnMaxDepthReached is false). Skip if it is a multipart
            if (!part.isMimeType("multipart/*"))
            {
                if (part.isMimeType("text/plain")) {
                    handleTextPart(part);
                }
                else if (part.isMimeType("text/html")) {
                    handleHTMLPart(parent, part, context);
                }
                else {
                    handleBinaryPart(parent, part, context);
                }
            }
            return true;

        }
        catch (MessagingException | PGPException | IOException e) {
            throw new PartException(e);
        }
    }

    private Multipart getParentMultipart(Multipart part)
    {
        Multipart result = null;

        Part parentPart = part.getParent();

        // After 25 tries, give up, we might have a loop or some very deep mime structure
        for (int i = 0; i < 25; i++)
        {
            if (parentPart instanceof Message || parentPart == null) {
                // We are done
                break;
            }

            if (parentPart instanceof Multipart)
            {
                result = (Multipart) parentPart;

                break;
            }
            else if (parentPart instanceof BodyPart)
            {
                result = ((BodyPart)parentPart).getParent();

                break;
            }
        }

        return result;
    }

    public MimeMessage build(MimeMessage message)
    throws IOException, MessagingException, PGPException
    {
        if (!sign && !encrypt){
            throw new PGPException("sign and/or encrypt must be set");
        }

        if (sign && (signerPrivateKey == null || signerPublicKey == null)) {
            throw new PGPException("Not all signer keys are set.");
        }

        if (encrypt && CollectionUtils.isEmpty(encryptionKeys)) {
            throw new PGPException("Encryption keys are not set.");
        }

        // Since the message will be changed "in place", we need to clone it first
        message = retainMessageID ? MailUtils.cloneMessageWithFixedMessageID(message) : MailUtils.cloneMessage(message);

        if (convertTo7Bit && MailUtils.convertTo7Bit(message)) {
            message.saveChanges();
        }

        try {
            PartScanner partScanner = new PartScanner(partListener, maxMimeDepth);

            partScanner.setExceptionOnMaxDepthReached(true);

            PartScannerContext context = new PartScannerContext();

            partScanner.scanPart(message, context);

            if (context.parts.size() > 0 || !context.flatten.isEmpty()) {
                // If some post-scanning changes need to be applied, save message first to make sure that
                // all changes (like updated content types) are seen
                message.saveChanges();
            }

            if (context.parts.size() > 0)
            {
                // Some new parts were added which should not be added to the final message
                for (Entry<Part, List<PartWrapper>> entry : context.parts.entrySet())
                {
                    Part parent = entry.getKey();

                    Multipart mp;

                    if (parent.isMimeType("multipart/mixed") || parent.isMimeType("multipart/related")) {
                        mp = (Multipart) parent.getContent();
                    }
                    else {
                        // The message is not a multipart/mixed so we need to create a new multipart and replace this
                        // part with the new multipart
                        mp = new MimeMultipart();

                        // Add the existing part message as a bodypart
                        mp.addBodyPart(BodyPartUtils.makeContentBodyPart(parent, new ContentHeaderNameMatcher()));

                        parent.setContent(mp);
                    }

                    boolean alternativePartFound = context.alternativePartFound;

                    for (PartWrapper partToAdd : entry.getValue())
                    {
                        if (!alternativePartFound && partToAdd.isTextPart) {
                            // There was not alternative part in the message, add any text part as the
                            // first part to make HTML only messages readable by Enigmail
                            mp.addBodyPart(BodyPartUtils.toMimeBodyPart(partToAdd.part), 0);
                        }
                        else {
                            mp.addBodyPart(BodyPartUtils.toMimeBodyPart(partToAdd.part));
                        }
                    }
                }
            }

            if (!context.flatten.isEmpty())
            {
                // Some multiparts must be flattened
                for (Multipart partToFlatten : context.flatten)
                {
                    Multipart parent = getParentMultipart(partToFlatten);

                    // multipart/report does not allow new bodyparts to be added so we will skip MultipartReport.
                    if (parent != null && !(MimeUtils.isMimeType(parent, "multipart/report")))
                    {
                        // Flattening is required. We want the flattened parts to be the first parts,
                        //* since they were most likely the multipart/alternative parts.
                        for (int i = 0; i < partToFlatten.getCount(); i++) {
                            parent.addBodyPart(partToFlatten.getBodyPart(i), i);
                        }

                        // Remove the multipart which was flattened
                        parent.removeBodyPart((BodyPart) partToFlatten.getParent());
                    }
                }
            }

            message.setHeader("X-PGP-Encoding-Format", "Partitioned");

            message.saveChanges();

            return message;
        }
        catch (PartException | IOException e) {
            throw new MessagingException("Error handling part", e);
        }
    }

    public boolean isRetainMessageID() {
        return retainMessageID;
    }

    public void setRetainMessageID(boolean retainMessageID) {
        this.retainMessageID = retainMessageID;
    }

    public boolean isConvertTo7Bit() {
        return convertTo7Bit;
    }

    public void setConvertTo7Bit(boolean convertTo7Bit) {
        this.convertTo7Bit = convertTo7Bit;
    }

    public void setSigner(PGPPublicKey signerPublicKey, PrivateKey signerPrivateKey)
    {
        this.signerPublicKey = signerPublicKey;
        this.signerPrivateKey = signerPrivateKey;
    }

    public PGPPublicKey getSignerPublicKey() {
        return signerPublicKey;
    }

    public PrivateKey getSignerPrivateKey() {
        return signerPrivateKey;
    }

    public Collection<PGPPublicKey> getEncryptionKeys() {
        return encryptionKeys;
    }

    public void setEncryptionKeys(Collection<PGPPublicKey> encryptionKeys) {
        this.encryptionKeys = encryptionKeys;
    }

    public int getMaxMimeDepth() {
        return maxMimeDepth;
    }

    public void setMaxMimeDepth(int maxMimeDepth) {
        this.maxMimeDepth = maxMimeDepth;
    }

    public PGPHashAlgorithm getHashAlgorithm() {
        return hashAlgorithm;
    }

    public void setHashAlgorithm(PGPHashAlgorithm hashAlgorithm) {
        this.hashAlgorithm = hashAlgorithm;
    }

    public PGPCompressionAlgorithm getCompressionAlgorithm() {
        return compressionAlgorithm;
    }

    public void setCompressionAlgorithm(PGPCompressionAlgorithm compressionAlgorithm) {
        this.compressionAlgorithm = compressionAlgorithm;
    }

    public PGPEncryptionAlgorithm getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }

    public void setEncryptionAlgorithm(PGPEncryptionAlgorithm encryptionAlgorithm) {
        this.encryptionAlgorithm = encryptionAlgorithm;
    }

    public String getCompressedFilename() {
        return compressedFilename;
    }

    public void setCompressedFilename(String compressedFilename) {
        this.compressedFilename = compressedFilename;
    }

    public String getLiteralFilename() {
        return literalFilename;
    }

    public void setLiteralFilename(String literalFilename) {
        this.literalFilename = literalFilename;
    }

    public boolean isAddIntegrityPacket() {
        return addIntegrityPacket;
    }

    public void setAddIntegrityPacket(boolean addIntegrityPacket) {
        this.addIntegrityPacket = addIntegrityPacket;
    }

    public boolean isConvertHTMLToText() {
        return convertHTMLToText;
    }

    public void setConvertHTMLToText(boolean convertHTMLToText) {
        this.convertHTMLToText = convertHTMLToText;
    }

    public boolean isSign() {
        return sign;
    }

    public void setSign(boolean sign) {
        this.sign = sign;
    }

    public boolean isEncrypt() {
        return encrypt;
    }

    public void setEncrypt(boolean encrypt) {
        this.encrypt = encrypt;
    }

    public boolean isSessionKeyObfuscation() {
        return sessionKeyObfuscation;
    }

    public void setSessionKeyObfuscation(boolean sessionKeyObfuscation) {
        this.sessionKeyObfuscation = sessionKeyObfuscation;
    }
}
