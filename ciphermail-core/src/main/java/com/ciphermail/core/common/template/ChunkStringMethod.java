/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.template;

import com.ciphermail.core.common.util.MiscStringUtils;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateModelException;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * A function for Freemarker that allows you to split up a string into chunks of a certain length
 * <p>
 * There should be 2 parameters to the Freemarker call with one optional:
 * <p>
 * content      : String
 * chunk size   : Integer [optional]
 */
public class ChunkStringMethod extends TemplateMethodModelBase
{
    public static final String METHOD_NAME = "chunkString";

    @Override
    public Object exec(@SuppressWarnings("rawtypes") List arguments)
    throws TemplateModelException
    {
        // There should be 1 or 2 parameters.
        //
        // [0] content      : String
        // [1] chunk size   : Integer [optional]
        checkMethodArgCount(arguments, 1, 2);

        int chunkSize = 76;

        if (arguments.size() > 1) {
            chunkSize = argToInt(arguments.get(1));
        }

        String input = argToString(arguments.get(0));

        // Remove any CR/LF characters before chunking
        input = StringUtils.replaceChars(input, "\r\n", null);

        return new SimpleScalar(MiscStringUtils.splitStringIntoFixedSizeChunks(input, chunkSize));
    }
}
