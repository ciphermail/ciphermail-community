/*
 * Copyright (c) 2017-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.tools;

import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.cms.SignerInfo;
import com.ciphermail.core.common.security.cms.SignerInfoException;
import com.ciphermail.core.common.security.smime.SMIMEHeader;
import com.ciphermail.core.common.security.smime.SMIMESignedInspector;
import com.ciphermail.core.common.security.smime.SMIMESignedInspectorImpl;
import com.ciphermail.core.common.util.MiscFilenameUtils;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.subethamail.smtp.MessageContext;
import org.subethamail.smtp.MessageHandler;
import org.subethamail.smtp.MessageHandlerFactory;
import org.subethamail.smtp.server.SMTPServer;

import javax.mail.internet.MimeMessage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.cert.CertSelector;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Tool which acts as an SMTP server and reports details about the messages it received.
 */
@SuppressWarnings({"java:S106", "java:S112"})
public class SMTPSink
{
    private static final String COMMAND_NAME = SMTPSink.class.getName();

    /*
     * The CLI option for showing the help
     */
    private static final String HELP_OPTION_SHORT = "h";
    private static final String HELP_OPTION_LONG = "help";

    /*
     * If set, the SMTP sink will be started
     */
    private static final String START_OPTION = "start";

    /*
     * The CLI option for the smtp port
     */
    private static final String PORT_OPTION = "port";

    /*
     * If set, the S/MIME signature if the incoming message is validated
     */
    private static final String VALIDATE_SMIME_SIGNATURES_OPTION = "validate-smime-signatures";

    /*
     * If set, the incoming message must be S/MIME signed. If not, an error message will be written to stderr
     */
    private static final String MUST_BE_SIGNED_OPTION = "must-be-signed";

    /*
     * If set, the incoming message will be stored in the provided dir
     */
    private static final String DUMP_MESSAGE_OPTION = "dump-message";

    /*
     * The SMTP port to listen on
     */
    private int smtpPort = 2525;

    /*
     * If set, the S/MIME signature if the incoming message is validated
     */
    private boolean validateSMIMESignature;

    /*
     * If set, the incoming message must be S/MIME signed. If not, a error message will be written to stderr
     */
    private boolean requireSMIMESigned;

    /*
     * There to which dumped MIME message will be stored
     */
    private File dumpMessageDir;

    /*
     * Keeps count of the number of messages received
     */
    private final AtomicInteger received = new AtomicInteger();

    private Options createCommandLineOptions()
    {
        Options options = new Options();

        options.addOption(Option.builder(HELP_OPTION_SHORT).longOpt(HELP_OPTION_LONG).desc("Show help").build());

        options.addOption(Option.builder().longOpt(START_OPTION).desc(
                "Start listening for incoming email").build());

        options.addOption(Option.builder().longOpt(PORT_OPTION).argName("port").hasArg().
                desc("The SMTP port to bind to (default: 2525)").build());

        options.addOption(Option.builder().longOpt(VALIDATE_SMIME_SIGNATURES_OPTION).desc(
                "If set, the S/MIME signatures of incoming messages are validated").build());

        options.addOption(Option.builder().longOpt(MUST_BE_SIGNED_OPTION).desc(
                "If set, incoming message must be S/MIME signed").build());

        options.addOption(Option.builder().longOpt(DUMP_MESSAGE_OPTION).argName("dir").hasArg().
                desc("If set, incoming mail will be stored in the specified dir").build());

        return options;
    }

    private void logError(String message, Throwable t)
    {
        System.err.println(message);
        System.err.println(ExceptionUtils.getFullStackTrace(t));
    }

    private void logError(String message) {
        System.err.println(message);
    }

    /*
     * MessageHandlerFactory used by the internal SMTP server
     */
    class MessageHandlerFactoryImpl implements MessageHandlerFactory
    {
        @Override
        public MessageHandler create(MessageContext context) {
            return new Handler();
        }

        class Handler implements MessageHandler
        {
            @Override
            public void data(InputStream input)
            {
                MimeMessage message = null;

                try {
                    System.out.println(received.incrementAndGet());

                    ByteArrayOutputStream mime = new ByteArrayOutputStream();

                    IOUtils.copy(input, mime);

                    try {
                        message = new MimeMessage(MailSession.getDefaultSession(), new ByteArrayInputStream(
                                mime.toByteArray()));
                    }
                    catch (Exception e) {
                        logError("Error parsing MIME", e);
                    }

                    if (dumpMessageDir != null) {
                        dumpMessage(mime.toByteArray(),  MailUtils.getMessageIDQuietly(message));
                    }

                    handleMessage(message);
                }
                catch (Exception e) {
                    logError("Error handling message. Message-Id: " + MailUtils.getMessageIDQuietly(message), e);
                }
            }

            @Override
            public void done() {
                // ignored
            }

            @Override
            public void from(String from) {
                // ignored
            }

            @Override
            public void recipient(String recipient) {
                // ignored
            }
        }
    }

    private void dumpMessage(byte[] mime, String messageId)
    throws Exception
    {
        File outputFile = new File(dumpMessageDir, MiscFilenameUtils.safeEncodeFilenameBlacklist(
                UUID.randomUUID() + "-" + messageId + ".eml", true));

        OutputStream output = new BufferedOutputStream(new FileOutputStream(outputFile));

        try {
            IOUtils.copy(new ByteArrayInputStream(mime), output);
        }
        finally {
            IOUtils.closeQuietly(output);
        }
    }

    private void validateSMIMESignature(MimeMessage message)
    throws Exception
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(message,
                securityFactory.getNonSensitiveProvider(), securityFactory.getNonSensitiveProvider());

        List<SignerInfo> signers = inspector.getSigners();

        List<X509Certificate> certificates = inspector.getCertificates();

        boolean verified = false;

        for (int i = 0; i < signers.size(); i++)
        {
            SignerInfo signer = signers.get(i);

            CertSelector selector = signer.getSignerId().getSelector();

            List<X509Certificate> signingCerts = CertificateUtils.getMatchingCertificates(certificates, selector);

            if (!signingCerts.isEmpty())
            {
                // get the first certificate
                X509Certificate certificate = signingCerts.get(0);

                try {
                    if (signer.verify(certificate.getPublicKey()))
                    {
                        verified = true;

                        break;
                    }
                }
                catch(SignerInfoException e) {
                    logError("Verification failed. Message-Id: " + MailUtils.getMessageIDQuietly(message), e);
                }
            }
            else {
                logError("Signing certificate not found so unable to verify signature. Message-Id: " +
                        MailUtils.getMessageIDQuietly(message));
            }
        }

        if (!verified) {
            logError("Signature verification failed. Message-Id: " + MailUtils.getMessageIDQuietly(message));
        }
    }

    private void handleMessage(MimeMessage message)
    {
        try {
            SMIMEHeader.Type smimetype = SMIMEHeader.getSMIMEContentType(message, SMIMEHeader.Strict.YES);

            boolean signed = smimetype == SMIMEHeader.Type.CLEAR_SIGNED ||
                    smimetype == SMIMEHeader.Type.OPAQUE_SIGNED;

            if (signed)
            {
                if (validateSMIMESignature) {
                    validateSMIMESignature(message);
                }
            }
            else {
                if (requireSMIMESigned)
                {
                    logError("Message is not S/MIME signed. Message-Id: " +
                            MailUtils.getMessageIDQuietly(message));
                }
            }
        }
        catch (Exception e) {
            logError("Error handling message. Message-Id: " + MailUtils.getMessageIDQuietly(message), e);
        }
    }

    private void startSMTPServer()
    {
        MessageHandlerFactoryImpl factory = new MessageHandlerFactoryImpl();

        SMTPServer smtpServer = new SMTPServer(factory);
        smtpServer.setPort(smtpPort);
        smtpServer.setHideTLS(true);

        System.out.println("Listening on port " + smtpPort);

        smtpServer.start();
    }

    private void handleCommandline(String[] args)
    throws Exception
    {
        CommandLineParser parser = new DefaultParser();

        Options options = createCommandLineOptions();

        HelpFormatter formatter = new HelpFormatter();

        CommandLine commandLine;

        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, true);

            throw e;
        }

        if (commandLine.hasOption(PORT_OPTION)) {
            smtpPort = Integer.parseInt(commandLine.getOptionValue(PORT_OPTION));
        }

        if (commandLine.hasOption(DUMP_MESSAGE_OPTION))
        {
            dumpMessageDir = new File(commandLine.getOptionValue(DUMP_MESSAGE_OPTION));

            if (!dumpMessageDir.exists()) {
                throw new IllegalArgumentException(dumpMessageDir + " does not exist");
            }
            if (!dumpMessageDir.isDirectory()) {
                throw new IllegalArgumentException(dumpMessageDir + " is not a directory");
            }
            if (!dumpMessageDir.canWrite()) {
                throw new IllegalArgumentException(dumpMessageDir + " is not writable");
            }
        }

        validateSMIMESignature = commandLine.hasOption(VALIDATE_SMIME_SIGNATURES_OPTION);
        requireSMIMESigned = commandLine.hasOption(MUST_BE_SIGNED_OPTION);

        if (commandLine.getOptions().length == 0)
        {
            formatter.printHelp(COMMAND_NAME, options, true);

            System.exit(1);
        }
        if (commandLine.getOptions().length == 0 || commandLine.hasOption(HELP_OPTION_SHORT) ||
            commandLine.hasOption(HELP_OPTION_LONG))
        {
            formatter.printHelp(COMMAND_NAME, options, true);
        }
        if (commandLine.hasOption(START_OPTION)) {
            startSMTPServer();
        }
    }

    public static void main(String[] args)
    throws Exception
    {
        SMTPSink tool = new SMTPSink();

        try {
            tool.handleCommandline(args);
        }
        catch (MissingArgumentException e)
        {
            System.err.println("Not all required parameters are specified. " + e.getMessage());

            System.exit(3);
        }
        catch (ParseException e)
        {
            System.err.println("Command line parsing error. " + e);

            System.exit(4);
        }
    }
}
