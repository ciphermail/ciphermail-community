/*
 * Copyright (c) 2010-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

/**
 * An InputStream that delegates to an underlying InputStream and checks whether the stream is closed. This can be
 * used when an InputStream does not support close, yet the caller wants to close the InputStream.
 *
 * @author Martijn Brinkers
 *
 */
import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class CheckCloseInputStream extends InputStream
{
    /*
     * Set to true if the stream is closed.
     */
    private boolean closed;

    /*
     * The InputStream to delegate to
     */
    private final InputStream delegate;

    public CheckCloseInputStream(@Nonnull InputStream delegate) {
        this.delegate = Objects.requireNonNull(delegate);
    }

    private void checkClosed()
    throws IOException
    {
        if (closed) {
            throw new IOException("The InputStream is closed.");
        }
    }

    @Override
    public int read()
    throws IOException
    {
        checkClosed();

        return delegate.read();
    }

    @Override
    public int read(@Nonnull byte[] b)
    throws IOException
    {
        checkClosed();

        return delegate.read(b);
    }

    @Override
    public int read(@Nonnull byte[] b, int off, int len)
    throws IOException
    {
        checkClosed();

        return delegate.read(b, off, len);
    }

    @Override
    public long skip(long n)
    throws IOException
    {
        checkClosed();

        return delegate.skip(n);
    }

    @Override
    public int available()
    throws IOException
    {
        checkClosed();

        return delegate.available();
    }

    @Override
    public void close()
    throws IOException
    {
        closed = true;

        super.close();
    }

    @Override
    public synchronized void mark(int i) {
        delegate.mark(i);
    }

    @Override
    public synchronized void reset()
    throws IOException
    {
        checkClosed();

        delegate.reset();
    }

    @Override
    public boolean markSupported() {
        return delegate.markSupported();
    }

    @Override
    public String toString() {
        return delegate.toString();
    }
}
