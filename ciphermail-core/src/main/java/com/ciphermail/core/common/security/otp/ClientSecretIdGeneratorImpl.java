/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.otp;

import org.apache.commons.lang.CharEncoding;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Objects;

public class ClientSecretIdGeneratorImpl implements ClientSecretIdGenerator
{
    /*
     * Generator used for generating the passwords.
     */
    private final OTPGenerator otpGenerator;

    /*
     * The encoding used for the conversion of passwords to bytes
     */
    private String encoding = CharEncoding.US_ASCII;

    /*
     * A known, i.e., non-secret, value which is used to deterministically generate a clientSecretID
     */
    private String clientSecretIdKnownValue = "ciphermail";

    /*
     * The length of the clientSecretID in bytes
     */
    private int clientSecretIdLength = 4;

    public ClientSecretIdGeneratorImpl(@Nonnull OTPGenerator otpGenerator) {
        this.otpGenerator = Objects.requireNonNull(otpGenerator);
    }

    @Override
    public @Nonnull String generateClientSecretID(@Nonnull byte[] secret)
    throws IOException
    {
        try {
            return otpGenerator.generate(secret,
                    clientSecretIdKnownValue.getBytes(encoding),
                    clientSecretIdLength);
        }
        catch (GeneralSecurityException e) {
            throw new IOException(e);
        }
    }

    public String getEncoding() {
        return encoding;
    }

    public ClientSecretIdGeneratorImpl setEncoding(String encoding)
    {
        this.encoding = encoding;
        return this;
    }

    public String getClientSecretIdKnownValue() {
        return clientSecretIdKnownValue;
    }

    public ClientSecretIdGeneratorImpl setClientSecretIdKnownValue(String clientSecretIdKnownValue)
    {
        this.clientSecretIdKnownValue = clientSecretIdKnownValue;
        return this;
    }

    public int getClientSecretIdLength() {
        return clientSecretIdLength;
    }

    public ClientSecretIdGeneratorImpl setClientSecretIdLength(int clientSecretIdLength)
    {
        this.clientSecretIdLength = clientSecretIdLength;
        return this;
    }
}
