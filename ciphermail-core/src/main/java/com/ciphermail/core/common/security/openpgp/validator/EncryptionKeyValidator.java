/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.security.openpgp.PGPKeyFlagChecker;
import com.ciphermail.core.common.security.openpgp.PGPNotSelfSignedException;
import com.ciphermail.core.common.util.Context;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * PGPPublicKeyValidator implementation that checks whether a PGP public key is valid for encryption
 *
 * @author Martijn Brinkers
 *
 */
public class EncryptionKeyValidator implements PGPPublicKeyValidator
{
    private static final Logger logger = LoggerFactory.getLogger(EncryptionKeyValidator.class);

    /*
     * For checking the key flags
     */
    private final PGPKeyFlagChecker keyFlagChecker;

    public EncryptionKeyValidator(@Nonnull PGPKeyFlagChecker keyFlagChecker) {
        this.keyFlagChecker = Objects.requireNonNull(keyFlagChecker);
    }

    @Override
    public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
    {
        boolean valid = false;

        try {
            // Subkey binding check is only relevant for sub keys
            if (!publicKey.isMasterKey())
            {
                // The master key must be stored in the context
                PGPPublicKey masterKey = context.get(StandardValidatorContextObjects.MASTER_KEY, PGPPublicKey.class);

                if (masterKey == null)
                {
                    return new PGPPublicKeyValidatorResultImpl(this, false, PGPPublicKeyValidatorFailureSeverity.MAJOR,
                            "Master key not available. Unable to validate key flags.");
                }

                valid = keyFlagChecker.isSubKeyValidForEncryption(masterKey, publicKey);
            }
            else {
                valid = keyFlagChecker.isMasterKeyValidForEncryption(publicKey);
            }
        }
        catch (PGPNotSelfSignedException e) {
            logger.debug("Key flags signature was not self signed", e);
        }
        catch (Exception e) {
            logger.error("Error validating the key flags", e);
        }

        String message = null;
        PGPPublicKeyValidatorFailureSeverity severity = null;

        if (!valid)
        {
            message = "Key is not valid for encryption.";
            severity = PGPPublicKeyValidatorFailureSeverity.CRITICAL;
        }

        return new PGPPublicKeyValidatorResultImpl(this, valid, severity, message);
    }
}
