/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.EOFInputStream;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.BufferedInputStream;
import java.io.InputStream;

/**
 * PGPKeyRing utility classes
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyRingUtils
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyRingUtils.class);

    private PGPKeyRingUtils() {
        // empty on purpose
    }

    /**
     * Tries to detect what type of keyring the input is. Returns null if the input is not a keyring. The input can
     * contains public and secret key rings. The complete input is scanned until EOF or until a secret key ring is
     * found.
     */
    public static PGPKeyRingType getKeyRingType(@Nonnull InputStream input)
    {
        PGPKeyRingType type = null;

        if (!input.markSupported()) {
            input = new BufferedInputStream(input);
        }

        EOFInputStream eofInputStream = new EOFInputStream(input);

        try {
            while (!eofInputStream.isEOF())
            {
                PGPObjectFactory factory = new JcaPGPObjectFactory(PGPUtil.getDecoderStream(eofInputStream));

                Object obj;

                while ((obj = factory.nextObject()) != null)
                {
                    if (obj instanceof PGPPublicKeyRing) {
                        type = PGPKeyRingType.PUBLIC;
                    }
                    else if (obj instanceof PGPSecretKeyRing)
                    {
                        // Done. A secret ring supersedes a public ring
                        return PGPKeyRingType.SECRET;
                    }
                }
            }
        }
        catch (Exception e) {
            // log and ignore
            logger.debug("Error #getKeyRingType", e);
        }

        return type;
    }
}
