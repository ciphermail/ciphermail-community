/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.CRLFInputStream;
import com.ciphermail.core.common.util.DelegateInputStream;
import com.ciphermail.core.common.util.DirectAccessByteArrayOutputStream;
import com.ciphermail.core.common.util.MutableString;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.io.input.CloseShieldInputStream;
import org.apache.commons.io.input.ReaderInputStream;
import org.apache.commons.io.input.TeeInputStream;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.bcpg.ArmoredInputStream;
import org.bouncycastle.openpgp.PGPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.internet.MimeUtility;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Handler for PGP inline
 *
 * @author Martijn Brinkers
 *
 */
public class PGPInlineHandler
{
    private static final Logger logger = LoggerFactory.getLogger(PGPInlineHandler.class);

    private static final byte[] CRLF = new byte[]{'\r', '\n'};

    /*
     * PGP headers
     */
    private static final String BEGIN_PGP_MESSAGE_HEADER  = "-----BEGIN PGP MESSAGE-----";
    private static final String END_PGP_MESSAGE_HEADER  = "-----END PGP MESSAGE-----";
    private static final String BEGIN_PGP_SIGNED_MESSAGE_HEADER  = "-----BEGIN PGP SIGNED MESSAGE-----";

    /*
     * Provides PGP key pair based on key id
     */
    private final PGPKeyPairProvider keyPairProvider;

    /*
     * The PGPHandler which handles the PGP message
     */
    private PGPHandler handler;

    /*
     * The maximum number of bytes read for decompression. This is done to protect against "ZIP bombs".
     * The default value will be set to 10% of Runtime.getRuntime().maxMemory()
     */
    private int decompressionUpperlimit = (int) (Runtime.getRuntime().maxMemory() * 0.1);

    /*
     * The maximum number of PGP objects. If the number of PGP objects read exceeds the maximum, a PGP
     * exception will be thrown.
     */
    private int maxObjects = 64;

    /*
     * If true (the default), the PGP blob will be decrypted if it is encrypted and a suitable key is available
     */
    private boolean decrypt = true;

    /*
     * If true, any PGP signature will be removed
     */
    private boolean removeSignature = true;

    /*
     * True if there is extra data other the empty lines before or after the PGP part
     */
    private boolean mixedContent;

    /*
     * True if the handled message has been changed (for example decrypted or signature removed)
     */
    private boolean changed;

    /*
     * All the PGPHandler's used during handling of the message
     */
    private final List<PGPHandler> handlers = new LinkedList<>();

    /*
     * Listener which is called when a PGP keyring is found
     */
    private PGPPublicKeyRingListener publicKeyRingListener;

    /*
     * The System property for fully stripping the PGP headers
     */
    private static final String FULL_STRIP_HEADERS_PROPERTY =
            "mitm.common.security.openpgp.pgpinlinehandler.fullstripheaders";

    /*
     * If set, PGP header lines and Base64 body of PGP encrypted messages are fully stripped of spaces
     */
    private static boolean fullStripHeaders = false;

    /*
     * if mitm.common.security.openpgp.pgpinlinehandler.fullstripheaders system property is set, use it for the
     * fullStripHeaders value
     */
    static {
        String value = System.getProperty(FULL_STRIP_HEADERS_PROPERTY);

        if (value != null) {
            fullStripHeaders = Boolean.parseBoolean(value);
        }
    }

    public PGPInlineHandler(@Nonnull PGPKeyPairProvider keyPairProvider) {
        this.keyPairProvider = Objects.requireNonNull(keyPairProvider);
    }

    private PGPHandler createPGPHandler()
    {
        PGPHandler handler = new PGPHandler(keyPairProvider);

        handler.setDecrypt(decrypt);
        handler.setDecompressionUpperlimit(decompressionUpperlimit);
        handler.setMaxObjects(maxObjects);
        handler.setPublicKeyRingListener(publicKeyRingListener);

        return handler;
    }

    private String getCharsetFromArmor(@Nonnull PGPHandler handler)
    {
        String charset = null;

        // Check if there is a Charset in the armor of the first layer
        if (CollectionUtils.isNotEmpty(handler.getPGPLayers()))
        {
            PGPLayer layer = handler.getPGPLayers().get(0);

            if (CollectionUtils.isNotEmpty(layer.getParts()))
            {
                PGPLayerPart part = layer.getParts().get(0);

                String[] armorHeaders = part.getArmorHeaders();

                if (armorHeaders != null)
                {
                    for (String header : armorHeaders)
                    {
                        if (header == null) {
                            continue;
                        }

                        String[] nameValue = StringUtils.split(header, ":", 2);

                        if (nameValue.length != 2) {
                            continue;
                        }

                        if ("charset".equalsIgnoreCase(StringUtils.trim(nameValue[0])))
                        {
                            charset = StringUtils.trim(nameValue[1]);

                            break;
                        }
                    }
                }
            }
        }

        return charset;
    }

    private static String stripPGPHeader(String header)
    {
        // GPG4win (2.2.1) adds spaces to the end of the PGP header so we will trim the end before comparing.
        //
        // https://jira.djigzo.com/browse/GATEWAY-91 requests that the full headers are stripped. This is however
        // not RFC 4880 compliant page 55: "The header lines, therefore, MUST start at the beginning of a line, and
        // MUST NOT have text other than whitespace following them on the same line". This option is therefore not
        // enabled by default
        return fullStripHeaders ? StringUtils.strip(header, null) : StringUtils.stripEnd(header, null);
    }

    public boolean handle(@Nonnull InputStream input, @Nonnull MutableString charset, @Nonnull OutputStream output)
    throws IOException, PGPException
    {
        boolean detected = false;

        // Make sure all lines end with CR/LF
        InputStream crlfInput = new CRLFInputStream(input);

        Reader reader = new BufferedReader(new InputStreamReader(crlfInput,
                charset.getValue() != null ? charset.getValue() : CharEncoding.UTF_8));

        LineIterator li = new LineIterator(reader);

        while (li.hasNext())
        {
            String line = li.next();

            String header = stripPGPHeader(line);

            if (header.equals(BEGIN_PGP_MESSAGE_HEADER))
            {
                FilterInputStream filteredInput = new FilterInputStream(li, END_PGP_MESSAGE_HEADER, charset.getValue(),
                        header);

                handler = createPGPHandler();

                handlers.add(handler);

                ByteArrayOutputStream bodyStream = new ByteArrayOutputStream();

                handler.handle(filteredInput, bodyStream);

                // Check whether the armor headers contain the charset
                String bodyCharset = getCharsetFromArmor(handler);

                // Assume the body was encoded with fromCharSet if the charset was not set in the armor. If from
                // charset is not set, assume it's UTF-8
                if (bodyCharset == null) {
                    bodyCharset = (charset.getValue() != null ? charset.getValue() : CharEncoding.UTF_8);
                }

                // Convert to a Java charset
                String javaBodyCharset = MimeUtility.javaCharset(bodyCharset);

                if (!Charset.isSupported(javaBodyCharset)) {
                    // The charset was not found. Revert to UTF-8
                    logger.warn("Charset {} is not a supported charset. Reverting to UTF-8", javaBodyCharset);

                    javaBodyCharset = CharEncoding.UTF_8;
                }

                charset.setValue(javaBodyCharset);

                IOUtils.write(bodyStream.toString(charset.getValue()), output, charset.getValue());

                detected = true;
                changed = true;
            }
            else if (header.equals(BEGIN_PGP_SIGNED_MESSAGE_HEADER))
            {
                // Closing the input should not close the underlying reader
                InputStream partInputStream = CloseShieldInputStream.wrap(ReaderInputStream.builder()
                        .setReader(reader)
                        .setCharset(charset.getValue()).get());

                // If we keep the signature, we still need to write the content since this part might start
                // with some other non PGP text
                TeeInputStream teeInput = new TeeInputStream(new DelegateInputStream(
                        IOUtils.toInputStream(BEGIN_PGP_SIGNED_MESSAGE_HEADER + "\r\n", StandardCharsets.US_ASCII),
                        partInputStream), removeSignature ? NullOutputStream.INSTANCE : output);

                ArmoredInputStream armor = new ArmoredInputStream(teeInput);

                CRLFInputStream crlfArmored = new CRLFInputStream(armor);

                ByteArrayOutputStream signed = new ByteArrayOutputStream();

                OutputStream partOutput = removeSignature ? output : NullOutputStream.INSTANCE;

                try {
                    int ch;

                    ByteArrayOutputStream lineOutput = new ByteArrayOutputStream();

                    boolean firstLine = true;

                    while ((ch = crlfArmored.read()) >= 0 && armor.isClearText())
                    {
                        partOutput.write(ch);

                        // Trailing whitespace must be removed for signature calculation (see RFC 4880 7.1).
                        lineOutput.write(ch);

                        if (ch == '\n')
                        {
                            byte[] lineBuf = lineOutput.toByteArray();

                            // Write CR/LF but not for the first line
                            if (!firstLine) {
                                signed.write(CRLF);
                            }

                            firstLine = false;

                            signed.write(lineBuf, 0, PGPUtils.getLengthWithoutTrailingWhitespace(lineBuf));

                            lineOutput = new ByteArrayOutputStream();
                        }
                    }

                    if (lineOutput.size() > 0)
                    {
                        // There was some data which was not written
                        byte[] lineBuf = lineOutput.toByteArray();

                        signed.write(lineBuf, 0, PGPUtils.getLengthWithoutTrailingWhitespace(lineBuf));
                    }

                    handler = createPGPHandler();

                    handlers.add(handler);

                    DirectAccessByteArrayOutputStream signature = new DirectAccessByteArrayOutputStream();

                    // Read the binary signature
                    while ((ch = armor.read()) >= 0) {
                        signature.write(ch);
                    }

                    handler.handle(signature.getInputStream(), partOutput);

                    signature.close();

                    // There might be some data after the signature. If so, read it as mixed content
                    while ((ch = partInputStream.read()) >= 0)
                    {
                        if (!Character.isWhitespace(ch)) {
                            mixedContent = true;
                        }

                        partOutput.write(ch);
                    }
                    PGPLiteralLayerPart literalLayerPart = new PGPLiteralLayerPartImpl(armor.getArmorHeaders(), null,
                            signed);

                    handler.getPGPLayers().get(handler.getPGPLayers().size() - 1).addPart(0, literalLayerPart);
                }
                finally {
                    IOUtils.closeQuietly(crlfArmored);
                }

                detected = true;

                if (removeSignature) {
                    changed = true;
                }
            }
            else {
                // If there is something other than whitespace outside a PGP block, we have mixed content
                if (StringUtils.isNotBlank(line)) {
                    mixedContent = true;
                }

                output.write(line.getBytes(charset.getValue()));
                output.write(CRLF);
            }
        } // end while

        return detected;
    }

    public List<PGPLayer> getPGPLayers() {
        return handler != null ? handler.getPGPLayers() : null;
    }

    /**
     * True if there is extra data other the empty lines before or after the PGP part
     */
    public boolean isMixedContent() {
        return mixedContent;
    }

    /**
     * True if the handled message has been changed (for example decrypted or signature removed)
     */
    public boolean isChanged() {
        return changed;
    }

    public int getDecompressionUpperlimit() {
        return decompressionUpperlimit;
    }

    public void setDecompressionUpperlimit(int decompressionUpperlimit) {
        this.decompressionUpperlimit = decompressionUpperlimit;
    }

    public int getMaxObjects() {
        return maxObjects;
    }

    public void setMaxObjects(int maxObjects) {
        this.maxObjects = maxObjects;
    }

    public boolean isDecrypt() {
        return decrypt;
    }

    public void setDecrypt(boolean decrypt) {
        this.decrypt = decrypt;
    }

    public boolean isRemoveSignature() {
        return removeSignature;
    }

    public void setRemoveSignature(boolean removeSignature) {
        this.removeSignature = removeSignature;
    }

    public List<PGPHandler> getHandlers() {
        return handlers;
    }

    public PGPPublicKeyRingListener getPublicKeyRingListener() {
        return publicKeyRingListener;
    }

    public void setPublicKeyRingListener(PGPPublicKeyRingListener publicKeyRingListener) {
        this.publicKeyRingListener = publicKeyRingListener;
    }

    public static boolean isFullStripHeaders() {
        return fullStripHeaders;
    }

    public static void setFullStripHeaders(boolean fullStripHeaders) {
        PGPInlineHandler.fullStripHeaders = fullStripHeaders;
    }

    static class FilterInputStream extends InputStream
    {
        /*
         * Steps through the input line by line
         */
        private final LineIterator li;

        /*
         * The header that ends the PGP block
         */
        private final String endHeader;

        /*
         * The charset to use
         */
        private final String charset;

        /*
         * The data read
         */
        private byte[] buffer;

        /*
         * Current index into the line
         */
        private int index;

        /*
         * If true, the last line is read
         */
        private boolean endHeaderFound;

        FilterInputStream(@Nonnull LineIterator li, @Nonnull String endHeader, @Nonnull String charset,
                @Nonnull String initialLine)
        throws UnsupportedEncodingException
        {
            this.li = li;
            this.endHeader = endHeader;
            this.charset = charset;
            this.buffer = ArrayUtils.addAll(initialLine.getBytes(charset), CRLF);
        }

        @Override
        public int read()
        throws IOException
        {
            if (index >= buffer.length)
            {
                // We need to read a new line but only if we did not yet found the end header
                if (!endHeaderFound && li.hasNext())
                {
                    String line = li.next();

                    // We need to add CR/LF to the buffer
                    buffer = ArrayUtils.addAll(line.getBytes(charset), CRLF);

                    index = 0;

                    // GPG4win (2.2.1) adds spaces to the end of the PGP header so we will trim before comparing.
                    // Not sure whether we need to trim the whole line or just the end
                    if (endHeader.equals(stripPGPHeader(line))) {
                        endHeaderFound = true;
                    }
                }
                else {
                    return -1;
                }
            }

            return buffer[index++];
        }
    }
}
