/*
 * Copyright (c) 2013-2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPKeyConverter;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.Objects;

/**
 * Certificate implementation of a PGPPublicKey
 *
 * @author Martijn Brinkers
 *
 */
public class PGPCertificate extends Certificate
{
    public static final String TYPE = "PGP";

    /*
     * The PGP public stored in the certificate
     */
    private final PGPPublicKey pgpPublicKey;

    /*
     * The PublicKey from the PGP public key
     */
    private final PublicKey publicKey;

    public PGPCertificate(@Nonnull PGPPublicKey pgpPublicKey)
    throws PGPException
    {
        super(TYPE);

        this.pgpPublicKey = Objects.requireNonNull(pgpPublicKey);

        JcaPGPKeyConverter keyConverter = new JcaPGPKeyConverter();

        keyConverter.setProvider(PGPSecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());

        this.publicKey = keyConverter.getPublicKey(pgpPublicKey);
    }

    @Override
    public byte[] getEncoded()
    throws CertificateEncodingException
    {
        try {
            return pgpPublicKey.getEncoded();
        }
        catch (IOException e) {
            throw new CertificateEncodingException("Error encoding PGPPublicKey", e);
        }
    }

    @Override
    public void verify(PublicKey key)
    {
        // Not implemented
    }

    @Override
    public void verify(PublicKey key, String sigProvider)
    {
        // Not implemented
    }

    @Override
    public String toString() {
        return "PGP certificate. Key ID:" + pgpPublicKey.getKeyID();
    }

    @Override
    public PublicKey getPublicKey() {
        return publicKey;
    }

    public PGPPublicKey getPGPPublicKey() {
        return pgpPublicKey;
    }
}
