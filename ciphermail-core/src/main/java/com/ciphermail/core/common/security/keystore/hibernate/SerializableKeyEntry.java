/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore.hibernate;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.password.PBEncryption;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.annotation.Nonnull;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * SerializableKeyEntry is used to store private keys as a byte array. The keys are stored in a format that allows the
 * keys to be recreated from the byte array (should be obvious ;).
 *
 * WARNING: this class is used for long term serialization. The class should there not be moved or renamed. Be careful
 * with any changes made to class members, changes might result in changes to deserialization.
 *
 * @author Martijn Brinkers
 *
 */
@JsonAutoDetect(
    fieldVisibility = JsonAutoDetect.Visibility.ANY,
    setterVisibility = JsonAutoDetect.Visibility.NONE,
    getterVisibility = JsonAutoDetect.Visibility.NONE,
    isGetterVisibility = JsonAutoDetect.Visibility.NONE,
    creatorVisibility = JsonAutoDetect.Visibility.NONE
)
public class SerializableKeyEntry
{
    /*
     * The type of the key
     */
    private enum KeyType {PRIVATE, PUBLIC, SECRET}

    /*
     * If and how keys are protected
     */
    private enum Protection {NONE, ENCRYPTED}

    /*
     * The encoding of the key
     */
    private enum Format {PKCS8, X509, RAW}

    /*
     * Raw key material.
     */
    private byte[] rawKey;

    /*
     * The algorithm uses to create the key
     */
    private String algorithm;

    /*
     * The format of encoded key
     */
    private String format;

    /*
     * The type of the key
     */
    private KeyType keyType;

    /*
     * Stores how the key is protected
     */
    private Protection protection;

    /*
     * The security factory to use to create the key from the raw key material. If null, the default security factory
     * will be used.
     *
     * Note: the SecurityFactory should not be serialized
     */
    @JsonIgnore
    private SecurityFactory securityFactory;

    public static SerializableKeyEntry createInstance(@Nonnull Key key, char[] password, PBEncryption encryptor)
    throws GeneralSecurityException, IOException
    {
        return new SerializableKeyEntry(key, password, encryptor);
    }

    public static SerializableKeyEntry createInstance(@Nonnull Key key)
    throws GeneralSecurityException, IOException
    {
        return SerializableKeyEntry.createInstance(key, null, null);
    }

    protected SerializableKeyEntry() {
        // Jackson requires a default constructor
    }

    protected SerializableKeyEntry(@Nonnull Key key, char[] password, PBEncryption encryptor)
    throws GeneralSecurityException, IOException
    {
        if (encryptor == null || password == null)
        {
            this.rawKey = key.getEncoded();
            this.protection = Protection.NONE;
        }
        else {
            this.rawKey = encryptor.encrypt(key.getEncoded(), password);
            this.protection = Protection.ENCRYPTED;
        }

        this.algorithm = key.getAlgorithm();
        this.format = key.getFormat();

        if (key instanceof PrivateKey) {
            keyType = KeyType.PRIVATE;
        }
        else {
            if (key instanceof PublicKey) {
                keyType = KeyType.PUBLIC;
            }
            else {
                keyType = KeyType.SECRET;
            }
        }
    }

    /**
     * Deserialize the serialized SerializableKeyEntry object using the default system SecurityFactory
     *
     * @param serialized the serialized SerializableKeyEntry
     * @return a deserialized object
     * @throws UnrecoverableKeyException
     */
    public static SerializableKeyEntry deserialize(@Nonnull byte[] serialized)
    throws UnrecoverableKeyException, IOException
    {
        return deserialize(serialized, null);
    }

    /**
     * Deserialize the serialized SerializableKeyEntry object using the provided SecurityFactory
     *
     * @param serialized the serialized SerializableKeyEntry
     * @return a deserialized object
     * @throws UnrecoverableKeyException
     */
    public static SerializableKeyEntry deserialize(@Nonnull byte[] serialized,
            SecurityFactory securityFactory)
    throws UnrecoverableKeyException, IOException
    {
        Object o = JacksonUtil.getObjectMapper().readValue(serialized, SerializableKeyEntry.class);

        if (!(o instanceof SerializableKeyEntry keyEntry)) {
            throw new UnrecoverableKeyException("The serialized is not the correct type.");
        }

        keyEntry.setSecurityFactory(securityFactory);

        return keyEntry;
    }

    /**
     * Serializes this object
     *
     * @return serialized this
     */
    public @Nonnull byte[] serialize()
    throws JsonProcessingException
    {
        return JacksonUtil.getObjectMapper().writeValueAsBytes(this);
    }

    private Format toFormat(@Nonnull String format)
    throws KeyStoreException
    {
        if (format.equals("PKCS#8") || format.equals("PKCS8")) {
            return Format.PKCS8;
        }

        if (format.equals("X.509") || format.equals("X509")) {
            return Format.X509;
        }

        if (format.equals("RAW")) {
            return Format.RAW;
        }

        throw new KeyStoreException("Unknown key format " + format);
    }

    /**
     * Creates the key from the given byte array using the stored format and type.
     *
     * @param rawKey
     * @return the key
     * @throws KeyStoreException
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    private Key getKey(byte[] rawKey)
    throws GeneralSecurityException
    {
        KeySpec keySpec;

        Format keyFormat = toFormat(format);

        switch (keyFormat) {
            case PKCS8 -> keySpec = new PKCS8EncodedKeySpec(rawKey);
            case X509  -> keySpec = new X509EncodedKeySpec(rawKey);
            case RAW   -> {
                return new SecretKeySpec(rawKey, algorithm);
            }
            default -> throw new KeyStoreException("Unknown key format " + keyFormat);
        }

        return switch (keyType) {
            case PRIVATE -> getActiveSecurityFactory().createKeyFactory(algorithm).generatePrivate(keySpec);
            case PUBLIC  -> getActiveSecurityFactory().createKeyFactory(algorithm).generatePublic(keySpec);
            case SECRET  -> getActiveSecurityFactory().createSecretKeyFactory(algorithm).generateSecret(keySpec);
            default -> throw new KeyStoreException("Unknown key type " + keyType);
        };
    }

    /**
     * Returns an unprotected key. If the key is a protected key (ie. Protection != NONE) a KeyStoreException
     * will be thrown.
     *
     * @return the key
     * @throws GeneralSecurityException
     */
    public @Nonnull Key getKey()
    throws GeneralSecurityException
    {
        if (protection != Protection.NONE) {
            throw new KeyStoreException("This entry is protected.");
        }

        return getKey(rawKey);
    }

    /**
     * Returns a password protected key.
     *
     * @param password the password used to decrypt the key.
     * @param decryptor the decryptor used to decrypt the key. If null it is assumed that the
     * key is not password protected
     *
     * @return the key
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public @Nonnull Key getKey(char[] password, PBEncryption decryptor)
    throws GeneralSecurityException, IOException
    {
        if (protection !=  Protection.ENCRYPTED) {
            throw new KeyStoreException("This entry is not an encrypted entry.");
        }

        if (decryptor == null) {
            throw new KeyStoreException("The decryptor should not be null.");
        }

        byte[] decryptedKey = decryptor.decrypt(rawKey, password);

        return getKey(decryptedKey);
    }

    private @Nonnull SecurityFactory getActiveSecurityFactory() {
        return securityFactory != null ? securityFactory : SecurityFactoryFactory.getSecurityFactory();
    }

    public void setSecurityFactory(SecurityFactory securityFactory) {
        this.securityFactory = securityFactory;
    }

    public SecurityFactory getSecurityFactory() {
        return securityFactory;
    }
}