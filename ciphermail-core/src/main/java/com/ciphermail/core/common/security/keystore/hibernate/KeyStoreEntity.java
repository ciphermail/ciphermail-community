/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore.hibernate;

import com.ciphermail.core.common.hibernate.CertificateUserType;
import com.ciphermail.core.common.util.SizeUtils;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.CompositeType;
import org.hibernate.annotations.UuidGenerator;

import java.security.cert.Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity(name = KeyStoreEntity.ENTITY_NAME)
@Table(
uniqueConstraints = {@UniqueConstraint(columnNames = {KeyStoreEntity.STORE_NAME_COLUMN,
        KeyStoreEntity.ALIAS_COLUMN})},
indexes = {
    @Index(name = "keystore_storename_index", columnList = KeyStoreEntity.STORE_NAME_COLUMN),
    @Index(name = "keystore_alias_index",     columnList = KeyStoreEntity.ALIAS_COLUMN)
}
)
public class KeyStoreEntity
{
    public static final String ENTITY_NAME = "KeyStore";

    /**
     * Name of columns which are referenced from other classes
     */
    public static final String STORE_NAME_COLUMN = "storeName";
    public static final String ALIAS_COLUMN = "alias";
    public static final String THUMBPRINT_COLUMN = CertificateUserType.THUMBPRINT_COLUMN_NAME;

    /*
     * Name of the CertificateUserType
     */
    public static final String CERTIFICATE_USER_TYPE_NAME = "certificate";

    /*
     * 256K for a certificate should be more than plenty
     */
    private static final int MAX_CERTIFICATE_SIZE = SizeUtils.KB * 256;

    @Id
    @Column(name = "id")
    @UuidGenerator
    private UUID id;

    /*
     * The name of the store this entry belongs to.
     */
    @Column (name = STORE_NAME_COLUMN, length = 255, unique = false, nullable = false)
    private String storeName;

    /*
     * The certificate associated with this entry.
     */
    @Embedded
    @AttributeOverride(name = CertificateUserType.CERTIFICATE_COLUMN_NAME, column = @Column(length = MAX_CERTIFICATE_SIZE))
    @AttributeOverride(name = CertificateUserType.CERTIFICATE_TYPE_COLUMN_NAME, column = @Column(length = 255))
    @AttributeOverride(name = CertificateUserType.THUMBPRINT_COLUMN_NAME, column = @Column(length = 255, unique = false))
    @CompositeType(CertificateUserType.class)
    private Certificate certificate;

    /*
     * The associated certificate chain.
     */
    @ElementCollection
    @AttributeOverride(name = CertificateUserType.CERTIFICATE_COLUMN_NAME, column = @Column(length = MAX_CERTIFICATE_SIZE))
    @AttributeOverride(name = CertificateUserType.CERTIFICATE_TYPE_COLUMN_NAME, column = @Column(length = 255))
    @AttributeOverride(name = CertificateUserType.THUMBPRINT_COLUMN_NAME, column = @Column(length = 255, unique = false))
    @CompositeType(CertificateUserType.class)
    private List<Certificate> certificateChain = Collections.emptyList();

    /*
     * The encoded key material.
     */
    @Column (name = "encodedKey", length = SizeUtils.MB, unique = false, nullable = true)
    private byte[] encodedKey;

    /*
     * The alias of this key entry.
     */
    @Column (name = ALIAS_COLUMN, length = SizeUtils.KB, unique = false, nullable = false)
    private String alias;

    /*
     * Date this entry was created
     */
    @Column (name = "creationDate", unique = false, nullable = true)
    private Date creationDate;

    public KeyStoreEntity(String storeName, Certificate certificate, Certificate[] certificateChain,
            byte[] encodedKey, String alias, Date creationDate)
    {
        this.storeName = storeName;
        this.certificate = certificate;
        this.certificateChain = certificateChain != null ? List.of(certificateChain) : Collections.emptyList();
        this.encodedKey = encodedKey;
        this.alias = alias;
        this.creationDate = creationDate;
    }

    public KeyStoreEntity(String storeName, String alias, Date creationDate)
    {
        this.storeName = storeName;
        this.alias = alias;
        this.creationDate = creationDate;
    }

    protected KeyStoreEntity() {
        // Hibernate requires a default constructor
    }

    public String getStoreName() {
        return storeName;
    }

    /**
     * Returns the certificate associated with this entry
     * @return the certificate
     */
    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    /**
     * Returns the certificate chain associated with this entry.
     * @return the certificate chain
     */
    public Certificate[] getChain() {
        return certificateChain == null || certificateChain.isEmpty() ? null : certificateChain.toArray(new Certificate[0]);
    }

    public void setChain(Certificate[] chain) {
        this.certificateChain = chain != null ? List.of(chain) : Collections.emptyList();
    }

    /**
     * Returns the raw (encoded) key material.
     * @return the encoded key
     */
    public byte[] getEncodedKey() {
        return encodedKey;
    }

    public void setEncodedKey(byte[] encodedKey) {
        this.encodedKey = encodedKey;
    }

    /**
     * Returns the alias of this entry.
     * @return the alias of this entry
     */
    public String getKeyAlias() {
        return alias;
    }

    /**
     * The date this entry was created.
     *
     * @return The date this entry was created.
     */
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date date) {
        this.creationDate = date;
    }

    /**
     * KeyStoreEntity is equal if and only if the alias and storeName are equal
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof KeyStoreEntity rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(alias, rhs.alias)
            .append(storeName, rhs.storeName)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(alias)
            .append(storeName)
            .toHashCode();
    }
}
