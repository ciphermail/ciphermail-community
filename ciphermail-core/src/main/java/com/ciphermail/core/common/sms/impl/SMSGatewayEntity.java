/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.impl;

import com.ciphermail.core.common.sms.SMS;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.SizeUtils;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import org.hibernate.annotations.UuidGenerator;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * The database entity for the SMS gateway.
 *
 * @author Martijn Brinkers
 *
 */
@Entity(name = SMSGatewayEntity.ENTITY_NAME)
@Table(
indexes = {
    @Index(name = "sms_datelasttry_index", columnList = SMSGatewayEntity.DATE_LAST_TRY_COLUMN_NAME)
})
public class SMSGatewayEntity implements SMS
{
    static final String ENTITY_NAME = "SMS";

    static final String ID_COLUMN_NAME = "id";
    static final String PHONENUMBER_COLUMN_NAME = "phoneNumber";
    static final String MESSAGE_COLUMN_NAME = "message";
    static final String DATE_CREATED_COLUMN_NAME = "dateCreated";
    static final String DATE_LAST_TRY_COLUMN_NAME = "dateLastTry";
    static final String DATA_COLUMN_NAME = "data";
    static final String LAST_ERROR_COLUMN_NAME = "lastError";

    /*
     * Names of JSON keys stored in the data object as JSON
     */
    private static final String TAG_JSON_KEY = "tag";

    @Id
    @Column(name = ID_COLUMN_NAME)
    @UuidGenerator
    private UUID id;

    @Column (name = PHONENUMBER_COLUMN_NAME, length = 255, unique = false, nullable = false)
    private String phoneNumber;

    @Column (name = MESSAGE_COLUMN_NAME, length = SizeUtils.KB * 2, unique = false, nullable = false)
    private String message;

    @Column (name = DATE_CREATED_COLUMN_NAME, unique = false, nullable = true)
    private Date dateCreated;

    @Column (name = DATE_LAST_TRY_COLUMN_NAME, unique = false, nullable = true)
    private Date dateLastTry;

    @Column (name = DATA_COLUMN_NAME, length = SizeUtils.MB * 32, unique = false, nullable = true)
    private byte[] data;

    @Column (name = LAST_ERROR_COLUMN_NAME, length = SizeUtils.KB, unique = false, nullable = true)
    private String lastError;

    protected SMSGatewayEntity() {
        // required by Hibernate
    }

    SMSGatewayEntity(String phoneNumber, String message, Date dateCreated)
    {
        this.phoneNumber = Objects.requireNonNull(phoneNumber);
        this.message = Objects.requireNonNull(message);
        this.dateCreated = Objects.requireNonNull(dateCreated);
    }

    @Override
    public UUID getID() {
        return id;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Date getDateCreated() {
        return dateCreated;
    }

    @Override
    public Date getDateLastTry() {
        return dateLastTry;
    }

    public void setDateLastTry(Date dateLastTry) {
        this.dateLastTry = dateLastTry;
    }

    @Override
    public String getLastError() {
        return lastError;
    }

    public void setLastError(String lastError) {
        this.lastError = lastError;
    }

    private JSONObject getJSONData()
    throws JSONException
    {
        return data != null ? new JSONObject(MiscStringUtils.toStringFromUTF8Bytes(data)) : new JSONObject();
    }

    private void setJSONData(JSONObject json) {
        data = json != null ? MiscStringUtils.getBytesUTF8(json.toString()) : null;
    }

    public void setTag(String tag)
    throws JSONException
    {
        JSONObject json = getJSONData();

        json.put(TAG_JSON_KEY, tag);

        setJSONData(json);
    }

    public String getTag()
    throws JSONException
    {
        return getJSONData().optString(TAG_JSON_KEY);
    }
}
