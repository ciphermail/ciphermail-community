/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp;

import org.apache.commons.lang.text.StrBuilder;

/**
 * Information concerning a policy violation
 */
public class PolicyViolation
{
    /*
     * The name of the policy that was violated
     */
    private String policy;

    /*
     * The name of the rule that was violated
     */
    private String rule;

    /*
     * The part that matched the rule (i.e. the part that violated the policy)
     */
    private String match;

    /*
     * The priority associated with this violation.
     */
    private PolicyViolationAction priority;

    /*
     * If true, the evaluation is delayed when the policy is violated. What delayed evaluation actually means is not
     * defined. It's up to the caller to decided what it means. For example, it can mean that the policy is not violated
     * if the message has been encrypted.
     */
    private boolean delayEvaluation;

    // Required for Jackson
    private PolicyViolation() {
        // empty on purpose
    }

    public PolicyViolation(String policy, String rule, String match, PolicyViolationAction priority,
        boolean delayEvaluation)
    {
        this.policy = policy;
        this.rule = rule;
        this.match = match;
        this.priority = priority;
        this.delayEvaluation = delayEvaluation;
    }

    public String getPolicy() {
        return policy;
    }

    public String getRule() {
        return rule;
    }

    public String getMatch() {
        return match;
    }

    public PolicyViolationAction getPriority() {
        return priority;
    }

    public boolean isDelayEvaluation() {
        return delayEvaluation;
    }

    @Override
    public String toString()
    {
        StrBuilder sb = new StrBuilder(256);

        /*
         * Provide a string representation of the policy failure.
         *
         * Note: not all values need to be shown. Only values relevant for end users. For example delayEvaluation is
         * not something end users are interested in
         */
        sb.append("Policy: ").append(policy).
           append(", Rule: ").append(rule).
           append(", Priority: ").append(priority).
           append(", Match: ").append(match);

        return sb.toString();
    }
}
