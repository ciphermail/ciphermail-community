/*
 * Copyright (c) 2014-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.PartScanner;
import com.ciphermail.core.common.mail.PartScanner.PartListener;
import com.ciphermail.core.common.util.SizeLimitedInputStream;
import org.bouncycastle.openpgp.PGPMarker;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.io.InputStream;
import java.util.Objects;

/**
 * Scans a message for attached PGP keyrings and extracts the PGP keyrings. The extracted keyring is replaced by
 * a text that indicates that the keys were extracted
 *
 * @author Martijn Brinkers
 *
 */
public class PGPPublicKeyRingExtractor
{
    private static final Logger logger = LoggerFactory.getLogger(PGPPublicKeyRingExtractor.class);

    /*
     * maximum recursive depth for MIME parts.
     */
    private int maxMimeDepth = 8;

    /*
     * The maximum number of PGP objects. If the number of PGP objects read exceeds the maximum, a PGP
     * exception will be thrown.
     */
    private int maxObjects = 64;

    /*
     * maximum key size in bytes. If the key is larger then the max, the key will not be extracted
     */
    private int maxKeySize = Integer.MAX_VALUE;

    /*
     * If true, the keys will be replaced with the replacement text
     */
    private boolean removeKeys;

    /*
     * The text which will be used for the replaced keys (if removeKeys is true)
     */
    private String replacementText = "*** Attached PGP keys have been removed ***";

    /*
     * Listener called for each MIME part in the message
     */
    private final PartListener partListener = (parent, part, context) ->
            PGPPublicKeyRingExtractor.this.onPart(part, context);

    /*
     * Context used by PartScanner
     */
    private static class PartScannerContext
    {
        /*
         * True if the message contained PGP and was changed
         */
        private boolean changed;

        public boolean isChanged() {
            return changed;
        }

        public void setChanged(boolean changed) {
            this.changed = changed;
        }
    }

    /*
     * Even handler called for every extracted keyring
     */
    private final PGPPublicKeyRingListener keyRingListener;

    public PGPPublicKeyRingExtractor(@Nonnull PGPPublicKeyRingListener keyRingListener) {
        this.keyRingListener = Objects.requireNonNull(keyRingListener);
    }

    public boolean extract(MimeMessage message)
    throws MessagingException
    {
        boolean changed = false;

        PartScanner partScanner = new PartScanner(partListener, maxMimeDepth);

        partScanner.setExceptionOnMaxDepthReached(false);

        PartScannerContext context = new PartScannerContext();

        try {
            partScanner.scanPart(message, context);
        }
        catch (Exception e) {
            // Log and ignore
            logger.error("Error scanning message", e);
        }

        if (context.isChanged())
        {
            message.saveChanges();

            changed = true;
        }

        return changed;
    }

    private boolean onPart(Part part, Object context)
    {
        try {
            if (part.isMimeType(PGPUtils.PGP_PUBLIC_KEYS_MIME_TYPE))
            {
                InputStream decoderStream = PGPUtil.getDecoderStream(new SizeLimitedInputStream(
                        part.getInputStream(), maxKeySize));

                PGPObjectFactory objectFactory = new JcaPGPObjectFactory(decoderStream);

                int nrOfObjectsRead = 0;

                Object streamObject;

                boolean keysFound = false;

                while ((streamObject = objectFactory.nextObject()) != null)
                {
                    logger.debug("next PGP object: {}", streamObject.getClass());

                    nrOfObjectsRead++;

                    if (nrOfObjectsRead > maxObjects)
                    {
                        logger.warn("Maximum number of PGP objects read.");

                        break;
                    }

                    if (streamObject instanceof PGPMarker) {
                        // Ignore markers
                        continue;
                    }
                    else if (streamObject instanceof PGPPublicKeyRing pgpPublicKeyRing)
                    {
                        keyRingListener.onPGPPublicKeyRing(pgpPublicKeyRing);

                        keysFound = true;
                    }
                    else {
                        logger.warn("Unsupported PGP object {}", streamObject.getClass());
                    }
                }

                if (keysFound && removeKeys)
                {
                    logger.info("Replacing PGP keys with replacement text");

                    part.setText(replacementText);
                    part.setDisposition(null);

                    ((PartScannerContext)context).setChanged(true);
                }
            }
        }
        catch (Exception e) {
            logger.error("Error handling part", e);
        }

        return true;
    }

    public int getMaxMimeDepth() {
        return maxMimeDepth;
    }

    public void setMaxMimeDepth(int maxMimeDepth) {
        this.maxMimeDepth = maxMimeDepth;
    }

    public int getMaxObjects() {
        return maxObjects;
    }

    public void setMaxObjects(int maxObjects) {
        this.maxObjects = maxObjects;
    }

    public int getMaxKeySize() {
        return maxKeySize;
    }

    public void setMaxKeySize(int maxKeySize) {
        this.maxKeySize = maxKeySize;
    }

    public boolean isRemoveKeys() {
        return removeKeys;
    }

    public void setRemoveKeys(boolean removeKeys) {
        this.removeKeys = removeKeys;
    }

    public String getReplacementText() {
        return replacementText;
    }

    public void setReplacementText(String replacementText) {
        this.replacementText = replacementText;
    }
}
