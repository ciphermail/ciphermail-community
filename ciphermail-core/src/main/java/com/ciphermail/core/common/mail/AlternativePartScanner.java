/*
 * Copyright (c) 2011-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import java.io.IOException;
import java.util.Objects;

/**
 * Extension of PartScanner that scans the HTML or text part when a multipart is an alternative part.
 *
 * Note this class is not thread safe
 *
 * @author Martijn Brinkers
 *
 */
public class AlternativePartScanner extends PartScanner
{
    /*
     * The alternative part type which should be returned
     */
    private final AlternativePart alternativePart;

    /*
     * True if the message contains an alternative part
     */
    private boolean alternativeFound;

    public AlternativePartScanner(PartListener partListener, int maxDepth, @Nonnull AlternativePart alternativePart)
    {
        super(partListener, maxDepth);

        this.alternativePart = Objects.requireNonNull(alternativePart);
    }

    @Override
    protected boolean scanMultipart(Part parent, @Nonnull Part part, int depth, Object context)
    throws MessagingException, IOException, PartException
    {
        boolean stop = false;

        if (part.isMimeType("multipart/alternative"))
        {
            Multipart multipart = (Multipart) part.getContent();

            Part altPart = null;

            int partCount = multipart.getCount();

            for (int i = 0; i < partCount; i++)
            {
                Part child = multipart.getBodyPart(i);

                if (child == null) {
                    continue;
                }

                if (alternativePart == AlternativePart.TEXT && child.isMimeType("text/plain"))
                {
                    altPart = child;
                    // done because we found the text alternative
                    break;
                }
                if (alternativePart == AlternativePart.HTML && child.isMimeType("text/html"))
                {
                    altPart = child;
                    // done because we found the html alternative
                    break;

                }
                else {
                    // Something else. Might be for example another multipart message
                    altPart = child;
                }
            }

            if (altPart != null)
            {
                alternativeFound = true;

                // recursively call scanPart
                stop = scanPart(part, altPart, depth, context);
            }
        }
        else {
            // Some other multipart
            stop = super.scanMultipart(parent, part, depth, context);
        }

        return stop;
    }

    public boolean isAlternativeFound() {
        return alternativeFound;
    }
}
