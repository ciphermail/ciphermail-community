/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.X500NameStyle;
import org.bouncycastle.asn1.x500.style.RFC4519Style;

import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Builder class for building X500Principal instances.
 */
public class X500PrincipalBuilder
{
    private X500PrincipalBuilder() {
        // use getInstance
    }

    public static X500PrincipalBuilder getInstance() {
        return new X500PrincipalBuilder();
    }

    /*
     * Wrapper for custom OID values
     */
    private static class CustomValue
    {
        CustomValue(ASN1ObjectIdentifier oid, String[] values)
        {
            this.oid = oid;
            this.values = values;
        }

        final ASN1ObjectIdentifier oid;
        final String[] values;
    }

    private String[] email;
    private String[] organisation;
    private String[] organisationalUnit;
    private String[] countryCode;
    private String[] state;
    private String[] locality;
    private String[] commonName;
    private String[] givenName;
    private String[] surname;

    /*
     * Encoding style to use
     */
    private X500NameStyle x500NameStyle = RFC4519Style.INSTANCE;

    /*
     * Custom OID values
     */
    private final List<CustomValue> customValues = new LinkedList<>();

    public X500PrincipalBuilder setEmail(List<String> email)
    {
        if (email != null)
        {
            this.email = new String[email.size()];

            setEmail(email.toArray(this.email));
        }

        return this;
    }

    public X500PrincipalBuilder setEmail(String... email)
    {
        this.email = email;

        return this;
    }

    public X500PrincipalBuilder setOrganisation(List<String> organisation)
    {
        if (organisation != null)
        {
            this.organisation = new String[organisation.size()];

            setOrganisation(organisation.toArray(this.organisation));
        }

        return this;
    }

    public X500PrincipalBuilder setOrganisation(String... organisation)
    {
        this.organisation = organisation;

        return this;
    }

    public X500PrincipalBuilder setOrganisationalUnit(List<String> organisationalUnit)
    {
        if (organisationalUnit != null)
        {
            this.organisationalUnit = new String[organisationalUnit.size()];

            setOrganisationalUnit(organisationalUnit.toArray(this.organisationalUnit));
        }

        return this;
    }

    public X500PrincipalBuilder setOrganisationalUnit(String... organisationalUnit)
    {
        this.organisationalUnit = organisationalUnit;

        return this;
    }

    public X500PrincipalBuilder setCountryCode(List<String> countryCode)
    {
        if (countryCode != null)
        {
            this.countryCode = new String[countryCode.size()];

            setCountryCode(countryCode.toArray(this.countryCode));
        }

        return this;
    }

    public X500PrincipalBuilder setCountryCode(String... countryCode)
    {
        this.countryCode = countryCode;

        return this;
    }

    public X500PrincipalBuilder setState(List<String> state)
    {
        if (state != null)
        {
            this.state = new String[state.size()];

            setState(state.toArray(this.state));
        }

        return this;
    }

    public X500PrincipalBuilder setState(String... state)
    {
        this.state = state;

        return this;
    }

    public X500PrincipalBuilder setLocality(List<String> locality)
    {
        if (locality != null)
        {
            this.locality = new String[locality.size()];

            setLocality(locality.toArray(this.locality));
        }

        return this;
    }

    public X500PrincipalBuilder setLocality(String... locality)
    {
        this.locality = locality;

        return this;
    }

    public X500PrincipalBuilder setCommonName(List<String> cn)
    {
        if (cn != null)
        {
            this.commonName = new String[cn.size()];

            setCommonName(cn.toArray(this.commonName));
        }

        return this;
    }

    public X500PrincipalBuilder setCommonName(String... cn)
    {
        this.commonName = cn;

        return this;
    }

    public X500PrincipalBuilder setGivenName(List<String> givenName)
    {
        if (givenName != null)
        {
            this.givenName = new String[givenName.size()];

            setGivenName(givenName.toArray(this.givenName));
        }

        return this;
    }

    public X500PrincipalBuilder setGivenName(String... givenName)
    {
        this.givenName = givenName;

        return this;
    }

    public X500PrincipalBuilder setSurname(List<String> surname)
    {
        if (surname != null)
        {
            this.surname = new String[surname.size()];

            setSurname(surname.toArray(this.surname));
        }

        return this;
    }

    public X500PrincipalBuilder setSurname(String... surname)
    {
        this.surname = surname;

        return this;
    }

    public X500PrincipalBuilder addOID(ASN1ObjectIdentifier oid, String... values)
    {
        if (values != null && values.length > 0) {
            customValues.add(new CustomValue(oid, values));
        }

        return this;
    }

    public X500PrincipalBuilder addOID(ASN1ObjectIdentifier oid, List<String> values)
    {
        if (values != null) {
            addOID(oid, values.toArray(new String[] {}));
        }

        return this;
    }

    private void addToNameBuilder(ASN1ObjectIdentifier oid, String[] values, X500NameBuilder nameBuilder)
    {
        if (values != null)
        {
            for (String value : values)
            {
                if (value == null) {
                    continue;
                }

                nameBuilder.addRDN(oid, value);
            }
        }
    }

    public X500PrincipalBuilder setX500NameStyle(X500NameStyle x500NameStyle)
    {
        this.x500NameStyle = x500NameStyle;

        return this;
    }

    /**
     * Builds the X500Principal with the specified elements
     *
     * Example DNs:
     *
     * CN=DOD CLASS 3 EMAIL CA-9, OU=PKI, OU=DoD, O=U.S. Government, C=US
     * CN=Thawte Personal Freemail Issuing CA, O=Thawte Consulting (Pty) Ltd., C=ZA
     * CN=Senter Certification Authority SubCA, OU=Certification Authority, O=Senter, L=Den Haag, ST=Zuid-Holland,
     *          C=NL, EMAILADDRESS=SenterCA@Senter.nl
     * CN=Intel Corporation Basic Enterprise Issuing CA 1, OU=Information Technology Enterprise Business Computing,
     *      O=Intel Corporation, L=Folsom, ST=CA, C=US, EMAILADDRESS=pki@intel.com
     *
     * NOTE: building a X500Principal can result in a X500Principal which is not equivalent to a X500Principal
     * generated by JDK (from a string). For example the email address is encoded differently. To create a
     * X500Principal compatible with JDK, re-encode the X500Principal
     *
     * Example:
     *
     * new X500Principal(X500PrincipalBuilder.buildPrincipal().toString())
     *
     *  TODO: SHOULD WE ALWAYS GENERATE A JDK EQUIVALENT X500Principal?
     */
    public X500Principal buildPrincipal()
    throws IOException
    {
        return X500PrincipalUtils.fromX500Name(buildName());
    }

    /**
     * Builds the X500Name with the specified elements
     *
     * Example DNs:
     *
     * CN=DOD CLASS 3 EMAIL CA-9, OU=PKI, OU=DoD, O=U.S. Government, C=US
     * CN=Thawte Personal Freemail Issuing CA, O=Thawte Consulting (Pty) Ltd., C=ZA
     * CN=Senter Certification Authority SubCA, OU=Certification Authority, O=Senter, L=Den Haag, ST=Zuid-Holland,
     *          C=NL, EMAILADDRESS=SenterCA@Senter.nl
     * CN=Intel Corporation Basic Enterprise Issuing CA 1, OU=Information Technology Enterprise Business Computing,
     *      O=Intel Corporation, L=Folsom, ST=CA, C=US, EMAILADDRESS=pki@intel.com
     *
     */
    public X500Name buildName()
    {
        X500NameBuilder nameBuilder = new X500NameBuilder(x500NameStyle);

        addToNameBuilder(RFC4519Style.c, countryCode, nameBuilder);
        addToNameBuilder(RFC4519Style.st, state, nameBuilder);
        addToNameBuilder(RFC4519Style.l, locality, nameBuilder);
        addToNameBuilder(RFC4519Style.o, organisation, nameBuilder);
        addToNameBuilder(RFC4519Style.ou, organisationalUnit, nameBuilder);
        addToNameBuilder(RFC4519Style.cn, commonName, nameBuilder);
        addToNameBuilder(RFC4519Style.sn, surname, nameBuilder);
        addToNameBuilder(RFC4519Style.givenName, givenName, nameBuilder);
        addToNameBuilder(PKCSObjectIdentifiers.pkcs_9_at_emailAddress, email, nameBuilder);

        for (CustomValue customValue : customValues) {
            addToNameBuilder(customValue.oid, customValue.values, nameBuilder);
        }

        return nameBuilder.build();
    }
}
