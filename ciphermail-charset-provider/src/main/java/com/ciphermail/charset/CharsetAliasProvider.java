/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.charset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.charset.spi.CharsetProvider;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Charset provider which can be used to create aliases for charset names
 *
 * @author Martijn Brinkers
 *
 */
public class CharsetAliasProvider extends CharsetProvider
{
    private static final Logger logger = Logger.getLogger(CharsetAliasProvider.class.getName());

    private static final String ALIAS_PROPERTIES_FILENAME = "ciphermail-charset-aliases.properties";

    private Map<String, Charset> charsets;

    /*
     * tries to load the resource by name. Returns null if the resource cannot be found.
     */
    private InputStream getResourceAsStream(String resourceName)
    throws FileNotFoundException
    {
        InputStream input = null;

        File file = null;

        String filePath = System.getProperty("ciphermail.charset-alias-provider.properties");

        if (filePath != null)
        {
            logger.log(Level.INFO, "CharsetAliasProvider properties file {}", filePath);

            file = new File(filePath);
        }

        if (file != null && file.exists())
        {
            logger.log(Level.INFO, "Resource found at: {}", file);

            input = new FileInputStream(file);
        }
        else {
            input = getClass().getResourceAsStream(resourceName);
        }

        return input;
    }

    private void init()
    throws IOException
    {
        logger.info("Initializing CharsetAliasProvider.");

        charsets = new HashMap<>();

        // try to load a file containing the mapping from alias charset to actual charset
        InputStream resource = getResourceAsStream(ALIAS_PROPERTIES_FILENAME);

        if (resource != null)
        {
            try {
                Properties properties = new Properties();

                properties.load(resource);

                Set<Entry<Object, Object>> entries = properties.entrySet();

                Iterator<Entry<Object, Object>> entryIt = entries.iterator();

                while (entryIt.hasNext())
                {
                    Entry<Object, Object> entry = entryIt.next();

                    if (entry != null)
                    {
                      String alias = (String) entry.getKey();
                      String charsetName = (String) entry.getValue();

                      if (alias == null || charsetName == null) {
                          continue;
                      }

                      alias = alias.trim().toUpperCase();
                      charsetName = charsetName.trim();

                      if (alias.length() == 0 || charsetName.length() == 0) {
                          continue;
                      }

                      logger.log(Level.INFO, "Alias: {}, charsetName: {} ", new String[]{alias, charsetName});

                      try {
                          charsets.put(alias, Charset.forName(charsetName));
                      }
                      catch(IllegalCharsetNameException e) {
                          logger.log(Level.WARNING, "IllegalCharsetNameException", e);
                      }
                      catch(UnsupportedCharsetException e) {
                          logger.log(Level.WARNING, "UnsupportedCharsetException", e);
                      }
                    }
                }
            }
            finally {
                resource.close();
            }
        }
        else {
            logger.log(Level.WARNING, "charset-aliases.properties not found.");
        }
    }

    @Override
    public synchronized Charset charsetForName(String charsetName)
    {
        if (charsets == null)
        {
            try {
                init();
            }
            catch (IOException e)
            {
                logger.log(Level.SEVERE, "IOException while initializing CharsetAliasProvider", e);

                throw new IllegalCharsetNameException("IOException while initializing CharsetAliasProvider");
            }
        }

        if (charsetName != null)
        {
            charsetName = charsetName.toUpperCase();

            Charset charset = charsets.get(charsetName);

            if (charset != null)
            {
                logger.log(Level.INFO, "Alias found for {}. Charset: {}",
                        new String[]{charsetName,charset.displayName()});
            }

            return charset;
        }

        return null;
    }

    @Override
    public Iterator<Charset> charsets() {
        return null;
    }
}
