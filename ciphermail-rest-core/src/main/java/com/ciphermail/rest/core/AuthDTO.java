/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.core;

import com.ciphermail.core.app.admin.AuthenticationType;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotNull;

import java.util.List;
import java.util.Set;

public class AuthDTO
{
    private AuthDTO() {
        // empty on purpose
    }

    public record CsrfTokenDetails(
            @NotNull String headerName,
            @NotNull String parameterName,
            @NotNull String token) {}

    public record RoleDetails(
            @NotNull String name,
            @NotNull Set<String> permissions,
            @NotNull List<RoleDetails> inheritedRoles) {}

    public record AdminDetails(
            @NotNull String name,
            @NotNull AuthenticationType authenticationType,
            @Nonnull boolean tfaEnabled,
            @NotNull List<String> ipAddresses,
            @NotNull List<AuthDTO.RoleDetails> roles) {}

    public record OAuth2UserInfoEndpoint(
            String uri,
            String authenticationMethod,
            String userNameAttributeName){}

    public record OAuth2ProviderDetails(
            String authorizationUri,
            String tokenUri,
            @NotNull OAuth2UserInfoEndpoint userInfoEndpoint,
            String jwkSetUri,
            String issuerUri){}

    public record OAuth2ClientRegistrationDetails(
            String loginRedirectUri,
            String registrationId,
            String clientId,
            String clientAuthenticationMethod,
            String authorizationGrantType,
            String redirectUri,
            @NotNull Set<String> scopes,
            String clientName,
            OAuth2ProviderDetails providerDetails){}

    public record ChangePassword(
            @NotNull String currentPassword,
            @NotNull String newPassword,
            @NotNull String newPasswordRepeat){}
}
