/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.core;

import com.ciphermail.core.common.security.certificate.ExtendedKeyUsageType;
import com.ciphermail.core.common.security.certificate.KeyUsageType;
import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import jakarta.validation.constraints.NotNull;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

public class CertificateDTO
{
    private CertificateDTO() {
        // empty on purpose
    }

    public record X509CertificateDetails(
            @NotNull String thumbprint,
            @NotNull String thumbprintSHA1,
            @NotNull String serialNumberHex,
            @NotNull String issuerFriendly,
            @NotNull String subjectFriendly,
            String subjectKeyIdentifier,
            Boolean selfSigned,
            Long notBefore,
            Long notAfter,
            Boolean expired,
            @NotNull List<String> emailFromAltNames,
            @NotNull List<String> emailFromDN,
            Set<KeyUsageType> keyUsage,
            Set<ExtendedKeyUsageType> extendedKeyUsage,
            Boolean ca,
            BigInteger pathLengthConstraint,
            Integer publicKeyLength,
            @NotNull String publicKeyAlgorithm,
            @NotNull String signatureAlgorithm,
            @NotNull Set<String> uriDistributionPointNames,
            String keyAlias,
            Boolean privateKeyAccessible,
            CTLEntryStatus ctlStatus){}

    public record CertificateReference(
            @NotNull String category,
            @NotNull String name) {}

    public record CertificateValidationResult(
            @NotNull boolean valid,
            @NotNull boolean trusted,
            @NotNull boolean revoked,
            @NotNull boolean blackListed,
            @NotNull boolean whiteListed,
            String failureMessage,
            @NotNull CertificateDTO.X509CertificateDetails certificate) {}

    public record IssuerWithStore(
            @NotNull CertificateDTO.X509CertificateDetails certificate,
            @NotNull CertificateStore store) {}

    public record ExportPrivateKeysRequestBody(
            @NotNull String keyStorePassword,
            @NotNull List<String> thumbprints){}
}
