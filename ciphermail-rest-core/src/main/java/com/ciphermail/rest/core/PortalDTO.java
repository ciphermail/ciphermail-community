package com.ciphermail.rest.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;

public class PortalDTO
{
    private PortalDTO() {
        // empty on purpose
    }

    public record PdfReplyParameters(
            @NotNull String from,
            @NotNull String sender,
            @NotNull String to,
            String cc,
            @NotNull String subject,
            @NotNull String replyTo,
            String messageID,
            boolean expired,
            boolean replyAllowed,
            long bodyMaxSize,
            long attachmentsMaxSize) {}

    public record PortalSignupValidationResult(
            @NotNull String email) {}

    public record PortalPasswordResetValidationResult(
            @NotNull String email) {}

    public record OTPRequest(
            @JsonProperty("a") String app,
            @JsonProperty("k") String keyID,
            @JsonProperty("p") String passwordID,
            @JsonProperty("l") int length){}

    public record MACProtectedEmail(
            @JsonProperty("e") String email,
            @JsonProperty("t") long timestamp,
            @JsonProperty("m") String mac){}

    public record PortalProperties(
            String encodedPassword,
            boolean loginEnabled,
            boolean tfaEnabled,
            String tfaSecret,
            String orginizationID) {}
}
