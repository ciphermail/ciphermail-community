/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.core;

/**
 * All the CipherMail REST paths
 */
@SuppressWarnings("java:S1075")
public class RestPaths
{
    private RestPaths() {
        // empty on purpose
    }

    public static final String ACME_CREATE_ACCOUNT_KEYPAIR_PATH = "/api/acme/createAccountKeyPair";
    public static final String ACME_ACCOUNT_KEYPAIR_EXISTS_PATH = "/api/acme/accountKeyPairExists";
    public static final String ACME_GET_TERMS_OF_SERVICE_PATH = "/api/acme/getTermsOfService";
    public static final String ACME_SET_TOS_ACCEPTED_PATH = "/api/acme/setTosAccepted";
    public static final String ACME_GET_TOS_ACCEPTED_PATH = "/api/acme/getTosAccepted";
    public static final String ACME_FIND_OR_REGISTER_ACCOUNT_PATH = "/api/acme/findOrRegisterAccount";
    public static final String ACME_DEACTIVATE_ACCOUNT_PATH = "/api/acme/deactivateAccount";
    public static final String ACME_ORDER_CERTIFICATE_PATH = "/api/acme/orderCertificate";
    public static final String ACME_RENEW_CERTIFICATE_PATH = "/api/acme/renewCertificate";
    public static final String ACME_GET_AUTHORIZATION_PATH = "/api/acme/getAuthorization/{token}";

    public static final String AUTH_GET_OAUTH2_CLIENTS_PATH = "/api/auth/getOAuth2Clients";
    public static final String AUTH_GET_AVAILABLE_PERMISSIONS_PATH = "/api/auth/getAvailablePermissions";
    public static final String AUTH_GET_ROLES_PATH = "/api/auth/getRoles";
    public static final String AUTH_GET_ROLE_PATH = "/api/auth/getRole";
    public static final String AUTH_GET_ADMINS_PATH = "/api/auth/getAdmins";
    public static final String AUTH_GET_ADMIN_PATH = "/api/auth/getAdmin";
    public static final String AUTH_ADD_ADMIN_PATH = "/api/auth/addAdmin";
    public static final String AUTH_DELETE_ADMIN_PATH = "/api/auth/deleteAdmin";
    public static final String AUTH_CREATE_ROLE_PATH = "/api/auth/createRole";
    public static final String AUTH_DELETE_ROLE_PATH = "/api/auth/deleteRole";
    public static final String AUTH_ADD_ROLE_PERMISSIONS_PATH = "/api/auth/addRolePermissions";
    public static final String AUTH_SET_ROLE_PERMISSIONS_PATH = "/api/auth/setRolePermissions";
    public static final String AUTH_REMOVE_ROLE_PERMISSIONS_PATH = "/api/auth/removeRolePermissions";
    public static final String AUTH_SET_PASSWORD_PATH = "/api/auth/setPassword";
    public static final String AUTH_CHANGE_PASSWORD_PATH = "/api/auth/changePassword";
    public static final String AUTH_SET_IP_ADDRESSES_PATH = "/api/auth/setIpAddresses";
    public static final String AUTH_SET_ROLES_PATH = "/api/auth/setRoles";
    public static final String AUTH_SET_INHERITED_ROLES_PATH = "/api/auth/setInheritedRoles";
    public static final String AUTH_GET_EFFECTIVE_PERMISSIONS_PATH = "/api/auth/getEffectivePermissions";
    public static final String AUTH_GET_AUTHENTICATED_USER_PATH = "/api/auth/getAuthenticatedUser";
    public static final String AUTH_VALIDATE_TOTP_PATH = "/api/auth/validateTOTP";
    public static final String AUTH_SET_2FA_ENABLED_PATH = "/api/auth/set2FAEnabled";
    public static final String AUTH_SET_2FA_SECRET_PATH = "/api/auth/set2FASecret";
    public static final String AUTH_GET_2FA_SECRET_PATH = "/api/auth/get2FASecret";
    public static final String AUTH_GET_CALLER_IS_2FA_SECRET_SET_PATH = "/api/auth/getCallerIs2FASecretSet";
    public static final String AUTH_GET_CALLER_2FA_SECRET_PATH = "/api/auth/getCaller2FASecret";
    public static final String AUTH_GET_CALLER_2FA_SECRET_QR_CODE_PATH = "/api/auth/getCaller2FASecretQRCode";
    public static final String AUTH_GET_CALLER_2FA_ENABLED_PATH = "/api/auth/getCaller2FAEnabled";
    public static final String AUTH_SET_CALLER_2FA_ENABLED_PATH = "/api/auth/setCaller2FAEnabled";
    public static final String AUTH_GET_CALLER_AUTHENTICATION_TYPE_PATH = "/api/auth/getCallerAuthenticationType";

    public static final String CA_CREATE_CA_PATH = "/api/ca/createCA";
    public static final String CA_GET_AVAILABLE_CAS_PATH = "/api/ca/getAvailableCAs";
    public static final String CA_GET_CERTIFICATE_REQUEST_HANDLERS_PATH = "/api/ca/getCertificateRequestHandlers";
    public static final String CA_REQUEST_NEW_CERTIFICATE_PATH = "/api/ca/requestNewCertificate";
    public static final String CA_GET_PENDING_REQUESTS_PATH = "/api/ca/getPendingRequests";
    public static final String CA_GET_PENDING_REQUESTS_COUNT_PATH = "/api/ca/getPendingRequestsCount";
    public static final String CA_GET_PENDING_REQUESTS_BY_EMAIL_PATH = "/api/ca/getPendingRequestsByEmail";
    public static final String CA_GET_PENDING_REQUESTS_BY_EMAIL_COUNT_PATH = "/api/ca/getPendingRequestsByEmailCount";
    public static final String CA_GET_PENDING_REQUEST_PATH = "/api/ca/getPendingRequest";
    public static final String CA_DELETE_PENDING_REQUEST_PATH = "/api/ca/deletePendingRequest";
    public static final String CA_DELETE_PENDING_REQUESTS_PATH = "/api/ca/deletePendingRequests";
    public static final String CA_RESCHEDULE_REQUEST_PATH = "/api/ca/rescheduleRequest";
    public static final String CA_RESCHEDULE_REQUESTS_PATH = "/api/ca/rescheduleRequests";
    public static final String CA_FINALIZE_PENDING_REQUEST_PATH = "/api/ca/finalizePendingRequest";
    public static final String CA_CREATE_SELF_SIGNED_CERTIFICATE_PATH = "/api/ca/createSelfSignedCertificate";
    public static final String CA_CREATE_CRL_PATH = "/api/ca/createCRL";

    public static final String CERTIFICATE_GET_CERTIFICATE_DETAILS_PATH = "/api/certificate/getCertificateDetails";
    public static final String CERTIFICATE_GET_CERTIFICATES_PATH = "/api/certificate/getCertificates";
    public static final String CERTIFICATE_GET_CERTIFICATES_COUNT_PATH = "/api/certificate/getCertificatesCount";
    public static final String CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_PATH = "/api/certificate/searchForCertificatesByEmail";
    public static final String CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_COUNT_PATH = "/api/certificate/searchForCertificatesByEmailCount";
    public static final String CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_PATH = "/api/certificate/searchForCertificatesBySubject";
    public static final String CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_COUNT_PATH = "/api/certificate/searchForCertificatesBySubjectCount";
    public static final String CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_PATH = "/api/certificate/searchForCertificatesByIssuer";
    public static final String CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_COUNT_PATH = "/api/certificate/searchForCertificatesByIssuerCount";
    public static final String CERTIFICATE_GET_CERTIFICATE_PATH = "/api/certificate/getCertificate";
    public static final String CERTIFICATE_IMPORT_CERTIFICATES_PATH = "/api/certificate/importCertificates";
    public static final String CERTIFICATE_IMPORT_CERTIFICATES_PEM_PATH = "/api/certificate/importCertificatesPEM";
    public static final String CERTIFICATE_IMPORT_CERTIFICATES_BASE64_PATH = "/api/certificate/importCertificatesBase64";
    public static final String CERTIFICATE_DELETE_CERTIFICATE_PATH = "/api/certificate/deleteCertificate";
    public static final String CERTIFICATE_DELETE_CERTIFICATES_PATH = "/api/certificate/deleteCertificates";
    public static final String CERTIFICATE_EXPORT_CERTIFICATE_PATH = "/api/certificate/exportCertificate";
    public static final String CERTIFICATE_EXPORT_CERTIFICATE_CHAIN_PATH = "/api/certificate/exportCertificateChain";
    public static final String CERTIFICATE_EXPORT_CERTIFICATES_PATH = "/api/certificate/exportCertificates";
    public static final String CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH = "/api/certificate/importPrivateKeys";
    public static final String CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH = "/api/certificate/exportPrivateKeys";
    public static final String CERTIFICATE_GET_CERTIFICATE_REFERENCED_DETAILS_PATH = "/api/certificate/getCertificateReferencedDetails";
    public static final String CERTIFICATE_IS_CERTIFICATE_REFERENCED_PATH = "/api/certificate/isCertificateReferenced";
    public static final String CERTIFICATE_IMPORT_SYSTEM_ROOTS_PATH = "/api/certificate/importSystemRoots";

    public static final String CERTIFICATE_SELECTION_GET_AUTOSELECTED_ENCRYPTION_CERTIFICATES_FOR_USER_PATH = "/api/certificate/selection/getAutoSelectedEncryptionCertificatesForUser";
    public static final String CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_USER_PATH = "/api/certificate/selection/getExplicitCertificatesForUser";
    public static final String CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_USER_PATH = "/api/certificate/selection/setExplicitCertificatesForUser";
    public static final String CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_USER_PATH = "/api/certificate/selection/getInheritedCertificatesForUser";
    public static final String CERTIFICATE_SELECTION_GET_ENCRYPTION_CERTIFICATES_FOR_USER_PATH = "/api/certificate/selection/getEncryptionCertificatesForUser";
    public static final String CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_USER_PATH = "/api/certificate/selection/getNamedCertificatesForUser";
    public static final String CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_USER_PATH = "/api/certificate/selection/setNamedCertificatesForUser";
    public static final String CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_USER_PATH = "/api/certificate/selection/getInheritedNamedCertificatesForUser";
    public static final String CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH = "/api/certificate/selection/getExplicitCertificatesForDomain";
    public static final String CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH = "/api/certificate/selection/setExplicitCertificatesForDomain";
    public static final String CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_DOMAIN_PATH = "/api/certificate/selection/getInheritedCertificatesForDomain";
    public static final String CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH = "/api/certificate/selection/getNamedCertificatesForDomain";
    public static final String CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH = "/api/certificate/selection/setNamedCertificatesForDomain";
    public static final String CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_DOMAIN_PATH = "/api/certificate/selection/getInheritedNamedCertificatesForDomain";
    public static final String CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_GLOBAL_PATH = "/api/certificate/selection/getExplicitCertificatesGlobal";
    public static final String CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_GLOBAL_PATH = "/api/certificate/selection/setExplicitCertificatesGlobal";
    public static final String CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_GLOBAL_PATH = "/api/certificate/selection/getNamedCertificatesGlobal";
    public static final String CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_GLOBAL_PATH = "/api/certificate/selection/setNamedCertificatesGlobal";
    public static final String CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_USER_PATH = "/api/certificate/selection/getSigningCertificateForUser";
    public static final String CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_USER_PATH = "/api/certificate/selection/setSigningCertificateForUser";
    public static final String CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH = "/api/certificate/selection/getSigningCertificateForDomain";
    public static final String CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH = "/api/certificate/selection/setSigningCertificateForDomain";
    public static final String CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH = "/api/certificate/selection/getSigningCertificateForGlobal";
    public static final String CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH = "/api/certificate/selection/setSigningCertificateForGlobal";

    public static final String CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_SIGNING_PATH = "/api/certificate/validation/validateCertificateForSigning";
    public static final String CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_SIGNING_PATH = "/api/certificate/validation/validateExternalCertificateForSigning";
    public static final String CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_ENCRYPTION_PATH = "/api/certificate/validation/validateCertificateForEncryption";
    public static final String CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_ENCRYPTION_PATH = "/api/certificate/validation/validateExternalCertificateForEncryption";
    public static final String CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_PATH = "/api/certificate/validation/validateCertificate";
    public static final String CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_PATH = "/api/certificate/validation/validateExternalCertificate";
    public static final String CERTIFICATE_VALIDATION_GET_ISSUER_CERTIFICATE_PATH = "/api/certificate/validation/getIssuerCertificate";

    public static final String CRL_IMPORT_CRLS_PATH = "/api/crl/importCRLs";
    public static final String CRL_GET_CRLS_PATH = "/api/crl/getCRLs";
    public static final String CRL_GET_CRL_PATH = "/api/crl/getCRL";
    public static final String CRL_GET_CRLS_COUNT_PATH = "/api/crl/getCRLsCount";
    public static final String CRL_DELETE_CRL_PATH = "/api/crl/deleteCRL";
    public static final String CRL_DELETE_CRLS_PATH = "/api/crl/deleteCRLs";
    public static final String CRL_EXPORT_CRLS_PATH = "/api/crl/exportCRLs";
    public static final String CRL_REFRESH_CRL_STORE_PATH = "/api/crl/refreshCRLStore";
    public static final String CRL_VALIDATE_CRL_PATH = "/api/crl/validateCRL";

    public static final String TLS_CREATE_CSR_PATH = "/api/tls/createCSR";
    public static final String TLS_GET_CSRS_PATH = "/api/tls/getCSRs";
    public static final String TLS_GET_CSR_PATH = "/api/tls/getCSR";
    public static final String TLS_DELETE_CSR_PATH = "/api/tls/deleteCSR";
    public static final String TLS_EXPORT_PKCS10_REQUEST_PATH = "/api/tls/exportPKCS10Request";
    public static final String TLS_IMPORT_CERTIFICATE_CHAIN_PATH = "/api/tls/importCertificateChain";
    public static final String TLS_EXPORT_CERTIFICATE_CHAIN_PATH = "/api/tls/exportCertificateChain";
    public static final String TLS_INSTALL_CERTIFICATE_PATH = "/api/tls/installCertificate";
    public static final String TLS_IMPORT_PKCS12_PATH = "/api/tls/importPKCS12";

    public static final String CTL_GET_CTLS_PATH = "/api/ctl/getCTLs";
    public static final String CTL_GET_CTLS_COUNT_PATH = "/api/ctl/getCTLsCount";
    public static final String CTL_GET_CTL_PATH = "/api/ctl/getCTL";
    public static final String CTL_SET_CTL_PATH = "/api/ctl/setCTL";
    public static final String CTL_SET_CTLS_PATH = "/api/ctl/setCTLs";
    public static final String CTL_DELETE_CTL_PATH = "/api/ctl/deleteCTL";
    public static final String CTL_DELETE_CTLS_PATH = "/api/ctl/deleteCTLs";

    public static final String DKIM_GENERATE_KEY_PATH = "/api/dkim/generateKey";
    public static final String DKIM_GET_KEY_IDS_PATH = "/api/dkim/getkeyIds";
    public static final String DKIM_SET_KEY_PATH = "/api/dkim/setKey";
    public static final String DKIM_GET_PUBLIC_KEY_PATH = "/api/dkim/getPublicKey";
    public static final String DKIM_GET_KEY_PAIR_PATH = "/api/dkim/getKeyPair";
    public static final String DKIM_IS_KEY_AVAILABLE_PATH = "/api/dkim/isKeyAvailable";
    public static final String DKIM_DELETE_KEY_PATH = "/api/dkim/deleteKey";
    public static final String DKIM_PARSE_SIGNATURE_TEMPLATE_PATH = "/api/dkim/parseSignatureTemplate";

    public static final String DLP_ADD_POLICY_PATTERN_PATH = "/api/dlp/addPolicyPattern";
    public static final String DLP_ADD_POLICY_GROUP_PATH = "/api/dlp/addPolicyGroup";
    public static final String DLP_UPDATE_POLICY_PATTERN_PATH = "/api/dlp/updatePolicyPattern";
    public static final String DLP_GET_POLICY_PATTERN_PATH = "/api/dlp/getPolicyPattern";
    public static final String DLP_GET_POLICY_PATTERNS_BY_NAME_PATH = "/api/dlp/getPolicyPatternsByName";
    public static final String DLP_DELETE_POLICY_PATTERN_PATH = "/api/dlp/deletePolicyPattern";
    public static final String DLP_DELETE_POLICY_PATTERNS_PATH = "/api/dlp/deletePolicyPatterns";
    public static final String DLP_GET_POLICY_PATTERNS_PATH = "/api/dlp/getPolicyPatterns";
    public static final String DLP_GET_POLICY_PATTERNS_COUNT_PATH = "/api/dlp/getPolicyPatternsCount";
    public static final String DLP_RENAME_POLICY_PATTERN_PATH = "/api/dlp/renamePolicyPattern";
    public static final String DLP_IS_REFERENCED_PATH = "/api/dlp/isReferenced";
    public static final String DLP_GET_REFERENCED_DETAILS_PATH = "/api/dlp/getReferencedDetails";
    public static final String DLP_SET_CHILD_NODES_PATH = "/api/dlp/setChildNodes";
    public static final String DLP_ADD_CHILD_NODE_PATH = "/api/dlp/addChildNode";
    public static final String DLP_REMOVE_CHILD_NODE_PATH = "/api/dlp/removeChildNode";
    public static final String DLP_EXTRACT_TEXT_FROM_EMAIL_PATH = "/api/dlp/extractTextFromEmail";
    public static final String DLP_GET_MATCH_FILTERS_PATH = "/api/dlp/getMatchFilters";
    public static final String DLP_GET_MATCH_FILTER_PATH = "/api/dlp/getMatchFilter";
    public static final String DLP_GET_VALIDATORS_PATH = "/api/dlp/getValidators";
    public static final String DLP_GET_VALIDATOR_PATH = "/api/dlp/getValidator";
    public static final String DLP_GET_SKIP_LIST_PATH = "/api/dlp/getSkipList";
    public static final String DLP_SET_SKIP_LIST_PATH = "/api/dlp/setSkipList";
    public static final String DLP_SET_USER_POLICY_PATTERNS_PATH = "/api/dlp/setUserPolicyPatterns";
    public static final String DLP_GET_USER_POLICY_PATTERNS_PATH = "/api/dlp/getUserPolicyPatterns";
    public static final String DLP_SET_DOMAIN_POLICY_PATTERNS_PATH = "/api/dlp/setDomainPolicyPatterns";
    public static final String DLP_GET_DOMAIN_POLICY_PATTERNS_PATH = "/api/dlp/getDomainPolicyPatterns";
    public static final String DLP_SET_GLOBAL_POLICY_PATTERNS_PATH = "/api/dlp/setGlobalPolicyPatterns";
    public static final String DLP_GET_GLOBAL_POLICY_PATTERNS_PATH = "/api/dlp/getGlobalPolicyPatterns";

    public static final String DOMAIN_ADD_DOMAIN_PATH = "/api/domain/addDomain";
    public static final String DOMAIN_IS_DOMAIN_PATH = "/api/domain/isDomain";
    public static final String DOMAIN_DELETE_DOMAIN_PATH = "/api/domain/deleteDomain";
    public static final String DOMAIN_DELETE_DOMAINS_PATH = "/api/domain/deleteDomains";
    public static final String DOMAIN_GET_DOMAINS_PATH = "/api/domain/getDomains";
    public static final String DOMAIN_GET_DOMAINS_COUNT_PATH = "/api/domain/getDomainsCount";
    public static final String DOMAIN_IS_DOMAIN_IN_USE_PATH = "/api/domain/isDomainInUse";

    public static final String HOST_RESOURCES_GET_PUBLIC_RESOURCE = "/api/hostresources/getPublicResource";
    public static final String HOST_RESOURCES_GET_ENCODED_PUBLIC_RESOURCE = "/api/hostresources/getEncodedPublicResource";

    public static final String KEYSTORE_GET_ALIASES_PATH = "/api/keystore/getAliases";
    public static final String KEYSTORE_GET_KEYSTORE_SIZE_PATH = "/api/keystore/getKeyStoreSize";
    public static final String KEYSTORE_GET_CERTIFICATE_PATH = "/api/keystore/getCertificate";
    public static final String KEYSTORE_GET_CERTIFICATE_CHAIN_PATH = "/api/keystore/getCertificateChain";
    public static final String KEYSTORE_CONTAINS_ALIAS_PATH = "/api/keystore/containsAlias";
    public static final String KEYSTORE_IS_CERTIFICATE_ENTRY_PATH = "/api/keystore/isCertificateEntry";
    public static final String KEYSTORE_IS_KEY_ENTRY_PATH = "/api/keystore/isKeyEntry";
    public static final String KEYSTORE_DELETE_ENTRY_PATH = "/api/keystore/deleteEntry";
    public static final String KEYSTORE_RENAME_ALIAS_PATH = "/api/keystore/renameAlias";
    public static final String KEYSTORE_EXPORT_TO_PKCS12_PATH = "/api/keystore/exportToPKCS12";
    public static final String KEYSTORE_IMPORT_PKCS12_PATH = "/api/keystore/importPKCS12";

    public static final String MAIL_QUEUE_GET_QUEUE_NAMES_PATH = "/api/mpa/mailqueue/getQueueNames";
    public static final String MAIL_QUEUE_GET_QUEUED_MAILS_PATH = "/api/mpa/mailqueue/getQueuedMails";
    public static final String MAIL_QUEUE_GET_QUEUED_MAIL_PATH = "/api/mpa/mailqueue/getQueuedMail";
    public static final String MAIL_QUEUE_DELETE_QUEUED_MAIL_PATH = "/api/mpa/mailqueue/deleteQueuedMail";
    public static final String MAIL_QUEUE_DELETE_QUEUED_MAILS_PATH = "/api/mpa/mailqueue/deleteQueuedMails";
    public static final String MAIL_QUEUE_DELETE_ALL_QUEUED_MAIL_PATH = "/api/mpa/mailqueue/deleteAllQueuedMail";
    public static final String MAIL_QUEUE_GET_QUEUE_SIZE_PATH = "/api/mpa/mailqueue/getQueueSize";

    public static final String MAIL_REPOSITORY_GET_MAIL_REPOSITORY_URLS_PATH = "/api/mpa/mailrepository/getMailRepositoryUrls";
    public static final String MAIL_REPOSITORY_GET_MAIL_KEYS_PATH = "/api/mpa/mailrepository/getMailKeys";
    public static final String MAIL_REPOSITORY_GET_SIZE_PATH = "/api/mpa/mailrepository/getRepositorySize";
    public static final String MAIL_REPOSITORY_GET_MAILS_PATH = "/api/mpa/mailrepository/getMails";
    public static final String MAIL_REPOSITORY_GET_MAIL_PATH = "/api/mpa/mailrepository/getMail";
    public static final String MAIL_REPOSITORY_DELETE_MAIL_PATH = "/api/mpa/mailrepository/deleteMail";
    public static final String MAIL_REPOSITORY_DELETE_MAILS_PATH = "/api/mpa/mailrepository/deleteMails";
    public static final String MAIL_REPOSITORY_DELETE_ALL_MAIL_PATH = "/api/mpa/mailrepository/deleteAllMail";
    public static final String MAIL_REPOSITORY_REQUEUE_MAILS_PATH = "/api/mpa/mailrepository/requeueMails";

    public static final String MTA_GET_QUEUE_LIST_PATH = "/api/mta/getQueueList";
    public static final String MTA_GET_QUEUE_SIZE_PATH = "/api/mta/getQueueSize";
    public static final String MTA_FLUSH_QUEUE_PATH = "/api/mta/flushQueue";
    public static final String MTA_RESCHEDULE_DEFERRED_MAIL_PATH = "/api/mta/rescheduleDeferredMail";
    public static final String MTA_DELETE_MAIL_PATH = "/api/mta/deleteMail";
    public static final String MTA_HOLD_MAIL_PATH = "/api/mta/holdMail";
    public static final String MTA_RELEASE_MAIL_PATH = "/api/mta/releaseMail";
    public static final String MTA_BOUNCE_MAIL_PATH = "/api/mta/bounceMail";
    public static final String MTA_REQUEUE_MAIL_PATH = "/api/mta/requeueMail";
    public static final String MTA_GET_MAIL_PATH = "/api/mta/getMail";
    public static final String MTA_GET_MAIN_CONFIG_PATH = "/api/mta/getMainConfig";
    public static final String MTA_SET_MAIN_CONFIG_PATH = "/api/mta/setMainConfig";
    public static final String MTA_START_POSTFIX_PATH = "/api/mta/startPostfix";
    public static final String MTA_STOP_POSTFIX_PATH = "/api/mta/stopPostfix";
    public static final String MTA_RESTART_POSTFIX_PATH = "/api/mta/restartPostfix";
    public static final String MTA_GET_POSTFIX_STATUS_PATH = "/api/mta/getPostfixStatus";
    public static final String MTA_IS_POSTFIX_RUNNING_PATH = "/api/mta/isPostfixRunning";
    public static final String MTA_SET_STANDARD_SETTINGS_PATH = "/api/mta/setStandardSettings";
    public static final String MTA_GET_STANDARD_SETTINGS_PATH = "/api/mta/getStandardSettings";
    public static final String MTA_LIST_MAPS = "/api/mta/listMaps";
    public static final String MTA_GET_MAP_CONTENT = "/api/mta/getMapContent";
    public static final String MTA_SET_MAP_CONTENT = "/api/mta/setMapContent";
    public static final String MTA_CREATE_MAP = "/api/mta/createMap";
    public static final String MTA_DELETE_MAP = "/api/mta/deleteMap";
    public static final String MTA_SEND_EMAIL_MIME = "/api/mta/sendEmailMime";
    public static final String MTA_SEND_EMAIL = "/api/mta/sendEmail";

    public static final String PGP_IMPORT_KEYRING_PATH = "/api/pgp/importKeyring";
    public static final String PGP_EXPORT_PUBLIC_KEYS_PATH = "/api/pgp/exportPublicKeys";
    public static final String PGP_EXPORT_SECRET_KEYS_PATH = "/api/pgp/exportSecretKeys";
    public static final String PGP_SET_PGP_KEY_EMAIL_AND_DOMAINS_PATH = "/api/pgp/setPGPKeyEmailAndDomains";
    public static final String PGP_GET_ENCRYPTION_KEYS_FOR_EMAIL_PATH = "/api/pgp/getEncryptionKeysForEmail";
    public static final String PGP_GET_ENCRYPTION_KEYS_FOR_DOMAIN_PATH = "/api/pgp/getEncryptionKeysForDomain";
    public static final String PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_EMAIL_PATH = "/api/pgp/getAvailableSigningKeysForEmail";
    public static final String PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_DOMAIN_PATH = "/api/pgp/getAvailableSigningKeysForDomain";
    public static final String PGP_GET_SIGNING_KEY_FOR_USER_PATH = "/api/pgp/getSigningKeyForUser";
    public static final String PGP_GET_SIGNING_KEY_FOR_DOMAIN_PATH = "/api/pgp/getSigningKeyForDomain";
    public static final String PGP_SET_SIGNING_KEY_FOR_USER_PATH = "/api/pgp/setSigningKeyForUser";
    public static final String PGP_SET_SIGNING_KEY_FOR_DOMAIN_PATH = "/api/pgp/setSigningKeyForDomain";
    public static final String PGP_GENERATE_SECRET_KEY_RING_PATH = "/api/pgp/generateSecretKeyRing";
    public static final String PGP_REVOKE_KEY_PATH = "/api/pgp/revokeKey";
    public static final String PGP_REVOKE_KEYS_PATH = "/api/pgp/revokeKeys";
    public static final String PGP_IS_USER_ID_VALID_PATH = "/api/pgp/isUserIDValid";
    public static final String PGP_IS_USER_ID_REVOKED_PATH = "/api/pgp/isUserIDRevoked";

    public static final String PGP_KEYRING_GET_KEY_PATH = "/api/pgp/keyring/getKey";
    public static final String PGP_KEYRING_GET_KEY_BY_SHA256_FINGERPRINT_PATH = "/api/pgp/keyring/getKeyBySha256Fingerprint";
    public static final String PGP_KEYRING_GET_SUBKEYS_PATH = "/api/pgp/keyring/getSubkeys";
    public static final String PGP_KEYRING_GET_KEYS_PATH = "/api/pgp/keyring/getKeys";
    public static final String PGP_KEYRING_GET_KEYS_COUNT_PATH = "/api/pgp/keyring/getKeysCount";
    public static final String PGP_KEYRING_SEARCH_KEYS_PATH = "/api/pgp/keyring/searchKeys";
    public static final String PGP_KEYRING_SEARCH_KEYS_COUNT_PATH = "/api/pgp/keyring/searchKeysCount";
    public static final String PGP_KEYRING_DELETE_KEY_PATH = "/api/pgp/keyring/deleteKey";
    public static final String PGP_KEYRING_DELETE_KEYS_PATH = "/api/pgp/keyring/deleteKeys";

    public static final String PGP_KEY_SERVER_SEARCH_KEYS_PATH = "/api/pgp/keyserver/searchKeys";
    public static final String PGP_KEY_SERVER_DOWNLOAD_KEYS_PATH = "/api/pgp/keyserver/downloadKeys";
    public static final String PGP_KEY_SERVER_REFRESH_KEYS_PATH = "/api/pgp/keyserver/refreshKeys";
    public static final String PGP_KEY_SERVER_SUBMIT_KEYS_PATH = "/api/pgp/keyserver/submitKeys";
    public static final String PGP_KEY_SERVER_SET_PGP_KEY_SERVERS_PATH = "/api/pgp/keyserver/setPGPKeyServers";
    public static final String PGP_KEY_SERVER_GET_PGP_KEY_SERVERS_PATH = "/api/pgp/keyserver/getPGPKeyServers";

    public static final String PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRIES_PATH = "/api/pgp/trustlist/getPGPTrustListEntries";
    public static final String PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRY_PATH = "/api/pgp/trustlist/getPGPTrustListEntry";
    public static final String PGP_TRUST_LIST_GET_PGP_TRUST_LIST_COUNT_PATH = "/api/pgp/trustlist/getPGPTrustListCount";
    public static final String PGP_TRUST_LIST_SET_PGP_TRUST_LIST_ENTRY_PATH = "/api/pgp/trustlist/setPGPTrustListEntry";
    public static final String PGP_TRUST_LIST_SET_PGP_TRUST_LIST_ENTRIES_PATH = "/api/pgp/trustlist/setPGPTrustListEntries";
    public static final String PGP_TRUST_LIST_DELETE_PGP_TRUST_LIST_ENTRY_PATH = "/api/pgp/trustlist/deletePGPTrustListEntry";

    public static final String PORTAL_GET_PORTAL_PROPERTIES_PATH = "/api/portal/getPortalProperties";
    public static final String PORTAL_SET_2FA_ENABLED_PATH = "/api/portal/set2FAEnabled";
    public static final String PORTAL_SET_2FA_SECRET_PATH = "/api/portal/set2FASecret";
    public static final String PORTAL_GET_PDF_REPLY_URL_PATH = "/api/portal/getPdfReplyURL";
    public static final String PORTAL_GET_PDF_REPLY_PARAMETERS_PATH = "/api/portal/getPdfReplyParameters";
    public static final String PORTAL_GET_PDF_PASSWORD_PATH = "/api/portal/getPdfPassword";
    public static final String PORTAL_GET_OTP_SECRET_KEY_QR_CODE_PATH = "/api/portal/getOTPSecretKeyQRCode";
    public static final String PORTAL_GET_OTP_JSON_CODE_PATH = "/api/portal/getOTPJSONCode";
    public static final String PORTAL_GET_OTP_QR_CODE_PATH = "/api/portal/getOTPQRCode";
    public static final String PORTAL_VALIDATE_SIGNUP_PATH = "/api/portal/validatePortalSignup";
    public static final String PORTAL_SET_PORTAL_SIGNUP_PASSWORD_PATH = "/api/portal/setPortalSignupPassword";
    public static final String PORTAL_FORGOT_PASSWORD_PATH = "/api/portal/forgotPassword";
    public static final String PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH = "/api/portal/validatePortalPasswordReset";
    public static final String PORTAL_RESET_PORTAL_PASSWORD_PATH = "/api/portal/resetPortalPassword";
    public static final String PORTAL_CHANGE_PORTAL_PASSWORD_PATH = "/api/portal/changePortalPassword";

    public static final String PROPERTY_GET_USER_PROPERTY_PATH = "/api/property/getUserProperty";
    public static final String PROPERTY_GET_USER_PROPERTIES_PATH = "/api/property/getUserProperties";
    public static final String PROPERTY_SET_USER_PROPERTY_PATH = "/api/property/setUserProperty";
    public static final String PROPERTY_SET_USER_PROPERTIES_PATH = "/api/property/setUserProperties";
    public static final String PROPERTY_GET_DOMAIN_PROPERTY_PATH = "/api/property/getDomainProperty";
    public static final String PROPERTY_GET_DOMAIN_PROPERTIES_PATH = "/api/property/getDomainProperties";
    public static final String PROPERTY_SET_DOMAIN_PROPERTY_PATH = "/api/property/setDomainProperty";
    public static final String PROPERTY_SET_DOMAIN_PROPERTIES_PATH = "/api/property/setDomainProperties";
    public static final String PROPERTY_GET_GLOBAL_PROPERTY_PATH = "/api/property/getGlobalProperty";
    public static final String PROPERTY_GET_GLOBAL_PROPERTIES_PATH = "/api/property/getGlobalProperties";
    public static final String PROPERTY_SET_GLOBAL_PROPERTY_PATH = "/api/property/setGlobalProperty";
    public static final String PROPERTY_SET_GLOBAL_PROPERTIES_PATH = "/api/property/setGlobalProperties";
    public static final String PROPERTY_GET_AVAILABLE_PROPERTIES_PATH = "/api/property/getAvailableProperties";
    public static final String PROPERTY_GET_USER_PROPERTY_DESCRIPTOR_PATH = "/api/property/getUserPropertyDescriptor";

    public static final String QUARANTINE_GET_QUARANTINED_MAIL_PATH = "/api/quarantine/getQuarantinedMail";
    public static final String QUARANTINE_GET_QUARANTINED_MAIL_COUNT_PATH = "/api/quarantine/getQuarantinedMailCount";
    public static final String QUARANTINE_GET_QUARANTINED_MAIL_BY_ID_PATH = "/api/quarantine/getQuarantinedMailById";
    public static final String QUARANTINE_DELETE_QUARANTINED_MAIL_PATH = "/api/quarantine/deleteQuarantinedMail";
    public static final String QUARANTINE_DELETE_QUARANTINED_MAILS_PATH = "/api/quarantine/deleteQuarantinedMails";
    public static final String QUARANTINE_GET_QUARANTINED_MAIL_CONTENT_PATH = "/api/quarantine/getQuarantinedMailContent";
    public static final String QUARANTINE_SEARCH_QUARANTINED_MAIL_PATH = "/api/quarantine/searchQuarantinedMail";
    public static final String QUARANTINE_SEARCH_QUARANTINED_MAIL_COUNT_PATH = "/api/quarantine/searchQuarantinedMailCount";
    public static final String QUARANTINE_RELEASE_QUARANTINED_MAIL_PATH = "/api/quarantine/releaseQuarantinedMail";
    public static final String QUARANTINE_RELEASE_QUARANTINED_MAILS_PATH = "/api/quarantine/releaseQuarantinedMails";

    public static final String SMS_GET_SMS_MESSAGES_PATH = "/api/sms/getSMSMessages";
    public static final String SMS_SEND_SMS_PATH = "/api/sms/sendSMS";
    public static final String SMS_DELETE_SMS_PATH = "/api/sms/deleteSMS";
    public static final String SMS_GET_SMS_QUEUE_SIZE_PATH = "/api/sms/getSMSQueueSize";
    public static final String SMS_GET_SMS_TRANSPORTS_PATH = "/api/sms/getSMSTransports";

    public static final String SYSTEM_PING_PATH = "/api/system/ping";

    public static final String USER_ADD_USER_PATH = "/api/user/addUser";
    public static final String USER_GET_USER_PATH = "/api/user/getUser";
    public static final String USER_IS_USER_PATH = "/api/user/isUser";
    public static final String USER_DELETE_USER_PATH = "/api/user/deleteUser";
    public static final String USER_DELETE_USERS_PATH = "/api/user/deleteUsers";
    public static final String USER_GET_USERS_PATH = "/api/user/getUsers";
    public static final String USER_GET_USERS_COUNT_PATH = "/api/user/getUsersCount";
    public static final String USER_SEARCH_USERS_PATH = "/api/user/searchUsers";
    public static final String USER_SEARCH_USERS_COUNT_PATH = "/api/user/searchUsersCount";

    public static final String LOG_GET_LOG_LINES_COUNT_PATH = "/api/log/getLogLinesCount";
    public static final String LOG_GET_LOG_LINES_PATH = "/api/log/getLogLines";
}
