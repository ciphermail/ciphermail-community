/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotNull;

import java.util.List;
import java.util.Objects;

public class MTADTO
{
    private MTADTO() {
        // empty on purpose
    }

    public enum QueueName{HOLD, INCOMING, ACTIVE, DEFERRED}

    // json is created by postfix, we therefore need to ignore unknown json params
    // to make sure we are forward compatible with changes to postfix
    @JsonIgnoreProperties(ignoreUnknown = true)
    public record Recipient(
            @NotNull String address,
            @NotNull String delay_reason){}

    // json is created by postfix, we therefore need to ignore unknown json params
    // to make sure we are forward compatible with changes to postfix
    @JsonIgnoreProperties(ignoreUnknown = true)
    public record QueueItem(
            @NotNull String queue_name,
            @NotNull String queue_id,
            @NotNull long arrival_time,
            @NotNull long message_size,
            @NotNull boolean forced_expire,
            @NotNull String sender,
            @NotNull List<Recipient> recipients){}

    /**
     * Postfix main config settings which can be set using the REST API
     */
    public static class StandardMainSettings
    {
        @JsonProperty
        private String myHostname;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty
        private List<String> myNetworks;

        @JsonProperty
        private String externalRelayHost;

        @JsonProperty
        private Boolean externalRelayHostMxLookup;

        @JsonProperty
        private Integer externalRelayHostPort;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty
        private List<String> relayDomains;

        @JsonProperty
        private Long messageSizeLimit;

        @JsonProperty
        private String smtpHeloName;

        @JsonProperty
        private String internalRelayHost;

        @JsonProperty
        private Boolean internalRelayHostMxLookup;

        @JsonProperty
        private Integer internalRelayHostPort;

        @JsonProperty
        private Boolean rejectUnverifiedRecipient;

        @JsonProperty
        private Integer unverifiedRecipientRejectCode;

        public static StandardMainSettings createInstance() {
            return new StandardMainSettings();
        }

        public String getMyHostname() {
            return myHostname;
        }

        public @Nonnull StandardMainSettings setMyHostname(String myHostname)
        {
            this.myHostname = myHostname;
            return this;
        }

        public List<String> getMyNetworks() {
            return myNetworks;
        }

        public @Nonnull StandardMainSettings setMyNetworks(@Nonnull List<String> myNetworks)
        {
            this.myNetworks = Objects.requireNonNull(myNetworks);
            return this;
        }

        public String getExternalRelayHost() {
            return externalRelayHost;
        }

        public @Nonnull StandardMainSettings setExternalRelayHost(String externalRelayHost)
        {
            this.externalRelayHost = externalRelayHost;
            return this;
        }

        public Boolean getExternalRelayHostMxLookup() {
            return externalRelayHostMxLookup;
        }

        public @Nonnull StandardMainSettings setExternalRelayHostMxLookup(Boolean externalRelayHostMxLookup)
        {
            this.externalRelayHostMxLookup = externalRelayHostMxLookup;
            return this;
        }

        public Integer getExternalRelayHostPort() {
            return externalRelayHostPort;
        }

        public @Nonnull StandardMainSettings setExternalRelayHostPort(Integer externalRelayHostPort)
        {
            this.externalRelayHostPort = externalRelayHostPort;
            return this;
        }

        public List<String> getRelayDomains() {
            return relayDomains;
        }

        public @Nonnull StandardMainSettings setRelayDomains(List<String> relayDomains)
        {
            this.relayDomains = relayDomains;
            return this;
        }

        public Long getMessageSizeLimit() {
            return messageSizeLimit;
        }

        public @Nonnull StandardMainSettings setMessageSizeLimit(Long messageSizeLimit)
        {
            this.messageSizeLimit = messageSizeLimit;
            return this;
        }

        public String getSmtpHeloName() {
            return smtpHeloName;
        }

        public @Nonnull StandardMainSettings setSmtpHeloName(String smtpHeloName)
        {
            this.smtpHeloName = smtpHeloName;
            return this;
        }

        public String getInternalRelayHost() {
            return internalRelayHost;
        }

        public @Nonnull StandardMainSettings setInternalRelayHost(String internalRelayHost)
        {
            this.internalRelayHost = internalRelayHost;
            return this;
        }

        public Boolean getInternalRelayHostMxLookup() {
            return internalRelayHostMxLookup;
        }

        public @Nonnull StandardMainSettings setInternalRelayHostMxLookup(Boolean internalRelayHostMxLookup)
        {
            this.internalRelayHostMxLookup = internalRelayHostMxLookup;
            return this;
        }

        public Integer getInternalRelayHostPort() {
            return internalRelayHostPort;
        }

        public @Nonnull StandardMainSettings setInternalRelayHostPort(Integer internalRelayHostPort)
        {
            this.internalRelayHostPort = internalRelayHostPort;
            return this;
        }

        public Boolean getRejectUnverifiedRecipient() {
            return rejectUnverifiedRecipient;
        }

        public @Nonnull StandardMainSettings setRejectUnverifiedRecipient(Boolean rejectUnverifiedRecipient)
        {
            this.rejectUnverifiedRecipient = rejectUnverifiedRecipient;
            return this;
        }

        public Integer getUnverifiedRecipientRejectCode() {
            return unverifiedRecipientRejectCode;
        }

        public @Nonnull StandardMainSettings setUnverifiedRecipientRejectCode(Integer unverifiedRecipientRejectCode) {
            this.unverifiedRecipientRejectCode = unverifiedRecipientRejectCode;
            return this;
        }
    }

    public enum PostfixMapType{
        BTREE,
        CIDR,
        HASH,
        PCRE,
        REGEXP}

    public record PostfixMap(
            @NotNull PostfixMapType type,
            @NotNull String name,
            @NotNull String filename){}

    public enum MTAPort {
        DIRECT_DELIVERY,
        MPA}
}
