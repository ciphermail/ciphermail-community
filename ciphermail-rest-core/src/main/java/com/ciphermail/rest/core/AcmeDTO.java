/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.core;

import java.net.URI;
import java.net.URL;
import java.util.List;

public class AcmeDTO
{
    private AcmeDTO() {
        // empty on purpose
    }

    public enum KeyAlgorithm {
        RSA_2048,
        RSA_3072,
        RSA_4096,
        SECP521R1,
        SECP384R1,
        SECP256R1}

    public enum AccountStatus
    {
        PENDING,

        /**
         * The Order is ready to be finalized
         */
        READY,

        /**
         * The server is processing the resource.
         */
        PROCESSING,

        /**
         * The resource is valid and can be used as intended.
         */
        VALID,

        /**
         * An error or authorization/validation failure has occurred. The client should check
         * for error messages.
         */
        INVALID,

        /**
         * The Authorization has been revoked by the server.
         */
        REVOKED,

        /**
         * The Account or Authorization has been deactivated by the client.
         */
        DEACTIVATED,

        /**
         * The Authorization is expired.
         */
        EXPIRED,

        /**
         * An auto-renewing Order
         */
        CANCELED,

        /**
         * The server did not provide a status, or the provided status is not a specified ACME
         * status.
         */
        UNKNOWN;
    }

    public record Account(AccountStatus status, URL location, List<URI>contacts, boolean tosAccepted){}
}
