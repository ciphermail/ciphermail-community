/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.portal.rest.controller;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailTransport;
import com.ciphermail.core.common.mail.MimeMessageBuilder;
import com.ciphermail.core.common.util.InputStreamResource;
import com.ciphermail.portal.rest.PortalRestPaths;
import com.ciphermail.rest.client.PortalClient;
import com.ciphermail.rest.core.PortalDTO;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestException;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Nonnull;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "PDF")
@SuppressWarnings({"java:S6813"})
public class PDFController
{
    private static final Logger logger = LoggerFactory.getLogger(PDFController.class);

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private MailTransport mailTransport;

    @Autowired
    private PortalClient portalClient;

    @Autowired
    private AuthenticatedUserProvider authenticatedUserProvider;

    /*
     * The SMTP port for email which should be delivered via the MPA
     */
    @Value("${ciphermail.portal.rest.mpa-port:10027}")
    private int mpaPort;

    /*
     * The SMTP host for sending email
     */
    @Value("${ciphermail.portal.rest.smtp-host:127.0.0.1}")
    private String smtpHost;

    /*
     * special CipherMail specific header (default: X-CipherMail-PDF-Reply) which can be used to detect that
     * this is a PDF reply message
     */
    @Value("${ciphermail.portal.rest.pdf.reply-header:X-CipherMail-PDF-Reply}")
    private String replyHeader;

    /*
     * Cache which keeps track whether a reply link (env) was already used in a recent reply
     */
    private final Cache<String, Long> replyCache;

    public PDFController(@Value("${ciphermail.portal.rest.pdf.reply-wait-time-seconds:60}")
            long replyWaitTimeSeconds)
    {
        replyCache = Caffeine.newBuilder()
                .expireAfterWrite(Duration.ofSeconds(replyWaitTimeSeconds))
                .build();
    }

    @GetMapping(PortalRestPaths.PDF_PARSE_PDF_REPLY_PARAMETERS_PATH)
    public PortalDTO.PdfReplyParameters parsePdfReplyParameters(
            @RequestParam String env,
            @RequestParam String hmac)
    {
        try {
            return portalClient.getPdfReplyParameters(env, hmac);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @PostMapping(path = PortalRestPaths.PDF_SEND_PDF_REPLY_PATH, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public void sendPdfReply(
            @RequestPart String env,
            @RequestPart String hmac,
            @RequestPart String textBody,
            @RequestBody MultipartFile[] attachments)
    {
        PortalDTO.PdfReplyParameters pdfReplyParameters;

        try {
            pdfReplyParameters = portalClient.getPdfReplyParameters(env, hmac);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }

        if (pdfReplyParameters.expired()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "PDF reply link expired");
        }

        if (!pdfReplyParameters.replyAllowed()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "PDF reply not allowed");
        }

        if (textBody.length() > pdfReplyParameters.bodyMaxSize()) {
            throw RestException.createInstance("Body size exceeds the maximum size")
                    .setKey("pdf.body-too-large").log(logger);
        }

        final String cacheKey = StringUtils.trim(env);

        Long lastReplyTime = replyCache.getIfPresent(cacheKey);

        if (lastReplyTime != null) {
            throw RestException.createInstance("It's not allowed to send multiple replies in rapid succession (try again later)")
                    .setKey("pdf.reply-too-fast").log(logger);
        }

        try (MimeMessageBuilder messageBuilder = MimeMessageBuilder.createInstance()
                .setSubject(pdfReplyParameters.subject())
                .setFrom(pdfReplyParameters.from())
                .setTextBody(textBody)
                .addTo(List.of(pdfReplyParameters.to()))
                .addCc(pdfReplyParameters.cc() != null ? List.of(pdfReplyParameters.cc()) : null))
        {
            if (attachments != null)
            {
                long totalAttachmentsSize = 0;

                for (MultipartFile attachment : attachments)
                {
                    totalAttachmentsSize = totalAttachmentsSize + attachment.getSize();

                    if (totalAttachmentsSize > pdfReplyParameters.attachmentsMaxSize())
                    {
                        throw RestException.createInstance("Total size of the attachments exceeds the maximum size")
                                .setKey("pdf.attachments-too-large").log(logger);
                    }

                    // Note: InputStreamResource and attachment input stream will be closed when MimeMessageBuilder is closed
                    messageBuilder.addAttachment(new InputStreamResource(attachment.getInputStream(),
                            Optional.ofNullable(attachment.getContentType()).orElse(MediaType.APPLICATION_OCTET_STREAM_VALUE),
                            attachment.getOriginalFilename()));
                }

                // If the message-id of the message we are replying to was set, use it to set the In-Reply-To header
                // to support message threading in the mail client
                if (StringUtils.isNotEmpty(pdfReplyParameters.messageID())) {
                    messageBuilder.addHeader("In-Reply-To", pdfReplyParameters.messageID());
                }

                // Add special CipherMail specific header (default: X-CipherMail-PDF-Reply) which can be used to detect that
                // this is a PDF reply message
                messageBuilder.addHeader(replyHeader, "True");
            }

            List<String> recipients = new LinkedList<>();

            recipients.add(pdfReplyParameters.to());

            if (pdfReplyParameters.cc() != null) {
                recipients.add(pdfReplyParameters.cc());
            }

            sendMimeMessage(pdfReplyParameters.sender(), recipients, mpaPort, messageBuilder.build());

            replyCache.put(cacheKey, System.currentTimeMillis());

            logger.debug("replyCache size {}", replyCache.estimatedSize());
        }
        catch (IOException | MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @GetMapping(PortalRestPaths.PDF_GET_PDF_PASSWORD_PATH)
    public String getPdfPassword(@RequestParam String code)
    {
        try {
            return portalClient.getPdfPassword(authenticatedUserProvider.getAuthenticatedUser(), code);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @GetMapping(PortalRestPaths.GET_OTP_SECRET_KEY_QR_CODE_PATH)
    public String getOTPSecretKeyQRCode()
    {
        try {
            return portalClient.getOTPSecretKeyQRCode(authenticatedUserProvider.getAuthenticatedUser());
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @GetMapping(PortalRestPaths.GET_OTP_JSON_CODE_PATH)
    public String getOTPJSONCode(@Nonnull String passwordID, int passwordLength)
    {
        try {
            return portalClient.getOTPJSONCode(authenticatedUserProvider.getAuthenticatedUser(), passwordID, passwordLength);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @GetMapping(PortalRestPaths.GET_OTP_QR_CODE_PATH)
    public String getOTPQRCode(@Nonnull String passwordID, int passwordLength)
    {
        try {
            return portalClient.getOTPQRCode(authenticatedUserProvider.getAuthenticatedUser(), passwordID, passwordLength);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    private void sendMimeMessage(
            String sender,
            List<String> recipients,
            int mtaPort,
            @Nonnull MimeMessage message)
    {
        try {
            logger.info("Sending email (Sender: {}, Recipients: {}, Port: {})", sender, recipients, mtaPort);

            mailTransport.sendMessage(
                    smtpHost,
                    mtaPort,
                    message,
                    StringUtils.trimToNull(sender),
                    EmailAddressUtils.toInternetAddressesStrict(recipients));
        }
        catch (MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }
}
