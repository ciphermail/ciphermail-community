/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.portal.rest.service;

import com.ciphermail.rest.client.PortalClient;
import com.ciphermail.rest.core.PortalDTO;
import com.ciphermail.rest.server.service.AuthenticationFailureCache;
import com.ciphermail.rest.server.service.TFAUserDetails;
import jakarta.annotation.Nonnull;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class SystemUserDetailsService implements UserDetailsService
{
    private static final Logger logger = LoggerFactory.getLogger(SystemUserDetailsService.class);

    /*
     * PortalClient is used for interfacing with the REST back-end
     */
    private final PortalClient portalClient;

    /*
     * Keeps track of login failures and can ban admins from logging-in if the number of failed login exceeds the max
     * number of failed logins within a certain time period.
     */
    private final AuthenticationFailureCache authenticationFailureCache;

    // some methods come from XML
    public SystemUserDetailsService(
            @Nonnull PortalClient portalClient,
            @Nonnull AuthenticationFailureCache authenticationFailureCache)
    {
        this.portalClient = Objects.requireNonNull(portalClient);
        this.authenticationFailureCache = Objects.requireNonNull(authenticationFailureCache);
    }

    @Override
    public UserDetails loadUserByUsername(@Nonnull String username)
    throws UsernameNotFoundException
    {
        if (authenticationFailureCache.isBanned(username))
        {
            logger.warn("User {} exceeded the max number of failed login attempts. User will be banned.", username);

            throw new UsernameNotFoundException(String.format("User %s is banned", username));
        }

        PortalDTO.PortalProperties portalProperties = portalClient.getPortalProperties(username);

        if (StringUtils.isEmpty(portalProperties.encodedPassword())) {
            throw new UsernameNotFoundException(String.format("No password found for %s", username));
        }

        if (!portalProperties.loginEnabled())
        {
            logger.warn("User {} is not allowed to login", username);

            throw new UsernameNotFoundException(String.format("User %s is not allowed to login", username));
        }

        UserDetails userDetails = User.withUsername(username)
                .password(portalProperties.encodedPassword())
                .build();

        return portalProperties.tfaEnabled() && portalProperties.tfaSecret() != null ?
                new TFAUserDetails(userDetails, portalProperties.tfaSecret()) : userDetails;
    }
}
