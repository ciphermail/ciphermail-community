/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.portal.rest.controller;

import com.ciphermail.core.common.security.crypto.RandomGenerator;
import com.ciphermail.core.common.security.otp.TOTP;
import com.ciphermail.core.common.security.otp.TOTPValidationFailureException;
import com.ciphermail.core.common.util.Base32Utils;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.GoogleAuthenticatorUtils;
import com.ciphermail.portal.rest.PortalRestPaths;
import com.ciphermail.rest.client.PortalClient;
import com.ciphermail.rest.core.AuthDTO;
import com.ciphermail.rest.core.PortalDTO;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;

@RestController
@Tag(name = "Auth")
// suppress warning because most beans are created in XML
@SuppressWarnings({"java:S6813"})
public class AuthController
{
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Value("${ciphermail.rest.authentication.2fa.length:20}")
    private int tfaLength;

    @Value("${ciphermail.rest.authentication.2fa.qrcode.format:png}")
    private String qrCodeFormat;

    @Value("${ciphermail.rest.authentication.2fa.qrcode.width:200}")
    private int qrCodeWidth;

    @Value("${ciphermail.rest.authentication.2fa.qrcode.height:200}")
    private int qrCodeHeight;

    @Autowired
    private AuthenticatedUserProvider authenticatedUserProvider;

    @Autowired
    private PortalClient portalClient;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private TOTP totp;

    @Autowired
    private RandomGenerator randomGenerator;

    @GetMapping(RestPaths.AUTH_GET_AUTHENTICATED_USER_PATH)
    public String getAuthenticatedUser() {
        return authenticatedUserProvider.getAuthenticatedUser();
    }

    @GetMapping(PortalRestPaths.PORTAL_VALIDATE_SIGNUP_PATH)
    public PortalDTO.PortalSignupValidationResult validatePortalSignup(@RequestParam String signupCode)
    {
        try {
            return portalClient.validatePortalSignup(signupCode);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @PostMapping(path = PortalRestPaths.PORTAL_SET_PORTAL_SIGNUP_PASSWORD_PATH, consumes = {MediaType.TEXT_PLAIN_VALUE})
    public PortalDTO.PortalSignupValidationResult setPortalSignupPassword(
            @RequestParam String signupCode,
            @RequestBody String password)
    {
        try {
            return portalClient.setPortalSignupPassword(signupCode, password);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @PostMapping(PortalRestPaths.PORTAL_FORGOT_PASSWORD_PATH)
    public void forgotPassword(@RequestParam String email)
    {
        try {
            portalClient.forgotPassword(email);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @GetMapping(PortalRestPaths.PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH)
    public PortalDTO.PortalPasswordResetValidationResult validatePortalPasswordReset(@RequestParam String resetCode)
    {
        try {
            return portalClient.validatePortalPasswordReset(resetCode);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @PostMapping(path = PortalRestPaths.PORTAL_RESET_PORTAL_PASSWORD_PATH, consumes = {MediaType.TEXT_PLAIN_VALUE})
    public PortalDTO.PortalPasswordResetValidationResult resetPortalPassword(
            @RequestParam String resetCode,
            @RequestBody String password)
    {
        try {
            return portalClient.resetPortalPassword(resetCode, password);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @PostMapping(PortalRestPaths.PORTAL_CHANGE_PORTAL_PASSWORD_PATH)
    public void changePortalPassword(@RequestBody AuthDTO.ChangePassword changePassword)
    {
        try {
            portalClient.changePortalPassword(authenticatedUserProvider.getAuthenticatedUser(), changePassword);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }

    @PostMapping(PortalRestPaths.PORTAL_VALIDATE_TOTP_PATH)
    public void validateTOTP(@RequestParam String otp)
    {
        String otpSecret = StringUtils.trimToNull(portalClient.getPortalProperties(
                authenticatedUserProvider.getAuthenticatedUser()).tfaSecret());

        if (otpSecret == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    "OTP secret is not set", logger);
        }

        try {
            totp.verifyOTP(Base32Utils.base32Decode(otpSecret), otp);
        }
        catch (TOTPValidationFailureException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "TOTP verification failure", logger);
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @GetMapping(PortalRestPaths.PORTAL_GET_2FA_SECRET_QR_CODE_PATH)
    public String get2FASecretQRCode()
    {
        PortalDTO.PortalProperties portalProperties = portalClient.getPortalProperties(
                authenticatedUserProvider.getAuthenticatedUser());

        String tfaSecret = StringUtils.trimToNull(portalProperties.tfaSecret());

        if (tfaSecret == null) {
            // generate and save 2FA secret
            tfaSecret = Base32Utils.base32Encode(randomGenerator.generateRandom(tfaLength));
            portalClient.set2FASecret(authenticatedUserProvider.getAuthenticatedUser(), tfaSecret);
        }

        try {
            return Base64Utils.encode(GoogleAuthenticatorUtils.generateQRCode(
                    tfaSecret,
                    StringUtils.defaultString(portalProperties.orginizationID()),
                    authenticatedUserProvider.getAuthenticatedUser(),
                    qrCodeFormat,
                    qrCodeWidth,
                    qrCodeHeight));
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(PortalRestPaths.PORTAL_SET_2FA_ENABLED_PATH)
    public void set2FAEnabled(@RequestParam boolean enable)
    {
        try {
            portalClient.set2FAEnabled(authenticatedUserProvider.getAuthenticatedUser(), enable);
        }
        catch(HttpClientErrorException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e.getStatusCode(),
                    e.getResponseBodyAsString(), e, logger);
        }
    }
}