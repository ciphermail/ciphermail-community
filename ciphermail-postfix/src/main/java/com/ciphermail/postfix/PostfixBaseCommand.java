/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.postfix;

import com.ciphermail.core.common.util.ProcessException;
import com.ciphermail.core.common.util.ProcessRunner;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang3.time.DateUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of Postqueue which uses the postqueue command to manage the postfix queue
 */
public abstract class PostfixBaseCommand
{
    /*
     * The maximum time in milliseconds a command may run after which it is destroyed
     */
    private long timeout = 30 * DateUtils.MILLIS_PER_SECOND;

    /*
     * Max output size we are interested in
     */
    private int maxOutputSize = SizeUtils.MB * 100;

    /*
     * Max error output size we are interested in
     */
    private int maxErrorSize = SizeUtils.KB * 10;

    /*
     * If true, the postfix command will be run with sudo
     */
    private boolean requireSudo;

    protected void executeCommand(List<String> params)
    throws IOException
    {
        executeCommand(params, null, null, true);
    }

    protected void executeCommand(List<String> params, InputStream input, OutputStream output)
    throws IOException
    {
        executeCommand(params, input, output, true);
    }

    protected void executeCommand(List<String> params, InputStream input, OutputStream output,
            boolean throwExceptionOnErrorExitCode)
    throws IOException
    {
        ProcessRunner processRunner = new ProcessRunner();

        processRunner.setThrowExceptionOnErrorExitCode(throwExceptionOnErrorExitCode);
        processRunner.setRequireSudo(isRequireSudo());

        // split command on spaces to support a command with parameters
        List<String> cmd = new LinkedList<>(ProcessRunner.splitCommandLine(getBaseCommand()));

        if (params != null) {
            cmd.addAll(params);
        }

        StringWriter errorWriter = new StringWriter();

        OutputStream errorStream = new SizeLimitedOutputStream(WriterOutputStream.builder().setWriter(errorWriter).setCharset(
                StandardCharsets.UTF_8).get(), getMaxErrorSize(), false);

        try {
            if (input != null) {
                processRunner.setInput(input);
            }

            if (output != null) {
                processRunner.setOutput(output);
            }

            processRunner.setError(errorStream);

            processRunner.run(cmd);
        }
        catch(ProcessException e)
        {
            // need to close before getting the content
            IOUtils.closeQuietly(errorStream);

            throw new IOException(errorWriter.toString(), e);
        }
        finally {
            IOUtils.closeQuietly(errorStream);
        }
    }

    public abstract String getBaseCommand();

    public boolean isRequireSudo() {
        return requireSudo;
    }

    public void setRequireSudo(boolean requireSudo) {
        this.requireSudo = requireSudo;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public int getMaxOutputSize() {
        return maxOutputSize;
    }

    public void setMaxOutputSize(int maxOutputSize) {
        this.maxOutputSize = maxOutputSize;
    }

    public int getMaxErrorSize() {
        return maxErrorSize;
    }

    public void setMaxErrorSize(int maxErrorSize) {
        this.maxErrorSize = maxErrorSize;
    }
}
