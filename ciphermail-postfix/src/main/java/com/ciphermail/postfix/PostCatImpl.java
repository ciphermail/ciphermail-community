package com.ciphermail.postfix;

import org.apache.commons.io.IOUtils;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Objects;

public class PostCatImpl extends PostfixBaseCommand implements PostCat
{
    /*
     * The command to run
     */
    private final String command;

    public PostCatImpl(@Nonnull String command) {
        this.command = Objects.requireNonNull(command);
    }

    @Override
    public String getBaseCommand() {
        return command;
    }

    @Override
    public void writeMail(@Nonnull String queueID, @Nonnull OutputStream outputStream)
    throws IOException
    {
        if (!PostfixUtils.isValidQueueID(queueID)) {
            throw new IllegalArgumentException(String.format("queueID %s is invalid", queueID));
        }

        try {
            executeCommand(List.of("-q", queueID), null, outputStream);
        }
        finally {
            IOUtils.closeQuietly(outputStream);
        }
    }
}
