/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.postfix;

import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.core.common.util.DomainUtils.DomainType;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Postfix utility functions
 */
public class PostfixUtils
{
    /*
     * Reg ex for validating a queue id
     */
    private static final Pattern QUEUE_ID_PATTERN = Pattern.compile("\\w{1,32}");

    /*
     * Postfix uses comma or whitespace seperated lines to specify a list of items
     */
    private static final Pattern LIST_PATTERN = Pattern.compile("[\\s,]+");

    private PostfixUtils() {
        // empty on purpose
    }

    /**
     * Returns True if queueID is a valid Postfix queue ID.
     */
    public static boolean isValidQueueID(String queueID)
    {
        boolean valid = false;

        if (queueID != null)
        {
            Matcher matcher = QUEUE_ID_PATTERN.matcher(queueID);

            valid = matcher.matches();
        }

        return valid;
    }

    /**
     * Postfix has strict requirements for a valid hostname
     */
    public static boolean isValidHostname(String hostname)
    {
        hostname = StringUtils.trimToNull(hostname);

        if (hostname == null) {
            return false;
        }

        // domain upper limit
        if (hostname.length() > 1024) {
            return false;
        }

        if (!DomainUtils.isValid(hostname, DomainType.FRAGMENT)) {
            return false;
        }

        // The default postfix configuration "calculates" the domain value from the hostname by using the part after
        // the first dot (.). Postfix however does not accept a domain value with only digits. For example if the
        // hostname is set to a.1.2.3, domain will be set to 1.2.3 which is not allowed by postfix. We will therefore
        // check if hostname is illegal for postfix.
        String domain = StringUtils.trimToNull(StringUtils.substringAfter(hostname, "."));

        if (domain != null) {
            return !Pattern.matches("\\d+(\\.+\\d+)*", domain);
        }
        // Hostname cannot be a number
        else return !Pattern.matches("\\d+", hostname);
    }

    /**
     * Postfix uses comma or whitespace seperated lines to specify a list of items
     */
    public static String[] splitList(String input)
    {
        input = StringUtils.trimToNull(input);

        if (input == null) {
            return new String[]{};
        }

        return LIST_PATTERN.split(input);
    }
}
