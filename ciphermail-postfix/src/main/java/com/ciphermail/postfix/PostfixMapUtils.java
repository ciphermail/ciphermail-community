/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.postfix;

import org.apache.commons.lang3.StringUtils;

/**
 * Utility class for PostfixMap package
 */
public class PostfixMapUtils
{
    private PostfixMapUtils() {
        // empty on purpose
    }

    /**
     * Returns the filename for the given map name and map type. The filename is returned as "type-name"
     *
     * @throws IllegalArgumentException if name or type is invalid
     */
    public static String getFilename(MapType type, String name)
    {
        name = StringUtils.trimToNull(name);

        if (name == null) {
            throw new IllegalArgumentException("name is empty");
        }

        if (type == null) {
            throw new IllegalArgumentException("type is null");
        }

        return StringUtils.lowerCase(type.toString()) + "-" + name;
    }

    /**
     * Returns the name part from a filename. The filename should be of the form "type-name"
     *
     * @throws IllegalArgumentException if filename is invalid
     */
    public static String getName(String filename)
    {
        filename = StringUtils.trimToNull(filename);

        if (filename == null) {
            throw new IllegalArgumentException("filename is empty");
        }

        String[] parts = StringUtils.split(filename, "-", 2);

        if (parts == null || parts.length != 2) {
            throw new IllegalArgumentException("filename is invalid");
        }

        return parts[1];
    }

    /**
     * Returns the type from a filename. The filename should be of the form "type-name"
     *
     * @throws IllegalArgumentException if filename is invalid
     */
    public static MapType getType(String filename)
    {
        filename = StringUtils.trimToNull(filename);

        if (filename == null) {
            throw new IllegalArgumentException("filename is empty");
        }

        String[] parts = StringUtils.split(filename, "-", 2);

        if (parts == null || parts.length != 2) {
            throw new IllegalArgumentException("filename is invalid");
        }

        MapType type = MapType.fromString(parts[0]);

        if (type == null) {
            throw new IllegalArgumentException("type is invalid");
        }

        return type;
    }
}
