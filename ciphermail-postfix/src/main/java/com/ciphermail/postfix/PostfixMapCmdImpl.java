/*
 * Copyright (c) 2018-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.postfix;

import com.ciphermail.core.common.util.LFInputStream;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Executes the external script to configure SMTP maps
 */
public class PostfixMapCmdImpl extends PostfixBaseCommand implements PostfixMapCmd
{
    /*
     * The command to run
     */
    private final String command;

    public PostfixMapCmdImpl(@Nonnull String command) {
        this.command = Objects.requireNonNull(command);
    }

    @Override
    public String readMap(@Nonnull String filename)
    throws IOException
    {
        StringWriter outputWriter = new StringWriter();

        OutputStream outputStream = new SizeLimitedOutputStream(WriterOutputStream.builder()
                .setCharset(StandardCharsets.UTF_8).setWriter(outputWriter).get(),
                getMaxOutputSize(), false);

        try {
            executeCommand(List.of("--read-map", filename), null, outputStream);
        }
        finally {
            IOUtils.closeQuietly(outputStream);
        }

        return StringUtils.trim(outputWriter.toString());
    }

    @Override
    public void writeMap(@Nonnull String filename, @Nonnull String content)
    throws IOException
    {
        executeCommand(List.of("--write-map", filename), new LFInputStream(
                new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8))), null);
    }

    @Override
    public @Nonnull List<String> listMaps()
    throws IOException
    {
        StringWriter outputWriter = new StringWriter();

        OutputStream outputStream = new SizeLimitedOutputStream(WriterOutputStream.builder()
                .setCharset(StandardCharsets.UTF_8).setWriter(outputWriter).get(),
                getMaxOutputSize(), false);

        try {
            executeCommand(List.of("--list-maps"), null, outputStream);
        }
        finally {
            IOUtils.closeQuietly(outputStream);
        }

        String output = StringUtils.trimToNull(outputWriter.toString());

        return output != null ? Pattern.compile("\\R").splitAsStream(output).toList() : Collections.emptyList();
    }

    @Override
    public void deleteMap(@Nonnull String filename)
    throws IOException
    {
        executeCommand(List.of("--delete-map", filename));
    }

    @Override
    public String getBaseCommand() {
        return command;
    }
}
