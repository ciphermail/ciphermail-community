/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.postfix;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Interface to the Postfix postsuper command.
 */
public interface PostSuper
{
    enum QueueName{HOLD, INCOMING, ACTIVE, DEFERRED}

    /**
     * Delete one message with the named queue ID from the named mail queue(s) (default: hold, incoming,
     * active and deferred).
     */
    void deleteMailFromQueue(@Nonnull String queueID, QueueName queueName)
    throws IOException;

    /**
     * Put mail "on hold" so that no attempt is made to deliver it.  Move one message with the named queue ID from the
     * named mail queue(s) (default: incoming, active and deferred) to the hold queue.
     */
    void holdMailFromQueue(@Nonnull String queueID, QueueName queueName)
    throws IOException;

    /**
     * Release mail that was put "on hold".  Move one message with the named queue ID from the named mail queue(s)
     * (default: hold) to the deferred queue.
     */
    void releaseHoldMailFromQueue(@Nonnull String queueID, QueueName queueName)
    throws IOException;

    /**
     * Request forced expiration for one message with the named queue ID in the named mail queue(s) (default: hold,
     * incoming, active and deferred).
     * The message will be returned to the sender when the queue manager attempts to deliver that message
     */
    void bounceMailFromQueue(@Nonnull String queueID, QueueName queueName)
    throws IOException;

    /**
     * Requeue the message with the named queue ID from the named mail queue(s) (default: hold, incoming, active and
     * deferred).
     */
    void requeueMailFromQueue(@Nonnull String queueID, QueueName queueName)
    throws IOException;
}
