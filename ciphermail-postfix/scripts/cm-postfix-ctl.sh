#!/bin/bash

set -e
set -o pipefail

#
# Description: Script for managing postfix

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

usage()
{
    echo "usage: postfix (start | stop | restart | status | running)" >&2

    exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

start_postfix() {
    systemctl start postfix
}

stop_postfix() {
    systemctl stop postfix
}

restart_postfix() {
    systemctl restart postfix
}

status_postfix() {
    systemctl status postfix
}

is_running()
{
    running='true'

    systemctl status postfix > /dev/null 2>&1 || running='false'

    echo "$running"
}

command=$1

shift

case "$command" in
    start)
        start_postfix
    ;;
    stop)
        stop_postfix
    ;;
    restart)
        restart_postfix
    ;;
    status)
        status_postfix
    ;;
    running)
        is_running
    ;;
    *)
        usage
esac

exit 0
