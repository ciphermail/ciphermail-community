package com.ciphermail.rest.server.service;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.time.Duration;

/**
 * Service which keeps track of authentication failures for admin and IP address. If the number of authentication
 * failures for an admin from an IP address (or in case of IPv6 a network address) exceeds the max,
 * {@link AuthenticationFailureCache#isBanned(String)}  returns true. Failed logins will be cached for some time
 * after which they will be removed from the cache.
 */
@SuppressWarnings("java:S6813")
@Component
public class AuthenticationFailureCache
{
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationFailureCache.class);

    @Value("${ciphermail.rest.authentication.max-login-failures}")
    private int maxLoginFailures;

    /*
     * Limits the size of the cached username to prevent memory exhaustion attack
     */
    @Value("${ciphermail.rest.authentication.failure-cache-max-username-length:64}")
    private int maxUsernameLength;

    /*
     * Cache of all login failures
     */
    private final LoadingCache<String, Integer> loginFailureCache;

    @Autowired
    private ClientAddressProvider clientAddressProvider;

    public AuthenticationFailureCache(
            @Value("${ciphermail.rest.authentication.login-failure-lifetime-seconds}") int loginFailureLifetimeSeconds,
            @Value("${ciphermail.rest.authentication.login-failure-max-cached-items}") int loginFailuremaxCachedItems)
    {
        super();

        loginFailureCache = Caffeine.newBuilder()
                .expireAfterWrite(Duration.ofSeconds(loginFailureLifetimeSeconds))
                .maximumSize(loginFailuremaxCachedItems)
                .build(
                    new CacheLoader<>() {
                        @Override
                        @Nonnull public Integer load(@Nonnull String key) {
                            // set initial value to 0
                            return 0;
                        }
                    });
    }

    private String getKey(String username) {
        return clientAddressProvider.getRemoteAddress()
               + ":"
               + StringUtils.left(StringUtils.lowerCase(username), maxUsernameLength);
    }

    public void registerFailedLogin(String username)
    {
        int attempts;

        String key = getKey(username);

        attempts = loginFailureCache.get(key);

        attempts++;

        loginFailureCache.put(key, attempts);

        logger.debug("loginFailureCache size {}", loginFailureCache.estimatedSize());
    }

    public boolean isBanned(String username) {
        return loginFailureCache.get(getKey(username)) >= maxLoginFailures;
    }
}