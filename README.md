# CipherMail Community Gateway

This repository contains the 'open core' code that powers the CipherMail Email
Encryption Gateway. It contains everything you need to build the libraries and
OS packages of the Community Edition.

For more information on the Enterprise Edition and paid support, visit our
website <https://www.ciphermail.com/>.

**the main branch is the development branch and can be unstable. Please select a stable branch (for example ``stable-5.x_4.x``)
if you want to build a stable release**

## End-user documentation

This README is meant to offer help with developing, testing and releasing
CipherMail Email Encryption Gateway. For installation instructions, operational
guides and other end-user documentation, visit
<https://www.ciphermail.com/documentation>

## Setting things up

### Install dependencies 

On Debian GNU/Linux, this should install everything you need:
```shell
apt install \
    ansible \
    ant \
    ant-contrib \
    build-essential \
    createrepo-c \
    debhelper \
    dpkg-dev \
    fakeroot \
    git \
    git-lfs \
    gnupg \
    jq \
    libxml2-utils \
    make \
    mariadb-client \
    maven \
    openjdk-17-jdk \
    podman \
    podman-compose \
    postgresql-client \
    rpm \
    unzip \
    vagrant \
    vagrant-libvirt \
    wget
```

### Start the Docker service containers

```shell
podman-compose up -d
```

Note that your Unix user account needs to be in the `docker` group for this to work.

### Configure parent pom

The ciphermail-community git repo is mirrored from our main repo using the ``git subtree`` command. 
The community pom file references the parent pom. However, because only the community tree is mirrored, the parent 
pom is not directly accessible.

Because the project cannot be build if the parent pom is missing, we added a symlink to the parent pom under the name 
``parent.pom.xml``.

There are two options to configure the parent pom

1. Copy ``parent.pom.xml`` to the parent dir of the ciphermail-community project and rename it to pom.xml
    ```shell
    cp parent.pom.xml ../pom.xml
    ```
2. Configure a relative path to parent pom:

```xml
  <parent>
    <groupId>com.ciphermail</groupId>
    <artifactId>ciphermail</artifactId>
    <version>${revision}</version>
    <relativePath>./parent.pom.xml</relativePath> <!-- ==== ADD THIS LINE === -->
  </parent>
```

## Unit testing

The back-end unit tests can be started using maven:

```shell
mvn test
```

To test against MariaDB instead of the default PostgreSQL database, set the corresponding JUnit JVM system property like
this:

```shell
mvn -Dciphermail.hibernate.connection.driver_class=org.mariadb.jdbc.Driver \
    -Dciphermail.hibernate.connection.url=jdbc:mariadb://127.0.0.1:3306/ciphermail-test \
    test
```

## Run back-end locally

### Setup

To run certain commands, you need a sudoers configuration fragment that allows you to execute
some scripts as root without a password.

`sudo visudo /etc/sudoers.d/ciphermail`

Copy below sudo fragment (between start/end excluding the start/end line)

Note: replace `<YOUR-UNIX-NAME>` with your own linux username

```
# sudo fragment for CipherMail

User_Alias CIPHERMAIL_BACKEND_USERS = <YOUR-UNIX-NAME>

Cmnd_Alias CIPHERMAIL_BACKEND_COMMANDS = \
/usr/sbin/cm-postfix-configure-main.sh, \
/usr/sbin/cm-postfix-ctl.sh, \
/usr/sbin/cm-postfix-map.sh, \
/usr/sbin/postcat, \
/usr/sbin/postconf, \
/usr/sbin/postsuper, \
/usr/sbin/postqueue, \
/usr/bin/journalctl

CIPHERMAIL_BACKEND_USERS ALL=(ALL) NOPASSWD: CIPHERMAIL_BACKEND_COMMANDS

Defaults:CIPHERMAIL_BACKEND_USERS !requiretty,!syslog
```

Add symlinks to some scripts:

```shell
sudo ln -fs <GIT-HOME>/ciphermail-community/ciphermail-postfix/scripts/cm-postfix-configure-main.sh /usr/sbin/
sudo ln -fs <GIT-HOME>/ciphermail-community/ciphermail-postfix/scripts/cm-postfix-ctl.sh /usr/sbin/
sudo ln -fs <GIT-HOME>/ciphermail-community/ciphermail-postfix/scripts/cm-postfix-map.sh /usr/sbin/
```

Replace `<GIT-HOME>` with your git dir

Create config dirs:

```shell
sudo mkdir -p /etc/ciphermail/pki/tls/private
sudo mkdir -p /etc/ciphermail/
sudo bash -c 'chown -R $SUDO_USER /etc/ciphermail'
```

## Start back-end

To start the Gateway backend locally for development purposes:

```shell
sudo mkdir -p /run/ciphermail
sudo bash -c 'chown $SUDO_USER /run/ciphermail'
mvn -pl com.ciphermail:ciphermail-gateway-backend -am spring-boot:run
```

**NOTE:** /run/ciphermail should be owned by your user account

## Start ciphermail-shell

To start the ciphermail shell application:

```shell
mvn -Dspring-boot.run.jvmArguments="-Dciphermail.shell.base-url=http://localhost:8000 \
  -Dciphermail.shell.username=admin \
  -Dciphermail.shell.password=admin" \
  -pl com.ciphermail:ciphermail-shell -am spring-boot:run
```

Note: replace the `base-url`, `username` and `password` values with the correct values

## Build native version of ciphermail-shell

To build a native version of ciphermail-shell, GraalVM must be installed.

GraalVM can be downloaded from https://www.graalvm.org/downloads/

```shell
wget https://download.oracle.com/graalvm/22/latest/graalvm-jdk-22_linux-x64_bin.tar.gz
```
```shell
sha256sum graalvm-jdk-22_linux-x64_bin.tar.gz
```
```d583cdb01ca023a37eed45d9d184b68b7a8d7f50b58dde1369041f294c34f4a3  graalvm-jdk-22_linux-x64_bin.tar.gz```

Untar to ```/opt/```

```shell
sudo tar xzf graalvm-jdk-22_linux-x64_bin.tar.gz -C /opt/
```

Create a symlink to graalvm

```shell
sudo ln -fs /opt/graalvm-jdk-22.0.1+8.1/ /opt/graalvm
```

Replace  ```graalvm-jdk-22.0.1+8.1``` with the correct version.

build a native version

```shell
export GRAALVM_HOME=/opt/graalvm
mvn package -Pnative -DskipTests
```

The native version can be started with:

```shell
CIPHERMAIL_SHELL_USERNAME=admin CIPHERMAIL_SHELL_PASSWORD=admin ./ciphermail-shell/target/ciphermail-shell
```

## Maven

Maven is used for compiling and building the jars.

Some maven commands:

List all dependecies:

```shell
mvn test-compile dependency:tree
```

Create all jars without running the tests:

```shell
mvn package -DskipTests
```

## Building packages

Build Debian and RPM packages:

```shell
ant build-packages
```

## Validate jar dependency checksums

To make builds deterministic, all jar dependencies will be hashed. We can then check whether downloaded jars have the
correct checksum.

To generate the checksums files:

```shell
./scripts/maven-dependency-validator.sh --generate-checksums
```

A checksum file with name `maven-dependency-checksum.sha256` will be created in each maven module directory.

To validate the checksums files:

```shell
./scripts/maven-dependency-validator.sh --validate-checksums
```

A checksum file with name `maven-dependency-checksum.sha256.new` will be created in each maven module directory and
this file will be compared (using diff) with the existing checksum file. If there is a mismatch, the command will
exit with a non-zero exit code.

## Create database schema's

PostgreSQL:

```shell
mvn package -DskipTests

java -cp 'ciphermail-gateway-backend/target/lib/*' \
    com.ciphermail.core.app.tools.GenerateSchema \
    --url jdbc:postgresql://127.0.0.1/ciphermail \
    --driver-class org.postgresql.Driver \
    --username ciphermail \
    --password ciphermail \
    --output-file conf/sql/ciphermail.postgres.15.sql
```

MariaDB:

```shell
mvn package -DskipTests

java -cp 'ciphermail-gateway-backend/target/lib/*' \
    com.ciphermail.core.app.tools.GenerateSchema \
    --url jdbc:mariadb://127.0.0.1:3306/ciphermail \
    --driver-class org.mariadb.jdbc.Driver \
    --username ciphermail \
    --password ciphermail \
    --output-file conf/sql/ciphermail.mariadb.10_5.sql
```

Copyright (C) 2008-2025  CipherMail B.V.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License version 3, 19 November 2007
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.
