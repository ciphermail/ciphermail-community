This document lists the release notes of the CipherMail Email Encryption
Gateway. Important changes to the ciphermail-community project are also listed
here. Changes that only affect the professional/enterprise version are marked
with '[PRO/ENT]'. Special changes for enterprise customers are marked with
'[ENT]'.

The release dates shown here represent the dates the finished builds were made
available through our distribution channels. There is often a delay between the
source code releases (or tags) and the releases of the finished builds.

### 5.5.3.49 (2024-09-4)

Maintenance release

**Bug fixes**

- Add deep MIME scanning for text/calendar MIME parts
  This fixes a bug where Team invite messages with additional attachments were not detected as being a calendar message

**New features**

- Add Ansible support for java system http proxy and additional system properties
- Certificate request can be modified using an external script
- Add Ansible support for additional dnf configuration

**Technical changes**

- Fix accessibility issues (WIP) reported by WAVE (Web Accessibility Evaluation Tool) https://wave.webaim.org/

### 5.5.3.6 (2023-05-24)

Minor update of ciphermail-console, ciphermail-ansible and ciphermail-appliance-core package.

**Bug fixes**

- Fix high CPU usage for the console app when the ssh connection is terminated unexpectedly. The console app results
  in high CPU load if the ssh connection is terminated when the shell option is selected. This issue is more likely
  to happen on VMs running on Azure because Azure by default uses a very short TCP timeout value.

**Technical changes**

- RHEL8 upgraded to 8.8. [PRO/ENT]
- Ansible updated to 7.2. [PRO/ENT]
- Add support for Securosys HSM proxy. [PRO/ENT]
- Send email notification if update check fails. [PRO/ENT]

### 5.5.3 (2023-05-08)

**New features**

- Support changing the email logo from the GUI. [PRO/ENT]
- Add haproxy Ansible role. [PRO/ENT]
- Add Securosys primus HSM Ansible role. [PRO/ENT]

**Technical changes**

- PDF mail templates now include a logo.
- Text areas for some pages now vertically scale better on high-res screens.
- Additional PDF settings menu item moved to "other" pull-down menu.
- Community edition Virtual Appliance now uses AlmaLinux 8.
- Support changing certbot acme URL. [PRO/ENT]
- By default, postfix master.cf is managed by Ansible. This can however be overridden by setting the following ansible
  variable to false: `common__manage_postfix_master_config`. [PRO/ENT]

**Bug fixes**

- Fix password policy issue when using a weighted policy
  The check which checks that m (minimal match) should be less or equal to the number of rules was no longer correct
  because a password policy rule can define a weight > 1
- Fix German language for PDF page.
- Support PGP keys with email address which are not enclosed in angle brackets ("<" and ">").
- Fix SRS bug when time slot overflows. [PRO/ENT]

### 5.5.0-5.5.2 (N/A)

Version 5.5.0-5.5.2 were internal releases.

### 5.4.1 (2022-09-14)

**New features**

- Ability to configure static IPv4 or IPv6 address using Ansible. [PRO/ENT]

**Technical changes**

- The Postfix master process configuration is now fully managed with Ansible.
  [PRO/ENT]
- MariaDB cluster nodes now use a better-supported TLS method for state
  snapshot transfers. [PRO/ENT]

### 5.4.0 (N/A)

Version 5.4.0 was an internal release.

### 5.3.0 (2022-09-06)

**New features**

- Add option in the administrative web interface to generate a 3072-bit RSA
  key.
- Add support for Sender Rewriting Scheme (SRS). [PRO/ENT]

**Technical changes**

- Update dependencies: update Bouncycastle to 1.71, Ansible to 5.4, Jetty to
  9.4.48.
- The Ansible playbook after a CipherMail package update now runs
  asynchronously. [PRO/ENT]

**Bug fixes**

- Inrease console app heap size to 8 MB in order to prevent a (rare) crash of
  the CipherMail Console application.
- Fix Ansible idempotency issue related to fetchmail configuration. [PRO/ENT]

### 5.2.4 (2022-05-31)

**Bug fixes**

- A recent systemd update resulted in the unexpected cleaning of temporary
  files that have not been written to in at least 10 days. Whenever this
  happened, the administrative web interface and user portal would become
  dysfunctional until restarted. This has been fixed by adding configuration
  that prevents systemd from periodically cleaning the temporary files used by
  the CipherMail frontend.

### 5.2.3 (2022-05-11)

**New features**

- Ansible playbook output now gets logged to /var/log/ciphermail-ansible.log
  and is included in support dumps. [PRO/ENT]

**Technical changes**

- Update patched python3-dnf-plugin-post-transaction-actions package to version
  4.0.21-11.1.el8. [PRO/ENT]

### 5.2.2 (2022-05-04)

**Bug fixes**

- Pin MariaDB to custom CipherMail version <= 3:10.3.32-2.1 because MariaDB
  3:10.3.32-2 breaks State Snapshot Transfers. [PRO/ENT]
- Pin python3-dnf-plugin-post-transaction-actions to the CipherMail-patched
  version of 4.0.21-4.1.el8. The Red Hat-provided version contains a bug that
  prevents actions from running under specific circumstances. [PRO/ENT]
- Fixed an issue where an interrupted Ansible playbook did not result in a
  warning in the administrative web interface. [PRO/ENT]
- Prevent DNF from restoring the default MariaDB Galera configuration. The
  default configuration conflicts with the CipherMail-provided configuration,
  causing the service startup to fail. [PRO/ENT]

### 5.2.1 (2022-04-25)

**New features**

- The inactivity timeout of the administrative web interface can now be
  configured with Ansible. [PRO/ENT]
- If an error occurs during Ansible playbook execution, the error will now be
  logged. A warning will also be shown in the administrative web interface.
  [PRO/ENT]
- Postfix transport maps can now be managed with Ansible. [PRO/ENT]
- Nagios/Icinga check scripts are now provided for use with the CipherMail
  monitoring endpoints. [PRO/ENT]
- Microsoft Information Protection (MIP) decryption support added. [PRO/ENT]
- Add support for GlobalSign Atlas. The Gateway can now automatically request
  GlobalSign issued certificates. [PRO/ENT]
- Add support for monitoring MariaDB. [PRO/ENT]

**Technical changes**

- Upgrade log4j from version 1.2.15 to 2.17.1. Because of changes to log4j the
  logger levels can no longer be managed from the administrative web interface.
  We therefore removed this option. Log levels should now be set in
  conf/log4j2.xml. Changing the log level does not require a restart because
  the change will be applied automatically after 60 seconds.
- Remove CipherMail Console log directory.
- Update dependencies: update jquery to 3.6.0, update owasp-java-html-sanitizer
  to 20211018.2, update HTTP client to 5.1.3, update Apache POI to 5.2.0,
  update cxf to 3.3.13, update XML sec to 2.1.7, update jasypt to 1.9.3,
  update wss4j to 2.2.7, update netty to 4.1.75, update spring to 5.3.18,
  update activeMQ to 5.16.0, update libphonenumber to 8.12.45.
- Make console script crash-proof, i.e., fallback to shell if console script
  fails.
- Support for 32bit Linux was dropped. If 32bit Linux support is required, a
  32bit version of Java wrapper must be manually installed.

**Bug fixes**

- The postrm script of the 'djigzo' Debian package creates a trap with a
  non-existing script.

### 5.2.0 (N/A)

Version 5.2.0 was an internal release.

### 5.1.4 (2022-02-09)

**Bug fixes**

- Microsoft recently shut down the Office 365 cloud components in Germany. This
  caused HTTP 400 errors to occur in the CipherMail web service client that
  obtained the Office 365 IP ranges through a Microsoft-provided API. This
  release removes the German API endpoint configuration from the web service
  client.
- On IPv4-only hosts, the Postfix MTA would in some cases attempt outbound
  connections over IPv6. These connection attempts would be put in caches and
  logs, which made it harder to troubleshoot connection problems.

### 5.1.3 (2021-11-24)

**SECURITY fixes**

- The portal password policy functionality has been redesigned. A complex
  password policy can now be configured. The default portal password policy is
  set to require at least 8 characters. This is in accordance with the latest
  NIST password guidelines. The old "Min Password Strength" option has been
  removed. If "Min Password Strength" was set to a custom value, please make
  sure to revisit this configuration.
- Control characters and null characters now get removed from log output and
  subject headers. Because headers can be encoded (for example with base64),
  null values and control characters can be added to the subject and other parts
  of the message. Because of a bug in the Java wrapper, logging a null character
  can temporarily stop log output.

**New features**

- Full IPv6 support. CipherMail products are now fully functional and supported
  in IPv4-only, dual-stack and IPv6-only networks. This includes cluster setups.
- More configuration can now be managed with Ansible: system timezone,
  authorized SSH keys, GRUB, serial console, IPv6 support, DHCPv6 client,
  passwordless sudo, custom Postfix options, additional trusted CA certificates
  and secure syslog forwarding. [PRO/ENT]
- A new option called 'Subject Password Trigger' allows the sender to specify a
  password on the email subject line. The password is extracted from the subject
  line and is then used to encrypt the email using PDF encryption. To prevent
  the sender from selecting a weak password, a password policy can be defined.
  If the password is not strong enough, the email will not be sent and the
  sender will be notified.
- HTML email is now fully supported with PDF encryption. The PDF document will
  have similar-looking markup as the original HTML message. Inline images inside
  the email also end up in the PDF document. The PDF file is created from a
  configurable template and can be modified to match the corporate identity.
  [PRO/ENT]
- Support for SCRAM authentication with PostgreSQL 14. The Postgres JDB library
  was updated to 42.2.24 in order for this to work.
- P12Tool (previously PfxTool) now has a renew function.

**Technical changes**

- The default playbook execution has changed. The new default is to execute the
  playbook only against the local machine. Any inventory changes you've made are
  now automatically synchronized to any other cluster nodes, and will be applied
  once the playbook is started on those hosts as well. This happens
  automatically on every CipherMail package update. You can run the playbook
  against all inventory hosts using the `--all-hosts` flag.
- Each Ansible play is now executed against one host at a time when executing
  the playbook against all inventory hosts. Running the whole playbook now takes
  longer when managing a CipherMail cluster, but this prevents situations where
  a failure in the playbook affects the whole cluster. [PRO/ENT]
- If a valid database connection cannot be established, the back-end service
  will now keep trying indefinitely.
- When a database connection is retrieved from the database pool, a check is
  done to verify whether the database connection is valid.
- We've added non-strict email address checking functions to EmailAddressUtils.
  Not all email addresses used in practice are valid according to RFC 2822. For
  example, email addresses are not allowed to end with a full stop. In practice
  however, most mail servers accept email addresses ending with that character.
  In situations where email checking is not required to be strict, the full stop
  will not be treated as an error.
- Because the password policy is now configurable, the portal pages where the
  password can be configured no longer show the password policy. A "password
  policy URL" option has been added which can point to a self-hosted page that
  can explain the configured password policy.
- Ansible playbook execution now utilizes SSH connection pipelining, reducing
  execution times by some 40%. [PRO/ENT]
- All (feemarker) templates are now validated when saved. This requires that the
  template is safe for null values, i.e., if a variable is null, the variable
  should expand to a default value or not be used.
- All supplied (freemarker) templates are now null-value safe.
- Add `!syslog` and `!pam_session` to the default sudo setting for the back-end
  user to stop logging unnecessary messages to /var/log/messages.
- Remove deprecated SHA-1 admin password encoding. If the admin password was set
  in version <= 2.8.6 and the password was never changed, you need to reset your
  admin password.
- New portal passwords are now encoded with the bcrypt password hashing
  algorithm.
- Convert 8bit MIME parts to 7bit before S/MIME or PGP signing. This makes it
  easier to create email templates because the templates no longer need to be
  quoted-printable encoded.
- PDF email templates now contain an alternative HTML part. Some parts of the
  PDF email templates can be modified (for example the logo URL and footer)
  without having to completely rewrite the template.
- Minor changes: system library updates, PfxTool renamed to P12Tool, P12Tool now
  uses long arguments.
- The script which is used to set the hostname, should only add a localhost
  entry to /etc/hosts if there is no forward lookup for the hostname.
- cm-cluster-control command line tool have been replaced by cm-cluster-manage [PRO/ENT]

**Bug fixes**

- MariaDB Galera cluster instance startup will now be delayed until the host is
  online (that is, the network-online systemd target has been reached). This
  prevents a situation where MariaDB would have to be manually restarted after a
  host reboot. [PRO/ENT]
- CipherMail MPA and web logs were correctly saved by rsyslog, but were
  inadvertently excluded from syslog forwarding. This has been resolved.
- Fix PAM lockout. Clicking the apply button on the 'Administrators' page caused
  the application to disable PAM logins, potentially locking out all
  administrators.
- The default PGP key server is now set to keys.openpgp.org because the old key
  server (ha.pool.sks-keyservers.net) is no longer active.
- DNS settings were not configured on RHEL 8. [PRO/ENT]
- The Azure image would start with network configuration for both classic
  interface names and the new, systemd-style predictable interface names.
  Microsoft discourages use of the latter on Azure. We have modified our image
  building process so that only classic interface names are used for Azure
  images.
- The DigitalOcean image would start with network configuration for both classic
  interface names and the new, systemd-style predictable interface names.
  Predictable interface names actually work well on DigitalOcean. We have
  modified our image building process so that only predictable interface names
  are used for DigitalOcean images, just like our non-cloud images.
- Restarting the back-end from the GUI, resulted in a stopped back-end, i.e.,
  the back-end was not restarted.

### 5.1.0 - 5.1.2 (N/A)

Versions 5.1.0 through 5.1.2 were internal releases.

### 5.0.5 (2021-08-31)

Version 5.0.5 was previously released as 5.0.4.1gf13471e.

**Bug fixes**

- Fix for MariaDB maximum packet size. MariaDB by default only allows packets
  with a maximum size of 16 MB. The `max_allowed_packet` configuration variable
  should be set to a greater value to allow for large CRLs and quarantine
  emails. Previous releases of the community edition used 128MB for
  `max_allowed_packet`. This setting was missing in the community virtual
  appliance version 5.0.4.

### 5.0.4 (2021-07-06)

**Bug fixes**

- Fix for PGP key expiration date calculation.  When importing a PGP key, the
  expiration date of the key is calculated and stored in the database. The
  expiration data calculator stopped checking other certification types when a
  valid expiration date was returned. A key however might have different (self)
  certification types with different expiration times. The expiration date
  calculator should check all (self) certification types.
- The virtual appliance OVA file should not contain the nvram file because older
  vSphere Web Clients cannot import an ova file which contains an nvram file.
  vSphere Web Client error report: The "Deployed an OVF with NVRAM is not
  supported (incompatible vCenter version)."

### 5.0.3 (N/A)

Version 5.0.3 was an internal release.


### 5.0.2 (2021-05-28)

**SECURITY fixes**

- The TLS settings used by Postfix now exclude configurations that are deemed
  insufficient by NCSC-NL, like the SSLv3 protocol and RC4 encryption algorithm.
  Ref:
  [NCSC-NL TLS
  Guidelines](https://english.ncsc.nl/publications/publications/2021/january/19/it-security-guidelines-for-transport-layer-security-2.1)
- Patched JQuery 1.12.4 to fix all open security issues.

**New features**

- PAM authentication added to the administrative web interface. Administrators
  can now log in with their Unix credentials. PAM authentication can be disabled
  from the Admin page (after logging in) or by adding the properties file
  `conf/djigzo.properties.d/disable-pam.properties` with content
  `pam.enabled=false`.

**Technical changes**

- The enterprise virtual appliance is now based on RHEL 8. [PRO/ENT]
- The community virtual appliance is now based on CentOS Stream 8.
- CipherMail core packages (djigzo, djigzo-web) now require the
  ciphermail-core-os package. There are two packages that provide the new
  ciphermail-core-os dependency: ciphermail-core-os-no-deps and
  ciphermail-core-os-rhel8. When installing on RHEL 8 or CentOS Stream 8, you
  should use ciphermail-core-os-rhel8. In other cases, use
  ciphermail-core-os-no-deps.
- The back-end log file is now written to
  `/var/log/ciphermail-gateway-backend.log`.
- The front-end log file is now written to
  `/var/log/ciphermail-webmail-frontend.log`
- A default built-in administrative user is no longer created on first start.
  Administrators should log in with their Unix account after which they can
  configure new administrative users if needed.
- The IP filter properties file (for the Web GUI) was moved from
  `/etc/djigzo/ip-filter.properties` to
  `/etc/ciphermail/ip-filter/ip-filter.properties`.
- Replaced `service` commands with `systemctl` in all scripts. The back-end
  should now be started with `systemctl restart ciphermail-gateway-backend` and
  the front-end should be started with `systemctl restart
  ciphermail-gateway-frontend`.
- The graphs to show on the admin homepage are now read from a json file.
  [PRO/ENT]
- Add option to import/export root certificates with the CertStore tool (use
  --root-store option).
- The Fetchmail service is no longer enabled by default.
- The restore function of the backup page is now only enabled if the user is
  logged in via PAM. i.e., with a Unix account. The additional system password
  field for restoring has been removed.
- Files from the application directory are now by default owned by root. Files
  and directories that should be owned by the back-end user are excluded.
- There is now only one build of the console app which is shared by the gateway
  and ciphermail portal.

**Bug fixes**

- The Certbot manage script could no longer detect whether or not a Let's
  Encrypt certificate was available or not because the text returned by Certbot
  was changed. We now check whether the dir /etc/letsencrypt/live/ciphermail
  exists or not. [PRO/ENT]
- Fix PGP key expiration logic. If a key signature has no expiration date and
  is the most recent signature, the key should never expire even if there are
  older signatures that expire.

**Miscellaneous**

- Some password fields are now configured with autocomplete="new-password" to
  prevent autofilling.

### 5.0.0 - 5.0.1 (N/A)

Versions 5.0.0 and 5.0.1 were internal releases.

### 4.11.0 (2021-02-08)

**New features**

- Add option to select if database and/or other files should be included with
  backup/restore.
- JMS support added (activeMQ). The built-in CA generates an event when a new
  certificate is generated. PGPSecretKeyRequestor generates and event when a new
  PGP keyring is created.
- Event handler for certificate generator event added which can be used to
  upload the certificate to an external LDAP database. [PRO/ENT]
- Event handler for new PGP keyring event added which can be used to upload the
  PGP key to some remote system. [PRO/ENT]
- Location and owner (group) of the postfix socketmap domain socket is now
  configurable. [PRO/ENT]

**Technical changes**

- LDAP certificate lookup script refactored. Setting overrides can be configured
  in a defaults include file. Recipients can be excluded or included from a
  lookup. [PRO/ENT]
- Enable the network interface (i.e., set ONBOOT to true) when the interface is
  configured (the offline installer starts with a disabled interface). [PRO/ENT]

### 4.10.0 (2020-11-19)

**New features**

- PGP ECC support added. ECC keys can now be generated. The gateway can now use
  ECC keys for encryption and decryption.
- PGP option "Auto select signing algorithm" added. This is enabled by default.

**Technical changes**

- BC jars updated to 1.67.
- start.sh removed. To start the back end locally for development, use the ant
  start-back task.
- The EJBCA configuration has been moved to its own file
  (ejbca-certificate-request-handler.xml).
- For unit testing and development, a Docker Compose file is added which starts
  PostgreSQL, MariaDB and SKS keyserver containers.
- The back-end project is now called ciphermail-core. For backward
  compatibility, the Debian and RPM package names will not change.
- The front-end project is now called ciphermail-gateway-web. For backward
  compatibility, the Debian and RPM package names will not change.
- The jars from ciphermail-core are now used in multiple projects.
- Templates are now sorted (with the exception of custom templates, which always
  come at the end).
- Unbound and OpenLDAP Dockerfiles added for unit testing. [PRO/ENT]
- Sun PKCS11 provider was changed from Java 8 to Java 11 in a non-backward
  compatible way. Because we never used the Sun PKCS#11 provider, we removed the
  code. [ENT]

**Miscellaneous**

- Logo color changed to match new color scheme.
- We have migrated all our Subversion repositories to Git. The source code of
  ciphermail-core and ciphermail-gateway-web are now mirrored on
  [GitLab.com](https://gitlab.com/ciphermail).

### 4.9.1 (2020-07-07)

**New features**

- PGP "private key alias" and "key algorithm" added to PGP view.
- The administrator can now override the default (root) target processor on the
  respool page.
- The session timeout of the administrative web interface can be configured
  using the property ciphermail.gui.admin.max-inactive-interval.
- Support for Luna HSM added [PRO/ENT].
- PKCS#11 template attributes can be overridden from an XML file [PRO/ENT].

**Technical changes**

- Java wrapper ./lib and ./lib.d added to library.path (this is for example
  needed for Luna HSN support)
- If a PGP private key is not accessible, for example when an HSM is used and
  the key is not available, a warning icon is shown.
- Logging improved for email sent from back-end which and is not accepted by
  Postfix (for example because the email is too large).
- MTA "after queue filter size" and queue\_min\_free is now calculated based on
  the configured maximum message size.
- MTA "before queue filter size" renamed to "Message size limit" and "after
  queue filter size" removed from administrative web interface.
- HSM watchdog now checks if the key store is the CipherMail Database KeyStore
  and if so, exits [PRO/ENT].
- NTA 7516 code refactored and updated to match latest specifications [PRO/ENT].
- Default Postfix setting change: smtp\_address\_preference is now set to
  'ipv4'.
- X-Forward-For header is now by default removed unless the IP address comes
  from a trusted proxy (see /etc/httpd/conf.d/x-forward-for.xml) [PRO/ENT].

### 4.9.0 (N/A)

Version 4.9.0 was an internal release.

### 4.8.0 (2020-05-27)

**SECURITY fixes**

- Fix for privilege escalation issue (CVE-2020-12713).
- Fix for weak Diffie-Hellman parameters (CVE-2020-12714).

A [patch
script](https://www.ciphermail.com/blog/ciphermail-cve-2020-12713_2020-12714.html)
for both vulnerabilities is available for users of older Email Encryption
Gateway versions.

**New features**

- Support for Securosys HSM added. [ENT]
- ECallSMSTransport2 added which supports the new eCall API. [PRO/ENT]

**Technical changes**

- Java 8 or up is now required.
- Apache CXF jars and dependencies updated from 3.0.12 to 3.3.6.
- hsm-iaik-watchdog.xml renamed to hsm-watchdog.xml. [ENT]
- Docker Compose file added for unit testing.

**Bug fixes**

- Certbot timer was not started. [PRO/ENT]

### 4.7.1 (2020-01-22)

**New features**

- Wizard added for requesting ACME (Let's Encrypt) certificates for use with the
  virtual appliance. [PRO/ENT]
- Wizard added for importing trusted system root certificates. [PRO/ENT]

**Technical changes**

- SMTP connection throttling and maximum queue size changed (to 1000) to limit
  the number of emails in the spool queue. We want queuing to be done by
  Postfix.
- IsDatabaseInactive matcher added which checks if the database is active. The
  matcher is called just before a message is delivered back to Postfix. If the
  database is not active, the message will be stored in the error spool. This is
  to prevent email from being sent unencrypted when the database is not
  functional.

**Bug fixes**

- Only scan for attached S/MIME attachments (AttachedSMIMEHandler) if the part
  is a multipart message or if the filename is smime.p7m, smime.p7s or
  smime.p7z. This fixes a bug where a signed PDF was incorrectly identified as
  being an S/MIME blob.

### 4.7.0 (N/A)

Version 4.7.0 was an internal release.

### 4.6.2 (2019-12-23)

**New features**

- Support added for the Dutch NTA7516 draft standard (VWS-project
  [Veilige Mail](https://www.nen.nl/Alles-over-NEN-7510/NTA-7516.htm)).
  [PRO/ENT]
- Logos modifiable by the administrator are now stored in a logo registry. A
  standard logo editor can be used to add additional editable system logos.
  [PRO/ENT]
- System sender and From addresses can now be specified. These addresses are
  used by some system-generated notification messages. The default system sender
  is the null sender. The default From address is 'postmaster'. The default
  email templates have been modified to reflect this change; From is set to
  'postmaster' if not overridden.
- The MPA log viewer now supports multiple log files. Previously only the last
  two MPA log files were shown. The administrator can now select the number of
  log files to show and filter on.
- The system URLs (Base URL, OTP URL etc.) can now be set on domain and user
  level. Previously this was only possible on the global system level.
- Subject and header triggers added for skipping encryption, forcing the use of
  Webmail Messenger and forcing encrypted PDF messaging. [PRO/ENT]
- It is now possible to prevent the Gateway from adding certain security
  keywords by setting the related subject tag to "\<skip\>".
- Command line script added for adding log statements to the MPA log.
- The CertStore command line tool was updated: certificates can now be
  validated, output can be written to a CSV file.

**Technical changes**

- Native NTLM client support is now disabled because only version 1 was
  supported, which is insecure and discouraged by Microsoft. If you need NTLMv2
  support, use the included cntlm proxy or an NTLM proxy of your choice.
- Notify and PDFEncrypt "userProperty" must now be a JSON string. If you have
  modified the standard config.xml file, or any other MPA XML fragment that uses
  Notify or PDFEncrypt and where "userProperty" is set, please update these XML
  files to make sure that the new JSON configuration is used.
- System jars updated for Java 11.
- The type of the cm\_value field of the cm\_properties\_cm\_name\_values table
  was changed to mediumtext in order to support larger values. This change only
  affects installations that use a MariaDB or MySQL DBMS.
- Subject encryption triggers are now removed from incoming messages before the
  S/MIME handler is executed. We need this so we can trigger on the "decryption
  info tag" when a reply to an email is sent so we can force encryption if the
  email was received by the Gateway in encrypted form.
- The maximum MPA log size is now 20MB (was 10MB). The log is rotated once it
  reaches this size.
- Packages are signed with a new signing key. The fingerprint of this key is
  `034416869EBF877A9C37B22E81DD361DC65A8999`.
- dnsjava upgraded to 2.1.9.
- All non-CipherMail package dependencies for the RPM and Debian packages are
  removed. This makes it easier for us to support multiple RedHat/CentOS and
  Debian/Ubuntu releases.
- Parts of properties code changed to improve multitenancy.

**Bug fixes**

- Email address highlighting was missing the '-' character in domain names.

**Miscellaneous**

- The 'Reporting' menu item was moved to the 'Other' pull-down menu. [PRO/ENT]
- The 'Logos' menu item was moved to a new 'Style' pull-down menu. [PRO/ENT]
- Subject and header trigger settings are now moved to dedicated pages. This
  allows us to support multiple triggers without cluttering the settings page
  too much. The triggers can be accessed from the new 'Triggers' pull-down menu
  on the 'User', 'Domain' and 'Global settings' pages.

### 4.6.0 - 4.6.1 (N/A)

Versions 4.6.0 and 4.6.1 were internal releases.

### 4.5.0 (2019-09-12)

**New features**

- Support for Securosys Primus HSM added. [ENT]
- Intellicard Certificate Request handler added. Certificates can now be
  automatically requested from the Intellicard remote PKI. [PRO/ENT]
- There is now a help button for most pages and settings. If clicked, the online
  help page for that page/setting will be opened.
- Additional email forwarding rules can be configured. [PRO/ENT]
- Wizards added for initial setup, encryption setup and importing TLS
  certificates. The wizards make it easier to set up and configure the Gateway.
  [PRO/ENT]
- GetPasswords mailet added.
- PDF option 'Use reply sender' added. If 'Use reply sender' and 'Reply sender'
  are set, the 'Reply sender' email address is used as the sender of the PDF
  reply email.
- DKIM sign/verify now supports different cryptographic providers. This is
  required for HSM support.
- Every admin page now contains the admin menu. If you open an admin page, you
  no longer have to use the back button to open another one.
- JEVAL decryptString function added. EvaluateUserProperty matcher now supports
  decryptString function.
- Freemarker DecryptString method added (which cannot be used from a freemarker
  template).
- PGP 'Ignore parsing errors' checkbox added. If set, only the faulty key from a
  keyring is skipped, the other keys are still imported.

**Technical changes**

- Postfix main setting daemon\_directory was changed for SUSE
  (/usr/lib/postfix -> /usr/lib/postfix/bin).
- Domain validator is now more strict. A domain fragment cannot start or end
  with a dot or contain consecutive dots.
- Additional HTTP security headers (Content-Security-Policy, X-XSS-Protection,
  X-Content-Type-Options, Referrer-Policy, Feature-Policy) added to Apache HTTP
  Server configuration. [PRO/ENT]
- DNS and network settings are now always reconfigured in the background when
  restarted from the administrative web interface.
- For most RPM spec configuration files, '%config(noreplace)' is replaced by
  '%config'.
- Improved parsing of 'unparseable' PGP keys.
- A PDF reply message will now contain the 'X-CipherMail-Reply' header.
- Dynamic memory allocation is no longer calculated in the default include file
  but in the startup script. The maximum amount of memory available to the
  application can now be overruled with the /etc/default/djigzo-override file.
  We need to set a higher limit for the virtual appliance because it runs on
  64-bit Linux, which has a much higher memory ceiling.

**Bug fixes**

- The TLS/SSL import page now allows an empty password.

**Miscellaneous**

- Network, software and system update layouts now use tabs instead of links.
- Fixed sonarlint warnings.
- Some pulldown menu items (Settings/Other, Admin/Other and MTA) are now sorted.

### 4.4.0 (N/A)

Version 4.4.0 was an internal release.

### 4.3.0 (2019-01-30)

**New features**

- The IP address range used by Office 365 and G Suite can be automatically
  configured. This makes it easier to use the Gateway with these services.
  [PRO/ENT]
- MTA lookup tables can be configured from the administrative web interface.
  This allows you to configure your own MTA maps, for example to configure a
  fine-grained TLS policy. [PRO/ENT]
- Front end rework. The Bootstrap HTML/CSS framework is now used for the web
  interface. The interface is now responsive and scales on mobile devices. Some
  menu items are combined under one pulldown menu (like the S/MIME menu), while
  others have been moved.
- MimeEncodeHeaderMethod freemarker method added. This can be used for UTF-7
  header encoding.
- EncodedSubject template parameter added. This parameter contains the UTF-7
  encoded subject.
- Metrics added for data leak prevention and encryption failures. [PRO/ENT]
- The 'Download key' page now supports downloading PEM-formatted keys.
- The web server can now be restarted from the 'System' page.
- License notifications added. A notification is sent if 25%, 50%, 75% and 100%
  of the max licenses have been assigned. A license expiration warning is shown
  if the license is about to expire in 30 days. A daily notification is sent if
  the license expires within 7 days. [PRO/ENT]

**Technical changes**

- Factory property files are now read from conf/djigzo.properties.d in sorted
  order. This can be used to add new factory properties or to override existing
  properties.
- The virtual appliance no longer maximizes the back-end memory to 2 GB. This
  limit was due to the 32-bits Linux not supporting more than 2 GB for the Java
  virtual machine.
- GlobalSign certificate request handler added. S/MIME certificates can now be
  automatically requested from the GlobalSign Managed ePKI service. [PRO/ENT]
- Changes to the LDAP configuration. The LDAP server URL and credentials should
  now be configured in ldap-context-source.xml. [PRO/ENT]
- Milter classes refactored. The "Change sender" milter has been added.
  [PRO/ENT]
- Postfix long queue IDs are now enabled by default.
- New TLS certificates for the web server are now installed using a Bash
  script. This requires that the Tomcat `-Ddjigzo.home` parameter is set.
- Reboots and shutdowns are now done in the background to prevent browser
  errors.
- Post-quarantine processors renamed. There are now special processors used for
  email released from quarantine.
- MariaDB JDBC driver updated to 1.7.4.
- Apache HTTP Server is now used for the front end of the
  professional/enterprise virtual appliance. Reconfiguring the web server for
  new TLS certificates is now much faster and does not invalidate existing
  sessions. Another benefit is that Apache HTTP Server allows for a large number
  of configuration options, ranging from custom headers to specific TLS
  settings. [PRO/ENT]

**Miscellaneous**

- General housekeeping of copyright statements, expired test keys/certs, code
  etc.

### 4.2.0 (N/A)

Version 4.2.0 was an internal release.

### 4.1.3 (2018-08-14)

**New features**

- Support for Clickatell connect SMS API added.

**Bug fixes**

- Attachments could not be added to the PDF reply because of the
  'X-Frame-Options' header, which was added in version 4.1.2. The header value
  is now set to SAMEORIGIN instead of DENY. Ref: GATEWAY-115.

### 4.1.2 (2018-06-07)

**New features**

- [EFAIL
  detection](https://ciphermail.com/blog/efail-detection-and-prevention.html)
  added.
- MTA lookup tables can now be configured from the administrative web interface.
  This allows you to easily configure your own MTA maps, for example to enforce
  a strict TLS policy for certain domains. [PRO/ENT]

**Technical changes**

- TLSv1 and TLSv1.1 have been disabled in the web server, only TLSv1.2
  connections are now supported.
- Clickjacking protection with the 'X-Frame-Options' header added.

### 4.1.1 (N/A)

Version 4.1.1 was an internal release.

### 4.1.0 (2018-05-08)

**New features**

- The CertStore command line tool can now export certificates and keys.
- The PGP command line tool can now export (public and private) keys.
- The back end now supports a mail attribute named
  'remote-delivery.smtp.relay-host'. It can be used to deliver mail to a
  different relay host or local service based on the message content.
- SetRecipients mailet added. This can be used to change the recipients of a
  message.
- Matcher added for matching on a user-configurable list of senders and
  recipients. This also works for Exchange's journaling messages by looking
  inside the journal. [PRO/ENT]
- Intellicard certificate request handler added. [PRO/ENT]
- The database can now be exported in XML format. [PRO/ENT]
- The hardware security module integration now supports the RSAES-OAEP
  encryption scheme, which is a requirement for the German edi@energy standard.
  [PRO/ENT]

**Technical changes**

- The post-processors for S/MIME and PGP are now only called when a message was
  encrypted with S/MIME or PGP. The post-processor functionality can be used to
  tweak the behavior of the Gateway when a message is decrypted. One example
  might be to redirect the decrypted message to a content scanner for virus or
  spam filtering.
- Systemd unit 'fetchmail.service' added.
- SleepTimeOnError added to SMSGatewayImpl background thread. The thread will
  sleep for 30 seconds (configurable) if there was an exception in the
  background thread not caused by a transport. This is done to prevent
  overwhelming the system with log messages if there is a problem with the
  database.
- S/MIME command line tool refactored. The tool now uses long option names.
- System property ciphermail.crypto.cms.mustProduceEncodableUnwrappedKey added.
  This sets the mustProduceEncodableUnwrappedKey property for Bouncy Castle,
  which is needed for supporting hardware security modules produced by Utimaco.
- Postgres JDB driver updated to support Postgres 10.
- Most dependencies have been removed from the Debian and Red Hat packages. This
  makes it easier for us to support different Debian and Red Hat/CentOS
  releases.
- The CipherMail Gateway virtual appliance is now based on CentOS 7 instead of
  Ubuntu and runs MariaDB instead of PostgreSQL. Unfortunately this means that
  backups of previous virtual appliances cannot be imported directly in
  CipherMail Gateway 4.1.0+. Users with a support contract can contact us for
  help with migrating the database to the new version. Note: this change only
  impacts customers who wish to upgrade to the new CentOS-based virtual
  appliance.
- Jetty upgraded to release 9.4. This requires Java 8 or up. [PRO/ENT]
- The selected certificate request handler is now session-persistent, which
  means that the selection is remembered while the session is active.

**Bug fixes**

- Cipher suites for HTTPClient are no longer set. The cipher suite configuration
  resulted in a bug after a Java update. This only affects old versions of
  CipherMail Gateway that used a symbolic link to sunjce\_provider.jar in
  /usr/share/djigzo/james-2.3.1/lib.
- The varchar columns in MySQL/MariaDB databases have changed from a maximum of
  128 characters to 255, because the 'alias' field was too short to fit a
  SHA-512 key thumbprint and related metadata. This resulted in a 'field too
  small' error when trying to set the key alias. [PROF/ENT]
- SOAP communication between the front and back ends now uses HTTP Basic
  Authentication instead of Web Services Security to work around a recently
  introduced bug in Java 1.8.0\_162. Ref:
  [1](https://bugs.openjdk.java.net/browse/JDK-8196491),
  [2](https://github.com/javaee/metro-jax-ws/issues/1209)
- The CipherMail Gateway license was only checked during startup. [PRO/ENT]
- Postgres 10 does not allow the JDBC URL to end with a slash character. The
  last slash has been removed from the URL.

### 4.0.0 (N/A)

Version 4.0.0 was an internal release.

### 3.3.1 (2017-10-08)

**New features**

- S/MIME support for the RSASSA-PSS signing algorithm and RSAES-OAEP encryption
  scheme added. This is a requirement for the German edi@energy standard.
- RPM packages can now be relocated.
- PGPSkipSignOnly property added.
- Authentication with Active Directory implemented for the administrative web
  interface. [PRO/ENT]
- MPA log search filter now has a "context" option. If the context is set,
  context number of log lines before and after the match will be shown. This
  makes it easier to search for multi-line log output.
- SMTP transport config is now directly editable from the GUI [PRO/ENT]
- SMTP transport now allows mapping from email address to relay [PRO/ENT]

**Technical changes**

- Bouncy Castle updated to 1.58. Note: the updated Bouncy Castle jar now
  strictly follows the standards for ASN1 integer encoding. Because of this some
  old, incorrectly encoded certificates might now be considered invalid. To
  revert to the old, less strict encoding, set the Java system property
  `org.bouncycastle.asn1.allow_unsafe_integer` to 'true'.
- Freemarker updated.

**Bug fixes**

- Trying to export a PGP secret key resulted in a NullPointerException if the
  selected keyring did not contain a private key.
- The DHCP client used by the virtual appliance was not stopped when changing
  from DHCP to static network configuration.

### 3.3.0 (N/A)

Version 3.3.0 was an internal release.

### 3.2.7 (2017-04-24)

**New features**

- IsSMIMEDeepScan matcher added. The IsSMIMEDeepScan matcher can be used to
  detect if a message is secured with S/MIME and if it contains an S/MIME
  attached message part (message/rfc822).
- The Gateway now adds a special header to a message if the message could not be
  decrypted with S/MIME or PGP.
- CertStore command line tool added for managing the certificate store. This
  replaces the old CertManager tool.
- SMTPSink command line tool added for testing incoming email.
- CheckKeyStore command line tool added for checking whether keys are
  accessible. This is currently used to validate key storage in hardware
  security modules, which is an enterprise feature.
- conf/spring/spring.properties.d directory added from which properties files
  are read. This allows you to use '${...}' placeholders in Spring XML
  configuration files, which will be replaced by the values defined in the
  properties files.
- REST service API added. [PRO/ENT]
- Respool option added. This can for example be used to retry decrypting a
  message which could not be decrypted because the private key was not available
  when the message was received. [PRO/ENT]
- Meta certificate request resolver added for trying multiple certificate
  request resolvers in succession until one returns a valid Distinguished Name
  for the certificate request. [PRO/ENT]
- Static certificate request resolver added. This allows you to specify a static
  mapping from domain or email address to Distinguished Name parameters.
  [PRO/ENT]
- Milter added for checking the MTA queue size and returning temporary errors
  (450) if the MTA queue size exceeds the limit. This can for example be used in
  a clustered setup to refuse incoming connections to a server under heavy load.
  [PRO/ENT]
- Thales nCipher hardware security modules can now be used in high-availability
  clusters. The HSM keys are replicated between cluster nodes. [PRO/ENT]
- On-demand key store added. This key store can be integrated with other
  security products or custom scripts to retrieve decryption keys from external
  systems on demand. [PRO/ENT]
- A 'do nothing' post-smime-incoming processor added. This can be used to
  dynamically add new mail rules without having to change the XML configuration.
- Support for SUSE Linux Enterprise Server 12 added.

**Technical changes**

- Every CRL is now imported in a separate transaction instead of one transaction
  containing all new CRLs. This improves memory usage and makes it less likely
  that the transaction is rolled back in a highly-available cluster when the
  CRL was already imported on another node.
- Added command line tool functionality for managing users.
- Some Java libraries were updated.
- system.trustAnchorBuilder.updateCheckInterval changed from 30 to 5 minutes.
  This is useful in highly-available clusters to make sure that the cached
  list of root certificates is somewhat synchronous.
- Because some NIO classes are now used, Java 7 or up is now required.

**Bug fixes**

- PDF encryption now supports deep scanning, which scans all (nested) MIME
  parts for attachments. This is needed because some clients (e.g. Apple Mail)
  put attachments inside text/html multipart-alternatives, which were not
  scanned when a text/plain version was available. Ref: GATEWAY-89.
- PostgreSQL clauses NOCREATEUSER and NOCREATEDB are no longer used in the
  installation scripts. NOCREATEUSER is no longer supported in PostgreSQL 9.6.
  Ref: GATEWAY-108.

### 3.2.4 - 3.2.6 (N/A)

Versions 3.2.4 through 3.2.6 were internal releases.

### 3.2.3 (2016-12-12)

**New features**

- Outgoing email can now be signed with DKIM. The signing keys can be configured
  for all mail, for mail from specific domains or for mail from specific
  addresses.
- DKIM signing keys can be generated and stored on a hardware security module.
  [ENT]
- Actions regarding data leak prevention (DLP) can now be delayed until it is
  determined whether a message was encrypted or not. This allows configuration
  of a DLP rule which is only activated when the message was not encrypted by
  the sender.
- DLP validators added. A validator can check whether a matched pattern has some
  structure, other than those that can be matched with regular expressions. A
  credit card number for example can be matched this way, because credit card
  numbers use the Luhn algorithm. The currently supported validators can match
  Dutch citizenship numbers (BSN), U.S. DEA registration numbers, U.S. National
  Provider Identifiers (NPI) and arbitrary numbers that can be validated against
  the Luhn algorithm. [ENT]
- Twilio and eCall SMS transports added. [ENT]
- LDAP certificate lookup module added. The certificate lookup/download process
  can be modified by editing the default Bash script. [ENT]
- The administrative web interface now has a page that displays the database
  cluster status. [ENT]
- SMTP transport configuration page now supports specifying alternative TCP port
  numbers. [ENT]
- Tool added for making and restoring backups to/from XML. This allows the
  Gateway configuration to be migrated to a different database type (e.g. from
  PostgreSQL to MariaDB). [ENT]

**Technical changes**

- Phone numbers are now validated with libphonenumber.
- The backup and restore functionality can now omit the database (i.e., only
  backup or restore configuration files).
- Thread pool size for Apache CXF is now set to 10 threads instead of the
  default 200.
- Java wrapper overrides should now be done in
  wrapper-additional-parameters.conf.

**Bug fixes**

- Fixed hostname resolving issue. If the hostname was changed and the old
  hostname was no longer resolvable, it could happen that the front end was no
  longer able to connect to the back end, which would require a restart of the
  back end in order to restore service. Ref: GATEWAY-105.
- Other minor bug fixes.

### 3.2.0 - 3.2.2 (N/A)

Versions 3.2.0 through 3.2.2 were internal releases.

### 3.1.2 (2016-08-23)

**New features**

- dateCreated user property added.
- Templates for OTP-encrypted PDF now include the password length in the
  filename of the PDF document.
- Placeholder 'auto-submitted-incoming-verified',
  'auto-submitted-incoming-invalid-signature' and 'otp-setup' processors added
  which can be overridden by optional Gateway modules.
- OTP-encrypted PDFs are now supported in combination with the Webmail
  Messenger. [ENT]
- The 'Reporting' page now reads supported parameters from a configuration file
  and dynamically updates the administrative web interface. [ENT]

### 3.1.1 (2016-06-30)

**Technical changes**

- MySQL/MariaDB jdbc drivers updated.
- Apache CXF upgraded. CXF now uses netty for SOAP server connections instead of
  jetty.
- config.xml refactored. It is now easier to change the mail flow using
  additional XML fragments.
- Advanced PGP option 'Skip non-PGP extensions' added. If enabled (the default),
  only attachments with the extensions ".pgp", ".asc", ".gpg" or ".sig" are
  scanned for binary PGP data. This should speed up the scanning of incoming
  messages.

**Bug fixes**

- Fixed an issue where new admin roles could no longer be assigned or removed.
  Ref: GATEWAY-102.
- The Enigmail PGP encryption add-on for Thunderbird shortens long filenames in
  a way that the JavaMail component of CipherMail Gateway could not correctly
  process. We have implemented a workaround for this.

### 3.1.0 (N/A)

Version 3.1.0 was an internal release.

### 3.0.5 (2016-04-18)

**New features**

- Support for MySQL/MariaDB and Oracle Database added.
- JDB connection tester tool added. This can be used to test a connection to an
  external database (PostgreSQL, MySQL/MariaDB and Oracle Database).
- Log export functionality added to the virtual appliance. This used to be an
  enterprise-only feature.
- Java 8 is now supported. Note: because elliptic curve ciphers are currently
  [not supported](https://bugs.centos.org/view.php?id=9482) for TLS in
  the RedHat/CentOS OpenJDK 8 packages, we recommend that users of these
  packages wait before upgrading.
- The certificate request page now supports more request parameters, like
  country, locality, state and organizational unit.
- The Gateway can now trim off whitespace at the beginning and end of all PGP
  header lines. Note that this is not enabled by default as this is not RFC
  compliant, and a system property must be set to enable it. Ref: GATEWAY-91.
- PKCS10CertificateRequestHandler added for requesting S/MIME certificates from
  external certificate authorities. [ENT]
- Additional SMTP configuration pages added: RBL, forwards, header checks and
  SMTP transports. [ENT]
- SMTP SSL/TLS certificate management page added. [ENT]
- System status page added. This page shows various graphs that can be used to
  get a quick overview of the state of the system. [ENT]
- Logo module added, which can be used to change the logo displayed on the login
  and dashboard pages. [ENT]
- Statistics module added. This module keeps track of the number of
  cryptographic operations per time unit. A PDF report with these statistics can
  be exported. [ENT]
- Option added to webmail settings page for requesting read-receipts. [ENT]
- License module added. [ENT]
- TLS certificate signing request (CSR) module added. Requesting a TLS
  certificate signed by external CA can now be done from the administrative web
  interface. [ENT]
- Webmail option 'only encrypt if mandatory' added. [ENT]
- Update check added. [ENT]
- Software install module added. This can be used to install new software using
  digitally signed software update packages. [ENT]
- Support added for hardware security modules. [ENT]
- Additional PDF features added: cover page, automatic renaming based on the
  attachment type, support for additional fonts, and configurable size limits of
  PDF attachments. [ENT]
- Support for CipherMail Webmail Messenger added. [ENT]
- LDAP mail rules module added. This feature can be used to change the mail flow
  based on LDAP object attributes. [ENT]
- LDAP certificate request resolver added. When a certificate is requested,
  additional certificate request parameters (for example the common name, city
  and organization) can be obtained from the LDAP database. [ENT]
- Email-based certificate request module added. If enabled, this module will
  automatically reply with a digitally signed email containing the S/MIME
  certificate. This can be used by an external user to retrieve the certificate,
  so that they can start an encrypted conversation. [ENT]
- Remote system monitor added which can be used to monitor system variables.
  Scripts for monitoring with Icinga and Nagios are included. Examples of the
  variables that can be monitored are the size of the mail queues and the
  number of stored certificates. [ENT]
- Notifications module added. If enabled, system notifications are sent to the
  configured recipients. An example of an action that may trigger a notification
  is when the Gateway imports a new certificate or PGP key from an incoming
  message. [ENT]
- PEM to PFX conversion module added. This can be used to convert PEM files to
  password-protected PFX files. [ENT]
- rsyslog module added. Logs from the mail processing agent and webserver will
  be sent to the rsyslog service. [ENT]

**Technical changes**

- Because support for MySQL/MariaDB and Oracle Database was added, there is now
  a separate package for PostgreSQL. The main back end package no longer
  depends on the PostgreSQL package.
- Mail generated by the administrative web interface is now sent to a local mail
  service on TCP port 10027. This makes it possible to change settings on the
  default smtpd port (25) without interfering with email sent from the
  administrative web interface. This requires an additional service setting for
  port 10027 in the master.cf file.
- Libraries updated.
- The database connection string is now stored in a separate XML fragment
  (hibernate.connection.xml). This makes upgrading easier because upgrading the
  hibernate configuration file no longer results in overwriting the database
  connection string.
- ciphermail.backup.enabled system property added which can be used to disable
  the backup option from the administrative web interface.
- The Tomcat servlet engine now only offers strong TLS ciphers to its clients.
- Certificate/key import pages and PGP keyring import page now redirect the user
  to the parent page after import. The import result is shown in the parent
  page.
- The default S/MIME encryption algorithm was changed from 3DES to AES-128, and
  the default signing algorithm was changed from SHA-1 to SHA-256. Since
  Windows XP is no longer supported, the stronger AES-128 and SHA-256 algorithms
  are now supported by most (if not all) systems.
- The MTA page no longer has any advanced options since the old advanced options
  were actually important enough to be made visible at all times. The mailbox
  size limit is no longer editable by default since this is not used with a
  standard Gateway setup.
- User objects are no longer created by default when certificates are created
  or requested through the CA module. This is now only done in case an inherited
  setting needs to be overruled.
- The 'CA Request' page now supports downloading of certificate signing requests
  and uploading of certificates. This is not supported by all certificate
  request handlers.

**Bug fixes**

- Java wrapper updated to
  [3.5.28](https://wrapper.tanukisoftware.com/doc/english/release-notes.html).
  This fixes the following Java wrapper bugs: multi-byte character logging
  sometimes resulted in erroneous '?' in the log files, and a memory leak on
  RedHat/CentOS.
- PDF encryption failed on non-standard Unicode characters (Webdings font). Ref:
  GATEWAY-92.
- The PGP keyring importer did not report failure if the password was incorrect.
  Ref: GATEWAY-96.
- Signing a message with PGP/MIME using an invalid content-transfer-encoding
  resulted in the message ending up in the error queue. Ref: GATEWAY-97.
- The CA issue page now only complains of a missing CA if the built-in
  certificate request handler is selected.
- The PGPKeyServer page now shows a scrollbar instead of letting long lines
  overflow.

**Miscellaneous**

- The administrative web interface and user portal are now responsive (i.e.,
  they scale on smaller devices).
- Most close/cancel buttons were removed to make the administrative web
  interface more consistent. Users should use the back button or menu items for
  navigation.
- The X500 subject field now shows most settings by default and only
  organizational unit under the 'more' option.
- The order of items in the CA menu was changed. The CA settings page is renamed
  to 'Configure CA'.

### 3.0.0 - 3.0.4 (N/A)

Versions 3.0.0 through 3.0.4 were internal releases.

### 2.10.0 (2015-03-30)

**New features**

- HTML parts are now scanned for PGP content. Ref: GATEWAY-88.
- PGP desktop/universal (now Symantec) proprietary partitioned encoding format
  is now fully supported for incoming email. Email encrypted with PGP
  desktop/universal using the partitioned encoding format is now "repaired" to a
  proper MIME message.
- RedHat/CentOS 7 is now supported.

**Technical changes**

- The Gateway now ignores content-transfer-encoding of multipart messages.
- The S/MIME handler now adds `X-Djigzo-Info-Signer-Email-*` headers with the
  email address on the signing certificate.
- There is no longer a separate virtual appliance for VMware Workstation. The
  VMware virtual appliance now supports ESX 4+ and VMware Workstation 7+.

### 2.9.0 (2014-09-24)

**New features**

- Global "Skip calendar messages" property added which will skip encrypting and
  signing if the message is a meeting invite. This is useful in case the invite
  recipient uses Outlook, as Outlook cannot handle encrypted or signed meeting
  invites.
- PGP sign-only is now supported. "Only sign when encrypt" now also works for
  PGP messages. S/MIME signing is tried before PGP signing, i.e., if a sender
  has a valid S/MIME signing key, the message will be S/MIME signed. If the
  sender does not have a valid S/MIME signing key but has a valid PGP signing
  key, the message will be PGP signed. Signing of PDF encrypted mail now also
  supports PGP signing if a valid PGP signing key is available.
- The Gateway administrator can now disable automatic PGP decryption. Doing so
  requires a change to the config.xml file. Ref: GATEWAY-81.

**Technical changes**

- Symantec's PGP Universal Gateway uses non standard encoding for PGP/MIME. The
  header 'x-pgp-encoding-format' is now used to detect PGP/MIME encoding for
  messages generated by this product. Ref: GATEWAY-83.
- The PDF encryption module now supports UTF-8 encoded passwords. Ref:
  GATEWAY-82.
- The default signature algorithm for newly generated certificates is now
  SHA-256 instead of SHA-1.
- BlackBerry module handling is no longer enabled by default. The module only
  supports BlackBerry OS 7 and lower using BlackBerry Internet Service (BIS).
  BlackBerry OS 10 does not use BIS, so for BlackBerry OS 10 the module is not
  required.
- PGP/MIME encrypted messages now use 'inline' as default disposition. This is
  similar to how Enigmail sets the disposition and allows the Mailvelope browser
  add-on to open PGP/MIME encrypted messages.
- Bouncy Castle library updated to 1.51.
- Spring libraries updated to 3.2.9.
- Spring security libraries updated to 3.2.5.
- Incorrect web interface logins are now remembered for 5 minutes (was 1
  minute).

**Bug fixes**

- Startup failed if the Linux 'free' command was localized. A fallback to
  reading out /proc/meminfo is added which will be used if 'free' does not
  return the required data. Ref: GATEWAY-80.
- PGP/MIME signatures were invalid if a multipart message contained extra
  newlines at the end of the message. The Zimbra mail client for example adds
  additional empty lines at the end of a multipart message. These lines are now
  removed before signing.
- The NTP settings of the virtual appliance could not be changed. This was due
  to Ubuntu 14.04 using a different name for NTP server settings. Ref:
  GATEWAY-79.
- Due to an incompatible change in a recent update of OpenJDK, CipherMail would
  no longer start up. Ref: GATEWAY-78.
- PGP signatures are now created in plain text and not as a binary blob. This is
  a workaround for a [bug](https://sourceforge.net/p/enigmail/bugs/329/) in
  Enigmail 1.7.

**Miscellaneous**

- The 'Mobile settings' link in the administrative web interface is disabled by
  default since the BlackBerry add-on is no longer enabled by default.
- Minor changes to the 'Settings' page. Some settings were moved to additional
  settings and vice versa. Some settings were grouped differently.

### 2.8.6 (2014-06-10)

**New features**

- OpenPGP support.
- Support for SUSE Linux Enterprise Server.
- A PDF reply now sets the In-Reply-To header. Conversation threading is now
  supported, i.e., the email client can now group the original message and the
  reply.
- A comment field has been added to the user, domain and global properties.
- MPA logging improved. More information is provided as to why messages are
  handled in a certain way.
- The certificates filter in the administrative web interface can now filter on
  "expired only".
- Fail2ban is now installed on the virtual appliance to provide better
  protection against brute force SSH login attempts.

**Technical changes**

- The TCP port used by the SOAP back end changed from 9000 to 9009 to prevent a
  conflict with nCipher netHSM.
- The default maximum message size for S/MIME and PDF is now set to 50 MB.
- PDF encrypted email now uses the same Message-Id as the original message.
- Lots of internal changes to allow new functionality to be dynamically added as
  plugins.
- Session timeout of administrative web interface changed to 15 minutes and
  timeout of user portal changed to 30 minutes.
- The virtual appliance has been upgraded from 32-bit Ubuntu 12.04 to 64-bit
  Ubuntu 14.04.
- The default credentials of the administrative Unix user of the virtual
  appliance are now:
  username: sa
  password: sa

**Bug fixes**

- Recent versions of OpenJDK register additional DataHandlers which can conflict
  with the way the back-end handles certain attachments. The additional
  DataHandlers are now ignored.
- The fallback charset providers were accidentally disabled. They are now
  enabled again.

**Miscellaneous**

- The software product has been renamed to CipherMail Email Encryption Gateway.
  The company that owns CipherMail Gateway is still called DJIGZO.
- The S/MIME certificate selection links in the administrative web interface
  have been moved to a pulldown menu.
- The German translation of the user portal has been updated.
- The encrypt mode option "Force" in the administrative web interface has been
  renamed to "Allow (sender or recipient)".

### 2.6.0 - 2.8.5 (N/A)

Versions 2.6.0 through 2.8.5 were internal releases.

### 2.5.0 (2013-05-07)

**New features**

- A page has been added where a MIME-encoded message can be uploaded for text
  extraction. This allows the administrator to see the text used by the DLP
  scanner for pattern matching.
- PDF option "Send CC to replier" added. If set, a carbon copy of the PDF reply
  will be sent to the replying user.
- S/MIME option "Skip import of untrusted certificates" added. By default all
  certificates from a signed email will be imported, even if they are untrusted.
  By enabling "Skip import of untrusted certificates", only trusted certificates
  are imported.
- "Signing subject trigger" option added. This can be used to force signing of
  email if the subject contains a user defined keyword. Keyword can be removed
  if configured.
- Password strength check added to the user portal. The minimum required
  password strength can be set by the administrator. By default you are not
  allowed to use your email address as your password, base your password on
  your username or use a keyboard sequence of more than five characters.
- System page added where the server can be restarted and where Postfix can be
  stopped and started.
- Three custom properties and templates added. These properties and templates
  can be used if the mail flow (defined in config.xml) is modified by the
  administrator and the changes require some user-configurable options.
- A page has been added to the administrative web interface which can be used to
  send a test message.
- French language support added to user portal.
- Administrative users are automatically logged out from the web interface after
  five minutes of inactivity.
- Backups are now compressed with gzip.
- OpenJDK 7 is now supported.

**Technical changes**

- The HTML text extractor for the DLP scanner now skips HTML comments by
  default. This makes it less likely to have false positives for some patterns.
- The HTML text extractor for the DLP scanner now only scans the HTML body by
  default. This makes it less likely to have false positives for some patterns.
- The HTML text extractor now also scans 'application/xhtml+xml' attachments.
- "Dynamic" memory allocation is now enabled by default. The DJIGZO back end now
  uses a heap size of 0.6 times the available memory by default.
- The back-end SMTP server no longer adds a 'Received:' header. This makes it
  easier to remove internal IP addresses from the received headers using Postfix
  header checks.
- Changing the IP address filter of the administrative web interface no longer
  requires a restart of the web server. The filter settings are read from
  /etc/djigzo/ip-filter.properties.
- The maximum inline body of PDF messages is now 256 kB by default. If the
  message is larger than that, it will be added as a text attachment.
- Back-end logs now rotate once grown to 10 MB (was 5 MB).
- The SMS option "Phone number allowed" is no longer enabled by default. Note:
  this is a non-backward compatible change since the default value changed. To
  revert back to the old behavior, enable the global "Phone number allowed"
  option.
- sudo usage has been simplified. Scripts will now be executed from scripts.d.
- Some jars updated.
- Support for right-to-left (RTL) text added to the PDF encryption module.
- Support for multiple SMS transports added.
- The DLP scanner now also extracts the meta content of MIME parts. This can for
  example be used to block or quarantine certain attachment types. The
  attachment type is detected from the content of the attachment and not from
  the filename.
- Comodo classes have been removed for now since the Comodo EPKI no longer
  allows certificates to be requested automatically.
- Changing the automatic backup cron expression no longer requires a restart.
  The backup job will be rescheduled.
- The signature algorithm identifier of a signed email changed between RFC 3851
  and RFC 5751. RFC 5751 for example uses 'sha-1' whereas RFC 3851 uses 'sha1'.
  The Gateway now follows RFC 5751 by default. Some spam filtering systems
  however cannot handle the new RFC 5751 syntax. The SMIMESign mailet can be
  configured to revert back to RFC 3851 behaviour.
- URL detection for the PDF module has been improved.
- JCE policy manager page removed since it is no longer required with OpenJDK.
- Most pages now close after applying the settings instead of showing a
  "settings applied" message.
- The administrative web interface now checks whether regular expressions are
  valid.

**Bug fixes**

- The HTML text extractor for the DLP scanner sometimes removed white space
  characters, which resulted in merged words.
- Some internal James log files were not correctly rotated.
- On Chrome, if an attachment was removed from the PDF reply page, the frame was
  not resized.

**Miscellaneous**

- The reply link in the PDF is now a clickable image.
- The look and feel of the administrative web interface has been changed. We
  moved some functionality to different pages. The SMS queue for example is now
  part of the Queues page. Most menu items can be dynamically extended to
  support additional Gateway modules.

### 2.4.0 (2012-05-30)

**New features**

- Sign and encrypt tags can be added to the subject for incoming signed and/or
  encrypted email. Ref: GATEWAY-36.
- Added signer and sender address mismatch detection. Ref: GATEWAY-21.
- S/MIME encrypt mailet can selectively encrypt headers. This is mainly used in
  combination with DJIGZO for Android.
- S/MIME encryption and signing algorithm can be set per recipient or domain.
- Simple subject filter added which can be used to filter the subject using a
  regular expression.
- Locale (i.e., language) can be selected on the user portal login and signup
  pages.
- Command line interface tool added which can be used to set/get user
  properties.
- Spanish translation for the user portal added. Translation by Diego A. Fliess.
- Clickatell provider now supports additional parameters.

**Technical changes**

- Logging has been improved. More information is logged and color coding has
  been improved. Ref: GATEWAY-42, GATEWAY-26.
- Upgraded Bouncy Castle to 1.47.
- LogLevel OFF added which can be used to completely disable logging for a
  specified class.
- Loading speed of the preferences page has been improved.
- The user no longer has to log in after the portal signup. They are
  automatically logged in after the signup process.
- The packages are now signed with a new PGP key.

**Bug fixes**

- The 8.4 PostgreSQL JDBC drivers were not compatible with PostgreSQL 9, which
  is the default version in Ubuntu 12.04. The PostgreSQL JDBC drivers have been
  updated to 9.X to remedy this issue. Ref: GATEWAY-56.
- sudo added as a required package to the RPM spec file. Ref: GATEWAY-55.
- PDF replies did not use the 'Reply-To:' header. Ref: GATEWAY-45.
- Under certain circumstances, Unicode characters were incorrectly encoded when
  replying to an encrypted PDF message. Ref: GATEWAY-48.
- Fixed compatibility bug with Internet Explorer 9. Ref: GATEWAY-40.
- The RPM package installer now waits for PostgreSQL to be running before
  continuing the installation, to prevent database connection errors.

### 2.3.1 (2011-11-27)

**New features**

- Support for One-Time Password (OTP) PDF encryption. The OTP can be obtained
  by the recipient after logging in to the portal.
- Email can be encrypted with additional (escrow) certificates. Ref: GATEWAY-9.
- The PDF reply page and quarantine viewer now support multiple languages (de,
  en, nl).
- The message with the encrypted PDF can be digitally signed.
- EJBCA integration added (certificates can be requested from an EJBCA server).
- The login pages of the administrative web interface and user portal now
  contain a brute-force filter that blocks access for some time if an attacker
  tried to log in with many incorrect credentials in a short timeframe.
- Added support for RedHat/CentOS 6.

**Technical changes**

- The entropy of generated passwords is now 128+ bits.
- The URL path for PDF and quarantine view has been changed from `/external/*`
  to `/web/portal/*`.
- The portal functionality (PDF reply, external quarantine view) is now in a
  separate war file (djigzo-portal.war).

**Bug fixes**

- The wildcard domains were not functioning correctly. Ref: GATEWAY-35.
- Incorrect URL for multiple recipients when using OTP PDF encryption. Ref:
  GATEWAY-38.

**Known bugs**

- In Internet Explorer 9, Compatibility View should be enabled for the web
  interface to work correctly. This bug will be fixed in the next release.

**Miscellaneous**

- Some CSS changes: fonts are bigger, the background color is now white and
  other minor changes.
- SMS settings moved from the main settings page to a separate setting page.

### 2.2.0 - 2.3.0 (N/A)

Versions 2.2.0 and 2.3.0 were internal releases.

### 2.1.1 (2011-08-16)

**New features**

- Advanced S/MIME setting "Always use freshest signing certificate" added. If
  enabled, every time the sender needs to sign a message, the most recent (i.e.,
  the latest "not before" date) signing certificate will be used. Ref:
  GATEWAY-14.
- Advanced PDF setting "Only encrypt if mandatory" added. If enabled, PDF
  encryption will only be activated if encryption is mandatory. Ref: GATEWAY-22.
- DLP setting "Quarantine on failed encryption" added. If enabled, when
  encryption is mandatory and a message cannot be encrypted, the message will be
  quarantined instead of returned to sender. Note: this required minor changes
  to the "DLP quarantine" template.
- Quarantined emails can now be released as-is. When a quarantined message is
  released this way, no further processing is done and the message is
  immediately delivered.
- The administrator can now specify how many rows the grid should show per
  page (users, certificates, MTA queue). Ref: GATEWAY-23.
- The administrator can now filter for specific email in the MTA queue.
- The MTA logs are now shown in "raw" format by default (i.e., in the exact same
  order as the log file). To view the MTA logs grouped on queue ID (the old
  behavior), the administrator should select "Grouped" ordering.
- If a certificate chain is valid, the issuer of the certificate can be clicked
  on in the certificate viewer to show details about the issuer certificate.
- New role `ROLE_MOBILE_MANAGER` added.
- The command line tool can be used to add new charsets to the PDF encryption
  module, in order to use charsets that are not supported out-of-the-box by
  Acrobat reader. Ref: GATEWAY-20.

**Technical changes**

- If a certificate was available for a recipient, a user object was always
  created for that recipient. The user is no longer added by default. A new
  S/MIME advanced setting "Add user" is added which can be used to specify
  if a user should be added when a certificate is available for a recipient.
- Djigzo has the capability of adding certain headers to the signed and/or
  encrypted inner MIME part. This can be used to protect certain headers (for
  example the subject line). However there are some S/MIME gateways that cannot
  handle S/MIME messages with headers within inner MIME parts. Because
  interoperability is important, the subject header protection has been disabled
  by default. Ref: GATEWAY-31.

**Bug fixes**

- With S/MIME "strict mode" enabled, S/MIME messages were only handled by the
  S/MIME handler if the recipient had a valid certificate with private key. If a
  digitally signed message was received for a recipient not having a private
  key, the certificates were not extracted from the message and the signature
  was not removed when "Remove signature" was enabled for that recipient. The
  message is now always handled by the S/MIME handler. Ref: GATEWAY-27.
- Under certain special conditions, the base64 encoder of Javamail created lines
  with more than 76 characters. OpenSSL, which is used by some S/MIME gateways,
  cannot handle base64-encoded parts containing lines longer than 76 characters.
  Javamail has been updated to remedy this issue. Ref: GATEWAY-29.
- When deleting a large number of messages from the MTA queue, the MTA queue
  could no longer be read until after the garbage collector ran. Ref:
  GATEWAY-32.

**Miscellaneous**

- Some settings have been moved to advanced settings.
- The BlackBerry and mobile settings have been moved to a separate page.

### 2.1.0 (N/A)

Version 2.1.0 was an internal release.

### 2.0.1 (2011-03-18)

**Bug fixes**

- If a message was placed in quarantine because of a DLP violation and the
  'Message-Id:' or 'From:' header contained a '%' character, deleting the mail
  from quarantine resulted in an error. This bug was reported by Andreas Beier.
  Ref: GATEWAY-15.

### 2.0.0 (2011-03-07)

**New features**

- Data leak prevention (DLP) module added.
- S/MIME strict mode added. In strict mode, additional checks will be done to
  make sure that the message will only be decrypted if there is a matching
  certificate for the recipient.
- Delivery Status Notifications (DSN) are now supported. DSNs are transparently
  handled by the back end.
- Header protection can now be disabled. Ref: GATEWAY-13.

**Technical changes**

- Switched to Java 6.
- Locally-generated notifications are now signed. If a locally-generated
  notification is received by the Gateway (for example because of email
  forwarding), the message is sent as-is to prevent mail loops.
- Javamail updated to 1.4.4-final.

**Bug fixes**

- Workaround for non-RFC compliant SKI support in Outlook 2010.
  Ref:
  [Bugzilla bug 559243](https://bugzilla.mozilla.org/show_bug.cgi?id=559243),
  [IETF mail
  archive](https://mailarchive.ietf.org/arch/msg/smime/2sHKNfusAbNtnoUHadHRCr_vFVM/)

### 1.4.1 (2010-11-11)

**New features**

- The virtual appliance now allows you to specify which IP addresses are allowed
  to access the login page of the administrative web interface.

**Bug fixes**

- Fix for "The PDF reply portal no longer attaches the uploaded attachment"
  (Ref: GATEWAY-12). This was a regression introduced by a previous bug fix
  (GATEWAY-4).
- Fix for "The telephone number in the subject should be detected before
  removing the encryption trigger". Ref: GATEWAY-11.

### 1.4.0 (2010-08-23)

**New features**

- Different certificate request handlers can now be added using a pluggable
  infrastructure.
- Certificate request handler for Comodo has been added. With the Comodo
  certificate request handler, certificates from Comodo's managed PKI services
  (EPKI) can be automatically requested from the Gateway.
- Certificates can now be requested in bulk. A comma-separated text file
  containing the request details can be imported. The certificates will be
  requested using the selected certificate request handler.
- An email encryption header trigger has been added. Email encryption can be
  triggered using a pre-defined header, which is matched against a regular
  expression.
- A certificate can now be automatically requested for a sender using the
  default selected certificate request handler, if the sender does not already
  possess a certificate and matching private key.

**Technical changes**

- When a large number of certificates (more than 60 000) were imported, the
  certificates view in the administrative web interface was having performance
  issues. The certificate view has been optimized for this.
- Upgraded to a new version of Apache CXF. The SOAP TCP port is now only
  accessible from localhost by default.
- The virtual appliance has been upgraded to Ubuntu 10.04 LTS. Installing the
  Unlimited Strength Policy Files is no longer required.

**Bug fixes**

- The PDF reply URL and body didn't support Unicode characters, only ASCII. The
  reply URL parameters are now UTF-8 encoded. The body is now UTF-8 encoded if
  it contains non-ASCII characters. Thanks to Benedikt Zorn for reporting the
  issue.
- When creating certificates, the email address wasn't added to the alternative
  names, only to the subject.
- Domains containing an underscore were accepted as a valid domain. This is
  incorrect, and the code that performs this check has been updated.
- Java wrapper version upgraded to the latest version. Sometimes the Java
  wrapper was not able to automatically restart Djigzo when the wrapper detected
  that Djigzo wasn't running.
- The update menu items in the virtual appliance console were incorrectly named.
  The menu items were named "Update" and "Upgrade", but they should have been
  named "Upgrade" and "Dist-upgrade".
- Importing a PFX file containing a large number of certificates resulted in a
  timeout.

**Miscellaneous**

- Some menu items in the administrative web interface have been moved to a
  left-hand submenu.
- The SMS settings menu item has been moved to the SMS page.
- The message template to edit should now be selected from a drop-down menu.
- The restart and PDF import attachment wait animation has been replaced with an
  animated gif.
- Added note to the virtual appliance documentation about reserving memory to
  prevent swapping. Thanks to Andreas Beier for his help in finding the cause of
  spurious crashes when running under ESX when the total memory used by all VMs
  was larger than the total host memory.

### 1.3.2 (2010-03-30)

**New features**

- Subdomain matching can now be configured on the MTA configuration page.
- PFX passwords can now be stored in the user preferences.
- MTA authentication (SASL passwords) for external and internal relay hosts can
  now be set.
- Virtual appliance now has Fetchmail support. This feature is disabled by
  default.
- Support for Internet Explorer 8.
- Debian 5 (Lenny) is now supported.
- Multipart/alternative calendar items are now detected.

**Bug fixes**

- When SELinux was enabled and Postfix was restarted by Djigzo, Postfix
  sometimes stopped working.
- Upgrading with the RPM package wasn't working.
- The owner and file mode of the TLS certificate was reset when upgrading the
  Debian packages.

### 1.3.1 (2010-01-10)

**New features**

- RPM packages for Red Hat and CentOS are now available. The installation guides
  have been updated with instructions for these operating systems.
- S/MIME signatures can be selectively removed.
- S/MIME can be selectively skipped for calendar items (text/calendar). Outlook
  cannot handle signed or encrypted meeting requests.
- CRL downloader and SMS gateway can now access external resources via a
  configurable HTTP proxy.
- Configuration files split into multiple files to make upgrades easier when the
  files have been changed.
- Password setting "Send to originator" added. If it's enabled, generated
  passwords for the PDF encryption will be sent to the sender of the message.
- The 'force signing' trigger can be used to trigger S/MIME signing of a message
  using a special email header.
- Certificate Trust List added which can be used to allow or deny certificates.
- Messages sent from BlackBerry (via BlackBerry Internet Service) can now be
  relayed through Djigzo.
- Support for Mac OS X Java wrapper added. Patch submitted by Rainer Frey.
- The virtual appliance now contains a firewall which blocks access to services
  that don't need to be externally accessible.
- Most templates are now by default UTF-8 encoded. Templates now fully support
  Unicode.

**Technical changes**

- Tomcat is now used by default instead of Jetty.
- The 'Expired' column is now shown in red when a certificate is expired.
- Original Message-ID of messages is retained if possible.
- Subject triggers are now recursively removed.

**Bug fixes**

- Signature check sometimes failed for clear-signed messages.
- Backup and restore now works across different PostgreSQL versions.

### 1.3.0 (N/A)

Version 1.3.0 was an internal release.

### 1.2.4 (2009-09-01)

**New features**

- Command line tool added for monitoring the MPA queue.
- Authentication and authorisation against an external LDAP database is now
  possible.

**Bug fixes**

- Some 'Received:' headers were sorted in incorrect order. Reported by Steven
  Geerts.
- The MX setting on the MTA page was reversed. Reported by Steven Geerts.
- The telephone number in the message subject was not picked up when the subject
  only contained the telephone number.
- Djigzo did not start if the virtual appliance was given more than 2048 MB.
  Reported by Stefan Schwarz.
- Encoded smime.p7m filenames are now recognized.

### 1.2.3 (2009-07-01)

**Bug fixes**

- The CRL distribution point did not contain an issuer. When Outlook option
  SigStatusNoCRL was enabled, Outlook reported that the CRL was not available.
  The CRL distribution point is now moved to the end-user certificate. Reported
  by Mark van Voorden.
- An error occurred if a CA certificate was not yet configured and 'apply' was
  selected on the CA select page.

### 1.2.2 (2009-06-02)

**New features**

- Built-in CA added. The CA can issue certificates for internal and external
  users.
- A telephone number specified in the message subject can be used as recipient
  for the SMS text messages. This feature is used for sending the PDF encryption
  password.
- VMware tools can be rebuilt from the menu.
- Large attachment are now supported when using the BlackBerry module.
- "Force encrypt mode" added to overrule the noEncryption setting.
- The administrator can see who (user, domain) is using a certificate.
- Rows per page for the display grids can be set using a system property.
- Certificate and CRL algorithms and more properties can now be viewed.
- CRL certificate entries (the revoked serial numbers) can be downloaded as a
  text file.
- A dialog is now shown when a network configuration error occurs.

**Technical changes**

- Attachments not supported by the BlackBerry application are stripped.
  HTML-only mail is converted to plain text.
- The root certificate is added to the chain when exporting certificates and
  signing messages.
- CRLDistPointCertPathChecker added to certificate path builder. Critical CRL
  distribution points are now accepted.
- SMS text messages now expire in 24 hours if not sent. This was 4 hours.
- PDF replies are now disallowed by default.
- Default S/MIME encryption algorithm is now 3DES because not all Windows
  versions support the stronger AES algorithm.
- Password validity interval should now be set in minutes. Old settings will be
  automatically converted.
- Jetty upgraded to 6.1.17.

**Bug fixes**

- mail.mime.parameters.strict system property added. This is a workaround for
  mail created by Mac OS X. The Mac OS X mailer "forgets" to quote filenames
  containing spaces.

**Miscellaneous**

- Some user settings in the administrative web interface have been moved to
  advanced settings.

### 1.2.0 - 1.2.1 (N/A)

Versions 1.2.0 and 1.2.1 were internal releases.

### 1.1.0 (2009-03-31)

**New features**

- Automatic and manual encrypted system backups. It is possible to make backups
  to and restore from local files and remote shares.
- BlackBerry support.
- "Only sign when encrypt" preference added.

**Technical changes**

- Debian package dependency changed from Sun JRE to OpenJDK.
- Postfix 'mydestination' setting can no longer be specified. It can be
  re-enabled through the system settings.
- `X-Djigzo-*` headers are removed from incoming (to internal) email.
- Subject trigger is removed from incoming (to internal) email.

**Bug fixes**

- Already signed messages are not signed again.

**Miscellaneous**

- Internal users and domains are marked with a star icon.
- Certificate entries with a private key are marked with a key icon.

### 1.0.1 (2009-02-08)

- Initial release.
