package com.ciphermail.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
    scanBasePackages = {"com.ciphermail.portal", "com.ciphermail.rest"}
)
public class PortalApplication
{
    public static void main(String[] args) {
        SpringApplication.run(PortalApplication.class, args);
    }
}
