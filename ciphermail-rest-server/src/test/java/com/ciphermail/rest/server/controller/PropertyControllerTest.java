/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.properties.PropertyValidationFailedException;
import com.ciphermail.core.app.properties.UserPropertiesAccessor;
import com.ciphermail.core.app.properties.UserPropertiesAccessorImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.properties.UserPropertyOption;
import com.ciphermail.core.app.properties.UserPropertyRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.util.BeanUtilsBeanBuilderImpl;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PropertyDTO;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.function.Failable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import java.io.StringWriter;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PropertyController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class
})
public class PropertyControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices
    {
        @Bean
        public UserPropertiesAccessor createUserPropertiesAccessor(UserPropertyRegistry userPropertyRegistry,
                UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
        {
            return new UserPropertiesAccessorImpl(userPropertyRegistry, new BeanUtilsBeanBuilderImpl(),
                    userPropertiesFactoryRegistry);
        }
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            // delete all users
            userWorkflow.getUsers(null, null, SortDirection.ASC).forEach(
                    u -> userWorkflow.deleteUser(u));
            // delete all domains
            domainManager.deleteAllDomains();
        }));

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any(), any());
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any(), any(), any());
    }

    @Test
    @WithMockUser
    public void getUserProperty()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "USER", "password-policy");

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "password-policy")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        PropertyDTO.PropertyValue response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("password-policy", response.name());
        assertEquals("{\"r\":[{\"p\":\"\\\\S\"}]}", response.value());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "USER", "password-policy");
    }

    @Test
    @WithMockUser
    public void getUserPropertyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "password-policy")
                        .contentType("application/json"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "USER", "password-policy");
    }

    @Test
    @WithMockUser
    public void getUserPropertyUnknownProperty()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "unknown")
                        .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("Property with name unknown does not exist", result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void getUserPropertyNoUserProperty()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "ca-key-length")
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Property with name ca-key-length is not a user property",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setUserProperty()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                userWorkflow.makePersistent(userWorkflow.addUser("test@example.com"))));

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "pgp-max-size");

        mockMvc.perform(post(RestPaths.PROPERTY_SET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "pgp-max-size")
                        .content("111111")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "pgp-max-size");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "USER", "pgp-max-size");

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "pgp-max-size")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        PropertyDTO.PropertyValue response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("111111", response.value());
    }

    @Test
    @WithMockUser
    public void setUserPropertyNoPermission()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                userWorkflow.makePersistent(userWorkflow.addUser("test@example.com"))));

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "pgp-max-size")
                        .content("111111")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "pgp-max-size");
    }

    @Test
    @WithMockUser
    public void setUserPropertyUnknownProperty()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                userWorkflow.makePersistent(userWorkflow.addUser("test@example.com"))));

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "unknown")
                        .content("test")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("Property with name unknown does not exist", result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setUserPropertyNoUser()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_USER_PROPERTY_PATH)
                        .param("email", "unknown-user@example.com")
                        .param("name", "unknown")
                        .content("test")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("User with email address unknown-user@example.com not found",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setUserPropertyInvalidValue()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                userWorkflow.makePersistent(userWorkflow.addUser("test@example.com"))));

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "pgp-max-size");

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "pgp-max-size")
                        .content("not-an-integer")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Error converting from 'String' to 'Long' For input string: \"not-an-integer\"",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setUserPropertyValidationFailure()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                userWorkflow.makePersistent(userWorkflow.addUser("test@example.com"))));

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "sms-phone-number");

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "sms-phone-number")
                        .content("invalid")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();


        PropertyValidationFailedException validationFailedException = (PropertyValidationFailedException)
                result.getResolvedException();

        assertEquals("invalid is not a valid phone number", validationFailedException.getMessage());
        assertEquals("backend.validation.invalid-phone-number", validationFailedException.getKey());
        assertEquals("sms-phone-number", validationFailedException.getPropertyName());
    }

    @Test
    @WithMockUser
    public void setUserPropertyNoUserProperty()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                userWorkflow.makePersistent(userWorkflow.addUser("test@example.com"))));

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_USER_PROPERTY_PATH)
                        .param("email", "test@example.com")
                        .param("name", "ca-key-length")
                        .content("test")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Property with name ca-key-length is not a user property",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setUserProperties()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "pgp-max-size");
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "sms-phone-number");

        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                userWorkflow.makePersistent(userWorkflow.addUser("test@example.com"))));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(
                new PropertyDTO.PropertyRequest("pgp-max-size", "123"),
                new PropertyDTO.PropertyRequest("sms-phone-number", "31611111111")));

        mockMvc.perform(post(RestPaths.PROPERTY_SET_USER_PROPERTIES_PATH)
                        .param("email", "test@example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "pgp-max-size");
        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "sms-phone-number");
    }

    @Test
    @WithMockUser
    public void setUserPropertiesMissingPermission()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "pgp-max-size");

        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                userWorkflow.makePersistent(userWorkflow.addUser("test@example.com"))));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(
                new PropertyDTO.PropertyRequest("pgp-max-size", "123"),
                new PropertyDTO.PropertyRequest("sms-phone-number", "31611111111")));

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_USER_PROPERTIES_PATH)
                        .param("email", "test@example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "pgp-max-size");
        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "USER", "sms-phone-number");
    }

    @Test
    @WithMockUser
    public void getDomainProperty()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "DOMAIN", "password-policy");

        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "password-policy")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        PropertyDTO.PropertyValue response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("password-policy", response.name());
        assertEquals("{\"r\":[{\"p\":\"\\\\S\"}]}", response.value());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "DOMAIN", "password-policy");
    }

    @Test
    @WithMockUser
    public void getDomainPropertyNoPermission()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "password-policy")
                        .contentType("application/json"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "DOMAIN", "password-policy");
    }

    @Test
    @WithMockUser
    public void getDomainPropertyUnknownProperty()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "unknown")
                        .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("Property with name unknown does not exist", result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void getDomainPropertyNoDomainProperty()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "ca-key-length")
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Property with name ca-key-length is not a domain property",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setDomainProperty()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "pgp-max-size");

        mockMvc.perform(post(RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "pgp-max-size")
                        .content("777")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "pgp-max-size");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "DOMAIN", "pgp-max-size");

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "pgp-max-size")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        PropertyDTO.PropertyValue response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("777", response.value());
    }

    @Test
    @WithMockUser
    public void setDomainPropertyNoPermission()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "pgp-max-size")
                        .content("111111")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "pgp-max-size");
    }

    @Test
    @WithMockUser
    public void setDomainPropertyUnknownProperty()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "unknown")
                        .content("test")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("Property with name unknown does not exist", result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setDomainPropertyNoDomain()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "password-policy")
                        .content("test")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("Domain example.com not found",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setDomainPropertyInvalidValue()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "pgp-max-size");

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "pgp-max-size")
                        .content("not-an-integer")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Error converting from 'String' to 'Long' For input string: \"not-an-integer\"",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setDomainPropertyValidationFailure()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "sms-phone-number");

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "sms-phone-number")
                        .content("invalid")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        PropertyValidationFailedException validationFailedException = (PropertyValidationFailedException)
                result.getResolvedException();

        assertEquals("invalid is not a valid phone number", validationFailedException.getMessage());
        assertEquals("backend.validation.invalid-phone-number", validationFailedException.getKey());
        assertEquals("sms-phone-number", validationFailedException.getPropertyName());
    }

    @Test
    @WithMockUser
    public void setDomainPropertyNoDomainProperty()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH)
                        .param("domain", "example.com")
                        .param("name", "ca-key-length")
                        .content("test")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Property with name ca-key-length is not a domain property",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setDomainProperties()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "pgp-max-size");
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "sms-phone-number");

        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(
                new PropertyDTO.PropertyRequest("pgp-max-size", "123"),
                new PropertyDTO.PropertyRequest("sms-phone-number", "31611111111")));

        mockMvc.perform(post(RestPaths.PROPERTY_SET_DOMAIN_PROPERTIES_PATH)
                        .param("domain", "example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "pgp-max-size");
        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "sms-phone-number");
    }

    @Test
    @WithMockUser
    public void setDomainPropertiesMissingPermission()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "pgp-max-size");

        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
                domainManager.addDomain("example.com")));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(
                new PropertyDTO.PropertyRequest("pgp-max-size", "123"),
                new PropertyDTO.PropertyRequest("sms-phone-number", "31611111111")));

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_DOMAIN_PROPERTIES_PATH)
                        .param("domain", "example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "pgp-max-size");
        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "DOMAIN", "sms-phone-number");
    }

    @Test
    @WithMockUser
    public void getGlobalProperty()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "GLOBAL", "password-policy");

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_GLOBAL_PROPERTY_PATH)
                        .param("name", "password-policy")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        PropertyDTO.PropertyValue response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("password-policy", response.name());
        assertEquals("{\"r\":[{\"p\":\"\\\\S\"}]}", response.value());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "GLOBAL", "password-policy");
    }

    @Test
    @WithMockUser
    public void getGlobalPropertyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_GLOBAL_PROPERTY_PATH)
                        .param("name", "password-policy")
                        .contentType("application/json"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "GLOBAL", "password-policy");
    }

    @Test
    @WithMockUser
    public void getGlobalPropertyUnknownProperty()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_GLOBAL_PROPERTY_PATH)
                        .param("name", "unknown")
                        .contentType("application/json"))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("Property with name unknown does not exist", result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void getGlobalPropertyNoGlobalProperty()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_GLOBAL_PROPERTY_PATH)
                        .param("name", "date-created")
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Property with name date-created is not a global property",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setGlobalProperty()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "pgp-max-size");

        mockMvc.perform(post(RestPaths.PROPERTY_SET_GLOBAL_PROPERTY_PATH)
                        .param("name", "pgp-max-size")
                        .content("99999")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "pgp-max-size");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "GET",
                "GLOBAL", "pgp-max-size");

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_GLOBAL_PROPERTY_PATH)
                        .param("name", "pgp-max-size")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        PropertyDTO.PropertyValue response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("99999", response.value());
    }

    @Test
    @WithMockUser
    public void setGlobalPropertyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_GLOBAL_PROPERTY_PATH)
                        .param("name", "pgp-max-size")
                        .content("9999")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "pgp-max-size");
    }

    @Test
    @WithMockUser
    public void setGlobalPropertyUnknownProperty()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_GLOBAL_PROPERTY_PATH)
                        .param("name", "unknown")
                        .content("test")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("Property with name unknown does not exist", result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setGlobalPropertyInvalidValue()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "pgp-max-size");

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_GLOBAL_PROPERTY_PATH)
                        .param("name", "pgp-max-size")
                        .content("not-an-integer")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Error converting from 'String' to 'Long' For input string: \"not-an-integer\"",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setGlobalPropertyValidationFailure()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "sms-phone-number");

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_GLOBAL_PROPERTY_PATH)
                        .param("name", "sms-phone-number")
                        .content("invalid")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        PropertyValidationFailedException validationFailedException = (PropertyValidationFailedException)
                result.getResolvedException();

        assertEquals("invalid is not a valid phone number", validationFailedException.getMessage());
        assertEquals("backend.validation.invalid-phone-number", validationFailedException.getKey());
        assertEquals("sms-phone-number", validationFailedException.getPropertyName());
    }

    @Test
    @WithMockUser
    public void setGlobalPropertyNoGlobalProperty()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_GLOBAL_PROPERTY_PATH)
                        .param("name", "date-created")
                        .content("test")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Property with name date-created is not a global property",
                result.getResponse().getErrorMessage());
    }

    @Test
    @WithMockUser
    public void setGlobalProperties()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "pgp-max-size");
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "sms-phone-number");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(
                new PropertyDTO.PropertyRequest("pgp-max-size", "123"),
                new PropertyDTO.PropertyRequest("sms-phone-number", "31611111111")));

        mockMvc.perform(post(RestPaths.PROPERTY_SET_GLOBAL_PROPERTIES_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "pgp-max-size");
        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "sms-phone-number");
    }

    @Test
    @WithMockUser
    public void setGlobalPropertiesMissingPermission()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "pgp-max-size");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(
                new PropertyDTO.PropertyRequest("pgp-max-size", "123"),
                new PropertyDTO.PropertyRequest("sms-phone-number", "31611111111")));

        MvcResult result = mockMvc.perform(post(RestPaths.PROPERTY_SET_GLOBAL_PROPERTIES_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "pgp-max-size");
        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "sms-phone-number");
    }

    @Test
    @WithMockUser
    public void getAvailableProperties()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY,
                "get-property-details");

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_AVAILABLE_PROPERTIES_PATH)
                        .param("skipUnauthorized", "false"))
                .andExpect(status().isOk())
                .andReturn();

        List<PropertyDTO.UserPropertiesDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertFalse(response.isEmpty());

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY,
                "get-property-details");

        result = mockMvc.perform(get(RestPaths.PROPERTY_GET_AVAILABLE_PROPERTIES_PATH)
                        .param("skipUnauthorized", "true"))
                .andExpect(status().isOk())
                .andReturn();

        response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertTrue(response.isEmpty());

        when(permissionChecker.hasPermission(PermissionCategory.PROPERTY, "GET",
                "USER", "date-created")).thenReturn(true);

        when(permissionChecker.hasPermission(PermissionCategory.PROPERTY, "GET",
                "DOMAIN", "sms-phone-number")).thenReturn(true);

        when(permissionChecker.hasPermission(PermissionCategory.PROPERTY, "SET",
                "GLOBAL", "pgp-max-size")).thenReturn(true);

        result = mockMvc.perform(get(RestPaths.PROPERTY_GET_AVAILABLE_PROPERTIES_PATH)
                        .param("skipUnauthorized", "true"))
                .andExpect(status().isOk())
                .andReturn();

        response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(3, response.size());
    }

    @Test
    @WithMockUser
    public void getAvailablePropertiesNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_AVAILABLE_PROPERTIES_PATH)
                        .param("skipUnauthorized", "false"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void getUserPropertyDescriptor()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PROPERTY,
                "get-property-details");

        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_USER_PROPERTY_DESCRIPTOR_PATH)
                        .param("name", "smime-signing-algorithm"))
                .andExpect(status().isOk())
                .andReturn();

        PropertyDTO.UserPropertyDescriptorDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        System.out.println(response);

        List<?> values = (List<?>)response.getter().type().options().get(UserPropertyOption.VALUES);

        assertEquals(14, values.size());

        // check some values
        assertTrue(values.contains("SHA256_WITH_RSA"));
        assertTrue(values.contains("SHA512_WITH_RSA"));

        verify(permissionChecker).checkPermission(PermissionCategory.PROPERTY,
                "get-property-details");
    }

    @Test
    @WithMockUser
    public void getUserPropertyDescriptorNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PROPERTY_GET_USER_PROPERTY_DESCRIPTOR_PATH)
                        .param("name", "smime-signing-algorithm"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());
    }
}
