/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.james.MailEnqueuer;
import com.ciphermail.core.app.james.MailRepositoryStoreProvider;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.MailDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.MailDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.james.mailrepository.api.MailKey;
import org.apache.james.mailrepository.api.MailRepository;
import org.apache.james.mailrepository.api.MailRepositoryPath;
import org.apache.james.mailrepository.api.MailRepositoryStore;
import org.apache.james.mailrepository.api.MailRepositoryUrl;
import org.apache.james.mailrepository.api.Protocol;
import org.apache.mailet.Mail;
import org.apache.mailet.base.test.FakeMail;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = MailRepositoryController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class,
        MailDetailsFactory.class})
public class MailRepositoryControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean
    private MailRepositoryStoreProvider mailRepositoryStoreProvider;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @MockBean
    private MailEnqueuer mailEnqueuer;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    static class MockMailRepository implements MailRepository
    {
        private final Map<MailKey, Mail> mails = new HashMap<>();

        @Override
        public long size() {
            return 0;
        }

        @Override
        public MailKey store(Mail mc) {
            return null;
        }

        @Override
        public Iterator<MailKey> list() {
            return mails.keySet().iterator();
        }

        @Override
        public Mail retrieve(MailKey key)
        {
            try {
                return mails.get(key);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void remove(MailKey key) {
            // empty on purpose
        }

        @Override
        public void removeAll() {
            // empty on purpose
        }

        MockMailRepository addMail(Mail mail)
        {
            mails.put(MailKey.forMail(mail), mail);
            return this;
        }
    }

    static class MockMailRepositoryStore implements MailRepositoryStore
    {
        private final MockMailRepository mailRepository;
        private Stream<MailRepositoryUrl> urls;

        MockMailRepositoryStore(@Nonnull MockMailRepository mailRepository) {
            this.mailRepository = mailRepository;
        }

        @Override
        public MailRepository select(MailRepositoryUrl url) {
            return null;
        }

        @Override
        public Optional<Protocol> defaultProtocol() {
            return Optional.empty();
        }

        @Override
        public Optional<MailRepository> get(MailRepositoryUrl url) {
            return Optional.of(mailRepository);
        }

        @Override
        public Stream<MailRepository> getByPath(MailRepositoryPath path) {
            return null;
        }

        @Override
        public Stream<MailRepositoryUrl> getUrls() {
            return urls;
        }

        public MockMailRepositoryStore setUrls(Stream<MailRepositoryUrl> urls)
        {
            this.urls = urls;
            return this;
        }
    }

    private static Mail createMail(String key)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject("test");
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        return FakeMail.builder().name(key)
                .recipients("test@example.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .state(Mail.DEFAULT)
                .build();
    }

    @Test
    @WithMockUser
    public void getMailRepositoryUrls()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mail-repository-urls");

        MockMailRepository mailRepository = Mockito.mock(MockMailRepository.class);

        when(mailRepository.size()).thenReturn(10L);

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                mailRepository).setUrls(Stream.of(MailRepositoryUrl.from("http://example.com"))));

        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_REPOSITORY_GET_MAIL_REPOSITORY_URLS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        List<String> results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, results.size());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mail-repository-urls");
    }

    @Test
    @WithMockUser
    public void getMailRepositoryUrlsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_REPOSITORY_GET_MAIL_REPOSITORY_URLS_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mail-repository-urls");
    }

    @Test
    @WithMockUser
    public void getMailKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mail-keys");

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                new MockMailRepository().addMail(createMail("mail1")).addMail(createMail("mail2"))));

        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_REPOSITORY_GET_MAIL_KEYS_PATH)
                        .param("mailRepositoryUrl", "http://example.com"))
                .andExpect(status().isOk())
                .andReturn();

        List<String> results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, results.size());
        assertEquals("mail1", results.get(0));
        assertEquals("mail2", results.get(1));

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mail-keys");
    }

    @Test
    @WithMockUser
    public void getMailKeysNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_REPOSITORY_GET_MAIL_KEYS_PATH)
                        .param("mailRepositoryUrl", "http://example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(mailRepositoryStoreProvider, never()).getMailRepositoryStore();

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mail-keys");
    }

    @Test
    @WithMockUser
    public void getMailRepositorySize()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-size");

        MockMailRepository mailRepository = Mockito.mock(MockMailRepository.class);

        when(mailRepository.size()).thenReturn(10L);

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                mailRepository));

        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_REPOSITORY_GET_SIZE_PATH)
                        .param("mailRepositoryUrl", "http://example.com"))
                .andExpect(status().isOk())
                .andReturn();

        Long response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(10L, (long) response);

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-size");
    }

    @Test
    @WithMockUser
    public void getMailRepositorySizeNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_REPOSITORY_GET_SIZE_PATH)
                        .param("mailRepositoryUrl", "http://example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(mailRepositoryStoreProvider, never()).getMailRepositoryStore();

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-size");
    }

    @Test
    @WithMockUser
    public void getMails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mails");

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                new MockMailRepository().addMail(createMail("test-key-1")).addMail(createMail("test-key-2"))));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test-key-1", "test-key-2"));

        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_GET_MAILS_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<MailDTO.MailDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mails");
    }

    @Test
    @WithMockUser
    public void getMailsNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test-key-1", "test-key-2"));

        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_GET_MAILS_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mails");
    }

    @Test
    @WithMockUser
    public void getMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mail");

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                new MockMailRepository().addMail(createMail("test-key"))));

        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_REPOSITORY_GET_MAIL_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .param("key", "test-key")
                        .param("includeMIME", "true"))
                .andExpect(status().isOk())
                .andReturn();

        MailDTO.MailDetailsWithMIME response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test-key", response.mailDetails().name());
        assertEquals("test@example.com", String.join(",", response.mailDetails().recipients()));
        assertEquals("sender@example.com", response.mailDetails().sender());
        assertEquals("root", response.mailDetails().state());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mail");
    }

    @Test
    @WithMockUser
    public void getMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_REPOSITORY_GET_MAIL_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .param("key", "not-used")
                        .param("includeMIME", "true"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "get-mail");
    }

    @Test
    @WithMockUser
    public void deleteMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "delete-mail");

        MockMailRepository mailRepository = Mockito.mock(MockMailRepository.class);

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                mailRepository));

        mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_DELETE_MAIL_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .param("key", "not-used")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(mailRepository).remove((MailKey) any());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "delete-mail");
    }

    @Test
    @WithMockUser
    public void deleteMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_DELETE_MAIL_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .param("key", "not-used")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(mailRepositoryStoreProvider, never()).getMailRepositoryStore();

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "delete-mail");
    }

    @Test
    @WithMockUser
    public void deleteMails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "delete-mails");

        MockMailRepository mailRepository = Mockito.mock(MockMailRepository.class);

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                mailRepository));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test-key-1", "test-key-2"));

        mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_DELETE_MAILS_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(mailRepository).remove(List.of(new MailKey("test-key-1"), new MailKey("test-key-2")));

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "delete-mails");
    }

    @Test
    @WithMockUser
    public void deleteMailsNoPermission()
    throws Exception
    {
        MockMailRepository mailRepository = Mockito.mock(MockMailRepository.class);

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                mailRepository));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test-key-1", "test-key-2"));

        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_DELETE_MAILS_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "delete-mails");
    }

    @Test
    @WithMockUser
    public void deleteAllMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "delete-all-mail");

        MockMailRepository mailRepository = Mockito.mock(MockMailRepository.class);

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                mailRepository));

        mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_DELETE_ALL_MAIL_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(mailRepository).removeAll();

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "delete-all-mail");
    }

    @Test
    @WithMockUser
    public void deleteAllMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_DELETE_ALL_MAIL_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(mailRepositoryStoreProvider, never()).getMailRepositoryStore();

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "delete-all-mail");
    }

    @Test
    @WithMockUser
    public void requeueMails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "requeue-mails");

        MockMailRepository mailRepository = new MockMailRepository();

        mailRepository.addMail(createMail("test-key-1"));
        mailRepository.addMail(createMail("test-key-2"));

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                mailRepository));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test-key-1", "test-key-2"));

        mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_REQUEUE_MAILS_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(mailEnqueuer, times(2)).enqueue(any());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "requeue-mails");
    }

    @Test
    @WithMockUser
    public void requeueMailsNoPermission()
    throws Exception
    {
        MockMailRepository mailRepository = new MockMailRepository();

        mailRepository.addMail(createMail("test-key-1"));
        mailRepository.addMail(createMail("test-key-2"));

        when(mailRepositoryStoreProvider.getMailRepositoryStore()).thenReturn(new MockMailRepositoryStore(
                mailRepository));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test-key-1", "test-key-2"));

        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_REPOSITORY_REQUEUE_MAILS_PATH)
                        .param("mailRepositoryUrl", "http://example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(mailEnqueuer, never()).enqueue(any());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_REPOSITORY,
                "requeue-mails");
    }
}