/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.csr.CSRStore;
import com.ciphermail.core.common.csr.CSRStoreEntryImpl;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.certificate.CSRBuilder;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.rest.core.TLSDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockPart;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = TLSController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
@TestPropertySource(properties = {
    "ciphermail.tls.additional-trust-anchors-path=" +
            "../ciphermail-core/src/test/resources/testdata/tls/easy-rsa-tls-test-root.pem",
        "ciphermail.tls.private-key-path=" + TLSControllerTest.INSTALL_CERT_PATH
})
public class TLSControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    static final String INSTALL_CERT_PATH = "/tmp/com.ciphermail.rest.server.controller.cer";

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @MockBean
    private CSRStore csrStore;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    private CSRStoreEntryImpl createCSRStoreEntry(String id)
    throws Exception
    {
        CSRBuilder csrBuilder = CSRBuilder.getInstance();

        csrBuilder.setSubject(X500PrincipalBuilder.getInstance()
                .setEmail("test@example.com")
                .setOrganisation("Organisation")
                .setOrganisationalUnit("OrganisationalUnit")
                .setCountryCode("CountryCode")
                .setState("State")
                .setLocality("Locality")
                .setCommonName("CommonName")
                .setGivenName("GivenName")
                .setSurname("Surname")
                .buildName());

        PKCS10CertificationRequest pkcs10 = csrBuilder.buildPKCS10();

        CSRStoreEntryImpl entry = new CSRStoreEntryImpl(id);

        entry.setKeyPair(csrBuilder.getKeyPair());
        entry.setCSR(pkcs10);

        return entry;
    }

    @Test
    @WithMockUser
    public void createCSR()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "create-csr");

        when(csrStore.generateCSR(any(), any(), any())).thenReturn(createCSRStoreEntry("test"));

        TLSDTO.Request request = new TLSDTO.Request().setCommonName("test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, request);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(RestPaths.TLS_CREATE_CSR_PATH)
                        .param("keyAlgorithm", TLSDTO.KeyAlgorithm.SECP256R1.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        TLSDTO.CSRStoreEntry results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test", results.id());
        assertEquals("EC", results.keyAlgorithm());
        assertEquals(256, results.keyLength());

        assertEquals("1.2.840.113549.1.9.1=test@example.com,givenName=GivenName,sn=Surname,cn=CommonName," +
                     "ou=OrganisationalUnit,o=Organisation,l=Locality,st=State,c=CountryCode", results.subject());
        assertFalse(results.certificateAvailable());

        verify(csrStore).generateCSR(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "create-csr");
    }

    @Test
    @WithMockUser
    public void createCSRNoPermission()
    throws Exception
    {
        TLSDTO.Request request = new TLSDTO.Request().setCommonName("test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, request);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(RestPaths.TLS_CREATE_CSR_PATH)
                        .param("keyAlgorithm", TLSDTO.KeyAlgorithm.SECP256R1.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(csrStore, never()).generateCSR(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "create-csr");
    }

    @Test
    @WithMockUser
    public void getCSRs()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "get-csrs");

        when(csrStore.getCSRs()).thenReturn(List.of(createCSRStoreEntry("test1"), createCSRStoreEntry("test2")));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(RestPaths.TLS_GET_CSRS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        List<TLSDTO.CSRStoreEntry> results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, results.size());

        verify(csrStore).getCSRs();

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "get-csrs");
    }

    @Test
    @WithMockUser
    public void getCSRsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(RestPaths.TLS_GET_CSRS_PATH))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(csrStore, never()).getCSRs();

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "get-csrs");
    }

    @Test
    @WithMockUser
    public void getCSR()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "get-csr");

        when(csrStore.getCSR("test")).thenReturn(createCSRStoreEntry("test"));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(RestPaths.TLS_GET_CSR_PATH)
                        .param("id", "test"))
                .andExpect(status().isOk())
                .andReturn();

        TLSDTO.CSRStoreEntry results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test", results.id());

        verify(csrStore).getCSR("test");

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "get-csr");
    }

    @Test
    @WithMockUser
    public void getCSRNoMatch()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "get-csr");

        mockMvc.perform(MockMvcRequestBuilders.get(RestPaths.TLS_GET_CSR_PATH)
                        .param("id", "test"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("CSR with id test not found"));

        verify(csrStore).getCSR("test");

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "get-csr");
    }

    @Test
    @WithMockUser
    public void getCSRNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(RestPaths.TLS_GET_CSR_PATH)
                        .param("id", "test"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(csrStore, never()).getCSR(any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "get-csr");
    }

    @Test
    @WithMockUser
    public void deleteCSR()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "delete-csr");

        when(csrStore.getCSR("test")).thenReturn(createCSRStoreEntry("test"));

        mockMvc.perform(MockMvcRequestBuilders.post(RestPaths.TLS_DELETE_CSR_PATH)
                        .param("id", "test")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(csrStore).deleteCSR("test");

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "delete-csr");
    }

    @Test
    @WithMockUser
    public void deleteCSRNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(RestPaths.TLS_DELETE_CSR_PATH)
                        .param("id", "test")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(csrStore, never()).getCSR(any());
        verify(csrStore, never()).deleteCSR(any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "delete-csr");
    }

    @Test
    @WithMockUser
    public void exportPKCS10Request()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "export-pkcs10");

        when(csrStore.getCSR("test")).thenReturn(createCSRStoreEntry("test"));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(RestPaths.TLS_EXPORT_PKCS10_REQUEST_PATH)
                        .param("id", "test")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        PKCS10CertificationRequest pkcs10 = (PKCS10CertificationRequest) new PEMParser(new StringReader(
                result.getResponse().getContentAsString())).readObject();

        assertEquals(X500PrincipalBuilder.getInstance()
                .setEmail("test@example.com")
                .setOrganisation("Organisation")
                .setOrganisationalUnit("OrganisationalUnit")
                .setCountryCode("CountryCode")
                .setState("State")
                .setLocality("Locality")
                .setCommonName("CommonName")
                .setGivenName("GivenName")
                .setSurname("Surname")
                .buildName(), pkcs10.getSubject());

        verify(csrStore).getCSR("test");

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "export-pkcs10");
    }

    @Test
    @WithMockUser
    public void exportPKCS10RequestNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(RestPaths.TLS_EXPORT_PKCS10_REQUEST_PATH)
                        .param("id", "test")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(csrStore, never()).getCSR(any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "export-pkcs10");
    }

    @Test
    @WithMockUser
    public void importCertificateChain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "import-certificate-chain");

        CSRStoreEntryImpl entry = createCSRStoreEntry("test");

        X509Certificate[] chain = CollectionUtils.toArray(CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "tls/easy-rsa-tls-test.cer")), X509Certificate.class);

        entry.setCertificateChain(chain);

        when(csrStore.findCSR(any())).thenReturn(entry);
        when(csrStore.importCertificate(any(), any())).thenReturn(entry);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.TLS_IMPORT_CERTIFICATE_CHAIN_PATH)
                        .file("chain", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "tls/easy-rsa-tls-test.cer")))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        TLSDTO.CSRStoreEntry resultEntry = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertTrue(resultEntry.certificateAvailable());

        verify(csrStore).importCertificate(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "import-certificate-chain");
    }

    @Test
    @WithMockUser
    public void importCertificateChainInvalidChain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "import-certificate-chain");

        CSRStoreEntryImpl entry = createCSRStoreEntry("test");

        X509Certificate[] chain = CollectionUtils.toArray(CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testcertificate.cer")), X509Certificate.class);

        entry.setCertificateChain(chain);

        when(csrStore.findCSR(any())).thenReturn(entry);
        when(csrStore.importCertificate(any(), any())).thenReturn(entry);

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.TLS_IMPORT_CERTIFICATE_CHAIN_PATH)
                        .file("chain", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/testcertificate.cer")))
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("No issuer certificate for certificate in certification path found."))
                .andReturn();

        verify(csrStore, never()).importCertificate(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "import-certificate-chain");
    }

    @Test
    @WithMockUser
    public void importCertificatesNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.TLS_IMPORT_CERTIFICATE_CHAIN_PATH)
                        .file("chain", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/mitm-test-chain.pem.cer")))
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(csrStore, never()).importCertificate(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "import-certificate-chain");
    }

    @Test
    @WithMockUser
    public void exportCertificateChain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "export-certificate-chain");

        CSRStoreEntryImpl entry = createCSRStoreEntry("test");

        X509Certificate[] chain = CollectionUtils.toArray(CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/mitm-test-chain.pem.cer")), X509Certificate.class);

        entry.setCertificateChain(chain);

        when(csrStore.getCSR("test")).thenReturn(entry);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(RestPaths.TLS_EXPORT_CERTIFICATE_CHAIN_PATH)
                        .param("id", "test")
                        .content("some-password")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        PEMParser parser = new PEMParser(new StringReader(result.getResponse().getContentAsString()));

        assertTrue(parser.readObject() instanceof X509CertificateHolder);

        PEMEncryptedKeyPair encryptedKeyPair = (PEMEncryptedKeyPair) parser.readObject();

        PEMKeyPair pemKeyPair = encryptedKeyPair.decryptKeyPair(new JcePEMDecryptorProviderBuilder()
                .build("some-password".toCharArray()));

        assertNotNull(pemKeyPair.getPrivateKeyInfo());
        assertNotNull(pemKeyPair.getPublicKeyInfo());

        assertTrue(parser.readObject() instanceof X509CertificateHolder);
        assertTrue(parser.readObject() instanceof X509CertificateHolder);
        assertNull(parser.readObject());

        verify(csrStore).getCSR("test");

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "export-certificate-chain");
    }

    @Test
    @WithMockUser
    public void exportCertificateChainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(RestPaths.TLS_EXPORT_CERTIFICATE_CHAIN_PATH)
                        .param("id", "test")
                        .content("some-password")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(csrStore, never()).getCSR(any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "export-certificate-chain");
    }

    @Test
    @WithMockUser
    public void installCertificate()
    throws Exception
    {
        File certFile = new File(INSTALL_CERT_PATH);

        FileUtils.deleteQuietly(certFile);

        assertFalse(certFile.exists());

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "install-certificate");

        CSRStoreEntryImpl entry = createCSRStoreEntry("test");

        X509Certificate[] chain = CollectionUtils.toArray(CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/mitm-test-chain.pem.cer")), X509Certificate.class);

        entry.setCertificateChain(chain);

        when(csrStore.getCSR("test")).thenReturn(entry);

        mockMvc.perform(MockMvcRequestBuilders.post(RestPaths.TLS_INSTALL_CERTIFICATE_PATH)
                        .param("id", "test")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(certFile.exists());

        PEMParser parser = new PEMParser(new StringReader(FileUtils.readFileToString(new File(INSTALL_CERT_PATH),
                StandardCharsets.UTF_8)));

        assertTrue(parser.readObject() instanceof X509CertificateHolder);

        PEMKeyPair pemKeyPair = (PEMKeyPair) parser.readObject();

        assertNotNull(pemKeyPair.getPrivateKeyInfo());
        assertNotNull(pemKeyPair.getPublicKeyInfo());

        assertTrue(parser.readObject() instanceof X509CertificateHolder);
        assertTrue(parser.readObject() instanceof X509CertificateHolder);
        assertNull(parser.readObject());

        verify(csrStore).getCSR("test");

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "install-certificate");
    }

    @Test
    @WithMockUser
    public void installCertificateNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(RestPaths.TLS_INSTALL_CERTIFICATE_PATH)
                        .param("id", "test")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(csrStore, never()).getCSR(any());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "install-certificate");
    }

    @Test
    @WithMockUser
    public void importPKCS12()
    throws Exception
    {
        File certFile = new File(INSTALL_CERT_PATH);

        FileUtils.deleteQuietly(certFile);

        assertFalse(certFile.exists());

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.TLS,
                "import-pkcs12");

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.TLS_IMPORT_PKCS12_PATH)
                        .file("pkcs12", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "tls/easy-rsa-tls-test.p12")))
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(certFile.exists());

        PEMParser parser = new PEMParser(new StringReader(FileUtils.readFileToString(new File(INSTALL_CERT_PATH),
                StandardCharsets.UTF_8)));

        assertTrue(parser.readObject() instanceof X509CertificateHolder);

        PEMKeyPair pemKeyPair = (PEMKeyPair) parser.readObject();

        assertNotNull(pemKeyPair.getPrivateKeyInfo());
        assertNotNull(pemKeyPair.getPublicKeyInfo());

        assertTrue(parser.readObject() instanceof X509CertificateHolder);
        assertNull(parser.readObject());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "import-pkcs12");
    }

    @Test
    @WithMockUser
    public void importPKCS12NoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.TLS_IMPORT_PKCS12_PATH)
                        .file("pkcs12", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "tls/easy-rsa-tls-test.p12")))
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.TLS,
                "import-pkcs12");
    }
}
