/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.impl.MockupUserPreferences;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserPreferencesWorkflow;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.ExtendedKeyUsageType;
import com.ciphermail.core.common.security.certificate.KeyUsageType;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilderFactory;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorAdapter;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.CertificateImportAction;
import com.ciphermail.rest.core.CertificateStore;
import com.ciphermail.rest.core.KeyExportFormat;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockPart;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertPath;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CertificateController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        X509CertificateDetailsFactory.class,
        PermissionProviderRegistry.class
})
public class CertificateControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    private static final String NON_EXISTING_THUMBPRINT =
            "0000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000";

    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    private SessionManager sessionManager;

    @MockBean
    @Qualifier("certStore")
    private X509CertStoreExt certificateStore;

    @MockBean
    @Qualifier("rootStore")
    private X509CertStoreExt rootStore;

    @MockBean
    @Qualifier("keyAndCertStore")
    private KeyAndCertStore keyAndCertStore;

    @MockBean
    @Qualifier("keyStoreProvider")
    private KeyStoreProvider keyStoreProvider;

    @MockBean
    @Qualifier("keyAndRootCertStore")
    private KeyAndCertStore keyAndRootStore;

    @MockBean
    @Qualifier("keyAndCertificateWorkflow")
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @MockBean
    @Qualifier("keyAndRootCertificateWorkflow")
    private KeyAndCertificateWorkflow keyAndRootCertificateWorkflow;

    @MockBean
    private UserPreferencesWorkflow userPreferencesWorkflow;

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    private CTLManager ctlManager;

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    private CertificatePathBuilderFactory certificatePathBuilderFactory;

    @MockBean
    private PermissionChecker permissionChecker;

    @Autowired
    private MockMvc mockMvc;

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    private X509CertStoreEntry createX509CertStoreEntry(@Nonnull X509Certificate certificate)
    {
        return new X509CertStoreEntry()
        {
            @Override
            public @Nonnull X509Certificate getCertificate() {
                return certificate;
            }

            @Override
            public void setCertificatePath(CertPath chain) {}

            @Override
            public CertPath getCertificatePath() {
                return null;
            }

            @Override
            public void setDatePathUpdated(Date date) {}

            @Override
            public Date getDatePathUpdated() {
                return null;
            }

            @Override
            public void setCreationDate(Date creationDate) {}

            @Override
            public Date getCreationDate() {
                return null;
            }

            @Override
            public @Nonnull Set<String> getEmail() {
                return Collections.emptySet();
            }

            @Override
            public void setKeyAlias(String keyAlias) {}

            @Override
            public String getKeyAlias() {
                return null;
            }
        };
    }

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @Test
    @WithMockUser
    public void testGetCertificateDetails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "get-certificate-details");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_GET_CERTIFICATE_DETAILS_PATH)
                        .file("chain", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/testCertificates.p7b")))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(20, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificate-details");
    }

    @Test
    @WithMockUser
    public void testGetCertificateDetailsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_GET_CERTIFICATE_DETAILS_PATH)
                        .file("chain", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/testCertificates.p7b")))
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificate-details");
    }

    @Test
    @WithMockUser
    public void testGetCertificates()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "get-certificates");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        CloseableIteratorAdapter<X509CertStoreEntry> iterator = new CloseableIteratorAdapter<>(
                certificateEntries.iterator());

        OngoingStubbing<CloseableIterator<? extends X509CertStoreEntry>> stubbing = when(
                certificateStore.getCertStoreIterator((Expired) any(), any(), any(), any()));

        stubbing.thenReturn(iterator);

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_GET_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(20, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificates");
    }

    @Test
    @WithMockUser
    public void testGetCertificatesNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_GET_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificates");
    }

    @Test
    @WithMockUser
    public void testGetCertificatesTooManyResults()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "get-certificates");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        CloseableIteratorAdapter<X509CertStoreEntry> iterator = new CloseableIteratorAdapter<>(
                certificateEntries.iterator());

        OngoingStubbing<CloseableIterator<? extends X509CertStoreEntry>> stubbing = when(
                certificateStore.getCertStoreIterator((Expired) any(), any(), any(), any()));

        stubbing.thenReturn(iterator);

        mockMvc.perform(get(RestPaths.CERTIFICATE_GET_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("maxResults", "251")
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("maxResults exceed the upper limit 250"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificates");
    }

    @Test
    @WithMockUser
    public void testGetCertificatesCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "get-certificates-count");

        when(rootStore.size(any(), any())).thenReturn(10L);

        mockMvc.perform(get(RestPaths.CERTIFICATE_GET_CERTIFICATES_COUNT_PATH)
                        .param("store", CertificateStore.ROOTS.name())
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificates-count");
    }

    @Test
    @WithMockUser
    public void testGetCertificatesCountNoPermission()
    throws Exception
    {
        when(rootStore.size(any(), any())).thenReturn(10L);

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_GET_CERTIFICATES_COUNT_PATH)
                        .param("store", CertificateStore.ROOTS.name())
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificates-count");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesByEmail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-email");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        CloseableIteratorAdapter<X509CertStoreEntry> iterator = new CloseableIteratorAdapter<>(
                certificateEntries.iterator());

        OngoingStubbing<CloseableIterator<? extends X509CertStoreEntry>> stubbing = when(
                certificateStore.getByEmail(any(), any(), any(), any(), any(), any()));

        stubbing.thenReturn(iterator);

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("email", "test@example.com")
                        .param("match", Match.LIKE.name())
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(20, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-email");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesByEmailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("email", "test@example.com")
                        .param("match", Match.LIKE.name())
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-email");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesByEmailCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-email-count");

        when(certificateStore.getByEmailCount(any(), any(), any(), any())).thenReturn(10L);

        mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_COUNT_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("email", "test@example.com")
                        .param("match", Match.LIKE.name())
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-email-count");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesByEmailCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_COUNT_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("email", "test@example.com")
                        .param("match", Match.LIKE.name())
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-email-count");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesBySubject()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-subject");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/random-self-signed-1000.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        CloseableIteratorAdapter<X509CertStoreEntry> iterator = new CloseableIteratorAdapter<>(
                certificateEntries.iterator());

        OngoingStubbing<CloseableIterator<? extends X509CertStoreEntry>> stubbing = when(
                certificateStore.searchBySubject(any(), any(), any(), any(), any()));

        stubbing.thenReturn(iterator);

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("subject", "test")
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1000, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-subject");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesBySubjectNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("subject", "test")
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-subject");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesBySubjectCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-subject-count");

        when(certificateStore.getSearchBySubjectCount(any(), any(), any())).thenReturn(10L);

        mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_COUNT_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("subject", "test")
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-subject-count");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesBySubjectCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_COUNT_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("subject", "test")
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-subject-count");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesByIssuer()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-issuer");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/random-self-signed-1000.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        CloseableIteratorAdapter<X509CertStoreEntry> iterator = new CloseableIteratorAdapter<>(
                certificateEntries.iterator());

        OngoingStubbing<CloseableIterator<? extends X509CertStoreEntry>> stubbing = when(
                certificateStore.searchByIssuer(any(), any(), any(), any(), any()));

        stubbing.thenReturn(iterator);

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("issuer", "test")
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1000, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-issuer");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesByIssuerNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("issuer", "test")
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-issuer");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesByIssuerCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-issuer-count");

        when(certificateStore.getSearchByIssuerCount(any(), any(), any())).thenReturn(Long.MAX_VALUE);

        mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_COUNT_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("issuer", "test")
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().isOk())
                .andExpect(content().string(Long.toString(Long.MAX_VALUE)));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-issuer-count");
    }

    @Test
    @WithMockUser
    public void testSearchForCertificatesByIssuerCountNoPermission()
    throws Exception
    {
        when(certificateStore.getSearchByIssuerCount(any(), any(), any())).thenReturn(Long.MAX_VALUE);

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_COUNT_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("issuer", "test")
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "search-for-certificates-by-issuer-count");
    }

    @Test
    @WithMockUser
    public void testGetCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "get-certificate");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/hash-starts-with-0.cer")).stream().map(this::createX509CertStoreEntry).toList();

        when(certificateStore.getByThumbprint(any())).thenReturn(certificateEntries.get(0));

        MvcResult result = mockMvc.perform(get( RestPaths.CERTIFICATE_GET_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.X509CertificateDetails certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CertificateDTO.X509CertificateDetails.class);

        assertEquals("05818393AAE1765AD32F1D78C157220147469091E9B1BA92E6C94E687A679BEB5FA85CC1058" +
                     "5FC5090283602EEAD6595A91C993A3D4ED99D6F517EF2ED7DC73E", certificateDetails.thumbprint());
        assertEquals("6E346DA406EE3160CE9FDEF33CB814C1433C5E13", certificateDetails.thumbprintSHA1());
        assertEquals("1178C30016DF6384363585B394C6CA6", certificateDetails.serialNumberHex());
        assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL",
                certificateDetails.issuerFriendly());
        assertEquals("EMAILADDRESS=test@example.com, CN=No S/MIME extended key usage non critical, " +
                     "L=Amsterdam, ST=NH, C=NL", certificateDetails.subjectFriendly());
        assertNull(certificateDetails.subjectKeyIdentifier());
        assertFalse(certificateDetails.selfSigned());
        assertEquals(1193917595000L, (long) certificateDetails.notBefore());
        assertEquals(1826797595000L, (long) certificateDetails.notAfter());
        assertFalse(certificateDetails.expired());
        assertThat(certificateDetails.emailFromAltNames(), containsInAnyOrder("test@example.com"));
        assertThat(certificateDetails.emailFromDN(), containsInAnyOrder("test@example.com"));
        assertThat(certificateDetails.keyUsage(), containsInAnyOrder(
                KeyUsageType.DIGITALSIGNATURE,
                KeyUsageType.KEYENCIPHERMENT,
                KeyUsageType.NONREPUDIATION));
        assertThat(certificateDetails.extendedKeyUsage(), containsInAnyOrder(ExtendedKeyUsageType.CLIENTAUTH));
        assertFalse(certificateDetails.ca());
        assertNull(certificateDetails.pathLengthConstraint());
        assertEquals(1024, (int) certificateDetails.publicKeyLength());
        assertEquals("RSA", certificateDetails.publicKeyAlgorithm());
        assertEquals("SHA1WITHRSA", certificateDetails.signatureAlgorithm());
        assertEquals(0, certificateDetails.uriDistributionPointNames().size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificate");
    }

    @Test
    @WithMockUser
    public void testGetCertificateNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_GET_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificate");
    }

    @Test
    @WithMockUser
    public void testGetCertificateCertificateMissing()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "get-certificate");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_GET_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificate");
    }

    @Test
    @WithMockUser
    public void testGetCertificateInvalidThumbprint()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "get-certificate");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_GET_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "invalid"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                ",\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException" +
                "\",\"error\":\"Bad Request\",\"message\":\"invalid is not a valid postfix thumbprint\"," +
                "\"key\":\"backend.validation.invalid-thumbprint\",\"params\":null,\"path\":\"\"}"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificate");
    }

    @Test
    @WithMockUser
    public void testImportCertificates()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-certificates");

        MutableInt certCounter = new MutableInt();

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/random-self-signed-10000.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        when(certificateStore.addCertificate(any())).thenAnswer(certificate -> {
            certCounter.increment();
            return certificateEntries.get(certCounter.intValue() - 1);
        });

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PATH)
                        .file("certificates", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/random-self-signed-10000.p7b")))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("importAction", CertificateImportAction.IMPORT_ALL.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(10000, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates");
    }

    @Test
    @WithMockUser
    public void testImportCertificatesNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PATH)
                        .file("certificates", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/random-self-signed-10000.p7b")))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("importAction", CertificateImportAction.IMPORT_ALL.name())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates");
    }

    @Test
    @WithMockUser
    public void testImportCertificatesSkipSelfSigned()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-certificates");

        MutableInt certCounter = new MutableInt();

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/mitm-test-chain.pem.cer")).stream().map(this::createX509CertStoreEntry).toList();

        when(certificateStore.addCertificate(any())).thenAnswer(certificate -> {
            certCounter.increment();
            return certificateEntries.get(certCounter.intValue() - 1);
        });

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PATH)
                        .file("certificates", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/mitm-test-chain.pem.cer")))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("importAction", CertificateImportAction.SKIP_SELF_SIGNED.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates");
    }

    @Test
    @WithMockUser
    public void testImportCertificatesMustBeSelfSigned()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-certificates");

        MutableInt certCounter = new MutableInt();

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/mitm-test-chain.pem.cer")).stream().map(this::createX509CertStoreEntry).toList();

        when(certificateStore.addCertificate(any())).thenAnswer(certificate -> {
            certCounter.increment();
            return certificateEntries.get(certCounter.intValue() - 1);
        });

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PATH)
                        .file("certificates", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/mitm-test-chain.pem.cer")))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("importAction", CertificateImportAction.MUST_BE_SELF_SIGNED.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates");
    }

    @Test
    @WithMockUser
    public void testImportCertificatesImportAll()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-certificates");

        MutableInt certCounter = new MutableInt();

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/mitm-test-chain.pem.cer")).stream().map(this::createX509CertStoreEntry).toList();

        when(certificateStore.addCertificate(any())).thenAnswer(certificate -> {
            certCounter.increment();
            return certificateEntries.get(certCounter.intValue() - 1);
        });

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PATH)
                        .file("certificates", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/mitm-test-chain.pem.cer")))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("importAction", CertificateImportAction.IMPORT_ALL.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(3, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates");
    }

    @Test
    @WithMockUser
    public void testImportCertificateInvalidCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-certificates");

        MutableInt certCounter = new MutableInt();

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/mitm-test-chain.pem.cer")).stream().map(this::createX509CertStoreEntry).toList();

        when(certificateStore.addCertificate(any())).thenAnswer(certificate -> {
            certCounter.increment();
            return certificateEntries.get(certCounter.intValue() - 1);
        });

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PATH)
                        .file("certificates", "invalid".getBytes())
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("importAction", CertificateImportAction.IMPORT_ALL.name())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("No certificates in request"));

        assertEquals(0, certCounter.intValue());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates");
    }

    @Test
    @WithMockUser
    public void testImportCertificatesPEM()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-certificates-pem");

        MutableInt certCounter = new MutableInt();

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/mozilla-all-root-ca-certs.pem")).stream().map(this::createX509CertStoreEntry).toList();

        when(rootStore.addCertificate(any())).thenAnswer(certificate -> {
            certCounter.increment();
            return certificateEntries.get(certCounter.intValue() - 1);
        });

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PEM_PATH)
                        .param("store", CertificateStore.ROOTS.name())
                        .param("importAction", CertificateImportAction.IMPORT_ALL.name())
                        .content(FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/mozilla-all-root-ca-certs.pem")))
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(151, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates-pem");
    }

    @Test
    @WithMockUser
    public void testImportCertificatesPEMNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PEM_PATH)
                        .param("store", CertificateStore.ROOTS.name())
                        .param("importAction", CertificateImportAction.IMPORT_ALL.name())
                        .content(FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/mozilla-all-root-ca-certs.pem")))
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates-pem");
    }

    @Test
    @WithMockUser
    public void testImportCertificatesPEMInvalidCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-certificates-pem");

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PEM_PATH)
                        .param("store", CertificateStore.ROOTS.name())
                        .param("importAction", CertificateImportAction.IMPORT_ALL.name())
                        .content("invalid".getBytes())
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("No certificates in request"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates-pem");
    }

    @Test
    @WithMockUser
    public void testImportCertificatesBase64()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-certificates-base64");

        MutableInt certCounter = new MutableInt();

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/random-self-signed-10000.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        when(rootStore.addCertificate(any())).thenAnswer(certificate -> {
            certCounter.increment();
            return certificateEntries.get(certCounter.intValue() - 1);
        });

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_BASE64_PATH)
                        .param("store", CertificateStore.ROOTS.name())
                        .param("importAction", CertificateImportAction.IMPORT_ALL.name())
                        .content(Base64Utils.encode(FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/random-self-signed-10000.p7b"))))
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(10000, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates-base64");
    }

    @Test
    @WithMockUser
    public void testImportCertificatesBase64NoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_BASE64_PATH)
                        .param("store", CertificateStore.ROOTS.name())
                        .param("importAction", CertificateImportAction.IMPORT_ALL.name())
                        .content(Base64Utils.encode(FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/random-self-signed-10000.p7b"))))
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-certificates-base64");
    }

    @Test
    @WithMockUser
    public void testDeleteCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "delete-certificate");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/hash-starts-with-0.cer")).stream().map(this::createX509CertStoreEntry).toList();

        when(certificateStore.getByThumbprint(any())).thenReturn(certificateEntries.get(0));

        mockMvc.perform(post(RestPaths.CERTIFICATE_DELETE_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT)
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(certificateStore).removeCertificate(any());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "delete-certificate");
    }

    @Test
    @WithMockUser
    public void testDeleteCertificateNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_DELETE_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT)
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "delete-certificate");
    }

    @Test
    @WithMockUser
    public void testDeleteCertificates()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "delete-certificates");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        for (X509CertStoreEntry certStoreEntry : certificateEntries) {
            when(certificateStore.getByThumbprint(X509CertificateInspector.getThumbprint(certStoreEntry.getCertificate()))).thenReturn(certStoreEntry);
        }

        List<String> thumbprints = certificateEntries.stream().map(e ->
        {
            try {
                return X509CertificateInspector.getThumbprint(e.getCertificate());
            }
            catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }).toList();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_DELETE_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<String> notDeleted = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>(){});

        assertEquals(0, notDeleted.size());

        for (X509CertStoreEntry certStoreEntry : certificateEntries) {
            verify(certificateStore).removeCertificate(certStoreEntry.getCertificate());
        }

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "delete-certificates");
    }

    @Test
    @WithMockUser
    public void testDeleteCertificatesNoPermission()
    throws Exception
    {
        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        for (X509CertStoreEntry certStoreEntry : certificateEntries) {
            when(certificateStore.getByThumbprint(X509CertificateInspector.getThumbprint(certStoreEntry.getCertificate()))).thenReturn(certStoreEntry);
        }

        List<String> thumbprints = certificateEntries.stream().map(e ->
        {
            try {
                return X509CertificateInspector.getThumbprint(e.getCertificate());
            }
            catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }).toList();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_DELETE_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(certificateStore, never()).removeCertificate(any());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "delete-certificates");
    }


    @Test
    @WithMockUser
    public void testDeleteCertificatesInUse()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "delete-certificates");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        for (X509CertStoreEntry certStoreEntry : certificateEntries) {
            when(certificateStore.getByThumbprint(X509CertificateInspector.getThumbprint(certStoreEntry.getCertificate()))).thenReturn(certStoreEntry);
        }

        when(userPreferencesWorkflow.getReferencingFromCertificates(eq(certificateEntries.get(0).getCertificate()),
                any(), any())).thenReturn(List.of(
                        new MockupUserPreferences("test@example.com", "")));

        List<String> thumbprints = certificateEntries.stream().map(e ->
        {
            try {
                return X509CertificateInspector.getThumbprint(e.getCertificate());
            }
            catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }).toList();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_DELETE_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<String> notDeleted = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>(){});

        assertEquals(1, notDeleted.size());
        assertEquals(X509CertificateInspector.getThumbprint(certificateEntries.get(0).getCertificate()), notDeleted.get(0));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "delete-certificates");
    }

    @Test
    @WithMockUser
    public void testExportCertificateDER()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-certificate");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/certeurope_root_ca_illegal_crl_dist_point.crt"));

        List<X509CertStoreEntry> certificateEntries = certificates.stream().map(
                this::createX509CertStoreEntry).toList();

        when(certificateStore.getByThumbprint(any())).thenReturn(certificateEntries.get(0));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT)
                        .param("encoding", ObjectEncoding.DER.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<X509Certificate> retrievedCertificates = CertificateUtils.readX509Certificates(new ByteArrayInputStream(
                result.getResponse().getContentAsByteArray()));

        assertEquals(certificates, retrievedCertificates);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificate");
    }

    @Test
    @WithMockUser
    public void testExportCertificateDERNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT)
                        .param("encoding", ObjectEncoding.DER.name())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificate");
    }

    @Test
    @WithMockUser
    public void testExportCertificatePEM()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-certificate");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/certeurope_root_ca_illegal_crl_dist_point.crt"));

        List<X509CertStoreEntry> certificateEntries = certificates.stream().map(
                this::createX509CertStoreEntry).toList();

        when(certificateStore.getByThumbprint(any())).thenReturn(certificateEntries.get(0));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT)
                        .param("encoding", ObjectEncoding.PEM.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<X509Certificate> retrievedCertificates = CertificateUtils.readX509Certificates(new ByteArrayInputStream(
                result.getResponse().getContentAsByteArray()));

        assertEquals(certificates, retrievedCertificates);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificate");
    }

    @Test
    @WithMockUser
    public void testExportCertificateChain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-certificate-chain");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/certeurope_root_ca_illegal_crl_dist_point.crt"));

        List<X509CertStoreEntry> certificateEntries = certificates.stream().map(
                this::createX509CertStoreEntry).toList();

        when(certificateStore.getByThumbprint(any())).thenReturn(certificateEntries.get(0));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATE_CHAIN_PATH)
                        .param("thumbprint", NON_EXISTING_THUMBPRINT)
                        .param("encoding", ObjectEncoding.PEM.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<X509Certificate> retrievedCertificates = CertificateUtils.readX509Certificates(new ByteArrayInputStream(
                result.getResponse().getContentAsByteArray()));

        assertEquals(0, retrievedCertificates.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificate-chain");
    }

    @Test
    @WithMockUser
    public void testExportCertificateChainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATE_CHAIN_PATH)
                        .param("thumbprint", NON_EXISTING_THUMBPRINT)
                        .param("encoding", ObjectEncoding.PEM.name())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificate-chain");
    }

    @Test
    @WithMockUser
    public void testExportCertificatesDER()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-certificates");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b"));

        List<X509CertStoreEntry> certificateEntries = certificates.stream().map(
                this::createX509CertStoreEntry).toList();

        List<String> thumbprints = new LinkedList<>();

        for (X509CertStoreEntry entry : certificateEntries)
        {
            String thumbprint = X509CertificateInspector.getThumbprint(entry.getCertificate());

            thumbprints.add(thumbprint);

            when(certificateStore.getByThumbprint(thumbprint)).thenReturn(entry);
        }

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("encoding", ObjectEncoding.DER.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] encodedCertificates = result.getResponse().getContentAsByteArray();

        assertEquals(0x30, encodedCertificates[0]);

        List<X509Certificate> retrievedCertificates = CertificateUtils.readX509Certificates(new ByteArrayInputStream(
                encodedCertificates));

        assertEquals(certificates, retrievedCertificates);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificates");
    }

    @Test
    @WithMockUser
    public void testExportCertificatesDERNoPermission()
    throws Exception
    {
        StringWriter jsonWriter =new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("123"));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("encoding", ObjectEncoding.DER.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificates");
    }

    @Test
    @WithMockUser
    public void testExportCertificatesPEM()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-certificates");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b"));

        List<X509CertStoreEntry> certificateEntries = certificates.stream().map(
                this::createX509CertStoreEntry).toList();

        List<String> thumbprints = new LinkedList<>();

        for (X509CertStoreEntry entry : certificateEntries)
        {
            String thumbprint = X509CertificateInspector.getThumbprint(entry.getCertificate());

            thumbprints.add(thumbprint);

            when(certificateStore.getByThumbprint(thumbprint)).thenReturn(entry);
        }

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("encoding", ObjectEncoding.PEM.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] encodedCertificates = result.getResponse().getContentAsByteArray();

        assertTrue(new String(encodedCertificates).contains("-----BEGIN CERTIFICATE-----"));
        assertTrue(new String(encodedCertificates).contains("-----END CERTIFICATE-----"));

        List<X509Certificate> retrievedCertificates = CertificateUtils.readX509Certificates(new ByteArrayInputStream(
                encodedCertificates));

        assertEquals(certificates, retrievedCertificates);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificates");
    }

    @Test
    @WithMockUser
    public void testExportCertificatesCertificateNotFound()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-certificates");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b"));

        List<X509CertStoreEntry> certificateEntries = certificates.stream().map(
                this::createX509CertStoreEntry).toList();

        List<String> thumbprints = new LinkedList<>();

        for (X509CertStoreEntry entry : certificateEntries)
        {
            String thumbprint = X509CertificateInspector.getThumbprint(entry.getCertificate());

            thumbprints.add(thumbprint);

            when(certificateStore.getByThumbprint(thumbprint)).thenReturn(entry);
        }

        // now add an invalid thumbprint
        thumbprints.add(NON_EXISTING_THUMBPRINT);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("encoding", ObjectEncoding.PEM.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("Certificate with thumbprint " + NON_EXISTING_THUMBPRINT + " not found"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificates");
    }

    @Test
    @WithMockUser
    public void testTooManyThumbprints()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-certificates");

        List<String> thumbprints = new LinkedList<>();

        for (int i = 0; i < 251; i++) {
            thumbprints.add(Integer.toString(i));
        }

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("encoding", ObjectEncoding.PEM.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("number of thumbprints exceed the upper limit 250"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificates");
    }

    @Test
    @WithMockUser
    public void exportCertificatesMissingThumbprints()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-certificates");

        List<String> thumbprints = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_CERTIFICATES_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("encoding", ObjectEncoding.PEM.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("thumbprints are missing"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-certificates");
    }

    @Test
    @WithMockUser
    public void testImportPrivateKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-private-keys");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        when(keyAndCertificateWorkflow.importKeyStoreTransacted (any(), any())).thenReturn(certificateEntries);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH)
                        .file("file", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "keys/testCertificates.p12")))
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("missingKeyAction", MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(20, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-private-keys");
    }

    @Test
    @WithMockUser
    public void testImportPrivateKeysNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH)
                        .file("pkcs12", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "keys/testCertificates.p12")))
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("missingKeyAction", MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY.name())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-private-keys");
    }

    @Test
    @WithMockUser
    public void testImportPrivateKeysRootStore()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-private-keys");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        when(keyAndRootCertificateWorkflow.importKeyStoreTransacted (any(), any())).thenReturn(certificateEntries);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH)
                        .file("file", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "keys/testCertificates.p12")))
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .param("store", CertificateStore.ROOTS.name())
                        .param("missingKeyAction", MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(20, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-private-keys");
    }

    @Test
    @WithMockUser
    public void testImportPrivateKeysInvalidPassword()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-private-keys");

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH)
                        .file("file", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "keys/testCertificates.p12")))
                        .part(new MockPart("keyStorePassword", "invalid".getBytes(StandardCharsets.UTF_8)))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("missingKeyAction", MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY.name())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("PKCS12 key store mac invalid - wrong password or corrupted file."));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-private-keys");
    }

    @Test
    @WithMockUser
    public void testImportPrivateKeysInvalidPKCS12()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-private-keys");

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH)
                        .file("file", "invalid".getBytes())
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("missingKeyAction", MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY.name())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("Error loading private keys file"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-private-keys");
    }

    @Test
    @WithMockUser
    public void testImportPrivateKeysPEM()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-private-keys");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b")).stream().map(this::createX509CertStoreEntry).toList();

        when(keyAndCertificateWorkflow.importKeyStoreTransacted (any(), any())).thenReturn(certificateEntries);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH)
                        .file("file", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "keys/testCertificates.pem")))
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("missingKeyAction", MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(20, certificateDetails.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-private-keys");
    }

    @Test
    @WithMockUser
    public void testImportPrivateKeysCertificatesOnly()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-private-keys");

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH)
                        .file("file", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/mitm-test-chain.pem.cer")))
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("missingKeyAction", MissingKeyAction.SKIP_CERTIFICATE_ONLY_ENTRY.name())
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("File does not contain any private keys"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-private-keys");
    }

    @Test
    @WithMockUser
    public void testExportPrivateKeysDER()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-private-keys");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b"));

        List<X509CertStoreEntry> certificateEntries = certificates.stream().map(
                this::createX509CertStoreEntry).toList();

        List<String> thumbprints = new LinkedList<>();

        for (X509CertStoreEntry entry : certificateEntries)
        {
            String thumbprint = X509CertificateInspector.getThumbprint(entry.getCertificate());

            thumbprints.add(thumbprint);

            when(keyAndCertStore.getByThumbprint(thumbprint)).thenReturn(entry);
        }

        byte[] testResultNoP12 = "not a p12".getBytes();

        doAnswer(invocation -> {invocation.<OutputStream>getArgument(3).write(testResultNoP12); return null;})
                .when(keyAndCertificateWorkflow).copyKeysToPKCS12OutputstreamTransacted(any(), any(), anyBoolean(), any());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new CertificateDTO.ExportPrivateKeysRequestBody(
                "test", thumbprints));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("keyExportFormat", KeyExportFormat.PKCS12.name())
                        .param("includeRoot", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        Assert.assertArrayEquals(testResultNoP12, result.getResponse().getContentAsByteArray());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-private-keys");
    }

    @Test
    @WithMockUser
    public void testExportPrivateKeysDERNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new CertificateDTO.ExportPrivateKeysRequestBody(
                "test", List.of("invalid thumbprint")));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("keyExportFormat", KeyExportFormat.PKCS12.name())
                        .param("includeRoot", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-private-keys");
    }

    @Test
    @WithMockUser
    public void testExportPrivateKeysPEM()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-private-keys");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b"));

        List<X509CertStoreEntry> certificateEntries = certificates.stream().map(
                this::createX509CertStoreEntry).toList();

        List<String> thumbprints = new LinkedList<>();

        for (X509CertStoreEntry entry : certificateEntries)
        {
            String thumbprint = X509CertificateInspector.getThumbprint(entry.getCertificate());

            thumbprints.add(thumbprint);

            when(keyAndCertStore.getByThumbprint(thumbprint)).thenReturn(entry);
        }

        byte[] testResultNoPEM = "not a PEM".getBytes();

        doAnswer(invocation -> {invocation.<OutputStream>getArgument(3).write(testResultNoPEM); return null;})
                .when(keyAndCertificateWorkflow).copyKeysToPEMOutputstreamTransacted(any(), any(), anyBoolean(), any());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new CertificateDTO.ExportPrivateKeysRequestBody(
                "test", thumbprints));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("keyExportFormat", KeyExportFormat.PEM.name())
                        .param("includeRoot", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        Assert.assertArrayEquals(testResultNoPEM, result.getResponse().getContentAsByteArray());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-private-keys");
    }

    @Test
    @WithMockUser
    public void testExportPrivateKeysNoMatchingThumbprints()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-private-keys");

        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testCertificates.p7b"));

        List<X509CertStoreEntry> certificateEntries = certificates.stream().map(
                this::createX509CertStoreEntry).toList();

        List<String> thumbprints = new LinkedList<>();

        for (X509CertStoreEntry entry : certificateEntries)
        {
            String thumbprint = X509CertificateInspector.getThumbprint(entry.getCertificate());

            thumbprints.add(thumbprint);

            when(keyAndCertStore.getByThumbprint(thumbprint)).thenReturn(entry);
        }

        // add a fake thumbprint
        thumbprints.add(NON_EXISTING_THUMBPRINT);

        byte[] testResultNotAP12 = "not a p12".getBytes();

        doAnswer(invocation -> {invocation.<OutputStream>getArgument(3).write(testResultNotAP12); return null;})
                .when(keyAndCertificateWorkflow).copyKeysToPKCS12OutputstreamTransacted(any(), any(), anyBoolean(), any());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new CertificateDTO.ExportPrivateKeysRequestBody(
                "test", thumbprints));

        mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("keyExportFormat", KeyExportFormat.PKCS12.name())
                        .param("includeRoot", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("No entry with thumbprint " + NON_EXISTING_THUMBPRINT + " found"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-private-keys");
    }

    @Test
    @WithMockUser
    public void testExportPrivateKeysInvalidThumbprint()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-private-keys");

        List<String> thumbprints = new LinkedList<>();

        // add invalid thumbprint
        thumbprints.add("test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new CertificateDTO.ExportPrivateKeysRequestBody(
                "test", thumbprints));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("keyExportFormat", KeyExportFormat.PKCS12.name())
                        .param("includeRoot", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                ",\"status\":400,\"className\":\"com.ciphermail.core.app.properties." +
                "PropertyValidationFailedException\",\"error\":\"Bad Request\"," +
                "\"message\":\"test is not a valid postfix thumbprint\"," +
                "\"key\":\"backend.validation.invalid-thumbprint\",\"params\":null,\"path\":\"\"}"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-private-keys");
    }

    @Test
    @WithMockUser
    public void testExportPrivateKeysThumbprintsExceedMax()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-private-keys");

        List<String> thumbprints = new LinkedList<>();

        for (int i = 0; i < 251; i++) {
            thumbprints.add(Integer.toString(i));
        }

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new CertificateDTO.ExportPrivateKeysRequestBody(
                "test", thumbprints));

        mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("password", "test")
                        .param("keyExportFormat", KeyExportFormat.PKCS12.name())
                        .param("includeRoot", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("number of thumbprints exceed the upper limit 250"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-private-keys");
    }

    @Test
    @WithMockUser
    public void testExportPrivateKeysNoThumbprints()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "export-private-keys");

        List<String> thumbprints = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new CertificateDTO.ExportPrivateKeysRequestBody(
                "test", thumbprints));

        mockMvc.perform(post(RestPaths.CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("keyExportFormat", KeyExportFormat.PKCS12.name())
                        .param("includeRoot", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("Thumbprints are missing"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "export-private-keys");
    }

    @Test
    @WithMockUser
    public void testGetCertificateReferencedDetails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificate-referenced-details");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/hash-starts-with-0.cer")).stream().map(this::createX509CertStoreEntry).toList();

        when(certificateStore.getByThumbprint(any())).thenReturn(certificateEntries.get(0));
        when(userPreferencesWorkflow.getReferencingFromCertificates(any(), any(), any())).thenReturn(
                List.of(new MockupUserPreferences("name1", "cat1"),
                        new MockupUserPreferences("name2", "cat2")));

        MvcResult result = mockMvc.perform(get( RestPaths.CERTIFICATE_GET_CERTIFICATE_REFERENCED_DETAILS_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.CertificateReference> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>(){});

        assertEquals(2, response.size());
        assertEquals("name1", response.get(0).name());
        assertEquals("cat1", response.get(0).category());
        assertEquals("name2", response.get(1).name());
        assertEquals("cat2", response.get(1).category());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificate-referenced-details");
    }

    @Test
    @WithMockUser
    public void testGetCertificateReferencedDetailsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get( RestPaths.CERTIFICATE_GET_CERTIFICATE_REFERENCED_DETAILS_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "get-certificate-referenced-details");
    }

    @Test
    @WithMockUser
    public void testIsCertificateReferenced()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "is-certificate-referenced");

        List<X509CertStoreEntry> certificateEntries = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/hash-starts-with-0.cer")).stream().map(this::createX509CertStoreEntry).toList();

        when(certificateStore.getByThumbprint(any())).thenReturn(certificateEntries.get(0));
        when(userPreferencesWorkflow.getReferencingFromCertificates(any(), any(), any())).thenReturn(
                List.of(new MockupUserPreferences("name1", "cat1")));

        mockMvc.perform(get( RestPaths.CERTIFICATE_IS_CERTIFICATE_REFERENCED_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "is-certificate-referenced");
    }

    @Test
    @WithMockUser
    public void testIsCertificateReferencedNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get( RestPaths.CERTIFICATE_IS_CERTIFICATE_REFERENCED_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "is-certificate-referenced");
    }

    @Test
    @WithMockUser
    public void testImportSystemRoots()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE, "import-system-roots");

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_IMPORT_SYSTEM_ROOTS_PATH)
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>(){});

        assertFalse(response.isEmpty());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-system-roots");
    }

    @Test
    @WithMockUser
    public void testImportSystemRootsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_IMPORT_SYSTEM_ROOTS_PATH)
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE,
                "import-system-roots");
    }
}