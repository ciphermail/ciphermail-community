/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.mail.MailTransport;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.postfix.MapType;
import com.ciphermail.postfix.PostCat;
import com.ciphermail.postfix.PostConf;
import com.ciphermail.postfix.PostQueue;
import com.ciphermail.postfix.PostSuper;
import com.ciphermail.postfix.PostfixConfigureMain;
import com.ciphermail.postfix.PostfixCtl;
import com.ciphermail.postfix.PostfixMap;
import com.ciphermail.postfix.PostfixMapManager;
import com.ciphermail.rest.core.MTADTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.authorization.Permissions;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.MailDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockPart;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.mail.internet.InternetAddress;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = MTAController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class,
        MailDetailsFactory.class})
public class MTAControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean
    private PostQueue postqueue;

    @MockBean
    private PostSuper postsuper;

    @MockBean
    private PostCat postcat;

    @MockBean
    private PostfixConfigureMain postfixConfigureMain;

    @MockBean
    private PostfixCtl postfixCtl;

    @MockBean
    private PostConf postConf;

    @MockBean
    private PostfixMapManager postfixMapManager;

    @MockBean
    private MailTransport mailTransport;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @MockBean
    private Permissions permissions;

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any(), any());
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any(), any(), any());
    }

    private static class MockPostfixMap implements PostfixMap
    {
        private final String name;
        private final MapType type;
        private String content;

        MockPostfixMap(String name, MapType type)
        {
            this.name = name;
            this.type = type;
        }

        MockPostfixMap(String name, MapType type, String content)
        {
            this(name, type);
            this.content = content;
        }

        @Nonnull
        @Override
        public String getName() {
            return name;
        }

        @Nonnull
        @Override
        public MapType getMapType() {
            return type;
        }

        @Nonnull
        @Override
        public String getFilename() {
            return type.name() + "-" + name;
        }

        @Override
        public String getContent() {
            return content;
        }

        @Override
        public void setContent(@Nonnull String content) {
            this.content = content;
        }
    }

    @Test
    @WithMockUser
    public void getQueueList()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-queue-list");

        when(postqueue.listQueue(any(), any())).thenReturn(
                List.of("{\"queue_name\": \"deferred\", \"queue_id\": \"4TTBxY4Vsqz3Tq85\", " +
                        "\"arrival_time\": 1707151345, \"message_size\": 458, " +
                        "\"forced_expire\": false, \"sender\": \"martijn@workstation\", " +
                        "\"recipients\": [{\"address\": \"test@example.com\", " +
                        "\"delay_reason\": \"connect to 127.0.0.1[127.0.0.1]:10025: Connection refused\"}]}",
                        "{\"queue_name\": \"deferred\", \"queue_id\": \"4TTByb5Qd9z3Tq88\", " +
                        "\"arrival_time\": 1707151399, \"message_size\": 460, " +
                        "\"forced_expire\": false, \"sender\": \"martijn@workstation\", " +
                        "\"recipients\": [{\"address\": \"test2@example.com\", \"delay_reason\": " +
                        "\"connect to 127.0.0.1[127.0.0.1]:10025: Connection refused\"}]}",
                        "{\"queue_name\": \"deferred\", \"queue_id\": \"4TTByh10rMz3Tq89\", " +
                        "\"arrival_time\": 1707151404, \"message_size\": 460, " +
                        "\"forced_expire\": false, \"sender\": \"martijn@workstation\", " +
                        "\"recipients\": [{\"address\": \"test3@example.com\", " +
                        "\"delay_reason\": \"connect to 127.0.0.1[127.0.0.1]:10025: Connection refused\"}]}"));

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_QUEUE_LIST_PATH))
                .andExpect(status().isOk())
                .andReturn();

        List<MTADTO.QueueItem> results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(3, results.size());
        assertEquals("4TTBxY4Vsqz3Tq85", results.get(0).queue_id());
        assertEquals("4TTByb5Qd9z3Tq88", results.get(1).queue_id());
        assertEquals("4TTByh10rMz3Tq89", results.get(2).queue_id());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-queue-list");
    }

    @Test
    @WithMockUser
    public void getQueueListNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_QUEUE_LIST_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postqueue, never()).listQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-queue-list");
    }

    @Test
    @WithMockUser
    public void getQueueSize()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-queue-size");

        when(postqueue.getQueueSize()).thenReturn(10L);

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_QUEUE_SIZE_PATH))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("10", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-queue-size");
    }

    @Test
    @WithMockUser
    public void getQueueSizeNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_QUEUE_SIZE_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postqueue, never()).getQueueSize();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-queue-size");
    }

    @Test
    @WithMockUser
    public void flushQueue()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "flush-queue");

        mockMvc.perform(post(RestPaths.MTA_FLUSH_QUEUE_PATH)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postqueue).flushQueue();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "flush-queue");
    }

    @Test
    @WithMockUser
    public void flushQueueNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MTA_FLUSH_QUEUE_PATH)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postqueue, never()).flushQueue();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "flush-queue");
    }

    @Test
    @WithMockUser
    public void rescheduleDeferredMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "reschedule-deferred-mail");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        mockMvc.perform(post(RestPaths.MTA_RESCHEDULE_DEFERRED_MAIL_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postqueue, times(2)).rescheduleDeferredMail(any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "reschedule-deferred-mail");
    }

    @Test
    @WithMockUser
    public void rescheduleDeferredMailNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_RESCHEDULE_DEFERRED_MAIL_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postqueue, never()).rescheduleDeferredMail(any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "reschedule-deferred-mail");
    }

    @Test
    @WithMockUser
    public void deleteMailFromQueue()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "delete-mail");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        mockMvc.perform(post(RestPaths.MTA_DELETE_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.DEFERRED.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postsuper, times(2)).deleteMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "delete-mail");
    }

    @Test
    @WithMockUser
    public void deleteMailFromQueueNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_DELETE_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.DEFERRED.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postsuper, never()).deleteMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "delete-mail");
    }

    @Test
    @WithMockUser
    public void holdMailFromQueue()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "hold-mail");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        mockMvc.perform(post(RestPaths.MTA_HOLD_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.HOLD.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postsuper, times(2)).holdMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "hold-mail");
    }

    @Test
    @WithMockUser
    public void holdMailFromQueueNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_HOLD_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.HOLD.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postsuper, never()).holdMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "hold-mail");
    }

    @Test
    @WithMockUser
    public void releaseMailFromQueue()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "release-mail");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        mockMvc.perform(post(RestPaths.MTA_RELEASE_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.HOLD.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postsuper, times(2)).releaseHoldMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "release-mail");
    }

    @Test
    @WithMockUser
    public void releaseMailFromQueueNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_RELEASE_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.HOLD.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postsuper, never()).releaseHoldMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "release-mail");
    }

    @Test
    @WithMockUser
    public void bounceMailFromQueue()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "bounce-mail");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        mockMvc.perform(post(RestPaths.MTA_BOUNCE_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.HOLD.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postsuper, times(2)).bounceMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "bounce-mail");
    }

    @Test
    @WithMockUser
    public void bounceMailFromQueueNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_BOUNCE_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.HOLD.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postsuper, never()).bounceMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "bounce-mail");
    }

    @Test
    @WithMockUser
    public void requeueMailFromQueue()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "requeue-mail");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        mockMvc.perform(post(RestPaths.MTA_REQUEUE_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.HOLD.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postsuper, times(2)).requeueMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "requeue-mail");
    }

    @Test
    @WithMockUser
    public void requeueMailFromQueueNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("4TTBxY4Vsqz3Tq85", "4TTBxY4Vsqz3Tq86"));

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_REQUEUE_MAIL_PATH)
                        .param("queueName", MTADTO.QueueName.HOLD.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postsuper, never()).requeueMailFromQueue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "requeue-mail");
    }

    @Test
    @WithMockUser
    public void getMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-mail");

        mockMvc.perform(get(RestPaths.MTA_GET_MAIL_PATH)
                        .param("queueID", "4TTBxY4Vsqz3Tq85"))
                .andExpect(request().asyncStarted())
                .andDo(MvcResult::getAsyncResult)
                .andExpect(status().isOk())
                .andReturn();

        verify(postcat).writeMail(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-mail");
    }

    @Test
    @WithMockUser
    public void getMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_MAIL_PATH)
                        .param("queueID", "4TTBxY4Vsqz3Tq85"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postcat, never()).writeMail(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-mail");
    }

    @Test
    @WithMockUser
    public void getMainConfig()
    throws Exception
    {
        String config =
                """
                # forward local system accounts\s
                alias_maps = hash:/etc/aliases
                alias_database = hash:/etc/aliases
                virtual_alias_maps = hash:/etc/postfix/virtual-aliases
                """;

        when(postfixConfigureMain.getMainConfig()).thenReturn(config);

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-main-config");

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_MAIN_CONFIG_PATH))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(config, result.getResponse().getContentAsString());

        verify(postfixConfigureMain).getMainConfig();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-main-config");
    }

    @Test
    @WithMockUser
    public void getMainConfigNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_MAIN_CONFIG_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixConfigureMain, never()).getMainConfig();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-main-config");
    }

    @Test
    @WithMockUser
    public void setMainConfig()
    throws Exception
    {
        String config =
                """
                # forward local system accounts\s
                alias_maps = hash:/etc/aliases
                alias_database = hash:/etc/aliases
                virtual_alias_maps = hash:/etc/postfix/virtual-aliases
                """;

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "set-main-config");

        mockMvc.perform(post(RestPaths.MTA_SET_MAIN_CONFIG_PATH)
                        .content(config.getBytes(StandardCharsets.UTF_8))
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postfixConfigureMain).setMainConfig(any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "set-main-config");
    }

    @Test
    @WithMockUser
    public void setMainConfigNoPermission()
    throws Exception
    {
        String config =
                """
                # forward local system accounts\s
                alias_maps = hash:/etc/aliases
                alias_database = hash:/etc/aliases
                virtual_alias_maps = hash:/etc/postfix/virtual-aliases
                """;

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_SET_MAIN_CONFIG_PATH)
                        .content(config.getBytes(StandardCharsets.UTF_8))
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixConfigureMain, never()).setMainConfig(any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "set-main-config");
    }

    @Test
    @WithMockUser
    public void startPostfix()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "start-postfix");

        mockMvc.perform(post(RestPaths.MTA_START_POSTFIX_PATH)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postfixCtl).startPostfix();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "start-postfix");
    }

    @Test
    @WithMockUser
    public void startPostfixNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MTA_START_POSTFIX_PATH)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixCtl, never()).startPostfix();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "start-postfix");
    }

    @Test
    @WithMockUser
    public void stopPostfix()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "stop-postfix");

        mockMvc.perform(post(RestPaths.MTA_STOP_POSTFIX_PATH)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postfixCtl).stopPostfix();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "stop-postfix");
    }

    @Test
    @WithMockUser
    public void stopPostfixNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MTA_STOP_POSTFIX_PATH)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixCtl, never()).stopPostfix();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "stop-postfix");
    }

    @Test
    @WithMockUser
    public void restartPostfix()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "restart-postfix");

        mockMvc.perform(post(RestPaths.MTA_RESTART_POSTFIX_PATH)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postfixCtl).restartPostfix();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "restart-postfix");
    }

    @Test
    @WithMockUser
    public void restartPostfixNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MTA_RESTART_POSTFIX_PATH)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixCtl, never()).restartPostfix();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "restart-postfix");
    }

    @Test
    @WithMockUser
    public void getPostfixStatus()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-postfix-status");

        String status =
                """
                postfix.service - Postfix Mail Transport Agent
                     Loaded: loaded (/lib/systemd/system/postfix.service; enabled; vendor preset: enabled)
                     Active: inactive (dead) since Tue 2024-02-13 14:34:28 CET; 1s ago
                    Process: 1249726 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
                   Main PID: 1249726 (code=exited, status=0/SUCCESS)
                        CPU: 761us
                """;

        when(postfixCtl.getPostfixStatus()).thenReturn(status);

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_POSTFIX_STATUS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(status, result.getResponse().getContentAsString());

        verify(postfixCtl).getPostfixStatus();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-postfix-status");
    }

    @Test
    @WithMockUser
    public void getPostfixStatusNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_POSTFIX_STATUS_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixCtl, never()).getPostfixStatus();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-postfix-status");
    }

    @Test
    @WithMockUser
    public void isPostfixRunning()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "is-postfix-running");

        when(postfixCtl.isPostfixRunning()).thenReturn(true);

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_IS_POSTFIX_RUNNING_PATH))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("true", result.getResponse().getContentAsString());

        verify(postfixCtl).isPostfixRunning();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "is-postfix-running");
    }

    @Test
    @WithMockUser
    public void isPostfixRunningNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MTA_IS_POSTFIX_RUNNING_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixCtl, never()).isPostfixRunning();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "is-postfix-running");
    }

    @Test
    @WithMockUser
    public void setStandardMainSettings()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "set-standard-settings");

        MTADTO.StandardMainSettings settings = MTADTO.StandardMainSettings.createInstance();

        settings.setMyHostname("hostname.example.com");
        settings.setMyNetworks(List.of("1.2.3.4", "5.6.7.8"));
        settings.setExternalRelayHost("relay.example.com");
        settings.setExternalRelayHostMxLookup(true);
        settings.setExternalRelayHostPort(2525);
        settings.setRelayDomains(List.of("test1.example.com", "test2.example.com"));
        settings.setMessageSizeLimit(1000L);
        settings.setSmtpHeloName("helo.example.com");
        settings.setInternalRelayHost("relay-transport.example.com");
        settings.setInternalRelayHostMxLookup(true);
        settings.setInternalRelayHostPort(2526);
        settings.setRejectUnverifiedRecipient(true);
        settings.setUnverifiedRecipientRejectCode(555);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, settings);

        mockMvc.perform(post(RestPaths.MTA_SET_STANDARD_SETTINGS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postConf).setMainParameterValue("ciphermail_myhostname", "hostname.example.com");
        verify(postConf).setMainParameterValue("ciphermail_mynetworks", "1.2.3.4, 5.6.7.8");
        verify(postConf).setMainParameterValue("ciphermail_external_relay_host", "relay.example.com");
        verify(postConf).setMainParameterValue("ciphermail_external_relay_host_mx_lookup", "lookup");
        verify(postConf).setMainParameterValue("ciphermail_external_relay_host_port", "2525");
        verify(postConf).setMainParameterValue("ciphermail_relay_domains", "test1.example.com, test2.example.com");
        verify(postConf).setMainParameterValue("ciphermail_before_filter_message_size_limit", "1000");
        verify(postConf).setMainParameterValue("ciphermail_calculated_after_filter_message_size_limit", "2000");
        verify(postConf).setMainParameterValue("ciphermail_calculated_queue_minfree", "20000");
        verify(postConf).setMainParameterValue("ciphermail_calculated_after_filter_queue_minfree", "10000");
        verify(postConf).setMainParameterValue("ciphermail_smtp_helo_name", "helo.example.com");
        verify(postConf).setMainParameterValue("ciphermail_internal_relay_host", "relay-transport.example.com");
        verify(postConf).setMainParameterValue("ciphermail_internal_relay_host_mx_lookup", "lookup");
        verify(postConf).setMainParameterValue("ciphermail_internal_relay_host_port", "2526");
        verify(postConf).setMainParameterValue("ciphermail_reject_unverified_recipient", "reject");
        verify(postConf).setMainParameterValue("ciphermail_unverified_recipient_reject_code", "555");

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "set-standard-settings");
    }

    @Test
    @WithMockUser
    public void setStandardMainSettingsNoPermission()
    throws Exception
    {
        MTADTO.StandardMainSettings settings = MTADTO.StandardMainSettings.createInstance();

        settings.setMyHostname("hostname.example.com");
        settings.setMyNetworks(List.of("1.2.3.4", "5.6.7.8"));
        settings.setExternalRelayHost("relay.example.com");
        settings.setExternalRelayHostMxLookup(true);
        settings.setExternalRelayHostPort(2525);
        settings.setRelayDomains(List.of("test1.example.com", "test2.example.com"));
        settings.setMessageSizeLimit(1000L);
        settings.setSmtpHeloName("helo.example.com");
        settings.setInternalRelayHost("relay-transport.example.com");
        settings.setInternalRelayHostMxLookup(true);
        settings.setInternalRelayHostPort(2526);
        settings.setRejectUnverifiedRecipient(true);
        settings.setUnverifiedRecipientRejectCode(555);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, settings);

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_SET_STANDARD_SETTINGS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postConf, never()).setMainParameterValue(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "set-standard-settings");
    }

    @Test
    @WithMockUser
    public void getStandardMainSettings()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-standard-settings");

        when(postConf.getMainParameterValue("ciphermail_myhostname")).thenReturn("hostname.example.com");
        when(postConf.getMainParameterValue("ciphermail_mynetworks")).thenReturn("1.2.3.4, 5.6.7.8");
        when(postConf.getMainParameterValue("ciphermail_external_relay_host")).thenReturn("relay.example.com");
        when(postConf.getMainParameterValue("ciphermail_external_relay_host_mx_lookup")).thenReturn("lookup");
        when(postConf.getMainParameterValue("ciphermail_external_relay_host_port")).thenReturn("2525");
        when(postConf.getMainParameterValue("ciphermail_relay_domains")).thenReturn("test1.example.com, test2.example.com");
        when(postConf.getMainParameterValue("ciphermail_before_filter_message_size_limit")).thenReturn("1000");
        when(postConf.getMainParameterValue("ciphermail_calculated_after_filter_message_size_limit")).thenReturn("2000");
        when(postConf.getMainParameterValue("ciphermail_calculated_queue_minfree")).thenReturn("10000");
        when(postConf.getMainParameterValue("ciphermail_smtp_helo_name")).thenReturn("helo.example.com");
        when(postConf.getMainParameterValue("ciphermail_internal_relay_host")).thenReturn("relay-transport.example.com");
        when(postConf.getMainParameterValue("ciphermail_internal_relay_host_mx_lookup")).thenReturn("lookup");
        when(postConf.getMainParameterValue("ciphermail_internal_relay_host_port")).thenReturn("2526");
        when(postConf.getMainParameterValue("ciphermail_reject_unverified_recipient")).thenReturn("reject");
        when(postConf.getMainParameterValue("ciphermail_unverified_recipient_reject_code")).thenReturn("555");

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_STANDARD_SETTINGS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        MTADTO.StandardMainSettings response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), MTADTO.StandardMainSettings.class);

        assertEquals("hostname.example.com", response.getMyHostname());
        assertEquals(List.of("1.2.3.4", "5.6.7.8"), response.getMyNetworks());
        assertEquals("relay.example.com", response.getExternalRelayHost());
        assertTrue(response.getExternalRelayHostMxLookup());
        assertEquals(2525, (int) response.getExternalRelayHostPort());
        assertEquals(List.of("test1.example.com", "test2.example.com"), response.getRelayDomains());
        assertEquals(1000, (long) response.getMessageSizeLimit());
        assertEquals("helo.example.com", response.getSmtpHeloName());
        assertEquals("relay-transport.example.com", response.getInternalRelayHost());
        assertTrue(response.getInternalRelayHostMxLookup());
        assertEquals(2526, (int) response.getInternalRelayHostPort());
        assertTrue(response.getRejectUnverifiedRecipient());
        assertEquals(555, (int) response.getUnverifiedRecipientRejectCode());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-standard-settings");
    }

    @Test
    @WithMockUser
    public void getStandardMainSettingsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_STANDARD_SETTINGS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postConf, never()).getMainParameterValue(any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "get-standard-settings");
    }

    @Test
    @WithMockUser
    public void listMaps()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-list");

        when(postfixMapManager.getMaps()).thenReturn(List.of(
                new MockPostfixMap("test1", MapType.CIDR),
                new MockPostfixMap("test2", MapType.HASH))
        );

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_LIST_MAPS))
                .andExpect(status().isOk())
                .andReturn();

        List<MTADTO.PostfixMap> results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, results.size());

        assertEquals("test1", results.get(0).name());
        assertEquals(MTADTO.PostfixMapType.CIDR, results.get(0).type());

        assertEquals("test2", results.get(1).name());
        assertEquals(MTADTO.PostfixMapType.HASH, results.get(1).type());

        verify(postfixMapManager).getMaps();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-list");
    }

    @Test
    @WithMockUser
    public void listMapsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MTA_LIST_MAPS))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixMapManager, never()).getMaps();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-list");
    }

    @Test
    @WithMockUser
    public void getMapContentManagePermission()
    throws Exception
    {
        when(permissionChecker.hasPermission(PermissionCategory.MTA, "map-manage")).thenReturn(true);

        when(postfixMapManager.getMap(any(), any())).thenReturn(
                new MockPostfixMap("test", MapType.HASH, "some content"));

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("some content", result.getResponse().getContentAsString());

        verify(postfixMapManager).getMap(any(), any());

        verify(permissionChecker).hasPermission(PermissionCategory.MTA,
                "map-manage");
    }

    @Test
    @WithMockUser
    public void getMapContentNoManagePermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixMapManager, never()).getMap(any(), any());

        verify(permissionChecker).hasPermission(PermissionCategory.MTA,
                "map-manage");

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-get-content", "hash", "test");
    }

    @Test
    @WithMockUser
    public void getMapContent()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-get-content", "hash", "test");

        when(postfixMapManager.getMap(any(), any())).thenReturn(
                new MockPostfixMap("test", MapType.HASH, "some content"));

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("some content", result.getResponse().getContentAsString());

        verify(postfixMapManager).getMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-get-content", "hash", "test");
    }

    @Test
    @WithMockUser
    public void getMapContentNoPermissionName()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-get-content", "hash", "other-file");

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixMapManager, never()).getMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-get-content", "hash", "test");
    }

    @Test
    @WithMockUser
    public void getMapContentNoPermissionType()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-get-content", "pcre", "test");

        MvcResult result = mockMvc.perform(get(RestPaths.MTA_GET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixMapManager, never()).getMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-get-content", "hash", "test");
    }

    @Test
    @WithMockUser
    public void setMapContentManagePermission()
    throws Exception
    {
        when(permissionChecker.hasPermission(PermissionCategory.MTA, "map-manage")).thenReturn(true);

        MockPostfixMap mockPostfixMap = new MockPostfixMap("test", MapType.HASH);

        when(postfixMapManager.getMap(any(), any())).thenReturn(mockPostfixMap);

        mockMvc.perform(post(RestPaths.MTA_SET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test")
                        .content("some content")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("some content", mockPostfixMap.getContent());

        verify(postfixMapManager).getMap(any(), any());

        verify(permissionChecker).hasPermission(PermissionCategory.MTA,
                "map-manage");
    }

    @Test
    @WithMockUser
    public void setMapContentNoManagePermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MTA_SET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test")
                        .content("some content")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixMapManager, never()).getMap(any(), any());

        verify(permissionChecker).hasPermission(PermissionCategory.MTA,
                "map-manage");
    }

    @Test
    @WithMockUser
    public void setMapContent()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-set-content", "hash", "test");

        MockPostfixMap mockPostfixMap = new MockPostfixMap("test", MapType.HASH);

        when(postfixMapManager.getMap(any(), any())).thenReturn(mockPostfixMap);

        mockMvc.perform(post(RestPaths.MTA_SET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test")
                        .content("some content")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("some content", mockPostfixMap.getContent());

        verify(postfixMapManager).getMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-set-content", "hash", "test");
    }

    @Test
    @WithMockUser
    public void setMapContentNoPermissionName()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-set-content", "hash", "other-file");

        MockPostfixMap mockPostfixMap = new MockPostfixMap("test", MapType.HASH);

        when(postfixMapManager.getMap(any(), any())).thenReturn(mockPostfixMap);

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_SET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test")
                        .content("some content")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixMapManager, never()).getMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-set-content", "hash", "test");
    }

    @Test
    @WithMockUser
    public void setMapContentNoPermissionType()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-set-content", "pcre", "test");

        MockPostfixMap mockPostfixMap = new MockPostfixMap("test", MapType.HASH);

        when(postfixMapManager.getMap(any(), any())).thenReturn(mockPostfixMap);

        MvcResult result = mockMvc.perform(post(RestPaths.MTA_SET_MAP_CONTENT)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test")
                        .content("some content")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixMapManager, never()).getMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-set-content", "hash", "test");
    }

    @Test
    @WithMockUser
    public void createMap()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-manage");

        MockPostfixMap mockPostfixMap = new MockPostfixMap("test", MapType.HASH);

        when(postfixMapManager.createMap(any(), any())).thenReturn(mockPostfixMap);

        mockMvc.perform(post(RestPaths.MTA_CREATE_MAP)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("", mockPostfixMap.getContent());

        verify(postfixMapManager).getMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-manage");
    }

    @Test
    @WithMockUser
    public void createMapNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MTA_CREATE_MAP)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixMapManager, never()).getMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-manage");
    }

    @Test
    @WithMockUser
    public void deleteMap()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-manage");

        MockPostfixMap mockPostfixMap = new MockPostfixMap("test", MapType.HASH);

        when(postfixMapManager.getMap(any(), any())).thenReturn(mockPostfixMap);

        mockMvc.perform(post(RestPaths.MTA_DELETE_MAP)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(postfixMapManager).deleteMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-manage");
    }

    @Test
    @WithMockUser
    public void deleteMapNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MTA_DELETE_MAP)
                        .param("type", MTADTO.PostfixMapType.HASH.name())
                        .param("name", "test")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(postfixMapManager, never()).deleteMap(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "map-manage");
    }

    @Test
    @WithMockUser
    public void sendEmailMime()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "send-email-mime");

        String mime =
                """
                To: test@example.com
                Message-ID: <1978504976.0.1713801612036.JavaMail.martijn@workstation>
                Subject: subject
                MIME-Version: 1.0
                Content-Type: text/plain; charset=us-ascii
                Content-Transfer-Encoding: 7bit
                X-Custom-Header: value
                
                body
                """;

        ObjectMapper mapper = new ObjectMapper();

        MockPart senderPart = new MockPart("sender", mapper.writeValueAsBytes("sender@example.com"));
        senderPart.getHeaders().setContentType(MediaType.APPLICATION_JSON);

        MockPart recipientPart = new MockPart("recipients", mapper.writeValueAsBytes(List.of("test1@example.com",
                "test2@example.com")));
        recipientPart.getHeaders().setContentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.MTA_SEND_EMAIL_MIME)
                        .file("mime", mime.getBytes(StandardCharsets.UTF_8))
                        .part(senderPart)
                        .part(recipientPart)
                        .param("mtaPort", MTADTO.MTAPort.MPA.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "send-email-mime");
    }

    @Test
    @WithMockUser
    public void sendEmailMimeInvalidMIME()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "send-email-mime");

        String mime =
                """
                NOT MIME
                """;

        ObjectMapper mapper = new ObjectMapper();

        MockPart senderPart = new MockPart("sender", mapper.writeValueAsBytes("sender@example.com"));
        senderPart.getHeaders().setContentType(MediaType.APPLICATION_JSON);

        MockPart recipientPart = new MockPart("recipients", mapper.writeValueAsBytes(List.of("test1@example.com",
                "test2@example.com")));
        recipientPart.getHeaders().setContentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.MTA_SEND_EMAIL_MIME)
                        .file("mime", mime.getBytes(StandardCharsets.UTF_8))
                        .part(senderPart)
                        .part(recipientPart)
                        .param("mtaPort", MTADTO.MTAPort.MPA.name())
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Content is not valid MIME", result.getResponse().getErrorMessage());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "send-email-mime");
    }

    @Test
    @WithMockUser
    public void sendEmailMimeNoPermission()
    throws Exception
    {
        String mime =
                """
                To: test@example.com
                Message-ID: <1978504976.0.1713801612036.JavaMail.martijn@workstation>
                Subject: subject
                MIME-Version: 1.0
                Content-Type: text/plain; charset=us-ascii
                Content-Transfer-Encoding: 7bit
                X-Custom-Header: value
                
                body
                """;

        ObjectMapper mapper = new ObjectMapper();

        MockPart senderPart = new MockPart("sender", mapper.writeValueAsBytes("sender@example.com"));
        senderPart.getHeaders().setContentType(MediaType.APPLICATION_JSON);

        MockPart recipientPart = new MockPart("recipients", mapper.writeValueAsBytes(List.of("test1@example.com",
                "test2@example.com")));
        recipientPart.getHeaders().setContentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.MTA_SEND_EMAIL_MIME)
                        .file("mime", mime.getBytes(StandardCharsets.UTF_8))
                        .part(senderPart)
                        .part(recipientPart)
                        .param("mtaPort", MTADTO.MTAPort.MPA.name())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "send-email-mime");
    }

    @Test
    @WithMockUser
    public void sendEmail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MTA,
                "send-email");

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.MTA_SEND_EMAIL)
                        .part(new MockPart("subject", null, "some subject".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("from", null, "from@example.com".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("sender", null, "sender@example.com".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("textBody", null, "text body".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("htmlBody", null, "html body".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("recipients", null, mapper.writeValueAsBytes(
                                List.of("r1@example", "r2@example")),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("to", null, mapper.writeValueAsBytes(
                                List.of("to1@example", "to2@example")),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("cc", null, mapper.writeValueAsBytes(
                                List.of("cc1@example", "cc2@example")),
                                MediaType.APPLICATION_JSON))
                        .file("mime", new byte[]{1, 2, 3})
                        .param("mtaPort", MTADTO.MTAPort.MPA.name())
                .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(mailTransport).sendMessage(eq("127.0.0.1"), eq(10027), any(), eq("sender@example.com"),
                eq(List.of(new InternetAddress("r1@example"), new InternetAddress("r2@example"))));

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "send-email");
    }

    @Test
    @WithMockUser
    public void sendEmailNoPermission()
    throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.MTA_SEND_EMAIL)
                        .part(new MockPart("subject", null, "some subject".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("from", null, "from@example.com".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("sender", null, "sender@example.com".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("textBody", null, "text body".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("htmlBody", null, "html body".
                                getBytes(StandardCharsets.UTF_8),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("recipients", null, mapper.writeValueAsBytes(
                                List.of("r1@example", "r2@example")),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("to", null, mapper.writeValueAsBytes(
                                List.of("to1@example", "to2@example")),
                                MediaType.APPLICATION_JSON))
                        .part(new MockPart("cc", null, mapper.writeValueAsBytes(
                                List.of("cc1@example", "cc2@example")),
                                MediaType.APPLICATION_JSON))
                        .file("mime", new byte[]{1, 2, 3})
                        .param("mtaPort", MTADTO.MTAPort.MPA.name())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(mailTransport, never()).sendMessage(any(), anyInt(), any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.MTA,
                "send-email");
    }
}