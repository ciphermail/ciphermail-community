/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.ca.CABuilder;
import com.ciphermail.core.common.security.ca.CACertificateSignatureAlgorithm;
import com.ciphermail.core.common.security.ca.CertificateRequestStore;
import com.ciphermail.core.common.security.ca.SMIMECABuilder;
import com.ciphermail.core.common.security.ca.SelfSignedSMIMEKeyAndCertificateIssuer;
import com.ciphermail.core.common.security.certificate.impl.StandardSerialNumberGenerator;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crl.CRLPathBuilderFactory;
import com.ciphermail.core.common.security.crl.TransactedCRLStoreMaintainer;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import com.ciphermail.rest.core.CADTO;
import com.ciphermail.rest.core.CRLDTO;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CRLDetailsFactory;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.Nonnull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.function.Failable;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CAController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        X509CertificateDetailsFactory.class,
        X509CRLDetailsFactory.class,
        PermissionProviderRegistry.class})
public class CAControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private CABuilder caBuilder;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private CertificateRequestStore certificateRequestStore;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices
    {
        @Bean
        public CABuilder caBuilderService()
        throws Exception
        {
            return new SMIMECABuilder(new StandardSerialNumberGenerator());
        }

        @Bean
        public SelfSignedSMIMEKeyAndCertificateIssuer selfSignedSMIMEKeyAndCertificateIssuerService()
        throws Exception
        {
            return new SelfSignedSMIMEKeyAndCertificateIssuer(new StandardSerialNumberGenerator());
        }

        @Bean
        public TransactedCRLStoreMaintainer cRLStoreMaintainerService(
                X509CRLStoreExt crlStore,
                CRLPathBuilderFactory pathBuilderFactory,
                TransactionOperations transactionOperations)
        {
            return new TransactedCRLStoreMaintainer(crlStore, pathBuilderFactory, transactionOperations);
        }
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            // Clean stores
            keyAndCertStore.removeAllEntries();
            rootStore.removeAllEntries();
            certificateRequestStore.getAllRequests(null, null).forEach(
                    r -> certificateRequestStore.deleteRequest(r.getID()));
        }));

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    private CADTO.X500PrincipalRequest createX500PrincipalRequest(String email, @Nonnull String commonName)
    {
        return new CADTO.X500PrincipalRequest(email, null, null, null,
                null, null, commonName, null, null);
    }

    @Test
    @WithMockUser
    public void testCreateCA()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CA, "create-ca");

        CADTO.NewCASubjects subjects = new CADTO.NewCASubjects(
                createX500PrincipalRequest("root@example.com", "root"),
                createX500PrincipalRequest("intermediate@example.com", "intermediate"));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, subjects);

        MvcResult result = mockMvc.perform(post(RestPaths.CA_CREATE_CA_PATH)
                        .param("rootDaysValid", "3650")
                        .param("intermediateDaysValid", "365")
                        .param("rootKeyLength", "3072")
                        .param("intermediateKeyLength", "2048")
                        .param("signatureAlgorithm",
                                CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION.name())
                        .param("crlDistributionPoint", "http://www.example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CADTO.NewCAResponse response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CADTO.NewCAResponse.class);

        assertNotNull(response);
        assertNotNull(response.intermediateCertificate());
        assertNotNull(response.rootCertificate());

        verify(permissionChecker, atLeastOnce()).checkPermission(PermissionCategory.CA,
                "create-ca");
    }

    @Test
    @WithMockUser
    public void testCreateCANoPermission()
    throws Exception
    {
        CADTO.NewCASubjects subjects = new CADTO.NewCASubjects(
                createX500PrincipalRequest("root@example.com", "root"),
                createX500PrincipalRequest("intermediate@example.com", "intermediate"));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, subjects);

        MvcResult result = mockMvc.perform(post(RestPaths.CA_CREATE_CA_PATH)
                        .param("rootDaysValid", "3650")
                        .param("intermediateDaysValid", "365")
                        .param("rootKeyLength", "4096")
                        .param("intermediateKeyLength", "3072")
                        .param("signatureAlgorithm",
                                CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION.name())
                        .param("crlDistributionPoint", "http://www.example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "create-ca");
    }

    @Test
    @WithMockUser
    public void testGetAvailableCAs()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CA, "get-available-cas");

        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_AVAILABLE_CAS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(0, certificateDetails.size());

        testCreateCA();

        result = mockMvc.perform(get(RestPaths.CA_GET_AVAILABLE_CAS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, certificateDetails.size());

        testCreateCA();

        result = mockMvc.perform(get(RestPaths.CA_GET_AVAILABLE_CAS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        certificateDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, certificateDetails.size());

        verify(permissionChecker, times(3)).checkPermission(PermissionCategory.CA,
                "get-available-cas");
    }

    @Test
    @WithMockUser
    public void testGetAvailableCAsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_AVAILABLE_CAS_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "get-available-cas");
    }

    @Test
    @WithMockUser
    public void testGetCertificateRequestHandlers()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "get-certificate-request-handlers");

        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_CERTIFICATE_REQUEST_HANDLERS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CADTO.CertificateRequestHandlerDetails> handlers = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, handlers.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "get-certificate-request-handlers");
    }

    @Test
    @WithMockUser
    public void testGetCertificateRequestHandlersNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_CERTIFICATE_REQUEST_HANDLERS_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "get-certificate-request-handlers");
    }

    private CADTO.NewRequest createNewCertificateRequest()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CA, "request-new-certificate");

        CADTO.X500PrincipalRequest subject = createX500PrincipalRequest("test@example.com", "test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, subject);

        MvcResult result = mockMvc.perform(post(RestPaths.CA_REQUEST_NEW_CERTIFICATE_PATH)
                        .param("email", "test@example.com")
                        .param("daysValid", "10")
                        .param("keyLength", "2048")
                        .param("signatureAlgorithm",
                                CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION.name())
                        .param("crlDistributionPoint", "http://www.example.com")
                        .param("certificateRequestHandler", "CSR")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker, atLeastOnce()).checkPermission(PermissionCategory.CA,
                "request-new-certificate");

        return JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CADTO.NewRequest.class);
    }

    @Test
    @WithMockUser
    public void testRequestNewCertificate()
    throws Exception
    {
        CADTO.NewRequest request = createNewCertificateRequest();

        assertNotNull(request.request());
        assertNotNull(request.request().id());
        assertEquals("CSR", request.request().certificateRequestHandler());
        assertNull(request.keyAndCertificate());
    }

    @Test
    @WithMockUser
    public void testRequestNewCertificateNoPermission()
    throws Exception
    {
        CADTO.X500PrincipalRequest subject = createX500PrincipalRequest("root@example.com", "root");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, subject);

        MvcResult result = mockMvc.perform(post(RestPaths.CA_REQUEST_NEW_CERTIFICATE_PATH)
                        .param("email", "test@example.com")
                        .param("daysValid", "10")
                        .param("keyLength", "2048")
                        .param("signatureAlgorithm",
                                CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION.name())
                        .param("crlDistributionPoint", "http://www.example.com")
                        .param("certificateRequestHandler", "CSR")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "request-new-certificate");
    }

    @Test
    @WithMockUser
    public void testGetPendingRequests()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "get-pending-requests");

        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CADTO.PendingRequest> pendingRequests = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(0, pendingRequests.size());

        createNewCertificateRequest();
        createNewCertificateRequest();

        result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        pendingRequests = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, pendingRequests.size());

        verify(permissionChecker, times(2)).checkPermission(PermissionCategory.CA,
                "get-pending-requests");
    }

    @Test
    @WithMockUser
    public void testGetPendingRequestsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "get-pending-requests");
    }

    @Test
    @WithMockUser
    public void testGetPendingRequestsCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "get-pending-requests-count");

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));

        createNewCertificateRequest();
        createNewCertificateRequest();

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("2"));

        verify(permissionChecker, times(2)).checkPermission(PermissionCategory.CA,
                "get-pending-requests-count");
    }

    @Test
    @WithMockUser
    public void testGetPendingRequestsCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "get-pending-requests-count");
    }

    @Test
    @WithMockUser
    public void testGetPendingRequestsByEmail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "get-pending-requests-by-email");

        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_PATH)
                        .param("email", "test@example.com")
                        .param("match", Match.EXACT.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CADTO.PendingRequest> pendingRequests = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(0, pendingRequests.size());

        createNewCertificateRequest();
        createNewCertificateRequest();

        result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_PATH)
                        .param("email", "test@example.com")
                        .param("match", Match.EXACT.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        pendingRequests = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, pendingRequests.size());

        result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_PATH)
                        .param("email", "%test%")
                        .param("match", Match.LIKE.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        pendingRequests = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, pendingRequests.size());

        verify(permissionChecker, times(3)).checkPermission(PermissionCategory.CA,
                "get-pending-requests-by-email");
    }

    @Test
    @WithMockUser
    public void testGetPendingRequestsByEmailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_PATH)
                        .param("email", "test@example.com")
                        .param("match", Match.EXACT.name())
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "get-pending-requests-by-email");
    }

    @Test
    @WithMockUser
    public void getPendingRequestsByEmailCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "get-pending-requests-by-email-count");

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_COUNT_PATH)
                        .param("email", "test@example.com")
                        .param("match", Match.EXACT.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));

        createNewCertificateRequest();
        createNewCertificateRequest();

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_COUNT_PATH)
                        .param("email", "test@example.com")
                        .param("match", Match.EXACT.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("2"));

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_COUNT_PATH)
                        .param("email", "%test%")
                        .param("match", Match.LIKE.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("2"));

        verify(permissionChecker, times(3)).checkPermission(PermissionCategory.CA,
                "get-pending-requests-by-email-count");
    }

    @Test
    @WithMockUser
    public void testGetPendingRequestsByEmailCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_COUNT_PATH)
                        .param("email", "test@example.com")
                        .param("match", Match.EXACT.name())
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "get-pending-requests-by-email-count");
    }

    @Test
    @WithMockUser
    public void getPendingRequest()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "get-pending-request");

        CADTO.NewRequest newRequest = createNewCertificateRequest();

        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUEST_PATH)
                        .param("id", newRequest.request().id().toString())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        CADTO.PendingRequest pendingRequest = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CADTO.PendingRequest.class);

        assertNotNull(pendingRequest);
        assertEquals(newRequest.request().id(), pendingRequest.id());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "get-pending-request");
    }

    @Test
    @WithMockUser
    public void testGetPendingRequestNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUEST_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "get-pending-request");
    }

    @Test
    @WithMockUser
    public void testDeletePendingRequest()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "get-pending-requests-count");

        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "delete-pending-request");

        CADTO.NewRequest newRequest = createNewCertificateRequest();

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("1"));

        mockMvc.perform(post(RestPaths.CA_DELETE_PENDING_REQUEST_PATH)
                        .param("id", newRequest.request().id().toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "delete-pending-request");
    }

    @Test
    @WithMockUser
    public void testDeletePendingRequestNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CA_DELETE_PENDING_REQUEST_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "delete-pending-request");
    }

    @Test
    @WithMockUser
    public void testDeletePendingRequests()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "get-pending-requests-count");

        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "delete-pending-requests");

        CADTO.NewRequest newRequest1 = createNewCertificateRequest();
        CADTO.NewRequest newRequest2 = createNewCertificateRequest();

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("2"));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(newRequest1.request().id(), newRequest2.request().id()));

        mockMvc.perform(post(RestPaths.CA_DELETE_PENDING_REQUESTS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "delete-pending-requests");
    }

    @Test
    @WithMockUser
    public void testDeletePendingRequestsNoPermission()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "get-pending-requests-count");

        CADTO.NewRequest newRequest1 = createNewCertificateRequest();
        CADTO.NewRequest newRequest2 = createNewCertificateRequest();

        mockMvc.perform(get(RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("2"));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(newRequest1.request().id(), newRequest2.request().id()));

        MvcResult result = mockMvc.perform(post(RestPaths.CA_DELETE_PENDING_REQUESTS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "delete-pending-requests");
    }

    @Test
    @WithMockUser
    public void testRescheduleRequest()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "reschedule-request");

        CADTO.NewRequest newRequest = createNewCertificateRequest();

        MvcResult result = mockMvc.perform(post(RestPaths.CA_RESCHEDULE_REQUEST_PATH)
                        .param("id", newRequest.request().id().toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CADTO.PendingRequest rescheduledRequest = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CADTO.PendingRequest.class);

        assertTrue(rescheduledRequest.nextUpdate() < newRequest.request().nextUpdate());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "reschedule-request");
    }

    @Test
    @WithMockUser
    public void testRescheduleRequestNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CA_RESCHEDULE_REQUEST_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "reschedule-request");
    }

    @Test
    @WithMockUser
    public void testRescheduleRequests()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "reschedule-requests");

        CADTO.NewRequest newRequest1 = createNewCertificateRequest();
        CADTO.NewRequest newRequest2 = createNewCertificateRequest();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(newRequest1.request().id(), newRequest2.request().id()));

        mockMvc.perform(post(RestPaths.CA_RESCHEDULE_REQUESTS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "reschedule-requests");
    }

    @Test
    @WithMockUser
    public void testRescheduleRequestsNoPermission()
    throws Exception
    {
        CADTO.NewRequest newRequest1 = createNewCertificateRequest();
        CADTO.NewRequest newRequest2 = createNewCertificateRequest();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(newRequest1.request().id(), newRequest2.request().id()));

        MvcResult result = mockMvc.perform(post(RestPaths.CA_RESCHEDULE_REQUESTS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "reschedule-requests");
    }

    @Test
    @WithMockUser
    public void testFinalizePendingRequest()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(
                PermissionCategory.CA, "finalize-pending-request");

        createNewCertificateRequest();

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CA_FINALIZE_PENDING_REQUEST_PATH)
                        .file("certificates", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/testcertificate.cer")))
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                ",\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"," +
                "\"error\":\"Bad Request\"," +
                "\"message\":\"No pending request found that matches the uploaded certificate\"," +
                "\"key\":\"backend.validation.no-matching-pending-request\",\"params\":null,\"path\":\"\"}"));

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "finalize-pending-request");
    }

    @Test
    @WithMockUser
    public void testFinalizePendingRequestNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CA_FINALIZE_PENDING_REQUEST_PATH)
                        .file("certificates", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "certificates/testcertificate.cer")))
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "finalize-pending-request");
    }

    @Test
    @WithMockUser
    public void testCreateSelfSignedCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CA, "create-self-signed-certificate");

        CADTO.X500PrincipalRequest subject = createX500PrincipalRequest("test@example.com", "test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, subject);

        MvcResult result = mockMvc.perform(post(RestPaths.CA_CREATE_SELF_SIGNED_CERTIFICATE_PATH)
                        .param("email", "test@example.com")
                        .param("daysValid", "10")
                        .param("keyLength", "2048")
                        .param("signatureAlgorithm",
                                CACertificateSignatureAlgorithm.SHA384_WITH_RSA_ENCRYPTION.name())
                        .param("whitelist", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.X509CertificateDetails details = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CertificateDTO.X509CertificateDetails.class);

        assertNotNull(details);
        assertEquals(details.subjectFriendly(), details.issuerFriendly());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "create-self-signed-certificate");
    }

    @Test
    @WithMockUser
    public void testCreateSelfSignedCertificateNoPermission()
    throws Exception
    {
        CADTO.X500PrincipalRequest subject = createX500PrincipalRequest("test@example.com", "test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, subject);

        MvcResult result = mockMvc.perform(post(RestPaths.CA_CREATE_SELF_SIGNED_CERTIFICATE_PATH)
                        .param("email", "test@example.com")
                        .param("daysValid", "10")
                        .param("keyLength", "2048")
                        .param("signatureAlgorithm",
                                CACertificateSignatureAlgorithm.SHA384_WITH_RSA_ENCRYPTION.name())
                        .param("whitelist", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "create-self-signed-certificate");
    }

    @Test
    @WithMockUser
    public void testCreateCRL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CA, "create-ca");
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CA, "create-crl");

        CADTO.NewCASubjects subjects = new CADTO.NewCASubjects(
                createX500PrincipalRequest("root@example.com", "root"),
                createX500PrincipalRequest("intermediate@example.com", "intermediate"));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, subjects);

        MvcResult result = mockMvc.perform(post(RestPaths.CA_CREATE_CA_PATH)
                        .param("rootDaysValid", "3650")
                        .param("intermediateDaysValid", "365")
                        .param("rootKeyLength", "3072")
                        .param("intermediateKeyLength", "2048")
                        .param("signatureAlgorithm",
                                CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION.name())
                        .param("crlDistributionPoint", "http://www.example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CADTO.NewCAResponse newCAResponse = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CADTO.NewCAResponse.class);

        jsonWriter = new StringWriter();
        mapper.writeValue(jsonWriter, List.of("123", "abc"));

        result = mockMvc.perform(post(RestPaths.CA_CREATE_CRL_PATH)
                        .param("issuerThumbprint", newCAResponse.intermediateCertificate().thumbprint())
                        .param("nextUpdate", Long.toString(DateUtils.addWeeks(new Date(), 1).getTime()))
                        .param("updateExistingCRL", "true")
                        .param("signatureAlgorithm",
                                CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CRLDTO.X509CRLDetails details = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.X509CRLDetails.class);

        assertNotNull(details);
        assertEquals(newCAResponse.intermediateCertificate().subjectFriendly(), details.issuerFriendly());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "create-crl");
    }

    @Test
    @WithMockUser
    public void testCreateCRLNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("123", "abc"));

        MvcResult result = mockMvc.perform(post(RestPaths.CA_CREATE_CRL_PATH)
                        .param("issuerThumbprint", "invalid thumbprint")
                        .param("nextUpdate", Long.toString(DateUtils.addWeeks(new Date(), 1).getTime()))
                        .param("updateExistingCRL", "true")
                        .param("signatureAlgorithm",
                                CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CA,
                "create-crl");
    }
}