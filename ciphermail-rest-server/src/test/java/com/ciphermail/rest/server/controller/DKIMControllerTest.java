/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.dkim.DKIMPrivateKeyManager;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.rest.core.DKIMDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.function.Failable;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@WebMvcTest(value = DKIMController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class DKIMControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private DKIMPrivateKeyManager dKIMPrivateKeyManager;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices {
        // empty on purpose
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            // delete existing DKIM keys
            dKIMPrivateKeyManager.getKeyIds().forEach(id -> Failable.run(() -> dKIMPrivateKeyManager.deleteKey(id)));
        }));

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    private static final String RSA_KEY_PAIR_2048 =
            """
                -----BEGIN RSA PRIVATE KEY-----
                MIIEowIBAAKCAQEAjDL09cO7I/fa83FWHAcb9AprRlF+eGuoDD5jgauXq6ATbJPy
                tz0PO01uUWIoFHbfz0XWTgfCL6smaWegS/ATe2/nOx3cbxyn7RlZJFEm/hrDxyvl
                cTk/+sXWvCxDuiyQ3fBOav4kckqozJxB3Dd+k8xDL+rph2WElxwq649dTRVDY9+e
                MUp061H66ZMrsM4VzAJO2LiBQsDXEouts++JXQJbejVSMDbTKMlYT0XnP7t8fNrx
                myQcrgR1cd+py06Nxx6gqitCpDyHNjsHG6qUhZHUkK3tGgG8Qky0sAqmyMvOBQs8
                7/wUDDftaxCKTSy0ymKEZgzZStyj39YXCxupXQIDAQABAoIBAHhhCJQ9i+JGX2nI
                VveZ4xaYG6Q1NTKQvapFp4sfmwtyVztTjYRomTuCMMcQUPnBWErQ3EIDx2jGvCSl
                Ja4OZawHWbQY371IDag3q6LTS3fD43aQ6mFdluHsHYVVPJIO0hS+0ZZswT6hfG6z
                fJlpzPV63fW27u2i7UYSmYfJgYM9CVC0cYB5Yy3fgyoyEi6pa3EgZJrK9mXFCw3P
                5+IBh0W+OSz9f47k4kriAVEEwsao+EyNSz7YoPwLq+5/eXN4XAalDD5TgPgMZAz9
                unclOPmxZbPjQ/hbEuMTW+G58SbSyQWKxTLPIKqmi/0W217QhvgQ9IXAhwS3EPcw
                DipPegECgYEAwD0MOhOUxIIPDjke/QnYOqCZ0gudpCHtdfR8MYZeqiMbRQZsHJjX
                9YzuFV9LUlgoH5uq1qJub9mQrGayl3TQOl8KOeNx0vRoHR0xdkZeJ06E/d/It/FC
                xxzLykjhgrbfjGHkCVi7zz5aqxEz6i1a9A6uvKuHFB20hsEGwHQtZVUCgYEAurM/
                G/WgRb3nNI+aHzevfuXEijdhGZzhIf8AnDJTwnD6rEZDJaq2Wl7pi6Pi6Got05Lp
                +UmiPbttj9uXvp3kO66I7Iwz8aLP/faG8qPNoIlT4j2OaDRS4xppQ4v3McA8QKc2
                SveauuWPkBLOd0OBOLlDoJw4HmRpmSlJdFAws+kCgYAJosW7H4Ike3RvvRM9kcHB
                5ozvR1/Ge2DgubmD7f1Ov+W9Bv5iTJL+nurMLXOkFAEm3HGqwoeQmbIzwg4Po25Q
                jrT/g9QPw+p4Ex+IkyxjAf+OjioMdk5nlpzsbczH9YytB7cbdNQtJi37Ryh4A3/8
                ncPKdzqVGowS+Rfyi6A+5QKBgQCwMaNYZplH2C1PfEpuNaZIstyluBfKDTSmWbqg
                rhpAAVGArZqG/LG1Xac4YoUs46+14QsweR5E9hIy1oFBok6XSGLuIm6PFyEFQtge
                Oxo+6/sfIwq3KtIig3VeCsRRzmLOfT+OxlffE/BFfoodIbbc6nJ5K5UGkaraoY0x
                2M/tqQKBgFtfp0aGkOj/zFQ0aKrO/+WR9lOXfgbv/XhkzJHUOCqxcYBltGx835hx
                wK1r47Qg0f8ykDsEmgQMjlhjZE3JyqaG70WobmN0YqtS4MCaKnPgdav0Jqc6bAzA
                qecD6CBCde5o2U6KEMbPtsblABNssbx0aKA4IuYR4yDwb2V9lujP
                -----END RSA PRIVATE KEY-----""";

    @Test
    @WithMockUser
    public void testGenerateKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "generate-key");

        mockMvc.perform(post(RestPaths.DKIM_GENERATE_KEY_PATH)
                        .param("keyId", "id")
                        .param("keyLength", "2048")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "generate-key");
    }

    @Test
    @WithMockUser
    public void testGenerateKeyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DKIM_GENERATE_KEY_PATH)
                        .param("keyId", "id")
                        .param("keyLength", "2048")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "generate-key");
    }

    @Test
    @WithMockUser
    public void testGetKeyIds()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "get-key-ids");

        testGenerateKey();

        MvcResult result = mockMvc.perform(get(RestPaths.DKIM_GET_KEY_IDS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "20"))
                .andExpect(status().isOk())
                .andReturn();

        List<String> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "get-key-ids");
    }

    @Test
    @WithMockUser
    public void testGetKeyIdsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DKIM_GET_KEY_IDS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "20"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "get-key-ids");
    }

    @Test
    @WithMockUser
    public void testSetKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "set-key");

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.DKIM_SET_KEY_PATH)
                        .file("pemEncodedKeyPair", RSA_KEY_PAIR_2048.getBytes(StandardCharsets.UTF_8))
                        .param("keyId", "id")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "set-key");
    }

    @Test
    @WithMockUser
    public void testSetKeyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.DKIM_SET_KEY_PATH)
                        .file("pemEncodedKeyPair", RSA_KEY_PAIR_2048.getBytes(StandardCharsets.UTF_8))
                        .param("keyId", "id")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "set-key");
    }

    @Test
    @WithMockUser
    public void testSetKeyInvalidKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "set-key");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.DKIM_SET_KEY_PATH)
                        .file("pemEncodedKeyPair", "test".getBytes(StandardCharsets.UTF_8))
                        .param("keyId", "id")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Request does not contain a PEM encoded KeyPair", result.getResponse().getErrorMessage());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "set-key");
    }

    @Test
    @WithMockUser
    public void testGetPublicKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "get-public-key");

        testGenerateKey();

        MvcResult result = mockMvc.perform(get(RestPaths.DKIM_GET_PUBLIC_KEY_PATH)
                        .param("keyId", "id"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] publicKey = Base64Utils.decode(result.getResponse().getContentAsString());

        assertNotNull(publicKey);

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "get-public-key");
    }

    @Test
    @WithMockUser
    public void testGetPublicKeyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DKIM_GET_PUBLIC_KEY_PATH)
                        .param("keyId", "id"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "get-public-key");
    }

    @Test
    @WithMockUser
    public void testGetKeyPair()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "get-key-pair");

        testGenerateKey();

        MvcResult result = mockMvc.perform(get(RestPaths.DKIM_GET_KEY_PAIR_PATH)
                        .param("keyId", "id"))
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(new PEMParser(new StringReader(result.getResponse().getContentAsString())).readObject()
                instanceof PEMKeyPair);

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "get-key-pair");
    }

    @Test
    @WithMockUser
    public void testGetKeyPairNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DKIM_GET_KEY_PAIR_PATH)
                        .param("keyId", "id"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "get-key-pair");
    }

    @Test
    @WithMockUser
    public void testIsKeyAvailable()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "is-key-available");

        testGenerateKey();

        MvcResult result = mockMvc.perform(get(RestPaths.DKIM_IS_KEY_AVAILABLE_PATH)
                        .param("keyId", "id"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("true", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "is-key-available");
    }

    @Test
    @WithMockUser
    public void testIsKeyAvailableNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DKIM_IS_KEY_AVAILABLE_PATH)
                        .param("keyId", "id"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "is-key-available");
    }

    @Test
    @WithMockUser
    public void testDeleteKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "delete-key");

        testGenerateKey();

        MvcResult result = mockMvc.perform(post(RestPaths.DKIM_DELETE_KEY_PATH)
                        .param("keyId", "id")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("true", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "delete-key");
    }

    @Test
    @WithMockUser
    public void testDeleteKeyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DKIM_DELETE_KEY_PATH)
                        .param("keyId", "id")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "delete-key");
    }

    @Test
    @WithMockUser
    public void testParseSignatureTemplate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "parse-signature-template");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.DKIM_PARSE_SIGNATURE_TEMPLATE_PATH)
                        .file("signatureTemplate", ("v=1; c=relaxed/relaxed; s=ciphermail; d=${domain}; " +
                                                    "h=From:Subject:To:Date; a=rsa-sha256; t=; bh=; b=;")
                                                            .getBytes(StandardCharsets.UTF_8))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        DKIMDTO.SignatureTemplate response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("From,Subject,To,Date", StringUtils.join(response.headers(), ","));
        assertEquals("@example.com", response.identity());
        assertEquals("sha-256", response.hashAlgo());
        assertEquals("ciphermail", response.selector());
        assertEquals("rsa", response.hashKeyType());
        assertEquals("sha256", response.hashMethod());
        assertEquals("example.com", response.dToken());
        assertEquals("relaxed", response.headerCanonicalisationMethod());
        assertEquals("relaxed", response.bodyCanonicalisationMethod());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "parse-signature-template");
    }

    @Test
    @WithMockUser
    public void testParseSignatureTemplateNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.DKIM_PARSE_SIGNATURE_TEMPLATE_PATH)
                        .file("signatureTemplate", ("v=1; c=relaxed/relaxed; s=ciphermail; d=${domain}; " +
                                                    "h=From:Subject:To:Date; a=rsa-sha256; t=; bh=; b=;")
                                .getBytes(StandardCharsets.UTF_8))
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DKIM,
                "parse-signature-template");
    }
}