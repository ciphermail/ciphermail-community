/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.HostResources;
import com.ciphermail.core.common.util.ByteArrayResource;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = HostResourcesController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class HostResourcesControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @MockBean
    private HostResources hostResources;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    @Test
    @WithMockUser
    public void getPublicResource()
    throws Exception
    {
        when(hostResources.getResource("some-id", null)).thenReturn(
                new ByteArrayResource("test".getBytes(), "text/plain", null));

        MvcResult result = mockMvc.perform(get(RestPaths.HOST_RESOURCES_GET_PUBLIC_RESOURCE)
                        .param("id", "some-id"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("test", result.getResponse().getContentAsString());
        assertEquals("text/plain", result.getResponse().getContentType());

        verify(permissionChecker, never()).checkPermission(any(), any());
    }

    @Test
    @WithMockUser
    public void getPublicResourceNoMatch()
    throws Exception
    {
        when(hostResources.getResource("some-id", null)).thenReturn(null);

        MvcResult result = mockMvc.perform(get(RestPaths.HOST_RESOURCES_GET_PUBLIC_RESOURCE)
                        .param("id", "some-id"))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("Resource with id some-id and hostname null not found",
                result.getResponse().getErrorMessage());

        verify(permissionChecker, never()).checkPermission(any(), any());
    }

    @Test
    @WithMockUser
    public void getEncodedPublicResource()
    throws Exception
    {
        when(hostResources.getEncodedResource("some-id", null)).thenReturn(
                "{\"test\":123}");

        MvcResult result = mockMvc.perform(get(RestPaths.HOST_RESOURCES_GET_ENCODED_PUBLIC_RESOURCE)
                        .param("id", "some-id"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("{\"test\":123}", result.getResponse().getContentAsString());
        assertEquals("text/plain;charset=UTF-8", result.getResponse().getContentType());

        verify(permissionChecker, never()).checkPermission(any(), any());
    }

    @Test
    @WithMockUser
    public void getEncodedPublicResourceNoMatch()
    throws Exception
    {
        when(hostResources.getEncodedResource("some-id", null)).thenReturn(null);

        MvcResult result = mockMvc.perform(get(RestPaths.HOST_RESOURCES_GET_PUBLIC_RESOURCE)
                        .param("id", "some-id"))
                .andExpect(status().isNotFound())
                .andReturn();

        assertEquals("Resource with id some-id and hostname null not found",
                result.getResponse().getErrorMessage());

        verify(permissionChecker, never()).checkPermission(any(), any());
    }
}
