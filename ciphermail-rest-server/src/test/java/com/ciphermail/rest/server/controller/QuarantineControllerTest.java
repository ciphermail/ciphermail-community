/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailEnqueuer;
import com.ciphermail.core.app.mail.MailSerializer;
import com.ciphermail.core.common.dlp.PolicyViolation;
import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.repository.MailRepository;
import com.ciphermail.core.common.mail.repository.MailRepositoryEventListener;
import com.ciphermail.core.common.mail.repository.MailRepositoryItem;
import com.ciphermail.core.common.mail.repository.hibernate.MailRepositoryEntity;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.QuarantineDTO;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.base.test.FakeMail;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@WebMvcTest(value = QuarantineController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class QuarantineControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean
    private MailRepository quarantineMailRepository;

    @MockBean
    private MailEnqueuer mailEnqueuer;

    @MockBean
    private MailRepositoryEventListener mailRepositoryEventListener;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    private static MailRepositoryItem createMailRepositoryItem()
    throws Exception
    {
        MimeMessage message = new MimeMessage((Session) null);

        message.setText("test email");
        message.saveChanges();

        MailRepositoryEntity mailRepositoryEntity = new MailRepositoryEntity("quarantine", message);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();

        CoreApplicationMailAttributes.setPolicyViolations(mail, List.of(
                new PolicyViolation("policy1", "rule1", "match1",
                        PolicyViolationAction.BLOCK, false),
                new PolicyViolation("policy2", "rule2", "match2",
                        PolicyViolationAction.QUARANTINE, false)));

        mailRepositoryEntity.setAdditionalData(new MailSerializer().serialize(mail));

        return mailRepositoryEntity;
    }

    @Test
    @WithMockUser
    public void getQuarantinedMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail");

        doReturn(List.of(createMailRepositoryItem(), createMailRepositoryItem())).when(quarantineMailRepository).getItems(any(), any());

        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "100"))
                .andExpect(status().isOk())
                .andReturn();

        List<QuarantineDTO.QuarantinedMail> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail");
    }

    @Test
    @WithMockUser
    public void getQuarantinedMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "100"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(quarantineMailRepository, never()).getItems(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail");
    }

    @Test
    @WithMockUser
    public void getQuarantinedMailCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail-count");

        doReturn(10L).when(quarantineMailRepository).getItemCount();

        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_COUNT_PATH))
                .andExpect(status().isOk())
                .andReturn();

        Long response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(10L, (long) response);

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail-count");
    }

    @Test
    @WithMockUser
    public void getQuarantinedMailCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_COUNT_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(quarantineMailRepository, never()).getItemCount();

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail-count");
    }

    @Test
    @WithMockUser
    public void getQuarantinedMailById()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail-by-id");

        doReturn(createMailRepositoryItem()).when(quarantineMailRepository).getItem(any());

        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_BY_ID_PATH)
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().isOk())
                .andReturn();

        QuarantineDTO.QuarantinedMail response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertNotNull(response);

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail-by-id");
    }

    @Test
    @WithMockUser
    public void getQuarantinedMailByIdNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_BY_ID_PATH)
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(quarantineMailRepository, never()).getItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail-by-id");
    }

    @Test
    @WithMockUser
    public void deleteQuarantinedMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "delete-quarantined-mail");

        doReturn(createMailRepositoryItem()).when(quarantineMailRepository).getItem(any());

        MvcResult result = mockMvc.perform(post(RestPaths.QUARANTINE_DELETE_QUARANTINED_MAIL_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        QuarantineDTO.QuarantinedMail response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertNotNull(response);

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "delete-quarantined-mail");
    }

    @Test
    @WithMockUser
    public void deleteQuarantinedMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.QUARANTINE_DELETE_QUARANTINED_MAIL_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(quarantineMailRepository, never()).getItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "delete-quarantined-mail");
    }

    @Test
    @WithMockUser
    public void deleteQuarantinedMails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "delete-quarantined-mails");

        doReturn(createMailRepositoryItem()).when(quarantineMailRepository).getItem(any());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(UUID.randomUUID(), UUID.randomUUID()));

        mockMvc.perform(post(RestPaths.QUARANTINE_DELETE_QUARANTINED_MAILS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(quarantineMailRepository, times(2)).deleteItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "delete-quarantined-mails");
    }

    @Test
    @WithMockUser
    public void deleteQuarantinedMailsNoPermission()
    throws Exception
    {
        doReturn(createMailRepositoryItem()).when(quarantineMailRepository).getItem(any());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(UUID.randomUUID(), UUID.randomUUID()));

        MvcResult result = mockMvc.perform(post(RestPaths.QUARANTINE_DELETE_QUARANTINED_MAILS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(quarantineMailRepository, never()).deleteItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "delete-quarantined-mails");
    }

    @Test
    @WithMockUser
    public void getQuarantinedMailContent()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail-content");

        doReturn(createMailRepositoryItem()).when(quarantineMailRepository).getItem(any());

        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_CONTENT_PATH)
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(request().asyncStarted())
                .andDo(MvcResult::getAsyncResult)
                .andExpect(status().isOk())
                .andReturn();

        String mime = result.getResponse().getContentAsString();

        MimeMessage message = MailUtils.loadMessage(IOUtils.toInputStream(mime, StandardCharsets.UTF_8));
        assertEquals("test email", message.getContent());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail-content");
    }

    @Test
    @WithMockUser
    public void getQuarantinedMailContentNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_CONTENT_PATH)
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(quarantineMailRepository, never()).getItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "get-quarantined-mail-content");
    }

    @Test
    @WithMockUser
    public void searchQuarantinedMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "search-quarantined-mail");

        doReturn(List.of(createMailRepositoryItem(), createMailRepositoryItem())).when(quarantineMailRepository)
                .searchItems(any(), any(), any(), any());

        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_SEARCH_QUARANTINED_MAIL_PATH)
                        .param("searchField", QuarantineDTO.SearchField.FROM.name())
                        .param("searchKey", "test")
                        .param("firstResult", "0")
                        .param("maxResults", "100"))
                .andExpect(status().isOk())
                .andReturn();

        List<QuarantineDTO.QuarantinedMail> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "search-quarantined-mail");
    }

    @Test
    @WithMockUser
    public void searchQuarantinedMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_SEARCH_QUARANTINED_MAIL_PATH)
                        .param("searchField", QuarantineDTO.SearchField.FROM.name())
                        .param("searchKey", "test")
                        .param("firstResult", "0")
                        .param("maxResults", "100"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(quarantineMailRepository, never()).getItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "search-quarantined-mail");
    }

    @Test
    @WithMockUser
    public void searchQuarantinedMailCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "search-quarantined-mail-count");

        doReturn(66L).when(quarantineMailRepository)
                .getSearchCount(any(), any());

        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_SEARCH_QUARANTINED_MAIL_COUNT_PATH)
                        .param("searchField", QuarantineDTO.SearchField.FROM.name())
                        .param("searchKey", "test"))
                .andExpect(status().isOk())
                .andReturn();

        Long response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(66L, (long) response);

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "search-quarantined-mail-count");
    }

    @Test
    @WithMockUser
    public void searchQuarantinedMailCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.QUARANTINE_SEARCH_QUARANTINED_MAIL_COUNT_PATH)
                        .param("searchField", QuarantineDTO.SearchField.FROM.name())
                        .param("searchKey", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(quarantineMailRepository, never()).getItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "search-quarantined-mail-count");
    }

    @Test
    @WithMockUser
    public void releaseQuarantinedMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "release-quarantined-mail");

        doReturn(createMailRepositoryItem()).when(quarantineMailRepository).getItem(any());

        MvcResult result = mockMvc.perform(post(RestPaths.QUARANTINE_RELEASE_QUARANTINED_MAIL_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .param("releaseProcessor", QuarantineDTO.ReleaseProcessor.DEFAULT.toString())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        QuarantineDTO.QuarantinedMail response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertNotNull(response);

        verify(mailEnqueuer).enqueue(any());
        verify(mailRepositoryEventListener).onReleased(any(), any());
        verify(quarantineMailRepository).deleteItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "release-quarantined-mail");
    }

    @Test
    @WithMockUser
    public void releaseQuarantinedMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.QUARANTINE_RELEASE_QUARANTINED_MAIL_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .param("releaseProcessor", QuarantineDTO.ReleaseProcessor.DEFAULT.toString())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(quarantineMailRepository, never()).getItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "release-quarantined-mail");
    }

    @Test
    @WithMockUser
    public void releaseQuarantinedMails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "release-quarantined-mails");

        doReturn(createMailRepositoryItem()).when(quarantineMailRepository).getItem(any());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(UUID.randomUUID(), UUID.randomUUID()));

        mockMvc.perform(post(RestPaths.QUARANTINE_RELEASE_QUARANTINED_MAILS_PATH)
                        .param("releaseProcessor", QuarantineDTO.ReleaseProcessor.DEFAULT.toString())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(mailEnqueuer, times(2)).enqueue(any());
        verify(mailRepositoryEventListener, times(2)).onReleased(any(), any());
        verify(quarantineMailRepository, times(2)).deleteItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "release-quarantined-mails");
    }

    @Test
    @WithMockUser
    public void releaseQuarantinedMailsNoPermission()
    throws Exception
    {
        doReturn(createMailRepositoryItem()).when(quarantineMailRepository).getItem(any());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(UUID.randomUUID(), UUID.randomUUID()));

        MvcResult result = mockMvc.perform(post(RestPaths.QUARANTINE_RELEASE_QUARANTINED_MAILS_PATH)
                        .param("releaseProcessor", QuarantineDTO.ReleaseProcessor.DEFAULT.toString())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(mailEnqueuer, never()).enqueue(any());
        verify(mailRepositoryEventListener, never()).onReleased(any(), any());
        verify(quarantineMailRepository, never()).deleteItem(any());

        verify(permissionChecker).checkPermission(PermissionCategory.QUARANTINE,
                "release-quarantined-mails");
    }
}