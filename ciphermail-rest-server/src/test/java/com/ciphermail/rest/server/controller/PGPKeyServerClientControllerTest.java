/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.openpgp.keyserver.KeyServerClient;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporter;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyAlgorithm;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClient;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientSettings;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientSettingsImpl;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.rest.core.PGPKeyServerDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.PGPKeyServerSubmitDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.function.Failable;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPPublicKeyRingCollection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = PGPKeyServerClientController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class,
        PGPKeyServerSubmitDetailsFactory.class
})
public class PGPKeyServerClientControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private KeyServerClient keyServerClient;

    @Autowired
    private HKPKeyServerClient hkpKeyServerClient;

    @MockBean
    private PermissionChecker permissionChecker;

    @Autowired
    private PGPKeyRingImporter keyRingImporter;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private MockMvc mockMvc;

    private final List<PGPKeyRingEntry> keyRingEntries = new LinkedList<>();

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices
    {
        // empty on purpose
    }

    @Before
    public void setup()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            keyRing.deleteAll();

            keyRingEntries.addAll(keyRingImporter.importKeyRing(new FileInputStream(new File(TEST_BASE_CORE,
                            "pgp/0xb91671444a3c41eb.asc")),
                    new StaticPasswordProvider("test")));

            keyRingEntries.addAll(keyRingImporter.importKeyRing(new FileInputStream(new File(TEST_BASE_CORE,
                            "pgp/test-multiple-sub-keys.gpg.asc")),
                    new StaticPasswordProvider("test")));

            keyRingEntries.addAll(keyRingImporter.importKeyRing(new FileInputStream(new File(TEST_BASE_CORE,
                            "pgp/secp256k1@example.com.asc")),
                    new StaticPasswordProvider("test")));

            HKPKeyServerClientSettings settings = new HKPKeyServerClientSettingsImpl(TestProperties.getPGPKeyserverURL1());

            hkpKeyServerClient.submitKeys(settings, new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                    new FileInputStream(new File(TEST_BASE_CORE, "pgp/0xb91671444a3c41eb.asc")))));

        }));

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @Test
    @WithMockUser
    public void searchKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "search-keys");

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_SEARCH_KEYS_PATH)
                        .param("query", "0xb91671444a3c41eb")
                        .param("exactMatch", "true")
                        .param("maxKeys", "100")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        PGPKeyServerDTO.PGPKeyServerSearchResult searchResult= JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, searchResult.keys().size());
        // we cannot match the URL because this URL is different when running in gitlab (docker)
        //assertEquals("http://127.0.0.1:11371", searchResult.keys().get(0).keyServerURL());
        assertEquals("AF29335EE9DA5BEC89883CACB91671444A3C41EB", searchResult.keys().get(0).keyID());
        assertEquals(PGPPublicKeyAlgorithm.RSA, searchResult.keys().get(0).algorithm());
        assertEquals(2048, searchResult.keys().get(0).keyLength());
        assertEquals(1394466898000L, (long) searchResult.keys().get(0).creationDate());
        assertNull(searchResult.keys().get(0).expirationDate());
        assertEquals("r", searchResult.keys().get(0).keyFlags());
        assertEquals("", String.join(",", searchResult.errors()));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "search-keys");
    }

    @Test
    @WithMockUser
    public void searchKeysNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_SEARCH_KEYS_PATH)
                        .param("query", "test")
                        .param("exactMatch", "true")
                        .param("maxKeys", "100")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "search-keys");
    }

    @Test
    @WithMockUser
    public void downloadKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "download-keys");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("0xb91671444a3c41eb"));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_DOWNLOAD_KEYS_PATH)
                        .param("autoTrust", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        PGPKeyServerDTO.PGPKeyServerDownloadResult downloadResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, downloadResult.downloadedIds().size());
        assertEquals(0, downloadResult.importedIds().size());
        assertEquals(0, downloadResult.errors().size());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "download-keys");
    }

    @Test
    @WithMockUser
    public void downloadKeysError()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "download-keys");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("invalid"));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_DOWNLOAD_KEYS_PATH)
                        .param("autoTrust", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        PGPKeyServerDTO.PGPKeyServerDownloadResult downloadResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(0, downloadResult.downloadedIds().size());
        assertEquals(0, downloadResult.importedIds().size());
        assertEquals(1, downloadResult.errors().size());
        assertTrue(downloadResult.errors().get(0).startsWith("Error downloading key with key ID invalid."));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "download-keys");
    }

    @Test
    @WithMockUser
    public void downloadKeysNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("DC368B248911C140EF6564764605970C271AD23B"));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_DOWNLOAD_KEYS_PATH)
                        .param("autoTrust", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "download-keys");
    }

    @Test
    @WithMockUser
    public void refreshKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "refresh-keys");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(keyRingEntries.get(0).getID()));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_REFRESH_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        PGPKeyServerDTO.PGPKeyServerDownloadResult downloadResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, downloadResult.downloadedIds().size());
        assertEquals(0, downloadResult.importedIds().size());
        assertEquals(0, downloadResult.errors().size());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "refresh-keys");
    }

    @Test
    @WithMockUser
    public void refreshKeysNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(keyRingEntries.get(0).getID(), keyRingEntries.get(1).getID()));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_REFRESH_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "refresh-keys");
    }

    @Test
    @WithMockUser
    public void submitKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "submit-keys");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(keyRingEntries.get(0).getID(), keyRingEntries.get(1).getID()));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_SUBMIT_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        PGPKeyServerDTO.KeyServerSubmitDetails submitResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, submitResult.responses().size());
        assertEquals(1, submitResult.urls().size());
        assertEquals(0, submitResult.errors().size());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "submit-keys");
    }

    @Test
    @WithMockUser
    public void submitKeysNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(keyRingEntries.get(0).getID(), keyRingEntries.get(1).getID()));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_SUBMIT_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "submit-keys");
    }

    @Test
    @WithMockUser
    public void setPGPKeyServers()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "set-pgp-key-servers");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(new PGPKeyServerDTO.PGPKeyServerClientSettings(
                "http://127.0.0.1:11371", "/pks/lookup", "/pks/add"
        )));

        mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_SET_PGP_KEY_SERVERS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "set-pgp-key-servers");
    }

    @Test
    @WithMockUser
    public void setPGPKeyServersNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(new PGPKeyServerDTO.PGPKeyServerClientSettings(
                "http://127.0.0.1:11371", "/pks/lookup", "/pks/add"
        )));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEY_SERVER_SET_PGP_KEY_SERVERS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "set-pgp-key-servers");
    }

    @Test
    @WithMockUser
    public void getPGPKeyServers()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "get-pgp-key-servers");

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEY_SERVER_GET_PGP_KEY_SERVERS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<PGPKeyServerDTO.PGPKeyServerClientSettings> servers = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, servers.size());
        // we cannot match the URL because this URL is different when running in gitlab (docker)
        //assertEquals("http://127.0.0.1:11371", servers.get(0).serverURL());
        assertEquals("/pks/lookup", servers.get(0).lookupKeysPath());
        assertEquals("/pks/add", servers.get(0).submitKeysPath());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "get-pgp-key-servers");
    }

    @Test
    @WithMockUser
    public void getPGPKeyServersNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEY_SERVER_GET_PGP_KEY_SERVERS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                "get-pgp-key-servers");
    }
}