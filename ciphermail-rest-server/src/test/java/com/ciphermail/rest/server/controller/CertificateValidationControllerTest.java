/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.CertificateStore;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.ciphermail.rest.server.test.RestTestUtils;
import org.apache.commons.lang3.function.Failable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@WebMvcTest(value = CertificateValidationController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        X509CertificateDetailsFactory.class,
        PermissionProviderRegistry.class})
public class CertificateValidationControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    private static final String NON_EXISTING_THUMBPRINT =
            "0000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000";

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices {
        // empty on purpose
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            // Clean stores
            keyAndCertStore.removeAllEntries();
            rootStore.removeAllEntries();
        }));

        importCertificates(keyAndCertStore, new File(TEST_BASE_CORE,
                "certificates/chinesechars.cer"));

        importKeyStore(keyAndCertificateWorkflow, new File(TEST_BASE_CORE,
                "keys/testCertificates.p12"),"test");

        importCertificates(rootStore, new File(TEST_BASE_CORE,
                "certificates/mitm-test-root.cer"));

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    private void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile, String password)
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
            keyStore.load(new FileInputStream(pfxFile), password.toCharArray());

            keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                    MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
        }));
    }

    private void importCertificates(X509CertStoreExt certStore, File certificateFile)
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);

            for (Certificate certificate : certificates)
            {
                if (certificate instanceof X509Certificate) {
                    certStore.addCertificate((X509Certificate) certificate);
                }
            }
        }));
    }

    @Test
    @WithMockUser
    public void testValidateCertificateForSigning()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-signing");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_SIGNING_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "E37636F264F55EB141B5E3524F66D9E5EFCAC34D1FF85D97ACEA71673E" +
                                             "99E3B12294BD0B23B18C2D03FDAA75D3DB9A70700B73B72C557EB7F91D98B62CCB9DC4"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.CertificateValidationResult certificateValidationResult =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        CertificateDTO.CertificateValidationResult.class);

        assertTrue(certificateValidationResult.valid());
        assertTrue(certificateValidationResult.trusted());
        assertFalse(certificateValidationResult.blackListed());
        assertFalse(certificateValidationResult.whiteListed());
        assertFalse(certificateValidationResult.revoked());
        assertNull(certificateValidationResult.failureMessage());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-signing");
    }

    @Test
    @WithMockUser
    public void testValidateCertificateForSigningNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_SIGNING_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "E37636F264F55EB141B5E3524F66D9E5EFCAC34D1FF85D97ACEA71673E" +
                                             "99E3B12294BD0B23B18C2D03FDAA75D3DB9A70700B73B72C557EB7F91D98B62CCB9DC4"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-signing");
    }

    @Test
    @WithMockUser
    public void testNotValidForSigning()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-signing");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_SIGNING_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844" +
                                             "F3C9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.CertificateValidationResult certificateValidationResult =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        CertificateDTO.CertificateValidationResult.class);

        assertEquals("Certificate cannot be used for S/MIME signatures. Key usage does not allow " +
                     "DIGITALSIGNATURE or NONREPUDIATION", certificateValidationResult.failureMessage());
        assertFalse(certificateValidationResult.valid());
        assertTrue(certificateValidationResult.trusted());
        assertFalse(certificateValidationResult.blackListed());
        assertFalse(certificateValidationResult.whiteListed());
        assertFalse(certificateValidationResult.revoked());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-signing");
    }

    @Test
    @WithMockUser
    public void testValidateExternalCertificateForSigning()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-external-certificate-for-signing");

        X509Certificate certificate = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testcertificate.cer")).get(0);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(
                RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_SIGNING_PATH)
                        .file("chain", certificate.getEncoded())
                        .param("thumbprint", X509CertificateInspector.getThumbprint(certificate))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.CertificateValidationResult certificateValidationResult =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        CertificateDTO.CertificateValidationResult.class);

        assertFalse(certificateValidationResult.valid());
        assertFalse(certificateValidationResult.trusted());
        assertFalse(certificateValidationResult.blackListed());
        assertFalse(certificateValidationResult.whiteListed());
        assertFalse(certificateValidationResult.revoked());
        assertEquals("Error building certPath. No issuer certificate for certificate in certification path found.",
                certificateValidationResult.failureMessage());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-external-certificate-for-signing");
    }

    @Test
    @WithMockUser
    public void testValidateExternalCertificateForSigningNoPermission()
    throws Exception
    {
        X509Certificate certificate = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testcertificate.cer")).get(0);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(
                                RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_SIGNING_PATH)
                        .file("chain", certificate.getEncoded())
                        .param("thumbprint", X509CertificateInspector.getThumbprint(certificate))
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-external-certificate-for-signing");
    }

    @Test
    @WithMockUser
    public void testValidateCertificateForEncryption()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-encryption");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_ENCRYPTION_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "E37636F264F55EB141B5E3524F66D9E5EFCAC34D1FF85D97ACEA71673E" +
                                             "99E3B12294BD0B23B18C2D03FDAA75D3DB9A70700B73B72C557EB7F91D98B62CCB9DC4"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.CertificateValidationResult certificateValidationResult =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        CertificateDTO.CertificateValidationResult.class);

        assertTrue(certificateValidationResult.valid());
        assertTrue(certificateValidationResult.trusted());
        assertFalse(certificateValidationResult.blackListed());
        assertFalse(certificateValidationResult.whiteListed());
        assertFalse(certificateValidationResult.revoked());
        assertNull(certificateValidationResult.failureMessage());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-encryption");
    }

    @Test
    @WithMockUser
    public void testValidateCertificateForEncryptionNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_ENCRYPTION_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "E37636F264F55EB141B5E3524F66D9E5EFCAC34D1FF85D97ACEA71673E" +
                                             "99E3B12294BD0B23B18C2D03FDAA75D3DB9A70700B73B72C557EB7F91D98B62CCB9DC4"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-encryption");
    }

    @Test
    @WithMockUser
    public void testNotValidForEncryption()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-encryption");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_ENCRYPTION_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844" +
                                             "F3C9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.CertificateValidationResult certificateValidationResult =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        CertificateDTO.CertificateValidationResult.class);

        assertEquals("Certificate cannot be used for S/MIME encryption. Key usage does not allow " +
                     "KEYENCIPHERMENT", certificateValidationResult.failureMessage());
        assertFalse(certificateValidationResult.valid());
        assertTrue(certificateValidationResult.trusted());
        assertFalse(certificateValidationResult.blackListed());
        assertFalse(certificateValidationResult.whiteListed());
        assertFalse(certificateValidationResult.revoked());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-encryption");
    }

    @Test
    @WithMockUser
    public void testNoPath()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-encryption");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_ENCRYPTION_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "E59AF824078FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E3" +
                                             "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.CertificateValidationResult certificateValidationResult =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        CertificateDTO.CertificateValidationResult.class);

        assertEquals("Error building certPath. No issuer certificate for certificate in certification " +
                     "path found.", certificateValidationResult.failureMessage());
        assertFalse(certificateValidationResult.valid());
        assertFalse(certificateValidationResult.trusted());
        assertFalse(certificateValidationResult.blackListed());
        assertFalse(certificateValidationResult.whiteListed());
        assertFalse(certificateValidationResult.revoked());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate-for-encryption");
    }

    @Test
    @WithMockUser
    public void testValidateExternalCertificateForEncryption()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-external-certificate-for-encryption");

        X509Certificate certificate = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testcertificate.cer")).get(0);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(
                                RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_ENCRYPTION_PATH)
                        .file("chain", certificate.getEncoded())
                        .param("thumbprint", X509CertificateInspector.getThumbprint(certificate))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.CertificateValidationResult certificateValidationResult =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        CertificateDTO.CertificateValidationResult.class);

        assertFalse(certificateValidationResult.valid());
        assertFalse(certificateValidationResult.trusted());
        assertFalse(certificateValidationResult.blackListed());
        assertFalse(certificateValidationResult.whiteListed());
        assertFalse(certificateValidationResult.revoked());
        assertEquals("Error building certPath. No issuer certificate for certificate in certification path found.",
                certificateValidationResult.failureMessage());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-external-certificate-for-encryption");
    }

    @Test
    @WithMockUser
    public void testValidateExternalCertificateForEncryptionNoPermission()
    throws Exception
    {
        X509Certificate certificate = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testcertificate.cer")).get(0);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(
                                RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_ENCRYPTION_PATH)
                        .file("chain", certificate.getEncoded())
                        .param("thumbprint", X509CertificateInspector.getThumbprint(certificate))
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-external-certificate-for-encryption");
    }

    @Test
    @WithMockUser
    public void testValidateCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844" +
                                             "F3C9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.CertificateValidationResult certificateValidationResult =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        CertificateDTO.CertificateValidationResult.class);

        assertNull(certificateValidationResult.failureMessage());
        assertTrue(certificateValidationResult.valid());
        assertTrue(certificateValidationResult.trusted());
        assertFalse(certificateValidationResult.blackListed());
        assertFalse(certificateValidationResult.whiteListed());
        assertFalse(certificateValidationResult.revoked());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate");
    }

    @Test
    @WithMockUser
    public void testValidateCertificateNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844" +
                                             "F3C9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate");
    }

    @Test
    @WithMockUser
    public void testCertificateNotAvailable()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate");

        mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("Certificate with thumbprint " + NON_EXISTING_THUMBPRINT + " not found"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate");
    }

    @Test
    @WithMockUser
    public void testInvalidThumbprint()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "test"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                ",\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"," +
                "\"error\":\"Bad Request\",\"message\":\"test is not a valid postfix thumbprint\"," +
                "\"key\":\"backend.validation.invalid-thumbprint\",\"params\":null,\"path\":\"\"}"));

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-certificate");
    }

    @Test
    @WithMockUser
    public void testValidateExternalCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-external-certificate");

        X509Certificate certificate = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testcertificate.cer")).get(0);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(
                                RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_PATH)
                        .file("chain", certificate.getEncoded())
                        .param("thumbprint", X509CertificateInspector.getThumbprint(certificate))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.CertificateValidationResult certificateValidationResult =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        CertificateDTO.CertificateValidationResult.class);

        assertFalse(certificateValidationResult.valid());
        assertFalse(certificateValidationResult.trusted());
        assertFalse(certificateValidationResult.blackListed());
        assertFalse(certificateValidationResult.whiteListed());
        assertFalse(certificateValidationResult.revoked());
        assertEquals("Error building certPath. No issuer certificate for certificate in certification path found.",
                certificateValidationResult.failureMessage());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-external-certificate");
    }

    @Test
    @WithMockUser
    public void testValidateExternalCertificateNoPermission()
    throws Exception
    {
        X509Certificate certificate = CertificateUtils.readX509Certificates(new File(TEST_BASE_CORE,
                "certificates/testcertificate.cer")).get(0);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(
                                RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_PATH)
                        .file("chain", certificate.getEncoded())
                        .param("thumbprint", X509CertificateInspector.getThumbprint(certificate))
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "validate-external-certificate");
    }

    @Test
    @WithMockUser
    public void testGetIssuerCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "get-issuer-certificate");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_GET_ISSUER_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "05818393AAE1765AD32F1D78C157220147469091E9B1BA92E6C94E687" +
                                             "A679BEB5FA85CC10585FC5090283602EEAD6595A91C993A3D4ED99D6F517EF2ED7DC73E"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.IssuerWithStore issuerWithStore = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CertificateDTO.IssuerWithStore.class);

        Assert.assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL",
                issuerWithStore.certificate().subjectFriendly());
        Assert.assertEquals(CertificateStore.CERTIFICATES,
                issuerWithStore.store());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "get-issuer-certificate");
    }

    @Test
    @WithMockUser
    public void testGetIssuerCertificateRoot()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "get-issuer-certificate");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_GET_ISSUER_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844" +
                                             "F3C9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.IssuerWithStore issuerWithStore = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CertificateDTO.IssuerWithStore.class);

        Assert.assertEquals("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL",
                issuerWithStore.certificate().subjectFriendly());
        Assert.assertEquals(CertificateStore.ROOTS,
                issuerWithStore.store());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "get-issuer-certificate");
    }

    @Test
    @WithMockUser
    public void testNoIssuer()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "get-issuer-certificate");

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_VALIDATION_GET_ISSUER_CERTIFICATE_PATH)
                        .param("store", CertificateStore.CERTIFICATES.name())
                        .param("thumbprint", "E59AF824078FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E31" +
                                             "C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                "get-issuer-certificate");
    }
}