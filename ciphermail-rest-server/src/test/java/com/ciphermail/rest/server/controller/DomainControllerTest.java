/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.impl.MockupUserPreferences;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.util.CloseableIteratorAdapter;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.DomainDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.PropertyValidationFailedExceptionHandler;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import java.io.StringWriter;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DomainController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class DomainControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean
    private DomainManager domainManager;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }
    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    @Test
    @WithMockUser
    public void addDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "add-domain");

        when(domainManager.getDomainPreferences("example.com")).thenReturn(new MockupUserPreferences("example.com",
                UserPreferencesCategory.DOMAIN.name()));
        when(domainManager.addDomain("new.example.com")).thenReturn(new MockupUserPreferences("new.example.com",
                UserPreferencesCategory.DOMAIN.name()));

        // try to add a domain which was already added
        mockMvc.perform(post(RestPaths.DOMAIN_ADD_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));

        // try to add a new domain
        mockMvc.perform(post(RestPaths.DOMAIN_ADD_DOMAIN_PATH)
                        .param("domain", "new.example.com")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        verify(permissionChecker, times(2)).checkPermission(PermissionCategory.DOMAIN,
                "add-domain");
    }

    @Test
    @WithMockUser
    public void addDomainNoPermission()
    throws Exception
    {
        // try to add a new domain
        MvcResult result = mockMvc.perform(post(RestPaths.DOMAIN_ADD_DOMAIN_PATH)
                        .param("domain", "new.example.com")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "add-domain");
    }

    @Test
    @WithMockUser
    public void addInvalidDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "add-domain");

        // try to add an invalid domain
        MvcResult result = mockMvc.perform(post(RestPaths.DOMAIN_ADD_DOMAIN_PATH)
                        .param("domain", "!@#\"/")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                "\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"" +
                ",\"error\":\"Bad Request\",\"message\":\"!@#\\\"/ is not a valid domain\"," +
                "\"key\":\"backend.validation.invalid-domain\",\"params\":null,\"path\":\"\"}"));

        PropertyValidationFailedExceptionHandler.PropertyValidationFailedError propertyValidationFailedError =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        PropertyValidationFailedExceptionHandler.PropertyValidationFailedError.class);

        assertEquals("!@#\"/ is not a valid domain", propertyValidationFailedError.message());

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "add-domain");
    }

    @Test
    @WithMockUser
    public void isDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "is-domain");

        when(domainManager.getDomainPreferences("example.com")).thenReturn(new MockupUserPreferences("example.com",
                UserPreferencesCategory.DOMAIN.name()));

        mockMvc.perform(get(RestPaths.DOMAIN_IS_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(get(RestPaths.DOMAIN_IS_DOMAIN_PATH)
                        .param("domain", "nonexisting.example.com"))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));

        verify(permissionChecker, times(2)).checkPermission(PermissionCategory.DOMAIN,
                "is-domain");
    }

    @Test
    @WithMockUser
    public void isDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DOMAIN_IS_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "is-domain");
    }

    @Test
    @WithMockUser
    public void deleteDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "delete-domain");

        when(domainManager.getDomainPreferences("example.com")).thenReturn(new MockupUserPreferences("example.com",
                UserPreferencesCategory.DOMAIN.name()));

        when(domainManager.deleteDomainPreferences(any())).thenReturn(true);

        mockMvc.perform(post(RestPaths.DOMAIN_DELETE_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(post(RestPaths.DOMAIN_DELETE_DOMAIN_PATH)
                        .param("domain", "nonexisting.example.com")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));

        verify(permissionChecker, times(2)).checkPermission(PermissionCategory.DOMAIN,
                "delete-domain");
    }

    @Test
    @WithMockUser
    public void deleteDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DOMAIN_DELETE_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "delete-domain");
    }

    @Test
    @WithMockUser
    public void deleteDomains()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "delete-domains");

        when(domainManager.getDomainPreferences("example.com")).thenReturn(new MockupUserPreferences("example.com",
                UserPreferencesCategory.DOMAIN.name()));
        when(domainManager.getDomainPreferences("test.example.com")).thenReturn(new MockupUserPreferences("test.example.com",
                UserPreferencesCategory.DOMAIN.name()));

        when(domainManager.deleteDomainPreferences(any())).thenReturn(true);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("example.com", "test.example.com"));

        mockMvc.perform(post(RestPaths.DOMAIN_DELETE_DOMAINS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "delete-domains");
    }

    @Test
    @WithMockUser
    public void deleteDomainsNoPermissions()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("example.com", "test.example.com"));

        MvcResult result = mockMvc.perform(post(RestPaths.DOMAIN_DELETE_DOMAINS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "delete-domains");
    }

    @Test
    @WithMockUser
    public void getDomains()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "get-domains");

        List<String> domains = List.of("one.example.com", "two.example.com");

        try (CloseableIteratorAdapter<String> iterator = new CloseableIteratorAdapter<>(
                domains.iterator()))
        {
            when(domainManager.getDomainIterator(any(), any(), any())).thenReturn(iterator);
        }

        String json = mockMvc.perform(get(RestPaths.DOMAIN_GET_DOMAINS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "10")
                        .param("sortDirection", SortDirection.ASC.name()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        DomainDTO.DomainValue[] domainsValues = JacksonUtil.getObjectMapper().readValue(
                json, DomainDTO.DomainValue[].class);

        assertEquals(2, domainsValues.length);

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "get-domains");
    }

    @Test
    @WithMockUser
    public void getDomainsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DOMAIN_GET_DOMAINS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "10")
                        .param("sortDirection", SortDirection.ASC.name()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "get-domains");
    }

    @Test
    @WithMockUser
    public void getMaxResultsExceed()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "get-domains");

        mockMvc.perform(get(RestPaths.DOMAIN_GET_DOMAINS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "251")
                        .param("sortDirection", SortDirection.ASC.name()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("maxResults exceed the upper limit 250"));

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "get-domains");
    }

    @Test
    @WithMockUser
    public void getDomainsCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "get-domains-count");

        when(domainManager.getDomainCount()).thenReturn(10L);

        mockMvc.perform(get(RestPaths.DOMAIN_GET_DOMAINS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "get-domains-count");
    }

    @Test
    @WithMockUser
    public void getDomainsCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DOMAIN_GET_DOMAINS_COUNT_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "get-domains-count");
    }

    @Test
    @WithMockUser
    public void isDomainInUse()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "is-domain-in-use");

        when(domainManager.isDomainInUse(any())).thenReturn(true);

        mockMvc.perform(get(RestPaths.DOMAIN_IS_DOMAIN_IN_USE_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "is-domain-in-use");
    }

    @Test
    @WithMockUser
    public void isDomainInUseNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DOMAIN_IS_DOMAIN_IN_USE_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DOMAIN,
                "is-domain-in-use");
    }
}