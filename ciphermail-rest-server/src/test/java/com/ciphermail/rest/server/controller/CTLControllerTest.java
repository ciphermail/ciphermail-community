/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.ctl.CTLEntity;
import com.ciphermail.core.common.security.ctl.CTLEntry;
import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.digest.Digests;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.CTLDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = CTLController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class CTLControllerTest
{
    private static final String NON_EXISTING_THUMBPRINT =
            "0000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000";

    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    private CTLManager ctlManager;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    private static class MockCTLEntry implements CTLEntry
    {
        private final String thumbprint;
        private CTLEntryStatus status;
        boolean allowExpired;

        MockCTLEntry(String thumbprint, CTLEntryStatus status, boolean allowExpired)
        {
            this.thumbprint = thumbprint;
            this.status = status;
            this.allowExpired = allowExpired;
        }

        @Nonnull
        @Override
        public String getThumbprint() {
            return thumbprint;
        }

        @Nonnull
        @Override
        public CTLEntryStatus getStatus() {
            return status;
        }

        @Override
        public void setStatus(CTLEntryStatus status) {
            this.status = status;
        }

        @Override
        public boolean isAllowExpired() {
            return allowExpired;
        }

        @Override
        public void setAllowExpired(boolean allowExpired) {
            this.allowExpired = allowExpired;
        }
    }

    @Test
    @WithMockUser
    public void getCTLs()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "get-ctls");

        List<CTLEntry> ctls = new LinkedList<>();

        ctls.add(new CTLEntity("0", Digests.digestHex("0".getBytes(),Digest.SHA512)));
        ctls.get(0).setStatus(CTLEntryStatus.WHITELISTED);
        ctls.get(0).setAllowExpired(true);

        ctls.add(new CTLEntity("1", Digests.digestHex("1".getBytes(),Digest.SHA512)));
        ctls.get(1).setStatus(CTLEntryStatus.BLACKLISTED);
        ctls.get(1).setAllowExpired(false);

        OngoingStubbing<List<? extends CTLEntry>> stubbing = when(ctlManager.getCTL(any()).getEntries(any(), any()));

        stubbing.thenReturn(ctls);

        MvcResult result = mockMvc.perform(get(RestPaths.CTL_GET_CTLS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        CTLDTO.CTLResult[] results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CTLDTO.CTLResult[].class);

        assertEquals(2, results.length);

        assertEquals(ctls.get(0).getThumbprint(), results[0].thumbprint());
        assertEquals(ctls.get(0).getStatus(), results[0].status());
        assertEquals(ctls.get(0).isAllowExpired(), results[0].allowExpired());

        assertEquals(ctls.get(1).getThumbprint(), results[1].thumbprint());
        assertEquals(ctls.get(1).getStatus(), results[1].status());
        assertEquals(ctls.get(1).isAllowExpired(), results[1].allowExpired());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "get-ctls");
    }

    @Test
    @WithMockUser
    public void getCTLsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CTL_GET_CTLS_PATH))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "get-ctls");
    }

    @Test
    @WithMockUser
    public void getCTLsMaxResultsToHigh()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "get-ctls");

        mockMvc.perform(get(RestPaths.CTL_GET_CTLS_PATH)
                .param("maxResults", "251"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("maxResults exceed the upper limit 250"));

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "get-ctls");
    }

    @Test
    @WithMockUser
    public void getCTLsCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "get-ctls-count");

        when(ctlManager.getCTL(any()).size()).thenReturn(10L);

        mockMvc.perform(get(RestPaths.CTL_GET_CTLS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "get-ctls-count");
    }

    @Test
    @WithMockUser
    public void getCTLsCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CTL_GET_CTLS_COUNT_PATH))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "get-ctls-count");
    }

    @Test
    @WithMockUser
    public void getCTL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "get-ctl");

        String thumbprint = Digests.digestHex("1".getBytes(),Digest.SHA512);

        when(ctlManager.getCTL(any()).getEntry(thumbprint)).thenReturn(new CTLEntity("1", thumbprint));

        MvcResult result = mockMvc.perform(get(RestPaths.CTL_GET_CTL_PATH)
                        .param("thumbprint", thumbprint))
                .andExpect(status().isOk())
                .andReturn();

        CTLDTO.CTLResult ctl = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CTLDTO.CTLResult.class);

        assertEquals(thumbprint, ctl.thumbprint());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "get-ctl");
    }

    @Test
    @WithMockUser
    public void getCTLNoPermission()
    throws Exception
    {
        String thumbprint = Digests.digestHex("1".getBytes(),Digest.SHA512);

        MvcResult result = mockMvc.perform(get(RestPaths.CTL_GET_CTL_PATH)
                        .param("thumbprint", thumbprint))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "get-ctl");
    }

    @Test
    @WithMockUser
    public void getCTLNonExisting()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "get-ctl");

        when(ctlManager.getCTL(any()).getEntry(NON_EXISTING_THUMBPRINT)).thenReturn(null);

        MvcResult result = mockMvc.perform(get(RestPaths.CTL_GET_CTL_PATH)
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "get-ctl");
    }

    @Test
    @WithMockUser
    public void getCTLInvalidThumbprint()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "get-ctl");

        MvcResult result = mockMvc.perform(get(RestPaths.CTL_GET_CTL_PATH)
                        .param("thumbprint", "test"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                ",\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"," +
                "\"error\":\"Bad Request\",\"message\":\"test is not a valid postfix thumbprint\"," +
                "\"key\":\"backend.validation.invalid-thumbprint\",\"params\":null,\"path\":\"\"}"
        ));

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "get-ctl");
    }

    @Test
    @WithMockUser
    public void setNewCTL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "set-ctl");

        String thumbprint = "E59AF824078FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E3" +
                            "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75";

        when(ctlManager.getCTL(any()).getEntry(thumbprint)).thenReturn(null);
        when(ctlManager.getCTL(any()).createEntry(thumbprint)).thenReturn(new MockCTLEntry(thumbprint,
                CTLEntryStatus.WHITELISTED, false));

        MvcResult result = mockMvc.perform(post(RestPaths.CTL_SET_CTL_PATH)
                        .param("thumbprint", thumbprint)
                        .param("status", CTLEntryStatus.BLACKLISTED.name())
                        .param("allowExpired", "true")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CTLDTO.CTLResult ctl = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CTLDTO.CTLResult.class);

        assertEquals(CTLEntryStatus.BLACKLISTED, ctl.status());
        assertTrue(ctl.allowExpired());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "set-ctl");
    }

    @Test
    @WithMockUser
    public void setNewCTLNoPermission()
    throws Exception
    {
        String thumbprint = "E59AF824078FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E3" +
                            "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75";

        MvcResult result = mockMvc.perform(post(RestPaths.CTL_SET_CTL_PATH)
                        .param("thumbprint", thumbprint)
                        .param("status", CTLEntryStatus.BLACKLISTED.name())
                        .param("allowExpired", "true")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "set-ctl");
    }

    @Test
    @WithMockUser
    public void setExistingCTL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "set-ctl");

        when(ctlManager.getCTL(any()).getEntry((String)any())).thenReturn(new MockCTLEntry(
                "E59AF824078FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E3" +
                         "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75",
                CTLEntryStatus.WHITELISTED, false));

        mockMvc.perform(post(RestPaths.CTL_SET_CTL_PATH)
                        .param("thumbprint", "E59AF824078FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E3" +
                                             "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75")
                        .param("status", CTLEntryStatus.BLACKLISTED.name())
                        .param("allowExpired", "true")
                        .with(csrf()))
                .andExpect(status().isOk());

        CTLEntry ctlEntry = ctlManager.getCTL(CTLManager.DEFAULT_CTL).getEntry(
                "E59AF824078FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E3" +
                            "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75");

        assertEquals(CTLEntryStatus.BLACKLISTED, ctlEntry.getStatus());
        assertTrue(ctlEntry.isAllowExpired());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "set-ctl");
    }

    @Test
    @WithMockUser
    public void setCTLInvalidThumbprint()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "set-ctl");

        MvcResult result = mockMvc.perform(post(RestPaths.CTL_SET_CTL_PATH)
                        .param("thumbprint", "test")
                        .param("status", CTLEntryStatus.BLACKLISTED.name())
                        .param("allowExpired", "true")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                ",\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"," +
                "\"error\":\"Bad Request\",\"message\":\"test is not a valid postfix thumbprint\"," +
                "\"key\":\"backend.validation.invalid-thumbprint\",\"params\":null,\"path\":\"\"}"
        ));

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "set-ctl");
    }

    @Test
    @WithMockUser
    public void setCTLs()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "set-ctls");

        String thumbprint1 = "E59AF824078FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E3" +
                             "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75";
        String thumbprint2 = "FFFFFFFFFF8FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E3" +
                             "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75";
        String thumbprint3 = "FFFFFFFFFFFFFFFFFFFFFFFFE9961658E3E33579CE5632AB3EE7049E3" +
                             "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75";

        when(ctlManager.getCTL(any()).getEntry(thumbprint1)).thenReturn(null);
        when(ctlManager.getCTL(any()).createEntry(thumbprint1)).thenReturn(new MockCTLEntry(thumbprint1,
                CTLEntryStatus.WHITELISTED, false));
        when(ctlManager.getCTL(any()).getEntry(thumbprint2)).thenReturn(new MockCTLEntry(thumbprint2,
                CTLEntryStatus.BLACKLISTED, false));
        when(ctlManager.getCTL(any()).getEntry(thumbprint3)).thenReturn(null);
        when(ctlManager.getCTL(any()).createEntry(thumbprint3)).thenReturn(new MockCTLEntry(thumbprint3,
                CTLEntryStatus.BLACKLISTED, false));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(thumbprint1, thumbprint2, thumbprint3));

        mockMvc.perform(post(RestPaths.CTL_SET_CTLS_PATH)
                        .param("status", CTLEntryStatus.BLACKLISTED.name())
                        .param("allowExpired", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(ctlManager.getCTL(any())).createEntry(thumbprint1);
        verify(ctlManager.getCTL(any())).createEntry(thumbprint3);
        verify(ctlManager.getCTL(any()), times(2)).createEntry((String) any());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "set-ctls");
    }

    @Test
    @WithMockUser
    public void setCTLsNoPermission()
    throws Exception
    {
        String thumbprint1 = "E59AF824078FA7AD3FECFB0EE9961658E3E33579CE5632AB3EE7049E3" +
                             "1C7B2C5BC7E90E56BA876A42D29BDA984DA7DCD3C844F6DACCB5F3CBDE4A8EA6760CA75";

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(thumbprint1));

        MvcResult result = mockMvc.perform(post(RestPaths.CTL_SET_CTLS_PATH)
                        .param("status", CTLEntryStatus.BLACKLISTED.name())
                        .param("allowExpired", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "set-ctls");
    }

    @Test
    @WithMockUser
    public void deleteCTL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "delete-ctl");

        String thumbprint = Digests.digestHex("1".getBytes(),Digest.SHA512);

        when(ctlManager.getCTL(any()).getEntry(thumbprint)).thenReturn(new MockCTLEntry(thumbprint,
                CTLEntryStatus.BLACKLISTED, true));

        MvcResult result = mockMvc.perform(post(RestPaths.CTL_DELETE_CTL_PATH)
                        .param("thumbprint", thumbprint)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(ctlManager.getCTL(any())).deleteEntry(any());

        CTLDTO.CTLResult ctl = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CTLDTO.CTLResult.class);

        assertEquals(CTLEntryStatus.BLACKLISTED, ctl.status());
        assertTrue(ctl.allowExpired());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "delete-ctl");
    }

    @Test
    @WithMockUser
    public void deleteCTLNonExisting()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "delete-ctl");

        when(ctlManager.getCTL(any()).getEntry(NON_EXISTING_THUMBPRINT)).thenReturn(null);

        mockMvc.perform(post(RestPaths.CTL_DELETE_CTL_PATH)
                        .param("thumbprint", NON_EXISTING_THUMBPRINT)
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("CTL with thumbprint " + NON_EXISTING_THUMBPRINT + " not found"));

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "delete-ctl");
    }

    @Test
    @WithMockUser
    public void deleteCTLInvalidThumbprint()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "delete-ctl");

        MvcResult result = mockMvc.perform(post(RestPaths.CTL_DELETE_CTL_PATH)
                        .param("thumbprint", "test")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                ",\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"," +
                "\"error\":\"Bad Request\",\"message\":\"test is not a valid postfix thumbprint\"," +
                "\"key\":\"backend.validation.invalid-thumbprint\",\"params\":null,\"path\":\"\"}"
        ));

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "delete-ctl");
    }

    @Test
    @WithMockUser
    public void deleteCTLNoPermission()
    throws Exception
    {
        String thumbprint = Digests.digestHex("1".getBytes(),Digest.SHA512);

        MvcResult result = mockMvc.perform(post(RestPaths.CTL_DELETE_CTL_PATH)
                        .param("thumbprint", thumbprint)
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "delete-ctl");
    }

    @Test
    @WithMockUser
    public void deleteCTLs()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CTL, "delete-ctls");

        String thumbprint1 = Digests.digestHex("1".getBytes(),Digest.SHA512);
        String thumbprint2 = Digests.digestHex("2".getBytes(),Digest.SHA512);

        when(ctlManager.getCTL(any()).getEntry(thumbprint1)).thenReturn(new MockCTLEntry(thumbprint1,
                CTLEntryStatus.BLACKLISTED, true));
        when(ctlManager.getCTL(any()).getEntry(thumbprint2)).thenReturn(new MockCTLEntry(thumbprint2,
                CTLEntryStatus.WHITELISTED, true));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(thumbprint1, thumbprint2));

        mockMvc.perform(post(RestPaths.CTL_DELETE_CTLS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(ctlManager.getCTL(any()), times(2)).deleteEntry(any());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "delete-ctls");
    }

    @Test
    @WithMockUser
    public void deleteCTLsNoPermission()
    throws Exception
    {
        String thumbprint1 = Digests.digestHex("1".getBytes(),Digest.SHA512);
        String thumbprint2 = Digests.digestHex("2".getBytes(),Digest.SHA512);

        when(ctlManager.getCTL(any()).getEntry(thumbprint1)).thenReturn(new MockCTLEntry(thumbprint1,
                CTLEntryStatus.BLACKLISTED, true));
        when(ctlManager.getCTL(any()).getEntry(thumbprint2)).thenReturn(new MockCTLEntry(thumbprint2,
                CTLEntryStatus.WHITELISTED, true));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(thumbprint1, thumbprint2));

        MvcResult result = mockMvc.perform(post(RestPaths.CTL_DELETE_CTLS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(ctlManager.getCTL(any()), never()).deleteEntry(any());

        verify(permissionChecker).checkPermission(PermissionCategory.CTL,
                "delete-ctls");
    }
}