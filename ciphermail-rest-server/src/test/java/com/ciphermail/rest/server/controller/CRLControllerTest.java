/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crl.CRLUtils;
import com.ciphermail.core.common.security.crl.ThreadedCRLStoreUpdater;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.util.CurrentDateProvider;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import com.ciphermail.rest.core.CRLDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CRLDetailsFactory;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.function.Failable;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@WebMvcTest(value = CRLController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        X509CRLDetailsFactory.class,
        PermissionProviderRegistry.class})
public class CRLControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    private static final String NON_EXISTING_THUMBPRINT =
            "0000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000";

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    private X509CRLStoreExt crlStore;

    @MockBean
    private ThreadedCRLStoreUpdater crlStoreUpdater;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices {
        // empty on purpose
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
                // Clean stores
                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();
                crlStore.removeAllEntries();
        }));

        importCertificates(keyAndCertStore, new File(TEST_BASE_CORE,
                "certificates/chinesechars.cer"));

        importKeyStore(keyAndCertificateWorkflow, new File(TEST_BASE_CORE,
                "keys/testCertificates.p12"),"test");

        importCertificates(rootStore, new File(TEST_BASE_CORE,
                "certificates/mitm-test-root.cer"));

        importCRL(new File(TEST_BASE_CORE, "crls/test-ca-no-next-update.crl"));
        importCRL(new File(TEST_BASE_CORE, "crls/ca.cnipa.gov.it-crl3-thisupdate-211207.crl"));
        importCRL(new File(TEST_BASE_CORE, "crls/max-next-update-time.crl"));
        importCRL(new File(TEST_BASE_CORE, "crls/user-trust-intermediate.crl"));

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    private void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile, String password)
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
            keyStore.load(new FileInputStream(pfxFile), password.toCharArray());

            keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                    MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
        }));
    }

    private void importCertificates(X509CertStoreExt certStore, File certificateFile)
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);

            for (Certificate certificate : certificates)
            {
                if (certificate instanceof X509Certificate) {
                    certStore.addCertificate((X509Certificate) certificate);
                }
            }
        }));
    }

    private void importCRL(File crlFile)
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() -> crlStore.addCRL(
                TestUtils.loadX509CRL(crlFile))));
    }

    @Test
    @WithMockUser
    public void importCRLs()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "import-crls");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CRL_IMPORT_CRLS_PATH)
                        .file("derEncodedCRLs", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "crls/ThawteSGCCA.crl")))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CRLDTO.X509CRLDetails[] crls = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.X509CRLDetails[].class);

        assertEquals(1, crls.length);

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "import-crls");
    }

    @Test
    @WithMockUser
    public void importCRLsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CRL_IMPORT_CRLS_PATH)
                        .file("derEncodedCRLs", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "crls/ThawteSGCCA.crl")))
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "import-crls");
    }

    @Test
    @WithMockUser
    public void importExistingCRLs()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "import-crls");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.CRL_IMPORT_CRLS_PATH)
                        .file("derEncodedCRLs", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "crls/test-ca-no-next-update.crl")))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CRLDTO.X509CRLDetails[] crls = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.X509CRLDetails[].class);

        assertEquals(0, crls.length);

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "import-crls");
    }

    @Test
    @WithMockUser
    public void getCRLs()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls");

        MvcResult result = mockMvc.perform(get(RestPaths.CRL_GET_CRLS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        CRLDTO.X509CRLDetails[] crls = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.X509CRLDetails[].class);

        assertEquals(4, crls.length);

        result = mockMvc.perform(get(RestPaths.CRL_GET_CRLS_PATH)
                        .param("firstResult", "1")
                        .param("maxResults", "2"))
                .andExpect(status().isOk())
                .andReturn();

        crls = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.X509CRLDetails[].class);

        assertEquals(2, crls.length);
        assertEquals("CN=CNIPA CA1, OU=Servizi di Sicurezza e Certificazione, O=Centro Nazionale per " +
                     "l'Informatica nella PA, C=IT", crls[1].issuerFriendly());
        assertEquals("2007-12-22T01:38:05+01:00", DateFormatUtils.ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT
                .format(crls[1].nextUpdate()));
        assertEquals("2007-12-21T13:28:05+01:00", DateFormatUtils.ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT
                .format(crls[1].thisUpdate()));
        assertEquals(2, crls[1].version());
        assertEquals("448B", crls[1].crlNumber());
        assertNull(crls[1].deltaIndicator());
        assertFalse(crls[1].deltaCRL());
        assertEquals("706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7FABA8CCF78685D62A3F19" +
                     "FE56352C0268B05716D8A5FCD37726B534C0FE49B15", crls[1].thumbprint());
        assertEquals("E28ACC7B6A6282EA892A35CA0F49E17FD488D0E8", crls[1].thumbprintSHA1());
        assertEquals("SHA1WITHRSA", crls[1].signatureAlgorithm());

        verify(permissionChecker, times(2)).checkPermission(PermissionCategory.CRL,
                "get-crls");
    }

    @Test
    @WithMockUser
    public void getCRLsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CRL_GET_CRLS_PATH))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls");
    }

    @Test
    @WithMockUser
    public void getCRLsMaxResultExceedLimit()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls");

        mockMvc.perform(get(RestPaths.CRL_GET_CRLS_PATH)
                        .param("firstResult", "1")
                        .param("maxResults", "251"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("maxResults exceed the upper limit 250"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls");
    }

    @Test
    @WithMockUser
    public void getCRLsNoResults()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls");

        MvcResult result = mockMvc.perform(get(RestPaths.CRL_GET_CRLS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        CRLDTO.X509CRLDetails[] crls = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.X509CRLDetails[].class);

        assertEquals(4, crls.length);

        mockMvc.perform(get(RestPaths.CRL_GET_CRLS_PATH)
                        .param("firstResult", "10"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));

        verify(permissionChecker, times(2)).checkPermission(PermissionCategory.CRL,
                "get-crls");
    }

    @Test
    @WithMockUser
    public void getCRLsCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls-count");

        mockMvc.perform(get(RestPaths.CRL_GET_CRLS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("4"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls-count");
    }

    @Test
    @WithMockUser
    public void getCRLsCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CRL_GET_CRLS_COUNT_PATH))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls-count");
    }

    @Test
    @WithMockUser
    public void getCRL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crl");

        MvcResult result = mockMvc.perform(get(RestPaths.CRL_GET_CRL_PATH)
                        .param("thumbprint", "706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                                             "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15"))
                .andExpect(status().isOk())
                .andReturn();

        CRLDTO.X509CRLDetails crl = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.X509CRLDetails.class);
        assertEquals("CN=CNIPA CA1, OU=Servizi di Sicurezza e Certificazione, O=Centro Nazionale per " +
                     "l'Informatica nella PA, C=IT", crl.issuerFriendly());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crl");
    }

    @Test
    @WithMockUser
    public void getCRLNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CRL_GET_CRL_PATH)
                        .param("thumbprint", "706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                                             "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crl");
    }

    @Test
    @WithMockUser
    public void getInvalidThumbprintL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crl");

        MvcResult result = mockMvc.perform(get(RestPaths.CRL_GET_CRL_PATH)
                        .param("thumbprint", "test"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        Assert.assertTrue(result.getResponse().getContentAsString().endsWith(
                ",\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"," +
                "\"error\":\"Bad Request\",\"message\":\"test is not a valid postfix thumbprint\"," +
                "\"key\":\"backend.validation.invalid-thumbprint\",\"params\":null,\"path\":\"\"}"
        ));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crl");
    }

    @Test
    @WithMockUser
    public void getCRLNotFound()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crl");

        mockMvc.perform(get(RestPaths.CRL_GET_CRL_PATH)
                        .param("thumbprint", NON_EXISTING_THUMBPRINT))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("CRL with thumbprint " + NON_EXISTING_THUMBPRINT + " not found"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crl");
    }

    @Test
    @WithMockUser
    public void deleteCRL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls-count");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crl");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "delete-crl");

        mockMvc.perform(get(RestPaths.CRL_GET_CRLS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("4"));

        mockMvc.perform(get(RestPaths.CRL_GET_CRL_PATH)
                        .param("thumbprint", "706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                                             "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15"))
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(post(RestPaths.CRL_DELETE_CRL_PATH)
                        .param("thumbprint", "706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                                             "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CRLDTO.X509CRLDetails crl = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.X509CRLDetails.class);

        assertNotNull(crl);

        mockMvc.perform(get(RestPaths.CRL_GET_CRLS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("3"));

        mockMvc.perform(get(RestPaths.CRL_GET_CRL_PATH)
                        .param("thumbprint", "706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                                             "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("CRL with thumbprint 706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072" +
                        "E56EF6A2A592C7FABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15 not found"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "delete-crl");
    }

    @Test
    @WithMockUser
    public void deleteCRLNoPermission()
    throws Exception
    {
       MvcResult result = mockMvc.perform(post(RestPaths.CRL_DELETE_CRL_PATH)
                        .param("thumbprint", "706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                                             "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15")
                        .with(csrf()))
               .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
               .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "delete-crl");
    }

    @Test
    @WithMockUser
    public void deleteCRLNotFound()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "delete-crl");

        mockMvc.perform(post(RestPaths.CRL_DELETE_CRL_PATH)
                        .param("thumbprint", NON_EXISTING_THUMBPRINT)
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("CRL with thumbprint " + NON_EXISTING_THUMBPRINT + " not found"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "delete-crl");
    }

    @Test
    @WithMockUser
    public void deleteCRLNoCSRF()
    throws Exception
    {
        mockMvc.perform(post(RestPaths.CRL_DELETE_CRL_PATH)
                        .param("thumbprint", "706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                                             "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andExpect(status().reason("Forbidden"));
    }

    @Test
    @WithMockUser
    public void deleteCRLs()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls-count");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crl");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "delete-crls");

        mockMvc.perform(get(RestPaths.CRL_GET_CRLS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("4"));

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9" +
                        "EB500B12F762E037E0058CED354CAB1C42F3EEE60AE54C82E1FD90F453027CF");
        thumbprints.add("706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                        "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        mockMvc.perform(post(RestPaths.CRL_DELETE_CRLS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(get(RestPaths.CRL_GET_CRLS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("2"));

        mockMvc.perform(get(RestPaths.CRL_GET_CRL_PATH)
                        .param("thumbprint", "7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9" +
                                             "EB500B12F762E037E0058CED354CAB1C42F3EEE60AE54C82E1FD90F453027CF"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("CRL with thumbprint 7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9" +
                                           "EB500B12F762E037E0058CED354CAB1C42F3EEE60AE54C82E1FD90F453027CF not found"));

        mockMvc.perform(get(RestPaths.CRL_GET_CRL_PATH)
                        .param("thumbprint", "706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                                             "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("CRL with thumbprint 706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072" +
                                           "E56EF6A2A592C7FABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15 not found"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "delete-crls");
    }

    @Test
    @WithMockUser
    public void deleteCRLsNoPermission()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "get-crls-count");

        mockMvc.perform(get(RestPaths.CRL_GET_CRLS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("4"));

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9" +
                        "EB500B12F762E037E0058CED354CAB1C42F3EEE60AE54C82E1FD90F453027CF");
        thumbprints.add("706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                        "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CRL_DELETE_CRLS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        mockMvc.perform(get(RestPaths.CRL_GET_CRLS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("4"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "delete-crls");
    }

    @Test
    @WithMockUser
    public void exportCRLsDER()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9" +
                        "EB500B12F762E037E0058CED354CAB1C42F3EEE60AE54C82E1FD90F453027CF");
        thumbprints.add("706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                        "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CRL_EXPORT_CRLS_PATH)
                        .param("encoding", ObjectEncoding.DER.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] encodedCRLs = result.getResponse().getContentAsByteArray();

        assertEquals(0x30, encodedCRLs[0]);

        List<X509CRL> crls = CRLUtils.readX509CRLs(new ByteArrayInputStream(encodedCRLs));

        assertEquals(2, crls.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");
    }

    @Test
    @WithMockUser
    public void exportCRLsNoPermission()
    throws Exception
    {
        List<String> thumbprints = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CRL_EXPORT_CRLS_PATH)
                        .param("encoding", ObjectEncoding.DER.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");
    }

    @Test
    @WithMockUser
    public void exportCRLsPEM()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9" +
                        "EB500B12F762E037E0058CED354CAB1C42F3EEE60AE54C82E1FD90F453027CF");
        thumbprints.add("706DF5D82C25A43967108B7E5FF194533729DB53412AF0E072E56EF6A2A592C7F" +
                        "ABA8CCF78685D62A3F19FE56352C0268B05716D8A5FCD37726B534C0FE49B15");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CRL_EXPORT_CRLS_PATH)
                        .param("encoding", ObjectEncoding.PEM.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] encodedCRLs = result.getResponse().getContentAsByteArray();

        assertTrue(new String(encodedCRLs).contains("-----BEGIN X509 CRL-----"));
        assertTrue(new String(encodedCRLs).contains("-----END X509 CRL-----"));

        List<X509CRL> crls = CRLUtils.readX509CRLs(new ByteArrayInputStream(encodedCRLs));

        assertEquals(2, crls.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");
    }

    @Test
    @WithMockUser
    public void exportCRLsNotFound()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9" +
                        "EB500B12F762E037E0058CED354CAB1C42F3EEE60AE54C82E1FD90F453027CF");
        thumbprints.add(NON_EXISTING_THUMBPRINT);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        mockMvc.perform(post(RestPaths.CRL_EXPORT_CRLS_PATH)
                        .param("encoding", ObjectEncoding.PEM.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("CRL with thumbprint " + NON_EXISTING_THUMBPRINT + " not found"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");
    }

    @Test
    @WithMockUser
    public void exportCRLsTooManyThumbprints()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");

        List<String> thumbprints = new LinkedList<>();

        for (int i = 0; i < 251; i++) {
            thumbprints.add(Integer.toString(i));
        }

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        mockMvc.perform(post(RestPaths.CRL_EXPORT_CRLS_PATH)
                        .param("encoding", ObjectEncoding.DER.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("number of thumbprints exceed the upper limit 250"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");
    }

    @Test
    @WithMockUser
    public void exportCRLsThumbprintsMissing()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");

        List<String> thumbprints = new LinkedList<>();
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        mockMvc.perform(post(RestPaths.CRL_EXPORT_CRLS_PATH)
                        .param("encoding", ObjectEncoding.DER.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("thumbprints are missing"));

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "export-crls");
    }

    @Test
    @WithMockUser
    public void refreshCRLStore()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "refresh-crl-store");

        mockMvc.perform(post(RestPaths.CRL_REFRESH_CRL_STORE_PATH)
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(crlStoreUpdater).update();

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "refresh-crl-store");
    }

    @Test
    @WithMockUser
    public void refreshCRLStoreNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CRL_REFRESH_CRL_STORE_PATH)
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "refresh-crl-store");
    }

    @Test
    @WithMockUser
    public void validateCRL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "validate-crl");

        MvcResult result = mockMvc.perform(post(RestPaths.CRL_VALIDATE_CRL_PATH)
                        .param("thumbprint", "7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9" +
                                             "EB500B12F762E037E0058CED354CAB1C42F3EEE60AE54C82E1FD90F453027CF")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CRLDTO.CRLValidationResult validationResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.CRLValidationResult.class);

        assertFalse(validationResult.trusted());
        assertEquals("Error while building path for CRL. CRL: Issuer: CN=QuoVadis Swiss Advanced CA G2, " +
                     "O=QuoVadis Trustlink Switzerland Ltd., C=CH; CRL number: 75074; Thumbprint: " +
                     "7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9EB500B12F762E037E0058CE" +
                     "D354CAB1C42F3EEE60AE54C82E1FD90F453027CF; SHA1: 494CA6B7350FFC43AC80B361899BF034E3816173",
                validationResult.failureMessage());

        result = mockMvc.perform(post(RestPaths.CRL_VALIDATE_CRL_PATH)
                        .param("thumbprint", "2BE1470945F3CC9015C6E12276896B19529E7F119E8981A8CC3205916F961A85" +
                                             "DA4BAEEBBF569FDF4BA864C0EA6ABE7D5F44C0271CE4A3B995197DD424EFCF62")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        validationResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), CRLDTO.CRLValidationResult.class);

        assertTrue(validationResult.trusted());
        assertNull(validationResult.failureMessage());

        verify(permissionChecker, times(2)).checkPermission(PermissionCategory.CRL,
                "validate-crl");
   }

    @Test
    @WithMockUser
    public void validateCRLNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CRL_VALIDATE_CRL_PATH)
                        .param("thumbprint", "7EFD790BE5B76D7575269B4992724ECFA7E22F0A766204DF9410A14D48A75CAD9" +
                                             "EB500B12F762E037E0058CED354CAB1C42F3EEE60AE54C82E1FD90F453027CF")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "validate-crl");
    }

    @Test
    @WithMockUser
    public void validateCRLExpired()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CRL,
                "validate-crl");

        try {
            // override the current date to force the issuer to be expired
            CurrentDateProvider.setDateOverrideForCurrentThread(DateFormatUtils.
                    ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT.parse("2057-12-21T13:28:05+01:00"));

            MvcResult result = mockMvc.perform(post(RestPaths.CRL_VALIDATE_CRL_PATH)
                            .param("thumbprint", "2BE1470945F3CC9015C6E12276896B19529E7F119E8981A8CC3205916F961A85" +
                                                 "DA4BAEEBBF569FDF4BA864C0EA6ABE7D5F44C0271CE4A3B995197DD424EFCF62")
                            .with(csrf()))
                    .andExpect(status().isOk())
                    .andReturn();

            CRLDTO.CRLValidationResult validationResult = JacksonUtil.getObjectMapper().readValue(
                    result.getResponse().getContentAsString(), CRLDTO.CRLValidationResult.class);

            assertFalse(validationResult.trusted());
            assertEquals("Certificate in the CRL path is expired. CRL: Issuer: EMAILADDRESS=ca@example.com, " +
                         "CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL; CRL number: ; Thumbprint: " +
                         "2BE1470945F3CC9015C6E12276896B19529E7F119E8981A8CC3205916F961A85DA4BAEEBBF569FDF4BA8" +
                         "64C0EA6ABE7D5F44C0271CE4A3B995197DD424EFCF62; SHA1: 1AD781B8958B041E0DF86D6DCD5CCDEB" +
                         "5A0C9026. Message: certificate expired on 20271121063835GMT+00:00",
                    validationResult.failureMessage());
        }
        finally {
            // need to reset the date override
            CurrentDateProvider.removeDateOverrideForCurrentThread();
        }

        verify(permissionChecker).checkPermission(PermissionCategory.CRL,
                "validate-crl");
   }
}