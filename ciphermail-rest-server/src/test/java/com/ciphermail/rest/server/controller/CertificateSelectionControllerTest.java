/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.NamedCertificateCategory;
import com.ciphermail.core.app.impl.NamedCertificateImpl;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.Nonnull;
import org.apache.commons.lang3.function.Failable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CertificateSelectionController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        X509CertificateDetailsFactory.class,
        PermissionProviderRegistry.class})
public class CertificateSelectionControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices {
        // empty on purpose
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            // delete all users
            userWorkflow.getUsers(null, null, SortDirection.ASC).forEach(
                    u -> userWorkflow.deleteUser(u));
            // delete all domains
            domainManager.deleteAllDomains();
            // remove global certs
            globalPreferencesManager.getGlobalUserPreferences().getCertificates().clear();
            globalPreferencesManager.getGlobalUserPreferences().getNamedCertificates().clear();
            globalPreferencesManager.getGlobalUserPreferences().setKeyAndCertificate(null);
            // Clean stores
            keyAndCertStore.removeAllEntries();
            rootStore.removeAllEntries();
        }));

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    private void importKeyStore(File p12File, String password)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() ->
            {
                KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
                keyStore.load(new FileInputStream(p12File), password.toCharArray());

                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                        MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
            });
        });
    }

    private void importCertificates(X509CertStoreExt certStore, File certificateFile)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            Failable.stream(Failable.get(() -> CertificateUtils.readX509Certificates(certificateFile)).stream())
                    .forEach(certStore::addCertificate);
        });
    }

    private void importTestCertificates()
    {
        importCertificates(rootStore, new File(TEST_BASE_CORE, "certificates/mitm-test-root.cer"));
        importKeyStore(new File(TEST_BASE_CORE, "keys/testCertificates.p12"), "test");
    }

    private X509Certificate getCertificate(@Nonnull String thumbprint)
    {
        return transactionOperations.execute(tx ->
                Failable.get(() -> keyAndCertStore.getByThumbprint(thumbprint).getCertificate()));
    }

    @Test
    @WithMockUser
    public void testGetAutoSelectedEncryptionCertificatesForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-auto-selected-encryption-certificates-for-user");

        importTestCertificates();

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_AUTOSELECTED_ENCRYPTION_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(13, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-auto-selected-encryption-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetAutoSelectedEncryptionCertificatesForUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_AUTOSELECTED_ENCRYPTION_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-auto-selected-encryption-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetExplicitCertificatesForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-explicit-certificates-for-user");

        importTestCertificates();

        String email = "testGetExplicitCertificatesForUser@example.com";

        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() ->
                    userWorkflow.makePersistent(userWorkflow.addUser(email))
                            .getUserPreferences()
                            .getCertificates()
                            .add(keyAndCertStore.getByThumbprint(
                                    "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B38" +
                                    "0DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE").getCertificate()));
        });

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_USER_PATH)
                        .param("email", email)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-explicit-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetExplicitCertificatesForUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-explicit-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testSetExplicitCertificatesForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-explicit-certificates-for-user");

        importTestCertificates();

        String email = "testSetExplicitCertificatesForUser@example.com";

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() -> userWorkflow.makePersistent(userWorkflow.addUser(email))));

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                        "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE");
        thumbprints.add("13D9AD877E7DF833EA2B3B70F07D0E3E05F9D5C1AAFDE7FA23CC9705FE1EB6234B1A367965A272" +
                        "054103DA817F950448DFE1380EB8FD885102DF750CAA52598E");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_USER_PATH)
                        .param("email", email)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-explicit-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testSetExplicitCertificatesForUserNoPermission()
    throws Exception
    {
        List<String> thumbprints = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-explicit-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetInheritedCertificatesForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-certificates-for-user");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() -> domainManager.addDomain("example.com").getCertificates().add(
                    keyAndCertStore.getByThumbprint(
                            "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B38" +
                            "0DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE").getCertificate()));
        });

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "testGetInheritedCertificatesForUser@example.com")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetInheritedCertificatesForUserNoPermission()
    throws Exception
    {
       MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "testGetInheritedCertificatesForUser@example.com")
                        .contentType("application/json"))
               .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
               .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetEncryptionCertificatesForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-encryption-certificates-for-user");

        importTestCertificates();

        String email = "test3@example.com";

        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() -> domainManager.addDomain("example.com").getCertificates().add(
                    keyAndCertStore.getByThumbprint(
                            "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69FB45FF52A34049" +
                            "AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B").getCertificate()));
            Failable.run(() -> userWorkflow.makePersistent(userWorkflow.addUser(email)));
        });

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_ENCRYPTION_CERTIFICATES_FOR_USER_PATH)
                        .param("email", email)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-encryption-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetEncryptionCertificatesForUserNoPermissions()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_ENCRYPTION_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-encryption-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetNamedCertificatesForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-named-certificates-for-user");

        importTestCertificates();

        String email = "test@example.com";

        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() -> userWorkflow.makePersistent(userWorkflow.addUser(email))
                    .getUserPreferences()
                    .getNamedCertificates()
                    .add(new NamedCertificateImpl(NamedCertificateCategory.ADDITIONAL.name(),
                            keyAndCertStore.getByThumbprint(
                                    "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69FB45FF52A34049" +
                                    "AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B").getCertificate())));
        });

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_USER_PATH)
                        .param("email", email)
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-named-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetNamedCertificatesForUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-named-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testSetNamedCertificatesForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-named-certificates-for-user");

        importTestCertificates();

        String email = "testSetNamedCertificatesForUser@example.com";

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() -> userWorkflow.makePersistent(userWorkflow.addUser(email))));

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                        "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE");
        thumbprints.add("13D9AD877E7DF833EA2B3B70F07D0E3E05F9D5C1AAFDE7FA23CC9705FE1EB6234B1A367965A272" +
                        "054103DA817F950448DFE1380EB8FD885102DF750CAA52598E");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_USER_PATH)
                        .param("email", email)
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-named-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testSetNamedCertificatesForUserNoPermission()
    throws Exception
    {
        List<String> thumbprints = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-named-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetInheritedNamedCertificatesForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-named-certificates-for-user");

        importTestCertificates();

        String email = "test@example.com";

        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() -> userWorkflow.makePersistent(userWorkflow.addUser(email)));

            Failable.run(() -> domainManager.addDomain("example.com").getNamedCertificates().add(
                    new NamedCertificateImpl(NamedCertificateCategory.ADDITIONAL.name(),
                            keyAndCertStore.getByThumbprint(
                                    "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69FB45FF52A34049" +
                                    "AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B").getCertificate())));
        });

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_USER_PATH)
                        .param("email", email)
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-named-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetInheritedNamedCertificatesForUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-named-certificates-for-user");
    }

    @Test
    @WithMockUser
    public void testGetExplicitCertificatesForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-explicit-certificates-for-domain");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() ->
            {
                Failable.run(() -> domainManager.addDomain("example.com").getCertificates().add(
                        keyAndCertStore.getByThumbprint(
                                "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69FB45FF52A34049" +
                                "AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B").getCertificate()));
            });
        });

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-explicit-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetExplicitCertificatesForDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-explicit-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testSetExplicitCertificatesForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-explicit-certificates-for-domain");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() -> domainManager.addDomain("example.com")));

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                        "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE");
        thumbprints.add("13D9AD877E7DF833EA2B3B70F07D0E3E05F9D5C1AAFDE7FA23CC9705FE1EB6234B1A367965A272" +
                        "054103DA817F950448DFE1380EB8FD885102DF750CAA52598E");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-explicit-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testSetExplicitCertificatesForDomainNoPermission()
    throws Exception
    {
        List<String> thumbprints = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-explicit-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetInheritedCertificatesForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-certificates-for-domain");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() -> domainManager.addDomain("example.com"));

            Failable.run(() -> globalPreferencesManager.getGlobalUserPreferences().getCertificates().add(
                    keyAndCertStore.getByThumbprint(
                            "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B38" +
                            "0DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE").getCertificate()));
        });

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetInheritedCertificatesForDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetNamedCertificatesForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-named-certificates-for-domain");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() -> domainManager.addDomain("example.com")
                        .getNamedCertificates()
                        .add(new NamedCertificateImpl(NamedCertificateCategory.ADDITIONAL.name(),
                                keyAndCertStore.getByThumbprint(
                                        "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69FB45FF52A34049" +
                                        "AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B").getCertificate()))));

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-named-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetNamedCertificatesForDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-named-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testSetNamedCertificatesForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-named-certificates-for-domain");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() -> domainManager.addDomain("example.com")));

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                        "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE");
        thumbprints.add("13D9AD877E7DF833EA2B3B70F07D0E3E05F9D5C1AAFDE7FA23CC9705FE1EB6234B1A367965A272" +
                        "054103DA817F950448DFE1380EB8FD885102DF750CAA52598E");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-named-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testSetNamedCertificatesForDomainNoPermission()
    throws Exception
    {
        List<String> thumbprints = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-named-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetInheritedNamedCertificatesForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-named-certificates-for-domain");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() -> domainManager.addDomain("example.com"));
            Failable.run(() -> globalPreferencesManager.getGlobalUserPreferences().getNamedCertificates().add(
                    new NamedCertificateImpl(NamedCertificateCategory.ADDITIONAL.name(),
                            keyAndCertStore.getByThumbprint(
                                    "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69FB45FF52A34049" +
                                    "AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B").getCertificate())));
        });

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-named-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetInheritedNamedCertificatesForDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-inherited-named-certificates-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetExplicitCertificatesForGlobal()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-explicit-certificates-global");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() ->
            {
                Failable.run(() -> globalPreferencesManager.getGlobalUserPreferences().getCertificates().add(
                        keyAndCertStore.getByThumbprint(
                                "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69FB45FF52A34049" +
                                "AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B").getCertificate()));
            });
        });

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_GLOBAL_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-explicit-certificates-global");
    }

    @Test
    @WithMockUser
    public void testGetExplicitCertificatesForGlobalNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_GLOBAL_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-explicit-certificates-global");
    }

    @Test
    @WithMockUser
    public void testSetExplicitCertificatesForGlobal()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-explicit-certificates-global");

        importTestCertificates();

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                        "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE");
        thumbprints.add("13D9AD877E7DF833EA2B3B70F07D0E3E05F9D5C1AAFDE7FA23CC9705FE1EB6234B1A367965A272" +
                        "054103DA817F950448DFE1380EB8FD885102DF750CAA52598E");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_GLOBAL_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-explicit-certificates-global");
    }

    @Test
    @WithMockUser
    public void testSetExplicitCertificatesForGlobalNoPermission()
    throws Exception
    {
        List<String> thumbprints = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_GLOBAL_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-explicit-certificates-global");
    }

    @Test
    @WithMockUser
    public void testGetNamedCertificatesForGlobal()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-named-certificates-global");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() -> globalPreferencesManager.getGlobalUserPreferences()
                        .getNamedCertificates()
                        .add(new NamedCertificateImpl(NamedCertificateCategory.ADDITIONAL.name(),
                                keyAndCertStore.getByThumbprint(
                                        "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69FB45FF52A34049" +
                                        "AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B").getCertificate()))));

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_GLOBAL_PATH)
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-named-certificates-global");
    }

    @Test
    @WithMockUser
    public void testGetNamedCertificatesForGlobalNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_GLOBAL_PATH)
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-named-certificates-global");
    }

    @Test
    @WithMockUser
    public void testSetNamedCertificatesForGlobal()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-named-certificates-global");

        importTestCertificates();

        List<String> thumbprints = new LinkedList<>();

        thumbprints.add("56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                        "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE");
        thumbprints.add("13D9AD877E7DF833EA2B3B70F07D0E3E05F9D5C1AAFDE7FA23CC9705FE1EB6234B1A367965A272" +
                        "054103DA817F950448DFE1380EB8FD885102DF750CAA52598E");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_GLOBAL_PATH)
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<CertificateDTO.X509CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-named-certificates-global");
    }

    @Test
    @WithMockUser
    public void testSetNamedCertificatesForGlobalNoPermission()
    throws Exception
    {
        List<String> thumbprints = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, thumbprints);

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_GLOBAL_PATH)
                        .param("category", NamedCertificateCategory.ADDITIONAL.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-named-certificates-global");
    }

    @Test
    @WithMockUser
    public void testGetSigningCertificateForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-signing-certificate-for-user");

        importTestCertificates();

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.X509CertificateDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertNotNull(response);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-signing-certificate-for-user");
    }

    @Test
    @WithMockUser
    public void testGetSigningCertificateForUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-signing-certificate-for-user");
    }

    @Test
    @WithMockUser
    public void testSetSigningCertificateForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-signing-certificate-for-user");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() ->
                        userWorkflow.makePersistent(userWorkflow.addUser("test@example.com"))));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .param("thumbprint",
                                "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                                "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.X509CertificateDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertNotNull(response);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-signing-certificate-for-user");
    }

    @Test
    @WithMockUser
    public void testSetSigningCertificateForUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .param("thumbprint",
                                "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                                "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-signing-certificate-for-user");
    }

    @Test
    @WithMockUser
    public void testGetSigningCertificateForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-signing-certificate-for-domain");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() -> domainManager.addDomain("example.com").setKeyAndCertificate(
                        keyAndCertStore.getKeyAndCertificate(keyAndCertStore.getByThumbprint(
                                "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                                "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE"
                        )))));

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.X509CertificateDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertNotNull(response);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-signing-certificate-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetSigningCertificateForDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-signing-certificate-for-domain");
    }

    @Test
    @WithMockUser
    public void testSetSigningCertificateForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-signing-certificate-for-domain");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() -> domainManager.addDomain("example.com")));

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("thumbprint",
                                "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                                "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.X509CertificateDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertNotNull(response);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-signing-certificate-for-domain");
    }

    @Test
    @WithMockUser
    public void testSetSigningCertificateForDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("thumbprint",
                                "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                                "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-signing-certificate-for-domain");
    }

    @Test
    @WithMockUser
    public void testGetSigningCertificateForGlobal()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-signing-certificate-for-global");

        importTestCertificates();

        transactionOperations.executeWithoutResult(status ->
                Failable.run(() -> globalPreferencesManager.getGlobalUserPreferences().setKeyAndCertificate(
                        keyAndCertStore.getKeyAndCertificate(keyAndCertStore.getByThumbprint(
                                "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                                "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE"
                        )))));

        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.X509CertificateDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertNotNull(response);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-signing-certificate-for-global");
    }

    @Test
    @WithMockUser
    public void testGetSigningCertificateForGlobalNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "get-signing-certificate-for-global");
    }

    @Test
    @WithMockUser
    public void testSetSigningCertificateForGlobal()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-signing-certificate-for-global");

        importTestCertificates();

        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH)
                        .param("thumbprint",
                                "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                                "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        CertificateDTO.X509CertificateDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertNotNull(response);

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-signing-certificate-for-global");
    }

    @Test
    @WithMockUser
    public void testSetSigningCertificateForGlobalNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH)
                        .param("thumbprint",
                                "56D4F0129F97AB1ECEDD43AD9B044AA086A56A352B0623B24E23AB98AEAF3925CF6C73EC24B380" +
                                "DCD8BD90C760C3BD44A45033941B28E650F3C11D55455985BE")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.CERTIFICATE_SELECTION,
                "set-signing-certificate-for-global");
    }
}