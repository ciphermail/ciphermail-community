/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.acme.AcmeManager;
import com.ciphermail.core.app.acme.AcmeTokenCache;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.certificate.TLSKeyPairBuilder;
import com.ciphermail.rest.core.AcmeDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.exception.AcmeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@Tag(name = "ACME")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class AcmeController
{
    private static final Logger logger = LoggerFactory.getLogger(AcmeController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private AcmeManager acmeManager;

    private enum Action
    {
        CREATE_ACCOUNT_KEYPAIR,
        ACCOUNT_KEYPAIR_EXISTS,
        GET_TERMS_OF_SERVICE,
        SET_TOS_ACCEPTED,
        GET_TOS_ACCEPTED,
        FIND_OR_REGISTER_ACCOUNT,
        DEACTIVATE_ACCOUNT,
        ORDER_CERTIFICATE,
        RENEW_CERTIFICATE;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.ACME,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.ACME, action.toPermissionName());
    }

    public AcmeController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @PostMapping(RestPaths.ACME_CREATE_ACCOUNT_KEYPAIR_PATH)
    public void createAccountKeyPair(
            @RequestParam AcmeDTO.KeyAlgorithm keyAlgorithm,
            @RequestParam(defaultValue = "false") boolean replaceExistingKey)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.CREATE_ACCOUNT_KEYPAIR);

            try {
                if (acmeManager.getAccountKeyPair() != null && !replaceExistingKey) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Account Key already exist", logger);
                }

                acmeManager.createAccountKeyPair(TLSKeyPairBuilder.Algorithm.valueOf(keyAlgorithm.name()));
            }
            catch (IOException | HierarchicalPropertiesException | GeneralSecurityException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @GetMapping(RestPaths.ACME_ACCOUNT_KEYPAIR_EXISTS_PATH)
    public boolean accountKeyPairExists()
    {
        return Boolean.TRUE.equals(transactionOperations.execute(tx ->
        {
            checkPermission(Action.ACCOUNT_KEYPAIR_EXISTS);

            try {
                return acmeManager.getAccountKeyPair() != null;
            }
            catch (IOException | HierarchicalPropertiesException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        }));
    }

    @GetMapping(RestPaths.ACME_GET_TERMS_OF_SERVICE_PATH)
    public URI getTermsOfService()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_TERMS_OF_SERVICE);

            try {
                return acmeManager.getTermsOfService().orElse(null);
            }
            catch (HierarchicalPropertiesException | AcmeException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @PostMapping(RestPaths.ACME_SET_TOS_ACCEPTED_PATH)
    public void setTosAccepted(boolean accepted)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_TOS_ACCEPTED);

            try {
                acmeManager.setTosAccepted(accepted);
            }
            catch (HierarchicalPropertiesException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @GetMapping(RestPaths.ACME_GET_TOS_ACCEPTED_PATH)
    public boolean getTosAccepted()
    {
        return Boolean.TRUE.equals(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_TOS_ACCEPTED);

            try {
                return acmeManager.isTosAccepted();
            }
            catch (HierarchicalPropertiesException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        }));
    }

    @PostMapping(RestPaths.ACME_FIND_OR_REGISTER_ACCOUNT_PATH)
    public AcmeDTO.Account findOrRegisterAccount()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.FIND_OR_REGISTER_ACCOUNT);

            try {
                Account account = acmeManager.findOrRegisterAccount();

                return new AcmeDTO.Account(
                        AcmeDTO.AccountStatus.valueOf(account.getStatus().name()),
                        account.getLocation(),
                        account.getContacts(),
                        Boolean.TRUE.equals(account.getTermsOfServiceAgreed().orElse(false)));
            }
            catch (HierarchicalPropertiesException | AcmeException | IOException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @PostMapping(RestPaths.ACME_DEACTIVATE_ACCOUNT_PATH)
    public void deactivateAccount()
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DEACTIVATE_ACCOUNT);

            try {
                acmeManager.deactivateAccount();
            }
            catch (HierarchicalPropertiesException | AcmeException | IOException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @PostMapping(RestPaths.ACME_ORDER_CERTIFICATE_PATH)
    public void orderCertificate(
            @RequestParam AcmeDTO.KeyAlgorithm keyAlgorithm,
            @RequestBody List<String> domains)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.ORDER_CERTIFICATE);

            try {
                acmeManager.orderCertificate(TLSKeyPairBuilder.Algorithm.valueOf(keyAlgorithm.name()), domains);
            }
            catch (HierarchicalPropertiesException | AcmeException | IOException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @PostMapping(RestPaths.ACME_RENEW_CERTIFICATE_PATH)
    public void renewCertificate(@RequestParam boolean forceRenewal)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.RENEW_CERTIFICATE);

            try {
                acmeManager.renewCertificate(forceRenewal);
            }
            catch (AcmeException | IOException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    /*
     * #getAuthorization will be called by the ACME server to validate the domain(s).
     *
     * Note: #getAuthorization should be accessible without authentication and without requiring any permission
     */
    @GetMapping(RestPaths.ACME_GET_AUTHORIZATION_PATH)
    public String getAuthorization(@PathVariable String token)
    {
        return transactionOperations.execute(tx ->
        {
            try {
                AcmeTokenCache.Challenge challenge = acmeManager.getChallenge(token);

                if (challenge == null) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                            String.format("Challenge with token %s not found", token), logger);
                }

                return challenge.authorization();
            }
            catch (HierarchicalPropertiesException | JsonProcessingException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }
}
