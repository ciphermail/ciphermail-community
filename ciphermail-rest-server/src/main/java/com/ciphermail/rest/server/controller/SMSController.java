/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.sms.SMS;
import com.ciphermail.core.common.sms.SMSGateway;
import com.ciphermail.core.common.sms.SMSTransportFactoryRegistry;
import com.ciphermail.core.common.sms.SortColumn;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.core.SMSDTO;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "SMS")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class SMSController
{
    private static final Logger logger = LoggerFactory.getLogger(SMSController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SMSGateway smsGateway;

    @Autowired
    private SMSTransportFactoryRegistry transportFactoryRegistry;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        GET_SMS_MESSAGES,
        SEND_SMS,
        DELETE_SMS,
        GET_SMS_QUEUE_SIZE,
        GET_SMS_TRANSPORTS;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.SMS,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.SMS, action.toPermissionName());
    }

    public SMSController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @GetMapping(RestPaths.SMS_GET_SMS_MESSAGES_PATH)
    public List<SMSDTO.SMSMessage> getSMSMessages(
            @RequestParam SortColumn sortColumn,
            @RequestParam SortDirection sortDirection,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_SMS_MESSAGES));

        return smsGateway.getAll(firstResult,restValidator.limitMaxResults(
                maxResults, logger), sortColumn, sortDirection)
                    .stream().map(SMSController::toSMSMessage).toList();
    }

    @PostMapping(path = RestPaths.SMS_SEND_SMS_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public SMSDTO.SMSMessage sendSMSMessage(
            @RequestParam String phoneNumber,
            @RequestBody String message)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.SEND_SMS));

        return toSMSMessage(smsGateway.sendSMS(phoneNumber, message));
    }

    @PostMapping(RestPaths.SMS_DELETE_SMS_PATH)
    public SMSDTO.SMSMessage deleteSMS(@RequestParam UUID id)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.DELETE_SMS));

        return toSMSMessage(smsGateway.delete(id));
    }

    @GetMapping(RestPaths.SMS_GET_SMS_QUEUE_SIZE_PATH)
    public long getSMSQueueSize()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_SMS_QUEUE_SIZE));

        return smsGateway.getCount();
    }

    @GetMapping(RestPaths.SMS_GET_SMS_TRANSPORTS_PATH)
    public List<String> getSMSTransports()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_SMS_TRANSPORTS));

        return transportFactoryRegistry.getFactoryNames().stream().toList();
    }

    private static SMSDTO.SMSMessage toSMSMessage(SMS sms) {
        return sms != null ? new SMSDTO.SMSMessage(sms.getID(),
                StringUtils.defaultString(sms.getMessage()),
                StringUtils.defaultString(sms.getPhoneNumber()),
                Optional.ofNullable(sms.getDateCreated()).map(Date::getTime).orElse(0L),
                Optional.ofNullable(sms.getDateLastTry()).map(Date::getTime).orElse(0L),
                StringUtils.defaultString(sms.getLastError())) : null;
    }
}
