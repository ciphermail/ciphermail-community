/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.PortalEmailSigner;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.JamestUtils;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MailEnqueuer;
import com.ciphermail.core.app.properties.PDFProperties;
import com.ciphermail.core.app.properties.PDFPropertiesImpl;
import com.ciphermail.core.app.properties.PortalProperties;
import com.ciphermail.core.app.properties.PortalPropertiesImpl;
import com.ciphermail.core.app.properties.TemplateProperties;
import com.ciphermail.core.app.properties.TemplatePropertiesImpl;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.pdf.PDFReplyURLBuilder;
import com.ciphermail.core.common.pdf.PDFURLBuilderException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.otp.OTPGenerator;
import com.ciphermail.core.common.security.otp.OTPQRCodeGenerator;
import com.ciphermail.core.common.template.TemplateBuilder;
import com.ciphermail.core.common.template.TemplateBuilderException;
import com.ciphermail.core.common.template.TemplateHashModelBuilder;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.rest.core.AuthDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.PortalDTO;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestException;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import freemarker.template.SimpleHash;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.ParseException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "Portal")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PortalController
{
    private static final Logger logger = LoggerFactory.getLogger(PortalController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private OTPGenerator otpGenerator;

    @Autowired
    private OTPQRCodeGenerator otpQRCodeGenerator;

    @Autowired
    private TemplateBuilder templateBuilder;

    @Autowired
    private TemplateHashModelBuilder templateHashModelBuilder;

    @Autowired
    private MailEnqueuer mailEnqueuer;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /*
     * The next processor for the forgot password email
     */
    @Value("${ciphermail.portal.forgot-password-processor}")
    private String forgotPasswordProcessor;

    private enum Action
    {
        PORTAL_CHANGE_PORTAL_PASSWORD,
        PORTAL_FORGOT_PASSWORD,
        PORTAL_GET_OTP_JSON_CODE,
        PORTAL_GET_OTP_QR_CODE,
        PORTAL_GET_OTP_SECRET_KEY_QR_CODE,
        PORTAL_GET_PDF_PASSWORD,
        PORTAL_GET_PDF_REPLY_PARAMETERS,
        PORTAL_GET_PDF_REPLY_URL,
        PORTAL_GET_PORTAL_PROPERTIES,
        PORTAL_RESET_PORTAL_PASSWORD,
        PORTAL_SET_2FA_ENABLED,
        PORTAL_SET_2FA_SECRET,
        PORTAL_SET_PORTAL_SIGNUP_PASSWORD,
        PORTAL_VALIDATE_PORTAL_PASSWORD_RESET,
        PORTAL_VALIDATE_SIGNUP;

        public String toPermissionName()
        {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (PortalController.Action action : PortalController.Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.PORTAL,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull PortalController.Action action) {
        permissionChecker.checkPermission(PermissionCategory.PORTAL, action.toPermissionName());
    }

    public PortalController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PortalController.PermissionProviderImpl());
    }

    @GetMapping(RestPaths.PORTAL_GET_PORTAL_PROPERTIES_PATH)
    public PortalDTO.PortalProperties getPortalProperties(@Nonnull String email)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_GET_PORTAL_PROPERTIES);

            try {
                User user = restValidator.validateNotNull(userWorkflow.getUser(restValidator.validateEmail(
                                email, logger),
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST), "User", logger);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                        .createInstance(userProperties);

                return new PortalDTO.PortalProperties(
                        portalProperties.getPassword(),
                        Boolean.TRUE.equals(portalProperties.getEnabled()),
                        Boolean.TRUE.equals(portalProperties.get2FAEnabled()),
                        StringUtils.trimToNull(portalProperties.get2FASecret()),
                        userProperties.getOrganizationID());
            }
            catch (AddressException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(RestPaths.PORTAL_SET_2FA_ENABLED_PATH)
    public void set2FAEnabled(@RequestParam String email, @RequestParam boolean enable)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.PORTAL_SET_2FA_ENABLED);

            try {
                User user = restValidator.validateNotNull(userWorkflow.getUser(restValidator.validateEmail(
                                email, logger),
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST), "User", logger);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                        .createInstance(userProperties);

                portalProperties.set2FAEnabled(enable);
            }
            catch (AddressException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(path = RestPaths.PORTAL_SET_2FA_SECRET_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public void set2FASecret(@RequestParam String email, @RequestBody String secret)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.PORTAL_SET_2FA_SECRET);

            try {
                User user = restValidator.validateNotNull(userWorkflow.getUser(restValidator.validateEmail(
                                email, logger),
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST), "User", logger);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                        .createInstance(userProperties);

                portalProperties.set2FASecret(secret);
            }
            catch (AddressException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PORTAL_GET_PDF_REPLY_URL_PATH)
    public String getPdfReplyURL(
            @RequestParam String sender,
            @RequestParam String replyRecipient,
            @RequestParam String replySender,
            @RequestParam String subject,
            @RequestParam(required = false) String messageID,
            @RequestParam(required = false, defaultValue = "80") int maxSubjectLength,
            @RequestParam(required = false) String baseURL)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_GET_PDF_REPLY_URL);

            try {
                User user = userWorkflow.getUser(restValidator.validateEmail(sender, logger),
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                PDFProperties senderPdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                        .createInstance(user.getUserPreferences().getProperties());

                String pdfReplySecret = restValidator.validateNotEmpty(
                        senderPdfProperties.getPdfReplySecretKey(),
                        "Pdf Reply secret key", logger);

                String replyURL = baseURL;

                // if baseURL parameter is not set, use the PdfReplyURL value for the user
                if (replyURL == null) {
                    replyURL = senderPdfProperties.getPdfReplyURL();
                }

                return PDFReplyURLBuilder.createInstance()
                        .setBaseURL(restValidator.validateNotEmpty(replyURL, "PDF reply URL", logger))
                        .setUser(sender)
                        .setRecipient(restValidator.validateEmail(replyRecipient, logger))
                        .setFrom(restValidator.validateEmail(replySender, logger))
                        .setSubject(StringUtils.abbreviate(StringUtils.trimToEmpty(subject), maxSubjectLength))
                        .setMessageID(messageID)
                        .setKey(pdfReplySecret)
                        .buildURL();
            }
            catch (PDFURLBuilderException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Reply URL is invalid", logger);
            }
            catch (AddressException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PORTAL_GET_PDF_REPLY_PARAMETERS_PATH)
    public PortalDTO.PdfReplyParameters getPdfReplyParameters(
            @RequestParam String env,
            @RequestParam String hmac)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_GET_PDF_REPLY_PARAMETERS);

            try {
                MutableObject<PDFProperties> initiatorPdfPropertiesMO = new MutableObject<>();

                PDFReplyURLBuilder replyURLBuilder = PDFReplyURLBuilder.createInstance().loadFromEnvelope(
                        env, hmac, builder ->
                        {
                            try {
                                // get the email address of the initiator of the message, i.e., the sender of the
                                // encrypted PDF containing the reply link.
                                String user = builder.getUser();

                                if (user == null) {
                                    throw new PDFURLBuilderException("user is null");
                                }

                                initiatorPdfPropertiesMO.setValue(userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                                        .createInstance(userWorkflow.getUser(user,
                                                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).
                                                getUserPreferences().getProperties()));

                                return initiatorPdfPropertiesMO.getValue().getPdfReplySecretKey();
                            }
                            catch (AddressException | HierarchicalPropertiesException e) {
                                throw new PDFURLBuilderException(e);
                            }
                        });

                final String replier = restValidator.validateNotNull(replyURLBuilder.getFrom(), "replier", logger);

                PDFProperties replySenderPdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                        .createInstance(userWorkflow.getUser(replier,
                                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).
                                getUserPreferences().getProperties());

                // the PDF reply sender can be set to a system defined sender
                // this might be needed when DMARC is configured
                final String systemReplySender = initiatorPdfPropertiesMO.getValue().getPdfReplySender();

                final String from;
                final String sender;

                // check if a system reply sender is set and if this is enabled
                if (BooleanUtils.toBoolean(initiatorPdfPropertiesMO.getValue().getPdfReplySenderEnabled()) &&
                        StringUtils.isNotEmpty(systemReplySender))
                {
                    // The sender of the message a special PDF reply user which should be configured by the administrator
                    sender = systemReplySender;

                    // Use a special "name" field, "in name of bla@example.com" for the From containing the real senders
                    // email address
                    from = new InternetAddress(sender, initiatorPdfPropertiesMO.getValue().getPdfReplyFromFormatString().
                            formatted(replier)).toString();
                }
                else {
                    from = replyURLBuilder.getFrom();
                    sender = replier;
                }

                final String to = replyURLBuilder.getRecipient();
                final String cc;

                // Send a CC of the reply message to the replier if this is enabled
                if (BooleanUtils.toBoolean(initiatorPdfPropertiesMO.getValue().getPdfReplyCC()) &&
                    BooleanUtils.toBoolean(replySenderPdfProperties.getPdfReplyCC()))
                {
                    cc = replier;
                }
                else {
                    cc = null;
                }

                final String subject = initiatorPdfPropertiesMO.getValue().getPdfReplySubjectFormatString().formatted(
                        replyURLBuilder.getSubject());

                // check if the reply link expired
                final boolean expired = ((System.currentTimeMillis() - replyURLBuilder.getTime()) >
                         restValidator.validateNotNull(replySenderPdfProperties.getPdfReplyValidityInterval(),
                                "pdfReplyValidityInterval", logger) * 1000);

                final boolean replyAllowed = BooleanUtils.toBoolean(initiatorPdfPropertiesMO.getValue().getPdfReplyEnabled()) &&
                                             BooleanUtils.toBoolean(replySenderPdfProperties.getPdfReplyEnabled());

                final long bodyMaxSize = replySenderPdfProperties.getPdfReplyBodyMaxSize();
                final long attachmentsMaxSize = replySenderPdfProperties.getPdfReplyAttachmentsMaxSize();

                return new PortalDTO.PdfReplyParameters(
                        from,
                        sender,
                        to,
                        cc,
                        subject,
                        replier,
                        replyURLBuilder.getMessageID(),
                        expired,
                        replyAllowed,
                        bodyMaxSize,
                        attachmentsMaxSize);
            }
            catch (PDFURLBuilderException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Reply URL is invalid", logger);
            }
            catch (AddressException | HierarchicalPropertiesException | UnsupportedEncodingException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PORTAL_GET_PDF_PASSWORD_PATH)
    public String getPdfPassword(@RequestParam String email, @RequestParam String code)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_GET_PDF_PASSWORD);

            // the OTP request code is JSON encoded
            PortalDTO.OTPRequest otpRequest;

            try {
                otpRequest = JacksonUtil.getObjectMapper().readValue(Base64Utils.decode(code), PortalDTO.OTPRequest.class);
            }
            catch (Exception e) {
                throw RestException.createInstance("OTP code is invalid").setKey("portal.otp-code-invalid").log(logger);
            }

            try {
                User user = userWorkflow.getUser(restValidator.validateEmail(email, logger),
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                        .createInstance(user.getUserPreferences().getProperties());

                String otpSecretKey = restValidator.validateNotEmpty(pdfProperties.getOTPSecretKey(),
                        "OTP secret key", logger);

                return otpGenerator.generate(
                        otpSecretKey.getBytes(StandardCharsets.UTF_8),
                        otpRequest.passwordID().getBytes(StandardCharsets.UTF_8),
                        otpRequest.length());
            }
            catch (HierarchicalPropertiesException | AddressException | GeneralSecurityException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PORTAL_GET_OTP_SECRET_KEY_QR_CODE_PATH)
    public String getOTPSecretKeyQRCode(@RequestParam String email)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_GET_OTP_SECRET_KEY_QR_CODE);

            try {
                User user = userWorkflow.getUser(restValidator.validateEmail(email, logger),
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                        .createInstance(userProperties);

                String otpSecretKey = restValidator.validateNotEmpty(pdfProperties.getOTPSecretKey(),
                        "OTP secret key", logger);

                return Base64Utils.encode(otpQRCodeGenerator.createSecretKeyQRCode(
                        otpSecretKey.getBytes(StandardCharsets.UTF_8),
                        StringUtils.defaultString(userProperties.getOrganizationID())));
            }
            catch (HierarchicalPropertiesException | AddressException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PORTAL_GET_OTP_JSON_CODE_PATH)
    public String getOTPJSONCode(@RequestParam String email, @Nonnull String passwordID, int passwordLength)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_GET_OTP_JSON_CODE);

            try {
                User user = userWorkflow.getUser(restValidator.validateEmail(email, logger),
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                        .createInstance(user.getUserPreferences().getProperties());

                String otpSecretKey = restValidator.validateNotEmpty(pdfProperties.getOTPSecretKey(),
                        "OTP secret key", logger);

                return Base64Utils.encode(otpQRCodeGenerator.createOTPJSONCode(
                        otpSecretKey.getBytes(StandardCharsets.UTF_8),
                        passwordID,
                        passwordLength).getBytes(StandardCharsets.UTF_8));
            }
            catch (HierarchicalPropertiesException | AddressException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PORTAL_GET_OTP_QR_CODE_PATH)
    public String getOTPQRCode(@RequestParam String email, @Nonnull String passwordID, int passwordLength)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_GET_OTP_QR_CODE);

            try {
                User user = userWorkflow.getUser(restValidator.validateEmail(email, logger),
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                        .createInstance(user.getUserPreferences().getProperties());

                String otpSecretKey = restValidator.validateNotEmpty(pdfProperties.getOTPSecretKey(),
                        "OTP secret key", logger);

                return Base64Utils.encode(otpQRCodeGenerator.createOTPQRCode(
                        otpSecretKey.getBytes(StandardCharsets.UTF_8),
                        passwordID,
                        passwordLength));
            }
            catch (HierarchicalPropertiesException | AddressException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PORTAL_VALIDATE_SIGNUP_PATH)
    public PortalDTO.PortalSignupValidationResult validatePortalSignup(@RequestParam String signupCode)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_VALIDATE_SIGNUP);

            return validatePortalSignupInternal(signupCode);
        });
    }

    @PostMapping(path = RestPaths.PORTAL_SET_PORTAL_SIGNUP_PASSWORD_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public PortalDTO.PortalSignupValidationResult setPortalSignupPassword(
            @RequestParam String signupCode,
            @RequestBody String password)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_SET_PORTAL_SIGNUP_PASSWORD);

            try {
                PortalDTO.PortalSignupValidationResult validationResult = validatePortalSignupInternal(signupCode);

                User user = restValidator.validateNotNull(userWorkflow.getUser(restValidator.validateEmail(
                                validationResult.email(), logger),
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST), "User", logger);

                PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                        .createInstance(user.getUserPreferences().getProperties());

                portalProperties.setPassword(password);

                return validationResult;
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    private PortalDTO.PortalSignupValidationResult validatePortalSignupInternal(@RequestParam String signupCode)
    {
        // the signup code is JSON encoded
        PortalDTO.MACProtectedEmail signupRequest;

        try {
            signupRequest = JacksonUtil.getObjectMapper().readValue(Base64Utils.decode(signupCode), PortalDTO.MACProtectedEmail.class);
        }
        catch (Exception e) {
            throw RestException.createInstance("Sign up code is invalid")
                    .setKey("portal.signup-code-invalid").setThrowable(e).log(logger);
        }

        try {
            User user = userWorkflow.getUser(restValidator.validateEmail(
                            signupRequest.email(), logger),
                    UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

            if (user == null) {
                throw RestException.createInstance("User with email address %s not found".formatted(signupRequest.email()))
                        .setKey("portal.user-not-registered").log(logger);
            }

            UserProperties userProperties = user.getUserPreferences().getProperties();

            PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                    .createInstance(userProperties);

            // check if the signup link expired
            if ((System.currentTimeMillis() - signupRequest.timestamp()) >  restValidator.validateNotNull(
                    portalProperties.getSignupValidityIntervalSeconds(),
                    "SignupValidityIntervalSeconds", logger) * 1000)
            {
                throw RestException.createInstance("Portal signup link has expired").setKey("portal.signup-link-expired").log(logger);
            }

            // validate mac
            PortalEmailSigner emailSigner = new PortalEmailSigner(signupRequest.email(), signupRequest.timestamp(),
                    PortalEmailSigner.PORTAL_SIGNUP);

            String calculatedMAC = emailSigner.calculateMAC(userProperties.getClientSecret());

            if (!StringUtils.equals(signupRequest.mac(), calculatedMAC)) {
                throw RestException.createInstance("Checksum is not valid").setKey("portal.checksum-invalid").log(logger);
            }

            if (StringUtils.isNotEmpty(portalProperties.getPassword())) {
                throw RestException.createInstance("Password is already set").setKey("portal.password-already-set").log(logger);
            }
        }
        catch (HierarchicalPropertiesException | AddressException | GeneralSecurityException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }

        return new PortalDTO.PortalSignupValidationResult(signupRequest.email());
    }

    @PostMapping(path = RestPaths.PORTAL_FORGOT_PASSWORD_PATH)
    public void forgotPassword(@RequestParam String email)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.PORTAL_FORGOT_PASSWORD);

            try {
                String validatedEmail = restValidator.validateEmail(email, logger);

                User user = userWorkflow.getUser(validatedEmail,
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

                if (user == null) {
                    throw RestException.createInstance("User with email address %s not found".formatted(validatedEmail))
                            .setKey("portal.user-not-registered").log(logger);
                }

                UserProperties userProperties = user.getUserPreferences().getProperties();

                PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                        .createInstance(userProperties);

                Long passwordResetTimestamp = portalProperties.getPasswordResetTimestamp();

                // check if the last forgot password action was recent
                if (passwordResetTimestamp != null &&
                    (System.currentTimeMillis() - passwordResetTimestamp) < portalProperties.getPasswordResetInterval())
                {
                    throw RestException.createInstance("It's not allowed to start password resets in rapid succession (try again later)")
                            .setKey("portal.password-reset-too-fast").log(logger);
                }

                if (StringUtils.isEmpty(portalProperties.getPasswordResetURL())) {
                    throw RestException.createInstance("Password reset URL is not set")
                            .setKey("portal.password-reset-url-not-set").log(logger);
                }

                String from = userProperties.getSystemFrom();

                if (StringUtils.isEmpty(from)) {
                    throw RestException.createInstance("System From is not set")
                            .setKey("portal.system-from-not-set").log(logger);
                }

                String clientSecret = userProperties.getClientSecret();

                if (StringUtils.isEmpty(clientSecret)) {
                    // should not happen
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Client secret for user %s is not set".formatted(user.getEmail()), logger);
                }

                long timestamp = System.currentTimeMillis();

                portalProperties.setPasswordResetTimestamp(timestamp);

                PortalEmailSigner emailSigner = new PortalEmailSigner(validatedEmail, timestamp,
                        PortalEmailSigner.PORTAL_RESET_PASSWORD);

                SimpleHash templateHash = templateHashModelBuilder.buildSimpleHash();

                templateHash.put("originator", from);
                templateHash.put("from", from);
                templateHash.put("mac", emailSigner.calculateMAC(clientSecret));
                templateHash.put("timestamp", timestamp);
                templateHash.put("recipient", validatedEmail);
                templateHash.put("boundary", "=-" + UUID.randomUUID());

                TemplateProperties templateProperties = userPropertiesFactoryRegistry.getFactoryForClass(
                        TemplatePropertiesImpl.class).createInstance(userProperties);

                ByteArrayOutputStream mime = new ByteArrayOutputStream();

                templateBuilder.buildTemplate(templateProperties.getPortalPasswordResetTemplate(), templateHash, mime);

                MimeMessage message = MailUtils.byteArrayToMessage(mime.toByteArray());

                // Make sure the message has a unique message-ID
                message.saveChanges();

                MailAddress sender = null;

                if (userProperties.getSystemSender() != null)
                {
                    try {
                        sender = new MailAddress(userProperties.getSystemSender());
                    }
                    catch (ParseException e) {
                        // Should normally not happen because settings.from should have been validated
                        logger.error("Invalid address", e);
                    }
                }

                MailImpl mail = MailImpl.builder().name(MailImpl.getId())
                        .sender(sender)
                        .addRecipients(MailAddressUtils.toMailAddressList(email))
                        .mimeMessage(message)
                        .state(forgotPasswordProcessor)
                        .build();

                try {
                    mailEnqueuer.enqueue(mail);
                }
                finally {
                    JamestUtils.dispose(mail);
                }
            }
            catch (HierarchicalPropertiesException | GeneralSecurityException | TemplateBuilderException |
                   MessagingException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH)
    public PortalDTO.PortalPasswordResetValidationResult validatePortalPasswordReset(@RequestParam String resetCode)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_VALIDATE_PORTAL_PASSWORD_RESET);

            return validatePortalPasswordResetInternal(resetCode);
        });
    }

    @PostMapping(path = RestPaths.PORTAL_RESET_PORTAL_PASSWORD_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public PortalDTO.PortalPasswordResetValidationResult resetPortalPassword(
            @RequestParam String resetCode,
            @RequestBody String password)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.PORTAL_RESET_PORTAL_PASSWORD);

            try {
                PortalDTO.PortalPasswordResetValidationResult validationResult = validatePortalPasswordResetInternal(resetCode);

                User user = restValidator.validateNotNull(userWorkflow.getUser(restValidator.validateEmail(
                                validationResult.email(), logger),
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST), "User", logger);

                PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                        .createInstance(user.getUserPreferences().getProperties());

                portalProperties.setPassword(password);
                portalProperties.setPasswordResetTimestamp(null);

                return validationResult;
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    private PortalDTO.PortalPasswordResetValidationResult validatePortalPasswordResetInternal(@RequestParam String resetCode)
    {
        // the signup code is JSON encoded
        PortalDTO.MACProtectedEmail signupRequest;

        try {
            signupRequest = JacksonUtil.getObjectMapper().readValue(Base64Utils.decode(resetCode), PortalDTO.MACProtectedEmail.class);
        }
        catch (Exception e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Reset code is invalid", logger);
        }

        try {
            User user = userWorkflow.getUser(restValidator.validateEmail(signupRequest.email(), logger),
                    UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

            if (user == null) {
                throw RestException.createInstance("User with email address %s not found".formatted(signupRequest.email()))
                        .setKey("portal.user-not-registered").log(logger);
            }

            UserProperties userProperties = user.getUserPreferences().getProperties();

            PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                    .createInstance(userProperties);

            // check if a password reset request is pending
            if (portalProperties.getPasswordResetTimestamp() == null) {
                throw RestException.createInstance("There is no pending password reset")
                        .setKey("portal.no-pending-password-reset").log(logger);
            }

            // check if the signup link expired
            if ((System.currentTimeMillis() - signupRequest.timestamp()) >  restValidator.validateNotNull(
                    portalProperties.getPasswordResetValidityIntervalSeconds(),
                    "PasswordResetValidityIntervalSeconds", logger) * 1000)
            {
                throw RestException.createInstance("Portal password reset link has expired")
                        .setKey("portal.password-reset-link-expired").log(logger);
            }

            // validate mac
            PortalEmailSigner emailSigner = new PortalEmailSigner(signupRequest.email(), signupRequest.timestamp(),
                    PortalEmailSigner.PORTAL_RESET_PASSWORD);

            String calculatedMAC = emailSigner.calculateMAC(userProperties.getClientSecret());

            if (!StringUtils.equals(signupRequest.mac(), calculatedMAC)) {
                throw RestException.createInstance("Checksum is not valid").setKey("portal.checksum-invalid").log(logger);
            }
        }
        catch (HierarchicalPropertiesException | AddressException | GeneralSecurityException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }

        return new PortalDTO.PortalPasswordResetValidationResult(signupRequest.email());
    }

    @PostMapping(path = RestPaths.PORTAL_CHANGE_PORTAL_PASSWORD_PATH)
    public void changePortalPassword(@RequestParam String email, @RequestBody AuthDTO.ChangePassword changePassword)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            try {
                checkPermission(Action.PORTAL_CHANGE_PORTAL_PASSWORD);

                User user = restValidator.validateNotNull(userWorkflow.getUser(restValidator.validateEmail(
                                email, logger),
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST), "User", logger);

                PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                        .createInstance(user.getUserPreferences().getProperties());

                if (changePassword.newPassword().isEmpty()) {
                    throw RestException.createInstance("New password is empty")
                            .setKey("portal.new-password-empty").log(logger);
                }

                if (!changePassword.newPassword().equals(changePassword.newPasswordRepeat())) {
                    throw RestException.createInstance("New password and repeated password do not match")
                            .setKey("portal.password-mismatch").log(logger);
                }

                if (!passwordEncoder.matches(changePassword.currentPassword(), portalProperties.getPassword())) {
                    throw RestException.createInstance("Current password is incorrect")
                            .setKey("portal.current-password-incorrect").log(logger);
                }

                portalProperties.setPassword(changePassword.newPassword());
                portalProperties.setPasswordResetTimestamp(null);
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }
}