/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.james.JamestUtils;
import com.ciphermail.core.app.james.MailEnqueuer;
import com.ciphermail.core.app.james.MailRepositoryStoreProvider;
import com.ciphermail.rest.core.MailDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.MailDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.apache.james.mailrepository.api.MailKey;
import org.apache.james.mailrepository.api.MailRepository;
import org.apache.james.mailrepository.api.MailRepositoryStore;
import org.apache.james.mailrepository.api.MailRepositoryUrl;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
@Tag(name = "MPAMailRepository")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class MailRepositoryController
{
    private static final Logger logger = LoggerFactory.getLogger(MailRepositoryController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MailRepositoryStoreProvider mailRepositoryStoreProvider;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private MailDetailsFactory mailDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private MailEnqueuer mailEnqueuer;

    private enum Action
    {
        GET_MAIL_REPOSITORY_URLS,
        GET_MAIL_KEYS,
        GET_SIZE,
        GET_MAILS,
        GET_MAIL,
        DELETE_MAIL,
        DELETE_MAILS,
        DELETE_ALL_MAIL,
        REQUEUE_MAILS;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.MAIL_REPOSITORY,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.MAIL_REPOSITORY, action.toPermissionName());
    }

    public MailRepositoryController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @GetMapping(RestPaths.MAIL_REPOSITORY_GET_MAIL_REPOSITORY_URLS_PATH)
    public List<String> getMailRepositoryUrls()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_MAIL_REPOSITORY_URLS));

        return mailRepositoryStoreProvider.getMailRepositoryStore().getUrls().map(MailRepositoryUrl::asString).toList();
    }

    @GetMapping(RestPaths.MAIL_REPOSITORY_GET_MAIL_KEYS_PATH)
    public List<String> getMailKeys(@RequestParam String mailRepositoryUrl)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_MAIL_KEYS));

        try {
            Stream<MailKey> mailKeyStream = StreamSupport.stream(
                    Spliterators.spliteratorUnknownSize(getMailRepository(mailRepositoryUrl).list(), Spliterator.ORDERED),
                    false);

            return mailKeyStream.map(MailKey::asString).toList();
        }
        catch (MailRepositoryStore.MailRepositoryStoreException | MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @GetMapping(RestPaths.MAIL_REPOSITORY_GET_SIZE_PATH)
    public long getMailRepositorySize(@RequestParam String mailRepositoryUrl)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_SIZE));

        try {
            return getMailRepository(mailRepositoryUrl).size();
        }
        catch (MailRepositoryStore.MailRepositoryStoreException | MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(RestPaths.MAIL_REPOSITORY_GET_MAILS_PATH)
    public List<MailDTO.MailDetails> getMails(
            @RequestParam String mailRepositoryUrl,
            @RequestBody List<String> keys)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_MAILS));

        List<MailDTO.MailDetails> result = new LinkedList<>();

        try {
            restValidator.limitMaxResults(keys.size(), "number of keys exceed the upper limit %s",
                    logger);

            MailRepository mailRepository = getMailRepository(mailRepositoryUrl);

            for (String key : keys)
            {
                Mail mail = mailRepository.retrieve(new MailKey(key));

                if (mail != null)
                {
                    try {
                        result.add(mailDetailsFactory.toMailDetails(mail));
                    }
                    finally {
                        JamestUtils.dispose(mail);
                    }
                }
            }

            return result;
        }
        catch (MailRepositoryStore.MailRepositoryStoreException | MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @GetMapping(RestPaths.MAIL_REPOSITORY_GET_MAIL_PATH)
    public MailDTO.MailDetailsWithMIME getMail(
            @RequestParam String mailRepositoryUrl,
            @RequestParam String key,
            @RequestParam(defaultValue = "false") boolean includeMIME)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_MAIL));

        try {
            Mail mail = getMailRepository(mailRepositoryUrl).retrieve(new MailKey(key));

            if (mail == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Mail item with key %s for repository URL %s does not exist",
                                key, mailRepositoryUrl), logger);
            }

            try {
                return mailDetailsFactory.toMailDetailsWithMIME(mail, includeMIME);
            }
            finally {
                JamestUtils.dispose(mail);
            }
        }
        catch (MailRepositoryStore.MailRepositoryStoreException | MessagingException | IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(RestPaths.MAIL_REPOSITORY_DELETE_MAIL_PATH)
    public void deleteMail(
            @RequestParam String mailRepositoryUrl,
            @RequestParam String key)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.DELETE_MAIL));

        try {
            getMailRepository(mailRepositoryUrl).remove(new MailKey(key));
        }
        catch (MailRepositoryStore.MailRepositoryStoreException | MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(RestPaths.MAIL_REPOSITORY_DELETE_MAILS_PATH)
    public void deleteMails(
            @RequestParam String mailRepositoryUrl,
            @RequestBody List<String> keys)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.DELETE_MAILS));

        try {
            restValidator.limitMaxResults(keys.size(), "number of keys exceed the upper limit %s",
                    logger);

            getMailRepository(mailRepositoryUrl).remove(keys.stream().map(MailKey::new).toList());
        }
        catch (MailRepositoryStore.MailRepositoryStoreException | MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(RestPaths.MAIL_REPOSITORY_DELETE_ALL_MAIL_PATH)
    public void deleteAllMail(@RequestParam String mailRepositoryUrl)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.DELETE_ALL_MAIL));

        try {
            getMailRepository(mailRepositoryUrl).removeAll();
        }
        catch (MailRepositoryStore.MailRepositoryStoreException | MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(RestPaths.MAIL_REPOSITORY_REQUEUE_MAILS_PATH)
    public void requeueMails(
            @RequestParam String mailRepositoryUrl,
            @RequestParam(required = false) String newState,
            @RequestBody List<String> keys)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.REQUEUE_MAILS));

        try {
            restValidator.limitMaxResults(keys.size(), "number of keys exceed the upper limit %s",
                    logger);

            MailRepository mailRepository = getMailRepository(mailRepositoryUrl);

            newState = StringUtils.trimToNull(newState);

            for (String key : keys)
            {
                Mail mail = mailRepository.retrieve(new MailKey(key));

                if (mail != null)
                {
                    try {
                        mail.setState(newState != null ? newState : Mail.DEFAULT);

                        // enqueue and remove old
                        mailEnqueuer.enqueue(mail);
                        mailRepository.remove(new MailKey(key));
                    }
                    finally {
                        JamestUtils.dispose(mail);
                    }
                }
            }
        }
        catch (MailRepositoryStore.MailRepositoryStoreException | MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    private MailRepository getMailRepository(@RequestParam String mailRepositoryUrl)
    throws MailRepositoryStore.MailRepositoryStoreException
    {
        MailRepository mailRepository = mailRepositoryStoreProvider.getMailRepositoryStore().get(
                MailRepositoryUrl.from(mailRepositoryUrl)).orElse(null);

        if (mailRepository == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Repository URL %s does not exist",
                            mailRepositoryUrl), logger);
        }

        return mailRepository;
    }
}
