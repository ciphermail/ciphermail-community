/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.properties.PropertyValidationFailedException;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.properties.UserPropertiesAccessor;
import com.ciphermail.core.app.properties.UserPropertiesType;
import com.ciphermail.core.app.properties.UserProperty;
import com.ciphermail.core.app.properties.UserPropertyOption;
import com.ciphermail.core.app.properties.UserPropertyRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.PropertyDTO;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@Tag(name = "Property")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PropertyController
{
    private static final Logger logger = LoggerFactory.getLogger(PropertyController.class);

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private UserPropertyRegistry userPropertyRegistry;

    @Autowired
    private UserPropertiesAccessor userPropertiesAccessor;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum PropertyAction
    {GET, SET}

    private enum UserLevel
    {USER, DOMAIN, GLOBAL}

    private static final String GET_PROPERTY_DETAILS_PERMISSION = "get-property-details";

    /*
     * PermissionProvider which will dynamically create a list if permissions based on all properties
     */
    private class PropertyPermissionProvider implements PermissionProvider
    {
        private void addPermission(
                @Nonnull UserLevel level,
                @Nonnull UserProperty property,
                @Nonnull PropertyAction action,
                @Nonnull Set<String> permissions)
        {
            switch (level) {
                case USER:
                    if (property.user()) permissions.add(PermissionUtils.toPermission(
                            PermissionCategory.PROPERTY,
                            action.name(),
                            UserLevel.USER.name(),
                            property.name()));
                    break;
                case DOMAIN:
                    if (property.domain()) permissions.add(PermissionUtils.toPermission(
                            PermissionCategory.PROPERTY,
                            action.name(),
                            UserLevel.DOMAIN.name(),
                            property.name()));
                    break;
                case GLOBAL:
                    if (property.global()) permissions.add(PermissionUtils.toPermission(
                            PermissionCategory.PROPERTY,
                            action.name(),
                            UserLevel.GLOBAL.name(),
                            property.name()));
                    break;
            }
        }

        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Map.Entry<UserPropertiesType, List<UserPropertyRegistry.UserPropertyDescriptor>> entry :
                    userPropertyRegistry.getGroupedProperties().entrySet())
            {
                for (UserPropertyRegistry.UserPropertyDescriptor descriptor : entry.getValue())
                {
                    UserProperty getter = descriptor.getter();
                    UserProperty setter = descriptor.setter();

                    addPermission(UserLevel.USER, getter, PropertyAction.GET, permissions);
                    addPermission(UserLevel.DOMAIN, getter, PropertyAction.GET, permissions);
                    addPermission(UserLevel.GLOBAL, getter, PropertyAction.GET, permissions);

                    addPermission(UserLevel.USER, setter, PropertyAction.SET, permissions);
                    addPermission(UserLevel.DOMAIN, setter, PropertyAction.SET, permissions);
                    addPermission(UserLevel.GLOBAL, setter, PropertyAction.SET, permissions);
                }
            }

            permissions.add(PermissionUtils.toPermission(PermissionCategory.PROPERTY,
                    GET_PROPERTY_DETAILS_PERMISSION));

            return permissions;
        }
    }

    public PropertyController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PropertyPermissionProvider());
    }

    private User getUser(
            @Nonnull String email,
            @Nonnull UserWorkflow.UserNotExistResult userNotExistResult)
    throws HierarchicalPropertiesException, AddressException
    {
        String validatedEmail = restValidator.validateEmail(email, logger);

        User user = userWorkflow.getUser(validatedEmail, userNotExistResult);

        if (user == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with email address %s not found", validatedEmail), logger);
        }

        return user;
    }

    private UserPreferences getDomainPreferences(@Nonnull String domain)
    {
        String validatedDomain = restValidator.validateDomain(domain, DomainUtils.DomainType.WILD_CARD, logger);

        UserPreferences domainPreferences = domainManager.getDomainPreferences(validatedDomain);

        if (domainPreferences == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Domain %s not found", validatedDomain), logger);
        }

        return domainPreferences;
    }

    private UserProperties getParentProperties(UserPreferences userPreferences)
    {
        // the preferences start with global, then wild-card domain etc.
        List<UserPreferences> inherited = new ArrayList<>(userPreferences.getInheritedUserPreferences());

        // get the one before the last
        return inherited.size() - 1 >= 0 ? inherited.get(inherited.size() - 1).getProperties() : null;
    }

    private UserPropertyRegistry.UserPropertyDescriptor getPropertyDescriptor(String propertyName)
    {
        UserPropertyRegistry.UserPropertyDescriptor userPropertyDescriptor = userPropertyRegistry
                .getProperties().get(propertyName);

        if (userPropertyDescriptor == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Property with name %s does not exist", propertyName), logger);
        }

        return userPropertyDescriptor;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_EMAIL_ADDRESS_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "User not found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a user property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @GetMapping(RestPaths.PROPERTY_GET_USER_PROPERTY_PATH)
    public PropertyDTO.PropertyValue getUserProperty(@RequestParam String email, @RequestParam String name) {
        return getUserProperties(email, List.of(name)).get(0);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_EMAIL_ADDRESS_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "User not found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a user property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(RestPaths.PROPERTY_GET_USER_PROPERTIES_PATH)
    public List<PropertyDTO.PropertyValue> getUserProperties(
            @RequestParam String email,
            @RequestBody List<String> names)
    {
        return transactionOperations.execute(ctx ->
        {
            try {
                UserPreferences userPreferences = getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST)
                        .getUserPreferences();

                UserProperties userProperties = userPreferences.getProperties();

                // the last prefs is for the user, the previous is the inherited value
                UserProperties inheritedUserProperties = getParentProperties(userPreferences);

                List<PropertyDTO.PropertyValue> values = new LinkedList<>();

                for (String name : names) {
                    // check if the property exists and is a readable user property
                    UserPropertyRegistry.UserPropertyDescriptor propertyDescriptor = getPropertyDescriptor(name);

                    if (!propertyDescriptor.getter().user()) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Property with name %s is not a user property", name), logger);
                    }

                    // check if REST user is authorized to read the user property
                    permissionChecker.checkPermission(PermissionCategory.PROPERTY, PropertyAction.GET.name(),
                            UserLevel.USER.name(), name);

                    values.add(
                            new PropertyDTO.PropertyValue(
                                    name,
                                    userProperties.getProperty(name),
                                    inheritedUserProperties != null ? inheritedUserProperties.getProperty(name) : null,
                                    userProperties.isInherited(name)));
                }

                return values;
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_EMAIL_ADDRESS_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "User not found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a user property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path = RestPaths.PROPERTY_SET_USER_PROPERTY_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public void setUserProperty(
            @RequestParam String email,
            @RequestParam String name,
            @RequestBody(required = false) String value)
    {
        setUserProperties(email, List.of(new PropertyDTO.PropertyRequest(name, value)));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_EMAIL_ADDRESS_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "User not found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a user property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(RestPaths.PROPERTY_SET_USER_PROPERTIES_PATH)
    public void setUserProperties(
            @RequestParam String email,
            @RequestBody List<PropertyDTO.PropertyRequest> properties)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            try {
                UserProperties userProperties = getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)
                        .getUserPreferences().getProperties();

                for (PropertyDTO.PropertyRequest property : properties)
                {
                    if (property.name() == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Property name for user %s cannot be empty", email), logger);
                    }

                    // check if the property exists and is a writable user property
                    UserPropertyRegistry.UserPropertyDescriptor propertyDescriptor = getPropertyDescriptor(
                            property.name());

                    if (!propertyDescriptor.getter().user()) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Property with name %s is not a user property", property.name()), logger);
                    }

                    // check if REST user is authorized to set the user property
                    permissionChecker.checkPermission(PermissionCategory.PROPERTY, PropertyAction.SET.name(),
                            UserLevel.USER.name(), property.name());

                    userPropertiesAccessor.setProperty(userProperties, property.name(), property.value());
                }
            }
            catch (InvocationTargetException e) {
                // check if this was caused by property validation failure
                if (e.getTargetException() instanceof PropertyValidationFailedException validationFailedException)
                {
                    logger.warn("Property validation failed {}", validationFailedException.toString());

                    throw validationFailedException;
                }
                else {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }
            catch (ConversionException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), logger);
            }
            catch (HierarchicalPropertiesException | AddressException | NoSuchMethodException | IllegalAccessException
                   | InstantiationException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_DOMAIN_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Domain not found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a domain property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @GetMapping(RestPaths.PROPERTY_GET_DOMAIN_PROPERTY_PATH)
    public PropertyDTO.PropertyValue getDomainProperty(
            @RequestParam String domain,
            @RequestParam(required = true) String name)
    {
        return getDomainProperties(domain, List.of(name)).get(0);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_DOMAIN_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Domain not found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a domain property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(RestPaths.PROPERTY_GET_DOMAIN_PROPERTIES_PATH)
    public List<PropertyDTO.PropertyValue> getDomainProperties(
            @RequestParam String domain,
            @RequestBody List<String> names)
    {
        return transactionOperations.execute(ctx ->
        {
            try {
                UserPreferences domainPreferences = getDomainPreferences(domain);

                UserProperties domainProperties = domainPreferences.getProperties();

                // the last prefs is for the user, the previous is the inherited value
                UserProperties inheritedUserProperties = getParentProperties(domainPreferences);

                List<PropertyDTO.PropertyValue> values = new LinkedList<>();

                for (String name : names)
                {
                    // check if the property exists and is a readable domain property
                    UserPropertyRegistry.UserPropertyDescriptor propertyDescriptor = getPropertyDescriptor(name);

                    if (!propertyDescriptor.getter().domain()) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Property with name %s is not a domain property", name), logger);
                    }

                    // check if REST user is authorized to read the domain property
                    permissionChecker.checkPermission(PermissionCategory.PROPERTY, PropertyAction.GET.name(),
                            UserLevel.DOMAIN.name(), name);

                    values.add(
                            new PropertyDTO.PropertyValue(
                                    name,
                                    domainProperties.getProperty(name),
                                    inheritedUserProperties != null ? inheritedUserProperties.getProperty(name) : null,
                                    domainProperties.isInherited(name)));
                }

                return values;
            }
            catch (HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_DOMAIN_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Domain not found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a domain property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path = RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public void setDomainProperty(
            @RequestParam String domain,
            @RequestParam String name,
            @RequestBody(required = false) String value)
    {
        setDomainProperties(domain, List.of(new PropertyDTO.PropertyRequest(name, value)));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_DOMAIN_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Domain not found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a domain property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(RestPaths.PROPERTY_SET_DOMAIN_PROPERTIES_PATH)
    public void setDomainProperties(
            @RequestParam String domain,
            @RequestBody List<PropertyDTO.PropertyRequest> properties)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            try {
                UserProperties domainProperties = getDomainPreferences(domain).getProperties();

                for (PropertyDTO.PropertyRequest property : properties)
                {
                    if (property.name() == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Property name for domain %s cannot be empty", domain), logger);
                    }

                    // check if the property exists and is a writeable domain property
                    UserPropertyRegistry.UserPropertyDescriptor propertyDescriptor = getPropertyDescriptor(
                            property.name());

                    if (!propertyDescriptor.getter().domain()) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Property with name %s is not a domain property", property.name()), logger);
                    }

                    // check if REST user is authorized to set the domain property
                    permissionChecker.checkPermission(PermissionCategory.PROPERTY, PropertyAction.SET.name(),
                            UserLevel.DOMAIN.name(), property.name());

                    userPropertiesAccessor.setProperty(domainProperties, property.name(), property.value());
                }
            }
            catch (InvocationTargetException e) {
                // check if this was caused by property validation failure
                if (e.getTargetException() instanceof PropertyValidationFailedException validationFailedException)
                {
                    logger.warn("Property validation failed {}", validationFailedException.toString());

                    throw validationFailedException;
                }
                else {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }
            catch (ConversionException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), logger);
            }
            catch (NoSuchMethodException | IllegalAccessException | InstantiationException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Property does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a global property",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.PROPERTY_GET_GLOBAL_PROPERTY_PATH)
    public PropertyDTO.PropertyValue getGlobalProperty(@RequestParam String name) {
        return getGlobalProperties(List.of(name)).get(0);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Property does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a global property",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.PROPERTY_GET_GLOBAL_PROPERTIES_PATH)
    public List<PropertyDTO.PropertyValue> getGlobalProperties(@RequestBody List<String> names)
    {
        return transactionOperations.execute(ctx ->
        {
            try {
                UserPreferences globalPreferences = globalPreferencesManager.getGlobalUserPreferences();

                UserProperties globalProperties = globalPreferences.getProperties();

                // the last prefs is for the user, the previous is the inherited value
                UserProperties inheritedUserProperties = getParentProperties(globalPreferences);

                List<PropertyDTO.PropertyValue> values = new LinkedList<>();

                for (String name : names)
                {
                    // check if the property exists and is a readable global property
                    UserPropertyRegistry.UserPropertyDescriptor propertyDescriptor = getPropertyDescriptor(name);

                    if (!propertyDescriptor.getter().global()) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Property with name %s is not a global property", name), logger);
                    }

                    // check if REST user is authorized to read the global property
                    permissionChecker.checkPermission(PermissionCategory.PROPERTY, PropertyAction.GET.name(),
                            UserLevel.GLOBAL.name(), name);

                    values.add(new PropertyDTO.PropertyValue(
                            name,
                            globalProperties.getProperty(name),
                            inheritedUserProperties != null ? inheritedUserProperties.getProperty(name) : null,
                            globalProperties.isInherited(name)));
                }

                return values;
            }
            catch (HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Property does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a global property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path = RestPaths.PROPERTY_SET_GLOBAL_PROPERTY_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public void setGlobalProperty(
            @RequestParam String name,
            @RequestBody(required = false) String value)
    {
        setGlobalProperties(List.of(new PropertyDTO.PropertyRequest(name, value)));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Property does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.METHOD_NOT_ALLOWED_RESPONSE_CODE,
                    description = "Property is not a global property",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(RestPaths.PROPERTY_SET_GLOBAL_PROPERTIES_PATH)
    public void setGlobalProperties(@RequestBody List<PropertyDTO.PropertyRequest> properties)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            try {
                UserProperties globalProperties = globalPreferencesManager.getGlobalUserPreferences().getProperties();

                for (PropertyDTO.PropertyRequest property : properties)
                {
                    // check if the property exists and is a writeable global property
                    UserPropertyRegistry.UserPropertyDescriptor propertyDescriptor = getPropertyDescriptor(
                            property.name());

                    if (!propertyDescriptor.getter().global()) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Property with name %s is not a global property", property.name()), logger);
                    }

                    // check if REST user is authorized to set the domain property
                    permissionChecker.checkPermission(PermissionCategory.PROPERTY, PropertyAction.SET.name(),
                            UserLevel.GLOBAL.name(), property.name());

                    userPropertiesAccessor.setProperty(globalProperties, property.name(), property.value());
                }
            }
            catch (InvocationTargetException e) {
                // check if this was caused by property validation failure
                if (e.getTargetException() instanceof PropertyValidationFailedException validationFailedException)
                {
                    logger.warn("Property validation failed {}", validationFailedException.toString());

                    throw validationFailedException;
                }
                else {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }
            catch (ConversionException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), logger);
            }
            catch (HierarchicalPropertiesException | NoSuchMethodException |
                   IllegalAccessException | InstantiationException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private List<String> getEnumConstantsAsList(PropertyDescriptor propertyDescriptor)
    {
        return Arrays.stream(((Class<Enum<?>>) propertyDescriptor
                .getPropertyType()).getEnumConstants()).map(Enum::name).toList();
    }

    private @Nonnull PropertyDTO.UserPropertyDetails createUserPropertyDetails(
            @Nonnull UserProperty userProperty,
            @Nonnull PropertyDTO.TypeDetails typeDetails,
            @Nonnull PropertyAction action,
            int order)
    {
        String displayNameKey = StringUtils.isNotEmpty(userProperty.displayNameKey()) ? userProperty.displayNameKey()
                : UserProperty.BASE_KEY + userProperty.name() + UserProperty.DISPLAY_NAME_KEY;

        String descriptionKey = StringUtils.isNotEmpty(userProperty.descriptionKey()) ? userProperty.descriptionKey()
                : UserProperty.BASE_KEY + userProperty.name() + UserProperty.DESCRIPTION_KEY;

        String helpKey = StringUtils.isNotEmpty(userProperty.descriptionKey()) ? userProperty.descriptionKey()
                : UserProperty.BASE_KEY + userProperty.name() + UserProperty.HELP_KEY;

        boolean userAccess = userProperty.user();
        boolean domainAccess = userProperty.domain();
        boolean globalAccess = userProperty.global();

        if (userAccess) {
            // check if the REST user is authorized for a user level property
            userAccess = permissionChecker.hasPermission(PermissionCategory.PROPERTY, action.name(),
                    UserLevel.USER.name(), userProperty.name());
        }

        if (domainAccess) {
            // check if the REST user is authorized for a domain level property
            domainAccess = permissionChecker.hasPermission(PermissionCategory.PROPERTY, action.name(),
                    UserLevel.DOMAIN.name(), userProperty.name());
        }

        if (globalAccess) {
            // check if the REST user is authorized for a global level property
            globalAccess = permissionChecker.hasPermission(PermissionCategory.PROPERTY, action.name(),
                    UserLevel.GLOBAL.name(), userProperty.name());
        }

        return new PropertyDTO.UserPropertyDetails(
                userProperty.name(),
                typeDetails,
                userProperty.editor(),
                displayNameKey,
                descriptionKey,
                helpKey,
                userAccess,
                domainAccess,
                globalAccess,
                order);
    }

    private PropertyDTO.UserPropertiesDetails createUserPropertiesDetails(
            @Nonnull UserPropertiesType userPropertiesType,
            @Nonnull List<PropertyDTO.UserPropertyDescriptorDetails> descriptorDetails,
            @Nonnull List<PropertyDTO.UserPropertiesDetails> children)
    {
        String displayNameKey = StringUtils.isNotEmpty(userPropertiesType.displayNameKey()) ?
                userPropertiesType.displayNameKey()
                : UserPropertiesType.BASE_KEY + userPropertiesType.name() + UserPropertiesType.DISPLAY_NAME_KEY;

        String descriptionKey = StringUtils.isNotEmpty(userPropertiesType.descriptionKey()) ?
                userPropertiesType.descriptionKey()
                : UserPropertiesType.BASE_KEY + userPropertiesType.name() + UserPropertiesType.DESCRIPTION_KEY;

        return new PropertyDTO.UserPropertiesDetails(
                userPropertiesType.name(),
                userPropertiesType.displayName(),
                displayNameKey,
                descriptionKey,
                userPropertiesType.order(),
                descriptorDetails,
                children);
    }

    record UserPropertiesTypeWithChildren(UserPropertiesType type, List<UserPropertiesType> children) {}

    private PropertyDTO.UserPropertiesDetails createUserPropertiesDetails(
            Pair<UserPropertiesTypeWithChildren, List<UserPropertyRegistry.UserPropertyDescriptor>> node,
            Map<String, Pair<UserPropertiesTypeWithChildren, List<UserPropertyRegistry.UserPropertyDescriptor>>> allNodes,
            boolean skipInvisible,
            boolean skipUnauthorized)
    {
        List<PropertyDTO.UserPropertyDescriptorDetails> descriptorDetails = new LinkedList<>();

        for (UserPropertyRegistry.UserPropertyDescriptor descriptor : node.getValue())
        {
            UserProperty getter = descriptor.getter();
            UserProperty setter = descriptor.setter();

            // the visible attribute should only be set on the getter
            // log if the setter was changed from the default (i.e., set to false)
            if (getter.visible() && !setter.visible()) {
                logger.warn("visible attribute cannot be changed for a setter {}}", setter.name());
            }

            // if the property is not visible, continue. Invisible properties can be edited (if allowed) with the
            // REST api but will not show up in the GUI
            if (skipInvisible && !getter.visible()) {
                continue;
            }

            PropertyDTO.TypeDetails typeDetails = createTypeDetails(descriptor.propertyDescriptor(), getter, setter);

            // the oder can only be set on a getter
            int order = getter.order();

            // log and ignore if order is set on a setter
            if (order != setter.order() && setter.order() != UserProperty.DEFAULT_ORDER) {
                logger.warn("setter order cannot be set to a different value than the getter order {}", setter.name());
            }

            PropertyDTO.UserPropertyDetails getterDetails = createUserPropertyDetails(getter, typeDetails,
                    PropertyAction.GET, order);

            PropertyDTO.UserPropertyDetails setterDetails = createUserPropertyDetails(setter, typeDetails,
                    PropertyAction.SET, order);

            // if the REST user is not allowed to access any getter/setter properties on all levels, do not
            // return the details
            if (skipUnauthorized && !getterDetails.user() && !getterDetails.domain() && !getterDetails.global()
                && !setterDetails.user() && !setterDetails.domain() && !setterDetails.global()) {
                continue;
            }

            descriptorDetails.add(new PropertyDTO.UserPropertyDescriptorDetails(
                    getterDetails, setterDetails));
        }

        List<PropertyDTO.UserPropertiesDetails> children = new LinkedList<>();

        if (!node.getKey().children().isEmpty())
        {
            for (UserPropertiesType child : node.getKey().children)
            {
                Pair<UserPropertiesTypeWithChildren, List<UserPropertyRegistry.UserPropertyDescriptor>> childNode = allNodes.get(child.name());

                if (childNode == null) {
                    throw new IllegalArgumentException(String.format("UserPropertiesType with name %s not found",
                            child.name()));
                }

                PropertyDTO.UserPropertiesDetails details = createUserPropertiesDetails(childNode, allNodes,
                        skipInvisible, skipUnauthorized);

                if (details != null) {
                    children.add(details);
                }
            }
        }

        // if there are no properties for a category (for example because the user is not authorized), return null
        return skipUnauthorized && descriptorDetails.isEmpty() && children.isEmpty() ? null :
                createUserPropertiesDetails(node.getKey().type, descriptorDetails, children);
    }

    @GetMapping(RestPaths.PROPERTY_GET_AVAILABLE_PROPERTIES_PATH)
    public List<PropertyDTO.UserPropertiesDetails> getAvailableProperties(
            @RequestParam(defaultValue = "true") boolean skipInvisible,
            @RequestParam(defaultValue = "true") boolean skipUnauthorized)
    {
        return transactionOperations.execute(ctx ->
        {
            permissionChecker.checkPermission(PermissionCategory.PROPERTY, GET_PROPERTY_DETAILS_PERMISSION);

            // mapping from name to UserPropertiesType to be used for merging duplicate items and finding parents
            Map<String, Pair<UserPropertiesTypeWithChildren, List<UserPropertyRegistry.UserPropertyDescriptor>>>
                    namedUserPropertiesTypes = new HashMap<>();

            for (Map.Entry<UserPropertiesType, List<UserPropertyRegistry.UserPropertyDescriptor>> entry :
                    userPropertyRegistry.getGroupedProperties().entrySet())
            {
                UserPropertiesType userPropertiesType = entry.getKey();

                if (!userPropertiesType.visible() && skipInvisible) {
                    continue;
                }

                // check if there is already a UserPropertiesType with the same name
                Pair<UserPropertiesTypeWithChildren, List<UserPropertyRegistry.UserPropertyDescriptor>> pair =
                        namedUserPropertiesTypes.get(userPropertiesType.name());

                if (pair != null) {
                    // warn if there are differences
                    if (!pair.getKey().type.descriptionKey().equals(userPropertiesType.descriptionKey())) {
                        logger.warn("Duplicate UserPropertiesType description mismatch: {} != {}",
                                pair.getKey().type.descriptionKey(), userPropertiesType.descriptionKey());
                    }
                    if (!pair.getKey().type.displayNameKey().equals(userPropertiesType.displayNameKey())) {
                        logger.warn("Duplicate UserPropertiesType displayName mismatch: {} != {}",
                                pair.getKey().type.displayNameKey(), userPropertiesType.displayNameKey());
                    }

                    // merge properties
                    entry.getValue().addAll(pair.getValue());
                }
                else {
                    namedUserPropertiesTypes.put(userPropertiesType.name(), new ImmutablePair<>(
                            new UserPropertiesTypeWithChildren(userPropertiesType, new LinkedList<>()),
                            entry.getValue()));
                }
            }

            // step over all nodes and add child's
            for (Pair<UserPropertiesTypeWithChildren, List<UserPropertyRegistry.UserPropertyDescriptor>> entry :
                    namedUserPropertiesTypes.values())
            {
                if (!entry.getKey().type.parent().isEmpty())
                {
                    Pair<UserPropertiesTypeWithChildren, List<UserPropertyRegistry.UserPropertyDescriptor>> parent =
                            namedUserPropertiesTypes.get(entry.getKey().type.parent());

                    if (parent == null) {
                        logger.warn("Parent with name {} not found", entry.getKey().type.parent());
                        continue;
                    }

                    parent.getKey().children.add(entry.getKey().type);
                }
            }

            List<PropertyDTO.UserPropertiesDetails> results = new LinkedList<>();

            // step over all top level nodes
            for (Pair<UserPropertiesTypeWithChildren, List<UserPropertyRegistry.UserPropertyDescriptor>> node :
                    namedUserPropertiesTypes.values())
            {
                if (node.getKey().type.parent().isEmpty())
                {
                    PropertyDTO.UserPropertiesDetails details = createUserPropertiesDetails(
                            node,
                            namedUserPropertiesTypes,
                            skipInvisible,
                            skipUnauthorized);

                    if (details != null) {
                        results.add(details);
                    }
                }
            }

            return results;
        });
    }

    @GetMapping(RestPaths.PROPERTY_GET_USER_PROPERTY_DESCRIPTOR_PATH)
    public PropertyDTO.UserPropertyDescriptorDetails getUserPropertyDescriptor(
            @RequestParam String name)
    {
        return transactionOperations.execute(ctx ->
        {
            permissionChecker.checkPermission(PermissionCategory.PROPERTY, GET_PROPERTY_DETAILS_PERMISSION);

            UserPropertyRegistry.UserPropertyDescriptor descriptor = getPropertyDescriptor(name);

            UserProperty getter = descriptor.getter();
            UserProperty setter = descriptor.setter();

            PropertyDTO.TypeDetails typeDetails = createTypeDetails(descriptor.propertyDescriptor(), getter, setter);

            PropertyDTO.UserPropertyDetails getterDetails = createUserPropertyDetails(getter, typeDetails,
                    PropertyAction.GET, getter.order());

            PropertyDTO.UserPropertyDetails setterDetails = createUserPropertyDetails(setter, typeDetails,
                    PropertyAction.SET, setter.order());

            return new PropertyDTO.UserPropertyDescriptorDetails(
                    getterDetails, setterDetails);
        });
    }

    private PropertyDTO.TypeDetails createTypeDetails(
            @Nonnull PropertyDescriptor propertyDescriptor,
            @Nonnull UserProperty getter,
            @Nonnull UserProperty setter)
    {
        Map<String, Object> options = new HashMap<>();
        String type;

        for (UserPropertyOption option : setter.options()) {
            options.put(option.name(), option.value());
        }

        for (UserPropertyOption option : getter.options()) {
            options.put(option.name(), option.value());
        }

        if (propertyDescriptor.getPropertyType().isEnum())
        {
            options.put(UserPropertyOption.VALUES, getEnumConstantsAsList(propertyDescriptor));
            type = "Enum";
        }
        else {
            type = propertyDescriptor.getPropertyType().getSimpleName();
        }

        return new PropertyDTO.TypeDetails(type, options);
    }
}