/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.rest.core.MailDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

@Component
public class MailDetailsFactory
{
    public MailDTO.MailDetails toMailDetails(@Nonnull Mail mail)
    throws MessagingException
    {
        return new MailDTO.MailDetails(mail.getName(),
                Optional.ofNullable(mail.getRecipients()).orElse(Collections.emptyList())
                        .stream().map(MailAddress::asString).toList(),
                mail.getMaybeSender().asString(),
                StringUtils.defaultString(mail.getState()),
                StringUtils.defaultString(mail.getRemoteHost()),
                StringUtils.defaultString(mail.getRemoteAddr()),
                StringUtils.defaultString(mail.getErrorMessage()),
                mail.getMessageSize(),
                Optional.ofNullable(mail.getLastUpdated()).map(Date::getTime).orElse(0L)
        );
    }

    public MailDTO.MailDetailsWithMIME toMailDetailsWithMIME(@Nonnull Mail mail, boolean includeMIME)
    throws MessagingException, IOException
    {
        return new MailDTO.MailDetailsWithMIME(toMailDetails(mail),
                includeMIME ? MailUtils.partToMimeString(mail.getMessage()) : null
        );
    }
}
