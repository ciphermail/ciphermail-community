/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListEntry;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListException;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@Tag(name = "PGPTrustList")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PGPTrustListController
{
    private static final Logger logger = LoggerFactory.getLogger(PGPTrustListController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPTrustList trustList;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        GET_PGP_TRUST_LIST_ENTRIES,
        GET_PGP_TRUST_LIST_ENTRY,
        GET_PGP_TRUST_LIST_COUNT,
        SET_PGP_TRUST_LIST_ENTRY,
        SET_PGP_TRUST_LIST_ENTRIES,
        DELETE_PGP_TRUST_LIST_ENTRY;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.PGP_TRUST_LIST,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.PGP_TRUST_LIST, action.toPermissionName());
    }

    public PGPTrustListController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @GetMapping(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRIES_PATH)
    public List<PGPDTO.PGPTrustListResult> getPGPTrustListEntries(
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_PGP_TRUST_LIST_ENTRIES);

            try {
                return Optional.ofNullable(trustList.getEntries(firstResult, restValidator.limitMaxResults(
                        maxResults, logger))).orElse(Collections.emptyList())
                        .stream().map(PGPTrustListController::toPGPTrustListResult).toList();
            }
            catch (PGPTrustListException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRY_PATH)
    public PGPDTO.PGPTrustListResult getPGPTrustListEntry(@RequestParam String sha256Fingerprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_PGP_TRUST_LIST_ENTRY);

            try {
                return toPGPTrustListResult(trustList.getEntry(restValidator.validatePGPSha256Fingerprint(
                        sha256Fingerprint, logger)));
            }
            catch (PGPTrustListException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_COUNT_PATH)
    public long getPGPTrustListCount()
    {
        return Optional.ofNullable(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_PGP_TRUST_LIST_COUNT);

            try {
                return trustList.size();
            }
            catch (PGPTrustListException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(0L);
    }

    @PostMapping(RestPaths.PGP_TRUST_LIST_SET_PGP_TRUST_LIST_ENTRY_PATH)
    public void setPGPTrustListEntry(
            @RequestParam String sha256Fingerprint,
            @RequestParam PGPTrustListStatus status,
            @RequestParam(defaultValue = "true") boolean includeSubkeys)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.SET_PGP_TRUST_LIST_ENTRY);

            try {
                trustList.setEntry(createPGPTrustListEntry(restValidator.validatePGPSha256Fingerprint(
                        sha256Fingerprint, logger), status), includeSubkeys);
            }
            catch (PGPTrustListException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(RestPaths.PGP_TRUST_LIST_SET_PGP_TRUST_LIST_ENTRIES_PATH)
    public void setPGPTrustListEntries(
            @RequestBody List<String> sha256Fingerprints,
            @RequestParam PGPTrustListStatus status,
            @RequestParam(defaultValue = "true") boolean includeSubkeys)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.SET_PGP_TRUST_LIST_ENTRIES);

            try {
                for (String sha256Fingerprint : sha256Fingerprints)
                {
                    trustList.setEntry(createPGPTrustListEntry(restValidator.validatePGPSha256Fingerprint(
                            sha256Fingerprint, logger), status), includeSubkeys);
                }
            }
            catch (PGPTrustListException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(RestPaths.PGP_TRUST_LIST_DELETE_PGP_TRUST_LIST_ENTRY_PATH)
    public void deletePGPTrustListEntry(@RequestParam String sha256Fingerprint,
            @RequestParam(defaultValue = "true") boolean includeSubkeys)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.DELETE_PGP_TRUST_LIST_ENTRY);

            try {
                trustList.deleteEntry(getPGPTrustListEntryNonNull(sha256Fingerprint), includeSubkeys);
            }
            catch (PGPTrustListException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    private PGPTrustListEntry getPGPTrustListEntryNonNull(String sha256Fingerprint)
    throws PGPTrustListException
    {
        PGPTrustListEntry entry = trustList.getEntry(restValidator.validatePGPSha256Fingerprint(
                sha256Fingerprint, logger));

        if (entry == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Entry with sha256 fingerprint %s not found", sha256Fingerprint), logger);
        }

        return entry;
    }

    private static PGPDTO.PGPTrustListResult toPGPTrustListResult(PGPTrustListEntry trustListEntry) {
        return trustListEntry != null ? new PGPDTO.PGPTrustListResult(trustListEntry.getSHA256Fingerprint(),
                trustListEntry.getStatus()) : null;
    }

    private PGPTrustListEntry createPGPTrustListEntry(String sha256Fingerprint, PGPTrustListStatus status)
    throws PGPTrustListException
    {
        PGPTrustListEntry entry = trustList.createEntry(sha256Fingerprint);

        entry.setStatus(status);

        return entry;
    }
}
