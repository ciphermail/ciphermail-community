/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.rest.core.KeyStoreDTO;
import com.ciphermail.rest.core.KeyStoreName;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@Tag(name = "KeyStore")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class KeyStoreController
{
    private static final Logger logger = LoggerFactory.getLogger(KeyStoreController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier("keyStoreProvider")
    private KeyStoreProvider certKeyStoreProvider;

    @Autowired
    @Qualifier("pgpKeyStoreProvider")
    private KeyStoreProvider pgpKeyStoreProvider;

    @Autowired
    @Qualifier("dkimKeyStoreProvider")
    private KeyStoreProvider dkimKeyStoreProvider;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private X509CertificateDetailsFactory x509CertificateDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        GET_ALIASES,
        GET_KEYSTORE_SIZE,
        GET_CERTIFICATE,
        GET_CERTIFICATE_CHAIN,
        CONTAINS_ALIAS,
        IS_CERTIFICATE_ENTRY,
        IS_KEY_ENTRY,
        DELETE_ENTRY,
        RENAME_ALIAS,
        EXPORT_TO_PKCS12,
        IMPORT_PKCS12;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.KEYSTORE,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.KEYSTORE, action.toPermissionName());
    }

    public KeyStoreController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    private KeyStoreProvider getKeyStoreProvider(@Nonnull KeyStoreName keyStoreName)
    {
        return switch (keyStoreName) {
            case CERT -> certKeyStoreProvider;
            case PGP  -> pgpKeyStoreProvider;
            case DKIM -> dkimKeyStoreProvider;
        };
    }

    private KeyStoreDTO.CertificateDetails toCertificateDetails(@Nonnull KeyStoreProvider keyStoreProvider,
            Certificate certificate, String alias)
    throws CertificateEncodingException, IOException
    {
        return certificate != null ? new KeyStoreDTO.CertificateDetails(certificate.getEncoded(), certificate.getType(),
                certificate.toString(), certificate instanceof X509Certificate x509Certificate ?
                        x509CertificateDetailsFactory.createX509CertificateDetails(keyStoreProvider,
                                x509Certificate, alias) : null) : null;
    }

    @GetMapping(RestPaths.KEYSTORE_GET_ALIASES_PATH)
    public List<String> getAliases(
            @RequestParam KeyStoreName keyStoreName,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_ALIASES);

            try {
                return Collections.list(getKeyStoreProvider(keyStoreName).getKeyStore().aliases()).stream()
                        .skip(firstResult).limit(restValidator.limitMaxResults(maxResults, logger)).toList();
            }
            catch (KeyStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.KEYSTORE_GET_KEYSTORE_SIZE_PATH)
    public int getKeyStoreSize(@RequestParam KeyStoreName keyStoreName)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_KEYSTORE_SIZE);

            try {
                return getKeyStoreProvider(keyStoreName).getKeyStore().size();
            }
            catch (KeyStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(-1);
    }

    @GetMapping(RestPaths.KEYSTORE_GET_CERTIFICATE_PATH)
    public KeyStoreDTO.CertificateDetails getCertificate(@RequestParam KeyStoreName keyStoreName,
            @RequestParam String alias)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CERTIFICATE);

            try {
                return toCertificateDetails(getKeyStoreProvider(keyStoreName),
                        getKeyStoreProvider(keyStoreName).getKeyStore().getCertificate(alias), alias);
            }
            catch (KeyStoreException | CertificateEncodingException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.KEYSTORE_GET_CERTIFICATE_CHAIN_PATH)
    public List<KeyStoreDTO.CertificateDetails> getCertificateChain(@RequestParam KeyStoreName keyStoreName,
            @RequestParam String alias)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CERTIFICATE_CHAIN);

            try {
                Certificate[] certificates = getKeyStoreProvider(keyStoreName).getKeyStore().getCertificateChain(alias);

                return Arrays.stream(Optional.ofNullable(certificates).orElse(new Certificate[]{})).map(certificate ->
                {
                    try {
                        return toCertificateDetails(getKeyStoreProvider(keyStoreName), certificate, alias);
                    }
                    catch (CertificateEncodingException | IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                    }
                }).toList();
            }
            catch (KeyStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.KEYSTORE_CONTAINS_ALIAS_PATH)
    public boolean containsAlias(@RequestParam KeyStoreName keyStoreName, @RequestParam String alias)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.CONTAINS_ALIAS);

            try {
                return getKeyStoreProvider(keyStoreName).getKeyStore().containsAlias(alias);
            }
            catch (KeyStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(false);
    }

    @GetMapping(RestPaths.KEYSTORE_IS_CERTIFICATE_ENTRY_PATH)
    public boolean isCertificateEntry(@RequestParam KeyStoreName keyStoreName, @RequestParam String alias)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.IS_CERTIFICATE_ENTRY);

            try {
                return getKeyStoreProvider(keyStoreName).getKeyStore().isCertificateEntry(alias);
            }
            catch (KeyStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(false);
    }

    @GetMapping(RestPaths.KEYSTORE_IS_KEY_ENTRY_PATH)
    public boolean isKeyEntry(@RequestParam KeyStoreName keyStoreName, @RequestParam String alias)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.IS_KEY_ENTRY);

            try {
                return getKeyStoreProvider(keyStoreName).getKeyStore().isKeyEntry(alias);
            }
            catch (KeyStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(false);
    }

    @PostMapping(RestPaths.KEYSTORE_DELETE_ENTRY_PATH)
    public void deleteEntry(@RequestParam KeyStoreName keyStoreName, @RequestParam String alias)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_ENTRY);

            try {
                getKeyStoreProvider(keyStoreName).getKeyStore().deleteEntry(alias);
            }
            catch (KeyStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(RestPaths.KEYSTORE_RENAME_ALIAS_PATH)
    public void renameEntry(
            @RequestParam KeyStoreName keyStoreName,
            @RequestParam String oldAlias,
            @RequestParam String newAlias,
            @RequestBody KeyStoreDTO.RenameEntryRequestBody requestBody)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.RENAME_ALIAS);

            try {
                KeyStore keyStore = getKeyStoreProvider(keyStoreName).getKeyStore();

                KeyStore.Entry entry = getEntryWithAlias(oldAlias, keyStore,
                        keyStore.isKeyEntry(oldAlias) ? StringUtils.defaultString(requestBody.keyEntryPassword()) : null);

                if (entry instanceof KeyStore.PrivateKeyEntry privateKeyEntry) {
                    // private key entry
                    keyStore.setKeyEntry(newAlias, privateKeyEntry.getPrivateKey(),
                            StringUtils.defaultString(requestBody.keyEntryPassword()).toCharArray(),
                            privateKeyEntry.getCertificateChain());
                }
                else if (entry instanceof KeyStore.TrustedCertificateEntry trustedCertificateEntry) {
                    // certificate entry
                    keyStore.setCertificateEntry(newAlias, trustedCertificateEntry.getTrustedCertificate());
                }
                else {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                            String.format("Entry with alias %s is a non-supported entry type %s", oldAlias,
                                    entry.getClass()), logger);
                }

                keyStore.deleteEntry(oldAlias);
            }
            catch (UnrecoverableEntryException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Key with alias %s cannot be decrypted (check password)", oldAlias), logger);
            }
            catch (KeyStoreException | NoSuchAlgorithmException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(RestPaths.KEYSTORE_EXPORT_TO_PKCS12_PATH)
    public byte[] exportToPKCS12(
            @RequestParam KeyStoreName keyStoreName,
            @RequestBody KeyStoreDTO.ExportToPKCS12RequestBody requestBody)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.EXPORT_TO_PKCS12);

            try {
                if (requestBody.aliases().isEmpty()) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "No aliases specified", logger);
                }

                KeyStore exportKeyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

                exportKeyStore.load(null);

                KeyStore keyStore = getKeyStoreProvider(keyStoreName).getKeyStore();

                for (String alias : requestBody.aliases())
                {
                     KeyStore.Entry entry = getEntryWithAlias(alias, keyStore, keyStore.isKeyEntry(alias) ?
                             StringUtils.defaultString(requestBody.keyStorePassword()) : null);

                     if (entry instanceof KeyStore.PrivateKeyEntry privateKeyEntry) {
                         // private key entry
                         exportKeyStore.setKeyEntry(alias, privateKeyEntry.getPrivateKey(),
                                 StringUtils.defaultString(requestBody.exportPassword())
                                         .toCharArray(), privateKeyEntry.getCertificateChain());
                     }
                     else if (entry instanceof KeyStore.TrustedCertificateEntry trustedCertificateEntry) {
                         // certificate entry
                         exportKeyStore.setCertificateEntry(alias, trustedCertificateEntry.getTrustedCertificate());
                     }
                }

                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                exportKeyStore.store(bos, StringUtils.defaultString(requestBody.exportPassword()).toCharArray());

                return bos.toByteArray();
            }
            catch (KeyStoreException | NoSuchProviderException | IOException | NoSuchAlgorithmException |
                   CertificateException | UnrecoverableEntryException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(path = RestPaths.KEYSTORE_IMPORT_PKCS12_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public int importPKCS12(
            @RequestParam KeyStoreName keyStoreName,
            @RequestPart(required = false) String keyStorePassword,
            @RequestBody MultipartFile pkcs12)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.IMPORT_PKCS12));

        KeyStore importKeyStore;

        try {
            SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

            importKeyStore = securityFactory.createKeyStore("PKCS12");

            try(InputStream pkcs12InputStream = pkcs12.getInputStream()) {
                importKeyStore.load(pkcs12InputStream, StringUtils.defaultString(keyStorePassword).toCharArray());
            }
            catch (CertificateException | NoSuchAlgorithmException | IOException e) {
                String errorMessage = e.getMessage();

                if (errorMessage == null) {
                    errorMessage = "Error loading PKCS#12";
                }

                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        errorMessage, e, logger);
            }

            if (importKeyStore.size() == 0) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "The request does not contain any keys or certificates", logger);
            }
        }
        catch (KeyStoreException | NoSuchProviderException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }

        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            int importCount = 0;

            try {
                for (String alias : Collections.list(importKeyStore.aliases()))
                {
                    KeyStore.Entry entry = getEntryWithAlias(alias, importKeyStore,
                            importKeyStore.isKeyEntry(alias) ? StringUtils.defaultString(keyStorePassword) : null);

                    if (entry instanceof KeyStore.PrivateKeyEntry privateKeyEntry) {
                        // private key entry
                        getKeyStoreProvider(keyStoreName).getKeyStore().setKeyEntry(alias, privateKeyEntry.getPrivateKey(),
                                StringUtils.defaultString(keyStorePassword).toCharArray(),
                                privateKeyEntry.getCertificateChain());
                        importCount++;
                    }
                    else if (entry instanceof KeyStore.TrustedCertificateEntry trustedCertificateEntry) {
                        // certificate entry
                        getKeyStoreProvider(keyStoreName).getKeyStore().setCertificateEntry(alias,
                                trustedCertificateEntry.getTrustedCertificate());
                        importCount++;
                    }
                }

                return importCount;
            }
            catch (KeyStoreException | UnrecoverableEntryException | NoSuchAlgorithmException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(0);
    }

    private KeyStore.Entry getEntryWithAlias(String alias, KeyStore keyStore, String password)
    throws KeyStoreException, UnrecoverableEntryException, NoSuchAlgorithmException
    {
        KeyStore.Entry entry = keyStore.getEntry(
                alias, password != null ? new KeyStore.PasswordProtection(password.toCharArray()): null);

        if (entry == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Entry with alias %s does not exist", alias), logger);
        }

        return entry;
    }
}
