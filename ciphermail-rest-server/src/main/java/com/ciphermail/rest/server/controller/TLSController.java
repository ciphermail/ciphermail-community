/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.csr.CSRStore;
import com.ciphermail.core.common.csr.CSRStoreEntry;
import com.ciphermail.core.common.security.PublicKeyInspector;
import com.ciphermail.core.common.security.SSLUtils;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.TLSKeyPairBuilder;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certpath.PKIXCertificatePathBuilder;
import com.ciphermail.core.common.security.certstore.CertStoreUtils;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.core.TLSDTO;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.openssl.MiscPEMGenerator;
import org.bouncycastle.openssl.PEMEncryptor;
import org.bouncycastle.openssl.jcajce.JcaMiscPEMGenerator;
import org.bouncycastle.openssl.jcajce.JcePEMEncryptorBuilder;
import org.bouncycastle.util.io.pem.PemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@Tag(name = "TLS")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class TLSController
{
    private static final Logger logger = LoggerFactory.getLogger(TLSController.class);

    @Value("${ciphermail.rest.controller.csr.export-encryption-algorithm:AES-256-CBC}")
    private String exportEncryptionAlgorithm;

    /*
     * The file for a private key and cert (in PEM format) for TLS protection of the web GUI and SMTP server
     */
    @Value("${ciphermail.rest.controller.csr.private-key-path:${ciphermail.tls.private-key-path}}")
    private File tlsPrivateKeyPath;

    /*
     * Optional file containing trusted root certificates
     */
    @Value("${ciphermail.rest.controller.csr.additional-trust-anchors-path:${ciphermail.tls.additional-trust-anchors-path:#{null}}}")
    private File tlsAdditionalTrustAnchorsPath;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private CSRStore csrStore;

    private enum Action
    {
        CREATE_CSR,
        GET_CSRS,
        GET_CSR,
        DELETE_CSR,
        EXPORT_PKCS10,
        IMPORT_CERTIFICATE_CHAIN,
        EXPORT_CERTIFICATE_CHAIN,
        INSTALL_CERTIFICATE,
        IMPORT_PKCS12;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.TLS,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.TLS, action.toPermissionName());
    }

    public TLSController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @PostMapping(RestPaths.TLS_CREATE_CSR_PATH)
    public TLSDTO.CSRStoreEntry createCSR(
            @RequestBody TLSDTO.Request request,
            @RequestParam TLSDTO.KeyAlgorithm keyAlgorithm)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.CREATE_CSR);

            try {
                List<String> domains = Optional.ofNullable(request.getDomains()).orElse(new LinkedList<>())
                        .stream().filter(domain -> !domain.isBlank()).map(String::trim).toList();

                domains.forEach(domain -> restValidator.validateDomain(domain,
                        DomainUtils.DomainType.FULLY_QUALIFIED, logger));

                return toCSRStoreEntryDTO(csrStore.generateCSR(
                        domains,
                        X500PrincipalBuilder.getInstance()
                                .setEmail(request.getEmail())
                                .setOrganisation(request.getOrganisation())
                                .setOrganisationalUnit(request.getOrganisationalUnit())
                                .setCountryCode(request.getCountryCode())
                                .setState(request.getState())
                                .setLocality(request.getLocality())
                                .setCommonName(request.getCommonName())
                                .setGivenName(request.getGivenName())
                                .setSurname(request.getSurname()).buildName(),
                        TLSKeyPairBuilder.Algorithm.valueOf(keyAlgorithm.name())));
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @GetMapping(RestPaths.TLS_GET_CSRS_PATH)
    public List<TLSDTO.CSRStoreEntry> getCSRs()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CSRS);

            try {
                return csrStore.getCSRs().stream().map(TLSController::toCSRStoreEntryDTO).toList();
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @GetMapping(RestPaths.TLS_GET_CSR_PATH)
    public TLSDTO.CSRStoreEntry getCSR(@RequestParam String id)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CSR);

            try {
                return toCSRStoreEntryDTO(getCSRStoreEntry(id));
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @PostMapping(RestPaths.TLS_DELETE_CSR_PATH)
    public void deleteCSR(@RequestParam String id)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_CSR);

            try {
                csrStore.deleteCSR(getCSRStoreEntry(id).getID());
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @GetMapping(RestPaths.TLS_EXPORT_PKCS10_REQUEST_PATH)
    public String exportPKCS10Request(@RequestParam String id)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.EXPORT_PKCS10);

            try {
                StringWriter pem = new StringWriter();

                try (PemWriter pemWriter = new PemWriter(pem)) {
                    pemWriter.writeObject(new MiscPEMGenerator(getCSRStoreEntry(id).getPKCS10Request()));
                }

                return pem.toString();
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @PostMapping(path= RestPaths.TLS_IMPORT_CERTIFICATE_CHAIN_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public TLSDTO.CSRStoreEntry importCertificateChain(@RequestBody MultipartFile chain)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.IMPORT_CERTIFICATE_CHAIN);

            try(InputStream chainInputStream = chain.getInputStream())
            {
                List<X509Certificate> certificates = CertificateUtils.readX509Certificates(chainInputStream);

                if (certificates.isEmpty()) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "No certificates in request", logger);
                }

                X509Certificate endUserCertificate = null;

                // check if there is a matching csr
                for (X509Certificate certificate : certificates)
                {
                    CSRStoreEntry importedEntry = csrStore.findCSR(certificate);

                    if (importedEntry != null)
                    {
                        endUserCertificate = certificate;

                        break;
                    }
                }

                if (endUserCertificate == null) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "No matching CSR found", logger);
                }

                // Only accept the certificate is it is valid for SSL
                if (!SSLUtils.isValidSSLServerCertificate(endUserCertificate))
                {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Certificate is not valid for TLS/SSL", logger);
                }

                CertPathBuilderResult certPathBuilderResult = buildPath(certificates, endUserCertificate);

                return toCSRStoreEntryDTO(csrStore.importCertificate(CollectionUtils.toArray(
                        CertificateUtils.getX509Certificates(certPathBuilderResult.getCertPath().getCertificates()),
                        X509Certificate.class)));
            }
            catch (IOException | GeneralSecurityException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @PostMapping(RestPaths.TLS_EXPORT_CERTIFICATE_CHAIN_PATH)
    public String exportCertificateChain(@RequestParam String id, @RequestBody String exportPassword)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.EXPORT_CERTIFICATE_CHAIN);

            return getCertificateChain(id, exportPassword);
        });
    }

    @PostMapping(RestPaths.TLS_INSTALL_CERTIFICATE_PATH)
    public void installCertificate(@RequestParam String id)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.INSTALL_CERTIFICATE);

            try {
                FileUtils.write(tlsPrivateKeyPath, getCertificateChain(id, null), StandardCharsets.UTF_8);
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        e.getMessage(), e, logger);
            }
        });
    }

    @PostMapping(path = RestPaths.TLS_IMPORT_PKCS12_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public void importPKCS12(
            @RequestBody MultipartFile pkcs12,
            @RequestPart(required = false) String keyStorePassword)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.IMPORT_PKCS12));

        KeyStore keyStore;

        try {
            SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

            keyStore = securityFactory.createKeyStore("PKCS12");

            try(InputStream pkcs12InputStream = pkcs12.getInputStream()) {
                keyStore.load(pkcs12InputStream, StringUtils.defaultString(keyStorePassword).toCharArray());
            }
            catch (CertificateException | NoSuchAlgorithmException | IOException e) {
                String errorMessage = e.getMessage();

                if (errorMessage == null) {
                    errorMessage = "Error loading PKCS#12";
                }

                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        errorMessage, e, logger);
            }

            if (keyStore.size() == 0) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "The request does not contain any keys or certificates", logger);
            }

            Enumeration<String> aliases = keyStore.aliases();

            KeyStore.PrivateKeyEntry privateKeyEntry = null;
            List<X509Certificate> additionalCertificates = new LinkedList<>();

            while (aliases.hasMoreElements())
            {
                String alias = aliases.nextElement();

                KeyStore.PasswordProtection protection = keyStore.isKeyEntry(alias) ? new KeyStore.PasswordProtection(
                        StringUtils.defaultString(keyStorePassword).toCharArray()) : null;

                KeyStore.Entry entry = keyStore.getEntry(alias, protection);

                if (entry instanceof KeyStore.PrivateKeyEntry localPrivateKeyEntry)
                {
                    Certificate certificate = localPrivateKeyEntry.getCertificate();

                    if (!(certificate instanceof X509Certificate)) {
                        continue;
                    }

                    // Only accept the certificate is it is valid for SSL
                    if (!SSLUtils.isValidSSLServerCertificate((X509Certificate) certificate))
                    {
                        logger.warn("Certificate is not valid for TLS/SSL");

                        continue;
                    }

                    if (privateKeyEntry != null) {
                        logger.warn("More than one private keys found (using last one)");
                    }

                    privateKeyEntry = localPrivateKeyEntry;

                    // add chain
                    additionalCertificates.addAll(List.of(
                            CertificateUtils.getX509Certificates(privateKeyEntry.getCertificateChain())));
                }
                else if (entry instanceof KeyStore.TrustedCertificateEntry trustedCertificateEntry)
                {
                    Certificate certificate = trustedCertificateEntry.getTrustedCertificate();

                    if (certificate instanceof X509Certificate x509Certificate) {
                        additionalCertificates.add(x509Certificate);
                    }
                }
            }

            if (privateKeyEntry == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "The PKCS#12 file does not contain any private keys", logger);
            }

            CertPathBuilderResult certPathBuilderResult = buildPath(additionalCertificates,
                    (X509Certificate) privateKeyEntry.getCertificate());

            StringWriter pem = new StringWriter();

            try (PemWriter pemWriter = new PemWriter(pem))
            {
                pemWriter.writeObject(new JcaMiscPEMGenerator(privateKeyEntry.getCertificate()));
                pemWriter.writeObject(new JcaMiscPEMGenerator(privateKeyEntry.getPrivateKey(), null));

                List<? extends Certificate> certificates = certPathBuilderResult.getCertPath().getCertificates();

                if (certificates.isEmpty()) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Chain is empty", logger);
                }

                Iterator<? extends Certificate> certificateIterator = certificates.iterator();

                // skip first cert (end-user)
                certificateIterator.next();

                for (int i = 1; i < certificates.size(); i++) {
                    pemWriter.writeObject(new JcaMiscPEMGenerator(certificates.get(i)));
                }
            }

            FileUtils.write(tlsPrivateKeyPath, pem.toString(), StandardCharsets.UTF_8);
        }
        catch (GeneralSecurityException | IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    private @Nonnull String getCertificateChain(@Nonnull String id, String password)
    {
        PEMEncryptor encryptor = password != null ? new JcePEMEncryptorBuilder(exportEncryptionAlgorithm).build(
                password.toCharArray()) : null;

        try {
            StringWriter pem = new StringWriter();

            try (PemWriter pemWriter = new PemWriter(pem))
            {
                CSRStoreEntry entry = getCSRStoreEntry(id);

                X509Certificate[] chain = entry.getCertificateChain();

                if (chain == null || chain.length == 0)  {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            String.format("CSR with id %s does not contain any certificates", id), logger);
                }

                pemWriter.writeObject(new JcaMiscPEMGenerator(chain[0]));
                pemWriter.writeObject(new JcaMiscPEMGenerator(entry.getKeyPair().getPrivate(), encryptor));

                for (int i = 1; i < chain.length; i++) {
                    pemWriter.writeObject(new JcaMiscPEMGenerator(chain[i]));
                }
            }

            return pem.toString();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    e.getMessage(), e, logger);
        }
    }

    private @Nonnull CSRStoreEntry getCSRStoreEntry(@Nonnull String id)
    throws IOException
    {
        CSRStoreEntry entry = csrStore.getCSR(id);

        if (entry == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("CSR with id %s not found", id), logger);
        }

        return entry;
    }

    private static TLSDTO.CSRStoreEntry toCSRStoreEntryDTO(@Nonnull CSRStoreEntry entry)
    {
        return new TLSDTO.CSRStoreEntry(
                entry.getID(),
                Optional.ofNullable(entry.getDate()).map(Date::getTime).orElse(0L),
                String.valueOf(entry.getPKCS10Request().getSubject()),
                PublicKeyInspector.getFriendlyAlgorithm(Optional.ofNullable(entry.getKeyPair()).map(KeyPair::getPublic).orElse(null)),
                PublicKeyInspector.getKeyLength(Optional.ofNullable(entry.getKeyPair()).map(KeyPair::getPublic).orElse(null)),
                entry.getCertificateChain() != null);
    }

    /*
     * Try to build a path against the system roots (and optional additional roots)
     */
    private CertPathBuilderResult buildPath(
            @Nonnull List<X509Certificate> certificates,
            @Nonnull X509Certificate endUserCertificate)
    throws GeneralSecurityException, FileNotFoundException
    {
        // build path against the system trust anchors to check whether the chain is complete
        PKIXCertificatePathBuilder pathBuilder = new PKIXCertificatePathBuilder();

        Set<TrustAnchor> trustAnchors = new HashSet<>(SSLUtils.getSystemTrustedRoots().stream().map(certificate ->
                new TrustAnchor(certificate, null)).toList());

        if (tlsAdditionalTrustAnchorsPath != null)
        {
            trustAnchors.addAll(CertificateUtils.readX509Certificates(tlsAdditionalTrustAnchorsPath)
                    .stream().map(certificate -> new TrustAnchor(certificate, null)).toList());
        }

        pathBuilder.setTrustAnchors(trustAnchors);

        pathBuilder.addCertStore(CertStoreUtils.createCertStore(certificates));

        return pathBuilder.buildPath(endUserCertificate);
    }
}
