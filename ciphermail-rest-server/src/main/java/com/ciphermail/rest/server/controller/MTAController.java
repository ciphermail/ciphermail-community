/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailTransport;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.MimeMessageBuilder;
import com.ciphermail.core.common.mail.MimeUtils;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.core.common.util.InputStreamResource;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.postfix.MapType;
import com.ciphermail.postfix.PostCat;
import com.ciphermail.postfix.PostConf;
import com.ciphermail.postfix.PostQueue;
import com.ciphermail.postfix.PostSuper;
import com.ciphermail.postfix.PostfixConfigureMain;
import com.ciphermail.postfix.PostfixCtl;
import com.ciphermail.postfix.PostfixMap;
import com.ciphermail.postfix.PostfixMapManager;
import com.ciphermail.postfix.PostfixUtils;
import com.ciphermail.rest.core.MTADTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.authorization.Permissions;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Encoding;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Nonnull;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@Tag(name = "MTA")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class MTAController
{
    private static final Logger logger = LoggerFactory.getLogger(MTAController.class);

    private static final String POSTFIX_MY_HOSTNAME = "ciphermail_myhostname";
    private static final String POSTFIX_MY_NETWORKS = "ciphermail_mynetworks";
    private static final String POSTFIX_EXTERNAL_RELAY_HOST = "ciphermail_external_relay_host";
    private static final String POSTFIX_EXTERNAL_RELAY_HOST_MX_LOOKUP = "ciphermail_external_relay_host_mx_lookup";
    private static final String POSTFIX_EXTERNAL_RELAY_HOST_PORT = "ciphermail_external_relay_host_port";
    private static final String POSTFIX_RELAY_DOMAINS = "ciphermail_relay_domains";
    private static final String POSTFIX_MESSAGE_SIZE_LIMIT = "ciphermail_before_filter_message_size_limit";
    private static final String POSTFIX_CALCULATED_AFTER_FILTER_MESSAGE_SIZE_LIMIT = "ciphermail_calculated_after_filter_message_size_limit";
    private static final String POSTFIX_CALCULATED_QUEUE_MIN_FREE = "ciphermail_calculated_queue_minfree";
    private static final String POSTFIX_CALCULATED_AFTER_FILTER_QUEUE_MIN_FREE = "ciphermail_calculated_after_filter_queue_minfree";
    private static final String POSTFIX_SMTP_HELO_NAME = "ciphermail_smtp_helo_name";
    private static final String POSTFIX_INTERNAL_RELAY_HOST = "ciphermail_internal_relay_host";
    private static final String POSTFIX_INTERNAL_RELAY_HOST_MX_LOOKUP = "ciphermail_internal_relay_host_mx_lookup";
    private static final String POSTFIX_INTERNAL_RELAY_HOST_PORT = "ciphermail_internal_relay_host_port";
    private static final String POSTFIX_REJECT_UNVERIFIED_RECIPIENT = "ciphermail_reject_unverified_recipient";
    private static final String POSTFIX_UNVERIFIED_RECIPIENT_REJECT_CODE = "ciphermail_unverified_recipient_reject_code";

    /*
     * The mail size limit for email after filter (after queue) will be set to a multiple of the "before queue" size
     * because a decrypted/encrypted etc. email might grow in size.
     */
    @Value("${ciphermail.rest.controller.mta.after-filter-size-multiplier:2}")
    private int afterFilterSizeMultiplier;

    /*
     * The minimal amount of free space in bytes in the queue file system that is needed to receive mail will be set
     * to a multiple of the max message size
     */
    @Value("${ciphermail.rest.controller.mta.queue-min-free-multiplier:20}")
    private int queueMinFreeMultiplier;

    /*
     * The minimal amount of free space in bytes in the queue file system that is needed to receive mail will be set
     * to a multiple of the max message size. This is for the after queue filter.
     * Note: this should be less than queueMinFreeMultiplier
     */
    @Value("${ciphermail.rest.controller.mta.after-filter-queue-min-free-multiplier:10}")
    private int afterFilterQueueMinFreeMultiplier;

    /*
     * The max raw message size returned. If the message size exceeds the max, the content will be truncated
     */
    @Value("${ciphermail.rest.controller.mta.max-message-size:1000000}")
    private int maxMessageSize;

    /*
     * The SMTP port for email which should be delivered without going through the MPA
     */
    @Value("${ciphermail.rest.controller.mta.direct-delivery-port:10026}")
    private int directDeliveryPort;

    /*
     * The SMTP port for email which should be delivered via the MPA
     */
    @Value("${ciphermail.rest.controller.mta.mpa-port:10027}")
    private int mpaPort;

    /*
     * The SMTP host for sending email
     */
    @Value("${ciphermail.rest.controller.mta.smtp-host:127.0.0.1}")
    private String smtpHost;

    /*
     * The max number of chars MIME check uses to determine whether the input is  MIME
     */
    @Value("${ciphermail.rest.controller.mta.mime-check-max-chars:128000}")
    private int mimeCheckMaxChars;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PostQueue postqueue;

    @Autowired
    private PostSuper postsuper;

    @Autowired
    private PostCat postcat;

    @Autowired
    private PostfixConfigureMain postfixConfigureMain;

    @Autowired
    private PostfixCtl postfixCtl;

    @Autowired
    private PostConf postConf;

    @Autowired
    private PostfixMapManager postfixMapManager;

    @Autowired
    private MailTransport mailTransport;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private Permissions permissions;

    private enum Action
    {
        GET_QUEUE_LIST,
        GET_QUEUE_SIZE,
        FLUSH_QUEUE,
        RESCHEDULE_DEFERRED_MAIL,
        DELETE_MAIL,
        HOLD_MAIL,
        RELEASE_MAIL,
        BOUNCE_MAIL,
        REQUEUE_MAIL,
        GET_MAIL,
        GET_MAIN_CONFIG,
        SET_MAIN_CONFIG,
        START_POSTFIX,
        STOP_POSTFIX,
        RESTART_POSTFIX,
        GET_POSTFIX_STATUS,
        IS_POSTFIX_RUNNING,
        SET_STANDARD_SETTINGS,
        GET_STANDARD_SETTINGS,
        MAP_LIST,
        MAP_MANAGE,
        SEND_EMAIL_MIME,
        SEND_EMAIL;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    /*
     * Actions for managing map contents
     */
    private enum MapContentAction
    {
        MAP_GET_CONTENT,
        MAP_SET_CONTENT;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> mapPermissions = new HashSet<>();

            for (Action action : Action.values())
            {
                // get all static permissions
                mapPermissions.add(PermissionUtils.toPermission(PermissionCategory.MTA,
                        action.toPermissionName()));
            }

            // add "dynamic" permissions for every existing map file and for every map action
            mapPermissions.addAll(getAllMapActionPermissions());

            return mapPermissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.MTA, action.toPermissionName());
    }

    /*
     * Check if admin can access/manage the content of the map file
     */
    private void checkPermission(@Nonnull MapContentAction action, @Nonnull MTADTO.PostfixMapType type, @Nonnull String name)
    {
        // if user has MANAGE_MAP permission, all access is allowed
        if (permissionChecker.hasPermission(PermissionCategory.MTA, Action.MAP_MANAGE.toPermissionName())) {
            return;
        }

        permissionChecker.checkPermission(
                PermissionCategory.MTA,
                action.toPermissionName(),
                CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, type.name()),
                name);
    }

    private List<String> getMapActionPermissions(@Nonnull MapContentAction mapAction, @Nonnull List<PostfixMap> maps)
    {
        return maps.stream().map(m -> PermissionUtils.toPermission(PermissionCategory.MTA,
                mapAction.toPermissionName(),
                CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, m.getMapType().name()),
                m.getName())).toList();
    }

    /*
     * Get permissions for every map file and for every map action
     */
    private List<String> getAllMapActionPermissions()
    {
        List<String> mapActionpermissions = new LinkedList<>();

        try {
            List<PostfixMap> maps = postfixMapManager.getMaps();

            for (MapContentAction mapAction : MapContentAction.values()) {
                mapActionpermissions.addAll(getMapActionPermissions(mapAction, maps));
            }
        }
        catch (IOException e) {
            logger.error("Error getting list of maps", e);
        }

        return mapActionpermissions;
    }

    /*
     * If a new map file is added, new permissions should be added.
     */
    private void reloadMapActionPermissions() {
        permissions.addPermissions(getAllMapActionPermissions());
    }

    public MTAController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @GetMapping(path= RestPaths.MTA_GET_QUEUE_LIST_PATH)
    public List<MTADTO.QueueItem> getQueueList(
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_QUEUE_LIST));

        try {
            return postqueue.listQueue(firstResult, restValidator.limitMaxResults(Optional.ofNullable(maxResults).orElse(
                    restValidator.getMaxResultsUpperLimit()), logger)).stream().map(json ->
            {
                // convert json string to JsonNode
                try {
                    return JacksonUtil.getObjectMapper().readValue(json, MTADTO.QueueItem.class);
                }
                catch (JsonProcessingException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }).toList();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @GetMapping(path=RestPaths.MTA_GET_QUEUE_SIZE_PATH)
    public long getQueueSize()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_QUEUE_SIZE));

        try {
            return postqueue.getQueueSize();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_FLUSH_QUEUE_PATH )
    public void flushQueue()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.FLUSH_QUEUE));

        try {
            postqueue.flushQueue();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_RESCHEDULE_DEFERRED_MAIL_PATH)
    public void rescheduleDeferredMail(@RequestBody List<String> queueIDs)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.RESCHEDULE_DEFERRED_MAIL));

        try {
            for (String queueID : queueIDs) {
                postqueue.rescheduleDeferredMail(restValidator.validatePostfixQueueID(queueID, logger));
            }
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_DELETE_MAIL_PATH)
    public void deleteMailFromQueue(
            @RequestBody List<String> queueIDs,
            @RequestParam(required = false) MTADTO.QueueName queueName)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.DELETE_MAIL));

        try {
            for (String queueID : queueIDs) {
                postsuper.deleteMailFromQueue(restValidator.validatePostfixQueueID(queueID, logger),
                        toPostSuperQueueName(queueName));
            }
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_HOLD_MAIL_PATH)
    public void holdMailFromQueue(
            @RequestBody List<String> queueIDs,
            @RequestParam(required = false) MTADTO.QueueName queueName)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.HOLD_MAIL));

        try {
            for (String queueID : queueIDs) {
                postsuper.holdMailFromQueue(restValidator.validatePostfixQueueID(queueID, logger),
                        toPostSuperQueueName(queueName));
            }
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_RELEASE_MAIL_PATH)
    public void releaseMailFromQueue(
            @RequestBody List<String> queueIDs,
            @RequestParam(required = false) MTADTO.QueueName queueName)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.RELEASE_MAIL));

        try {
            for (String queueID : queueIDs) {
                postsuper.releaseHoldMailFromQueue(restValidator.validatePostfixQueueID(queueID, logger),
                        toPostSuperQueueName(queueName));
            }
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_BOUNCE_MAIL_PATH)
    public void bounceMailFromQueue(
            @RequestBody List<String> queueIDs,
            @RequestParam(required = false) MTADTO.QueueName queueName)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.BOUNCE_MAIL));

        try {
            for (String queueID : queueIDs) {
                postsuper.bounceMailFromQueue(restValidator.validatePostfixQueueID(queueID, logger),
                        toPostSuperQueueName(queueName));
            }
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_REQUEUE_MAIL_PATH)
    public void requeueMailFromQueue(
            @RequestBody List<String> queueIDs,
            @RequestParam(required = false) MTADTO.QueueName queueName)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.REQUEUE_MAIL));

        try {
            for (String queueID : queueIDs) {
                postsuper.requeueMailFromQueue(restValidator.validatePostfixQueueID(queueID, logger),
                        toPostSuperQueueName(queueName));
            }
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @GetMapping(path=RestPaths.MTA_GET_MAIL_PATH)
    public ResponseEntity<StreamingResponseBody> getMail(@RequestParam String queueID,
            @RequestParam(required = false) Long maxSize)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_MAIL));

        StreamingResponseBody responseBody = outputStream ->
        {
            try {
                postcat.writeMail(restValidator.validatePostfixQueueID(queueID, logger),
                        maxSize != null ? new SizeLimitedOutputStream(outputStream, maxSize, false) : outputStream);
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Error getting mail with queue id %s. Message: %s", queueID, e.getMessage()), logger);
            }
        };

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + queueID + ".eml")
                .header(HttpHeaders.CONTENT_TYPE, "plain/txt")
                .body(responseBody);
    }

    @GetMapping(path=RestPaths.MTA_GET_MAIN_CONFIG_PATH)
    public String getMainConfig()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_MAIN_CONFIG));

        try {
            return postfixConfigureMain.getMainConfig();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error getting MTA main config. Message: %s", e.getMessage()), logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_SET_MAIN_CONFIG_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public void setMainConfig(@RequestBody String config)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.SET_MAIN_CONFIG));

        try {
            postfixConfigureMain.setMainConfig(config);
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error setting MTA main config. Message: %s", e.getMessage()), logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_START_POSTFIX_PATH)
    public void startPostfix()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.START_POSTFIX));

        try {
            postfixCtl.startPostfix();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error starting postfix. Message: %s", e.getMessage()), logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_STOP_POSTFIX_PATH)
    public void stopPostfix()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.STOP_POSTFIX));

        try {
            postfixCtl.stopPostfix();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error stopping postfix. Message: %s", e.getMessage()), logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_RESTART_POSTFIX_PATH)
    public void restartPostfix()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.RESTART_POSTFIX));

        try {
            postfixCtl.restartPostfix();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error restarting postfix. Message: %s", e.getMessage()), logger);
        }
    }

    @GetMapping(path=RestPaths.MTA_GET_POSTFIX_STATUS_PATH)
    public String getPostfixStatus()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_POSTFIX_STATUS));

        try {
            return postfixCtl.getPostfixStatus();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error getting postfix status. Message: %s", e.getMessage()), logger);
        }
    }

    @GetMapping(path=RestPaths.MTA_IS_POSTFIX_RUNNING_PATH)
    public boolean isPostfixRunning()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.IS_POSTFIX_RUNNING));

        try {
            return postfixCtl.isPostfixRunning();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error checking postfix running. Message: %s", e.getMessage()), logger);
        }
    }

    @PostMapping(path=RestPaths.MTA_SET_STANDARD_SETTINGS_PATH)
    public void setStandardMainSettings(@RequestBody MTADTO.StandardMainSettings settings)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.SET_STANDARD_SETTINGS));

        try {
            if (settings.getMyHostname() != null)
            {
                postConf.setMainParameterValue(POSTFIX_MY_HOSTNAME,
                        Optional.ofNullable(StringUtils.trimToNull(settings.getMyHostname())).map(
                                hostname -> restValidator.validatePostfixHostname(hostname, logger)).orElse(""));
            }

            if (settings.getMyNetworks() != null)
            {
                postConf.setMainParameterValue(POSTFIX_MY_NETWORKS,
                                StringUtils.join(settings.getMyNetworks().stream().map(network ->
                                        restValidator.validatePostfixNetwork(network, logger)).toList(), ", "));
            }

            if (settings.getExternalRelayHost() != null)
            {
                postConf.setMainParameterValue(POSTFIX_EXTERNAL_RELAY_HOST,
                        Optional.ofNullable(StringUtils.trimToNull(settings.getExternalRelayHost())).map(
                                hostname -> restValidator.validateHostname(hostname, DomainUtils.DomainType.FRAGMENT,
                                        logger)).orElse(""));
            }

            if (settings.getExternalRelayHostMxLookup() != null)
            {
                postConf.setMainParameterValue(POSTFIX_EXTERNAL_RELAY_HOST_MX_LOOKUP,
                        Boolean.TRUE.equals(settings.getExternalRelayHostMxLookup()) ? "lookup" : "");
            }

            if (settings.getExternalRelayHostPort() != null)
            {
                postConf.setMainParameterValue(POSTFIX_EXTERNAL_RELAY_HOST_PORT,
                        Integer.toString(restValidator.validateMinMax(
                                settings.getExternalRelayHostPort(), 0, 65535, logger)));
            }

            if (settings.getRelayDomains() != null)
            {
                postConf.setMainParameterValue(POSTFIX_RELAY_DOMAINS,
                                StringUtils.join(settings.getRelayDomains().stream().map(domain ->
                                        restValidator.validateDomain(domain, DomainUtils.DomainType.FULLY_QUALIFIED,
                                                logger)).toList(), ", "));
            }

            if (settings.getMessageSizeLimit() != null)
            {
                long messageSizeLimit = restValidator.validateMinMax(settings.getMessageSizeLimit(),
                        0L, 2147483647L, logger);

                postConf.setMainParameterValue(POSTFIX_MESSAGE_SIZE_LIMIT, Long.toString(messageSizeLimit));

                // update the calculated after queue message size limit
                postConf.setMainParameterValue(POSTFIX_CALCULATED_AFTER_FILTER_MESSAGE_SIZE_LIMIT,
                        Long.toString(messageSizeLimit * afterFilterSizeMultiplier));

                // update the calculated min queue free
                postConf.setMainParameterValue(POSTFIX_CALCULATED_QUEUE_MIN_FREE,
                        Long.toString(messageSizeLimit * queueMinFreeMultiplier));

                // update the calculated after filter min queue free
                postConf.setMainParameterValue(POSTFIX_CALCULATED_AFTER_FILTER_QUEUE_MIN_FREE,
                        Long.toString(messageSizeLimit * afterFilterQueueMinFreeMultiplier));
            }

            if (settings.getSmtpHeloName() != null)
            {
                postConf.setMainParameterValue(POSTFIX_SMTP_HELO_NAME,
                        Optional.ofNullable(StringUtils.trimToNull(settings.getSmtpHeloName())).map(
                                hostname -> restValidator.validateHostname(hostname, DomainUtils.DomainType.FRAGMENT,
                                        logger)).orElse(""));
            }

            if (settings.getInternalRelayHost() != null)
            {
                postConf.setMainParameterValue(POSTFIX_INTERNAL_RELAY_HOST,
                        Optional.ofNullable(StringUtils.trimToNull(settings.getInternalRelayHost())).map(
                                hostname -> restValidator.validateHostname(hostname, DomainUtils.DomainType.FRAGMENT,
                                        logger)).orElse(""));
            }

            if (settings.getInternalRelayHostMxLookup() != null)
            {
                postConf.setMainParameterValue(POSTFIX_INTERNAL_RELAY_HOST_MX_LOOKUP,
                        Boolean.TRUE.equals(settings.getInternalRelayHostMxLookup()) ? "lookup" : "");
            }

            if (settings.getInternalRelayHostPort() != null)
            {
                postConf.setMainParameterValue(POSTFIX_INTERNAL_RELAY_HOST_PORT,
                        Integer.toString(restValidator.validateMinMax(
                                settings.getInternalRelayHostPort(), 0, 65535, logger)));
            }

            if (settings.getRejectUnverifiedRecipient() != null)
            {
                postConf.setMainParameterValue(POSTFIX_REJECT_UNVERIFIED_RECIPIENT,
                        Boolean.TRUE.equals(settings.getRejectUnverifiedRecipient()) ? "reject" : "");
            }

            if (settings.getUnverifiedRecipientRejectCode() != null)
            {
                postConf.setMainParameterValue(POSTFIX_UNVERIFIED_RECIPIENT_REJECT_CODE,
                        Integer.toString(restValidator.validateMinMax(settings.getUnverifiedRecipientRejectCode(),
                                200, 599, logger)));
            }
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error setting postfix standard main settings. Message: %s", e.getMessage()), logger);
        }
    }

    @GetMapping(path=RestPaths.MTA_GET_STANDARD_SETTINGS_PATH)
    public MTADTO.StandardMainSettings getStandardMainSettings()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_STANDARD_SETTINGS));

        try {
            MTADTO.StandardMainSettings result = new MTADTO.StandardMainSettings();

            result.setMyHostname(postConf.getMainParameterValue(POSTFIX_MY_HOSTNAME));
            result.setMyNetworks(List.of(PostfixUtils.splitList(
                    postConf.getMainParameterValue(POSTFIX_MY_NETWORKS))));
            result.setExternalRelayHost(postConf.getMainParameterValue(POSTFIX_EXTERNAL_RELAY_HOST));
            result.setExternalRelayHostMxLookup(StringUtils.trimToNull(
                    postConf.getMainParameterValue(POSTFIX_EXTERNAL_RELAY_HOST_MX_LOOKUP)) != null);
            result.setExternalRelayHostPort(Optional.ofNullable(StringUtils.trimToNull(
                    postConf.getMainParameterValue(POSTFIX_EXTERNAL_RELAY_HOST_PORT)))
                        .map(NumberUtils::toInt).orElse(null));
            result.setRelayDomains(List.of(PostfixUtils.splitList(
                    postConf.getMainParameterValue(POSTFIX_RELAY_DOMAINS))));
            result.setMessageSizeLimit(Optional.ofNullable(StringUtils.trimToNull(
                    postConf.getMainParameterValue(POSTFIX_MESSAGE_SIZE_LIMIT)))
                        .map(NumberUtils::toLong).orElse(null));
            result.setSmtpHeloName(postConf.getMainParameterValue(POSTFIX_SMTP_HELO_NAME));
            result.setInternalRelayHost(postConf.getMainParameterValue(POSTFIX_INTERNAL_RELAY_HOST));
            result.setInternalRelayHostMxLookup(StringUtils.trimToNull(
                    postConf.getMainParameterValue(POSTFIX_INTERNAL_RELAY_HOST_MX_LOOKUP)) != null);
            result.setInternalRelayHostPort(Optional.ofNullable(StringUtils.trimToNull(
                    postConf.getMainParameterValue(POSTFIX_INTERNAL_RELAY_HOST_PORT)))
                        .map(NumberUtils::toInt).orElse(null));
            result.setRejectUnverifiedRecipient(StringUtils.trimToNull(
                            postConf.getMainParameterValue(POSTFIX_REJECT_UNVERIFIED_RECIPIENT)) != null);
            result.setUnverifiedRecipientRejectCode(Optional.ofNullable(StringUtils.trimToNull(
                    postConf.getMainParameterValue(POSTFIX_UNVERIFIED_RECIPIENT_REJECT_CODE)))
                        .map(NumberUtils::toInt).orElse(null));

            return result;
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error checking postfix running. Message: %s", e.getMessage()), logger);
        }
    }

    @GetMapping(path=RestPaths.MTA_LIST_MAPS)
    public List<MTADTO.PostfixMap> listMaps()
    throws IOException
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.MAP_LIST));

        return postfixMapManager.getMaps().stream().map(this::toPostfixMapDTO).toList();
    }

    @GetMapping(path=RestPaths.MTA_GET_MAP_CONTENT)
    public String getMapContent(
            @RequestParam MTADTO.PostfixMapType type,
            @RequestParam String name)
    throws IOException
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(MapContentAction.MAP_GET_CONTENT, type, name));

        return getPostfixMap(type, name).getContent();
    }

    @PostMapping(path=RestPaths.MTA_SET_MAP_CONTENT, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public void setMapContent(
            @RequestParam MTADTO.PostfixMapType type,
            @RequestParam String name,
            @RequestBody(required = false) String content)
    throws IOException
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(MapContentAction.MAP_SET_CONTENT, type, name));

        getPostfixMap(type, name).setContent(StringUtils.defaultString(content));
    }

    @PostMapping(path=RestPaths.MTA_CREATE_MAP)
    public MTADTO.PostfixMap createMap(
            @RequestParam MTADTO.PostfixMapType type,
            @RequestParam String name)
    throws IOException
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.MAP_MANAGE));

        if (postfixMapManager.getMap(MapType.valueOf(type.name()), name) != null)
        {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Map of type %s and name %s already exist", type, name), logger);
        }

        PostfixMap map = postfixMapManager.createMap(MapType.valueOf(type.name()), name);

        // content has to set otherwise the map file will not be created
        map.setContent("");

        // because a new map was added, need permissions should be added
        reloadMapActionPermissions();

        return toPostfixMapDTO(map);
    }

    @PostMapping(path=RestPaths.MTA_DELETE_MAP)
    public void deleteMap(
            @RequestParam MTADTO.PostfixMapType type,
            @RequestParam String name)
    throws IOException
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.MAP_MANAGE));

        PostfixMap map = getPostfixMap(type, name);

        postfixMapManager.deleteMap(map.getMapType(), map.getName());
    }

    @PostMapping(path = RestPaths.MTA_SEND_EMAIL_MIME, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(encoding =
            {
                    @Encoding(name = "recipients", contentType = MediaType.APPLICATION_JSON_VALUE),
            }))
    public void sendEmailMime(
            @RequestPart(required = false) String sender,
            @RequestPart(value = "recipients", required = false) List<String> recipients,
            @RequestParam MTADTO.MTAPort mtaPort,
            @RequestBody MultipartFile mime)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.SEND_EMAIL_MIME));

        try(InputStream mimeInputStream = mime.getInputStream()) {
            sendMime(sender, recipients, mtaPort, IOUtils.toString(mimeInputStream, StandardCharsets.UTF_8));
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(path = RestPaths.MTA_SEND_EMAIL, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
        @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(encoding =
                {
                        @Encoding(name = "recipients", contentType = MediaType.APPLICATION_JSON_VALUE),
                        @Encoding(name = "to", contentType = MediaType.APPLICATION_JSON_VALUE),
                        @Encoding(name = "cc", contentType = MediaType.APPLICATION_JSON_VALUE)
                }))
    public void sendEmail(
            @RequestPart String subject,
            @RequestPart String from,
            @RequestPart(required = false) String textBody,
            @RequestPart(required = false) String htmlBody,
            @RequestPart(required = false) String sender,
            @RequestPart(value = "recipients", required = false) List<String> recipients,
            @RequestPart(value = "to", required = false) List<String> to,
            @RequestPart(value = "cc", required = false) List<String> cc,
            @RequestParam MTADTO.MTAPort mtaPort,
            @RequestBody MultipartFile attachment)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.SEND_EMAIL));

        try(MimeMessageBuilder messageBuilder = MimeMessageBuilder.createInstance()
                    .setSubject(subject)
                    .setFrom(from)
                    .setTextBody(textBody)
                    .setHtmlBody(htmlBody)
                    .addTo(to)
                    .addCc(cc))
        {
            if (attachment != null)
            {
                // Note: InputStreamResource and attachment input stream will be closed when MimeMessageBuilder is closed
                messageBuilder.addAttachment(new InputStreamResource(attachment.getInputStream(),
                        Optional.ofNullable(attachment.getContentType()).orElse(MediaType.APPLICATION_OCTET_STREAM_VALUE),
                        attachment.getOriginalFilename()));
            }

            sendMimeMessage(sender, recipients, mtaPort, messageBuilder.build());
        }
        catch (IOException | MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    private PostfixMap getPostfixMap(
            @Nonnull MTADTO.PostfixMapType type,
            @Nonnull String name)
    throws IOException
    {
        PostfixMap map = postfixMapManager.getMap(MapType.valueOf(type.name()), name);

        if (map == null)
        {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Map of type %s and name %s not found", type, name), logger);
        }

        return map;
    }

    private PostSuper.QueueName toPostSuperQueueName(MTADTO.QueueName queueName)
    {
        if (queueName == null) {
            return null;
        }

        return switch (queueName) {
            case HOLD -> PostSuper.QueueName.HOLD;
            case INCOMING -> PostSuper.QueueName.INCOMING;
            case ACTIVE -> PostSuper.QueueName.ACTIVE;
            case DEFERRED -> PostSuper.QueueName.DEFERRED;
        };
    }

    private void sendMime(
            String sender,
            List<String> recipients,
            @Nonnull MTADTO.MTAPort mtaPort,
            @Nonnull String mime)
    {
        try {
            // check if the content is mime
            if (!MimeUtils.isMIME(IOUtils.toInputStream(mime, StandardCharsets.UTF_8), mimeCheckMaxChars)) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Content is not valid MIME", logger);
            }

            sendMimeMessage(
                    sender,
                    recipients,
                    mtaPort,
                    MailUtils.loadMessage(IOUtils.toInputStream(mime, StandardCharsets.UTF_8)));
        }
        catch (MessagingException | IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    private void sendMimeMessage(
            String sender,
            List<String> recipients,
            @Nonnull MTADTO.MTAPort mtaPort,
            @Nonnull MimeMessage message)
    {
        int port = switch (mtaPort)
        {
            case DIRECT_DELIVERY -> directDeliveryPort;
            case MPA -> mpaPort;
        };

        try {
            logger.info("Sending email (Sender: {}, Recipients: {}, Port: {})", sender, recipients, port);

            mailTransport.sendMessage(
                    smtpHost,
                    port,
                    message,
                    StringUtils.trimToNull(sender),
                    EmailAddressUtils.toInternetAddressesStrict(recipients));
        }
        catch (MessagingException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    private MTADTO.PostfixMap toPostfixMapDTO(@Nonnull PostfixMap postfixMap)
    {
        return new MTADTO.PostfixMap(
                MTADTO.PostfixMapType.valueOf(postfixMap.getMapType().name()),
                postfixMap.getName(),
                postfixMap.getFilename());
    }
}
