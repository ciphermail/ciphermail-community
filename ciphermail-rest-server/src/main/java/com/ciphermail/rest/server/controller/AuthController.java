/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.admin.Admin;
import com.ciphermail.core.app.admin.AdminManager;
import com.ciphermail.core.app.admin.AuthenticationType;
import com.ciphermail.core.app.admin.Role;
import com.ciphermail.core.app.admin.RoleManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.crypto.RandomGenerator;
import com.ciphermail.core.common.security.otp.TOTP;
import com.ciphermail.core.common.security.otp.TOTPValidationFailureException;
import com.ciphermail.core.common.util.Base32Utils;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.GoogleAuthenticatorUtils;
import com.ciphermail.rest.core.AuthDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.authorization.Permissions;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthenticationMethod;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.StreamSupport;

@RestController
@Tag(name = "Auth")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class AuthController
{
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Value("${ciphermail.rest.authentication.oauth2.authorization-request-base-uri}")
    private String authorizationRequestBaseUri;

    @Value("${ciphermail.rest.authentication.2fa.length:20}")
    private int tfaLength;

    @Value("${ciphermail.rest.authentication.2fa.qrcode.format:png}")
    private String qrCodeFormat;

    @Value("${ciphermail.rest.authentication.2fa.qrcode.width:200}")
    private int qrCodeWidth;

    @Value("${ciphermail.rest.authentication.2fa.qrcode.height:200}")
    private int qrCodeHeight;

    /*
     * OAuth2 client registrations. Can be null if OAuth2 is not enabled.
     */
    @Autowired(required = false)
    private ClientRegistrationRepository clientRegistrationRepository;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private Permissions permissions;

    @Autowired
    private RoleManager roleManager;

    @Autowired
    private AdminManager adminManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private AuthenticatedUserProvider authenticatedUserProvider;

    @Autowired
    private RandomGenerator randomGenerator;

    @Autowired
    private TOTP totp;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    private enum Action
    {
        GET_AVAILABLE_PERMISSIONS,
        GET_ROLES,
        GET_ROLE,
        GET_ADMINS,
        GET_ADMIN,
        ADD_ADMIN,
        DELETE_ADMIN,
        CREATE_ROLE,
        DELETE_ROLE,
        ADD_ROLE_PERMISSIONS,
        SET_ROLE_PERMISSIONS,
        REMOVE_ROLE_PERMISSIONS,
        SET_PASSWORD,
        CHANGE_PASSWORD,
        SET_IP_ADDRESSES,
        SET_ROLES,
        SET_INHERITED_ROLES,
        GET_EFFECTIVE_PERMISSIONS,
        GET_AUTHENTICATED_USER,
        SET_2FA_ENABLED,
        SET_2FA_SECRET,
        GET_2FA_SECRET,
        GET_CALLER_IS_2FA_SECRET_SET,
        GET_CALLER_2FA_SECRET,
        GET_CALLER_2FA_ENABLED,
        SET_CALLER_2FA_ENABLED,
        GET_CALLER_AUTHENTICATION_TYPE;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values()) {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.AUTH, action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.AUTH, action.toPermissionName());
    }

    public AuthController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @GetMapping(RestPaths.AUTH_GET_OAUTH2_CLIENTS_PATH)
    public List<AuthDTO.OAuth2ClientRegistrationDetails> getOIDCClients()
    {
        List<AuthDTO.OAuth2ClientRegistrationDetails> registrationDetails = new LinkedList<>();

        if (clientRegistrationRepository != null)
        {
            if (!(clientRegistrationRepository instanceof InMemoryClientRegistrationRepository
                    inMemoryClientRegistrationRepository))
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                        "clientRegistrationRepository is-not-a InMemoryClientRegistrationRepository", logger);
            }

            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                    inMemoryClientRegistrationRepository.iterator(), Spliterator.ORDERED), false)
                    .map(this::toOAuth2ClientRegistrationDetails).toList();
        }

        return registrationDetails;
    }

    @GetMapping(RestPaths.AUTH_GET_AVAILABLE_PERMISSIONS_PATH)
    public Set<String> getAvailablePermissions()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_AVAILABLE_PERMISSIONS);

            return permissions.getPermissions();
        });
    }

    @GetMapping(RestPaths.AUTH_GET_ROLES_PATH)
    public List<AuthDTO.RoleDetails> getRoles()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_ROLES);

            return roleManager.getRoles(null, null, SortDirection.ASC)
                    .stream().map(this::toRoleDetails).toList();
        });
    }

    @GetMapping(RestPaths.AUTH_GET_ROLE_PATH)
    public AuthDTO.RoleDetails getRoleByName(@RequestParam String role)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_ROLE);

             return toRoleDetails(roleManager.getRole(role));
        });
    }

    @GetMapping(RestPaths.AUTH_GET_ADMINS_PATH)
    public List<AuthDTO.AdminDetails> getAdmins()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_ADMINS);

            return adminManager.getAdmins(null, null, SortDirection.ASC)
                    .stream().map(this::toAdminDetails).toList();
        });
    }

    @GetMapping(RestPaths.AUTH_GET_ADMIN_PATH)
    public AuthDTO.AdminDetails getAdminByName(@RequestParam String admin)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_ADMIN);

            return toAdminDetails(adminManager.getAdmin(admin));
        });
    }

    @PostMapping(RestPaths.AUTH_ADD_ADMIN_PATH)
    public void addAdmin(
            @RequestParam String admin,
            @RequestParam AuthenticationType authenticationType)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.ADD_ADMIN);

            Admin adminEntity = adminManager.getAdmin(admin);

            if (adminEntity != null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Admin with name %s already exist", admin), logger);
            }

            adminEntity = adminManager.createAdmin(admin);
            adminEntity.setAuthenticationType(authenticationType);
            // generate 2FA secret
            adminEntity.set2FASecret(Base32Utils.base32Encode(randomGenerator.generateRandom(tfaLength)));

            adminManager.persistAdmin(adminEntity);
        });
    }

    @PostMapping(RestPaths.AUTH_DELETE_ADMIN_PATH)
    public void deleteAdmin(@RequestParam String admin)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_ADMIN);

            Admin adminEntity = getAdmin(admin);

            adminManager.deleteAdmin(adminEntity.getName());
        });
    }

    @PostMapping(RestPaths.AUTH_CREATE_ROLE_PATH)
    public AuthDTO.RoleDetails createRole(@RequestParam String role)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.CREATE_ROLE);

            Role roleEntity = roleManager.getRole(role);

            if (roleEntity != null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Role with name %s already exist", role), logger);
            }

            return toRoleDetails(roleManager.persistRole(roleManager.createRole(role)));
        });
    }

    @PostMapping(RestPaths.AUTH_DELETE_ROLE_PATH)
    public void deleteRole(@RequestParam String role)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_ROLE);

            roleManager.deleteRole(getRole(role).getName());
        });
    }

    @PostMapping(RestPaths.AUTH_ADD_ROLE_PERMISSIONS_PATH)
    public void addRolePermissions(
            @RequestParam String role,
            @RequestBody List<String> permissions)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.ADD_ROLE_PERMISSIONS);

            Role roleEntity = getRole(role);

            for (String permission : permissions)
            {
                if (!this.permissions.isPermission(permission)) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            String.format("%s is not a supported permission", permission), logger);
                }
            }

            roleEntity.getPermissions().addAll(permissions);
        });
    }

    @PostMapping(RestPaths.AUTH_SET_ROLE_PERMISSIONS_PATH)
    public void setRolePermissions(
            @RequestParam String role,
            @RequestBody List<String> permissions)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_ROLE_PERMISSIONS);

            Role roleEntity = getRole(role);

            for (String permission : permissions)
            {
                if (!this.permissions.isPermission(permission)) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            String.format("%s is not a supported permission", permission), logger);
                }
            }

            roleEntity.getPermissions().clear();
            roleEntity.getPermissions().addAll(permissions);
        });
    }

    @PostMapping(RestPaths.AUTH_REMOVE_ROLE_PERMISSIONS_PATH)
    public void removeRolePermissions(
            @RequestParam String role,
            @RequestBody List<String> permissions)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.REMOVE_ROLE_PERMISSIONS);

            Role roleEntity = getRole(role);

            // Note: do not check whether the permission is valid because we want to be able to remove permissions
            // which were renamed or removed
            permissions.forEach(roleEntity.getPermissions()::remove);
        });
    }

    @PostMapping(path = RestPaths.AUTH_SET_PASSWORD_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public void setPassword(
            @RequestParam String admin,
            @RequestBody String password)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_PASSWORD);

            Admin adminEntity = getAdmin(admin);

            if (AuthenticationType.USERNAME_PASSWORD != adminEntity.getAuthenticationType()) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Admin with name %s is not a USERNAME_PASSWORD type", admin), logger);
            }

            adminEntity.setPassword(passwordEncoder.encode(password));
        });
    }

    @PostMapping(path = RestPaths.AUTH_CHANGE_PASSWORD_PATH)
    public void changePassword(@RequestBody AuthDTO.ChangePassword changePassword)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.CHANGE_PASSWORD);

            Admin adminEntity = getAdmin(authenticatedUserProvider.getAuthenticatedUser());

            if (AuthenticationType.USERNAME_PASSWORD != adminEntity.getAuthenticationType()) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Admin with name %s is not a USERNAME_PASSWORD type",
                                authenticatedUserProvider.getAuthenticatedUser()), logger);
            }

            if (changePassword.newPassword().isEmpty())
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "New password is empty", logger);
            }

            if (!passwordEncoder.matches(changePassword.currentPassword(), adminEntity.getPassword()))
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Current password is incorrect", logger);
            }

            if (!changePassword.newPassword().equals(changePassword.newPasswordRepeat()))
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "New password and repeated password do not match", logger);
            }

            adminEntity.setPassword(passwordEncoder.encode(changePassword.newPassword()));
        });
    }

    @PostMapping(RestPaths.AUTH_SET_IP_ADDRESSES_PATH)
    public void setIpAddresses(
            @RequestParam String admin,
            @RequestBody List<String> ipAddresses)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_IP_ADDRESSES);

            Admin adminEntity = getAdmin(admin);

            for (String ipAddress : ipAddresses) {
                restValidator.validateIpAddressRange(ipAddress, logger);
            }

            adminEntity.setIpAddresses(ipAddresses);
        });
    }

    @PostMapping(RestPaths.AUTH_SET_ROLES_PATH)
    public void setRoles(
            @RequestParam String admin,
            @RequestBody List<String> roles)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_ROLES);

            Admin adminEntity = getAdmin(admin);

            adminEntity.getRoles().clear();
            adminEntity.getRoles().addAll(roles.stream().map(role -> Optional.ofNullable(roleManager.getRole(role))
                    .orElseThrow(() -> responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Role with name %s does not exist", role), logger))).toList());
        });
    }

    @PostMapping(RestPaths.AUTH_SET_INHERITED_ROLES_PATH)
    public void setInheritedRoles(@RequestParam String role, @RequestBody List<String> inheritedRoles)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_INHERITED_ROLES);

            Role roleEntity = getRole(role);

            roleEntity.getRoles().clear();
            roleEntity.getRoles().addAll(inheritedRoles.stream().map(this::getRole).toList());
        });
    }

    @GetMapping(RestPaths.AUTH_GET_EFFECTIVE_PERMISSIONS_PATH)
    public Set<String> getEffectivePermissions(@RequestParam String admin)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_EFFECTIVE_PERMISSIONS);

            return adminManager.getEffectivePermissions(getAdmin(admin).getName());
        });
    }

    @GetMapping(RestPaths.AUTH_GET_AUTHENTICATED_USER_PATH)
    public String getAuthenticatedUser()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_AUTHENTICATED_USER);

            return authenticatedUserProvider.getAuthenticatedUser();
        });
    }

    @PostMapping(RestPaths.AUTH_VALIDATE_TOTP_PATH)
    public void validateTOTP(@RequestParam String otp)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            // Note: should be allowed without a permission (i.e., allowed to all) but not without authentication.

            String otpSecret = StringUtils.trimToNull(getAdmin(authenticatedUserProvider.getAuthenticatedUser())
                    .get2FASecret());

            if (otpSecret == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "OTP secret is not set", logger);
            }

            try {
                totp.verifyOTP(Base32Utils.base32Decode(otpSecret), otp);
            }
            catch (TOTPValidationFailureException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.UNAUTHORIZED,
                        "TOTP verification failure", logger);
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(RestPaths.AUTH_SET_2FA_ENABLED_PATH)
    public void set2FAEnabled(@RequestParam String admin, @RequestParam boolean enable)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_2FA_ENABLED);

            Admin adminEntity = getAdmin(admin);

            adminEntity.setEnable2FA(enable);
        });
    }

    @PostMapping(path = RestPaths.AUTH_SET_2FA_SECRET_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public void set2FASecret(@RequestParam String admin, @RequestBody String secret)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_2FA_SECRET);

            getAdmin(admin).set2FASecret(secret);
        });
    }

    @GetMapping(RestPaths.AUTH_GET_2FA_SECRET_PATH)
    public String get2FASecret(@RequestParam String admin)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_2FA_SECRET);

            return getAdmin(admin).get2FASecret();
        });
    }

    @GetMapping(RestPaths.AUTH_GET_CALLER_IS_2FA_SECRET_SET_PATH)
    public boolean getCallerIs2FASecretSet()
    {
        return Boolean.TRUE.equals(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CALLER_IS_2FA_SECRET_SET);

            return StringUtils.isNotBlank(getAdmin(authenticatedUserProvider.getAuthenticatedUser()).get2FASecret());
        }));
    }

    @GetMapping(RestPaths.AUTH_GET_CALLER_2FA_SECRET_PATH)
    public String getCaller2FASecret()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CALLER_2FA_SECRET);

            return getAdmin(authenticatedUserProvider.getAuthenticatedUser()).get2FASecret();
        });
    }

    @GetMapping(RestPaths.AUTH_GET_CALLER_2FA_SECRET_QR_CODE_PATH)
    public String getCaller2FASecretQRCode()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CALLER_2FA_SECRET);

            String tfaSecret = StringUtils.trimToNull(getAdmin(authenticatedUserProvider.getAuthenticatedUser())
                    .get2FASecret());

            if (tfaSecret == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "OTP secret is not set", logger);
            }

            try {
                return Base64Utils.encode(GoogleAuthenticatorUtils.generateQRCode(
                        tfaSecret,
                        StringUtils.defaultString(globalPreferencesManager.getGlobalUserPreferences()
                                .getProperties().getOrganizationID()),
                        authenticatedUserProvider.getAuthenticatedUser(),
                        qrCodeFormat,
                        qrCodeWidth,
                        qrCodeHeight));
            }
            catch (IOException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.AUTH_GET_CALLER_2FA_ENABLED_PATH)
    public boolean getCaller2FAEnabled()
    {
        return Boolean.TRUE.equals(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CALLER_2FA_ENABLED);

            return getAdmin(authenticatedUserProvider.getAuthenticatedUser()).isEnable2FA();
        }));
    }

    @PostMapping(RestPaths.AUTH_SET_CALLER_2FA_ENABLED_PATH)
    public void setCaller2FAEnabled(@RequestParam boolean enable)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_CALLER_2FA_ENABLED);

            Admin adminEntity = getAdmin(authenticatedUserProvider.getAuthenticatedUser());

            adminEntity.setEnable2FA(enable);
        });
    }

    @GetMapping(RestPaths.AUTH_GET_CALLER_AUTHENTICATION_TYPE_PATH)
    public AuthenticationType getCallerAuthenticationType()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CALLER_AUTHENTICATION_TYPE);

            return getAdmin(authenticatedUserProvider.getAuthenticatedUser()).getAuthenticationType();
        });
    }

    private @Nonnull Admin getAdmin(@Nonnull String name)
    {
        Admin admin = adminManager.getAdmin(name);

        if (admin == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Admin with name %s does not exist", name), logger);
        }

        return admin;
    }

    private @Nonnull Role getRole(@Nonnull String name)
    {
        Role role = roleManager.getRole(name);

        if (role == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Role with name %s does not exist", name), logger);
        }

        return role;
    }

    private AuthDTO.RoleDetails toRoleDetails(Role role)
    {
        return role != null ? new AuthDTO.RoleDetails(role.getName(), role.getPermissions(),
                role.getRoles().stream().map(this::toRoleDetails).toList()) : null;
    }

    private AuthDTO.AdminDetails toAdminDetails(Admin admin) {
        return admin != null ? new AuthDTO.AdminDetails(
                admin.getName(),
                admin.getAuthenticationType(),
                admin.isEnable2FA(),
                admin.getIpAddresses(),
                admin.getRoles().stream().map(this::toRoleDetails).toList()) : null;
    }

    private AuthDTO.OAuth2ClientRegistrationDetails toOAuth2ClientRegistrationDetails(
            @Nonnull ClientRegistration clientRegistration)
    {
        return new AuthDTO.OAuth2ClientRegistrationDetails(
                authorizationRequestBaseUri + "/" + clientRegistration.getRegistrationId(),
                clientRegistration.getRegistrationId(),
                clientRegistration.getClientId(),
                Optional.ofNullable(clientRegistration.getClientAuthenticationMethod())
                        .map(ClientAuthenticationMethod::getValue).orElse(null),
                Optional.ofNullable(clientRegistration.getAuthorizationGrantType())
                        .map(AuthorizationGrantType::getValue).orElse(null),
                clientRegistration.getRedirectUri(),
                clientRegistration.getScopes(),
                clientRegistration.getClientName(),
                toOAuth2ProviderDetails(clientRegistration.getProviderDetails()));
    }

    private AuthDTO.OAuth2ProviderDetails toOAuth2ProviderDetails(@Nonnull ClientRegistration.ProviderDetails providerDetails)
    {
        return new AuthDTO.OAuth2ProviderDetails(providerDetails.getAuthorizationUri(), providerDetails.getTokenUri(),
                toOAuth2UserInfoEndpoint(providerDetails.getUserInfoEndpoint()), providerDetails.getJwkSetUri(),
                providerDetails.getIssuerUri());
    }

    private AuthDTO.OAuth2UserInfoEndpoint toOAuth2UserInfoEndpoint(
            @Nonnull ClientRegistration.ProviderDetails.UserInfoEndpoint userInfoEndpoint)
    {
        return new AuthDTO.OAuth2UserInfoEndpoint(userInfoEndpoint.getUri(),
                Optional.ofNullable(userInfoEndpoint.getAuthenticationMethod())
                        .map(AuthenticationMethod::getValue).orElse(null),
                userInfoEndpoint.getUserNameAttributeName());
    }
}
