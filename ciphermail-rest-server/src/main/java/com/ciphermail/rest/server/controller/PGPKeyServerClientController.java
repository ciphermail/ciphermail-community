/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.openpgp.keyserver.KeyServerClient;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClientSearchResult;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingExporter;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporter;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientSettings;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientSettingsImpl;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServers;
import com.ciphermail.core.common.security.openpgp.keyserver.KeyServerKeyInfo;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListEntry;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.core.common.util.DirectAccessByteArrayOutputStream;
import com.ciphermail.core.common.util.URIUtils;
import com.ciphermail.rest.core.PGPKeyServerDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.PGPKeyServerSubmitDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPPublicKeyRingCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "PGPKeyServerClient")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PGPKeyServerClientController
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyServerClientController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private KeyServerClient keyServerClient;

    @Autowired
    private PGPKeyRingImporter keyRingImporter;

    @Autowired
    private PGPTrustList trustList;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPKeyRingExporter keyRingExporter;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private PGPKeyServerSubmitDetailsFactory pgpKeyServerSubmitDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        SEARCH_KEYS,
        DOWNLOAD_KEYS,
        REFRESH_KEYS,
        SUBMIT_KEYS,
        SET_PGP_KEY_SERVERS,
        GET_PGP_KEY_SERVERS;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.PGP_KEY_SERVER_CLIENT, action.toPermissionName());
    }

    public PGPKeyServerClientController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    private PGPKeyServerDTO.PGPKeyServerKeyDetails toKeyServerKeyDetails(@Nonnull KeyServerKeyInfo keyInfo)
    {
        return new PGPKeyServerDTO.PGPKeyServerKeyDetails(
                keyInfo.getKeyID(),
                keyInfo.getAlgorithm(),
                keyInfo.getKeyLength(),
                Optional.ofNullable(keyInfo.getCreationDate()).map(Date::getTime).orElse(null),
                Optional.ofNullable(keyInfo.getExpirationDate()).map(Date::getTime).orElse(null),
                StringUtils.defaultString(keyInfo.getFlags()),
                keyInfo.getUserIDs(),
                keyInfo.getServerURL());
    }

    /**
     * Searches the key servers for the specified keys and returns the results from all key servers.
     * If a search on a key server fails, the error message will be stored and the next server will be tried.
     */
    @PostMapping(path= RestPaths.PGP_KEY_SERVER_SEARCH_KEYS_PATH)
    public PGPKeyServerDTO.PGPKeyServerSearchResult searchKeys(
            @RequestParam String query,
            @RequestParam boolean exactMatch,
            @RequestParam(required = false) Integer maxKeys)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SEARCH_KEYS);

            try {
                KeyServerClientSearchResult searchResult = keyServerClient.searchKeys(query, exactMatch, maxKeys);

                return new PGPKeyServerDTO.PGPKeyServerSearchResult(searchResult.getKeys().stream()
                        .map(this::toKeyServerKeyDetails).toList(), searchResult.getErrors());
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    /**
     * Downloads keys from the key servers with the given Key ID and imports the keys into the key ring. The keys from
     * the first key server with a result will be imported (i.e., if a key server returns a valid key ring, the other
     * servers will not be queried). If autoTrust is true, the key will be added to the trust list
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "KeyID is empty",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.PGP_KEY_SERVER_DOWNLOAD_KEYS_PATH)
    public PGPKeyServerDTO.PGPKeyServerDownloadResult downloadKeys(
            @RequestBody List<String> keyIDs,
            @RequestParam boolean autoTrust)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.DOWNLOAD_KEYS);

            return internalDownloadKeys(keyIDs, autoTrust);
        });
    }

    private PGPKeyServerDTO.PGPKeyServerDownloadResult internalDownloadKeys(
            @Nonnull List<String> keyIDs,
            @RequestParam boolean autoTrust)
    {
        if (keyIDs.isEmpty()) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    "keyIDs is empty", logger);
        }

        List<String> downloadedIds = new LinkedList<>();
        List<String> importedIds = new LinkedList<>();
        List<String> errors = new LinkedList<>();

        for (String keyID : keyIDs)
        {
            keyID = StringUtils.trimToNull(keyID);

            if (keyID == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "KeyID is empty", logger);
            }

            PGPPublicKeyRingCollection downloadedKeyRing = null;

            try {
                downloadedKeyRing = keyServerClient.getKeys(keyID);
            }
            catch (Exception e) {
                String errorMessage = String.format("Error downloading key with key ID %s. Error message: %s",
                        keyID, e.getMessage());

                errors.add(errorMessage);

                if (logger.isDebugEnabled()) {
                    logger.warn(errorMessage, e);
                }
                else {
                    logger.warn(errorMessage);
                }
            }

            if (downloadedKeyRing == null) {
                continue;
            }

            downloadedIds.add(keyID);

            Iterator<?> keyRingIterator = downloadedKeyRing.getKeyRings();

            if (keyRingIterator == null) {
                continue;
            }

            importKeyRing(autoTrust, importedIds, errors, keyID, keyRingIterator);
        }

        return new PGPKeyServerDTO.PGPKeyServerDownloadResult(downloadedIds, importedIds, errors);
    }

    private void importKeyRing(
            boolean autoTrust,
            @Nonnull List<String> importedIds,
            @Nonnull List<String> errors,
            @Nonnull String keyID,
            @Nonnull Iterator<?> keyRingIterator)
    {
        while (keyRingIterator.hasNext())
        {
            Object o = keyRingIterator.next();

            if (!(o instanceof PGPPublicKeyRing importedKeyRing)) {
                continue;
            }

            try {
                List<PGPKeyRingEntry> imported = keyRingImporter.importKeyRing(
                        new ByteArrayInputStream(importedKeyRing.getEncoded()), null);

                if (CollectionUtils.isEmpty(imported)) {
                    continue;
                }

                for (PGPKeyRingEntry entry : imported)
                {
                    if (!entry.isMasterKey()) {
                        continue;
                    }

                    String masterKeyId = PGPUtils.getKeyIDHex(entry.getKeyID());

                    importedIds.add(masterKeyId);

                    if (autoTrust)
                    {
                        // only trust the key if the keyID of the downloaded key matches the key which was
                        // requested
                        if (!StringUtils.endsWith(keyID, masterKeyId))
                        {
                            logger.warn("Downloaded key with keyID {}, does not match the " +
                                        "requested keyID {}. Key will not be auto trusted",
                                    masterKeyId, keyID);

                            continue;
                        }

                        logger.atInfo().log("Auto trusting key with key ID {}", masterKeyId);

                        PGPTrustListEntry trustListEntry = trustList.createEntry(
                                entry.getSHA256Fingerprint());

                        trustListEntry.setStatus(PGPTrustListStatus.TRUSTED);

                        trustList.setEntry(trustListEntry, true /* include sub keys */);
                    }
                }
            }
            catch (Exception e)
            {
                errors.add("Error importing key(s)");
                logger.error("Error importing key(s)", e);
            }
        }
    }

    /**
     * Downloads and imports the key from the key server with the given id (this is the id of the key ring entry,
     * not the KeyID). The key from the first key server with a result will be imported (i.e., if a key server
     * returns a valid key ring, the other servers will not be queried).
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.PGP_KEY_SERVER_REFRESH_KEYS_PATH)
    public PGPKeyServerDTO.PGPKeyServerDownloadResult refreshKeys(@RequestBody List<UUID> ids)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.REFRESH_KEYS);

            List<String> keyIDs = new LinkedList<>();

            if (ids == null || ids.isEmpty()) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "ids is empty", logger);
            }

            try {
                for (UUID id : ids)
                {
                    PGPKeyRingEntry entry = keyRing.getByID(id);

                    if (entry == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("Entry with id %s not found", id), logger);
                    }

                    keyIDs.add(PGPUtils.getKeyIDHex(entry.getKeyID()));
                }
            }
            catch (IOException | PGPException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }

            return internalDownloadKeys(keyIDs, false);
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.PGP_KEY_SERVER_SUBMIT_KEYS_PATH)
    public PGPKeyServerDTO.KeyServerSubmitDetails submitKeys(@RequestBody List<UUID> ids)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SUBMIT_KEYS);

            if (ids == null || ids.isEmpty()) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "ids is empty", logger);
            }

            try {
                Set<PGPKeyRingEntry> entries = new HashSet<>();

                for (UUID id : ids)
                {
                    PGPKeyRingEntry entry = keyRing.getByID(id);

                    if (entry == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("Entry with id %s not found", id), logger);
                    }

                    entries.add(entry);
                }

                DirectAccessByteArrayOutputStream encodedKeyRing = new DirectAccessByteArrayOutputStream();

                if (keyRingExporter.exportPublicKeys(entries, encodedKeyRing))
                {
                    return pgpKeyServerSubmitDetailsFactory.createKeyServerSubmitDetails(
                            keyServerClient.submitKeys(new JcaPGPPublicKeyRingCollection(PGPUtil.getDecoderStream(
                                    encodedKeyRing.getInputStream()))));
                }
                else {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "No keys were exported", logger);
                }
            }
            catch (IOException | PGPException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    /**
     * Sets the registered HPK key servers
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Server URL is invalid",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.PGP_KEY_SERVER_SET_PGP_KEY_SERVERS_PATH)
    public void setPGPKeyServers(@RequestBody List<PGPKeyServerDTO.PGPKeyServerClientSettings> keyServerClientSettings)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_PGP_KEY_SERVERS);

            try {
                if (keyServerClientSettings == null || keyServerClientSettings.isEmpty()) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "keyServerClientSettings is empty", logger);
                }

                HKPKeyServers servers = keyServerClient.getServers();

                List<HKPKeyServerClientSettings> clientSettings = new LinkedList<>();

                for (PGPKeyServerDTO.PGPKeyServerClientSettings server : keyServerClientSettings)
                {
                    if (!URIUtils.isValidURI(server.serverURL(), URIUtils.URIType.FULL)) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Server URL %s is invalid", server.serverURL()), logger);
                    }

                    HKPKeyServerClientSettings clientSetting = new HKPKeyServerClientSettingsImpl(server.serverURL());

                    if (StringUtils.isNotEmpty(server.lookupKeysPath())) {
                        clientSetting.setLookupKeysPath(server.lookupKeysPath());
                    }

                    if (StringUtils.isNotEmpty(server.submitKeysPath())) {
                        clientSetting.setSubmitKeysPath(server.submitKeysPath());
                    }

                    clientSettings.add(clientSetting);
                }

                servers.setClientSettings(clientSettings);

                keyServerClient.setServers(servers);
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    /**
     * Returns the registered HPK key servers
     */
    @GetMapping(path=RestPaths.PGP_KEY_SERVER_GET_PGP_KEY_SERVERS_PATH)
    public List<PGPKeyServerDTO.PGPKeyServerClientSettings> getPGPKeyServers()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_PGP_KEY_SERVERS);

            try {
                HKPKeyServers servers = keyServerClient.getServers();

                List<PGPKeyServerDTO.PGPKeyServerClientSettings> result = new LinkedList<>();

                for (HKPKeyServerClientSettings clientSettings : servers.getClientSettings())
                {
                    result.add(new PGPKeyServerDTO.PGPKeyServerClientSettings(
                            clientSettings.getServerURL(),
                            clientSettings.getLookupKeysPath(),
                            clientSettings.getSubmitKeysPath()));
                }

                return result;
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }
}
