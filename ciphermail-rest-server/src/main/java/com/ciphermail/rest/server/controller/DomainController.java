/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.rest.core.DomainDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@RestController
@Tag(name = "Domain")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class DomainController
{
    private static final Logger logger = LoggerFactory.getLogger(DomainController.class);

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        ADD_DOMAIN,
        IS_DOMAIN,
        DELETE_DOMAIN,
        DELETE_DOMAINS,
        GET_DOMAINS,
        GET_DOMAINS_COUNT,
        IS_DOMAIN_IN_USE;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.DOMAIN,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.DOMAIN, action.toPermissionName());
    }

    public DomainController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_DOMAIN_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.DOMAIN_ADD_DOMAIN_PATH)
    public boolean addDomain(@RequestParam String domain)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.ADD_DOMAIN);

            try {
                String validatedDomain = restValidator.validateDomain(domain, DomainUtils.DomainType.WILD_CARD, logger);

                UserPreferences newDomainPreferences = null;

                if (domainManager.getDomainPreferences(validatedDomain) == null) {
                    newDomainPreferences = domainManager.addDomain(domain);
                }

                return newDomainPreferences != null;
            }
            catch (HierarchicalPropertiesException | CloseableIteratorException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        }));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_DOMAIN_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.DOMAIN_IS_DOMAIN_PATH)
    public boolean isDomain(@RequestParam String domain)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.IS_DOMAIN);

            return domainManager.getDomainPreferences(restValidator.validateDomain(domain,
                    DomainUtils.DomainType.WILD_CARD, logger)) != null;
        }));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_DOMAIN_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.DOMAIN_DELETE_DOMAIN_PATH)
    public boolean deleteDomain(@RequestParam String domain)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.DELETE_DOMAIN);

            boolean deleted = false;

            UserPreferences domainPreferences = domainManager.getDomainPreferences(
                    restValidator.validateDomain(domain, DomainUtils.DomainType.WILD_CARD, logger));

            if (domainPreferences != null) {
                deleted = domainManager.deleteDomainPreferences(domainPreferences);
            }

            return deleted;
        }));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_DOMAIN_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.DOMAIN_DELETE_DOMAINS_PATH)
    public List<String> deleteDomains(@RequestBody List<String> domains)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(DomainController.Action.DELETE_DOMAINS);

            List<String> notDeleted = new LinkedList<>();

            for (String domain : domains)
            {
                UserPreferences domainPreferences = domainManager.getDomainPreferences(
                        restValidator.validateDomain(domain, DomainUtils.DomainType.WILD_CARD, logger));

                if (domainPreferences != null)
                {
                    // check if domain is in-use because an in-use domain cannot be deleted
                    if (!domainManager.isDomainInUse(domain)) {
                        domainManager.deleteDomainPreferences(domainPreferences);
                    }
                    else {
                        notDeleted.add(domain);
                    }
                }
            }

            return notDeleted;
        });
    }

    @GetMapping(RestPaths.DOMAIN_GET_DOMAINS_PATH)
    public List<DomainDTO.DomainValue> getDomains(
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults,
            @RequestParam(required = false) SortDirection sortDirection)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_DOMAINS);

            List<DomainDTO.DomainValue> domains = new LinkedList<>();

            try (CloseableIterator<String> domainIterator = domainManager.getDomainIterator(firstResult,
                    restValidator.limitMaxResults(maxResults, logger), sortDirection))
            {
                while (domainIterator.hasNext())
                {
                    String domain = domainIterator.next();

                    domains.add(new DomainDTO.DomainValue(domain, domainManager.isDomainInUse(domain)));
                }
                return domains;
            }
            catch (CloseableIteratorException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.DOMAIN_GET_DOMAINS_COUNT_PATH)
    public Long getDomainCount() {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_DOMAINS_COUNT);

            return domainManager.getDomainCount();
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_DOMAIN_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.DOMAIN_IS_DOMAIN_IN_USE_PATH)
    public boolean isDomainInUse(@RequestParam String domain)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.IS_DOMAIN_IN_USE);

            return domainManager.isDomainInUse(domain);
        }));
    }
}
