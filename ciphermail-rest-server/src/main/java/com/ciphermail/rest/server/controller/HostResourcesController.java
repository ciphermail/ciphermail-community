/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.HostResources;
import com.ciphermail.core.common.util.Resource;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@Tag(name = "HostResources")
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class HostResourcesController
{
    private static final Logger logger = LoggerFactory.getLogger(HostResourcesController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private HostResources hostResources;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    /**
     * Retrieves a public resource associated with the specified ID and hostname.
     * The resource is returned as a {@link ByteArrayResource} within the response.
     * A public resource should be accessible without authentication and without requiring any permission.
     *
     * @param id the unique identifier for the resource to be retrieved
     * @param hostname the name of the host from which the resource is to be retrieved
     * @return a {@link ResponseEntity} containing the resource as a {@link ByteArrayResource},
     *         or {@code null} if the resource is not found
     */
    @GetMapping(RestPaths.HOST_RESOURCES_GET_PUBLIC_RESOURCE)
    public ResponseEntity<ByteArrayResource> getPublicResource(
            @RequestParam String id,
            @RequestParam(required = false) String hostname)
    {
        return transactionOperations.execute(tx ->
        {
            try {
                Resource resource = hostResources.getResource(id, hostname);

                if (resource == null) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                            "Resource with id %s and hostname %s not found".formatted(id, hostname), logger);
                }

                return ResponseEntity.ok()
                        .contentType(MediaType.parseMediaType(resource.getContentType()))
                        .body(new ByteArrayResource(resource.getContentAsByteArray()));
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.HOST_RESOURCES_GET_ENCODED_PUBLIC_RESOURCE)
    public String getEncodedPublicResource(
            @RequestParam String id,
            @RequestParam(required = false) String hostname)
    {
        return transactionOperations.execute(tx ->
        {
            try {
                return hostResources.getEncodedResource(id, hostname);
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }
}
