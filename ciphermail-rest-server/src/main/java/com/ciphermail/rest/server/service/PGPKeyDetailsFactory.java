/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import com.ciphermail.core.common.security.openpgp.PGPExpirationChecker;
import com.ciphermail.core.common.security.openpgp.PGPKeyFlagChecker;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyInspector;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidatorResult;
import com.ciphermail.core.common.security.openpgp.validator.StandardValidatorContextObjects;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import com.ciphermail.core.common.util.DateTimeUtils;
import com.ciphermail.core.common.util.Functional;
import com.ciphermail.rest.core.PGPDTO;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PGPKeyDetailsFactory
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyDetailsFactory.class);

    /*
     * For checking the validity of the public key
     */
    @Autowired
    @Qualifier("pgpPublicKeyValidator")
    private PGPPublicKeyValidator publicKeyValidator;

    /*
     * For checking whether the key is valid for encryption and/or signing
     */
    @Autowired
    private PGPKeyFlagChecker keyFlagChecker;

    /*
     * For checking the expiration date
     */
    @Autowired
    private PGPExpirationChecker expirationChecker;

    /*
     * String return result when a parameter value results in an error
     */
    static final String ERROR = "<error>";

    /***
     * Creates a PGPKeyDetails from a PGPKeyRingEntry
     */
    public PGPDTO.PGPKeyDetails createPGPKeyDetails(PGPKeyRingEntry keyRingEntry)
    {
        if (keyRingEntry == null) {
            return null;
        }

        PGPPublicKey publicKey = Functional.catchAllGet(keyRingEntry::getPublicKey, null, logger);

        PGPKeyRingEntry parentKey = keyRingEntry.getParentKey();

        PGPPublicKey parentPublicKey = parentKey != null ? Functional.catchAllGet(parentKey::getPublicKey,
                null, logger) : null;

        Context context = new ContextImpl();

        context.set(StandardValidatorContextObjects.MASTER_KEY, parentPublicKey);

        PGPPublicKeyValidatorResult validatorResult = publicKey != null ?
                publicKeyValidator.validate(publicKey, context) : null;

        return new PGPDTO.PGPKeyDetails(
            keyRingEntry.getID(),
            PGPUtils.getKeyIDHex(keyRingEntry.getKeyID()),
            parentKey != null ? PGPUtils.getKeyIDHex(parentKey.getKeyID()) : "",
            DateTimeUtils.getTimestamp(keyRingEntry.getCreationDate()),
            // The expiration date will be determined with expirationChecker and not from the keyRingEntry because
            // the expiration date from the keyRingEntry is not always correct
            publicKey != null ? Functional.catchAllGet(() ->
                    DateTimeUtils.getTimestamp(expirationChecker.getKeyExpirationDate(publicKey, parentPublicKey)), null, logger) : null,
            DateTimeUtils.getTimestamp(keyRingEntry.getInsertionDate()),
            StringUtils.defaultString(keyRingEntry.getPrivateKeyAlias()),
            Functional.catchAllGet(() -> keyRingEntry.getPrivateKey() != null, false, logger),
            isValidForEncryption(publicKey, parentPublicKey),
            isValidForSigning(publicKey, parentPublicKey),
            keyRingEntry.getFingerprint(),
            keyRingEntry.getSHA256Fingerprint(),
            publicKey != null ? publicKey.getBitStrength() : -1,
            publicKey != null ? PGPKeyUtils.getAlgorithmFriendly(publicKey) : "<unknown>",
            publicKey != null ? PGPPublicKeyInspector.getUserIDsAsStrings(publicKey) : Collections.emptySet(),
            keyRingEntry.getEmail(),
            keyRingEntry.isMasterKey(),
            parentKey != null ? parentKey.getID() : null,
            validatorResult != null && validatorResult.isValid(),
            validatorResult != null ? validatorResult.getFailureSeverity() : null,
            validatorResult != null ? validatorResult.getFailureMessage() : "");
    }

    private boolean isValidForEncryption(PGPPublicKey publicKey, PGPPublicKey parentPublicKey)
    {
        boolean validForEncryption = false;

        if (publicKey != null)
        {
            if (publicKey.isMasterKey())
            {
                validForEncryption = Functional.catchAllGet(() ->
                                keyFlagChecker.isMasterKeyValidForEncryption(publicKey),
                        false, logger);
            }
            else {
                if (parentPublicKey != null && parentPublicKey.isMasterKey())
                {
                    validForEncryption = Functional.catchAllGet(() ->
                                    keyFlagChecker.isSubKeyValidForEncryption(parentPublicKey, publicKey),
                            false, logger);
                }
                else {
                    // should not happen
                    logger.warn("parent not set or parent is not a master key");
                }
            }
        }

        return validForEncryption;
    }

    private boolean isValidForSigning(PGPPublicKey publicKey, PGPPublicKey parentPublicKey)
    {
        boolean validForSigning = false;

        if (publicKey != null)
        {
            if (publicKey.isMasterKey())
            {
                validForSigning = Functional.catchAllGet(() ->
                                keyFlagChecker.isMasterKeyValidForSigning(publicKey),
                        false, logger);
            }
            else {
                if (parentPublicKey != null && parentPublicKey.isMasterKey())
                {
                    validForSigning = Functional.catchAllGet(() ->
                                    keyFlagChecker.isSubKeyValidForSigning(parentPublicKey, publicKey),
                            false, logger);
                }
                else {
                    // should not happen
                    logger.warn("parent not set or parent is not a master key");
                }
            }
        }

        return validForSigning;
    }
}
