/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.james.MailQueueFactoryProvider;
import com.ciphermail.rest.core.MailDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.MailDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.james.queue.api.MailQueue;
import org.apache.james.queue.api.MailQueueName;
import org.apache.james.queue.api.ManageableMailQueue;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
@Tag(name = "MPAMailQueue")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class MailQueueController
{
    private static final Logger logger = LoggerFactory.getLogger(MailQueueController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MailQueueFactoryProvider mailQueueFactoryProvider;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private MailDetailsFactory mailDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        GET_QUEUE_NAMES,
        GET_QUEUED_MAILS,
        GET_QUEUED_MAIL,
        DELETE_QUEUED_MAILS,
        DELETE_QUEUED_MAIL,
        DELETE_ALL_QUEUED_MAIL,
        GET_QUEUE_SIZE;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.MAIL_QUEUE,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.MAIL_QUEUE, action.toPermissionName());
    }

    public MailQueueController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @GetMapping(RestPaths.MAIL_QUEUE_GET_QUEUE_NAMES_PATH)
    public List<String> getQueueNames()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_QUEUE_NAMES));

        return mailQueueFactoryProvider.getMailQueueFactory().listCreatedMailQueues()
                .stream().map(MailQueueName::asString).toList();
    }

    @GetMapping(RestPaths.MAIL_QUEUE_GET_QUEUED_MAILS_PATH)
    public List<MailDTO.MailDetails> getQueuedMails(
            @RequestParam String mailQueueName,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_QUEUED_MAILS));

        try {
            ManageableMailQueue mailQueue = getManageableMailQueue(mailQueueName);

            Stream<ManageableMailQueue.MailQueueItemView> mailQueueItemStream = StreamSupport.stream(
                    Spliterators.spliteratorUnknownSize(mailQueue.browse(), Spliterator.ORDERED),
                    false);

            return mailQueueItemStream
                    .skip(Optional.ofNullable(firstResult).orElse(0))
                    .limit(restValidator.limitMaxResults(Optional.ofNullable(maxResults).orElse(
                            restValidator.getMaxResultsUpperLimit()), logger)).map(v ->
            {
                try {
                    return mailDetailsFactory.toMailDetails(v.getMail());
                }
                catch (MessagingException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }).toList();
        }
        catch (MailQueue.MailQueueException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @GetMapping(RestPaths.MAIL_QUEUE_GET_QUEUED_MAIL_PATH)
    public MailDTO.MailDetailsWithMIME getQueuedMail(
            @RequestParam String mailQueueName,
            @RequestParam String mailName,
            @RequestParam(defaultValue = "false") boolean includeMIME)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_QUEUED_MAIL));

        try {
            ManageableMailQueue mailQueue = getManageableMailQueue(mailQueueName);

            Stream<ManageableMailQueue.MailQueueItemView> mailQueueItemStream = StreamSupport.stream(
                    Spliterators.spliteratorUnknownSize(mailQueue.browse(), Spliterator.ORDERED),
                    false);

            Mail mail = mailQueueItemStream.filter(m -> m.getMail().getName().equals(mailName))
                    .findFirst().map(ManageableMailQueue.MailQueueItemView::getMail).orElse(null);

            if (mail == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Mail item with name %s for %s queue does not exist",
                                mailName, mailQueueName), logger);
            }

            return mailDetailsFactory.toMailDetailsWithMIME(mail, includeMIME);
        }
        catch (MessagingException | IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(RestPaths.MAIL_QUEUE_DELETE_QUEUED_MAILS_PATH)
    public long deleteQueuedMails(
            @RequestParam String mailQueueName,
            @RequestBody List<String> names)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.DELETE_QUEUED_MAILS));

        long count = 0;

        try {
            for (String name : names) {
                count = count + getManageableMailQueue(mailQueueName).remove(ManageableMailQueue.Type.Name, name);
            }

            return count;
        }
        catch (MailQueue.MailQueueException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(RestPaths.MAIL_QUEUE_DELETE_QUEUED_MAIL_PATH)
    public long deleteQueuedMail(
            @RequestParam String mailQueueName,
            @RequestParam MailDTO.MailQueueField field,
            @RequestParam String value)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.DELETE_QUEUED_MAIL));

        try {
            return getManageableMailQueue(mailQueueName).remove(toManageableMailQueueType(field), value);
        }
        catch (MailQueue.MailQueueException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @PostMapping(RestPaths.MAIL_QUEUE_DELETE_ALL_QUEUED_MAIL_PATH)
    public long deleteAllQueuedMail(@RequestParam String mailQueueName)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.DELETE_ALL_QUEUED_MAIL));

        try {
            return getManageableMailQueue(mailQueueName).clear();
        }
        catch (MailQueue.MailQueueException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @GetMapping(RestPaths.MAIL_QUEUE_GET_QUEUE_SIZE_PATH)
    public long getQueueSize(@RequestParam String mailQueueName)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_QUEUE_SIZE));

        try {
            return getManageableMailQueue(mailQueueName).getSize();
        }
        catch (MailQueue.MailQueueException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    private ManageableMailQueue getManageableMailQueue(@Nonnull String mailQueueName)
    {
        MailQueue mailQueue = mailQueueFactoryProvider.getMailQueueFactory().getQueue(
                MailQueueName.of(mailQueueName)).orElse(null);

        if (mailQueue == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Mail queue with name %s does not exist",
                            mailQueueName), logger);
        }

        if (!(mailQueue instanceof ManageableMailQueue manageableMailQueue)) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Mail queue with name %s is not a manageable mail queue",
                            mailQueueName), logger);
        }

        return manageableMailQueue;
    }

    private ManageableMailQueue.Type toManageableMailQueueType(@Nonnull MailDTO.MailQueueField field)
    {
        return switch (field) {
            case SENDER -> ManageableMailQueue.Type.Sender;
            case RECIPIENT -> ManageableMailQueue.Type.Recipient;
            case NAME -> ManageableMailQueue.Type.Name;
        };
    }
}
