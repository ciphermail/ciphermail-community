/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.core.UserDTO;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@Tag(name = "User")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class UserController
{
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    /*
     * Retry template which will retry 3 times when a database error occurs
     *
     * UserWorkflow#getUser has a side-effect which can result in a database change (a user will inherit from a
     * domain if the domain was added after the user was added)
     * Getting a user can therefore result in a constraint violation if a user is loaded from different threads at
     * the same time. This happens for example with React when StrictMode is enabled in development mode because
     * React sideeffects are called twice. The call is therefore wrapped with a retry template to retry if a
     * database exception occurs
     */
    private final RetryTemplate retryTemplate = RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder()
            .withListener(new LoggingRetryListener(logger))
            .build();

    private enum Action
    {
        ADD_USER,
        GET_USER,
        IS_USER,
        DELETE_USER,
        DELETE_USERS,
        GET_USERS,
        GET_USERS_COUNT,
        SEARCH_USERS,
        SEARCH_USERS_COUNT;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.USER,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.USER, action.toPermissionName());
    }

    public UserController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_EMAIL_ADDRESS_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.USER_ADD_USER_PATH)
    public UserDTO.UserValue addUser(@RequestParam String email)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.ADD_USER);

            try {
                String validatedEmail = restValidator.validateEmail(email, logger);

                User user = userWorkflow.getUser(validatedEmail, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

                if (user == null) {
                    user = userWorkflow.makePersistent(userWorkflow.addUser(validatedEmail));
                }

                return getUserValue(user);
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_EMAIL_ADDRESS_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "User does not exist",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.USER_GET_USER_PATH)
    public UserDTO.UserValue getUser(@RequestParam String email)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_USER);

            try {
                return getUserValue(userWorkflow.getUser(restValidator.validateEmail(email, logger),
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST));
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_EMAIL_ADDRESS_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.USER_IS_USER_PATH)
    public boolean isUser(@RequestParam String email)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.IS_USER);

            try {
                return userWorkflow.getUser(restValidator.validateEmail(email, logger),
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST) != null;
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        }));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_EMAIL_ADDRESS_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.USER_DELETE_USER_PATH)
    public UserDTO.UserValue deleteUser(@RequestParam String email)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.DELETE_USER);

            try {
                User user = getUserInternal(email);

                UserDTO.UserValue userValue = getUserValue(user);

                return userWorkflow.deleteUser(user) ? userValue : null;
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = HttpStatusUtils.INVALID_EMAIL_ADDRESS_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.USER_DELETE_USERS_PATH)
    public List<String> deleteUsers(@RequestBody List<String> emailAddresses)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.DELETE_USERS);

            List<String> notDeleted = new LinkedList<>();

            try {
                for (String email : emailAddresses) {
                    // check if user is in-use because an in-use user cannot be deleted
                    if (!userWorkflow.isInUse(email)) {
                        userWorkflow.deleteUser(getUserInternal(email));
                    }
                    else {
                        notDeleted.add(email);
                    }
                }

                return notDeleted;
            }
            catch (AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.USER_GET_USERS_PATH)
    public List<UserDTO.UserValue> getUsers(
            @RequestParam(required = false) SortDirection sortDirection,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        // UserWorkflow#getUser has a side-effect which can result in a database change (a user will inherit from a
        // domain if the domain was added after the user was added)
        return retryTemplate.execute(ignore ->
                transactionOperations.execute(ctx ->
                {
                    checkPermission(Action.GET_USERS);

                    try (CloseableIterator<String> emailIterator = userWorkflow.getEmailIterator(firstResult,
                            restValidator.limitMaxResults(maxResults, logger), sortDirection)) {
                        return getUsers(emailIterator);
                    }
                    catch (CloseableIteratorException | AddressException | HierarchicalPropertiesException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                    }
                }));
    }

    @GetMapping(RestPaths.USER_GET_USERS_COUNT_PATH)
    public long getUsersCount()
    {
        return Optional.ofNullable(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_USERS_COUNT);

            return userWorkflow.getUserCount();
        })).orElse(0L);
    }

    @GetMapping(RestPaths.USER_SEARCH_USERS_PATH)
    public List<UserDTO.UserValue> searchUsers(
            @RequestParam String search,
            @RequestParam(required = false) SortDirection sortDirection,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        // UserWorkflow#getUser has a side-effect which can result in a database change (a user will inherit from a
        // domain if the domain was added after the user was added)
        return retryTemplate.execute(ignore ->
            transactionOperations.execute(ctx ->
            {
                checkPermission(Action.SEARCH_USERS);

                try (CloseableIterator<String> emailIterator = userWorkflow.searchEmail(search, firstResult,
                        restValidator.limitMaxResults(maxResults, logger), sortDirection))
                {
                    return getUsers(emailIterator);
                }
                catch (AddressException | CloseableIteratorException | HierarchicalPropertiesException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            })
        );
    }

    @GetMapping(RestPaths.USER_SEARCH_USERS_COUNT_PATH)
    public long searchUsersCount(@RequestParam String search)
    {
        return Optional.ofNullable(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.SEARCH_USERS_COUNT);

            return userWorkflow.getSearchEmailCount(search);
        })).orElse(0L);
    }

    private @Nonnull User getUserInternal(String email)
    {
        try {
            User user = userWorkflow.getUser(restValidator.validateEmail(email, logger),
                    UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

            if (user == null)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("User %s does not exist", email), logger);
            }

            return user;
        }
        catch (HierarchicalPropertiesException | AddressException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    private UserDTO.UserValue getUserValue(User user)
    throws HierarchicalPropertiesException, AddressException
    {
        if (user == null) {
            return null;
        }

        UserProperties properties = user.getUserPreferences().getProperties();

        return new UserDTO.UserValue(user.getEmail(), userWorkflow.isInUse(user.getEmail()),
                Optional.ofNullable(properties.getDateCreated()).orElse(0L),
                StringUtils.defaultString(properties.getComment()));
    }

    private List<UserDTO.UserValue> getUsers(CloseableIterator<String> emailIterator)
    throws CloseableIteratorException, AddressException, HierarchicalPropertiesException
    {
        List<UserDTO.UserValue> users = new LinkedList<>();

        while (emailIterator.hasNext()) {
            users.add(getUserValue(getUserInternal(emailIterator.next())));
        }

        return users;
    }
}
