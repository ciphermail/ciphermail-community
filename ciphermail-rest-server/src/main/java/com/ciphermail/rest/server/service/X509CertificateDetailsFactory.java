/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import com.ciphermail.core.common.security.PublicKeyInspector;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.crl.CRLDistributionPointsInspector;
import com.ciphermail.core.common.security.ctl.CTL;
import com.ciphermail.core.common.security.ctl.CTLEntry;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.common.util.DateTimeUtils;
import com.ciphermail.core.common.util.Functional;
import com.ciphermail.rest.core.CertificateDTO;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Component
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class X509CertificateDetailsFactory
{
    private static final Logger logger = LoggerFactory.getLogger(X509CertificateDetailsFactory.class);

    /*
     * String return result when a parameter value results in an error
     */
    static final String ERROR = "<error>";

    /*
     * Default key store to use if keyStoreProvider is no explicitly set
     */
    private final KeyStoreProvider defaultKeyStoreProvider;

    /*
     * The certificate trust list
     */
    private final CTL ctl;

    public X509CertificateDetailsFactory(
            @Nonnull @Qualifier("keyStoreProvider") KeyStoreProvider defaultKeyStoreProvider,
            @Nonnull CTLManager ctlManager)
    {
        this.defaultKeyStoreProvider = Objects.requireNonNull(defaultKeyStoreProvider);
        this.ctl = ctlManager.getCTL(CTLManager.DEFAULT_CTL);
    }

    /**
     * Creates a X509CertificateDetails from an X509CertStoreEntry
     */
    public CertificateDTO.X509CertificateDetails createX509CertificateDetails(X509CertStoreEntry certStoreEntry)
    throws IOException
    {
        return certStoreEntry != null ? createX509CertificateDetails(
                certStoreEntry.getCertificate(), certStoreEntry.getKeyAlias()) : null;
    }

    /**
     * Creates a X509CertificateDetails from an X509Certificate.
     */
    public @Nonnull CertificateDTO.X509CertificateDetails createX509CertificateDetails(@Nonnull X509Certificate certificate)
    throws IOException
    {
        return createX509CertificateDetails(certificate, null);
    }

    /**
     * Creates a X509CertificateDetails from an X509Certificate.
     */
    public @Nonnull CertificateDTO.X509CertificateDetails createX509CertificateDetails(
            @Nonnull KeyStoreProvider keyStoreProvider,
            @Nonnull X509Certificate certificate,
            String keyAlias)
    throws IOException
    {
        X509CertificateInspector inspector = new X509CertificateInspector(certificate);

        return new CertificateDTO.X509CertificateDetails(
                Functional.catchAllGet(inspector::getThumbprint, ERROR, logger),
                Functional.catchAllGet(() -> inspector.getThumbprint(Digest.SHA1), ERROR, logger),
                Functional.catchAllGet(inspector::getSerialNumberHex, ERROR, logger),
                Functional.catchAllGet(inspector::getIssuerFriendly, ERROR, logger),
                Functional.catchAllGet(inspector::getSubjectFriendly, ERROR, logger),
                Functional.catchAllGet(inspector::getSubjectKeyIdentifierHex, ERROR, logger),
                Functional.catchAllGet(inspector::isSelfSigned, null, logger),
                Functional.catchAllGet(() -> DateTimeUtils.getTimestamp(certificate.getNotBefore()), null, logger),
                Functional.catchAllGet(() -> DateTimeUtils.getTimestamp(certificate.getNotAfter()), null, logger),
                Functional.catchAllGet(inspector::isExpired, null, logger),
                Functional.catchAllGet(inspector::getEmailFromAltNames, List.of(ERROR), logger),
                Functional.catchAllGet(inspector::getEmailFromDN, List.of(ERROR), logger),
                Functional.catchAllGet(inspector::getKeyUsage, null, logger),
                Functional.catchAllGet(inspector::getExtendedKeyUsage, null, logger),
                Functional.catchAllGet(inspector::isCA, null, logger),
                Functional.catchAllGet(() ->
                {
                    BasicConstraints bc = inspector.getBasicConstraints();
                    return bc != null ? bc.getPathLenConstraint() : null;
                }, null, logger),
                Functional.catchAllGet(() -> PublicKeyInspector.getKeyLength(certificate.getPublicKey()),
                        null, logger),
                Functional.catchAllGet(() -> PublicKeyInspector.getFriendlyAlgorithm(certificate.getPublicKey()),
                        ERROR, logger),
                Functional.catchAllGet(certificate::getSigAlgName, ERROR, logger),
                Functional.catchAllGet(() -> CRLDistributionPointsInspector.getURIDistributionPointNames(
                        inspector.getCRLDistibutionPoints()), Set.of(ERROR), logger),
                keyAlias,
                Functional.catchAllGet(() -> keyAlias != null ? keyStoreProvider.getKeyStore().isKeyEntry(keyAlias) : null,
                        false, logger),
                Functional.catchAllGet(() ->
                {
                    CTLEntry entry = ctl.getEntry(inspector.getThumbprint());
                    return entry != null ? entry.getStatus() : null;
                },null, logger)
        );
    }

    /**
     * Creates a X509CertificateDetails from an X509Certificate.
     */
    public @Nonnull CertificateDTO.X509CertificateDetails createX509CertificateDetails(
            @Nonnull X509Certificate certificate,
            String keyAlias)
    throws IOException
    {
        return createX509CertificateDetails(defaultKeyStoreProvider, certificate, keyAlias);
    }
}