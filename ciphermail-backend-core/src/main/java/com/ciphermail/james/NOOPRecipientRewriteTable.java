/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.james;

import org.apache.james.core.Domain;
import org.apache.james.rrt.api.RecipientRewriteTable;
import org.apache.james.rrt.api.RecipientRewriteTableConfiguration;
import org.apache.james.rrt.lib.Mapping;
import org.apache.james.rrt.lib.MappingSource;
import org.apache.james.rrt.lib.Mappings;
import org.apache.james.rrt.lib.MappingsImpl;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;

/**
 * Implementation of {@link NOOPRecipientRewriteTable} which does nothing. This is needed because James requires an
 * implementation of {@link NOOPRecipientRewriteTable} even if we do not use it
 */
public class NOOPRecipientRewriteTable implements RecipientRewriteTable
{
    @Override
    public void addMapping(MappingSource source, Mapping mapping) {
        // empty on purpose
    }

    @Override
    public void removeMapping(MappingSource source, Mapping mapping) {
        // empty on purpose
    }

    @Override
    public void addRegexMapping(MappingSource source, String regex) {
        // empty on purpose
    }

    @Override
    public void removeRegexMapping(MappingSource source, String regex) {
        // empty on purpose
    }

    @Override
    public void addAddressMapping(MappingSource source, String address) {
        // empty on purpose
    }

    @Override
    public void removeAddressMapping(MappingSource source, String address) {
        // empty on purpose
    }

    @Override
    public void addErrorMapping(MappingSource source, String error) {
        // empty on purpose
    }

    @Override
    public void removeErrorMapping(MappingSource source, String error) {
        // empty on purpose
    }

    @Override
    public void addDomainMapping(MappingSource source, Domain realDomain) {
        // empty on purpose
    }

    @Override
    public void removeDomainMapping(MappingSource source, Domain realDomain) {
        // empty on purpose
    }

    @Override
    public void addDomainAliasMapping(MappingSource source, Domain realDomain) {
        // empty on purpose
    }

    @Override
    public void removeDomainAliasMapping(MappingSource source, Domain realDomain) {
        // empty on purpose
    }

    @Override
    public void addForwardMapping(MappingSource source, String address) {
        // empty on purpose
    }

    @Override
    public void removeForwardMapping(MappingSource source, String address) {
        // empty on purpose
    }

    @Override
    public void addGroupMapping(MappingSource source, String address) {
        // empty on purpose
    }

    @Override
    public void removeGroupMapping(MappingSource source, String address) {
        // empty on purpose
    }

    @Override
    public void addAliasMapping(MappingSource source, String address) {
        // empty on purpose
    }

    @Override
    public void removeAliasMapping(MappingSource source, String address) {
        // empty on purpose
    }

    @Override
    public Mappings getResolvedMappings(String user, Domain domain, EnumSet<Mapping.Type> mappingTypes) {
        return MappingsImpl.empty();
    }

    @Override
    public Mappings getStoredMappings(MappingSource source) {
        return MappingsImpl.empty();
    }

    @Override
    public Map<MappingSource, Mappings> getAllMappings() {
        return Collections.emptyMap();
    }

    @Override
    public RecipientRewriteTableConfiguration getConfiguration() {
        return null;
    }
}
