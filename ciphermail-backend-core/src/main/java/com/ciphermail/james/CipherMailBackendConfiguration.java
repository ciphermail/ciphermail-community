/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.james;

import java.io.File;
import java.nio.file.Files;
import java.util.Objects;

public class CipherMailBackendConfiguration
{
    private CipherMailBackendConfiguration() {
        // empty on purpose
    }

    /**
     * Name of system property which configures the base directory where CipherMail config files are stored
     */
    public static final String BACKEND_CONFIG_HOME_SYSTEM_PROPERTY = "ciphermail.config.home";

    /**
     * Name of system property which configures the base directory where var files are stored
     */
    public static final String BACKEND_VAR_HOME_SYSTEM_PROPERTY = "ciphermail.var.home";

    /*
     * Base directory where ciphermail config files are stored
     */
    private static final File BACKEND_CONFIG_HOME;

    /*
     * Base directory where var files are stored
     */
    private static final File VAR_HOME;

    static
    {
        BACKEND_CONFIG_HOME = new File(Objects.requireNonNull(System.getProperty(BACKEND_CONFIG_HOME_SYSTEM_PROPERTY),
                BACKEND_CONFIG_HOME_SYSTEM_PROPERTY + " system property is missing"));

        if (!BACKEND_CONFIG_HOME.exists()) {
            throw new IllegalArgumentException(BACKEND_CONFIG_HOME + " does not exist");
        }
        if (!BACKEND_CONFIG_HOME.isDirectory()) {
            throw new IllegalArgumentException(BACKEND_CONFIG_HOME + " is not a directory");
        }

        VAR_HOME = new File(Objects.requireNonNull(System.getProperty(BACKEND_VAR_HOME_SYSTEM_PROPERTY),
                BACKEND_VAR_HOME_SYSTEM_PROPERTY + " system property is missing"));

        if (!VAR_HOME.exists()) {
            throw new IllegalArgumentException(VAR_HOME + " does not exist");
        }
        if (!VAR_HOME.isDirectory()) {
            throw new IllegalArgumentException(VAR_HOME + " is not a directory");
        }
        if (!Files.isWritable(VAR_HOME.toPath())) {
            throw new IllegalArgumentException(VAR_HOME + " is not writable");
        }
    }

    public static File getBackendConfigHome() {
        return BACKEND_CONFIG_HOME;
    }

    public static File getVarHome() {
        return VAR_HOME;
    }
}
