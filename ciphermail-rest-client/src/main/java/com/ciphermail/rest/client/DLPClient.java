/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.rest.client.util.RestClientUtil;
import com.ciphermail.rest.core.DLPDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class DLPClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public DLPDTO.DLPPolicyPattern addPolicyPattern(
            @Nonnull String name,
            String description,
            String notes,
            @Nonnull String regEx,
            String validator,
            int threshold,
            @Nonnull PolicyViolationAction action,
            boolean delayEvaluation,
            String matchFilter)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);
        params.put("description", description);
        params.put("notes", notes);
        params.put("regEx", regEx);
        params.put("validator", validator);
        params.put("threshold", threshold);
        params.put("action", action.name());
        params.put("delayEvaluation", delayEvaluation);
        params.put("matchFilter", matchFilter);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_ADD_POLICY_PATTERN_PATH)
                        .queryParam("name", "{name}")
                        .queryParamIfPresent("description", RestClientUtil.getOptionalQueryValue(description, "{description}"))
                        .queryParamIfPresent("notes", RestClientUtil.getOptionalQueryValue(notes, "{notes}"))
                        .queryParam("regEx", "{regEx}")
                        .queryParamIfPresent("validator", RestClientUtil.getOptionalQueryValue(validator, "{validator}"))
                        .queryParam("threshold", "{threshold}")
                        .queryParam("action", "{action}")
                        .queryParam("delayEvaluation", "{delayEvaluation}")
                        .queryParamIfPresent("matchFilter", RestClientUtil.getOptionalQueryValue(matchFilter, "{matchFilter}"))
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(DLPDTO.DLPPolicyPattern.class);
    }

    public DLPDTO.DLPPolicyPattern addPolicyGroup(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_ADD_POLICY_GROUP_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(DLPDTO.DLPPolicyPattern.class);
    }

    public DLPDTO.DLPPolicyPattern updatePolicyPattern(
            @Nonnull String name,
            String description,
            String notes,
            @Nonnull String regEx,
            String validator,
            int threshold,
            @Nonnull PolicyViolationAction action,
            boolean delayEvaluation,
            String matchFilter)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);
        params.put("description", description);
        params.put("notes", notes);
        params.put("regEx", regEx);
        params.put("validator", validator);
        params.put("threshold", threshold);
        params.put("action", action.name());
        params.put("delayEvaluation", delayEvaluation);
        params.put("matchFilter", matchFilter);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_UPDATE_POLICY_PATTERN_PATH)
                        .queryParam("name", "{name}")
                        .queryParamIfPresent("description", RestClientUtil.getOptionalQueryValue(description, "{description}"))
                        .queryParamIfPresent("notes", RestClientUtil.getOptionalQueryValue(notes, "{notes}"))
                        .queryParam("regEx", "{regEx}")
                        .queryParamIfPresent("validator", RestClientUtil.getOptionalQueryValue(validator, "{validator}"))
                        .queryParam("threshold", "{threshold}")
                        .queryParam("action", "{action}")
                        .queryParam("delayEvaluation", "{delayEvaluation}")
                        .queryParamIfPresent("matchFilter", RestClientUtil.getOptionalQueryValue(matchFilter, "{matchFilter}"))
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(DLPDTO.DLPPolicyPattern.class);
    }

    public DLPDTO.DLPPolicyPattern getPolicyPattern(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_POLICY_PATTERN_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(DLPDTO.DLPPolicyPattern.class);
    }

    public List<DLPDTO.DLPPolicyPattern> getPolicyPatterns(int firstResult, int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_POLICY_PATTERNS_PATH)
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getPolicyPatternsCount()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_POLICY_PATTERNS_COUNT_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public DLPDTO.DLPPolicyPattern deletePolicyPattern(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_DELETE_POLICY_PATTERN_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(DLPDTO.DLPPolicyPattern.class);
    }

    public DLPDTO.DLPPolicyPattern renamePolicyPattern(@RequestParam String oldName, @RequestParam String newName)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("oldName", oldName);
        params.put("newName", newName);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_RENAME_POLICY_PATTERN_PATH)
                        .queryParam("oldName", "{oldName}")
                        .queryParam("newName", "{newName}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(DLPDTO.DLPPolicyPattern.class);
    }

    public Boolean isPolicyPatternReferenced(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_IS_REFERENCED_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Boolean.class);
    }

    public List<String> getPolicyPatternReferencedDetails(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_REFERENCED_DETAILS_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Boolean addChildNode(@RequestParam String parentNode, @RequestParam String childNode)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("parentNode", parentNode);
        params.put("childNode", childNode);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_ADD_CHILD_NODE_PATH)
                        .queryParam("parentNode", "{parentNode}")
                        .queryParam("childNode", "{childNode}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Boolean.class);
    }

    public Boolean removeChildNode(@RequestParam String parentNode, @RequestParam String childNode)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("parentNode", parentNode);
        params.put("childNode", childNode);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_REMOVE_CHILD_NODE_PATH)
                        .queryParam("parentNode", "{parentNode}")
                        .queryParam("childNode", "{childNode}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Boolean.class);
    }

    public String extractTextFromMimeMessage(@Nonnull Resource mime)
    {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        parts.add("mime", mime);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_EXTRACT_TEXT_FROM_EMAIL_PATH)
                        .build())
                .accept(MediaType.TEXT_PLAIN)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(parts)
                .retrieve()
                .body(String.class);
    }

    public List<DLPDTO.DLPMatchFilter> getMatchFilters()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_MATCH_FILTERS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public DLPDTO.DLPMatchFilter getMatchFilter(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_MATCH_FILTER_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(DLPDTO.DLPMatchFilter.class);
    }

    public List<DLPDTO.DLPValidator> getValidators()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_VALIDATORS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public DLPDTO.DLPValidator getValidator(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_VALIDATOR_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(DLPDTO.DLPValidator.class);
    }

    public String getSkipList()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_SKIP_LIST_PATH)
                        .build())
                .accept(MediaType.TEXT_PLAIN)
                .retrieve()
                .body(String.class);
    }

    public void setSkipList(@Nonnull Resource skipList)
    {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        parts.add("skipList", skipList);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_SET_SKIP_LIST_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(parts)
                .retrieve()
                .body(Void.class);
    }

    public void setUserPolicyPatterns(
            @Nonnull String email,
            @Nonnull List<String> patternNames)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_SET_USER_POLICY_PATTERNS_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(patternNames)
                .retrieve()
                .body(Void.class);
    }

    public List<DLPDTO.DLPPolicyPattern> getUserPolicyPatterns(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_USER_POLICY_PATTERNS_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setDomainPolicyPatterns(
            @Nonnull String domain,
            @Nonnull List<String> patternNames)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_SET_DOMAIN_POLICY_PATTERNS_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(patternNames)
                .retrieve()
                .body(Void.class);
    }

    public List<DLPDTO.DLPPolicyPattern> getDomainPolicyPatterns(@Nonnull String domain)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_DOMAIN_POLICY_PATTERNS_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setGlobalPolicyPatterns(@Nonnull List<String> patternNames)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_SET_GLOBAL_POLICY_PATTERNS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(patternNames)
                .retrieve()
                .body(Void.class);
    }

    public List<DLPDTO.DLPPolicyPattern> getGlobalPolicyPatterns()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DLP_GET_GLOBAL_POLICY_PATTERNS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }
}
