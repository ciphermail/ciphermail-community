/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.core.TLSDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class TLSClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public TLSDTO.CSRStoreEntry createCSR(
            @Nonnull List<String> domains,
            String email,
            String organisation,
            String organisationalUnit,
            String countryCode,
            String state,
            String locality,
            @Nonnull String commonName,
            String givenName,
            String surname,
            @Nonnull TLSDTO.KeyAlgorithm keyAlgorithm)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyAlgorithm", keyAlgorithm);

        TLSDTO.Request subject = new TLSDTO.Request()
                .setDomains(domains)
                .setEmail(email)
                .setOrganisation(organisation)
                .setOrganisationalUnit(organisationalUnit)
                .setCountryCode(countryCode)
                .setState(state)
                .setLocality(locality)
                .setCommonName(commonName)
                .setGivenName(givenName)
                .setSurname(surname);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.TLS_CREATE_CSR_PATH)
                        .queryParam("keyAlgorithm", "{keyAlgorithm}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(subject)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<TLSDTO.CSRStoreEntry> getCSRs()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.TLS_GET_CSRS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public TLSDTO.CSRStoreEntry getCSR(@Nonnull String id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.TLS_GET_CSR_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void deleteCSR(@Nonnull String id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.TLS_DELETE_CSR_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String exportPKCS10Request(@Nonnull String id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.TLS_EXPORT_PKCS10_REQUEST_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public TLSDTO.CSRStoreEntry importCertificateChain(@Nonnull Resource chain)
    {
        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();

        requestBody.add("chain", chain);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.TLS_IMPORT_CERTIFICATE_CHAIN_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String exportCertificateChain(@Nonnull String id, @Nonnull String exportPassword)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.TLS_EXPORT_CERTIFICATE_CHAIN_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(exportPassword)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void installCertificate(@Nonnull String id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.TLS_INSTALL_CERTIFICATE_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public void importPKCS12(
            @Nonnull Resource resource,
            String keyStorePassword)
    {
        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();

        requestBody.add("pkcs12", resource);
        requestBody.add("keyStorePassword", StringUtils.defaultString(keyStorePassword));

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.TLS_IMPORT_PKCS12_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(Void.class);
    }
}
