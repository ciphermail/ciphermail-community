/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.core.MailDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class MailRepositoryClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public List<String> getMailRepositoryUrls()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MAIL_REPOSITORY_GET_MAIL_REPOSITORY_URLS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<String> getMailKeys(@Nonnull String mailRepositoryUrl)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("mailRepositoryUrl", mailRepositoryUrl);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MAIL_REPOSITORY_GET_MAIL_KEYS_PATH)
                        .queryParam("mailRepositoryUrl", "{mailRepositoryUrl}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getMailRepositorySize(@Nonnull String mailRepositoryUrl)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("mailRepositoryUrl", mailRepositoryUrl);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MAIL_REPOSITORY_GET_SIZE_PATH)
                        .queryParam("mailRepositoryUrl", "{mailRepositoryUrl}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public MailDTO.MailDetailsWithMIME getMail(
            @Nonnull String mailRepositoryUrl,
            @Nonnull String key,
            boolean includeMIME)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("mailRepositoryUrl", mailRepositoryUrl);
        params.put("key", key);
        params.put("includeMIME", includeMIME);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MAIL_REPOSITORY_GET_MAIL_PATH)
                        .queryParam("mailRepositoryUrl", "{mailRepositoryUrl}")
                        .queryParam("key", "{key}")
                        .queryParam("includeMIME", "{includeMIME}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void deleteMail(
            @Nonnull String mailRepositoryUrl,
            @Nonnull String key)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("mailRepositoryUrl", mailRepositoryUrl);
        params.put("key", key);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MAIL_REPOSITORY_DELETE_MAIL_PATH)
                        .queryParam("mailRepositoryUrl", "{mailRepositoryUrl}")
                        .queryParam("key", "{key}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public void deleteAllMail(@Nonnull String mailRepositoryUrl)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("mailRepositoryUrl", mailRepositoryUrl);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MAIL_REPOSITORY_DELETE_ALL_MAIL_PATH)
                        .queryParam("mailRepositoryUrl", "{mailRepositoryUrl}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }
}
