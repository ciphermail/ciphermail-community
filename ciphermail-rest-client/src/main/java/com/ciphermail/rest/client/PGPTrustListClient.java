/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class PGPTrustListClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public List<PGPDTO.PGPTrustListResult> getPGPTrustListEntries(
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRIES_PATH)
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getPGPTrustListEntriesCount()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_COUNT_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public PGPDTO.PGPTrustListResult getPGPTrustListEntry(@Nonnull String sha256Fingerprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("sha256Fingerprint", sha256Fingerprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRY_PATH)
                        .queryParam("sha256Fingerprint", "{sha256Fingerprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(PGPDTO.PGPTrustListResult.class);
    }

    public void setPGPTrustListEntry(
            @Nonnull String sha256Fingerprint,
            @Nonnull PGPTrustListStatus status,
            boolean includeSubkeys)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("sha256Fingerprint", sha256Fingerprint);
        params.put("status", status);
        params.put("includeSubkeys", includeSubkeys);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_TRUST_LIST_SET_PGP_TRUST_LIST_ENTRY_PATH)
                        .queryParam("sha256Fingerprint", "{sha256Fingerprint}")
                        .queryParam("status", "{status}")
                        .queryParam("includeSubkeys", "{includeSubkeys}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public void deletePGPTrustListEntry(@Nonnull String sha256Fingerprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("sha256Fingerprint", sha256Fingerprint);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_TRUST_LIST_DELETE_PGP_TRUST_LIST_ENTRY_PATH)
                        .queryParam("sha256Fingerprint", "{sha256Fingerprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }
}
