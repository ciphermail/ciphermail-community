package com.ciphermail.rest.client;

public interface RestClientSettingsProvider
{
    /**
     * The baseURL of the back-end to connect to. Example: <a href="http://localhost:8000">...</a>
     */
    String getBaseUrl();

    /**
     * The username for authenticating against the back-end
     */
    String getUsername();

    /**
     * The password for authenticating against the back-end
     */
    String getPassword();

    /**
     * TOTP code (required if 2FA is enabled)
     */
    String getTOTP();
}
