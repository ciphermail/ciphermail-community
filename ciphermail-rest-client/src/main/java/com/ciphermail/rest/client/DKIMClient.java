/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.core.DKIMDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class DKIMClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public void generateKey(@Nonnull String keyId, int keyLength)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyId", keyId);
        params.put("keyLength", keyLength);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DKIM_GENERATE_KEY_PATH)
                        .queryParam("keyId", "{keyId}")
                        .queryParam("keyLength", "{keyLength}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public List<String> getKeyIds(int firstResult, int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DKIM_GET_KEY_IDS_PATH)
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setKey(@Nonnull String keyId, @Nonnull Resource resource)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyId", keyId);

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        parts.add("pemEncodedKeyPair", resource);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DKIM_SET_KEY_PATH)
                        .queryParam("keyId", "{keyId}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(parts)
                .retrieve()
                .body(Void.class);
    }

    public String getPublicKey(@Nonnull String keyId)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyId", keyId);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DKIM_GET_PUBLIC_KEY_PATH)
                        .queryParam("keyId", "{keyId}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(String.class);
    }

    public String getKeyPair(@Nonnull String keyId)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyId", keyId);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DKIM_GET_KEY_PAIR_PATH)
                        .queryParam("keyId", "{keyId}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(String.class);
    }

    public String isKeyAvailable(@Nonnull String keyId)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyId", keyId);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DKIM_IS_KEY_AVAILABLE_PATH)
                        .queryParam("keyId", "{keyId}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(String.class);
    }

    public String deleteKey(@Nonnull String keyId)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyId", keyId);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DKIM_DELETE_KEY_PATH)
                        .queryParam("keyId", "{keyId}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(String.class);
    }

    public DKIMDTO.SignatureTemplate parseSignatureTemplate(@Nonnull Resource signatureTemplate)
    {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        parts.add("signatureTemplate", signatureTemplate);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.DKIM_PARSE_SIGNATURE_TEMPLATE_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(parts)
                .retrieve()
                .body(DKIMDTO.SignatureTemplate.class);
    }
}
