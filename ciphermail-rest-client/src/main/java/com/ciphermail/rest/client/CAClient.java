/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.common.security.ca.CACertificateSignatureAlgorithm;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.rest.core.CADTO;
import com.ciphermail.rest.core.CAKeyUsageFilter;
import com.ciphermail.rest.core.CRLDTO;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
@SuppressWarnings({"java:S6813"})
public class CAClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public CADTO.NewCAResponse createCA(
            int rootDaysValid,
            int intermediateDaysValid,
            int rootKeyLength,
            int intermediateKeyLength,
            @Nonnull CACertificateSignatureAlgorithm signatureAlgorithm,
            String crlDistributionPoint,
            String rootEmail,
            String rootOrganisation,
            String rootOrganisationalUnit,
            String rootCountryCode,
            String rootState,
            String rootLocality,
            @Nonnull String rootCommonName,
            String rootGivenName,
            String rootSurname,
            String intermediateEmail,
            String intermediateOrganisation,
            String intermediateOrganisationalUnit,
            String intermediateCountryCode,
            String intermediateState,
            String intermediateLocality,
            @Nonnull String intermediateCommonName,
            String intermediateGivenName,
            String intermediateSurname)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("rootDaysValid", rootDaysValid);
        params.put("intermediateDaysValid", intermediateDaysValid);
        params.put("rootKeyLength", rootKeyLength);
        params.put("intermediateKeyLength", intermediateKeyLength);
        params.put("signatureAlgorithm", signatureAlgorithm);
        params.put("crlDistributionPoint", crlDistributionPoint);

        CADTO.NewCASubjects subjects = new CADTO.NewCASubjects(
                new CADTO.X500PrincipalRequest(
                        rootEmail,
                        rootOrganisation,
                        rootOrganisationalUnit,
                        rootCountryCode,
                        rootState,
                        rootLocality,
                        rootCommonName,
                        rootGivenName,
                        rootSurname),
                new CADTO.X500PrincipalRequest(
                        intermediateEmail,
                        intermediateOrganisation,
                        intermediateOrganisationalUnit,
                        intermediateCountryCode,
                        intermediateState,
                        intermediateLocality,
                        intermediateCommonName,
                        intermediateGivenName,
                        intermediateSurname)
        );

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_CREATE_CA_PATH)
                        .queryParam("rootDaysValid", "{rootDaysValid}")
                        .queryParam("intermediateDaysValid", "{intermediateDaysValid}")
                        .queryParam("rootKeyLength", "{rootKeyLength}")
                        .queryParam("intermediateKeyLength", "{intermediateKeyLength}")
                        .queryParam("signatureAlgorithm", "{signatureAlgorithm}")
                        .queryParam("crlDistributionPoint", "{crlDistributionPoint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(subjects)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getAvailableCAs(@Nonnull CAKeyUsageFilter keyUsageFilter)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyUsageFilter", keyUsageFilter);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_GET_AVAILABLE_CAS_PATH)
                        .queryParam("keyUsageFilter", "{keyUsageFilter}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CADTO.CertificateRequestHandlerDetails> getCertificateRequestHandlers()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_GET_CERTIFICATE_REQUEST_HANDLERS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public CADTO.NewRequest requestNewCertificate(
            @Nonnull String email,
            String subjectEmail,
            String subjectOrganisation,
            String subjectOrganisationalUnit,
            String subjectCountryCode,
            String subjectState,
            String subjectLocality,
            @Nonnull String subjectCommonName,
            String subjectGivenName,
            String subjectSurname,
            int daysValid,
            int keyLength,
            @Nonnull CACertificateSignatureAlgorithm signatureAlgorithm,
            String crlDistributionPoint,
            @Nonnull String certificateRequestHandler)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("daysValid", daysValid);
        params.put("keyLength", keyLength);
        params.put("signatureAlgorithm", signatureAlgorithm);
        params.put("crlDistributionPoint", crlDistributionPoint);
        params.put("certificateRequestHandler", certificateRequestHandler);

        CADTO.X500PrincipalRequest subject = new CADTO.X500PrincipalRequest(
                subjectEmail,
                subjectOrganisation,
                subjectOrganisationalUnit,
                subjectCountryCode,
                subjectState,
                subjectLocality,
                subjectCommonName,
                subjectGivenName,
                subjectSurname);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_REQUEST_NEW_CERTIFICATE_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("daysValid", "{daysValid}")
                        .queryParam("keyLength", "{keyLength}")
                        .queryParam("signatureAlgorithm", "{signatureAlgorithm}")
                        .queryParam("crlDistributionPoint", "{crlDistributionPoint}")
                        .queryParam("certificateRequestHandler", "{certificateRequestHandler}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(subject)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CADTO.PendingRequest> getPendingRequests(int firstResult, int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_GET_PENDING_REQUESTS_PATH)
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getPendingRequestsCount()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public List<CADTO.PendingRequest> getPendingRequestsByEmail(@Nonnull String email, @Nonnull Match match, int firstResult, int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("match", match);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("match", "{match}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getPendingRequestsByEmailCount(@Nonnull String email, @Nonnull Match match)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("match", match);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_COUNT_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("match", "{match}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public CADTO.PendingRequest getPendingRequest(@Nonnull UUID id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_GET_PENDING_REQUEST_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void deletePendingRequest(@Nonnull UUID id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_DELETE_PENDING_REQUEST_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public CADTO.PendingRequest rescheduleRequest(@Nonnull UUID id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_RESCHEDULE_REQUEST_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> finalizePendingRequest(@Nonnull Resource resource)
    {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        parts.add("certificates", resource);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_FINALIZE_PENDING_REQUEST_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(parts)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public CertificateDTO.X509CertificateDetails createSelfSignedCertificate(
            @Nonnull String email,
            String subjectEmail,
            String subjectOrganisation,
            String subjectOrganisationalUnit,
            String subjectCountryCode,
            String subjectState,
            String subjectLocality,
            @Nonnull String subjectCommonName,
            String subjectGivenName,
            String subjectSurname,
            int daysValid,
            int keyLength,
            @Nonnull CACertificateSignatureAlgorithm signatureAlgorithm,
            boolean whiteList)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("daysValid", daysValid);
        params.put("keyLength", keyLength);
        params.put("signatureAlgorithm", signatureAlgorithm);
        params.put("whiteList", whiteList);

        CADTO.X500PrincipalRequest subject = new CADTO.X500PrincipalRequest(
                subjectEmail,
                subjectOrganisation,
                subjectOrganisationalUnit,
                subjectCountryCode,
                subjectState,
                subjectLocality,
                subjectCommonName,
                subjectGivenName,
                subjectSurname);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_CREATE_SELF_SIGNED_CERTIFICATE_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("daysValid", "{daysValid}")
                        .queryParam("keyLength", "{keyLength}")
                        .queryParam("signatureAlgorithm", "{signatureAlgorithm}")
                        .queryParam("whiteList", "{whiteList}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(subject)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public CRLDTO.X509CRLDetails createCRL(
            @Nonnull String issuerThumbprint,
            long nextUpdate,
            boolean updateExistingCRL,
            @Nonnull CACertificateSignatureAlgorithm signatureAlgorithm,
            @Nonnull List<String> serialNumbersHex)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("issuerThumbprint", issuerThumbprint);
        params.put("nextUpdate", nextUpdate);
        params.put("updateExistingCRL", updateExistingCRL);
        params.put("signatureAlgorithm", signatureAlgorithm);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CA_CREATE_CRL_PATH)
                        .queryParam("issuerThumbprint", "{issuerThumbprint}")
                        .queryParam("nextUpdate", "{nextUpdate}")
                        .queryParam("updateExistingCRL", "{updateExistingCRL}")
                        .queryParam("signatureAlgorithm", "{signatureAlgorithm}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(serialNumbersHex)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }
}
