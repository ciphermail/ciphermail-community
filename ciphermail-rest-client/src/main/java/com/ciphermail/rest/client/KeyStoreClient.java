/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.core.KeyStoreDTO;
import com.ciphermail.rest.core.KeyStoreName;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class KeyStoreClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public List<String> getAliases(
            @Nonnull KeyStoreName keyStoreName,
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_GET_ALIASES_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getKeyStoreSize(@Nonnull KeyStoreName keyStoreName)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_GET_KEYSTORE_SIZE_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public KeyStoreDTO.CertificateDetails getCertificate(
            @Nonnull KeyStoreName keyStoreName,
            @Nonnull String alias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);
        params.put("alias", alias);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_GET_CERTIFICATE_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .queryParam("alias", "{alias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(KeyStoreDTO.CertificateDetails.class);
    }

    public List<KeyStoreDTO.CertificateDetails> getChain(
            @Nonnull KeyStoreName keyStoreName,
            @Nonnull String alias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);
        params.put("alias", alias);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_GET_CERTIFICATE_CHAIN_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .queryParam("alias", "{alias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Boolean containsAlias(
            @Nonnull KeyStoreName keyStoreName,
            @Nonnull String alias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);
        params.put("alias", alias);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_CONTAINS_ALIAS_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .queryParam("alias", "{alias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Boolean.class);
    }

    public Boolean isCertificateEntry(
            @Nonnull KeyStoreName keyStoreName,
            @Nonnull String alias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);
        params.put("alias", alias);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_IS_CERTIFICATE_ENTRY_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .queryParam("alias", "{alias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Boolean.class);
    }

    public Boolean isKeyEntry(
            @Nonnull KeyStoreName keyStoreName,
            @Nonnull String alias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);
        params.put("alias", alias);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_IS_KEY_ENTRY_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .queryParam("alias", "{alias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Boolean.class);
    }

    public void deleteEntry(
            @Nonnull KeyStoreName keyStoreName,
            @Nonnull String alias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);
        params.put("alias", alias);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_DELETE_ENTRY_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .queryParam("alias", "{alias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public void renameEntry(
            @Nonnull KeyStoreName keyStoreName,
            @Nonnull String oldAlias,
            @Nonnull String newAlias,
            String keyEntryPassword)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);
        params.put("oldAlias", oldAlias);
        params.put("newAlias", newAlias);

        KeyStoreDTO.RenameEntryRequestBody requestBody = new KeyStoreDTO.RenameEntryRequestBody(keyEntryPassword);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_RENAME_ALIAS_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .queryParam("oldAlias", "{oldAlias}")
                        .queryParam("newAlias", "{newAlias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(Void.class);
    }

    public byte[] exportToPKCS12(
            @Nonnull KeyStoreName keyStoreName,
            @Nonnull String keyStorePassword,
            @Nonnull String exportPassword,
            @Nonnull List<String> aliases)
    {
        KeyStoreDTO.ExportToPKCS12RequestBody requestBody = new KeyStoreDTO.ExportToPKCS12RequestBody(
                keyStorePassword, exportPassword, aliases);

        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_EXPORT_TO_PKCS12_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(byte[].class);
    }

    public Integer importPKCS12(
            @Nonnull KeyStoreName keyStoreName,
            @Nonnull String keyStorePassword,
            @Nonnull Resource resource)
    {
        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();

        requestBody.add("keyStorePassword", keyStorePassword);
        requestBody.add("pkcs12", resource);

        Map<String, Object> params = new HashMap<>();

        params.put("keyStoreName", keyStoreName);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.KEYSTORE_IMPORT_PKCS12_PATH)
                        .queryParam("keyStoreName", "{keyStoreName}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(Integer.class);
    }
}
