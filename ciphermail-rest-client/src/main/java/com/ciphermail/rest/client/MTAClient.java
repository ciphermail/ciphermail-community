/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.client.util.RestClientUtil;
import com.ciphermail.rest.core.MTADTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class MTAClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public List<MTADTO.QueueItem> getQueueList(int firstResult, int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_GET_QUEUE_LIST_PATH)
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getQueueSize()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_GET_QUEUE_SIZE_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public void flushQueue()
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_FLUSH_QUEUE_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public void rescheduleDeferredMail(@Nonnull List<String> queueIDs)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_RESCHEDULE_DEFERRED_MAIL_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(queueIDs)
                .retrieve()
                .body(Void.class);
    }

    public void deleteMailFromQueue(@Nonnull List<String> queueIDs)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_DELETE_MAIL_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(queueIDs)
                .retrieve()
                .body(Void.class);
    }

    public void holdMailFromQueue(@Nonnull List<String> queueIDs)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_HOLD_MAIL_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(queueIDs)
                .retrieve()
                .body(Void.class);
    }

    public void releaseMailFromQueue(@Nonnull List<String> queueIDs)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_RELEASE_MAIL_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(queueIDs)
                .retrieve()
                .body(Void.class);
    }

    public void bounceMailFromQueue(@Nonnull List<String> queueIDs)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_BOUNCE_MAIL_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(queueIDs)
                .retrieve()
                .body(Void.class);
    }

    public void requeueMailFromQueue(@Nonnull List<String> queueIDs)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_REQUEUE_MAIL_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(queueIDs)
                .retrieve()
                .body(Void.class);
    }

    public String getMail(@Nonnull String queueID, Long maxSize)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("queueID", queueID);
        params.put("maxSize", maxSize);

        return StringUtils.trimToNull(restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_GET_MAIL_PATH)
                        .queryParam("queueID", "{queueID}")
                        .queryParamIfPresent("maxSize", RestClientUtil.getOptionalQueryValue(maxSize, "{maxSize}"))
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(String.class));
    }

    public String getMainConfig()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_GET_MAIN_CONFIG_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(String.class);
    }

    public void setMainConfig(@Nonnull String config)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_SET_MAIN_CONFIG_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(config)
                .retrieve()
                .body(Void.class);
    }

    public void startPostfix()
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_START_POSTFIX_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public void stopPostfix()
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_STOP_POSTFIX_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public void restartPostfix()
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_RESTART_POSTFIX_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public String getPostfixStatus()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_GET_POSTFIX_STATUS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(String.class);
    }

    public boolean isPostfixRunning()
    {
        return Boolean.TRUE.equals(restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_IS_POSTFIX_RUNNING_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Boolean.class));
    }

    public void setStandardMainSettings(@Nonnull MTADTO.StandardMainSettings settings)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_SET_STANDARD_SETTINGS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(settings)
                .retrieve()
                .body(Void.class);
    }

    public MTADTO.StandardMainSettings getStandardMainSettings()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_GET_STANDARD_SETTINGS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(MTADTO.StandardMainSettings.class);
    }

    public List<MTADTO.PostfixMap> getMaps()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_LIST_MAPS)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String getMapContent(@Nonnull MTADTO.PostfixMapType type, @Nonnull String name)
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_GET_MAP_CONTENT)
                        .queryParam("type", type)
                        .queryParam("name", name)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(String.class);
    }

    public void setMapContent(@Nonnull MTADTO.PostfixMapType type, @Nonnull String name, String content)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_SET_MAP_CONTENT)
                        .queryParam("type", type)
                        .queryParam("name", name)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(StringUtils.defaultString(content))
                .retrieve()
                .body(Void.class);
    }

    public MTADTO.PostfixMap createMap(@Nonnull MTADTO.PostfixMapType type, @Nonnull String name)
    {
        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_CREATE_MAP)
                        .queryParam("type", type)
                        .queryParam("name", name)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(MTADTO.PostfixMap.class);
    }

    public void deleteMap(@Nonnull MTADTO.PostfixMapType type, @Nonnull String name)
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_DELETE_MAP)
                        .queryParam("type", type)
                        .queryParam("name", name)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public void sendEmailMime(
            String sender,
            @Nonnull List<String> recipients,
            @Nonnull MTADTO.MTAPort mtaPort,
            @Nonnull Resource mime)
    {
        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();

        if (sender != null) {
            requestBody.add("sender", sender);
        }

        requestBody.add("recipients", recipients);
        requestBody.add("mime", mime);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_SEND_EMAIL_MIME)
                        .queryParam("mtaPort", mtaPort)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(Void.class);
    }

    public void sendEmail(
            @Nonnull String subject,
            @Nonnull String from,
            String textBody,
            String htmlBody,
            String sender,
            @Nonnull List<String> recipients,
            List<String> to,
            List<String> cc,
            @Nonnull MTADTO.MTAPort mtaPort,
            Resource attachment)
    {
        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();

        requestBody.add("subject", subject);
        requestBody.add("from", from);

        if (textBody != null) {
            requestBody.add("textBody", textBody);
        }

        if (htmlBody != null) {
            requestBody.add("htmlBody", htmlBody);
        }

        if (sender != null) {
            requestBody.add("sender", sender);
        }

        requestBody.add("recipients", recipients);

        if (to != null) {
            requestBody.add("to", to);
        }

        if (cc != null) {
            requestBody.add("cc", cc);
        }

        if (attachment != null) {
            requestBody.add("attachment", attachment);
        }

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.MTA_SEND_EMAIL)
                        .queryParam("mtaPort", mtaPort)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(Void.class);
    }
}
