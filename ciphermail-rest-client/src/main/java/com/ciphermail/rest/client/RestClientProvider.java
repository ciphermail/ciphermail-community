/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.common.security.TrustAllX509TrustManager;
import com.ciphermail.core.common.util.CannotContinueRuntimeException;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.JdkClientHttpRequestFactory;
import org.springframework.web.client.RestClient;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.net.http.HttpClient;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Consumer;

public class RestClientProvider
{
    public record CsrfTokenDetails(
            @NotNull String headerName,
            @NotNull String token) {}

    /*
     * Provides connection settings like baseURL, username etc. for connecting to the back-end
     */
    private final RestClientSettingsProvider restClientSettingsProvider;

    /*
     * Name of the CSRF token cookie
     */
    private static final String XSRF_TOKEN_COOKIE_NAME = "XSRF-TOKEN";

    /*
     * Name of the CSRF token cookie
     */
    private static final String XSRF_TOKEN_HEADER_NAME = "X-XSRF-TOKEN";

    /*
     * Shared instance of RestClient
     *
     * Note: RestClient is thread safe
     */
    private RestClient restClient;

    /*
     * Store the current baseUrl to detect whether the shared RestClient should be recreated
     */
    private String baseUrl;

    /*
     * Store the current username to detect whether the shared RestClient should be recreated
     */
    private String username;

    /*
     * Store the current password to detect whether the shared RestClient should be recreated
     */
    private String password;

    /*
     * Requests need to be protected with a CSRF token (GET requests do not require a CSRF token)
     */
    private CsrfTokenDetails csrfTokenDetails;

    @Value("${ciphermail.shell.skip-tls-check:false}")
    private boolean skipTLSChecks = false;

    public RestClientProvider(@Nonnull RestClientSettingsProvider restClientSettingsProvider) {
        this.restClientSettingsProvider = Objects.requireNonNull(restClientSettingsProvider);
    }

    /*
     * Returns an SSLContext configured to trust all peer certificates if the system property
     * "ciphermail.shell.skip-tls-check" is set to true.
     *
     * @return an SSLContext instance, either configured to trust all peer certificates or
     *         the default SSLContext if the system property is not set.
     * @throws CannotContinueRuntimeException if the SSLContext cannot be initialized.
     */
    private SSLContext getCustomSSLContext()
    {
        SSLContext sslContext = null;

        if (skipTLSChecks)
        {
            System.err.println( // NOSONAR
                """

'########:'##::::::::'######:::::::::'######::'##::::'##:'########::'######::'##:::'##::::::::'######::'##:::'##:'####:'########::'########::'########:'########::
... ##..:: ##:::::::'##... ##:::::::'##... ##: ##:::: ##: ##.....::'##... ##: ##::'##::::::::'##... ##: ##::'##::. ##:: ##.... ##: ##.... ##: ##.....:: ##.... ##:
::: ##:::: ##::::::: ##:::..:::::::: ##:::..:: ##:::: ##: ##::::::: ##:::..:: ##:'##::::::::: ##:::..:: ##:'##:::: ##:: ##:::: ##: ##:::: ##: ##::::::: ##:::: ##:
::: ##:::: ##:::::::. ######:::::::: ##::::::: #########: ######::: ##::::::: #####::::::::::. ######:: #####::::: ##:: ########:: ########:: ######::: ##:::: ##:
::: ##:::: ##::::::::..... ##::::::: ##::::::: ##.... ##: ##...:::: ##::::::: ##. ##::::::::::..... ##: ##. ##:::: ##:: ##.....::: ##.....::: ##...:::: ##:::: ##:
::: ##:::: ##:::::::'##::: ##::::::: ##::: ##: ##:::: ##: ##::::::: ##::: ##: ##:. ##::::::::'##::: ##: ##:. ##::: ##:: ##:::::::: ##:::::::: ##::::::: ##:::: ##:
::: ##:::: ########:. ######::::::::. ######:: ##:::: ##: ########:. ######:: ##::. ##:::::::. ######:: ##::. ##:'####: ##:::::::: ##:::::::: ########: ########::
:::..:::::........:::......::::::::::......:::..:::::..::........:::......:::..::::..:::::::::......:::..::::..::....::..:::::::::..:::::::::........::........:::
                """
            );

            try {
                sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, new TrustManager[]{new TrustAllX509TrustManager()}, new SecureRandom());
            }
            catch (NoSuchAlgorithmException | KeyManagementException e) {
                throw new CannotContinueRuntimeException(e);
            }
        }

        return sslContext;
    }

    public synchronized @Nonnull RestClient getRestClient()
    {
        // Create RestClient instance if's not created yet or if the Rest client settings were changed
        if (restClient == null ||
            !StringUtils.equals(baseUrl, restClientSettingsProvider.getBaseUrl()) ||
            !StringUtils.equals(username, restClientSettingsProvider.getUsername()) ||
            !StringUtils.equals(password, restClientSettingsProvider.getPassword()))
        {
            if (StringUtils.isBlank(restClientSettingsProvider.getBaseUrl())) {
                throw new IllegalArgumentException("baseUrl is not set");
            }

            this.baseUrl = restClientSettingsProvider.getBaseUrl();
            this.username = restClientSettingsProvider.getUsername();
            this.password = restClientSettingsProvider.getPassword();

            HttpClient.Builder httpClientBuilder = HttpClient.newBuilder();

            SSLContext customSSLContext = getCustomSSLContext();

            if (customSSLContext != null) {
                httpClientBuilder.sslContext(customSSLContext);
            }

            RestClient.Builder builder = RestClient.builder()
                    .requestFactory(new JdkClientHttpRequestFactory(httpClientBuilder.build()))
                    .baseUrl(baseUrl);

            if (username != null) {
                builder.defaultHeader(HttpHeaders.AUTHORIZATION, encodeBasic(username, password));
            }

            if (restClientSettingsProvider.getTOTP() != null) {
                builder.defaultHeader("TOTP", restClientSettingsProvider.getTOTP());
            }

            restClient = builder.build();

            csrfTokenDetails = new CsrfTokenDetails(XSRF_TOKEN_HEADER_NAME,
                    UUID.randomUUID().toString());
        }

        return restClient;
    }

    private static String encodeBasic(String username, String password) {
        return "Basic " + Base64
                .getEncoder()
                .encodeToString((username + ":" + password).getBytes());
    }

    /**
     * Forces a new RestClient to be created on the next call to {@link RestClientProvider#getRestClient}
     */
    public synchronized void reset() {
        restClient = null;
    }

    public Consumer<HttpHeaders> getCsrfTokenHeadersConsumer()
    {
        return httpHeaders ->
        {
            httpHeaders.add(csrfTokenDetails.headerName(), csrfTokenDetails.token());
            httpHeaders.add("Cookie", String.format("%s=%s", XSRF_TOKEN_COOKIE_NAME,
                    csrfTokenDetails.token()));
        };
    }
}
