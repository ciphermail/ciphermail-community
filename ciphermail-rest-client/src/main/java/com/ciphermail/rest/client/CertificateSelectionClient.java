/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.app.NamedCertificateCategory;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class CertificateSelectionClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public List<CertificateDTO.X509CertificateDetails> getAutoSelectedEncryptionCertificatesForUser(
            @Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_AUTOSELECTED_ENCRYPTION_CERTIFICATES_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getExplicitCertificatesForUser(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> setExplicitCertificatesForUser(
            @Nonnull String email,
            @Nonnull List<String> thumbprints)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(thumbprints)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getInheritedCertificatesForUser(
            @Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getEncryptionCertificatesForUser(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_ENCRYPTION_CERTIFICATES_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getNamedCertificatesForUser(
            @Nonnull String email,
            @Nonnull NamedCertificateCategory category)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("category", category);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("category", "{category}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> setNamedCertificatesForUser(
            @Nonnull String email,
            @Nonnull NamedCertificateCategory category,
            @Nonnull List<String> thumbprints)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("category", category);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("category", "{category}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(thumbprints)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getInheritedNamedCertificatesForUser(
            @Nonnull String email,
            @Nonnull NamedCertificateCategory category)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("category", category);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("category", "{category}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getExplicitCertificatesForDomain(
            @Nonnull String domain)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> setExplicitCertificatesForDomain(
            @Nonnull String domain,
            @Nonnull List<String> thumbprints)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(thumbprints)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getInheritedCertificatesForDomain(
            @Nonnull String domain)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getNamedCertificatesForDomain(
            @Nonnull String domain,
            @Nonnull NamedCertificateCategory category)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);
        params.put("category", category);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .queryParam("category", "{category}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> setNamedCertificatesForDomain(
            @Nonnull String domain,
            @Nonnull NamedCertificateCategory category,
            @Nonnull List<String> thumbprints)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);
        params.put("category", category);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .queryParam("category", "{category}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(thumbprints)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getInheritedNamedCertificatesForDomain(
            @Nonnull String domain,
            @Nonnull NamedCertificateCategory category)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);
        params.put("category", category);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .queryParam("category", "{category}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getNamedCertificatesForGlobal(
            @Nonnull NamedCertificateCategory category)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("category", category);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_GLOBAL_PATH)
                        .queryParam("category", "{category}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> setNamedCertificatesForGlobal(
            @Nonnull NamedCertificateCategory category,
            @Nonnull List<String> thumbprints)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("category", category);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_GLOBAL_PATH)
                        .queryParam("category", "{category}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(thumbprints)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public CertificateDTO.X509CertificateDetails getSigningCertificateForUser(
            @Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(CertificateDTO.X509CertificateDetails.class);
    }

    public CertificateDTO.X509CertificateDetails setSigningCertificateForUser(@Nonnull String email, @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(CertificateDTO.X509CertificateDetails.class);
    }

    public CertificateDTO.X509CertificateDetails getSigningCertificateForDomain(@Nonnull String domain)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(CertificateDTO.X509CertificateDetails.class);
    }

    public CertificateDTO.X509CertificateDetails setSigningCertificateForDomain(@Nonnull String domain, @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(CertificateDTO.X509CertificateDetails.class);
    }

    public CertificateDTO.X509CertificateDetails getSigningCertificateForGlobal()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(CertificateDTO.X509CertificateDetails.class);
    }

    public CertificateDTO.X509CertificateDetails setSigningCertificateForGlobal(@Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH)
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(CertificateDTO.X509CertificateDetails.class);
    }
}
