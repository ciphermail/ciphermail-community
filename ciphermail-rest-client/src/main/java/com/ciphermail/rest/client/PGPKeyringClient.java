/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.common.security.openpgp.PGPKeyType;
import com.ciphermail.core.common.security.openpgp.PGPSearchField;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
@SuppressWarnings({"java:S6813"})
public class PGPKeyringClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public PGPDTO.PGPKeyDetails getKey(@Nonnull UUID id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEYRING_GET_KEY_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(PGPDTO.PGPKeyDetails.class);
    }

    public PGPDTO.PGPKeyDetails getKeyBySha256Fingerprint(@Nonnull String sha256Fingerprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("sha256Fingerprint", sha256Fingerprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEYRING_GET_KEY_BY_SHA256_FINGERPRINT_PATH)
                        .queryParam("sha256Fingerprint", "{sha256Fingerprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(PGPDTO.PGPKeyDetails.class);
    }

    public List<PGPDTO.PGPKeyDetails> getSubKeys(@Nonnull UUID masterId)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("masterId", masterId);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEYRING_GET_SUBKEYS_PATH)
                        .queryParam("id", "{masterId}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<PGPDTO.PGPKeyDetails> getKeys(
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias,
            @Nonnull PGPKeyType keyType,
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);
        params.put("keyType", keyType);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEYRING_GET_KEYS_PATH)
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .queryParam("keyType", "{keyType}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getKeysCount(
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias,
            @Nonnull PGPKeyType keyType)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);
        params.put("keyType", keyType);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEYRING_GET_KEYS_COUNT_PATH)
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .queryParam("keyType", "{keyType}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public List<PGPDTO.PGPKeyDetails> searchKeys(
            @Nonnull PGPSearchField searchField,
            @Nonnull String searchValue,
            @Nonnull Match match,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias,
            @Nonnull PGPKeyType keyType,
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("searchField", searchField);
        params.put("searchValue", searchValue);
        params.put("match", match);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);
        params.put("keyType", keyType);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEYRING_SEARCH_KEYS_PATH)
                        .queryParam("searchField", "{searchField}")
                        .queryParam("searchValue", "{searchValue}")
                        .queryParam("match", "{match}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .queryParam("keyType", "{keyType}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long searchKeysCount(
            @Nonnull PGPSearchField searchField,
            @Nonnull String searchValue,
            @Nonnull Match match,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias,
            @Nonnull PGPKeyType keyType)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("searchField", searchField);
        params.put("searchValue", searchValue);
        params.put("match", match);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);
        params.put("keyType", keyType);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEYRING_SEARCH_KEYS_COUNT_PATH)
                        .queryParam("searchField", "{searchField}")
                        .queryParam("searchValue", "{searchValue}")
                        .queryParam("match", "{match}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .queryParam("keyType", "{keyType}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public void deleteKey(@Nonnull UUID id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEYRING_DELETE_KEY_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }
}
