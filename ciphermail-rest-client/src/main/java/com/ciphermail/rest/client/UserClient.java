/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.core.UserDTO;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class UserClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public UserDTO.UserValue addUser(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.USER_ADD_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(UserDTO.UserValue.class);
    }

    public UserDTO.UserValue getUser(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.USER_GET_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(UserDTO.UserValue.class);
    }

    public Boolean isUser(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.USER_IS_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Boolean.class);
    }

    public UserDTO.UserValue deleteUser(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.USER_DELETE_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(UserDTO.UserValue.class);
    }

    public List<UserDTO.UserValue> getUsers(
            @Nonnull SortDirection sortDirection,
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("sortDirection", sortDirection);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.USER_GET_USERS_PATH)
                        .queryParam("sortDirection", "{sortDirection}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getUsersCount()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.USER_GET_USERS_COUNT_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public List<UserDTO.UserValue> searchUsers(
            @Nonnull String search,
            @Nonnull SortDirection sortDirection,
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("search", search);
        params.put("sortDirection", sortDirection);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.USER_SEARCH_USERS_PATH)
                        .queryParam("search", "{search}")
                        .queryParam("sortDirection", "{sortDirection}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String searchUsersCount(@Nonnull String search)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("search", search);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.USER_SEARCH_USERS_COUNT_PATH)
                        .queryParam("search", "{search}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(String.class);
    }
}
