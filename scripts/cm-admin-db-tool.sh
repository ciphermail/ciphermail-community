#!/bin/bash

set -e
set -o pipefail


# The AdminTool class is a command-line tool that allows configuring admin credentials by directly connecting to the
# database. This should be used to add the initial admin or to reset an admin password. For all other admin related
# tasks, use the CLI tool.
# <p>
# To use the tool, provide the required command line options:
# -   --admin-name: The name of the admin.
# -   --admin-password: The password of the admin.
# <p>
# Optional command line options:
# -   --admin-ip-addresses: The authorized IP range for the admin.
# <p>
# If the required options are provided, the tool will create a new admin with the given name and password if the admin
# does not yet exist and optionally set the authorized IP range for the admin. If the admin already exists, only
# the password and optionally the authorized IP range will be changed.
# <p>
# The tool uses Hibernate for database operations, and the database connection details must be provided using
# the following options:
# -   --db-driver-class: The database driver class.
# -   --db-url: The database JDBC URL.
# -   --db-username: The database JDBC authentication username.
# -   --db-password: The database JDBC authentication password.
# -   --db-dialect: The database dialect (optional, default: auto detection).
# -   --db-physical-naming-strategy-class-name: The naming strategy for the database (optional).

java -cp '/opt/ciphermail/backend/lib/*' com.ciphermail.core.app.tools.AdminDBTool "$@"

exit 0