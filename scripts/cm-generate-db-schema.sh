#!/bin/bash

set -e
set -o pipefail

# script which generates the database schema

SCRIPT_NAME=$(basename "$0")

# the error exit codes
GENERAL_EXIT_CODE=100

usage()
{
    echo "Usage: $SCRIPT_NAME [option...]" >&2
    echo "" >&2
    echo "    [-h]                                  show usage" >&2
    echo "    [--help]                              show usage" >&2
    echo "    --output-file <file>                  the file to write the generated schema to" >&2
    echo "    --database-user <user>                the database user (default: ciphermail)" >&2
    echo "    --database-password <password>        the database password (default: ciphermail)" >&2
    echo "    --database-connection-url <url>       the connection URL" >&2
    echo "    --database-driver-class <class>       the Java class for the database" >&2
    echo "    [--drop]                              if set, the drop tables schema will be generated" >&2

    exit 1
}

exit_with_error()
{
    echo "$1" >&2

    local exit_code="$2"

    if [ -z "$exit_code" ]; then
        exit_code=$GENERAL_EXIT_CODE;
    fi

    exit "$exit_code"
}

if [ "$#" -eq 0 ]; then usage; fi

GETOPT_TEMP=$(getopt -o h --long "help,output-file:,database-user:,database-password:,database-connection-url:, database-driver-class:, drop" \
    -n "$SCRIPT_NAME" -- "$@")

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

# default settings
database_user="ciphermail"
database_password="ciphermail"

# Note the quotes around $GETOPT_TEMP: they are essential!
eval set -- "$GETOPT_TEMP"

while true ; do
    case "$1" in
        -h|--help) usage ;;
        --output-file) output_file=$2 ; shift 2 ;;
        --database-user) database_user=$2 ; shift 2 ;;
        --database-password) database_password=$2 ; shift 2 ;;
        --database-connection-url) database_connection_url=$2 ; shift 2 ;;
        --database-driver-class) database_driver_class=$2 ; shift 2 ;;
        --drop) generate_drop=drop ; shift 1 ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

if [[ -z $output_file ]]; then
  exit_with_error "output-file must be set"
fi

if [[ -z $database_user ]]; then
  exit_with_error "database-user must be set"
fi

if [[ -z $database_password ]]; then
  exit_with_error "database-password must be set"
fi

if [[ -z $database_connection_url ]]; then
  exit_with_error "database-connection-url must be set"
fi

if [[ -z $database_driver_class ]]; then
  exit_with_error "database-driver-class must be set"
fi

java -cp '/opt/ciphermail/backend/lib/*' \
    com.ciphermail.core.app.tools.GenerateSchema \
    --url "$database_connection_url" \
    --driver-class "$database_driver_class" \
    --username "$database_user" \
    --password "$database_password" \
    --output-file "$output_file" \
    ${generate_drop:+ --drop}

exit 0