
    create table cm_admin (
        cm_id uuid not null,
        cm_name varchar(255) not null unique,
        primary key (cm_id)
    );

    create table cm_admin_name_values (
        cm_admin_id uuid not null,
        cm_value varchar(32768),
        cm_name varchar(255) not null,
        primary key (cm_admin_id, cm_name)
    );

    create table cm_admin_role (
        cm_admin_id uuid not null,
        cm_roles_id uuid not null,
        primary key (cm_admin_id, cm_roles_id)
    );

    create table cm_certificate (
        cm_id uuid not null,
        cm_certificate bytea,
        cm_issuer varchar(32768),
        cm_issuer_friendly varchar(32768),
        cm_not_after timestamp(6),
        cm_not_before timestamp(6),
        cm_serial varchar(1024),
        cm_subject varchar(32768),
        cm_subject_friendly varchar(32768),
        cm_subject_key_identifier varchar(1024),
        cm_thumbprint varchar(255),
        cm_cert_path bytea,
        cm_cert_path_type varchar(255),
        cm_creation_date timestamp(6),
        cm_date_path_updated timestamp(6),
        cm_key_alias varchar(1024),
        cm_store_name varchar(255),
        primary key (cm_id),
        unique (cm_store_name, cm_thumbprint)
    );

    create table cm_certificate_request (
        cm_id uuid not null,
        cm_certificate_handler_name varchar(255),
        cm_created timestamp(6) not null,
        cm_crl_dist_point varchar(65536),
        cm_data bytea,
        cm_email varchar(320),
        cm_info varchar(1024),
        cm_iteration integer,
        cm_key_length integer,
        cm_last_message varchar(32768),
        cm_last_updated timestamp(6),
        cm_next_update timestamp(6),
        cm_signature_algorithm varchar(1024),
        cm_subject bytea,
        cm_validity integer,
        primary key (cm_id)
    );

    create table cm_certificates_email (
        cm_certificates_id uuid not null,
        cm_email varchar(320)
    );

    create table cm_crl (
        cm_id uuid not null,
        cm_creation_date timestamp(6),
        cm_crl bytea,
        cm_crl_number varchar(1024),
        cm_issuer varchar(32768),
        cm_next_update timestamp(6),
        cm_this_update timestamp(6),
        cm_thumbprint varchar(255),
        cm_store_name varchar(255),
        primary key (cm_id),
        unique (cm_store_name, cm_thumbprint)
    );

    create table cm_ctl (
        cm_id uuid not null,
        cm_name varchar(255),
        cm_thumbprint varchar(255) not null,
        primary key (cm_id),
        unique (cm_name, cm_thumbprint)
    );

    create table cm_ctl_name_values (
        cm_ctl_id uuid not null,
        cm_value varchar(32768),
        cm_name varchar(255) not null,
        primary key (cm_ctl_id, cm_name)
    );

    create table cm_key_ring_email (
        cm_key_ring_id uuid not null,
        cm_email varchar(320)
    );

    create table cm_key_store (
        cm_id uuid not null,
        cm_alias varchar(1024) not null,
        cm_certificate bytea,
        cm_certificate_type varchar(255),
        cm_thumbprint varchar(255),
        cm_creation_date timestamp(6),
        cm_encoded_key bytea,
        cm_store_name varchar(255) not null,
        primary key (cm_id),
        unique (cm_store_name, cm_alias)
    );

    create table cm_key_store_certificate_chain (
        cm_key_store_id uuid not null,
        cm_certificate bytea,
        cm_certificate_type varchar(255),
        cm_thumbprint varchar(255)
    );

    create table cm_mail_repository_recipients (
        cm_id uuid not null,
        cm_recipients varchar(1024)
    );

    create table cm_mail_repository (
        cm_id uuid not null,
        cm_data bytea,
        cm_created timestamp(6),
        cm_from_header varchar(1024),
        cm_last_updated timestamp(6),
        cm_messageid varchar(1024),
        cm_originator varchar(1024),
        cm_remote_address varchar(1024),
        cm_repository varchar(255),
        cm_sender varchar(1024),
        cm_subject varchar(1024),
        cm_mime_id uuid unique,
        primary key (cm_id)
    );

    create table cm_mail_repository_mime (
        cm_id uuid not null,
        cm_blob oid,
        primary key (cm_id)
    );

    create table cm_named_blob (
        cm_id uuid not null,
        cm_category varchar(1024) not null,
        cm_name varchar(1024) not null,
        cm_blob_entity_id uuid unique,
        primary key (cm_id),
        unique (cm_category, cm_name)
    );

    create table cm_named_blob_named_blob (
        cm_named_blob_id uuid not null,
        cm_named_blobs_id uuid not null,
        primary key (cm_named_blob_id, cm_named_blobs_id)
    );

    create table cm_named_blob_data (
        cm_id uuid not null,
        cm_blob bytea,
        primary key (cm_id)
    );

    create table cm_pgp_key_ring (
        cm_id uuid not null,
        cm_creation_date timestamp(6),
        cm_public_key bytea not null,
        cm_expiration_date timestamp(6),
        cm_fingerprint varchar(255) not null,
        cm_insertion_date timestamp(6),
        cm_key_id bigint not null,
        cm_key_id_hex varchar(2048) not null,
        cm_key_ring_name varchar(255) not null,
        cm_master boolean not null,
        cm_parent_key_id bigint,
        cm_private_key_alias varchar(255),
        cm_sha256fingerprint varchar(255) not null,
        cm_parent_id uuid,
        primary key (cm_id),
        unique (cm_key_ring_name, cm_sha256fingerprint)
    );

    create table cm_pgp_key_ring_user_id (
        cm_key_ring_id uuid not null,
        cm_user_id varchar(320)
    );

    create table cm_pgp_trust_list (
        cm_id uuid not null,
        cm_fingerprint varchar(255) not null,
        cm_name varchar(255),
        primary key (cm_id),
        unique (cm_name, cm_fingerprint)
    );

    create table cm_pgp_trust_list_name_values (
        cm_pgp_trust_list_id uuid not null,
        cm_value varchar(32768),
        cm_name varchar(255) not null,
        primary key (cm_pgp_trust_list_id, cm_name)
    );

    create table cm_property (
        cm_id uuid not null,
        cm_category varchar(320) unique,
        primary key (cm_id)
    );

    create table cm_property_name_values (
        cm_property_id uuid not null,
        cm_value text,
        cm_name varchar(255) not null,
        primary key (cm_property_id, cm_name)
    );

    create table cm_role (
        cm_id uuid not null,
        cm_name varchar(255) not null unique,
        primary key (cm_id)
    );

    create table cm_role_permissions (
        cm_role_id uuid not null,
        cm_permission varchar(255)
    );

    create table cm_role_role (
        cm_role_id uuid not null,
        cm_roles_id uuid not null,
        primary key (cm_role_id, cm_roles_id)
    );

    create table cm_sms (
        cm_id uuid not null,
        cm_data bytea,
        cm_date_created timestamp(6),
        cm_date_last_try timestamp(6),
        cm_last_error varchar(1024),
        cm_message varchar(2048) not null,
        cm_phone_number varchar(255) not null,
        primary key (cm_id)
    );

    create table cm_user (
        cm_id uuid not null,
        cm_email varchar(320) not null unique,
        cm_user_preferences_entity_id uuid unique,
        primary key (cm_id)
    );

    create table cm_user_preferences (
        cm_id uuid not null,
        cm_category varchar(320) not null,
        cm_name varchar(320) not null,
        cm_key_and_certificate_entry_id uuid,
        cm_property_entity_id uuid unique,
        primary key (cm_id),
        unique (cm_category, cm_name)
    );

    create table cm_user_preferences_certificate (
        cm_user_preferences_id uuid not null,
        cm_certificates_id uuid not null,
        primary key (cm_user_preferences_id, cm_certificates_id)
    );

    create table cm_user_preferences_named_blob (
        cm_user_preferences_id uuid not null,
        cm_named_blobs_id uuid not null,
        primary key (cm_user_preferences_id, cm_named_blobs_id)
    );

    create table cm_user_preferences_inherited_preferences (
        cm_user_preferences_id uuid not null,
        cm_index bigint not null,
        cm_inherited_preferences_id uuid not null,
        primary key (cm_user_preferences_id, cm_index, cm_inherited_preferences_id)
    );

    create table cm_user_preferences_named_certificates (
        cm_user_preferences_id uuid not null,
        cm_certificate_entry_id uuid,
        cm_name varchar(255)
    );

    create index certificate_store_name_index 
       on cm_certificate (cm_store_name);

    create index certificate_issuer_index 
       on cm_certificate (cm_issuer);

    create index certificate_serial_index 
       on cm_certificate (cm_serial);

    create index certificate_subject_key_id_index 
       on cm_certificate (cm_subject_key_identifier);

    create index certificate_subject_index 
       on cm_certificate (cm_subject);

    create index certificate_key_alias_index 
       on cm_certificate (cm_key_alias);

    create index certificate_thumbprint_index 
       on cm_certificate (cm_thumbprint);

    create index certificate_creationdate_index 
       on cm_certificate (cm_creation_date);

    create index certificaterequest_next_update_index 
       on cm_certificate_request (cm_next_update);

    create index certificaterequest_created_index 
       on cm_certificate_request (cm_created);

    create index certificaterequest_email_index 
       on cm_certificate_request (cm_email);

    create index crl_store_name_index 
       on cm_crl (cm_store_name);

    create index crl_issuer_index 
       on cm_crl (cm_issuer);

    create index crl_crlnumber_index 
       on cm_crl (cm_crl_number);

    create index crl_thumbprint_index 
       on cm_crl (cm_thumbprint);

    create index keystore_storename_index 
       on cm_key_store (cm_store_name);

    create index keystore_alias_index 
       on cm_key_store (cm_alias);

    create index sms_datelasttry_index 
       on cm_sms (cm_date_last_try);

    create index userpreferences_name_index 
       on cm_user_preferences (cm_name);

    create index userpreferences_category_index 
       on cm_user_preferences (cm_category);

    alter table if exists cm_admin_name_values 
       add constraint FKqupdrkq06u2p1el3uo51bs5iu 
       foreign key (cm_admin_id) 
       references cm_admin;

    alter table if exists cm_admin_role 
       add constraint FKi2tpp40gjgnicw8ml4jkt7yyf 
       foreign key (cm_roles_id) 
       references cm_role;

    alter table if exists cm_admin_role 
       add constraint FKmgjgx603vlcswb7mowl4oaqe3 
       foreign key (cm_admin_id) 
       references cm_admin;

    alter table if exists cm_certificates_email 
       add constraint FKiq36tj114ay7m11nxmey0lv77 
       foreign key (cm_certificates_id) 
       references cm_certificate;

    alter table if exists cm_ctl_name_values 
       add constraint FKcw1eqygm4dtqa13mocln6f6e 
       foreign key (cm_ctl_id) 
       references cm_ctl;

    alter table if exists cm_key_ring_email 
       add constraint FK4obptjd4t75en96f7gm36er8b 
       foreign key (cm_key_ring_id) 
       references cm_pgp_key_ring;

    alter table if exists cm_key_store_certificate_chain 
       add constraint FKst643n6sw3gmp1n21a17ptr91 
       foreign key (cm_key_store_id) 
       references cm_key_store;

    alter table if exists cm_mail_repository_recipients 
       add constraint FK70dw5te53l46cclsvtw0okrby 
       foreign key (cm_id) 
       references cm_mail_repository;

    alter table if exists cm_mail_repository 
       add constraint FK41vd3dqc3o0u2dk0vjrwxpacj 
       foreign key (cm_mime_id) 
       references cm_mail_repository_mime;

    alter table if exists cm_named_blob 
       add constraint FKiih6k8s2xhjubb37oy0psj1ae 
       foreign key (cm_blob_entity_id) 
       references cm_named_blob_data;

    alter table if exists cm_named_blob_named_blob 
       add constraint FK2vqmcf3pf7n3gg6jmjg7lece2 
       foreign key (cm_named_blobs_id) 
       references cm_named_blob;

    alter table if exists cm_named_blob_named_blob 
       add constraint FKag56bkxus22t0449a5vi6hbyy 
       foreign key (cm_named_blob_id) 
       references cm_named_blob;

    alter table if exists cm_pgp_key_ring_user_id 
       add constraint FKrjeo922ndywcb4v97lpie0r6a 
       foreign key (cm_key_ring_id) 
       references cm_pgp_key_ring;

    alter table if exists cm_pgp_trust_list_name_values 
       add constraint FK1tp965xg2w2r1waf3tk1cvyub 
       foreign key (cm_pgp_trust_list_id) 
       references cm_pgp_trust_list;

    alter table if exists cm_property_name_values 
       add constraint FKb5567885b7cqv9cm3ovno7k7i 
       foreign key (cm_property_id) 
       references cm_property;

    alter table if exists cm_role_permissions 
       add constraint FKncbjot7akur4ix757xx47jkac 
       foreign key (cm_role_id) 
       references cm_role;

    alter table if exists cm_role_role 
       add constraint FKaxd0kq51rc08a8pj2i3rvw3ec 
       foreign key (cm_roles_id) 
       references cm_role;

    alter table if exists cm_role_role 
       add constraint FKnd2ddo1htvoc5wn8fot1rd1tg 
       foreign key (cm_role_id) 
       references cm_role;

    alter table if exists cm_user 
       add constraint FKajlb7efnwn6dfi45sn0t78t2o 
       foreign key (cm_user_preferences_entity_id) 
       references cm_user_preferences;

    alter table if exists cm_user_preferences 
       add constraint FKnbthaqbxd6bb5k9n3pmb63nqn 
       foreign key (cm_key_and_certificate_entry_id) 
       references cm_certificate;

    alter table if exists cm_user_preferences 
       add constraint FKcivt2mu9ykffi8f6sbcitdkot 
       foreign key (cm_property_entity_id) 
       references cm_property;

    alter table if exists cm_user_preferences_certificate 
       add constraint FK1xk4t13m6am2yjo79ihcslyca 
       foreign key (cm_certificates_id) 
       references cm_certificate;

    alter table if exists cm_user_preferences_certificate 
       add constraint FK4ryqvyj7m2a1bu4f0nfsrypty 
       foreign key (cm_user_preferences_id) 
       references cm_user_preferences;

    alter table if exists cm_user_preferences_named_blob 
       add constraint FKlomdtyf7o7nebsmdnn3j1mch9 
       foreign key (cm_named_blobs_id) 
       references cm_named_blob;

    alter table if exists cm_user_preferences_named_blob 
       add constraint FKol1mhj7lbk1k2fmy5d1romd0w 
       foreign key (cm_user_preferences_id) 
       references cm_user_preferences;

    alter table if exists cm_user_preferences_inherited_preferences 
       add constraint FKor6xs4igfi929wiu299xr6cyv 
       foreign key (cm_inherited_preferences_id) 
       references cm_user_preferences;

    alter table if exists cm_user_preferences_inherited_preferences 
       add constraint FKplj9361271xfiwtbv1uyi7m2w 
       foreign key (cm_user_preferences_id) 
       references cm_user_preferences;

    alter table if exists cm_user_preferences_named_certificates 
       add constraint FKbx6a9avsdbpd968dnwlnidnlg 
       foreign key (cm_certificate_entry_id) 
       references cm_certificate;

    alter table if exists cm_user_preferences_named_certificates 
       add constraint FK1dxni6jil032poxuf8sit30px 
       foreign key (cm_user_preferences_id) 
       references cm_user_preferences;
