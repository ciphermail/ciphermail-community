/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.core.AcmeDTO;
import com.ciphermail.rest.core.AuthDTO;
import com.ciphermail.rest.core.CADTO;
import com.ciphermail.rest.core.CRLDTO;
import com.ciphermail.rest.core.CTLDTO;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.DKIMDTO;
import com.ciphermail.rest.core.DLPDTO;
import com.ciphermail.rest.core.DomainDTO;
import com.ciphermail.rest.core.KeyStoreDTO;
import com.ciphermail.rest.core.MTADTO;
import com.ciphermail.rest.core.MailDTO;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.PGPKeyServerDTO;
import com.ciphermail.rest.core.PropertyDTO;
import com.ciphermail.rest.core.QuarantineDTO;
import com.ciphermail.rest.core.SMSDTO;
import com.ciphermail.rest.core.TLSDTO;
import com.ciphermail.rest.core.UserDTO;
import org.jline.reader.LineReader;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.shell.command.annotation.CommandScan;
import org.springframework.shell.jline.PromptProvider;

@CommandScan
@SpringBootApplication(
    scanBasePackages = {"com.ciphermail.shell", "com.ciphermail.rest.client"}
)
// for supporting native builds (using GraalVM), some classes which are serialized with Jackson need to be registered
@RegisterReflectionForBinding({
        AcmeDTO.Account.class,
        AuthDTO.CsrfTokenDetails.class,
        AuthDTO.RoleDetails.class,
        AuthDTO.AdminDetails.class,
        AuthDTO.OAuth2UserInfoEndpoint.class,
        AuthDTO.OAuth2ProviderDetails.class,
        AuthDTO.OAuth2ClientRegistrationDetails.class,
        CADTO.X500PrincipalRequest.class,
        CADTO.NewCASubjects.class,
        CADTO.NewCAResponse.class,
        CADTO.PendingRequest.class,
        CADTO.NewRequest.class,
        CADTO.CertificateRequestHandlerDetails.class,
        CertificateDTO.X509CertificateDetails.class,
        CertificateDTO.CertificateReference.class,
        CertificateDTO.CertificateValidationResult.class,
        CertificateDTO.IssuerWithStore.class,
        CertificateDTO.ExportPrivateKeysRequestBody.class,
        CRLDTO.X509CRLDetails.class,
        CRLDTO.CRLValidationResult.class,
        TLSDTO.CSRStoreEntry.class,
        TLSDTO.Request.class,
        CTLDTO.CTLResult.class,
        DKIMDTO.SignatureTemplate.class,
        DLPDTO.DLPValidator.class,
        DLPDTO.DLPMatchFilter.class,
        DLPDTO.DLPPolicyPattern.class,
        DomainDTO.DomainValue.class,
        KeyStoreDTO.CertificateDetails.class,
        KeyStoreDTO.ExportToPKCS12RequestBody.class,
        KeyStoreDTO.RenameEntryRequestBody.class,
        MailDTO.MailDetails.class,
        MailDTO.MailDetailsWithMIME.class,
        MTADTO.Recipient.class,
        MTADTO.QueueItem.class,
        MTADTO.StandardMainSettings.class,
        MTADTO.PostfixMap.class,
        PGPDTO.PGPKeyDetails.class,
        PGPDTO.GenerateSecretKeyRingDetails.class,
        PGPDTO.PGPTrustListResult.class,
        PGPDTO.ExportSecretKeysRequestBody.class,
        PGPKeyServerDTO.KeyServerSubmitDetails.class,
        PGPKeyServerDTO.PGPKeyServerKeyDetails.class,
        PGPKeyServerDTO.PGPKeyServerSearchResult.class,
        PGPKeyServerDTO.PGPKeyServerDownloadResult.class,
        PGPKeyServerDTO.PGPKeyServerClientSettings.class,
        PropertyDTO.PropertyRequest.class,
        PropertyDTO.PropertyValue.class,
        PropertyDTO.TypeDetails.class,
        PropertyDTO.UserPropertyDetails.class,
        PropertyDTO.UserPropertyDescriptorDetails.class,
        PropertyDTO.UserPropertiesDetails.class,
        QuarantineDTO.PolicyViolation.class,
        QuarantineDTO.QuarantinedMail.class,
        SMSDTO.SMSMessage.class,
        UserDTO.UserValue.class,
})
public class CipherMailShellApplication
{
    public static void main(String[] args) {
        SpringApplication.run(CipherMailShellApplication.class, args);
    }

    @Value("${ciphermail.shell.history-ignore}")
    private String historyIgnore;

    @Bean
    public PromptProvider myPromptProvider(LineReader lineReader)
    {
        // disable history for certain sensitive keywords like password
        // Note: LineReader is lazy initialized and can therefore not be set in a PostConstruct method
        lineReader.setVariable(LineReader.HISTORY_IGNORE, historyIgnore);

        return () -> new AttributedString("CipherMail:>", AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW));
    }
}
