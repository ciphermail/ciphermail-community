/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.util.ProcessException;
import com.ciphermail.core.common.util.ProcessRunner;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import jakarta.annotation.Nonnull;
import jakarta.annotation.PostConstruct;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

@Component
public class JSONPrettyPrinter
{
    private static final Logger logger = LoggerFactory.getLogger(JSONPrettyPrinter.class);

    @Value("${ciphermail.shell.jq-command:jq}")
    private String jqCommand;

    /*
     * Use jq for pretty printing and syntax highlighting of json
     */
    @Value("${ciphermail.shell.enable-jq:true}")
    private boolean enableJq;

    @Value("${ciphermail.shell.color-output:true}")
    private boolean colorOutput;

    @Value("${ciphermail.shell.disable-dates-as-timestamps:false}")
    private boolean disableDatesAsTimestamps;

    /*
     * Jackson mapper
     */
    private final ObjectMapper mapper = new ObjectMapper();

    @PostConstruct
    private void init()
    {
        // if stdout is redirected, do not color code the output
        if (System.console() == null) {
            colorOutput = false;
        }

        if (disableDatesAsTimestamps) {
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        }
    }

    public String prettyPrintJSON(Object object)
    throws JsonProcessingException
    {
        if (object == null) {
            return null;
        }

        return prettyPrintJSON(mapper.writeValueAsString(object));
    }

    public String prettyPrintJSON(String json)
    {
        if (json == null) {
            return null;
        }

        String prettyJson = null;

        try {
            if (enableJq) {
                prettyJson = prettyPrintWithJq(json);
            }
        }
        catch (IOException e) {
            logger.error("Error running {}. Message: {}}", jqCommand, e.getMessage());
        }

        if (prettyJson == null)
        {
            try {
                prettyJson = JacksonUtil.prettyPrintJSON(json);
            }
            catch (IOException e) {
                logger.error("Error pretty printing JSON", e);
            }
        }

        // remove start and end newlines
        return StringUtils.strip(prettyJson != null ? prettyJson : json, "\n\r");
    }

    private String prettyPrintWithJq(@Nonnull String json)
    throws IOException
    {
        List<String> cmd = new LinkedList<>();

        cmd.add(jqCommand);

        if (colorOutput) {
            cmd.add("--color-output");
        }

        ProcessRunner processRunner = new ProcessRunner();

        StringWriter outputWriter = new StringWriter();

        OutputStream outputStream = WriterOutputStream.builder().setCharset(StandardCharsets.UTF_8)
                .setWriter(outputWriter).get();

        StringWriter errorWriter = new StringWriter();

        OutputStream errorStream = WriterOutputStream.builder().setCharset(StandardCharsets.UTF_8)
                .setWriter(errorWriter).get();

        try {
            processRunner.setInput(IOUtils.toInputStream(json, StandardCharsets.UTF_8));
            processRunner.setOutput(outputStream);
            processRunner.setError(errorStream);

            processRunner.run(cmd);
        }
        catch(ProcessException e)
        {
            // need to close before getting the content
            IOUtils.closeQuietly(errorStream);

            throw new IOException(errorWriter.toString(), e);
        }
        finally {
            IOUtils.closeQuietly(outputStream);
            IOUtils.closeQuietly(errorStream);
        }

        return outputWriter.toString();
    }
}
