/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.app.admin.AuthenticationType;
import com.ciphermail.rest.client.AuthClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Component
@Command(command = "auth", group = "Auth")
@SuppressWarnings({"java:S6813"})
public class AuthCommands
{
    @Autowired
    private AuthClient authClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Command(command = {"get", "oidc-clients"}, description = "List all registered OpenID client registrations")
    public String getOIDCClients()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(authClient.getOIDCClients());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "get"}, description = "Get admin details")
    public String getAdmin(@Option(required = true, description = "name of admin") String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(authClient.getAdmin(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "get", "all"}, description = "List all admins")
    public String getAdmins()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(authClient.getAdmins());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "add"}, description = "Add new admin")
    public void addAdmin(
            @Option(required = true, description = "name of new admin") String name,
            @Option(required = true, label = ShellConst.AUTHENTICATION_TYPE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") AuthenticationType authenticationType)
    {
        authClient.addAdmin(name, authenticationType);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "delete"}, description = "Delete admin")
    public void deleteAdmin(@Option(required = true, description = "name of the admin to delete") String name)
    {
        authClient.deleteAdmin(name);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "password"}, description = "Set admin password")
    public void setPassword(
            @Option(required = true, description = "name of admin") String name,
            @Option(required = true, description = "password for the admin") String password)
    {
        authClient.setPassword(name, password);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "ip-addresses", "set"}, description = "Set authorized IP addresses for an admin")
    public void setIpAddresses(
            @Option(required = true, description = "name of admin") String name,
            @Option(required = true, description = "IP addresses for the admin") List<String> ipAddresses)
    {
        authClient.setIpAddresses(name, ipAddresses);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "ip-addresses", "clear"}, description = "Clear authorized IP addresses for an admin")
    public void clearIpAddresses(
            @Option(required = true, description = "name of admin") String name)
    {
        authClient.setIpAddresses(name, Collections.emptyList());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "roles", "set"}, description = "Set roles for an admin")
    public void setRoles(
            @Option(required = true, description = "name of admin") String name,
            @Option(required = true, description = "roles for the admin") List<String> roles)
    {
        authClient.setRoles(name, roles);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"role", "get"}, description = "Get role details")
    public String getRole(@Option(required = true, description = "name of role") String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(authClient.getRole(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"role", "get", "all"}, description = "List available roles")
    public String getAvailableRoles()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(authClient.getRoles());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"role", "create"}, description = "Create new role")
    public void createRole(@Option(required = true, description = "name of new role") String name)
    {
        authClient.createRole(name);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"role", "delete"}, description = "Delete role")
    public void deleteRole(@Option(required = true, description = "name of role to delete") String name)
    {
        authClient.deleteRole(name);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"role", "permissions", "add"}, description = "Add permissions to a role")
    public void addRolePermissions(
            @Option(required = true, description = "name of role") String name,
            @Option(required = true, description = "permissions to add") List<String> permissions)
    {
        authClient.addRolePermissions(name, permissions);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"role", "permissions", "set"}, description = "Set permissions of a role")
    public void setRolePermissions(
            @Option(required = true, description = "name of role") String name,
            @Option(required = true, description = "permissions to set") List<String> permissions)
    {
        authClient.setRolePermissions(name, permissions);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"role", "permissions", "remove"}, description = "Remove permissions from a role")
    public void removeRolePermissions(
            @Option(required = true, description = "name of role") String name,
            @Option(required = true, description = "permissions to remove") List<String> permissions)
    {
        authClient.removeRolePermissions(name, permissions);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"role", "inherited", "set"}, description = "Set inherited roles for a role")
    public void setInheritedRoles(
            @Option(required = true, description = "name of role") String name,
            @Option(required = true, description = "inherited roles") List<String> inheritedRoles)
    {
        authClient.setInheritedRoles(name, inheritedRoles);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"permissions", "all"}, description = "List available permissions")
    public String getAvailablePermissions(@Option(required = false, description = "Regex to filters permissions") String filter)
    throws JsonProcessingException
    {
        List<String> permissions = new LinkedList<>(authClient.getAvailablePermissions().stream()
                .filter(p -> filter == null || p.matches(filter)).toList());

        Collections.sort(permissions);

        return jsonPrettyPrinter.prettyPrintJSON(permissions);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"permissions", "effective"},
            description = "List permissions for an admin including all inherited permissions from roles")
    public String getEffectivePermissions(@Option(required = true, description = "name of admin") String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(authClient.getEffectivePermissions(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "2fa", "set", "enable"}, description = "Enable Two Factor Authentication for an admin")
    public void enable2FA(@Option(required = true, description = "name of admin") String name,
            @Option(required = true) boolean enable)
    {
        authClient.set2FAEnabled(name, enable);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "2fa", "set", "secret"}, description = "Sets the Two Factor Authentication secret")
    public void set2FASecret(@Option(required = true, description = "name of admin") String name,
            @Option(required = true) String secret)
    {
        authClient.set2FASecret(name, secret);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "2fa", "get", "secret"}, description = "Gets the Two Factor Authentication secret")
    public String get2FASecret(@Option(required = true, description = "name of admin") String name) {
        return authClient.get2FASecret(name);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "2fa", "caller", "get", "secret"}, description = "Gets the Two Factor Authentication secret for the caller")
    public String getCaller2FASecret() {
        return authClient.getCaller2FASecret();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "2fa", "caller", "get", "secret-qr-code"}, description = "Gets the Two Factor Authentication secret for the caller as a base64 encoded image")
    public String getCaller2FASecretQRCode() {
        return authClient.getCaller2FASecretQRCode();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "2fa", "caller", "get", "enable"}, description = "Returns true if two Factor Authentication for the caller is enabled")
    public boolean getCaller2FAEnabled() {
        return authClient.getCaller2FAEnabled();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"admin", "2fa", "caller", "set", "enable"}, description = "Enable two Factor Authentication for the caller")
    public void setCaller2FAEnabled(@Option(required = true) boolean enable) {
        authClient.setCaller2FAEnabled(enable);
    }
}
