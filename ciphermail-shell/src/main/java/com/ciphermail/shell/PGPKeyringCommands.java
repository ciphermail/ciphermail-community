/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.common.security.openpgp.PGPKeyType;
import com.ciphermail.core.common.security.openpgp.PGPSearchField;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.rest.client.PGPKeyringClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Command(command = {"pgp", "keyring"}, group = "PGP")
@SuppressWarnings({"java:S6813"})
public class PGPKeyringCommands
{
    @Autowired
    private PGPKeyringClient pgpKeyringClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "public-key", "with", "id"}, description = "Get the public key with the given id")
    public String getKey(@Option(required = true) UUID id)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyringClient.getKey(id));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "public-key", "with", "fingerprint"}, description = "Get the public key with the " +
                                                                                   "given SHA256 fingerprint")
    public String getKeyBySha256Fingerprint(@Option(required = true) String sha256Fingerprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyringClient.getKeyBySha256Fingerprint(sha256Fingerprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "sub-keys"}, description = "Get the sub keys of a master key")
    public String getSubKeys(@Option(required = true) UUID masterId)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyringClient.getSubKeys(masterId));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "all"}, description = "List all matching keys")
    public String getKeys(
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
            @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
            @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias,
            @Option(required = true, defaultValue = "master-key", label = ShellConst.PGP_KEY_TYPE_LABEL)
            @OptionValues(provider = "enumCompletionProvider") PGPKeyType pgpKeyType,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyringClient.getKeys(
                expired,
                missingKeyAlias,
                pgpKeyType,
                firstResult,
                maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "all", "count"}, description = "Get the number of matching keys")
    public Long getKeysCount(
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
            @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
            @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias,
            @Option(required = true, defaultValue = "master-key", label = ShellConst.PGP_KEY_TYPE_LABEL)
            @OptionValues(provider = "enumCompletionProvider") PGPKeyType pgpKeyType)
    {
        return pgpKeyringClient.getKeysCount(
                expired,
                missingKeyAlias,
                pgpKeyType);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search", "keys"}, description = "Search for matching keys")
    public String searchKeys(
            @Option(required = true, defaultValue = "email", label = ShellConst.PGP_SEARCH_FIELD_LABEL)
            @OptionValues(provider = "enumCompletionProvider") PGPSearchField searchField,
            @Option(required = true) String searchValue,
            @Option(required = true, defaultValue = "exact", label = ShellConst.MATCH_LABEL)
            @OptionValues(provider = "enumCompletionProvider") Match match,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
            @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
            @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias,
            @Option(required = true, defaultValue = "master-key", label = ShellConst.PGP_KEY_TYPE_LABEL)
            @OptionValues(provider = "enumCompletionProvider") PGPKeyType pgpKeyType,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyringClient.searchKeys(
                searchField,
                searchValue,
                match,
                expired,
                missingKeyAlias,
                pgpKeyType,
                firstResult,
                maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search", "keys", "count"}, description = "Get the number of keys matching the search filter")
    public Long searchKeysCount(
            @Option(required = true, defaultValue = "email", label = ShellConst.PGP_SEARCH_FIELD_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") PGPSearchField searchField,
            @Option(required = true) String searchValue,
            @Option(required = true, defaultValue = "exact", label = ShellConst.MATCH_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Match match,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias,
            @Option(required = true, defaultValue = "master-key", label = ShellConst.PGP_KEY_TYPE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") PGPKeyType pgpKeyType)
    {
        return pgpKeyringClient.searchKeysCount(
                searchField,
                searchValue,
                match,
                expired,
                missingKeyAlias,
                pgpKeyType);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"delete", "key"}, description = "Delete key with the given id")
    public void deleteKey(@Option(required = true) UUID id)
    {
        pgpKeyringClient.deleteKey(id);
    }
}
