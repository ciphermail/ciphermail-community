/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.CertificateValidationClient;
import com.ciphermail.rest.core.CertificateStore;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@Command(command = {"certificate", "validation"}, group = "Certificate Validation")
@SuppressWarnings({"java:S6813"})
public class CertificateValidationCommands
{
    @Autowired
    private CertificateValidationClient certificateValidationClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"validate", "signing"},
            description = "Check if the certificate is valid for signing")
    public String validateCertificateForSigning(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateValidationClient
                .validateCertificateForSigning(store, thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"validate", "signing", "external"},
            description = "Check if the certificate is valid for signing without importing the certificate")
    public String validateExternalCertificateForSigning(
            @Option(required = false) String thumbprint,
            @Option(required = true)
                @OptionValues(provider = "fileCompletionProvider") File file)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateValidationClient.validateExternalCertificateForSigning(
                thumbprint,
                new FileSystemResource(ShellUtils.validateForReading(file))));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"validate", "encryption"},
            description = "Check if the certificate is valid for encryption")
    public String validateCertificateForEncryption(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateValidationClient
                .validateCertificateForEncryption(store, thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"validate", "encryption", "external"},
            description = "Check if the certificate is valid for encryption without importing the certificate")
    public String validateExternalCertificateForEncryption(
            @Option(required = false) String thumbprint,
            @Option(required = true)
            @OptionValues(provider = "fileCompletionProvider") File file)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateValidationClient.validateExternalCertificateForEncryption(
                thumbprint,
                new FileSystemResource(ShellUtils.validateForReading(file))));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"validate"},
            description = "Check if the certificate is valid")
    public String validateCertificate(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateValidationClient
                .validateCertificate(store, thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"validate", "external"},
            description = "Check if the certificate is valid without importing the certificate")
    public String validateExternalCertificate(
            @Option(required = false) String thumbprint,
            @Option(required = true)
                    @OptionValues(provider = "fileCompletionProvider") File file)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateValidationClient.validateExternalCertificate(
                thumbprint,
                new FileSystemResource(ShellUtils.validateForReading(file))));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "issuer"}, description = "Gets the issuer certificate")
    public String getIssuerCertificate(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateValidationClient
                .getIssuerCertificate(store, thumbprint));
    }
}
