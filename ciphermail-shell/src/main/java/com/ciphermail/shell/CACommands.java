/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.common.security.ca.CACertificateSignatureAlgorithm;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.rest.client.CAClient;
import com.ciphermail.rest.core.CAKeyUsageFilter;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@Command(command = "ca", group = "CA")
@SuppressWarnings({"java:S6813"})
public class CACommands
{
    @Autowired
    private CAClient caClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = "create", description = "Create new root and intermediate CA certificates")
    public String createCA(
            @Option(required = true) int rootDaysValid,
            @Option(required = true) int intermediateDaysValid,
            @Option(required = true, label = "[2048, 3072, 4096]", defaultValue = "4096") int rootKeyLength,
            @Option(required = true, label = "[2048, 3072, 4096]", defaultValue = "4096") int intermediateKeyLength,
            @Option(required = true, label = ShellConst.CA_CERT_SIG_ALG_LABEL, defaultValue = "sha256-with-rsa-encryption")
                    @OptionValues(provider = "enumCompletionProvider") CACertificateSignatureAlgorithm signatureAlgorithm,
            @Option(required = true) String rootCommonName,
            @Option(required = true) String intermediateCommonName,
            @Option(required = false) String rootEmail,
            @Option(required = false) String rootOrganisation,
            @Option(required = false) String rootOrganisationalUnit,
            @Option(required = false) String rootCountryCode,
            @Option(required = false) String rootState,
            @Option(required = false) String rootLocality,
            @Option(required = false) String rootGivenName,
            @Option(required = false) String rootSurname,
            @Option(required = false) String intermediateEmail,
            @Option(required = false) String intermediateOrganisation,
            @Option(required = false) String intermediateOrganisationalUnit,
            @Option(required = false) String intermediateCountryCode,
            @Option(required = false) String intermediateState,
            @Option(required = false) String intermediateLocality,
            @Option(required = false) String intermediateGivenName,
            @Option(required = false) String intermediateSurname,
            @Option(required = false) String crlDistributionPoint
    )
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.createCA(
                rootDaysValid,
                intermediateDaysValid,
                rootKeyLength,
                intermediateKeyLength,
                signatureAlgorithm,
                crlDistributionPoint,
                rootEmail,
                rootOrganisation,
                rootOrganisationalUnit,
                rootCountryCode,
                rootState,
                rootLocality,
                rootCommonName,
                rootGivenName,
                rootSurname,
                intermediateEmail,
                intermediateOrganisation,
                intermediateOrganisationalUnit,
                intermediateCountryCode,
                intermediateState,
                intermediateLocality,
                intermediateCommonName,
                intermediateGivenName,
                intermediateSurname
        ));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "available"}, description = "List available CA certificates")
    public String getAvailableCAs(@Option(required = true, label = ShellConst.CA_KEYUSAGE_FILTER_LABEL,
            defaultValue = "all") @OptionValues(provider = "enumCompletionProvider") CAKeyUsageFilter keyUsageFilter)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.getAvailableCAs(keyUsageFilter));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "request-handlers"}, description = "List available certificate request handlers")
    public String getCertificateRequestHandlers()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.getCertificateRequestHandlers());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"request", "certificate"}, description = "Request a new certificate using the selected " +
                                                                 "certificate request handler")
    public String requestNewCertificate(
            @Option(required = true)  String email,
            @Option(required = true)  String subjectCommonName,
            @Option(required = true) int daysValid,
            @Option(required = true, label = "[2048, 3072, 4096]", defaultValue = "3072") int keyLength,
            @Option(required = true, label = ShellConst.CA_CERT_SIG_ALG_LABEL, defaultValue = "sha256-with-rsa-encryption")
                    @OptionValues(provider = "enumCompletionProvider") CACertificateSignatureAlgorithm signatureAlgorithm,
            @Option(required = true) String certificateRequestHandler,
            @Option(required = false) String subjectEmail,
            @Option(required = false) String subjectOrganisation,
            @Option(required = false) String subjectOrganisationalUnit,
            @Option(required = false) String subjectCountryCode,
            @Option(required = false) String subjectState,
            @Option(required = false) String subjectLocality,
            @Option(required = false) String subjectGivenName,
            @Option(required = false) String subjectSurname,
            @Option(required = false) String crlDistributionPoint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.requestNewCertificate(
                email,
                subjectEmail,
                subjectOrganisation,
                subjectOrganisationalUnit,
                subjectCountryCode,
                subjectState,
                subjectLocality,
                subjectCommonName,
                subjectGivenName,
                subjectSurname,
                daysValid,
                keyLength,
                signatureAlgorithm,
                crlDistributionPoint,
                certificateRequestHandler));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pending", "get", "all"}, description = "List all pending requests")
    public String getPendingRequests(
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.getPendingRequests(firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pending", "count"}, description = "Get the number of pending requests")
    public Long getPendingRequestsCount()
    {
        return caClient.getPendingRequestsCount();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pending", "email"}, description = "List all pending requests for an email address")
    public String getPendingRequestsByEmail(
            @Option(required = true) String email,
            @Option(required = true, label = ShellConst.MATCH_LABEL, defaultValue = "exact")
                    @OptionValues(provider = "enumCompletionProvider") Match match,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.getPendingRequestsByEmail(email, match, firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pending", "email", "count"}, description = "Get the number of pending requests " +
                                                                    "for an email address")
    public Long getPendingRequestsByEmailCount(
            @Option(required = true) String email,
            @Option(required = true, label = ShellConst.MATCH_LABEL, defaultValue = "exact")
                    @OptionValues(provider = "enumCompletionProvider") Match match)
    {
        return caClient.getPendingRequestsByEmailCount(email, match);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pending", "get"}, description = "Get the pending requests with the given id")
    public String getPendingRequest(@Option(required = true) UUID id)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.getPendingRequest(id));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pending", "delete"}, description = "Delete the pending requests with the given id")
    public void deletePendingRequest(@Option(required = true) UUID id)
    {
        caClient.deletePendingRequest(id);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pending", "reschedule"}, description = "Reschedule the pending requests with the given id")
    public String rescheduleRequest(@Option(required = true) UUID id)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.rescheduleRequest(id));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pending", "finalize"}, description = "Upload certificate to a matching CSR")
    public String finalizePendingRequest(@Option(required = true)
            @OptionValues(provider = "fileCompletionProvider") File file)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.finalizePendingRequest(
                new FileSystemResource(ShellUtils.validateForReading(file))));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"create", "self-signed-certificate"}, description = "Create a self-signed certificate")
    public String createSelfSignedCertificate(
            @Option(required = true)  String email,
            @Option(required = true)  String subjectCommonName,
            @Option(required = true) int daysValid,
            @Option(required = true, label = "[2048, 3072, 4096]", defaultValue = "3072") int keyLength,
            @Option(required = true, label = ShellConst.CA_CERT_SIG_ALG_LABEL, defaultValue = "sha256-with-rsa-encryption")
                    @OptionValues(provider = "enumCompletionProvider") CACertificateSignatureAlgorithm signatureAlgorithm,
            @Option(required = true) boolean whiteList,
            @Option(required = false) String subjectEmail,
            @Option(required = false) String subjectOrganisation,
            @Option(required = false) String subjectOrganisationalUnit,
            @Option(required = false) String subjectCountryCode,
            @Option(required = false) String subjectState,
            @Option(required = false) String subjectLocality,
            @Option(required = false) String subjectGivenName,
            @Option(required = false) String subjectSurname)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.createSelfSignedCertificate(
                email,
                subjectEmail,
                subjectOrganisation,
                subjectOrganisationalUnit,
                subjectCountryCode,
                subjectState,
                subjectLocality,
                subjectCommonName,
                subjectGivenName,
                subjectSurname,
                daysValid,
                keyLength,
                signatureAlgorithm,
                whiteList));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"create", "crl"}, description = "Create a crl")
    public String createCRL(
            @Option(required = true) String issuerThumbprint,
            @Option(required = true) long nextUpdate,
            @Option(required = true, longNames = "update-existing-crl", defaultValue = "true") boolean updateExistingCRL,
            @Option(required = true, label = ShellConst.CA_CERT_SIG_ALG_LABEL, defaultValue = "sha256-with-rsa-encryption")
                    @OptionValues(provider = "enumCompletionProvider") CACertificateSignatureAlgorithm signatureAlgorithm,
            @Option List<String> serialNumbersHex)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(caClient.createCRL(
                issuerThumbprint,
                nextUpdate,
                updateExistingCRL,
                signatureAlgorithm,
                Optional.ofNullable(serialNumbersHex).orElse(Collections.emptyList())));
    }
}
