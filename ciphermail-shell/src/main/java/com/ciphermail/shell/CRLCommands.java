/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.rest.client.CRLClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
@Command(command = "crl", group = "CRL")
@SuppressWarnings({"java:S6813"})
public class CRLCommands
{
    @Autowired
    private CRLClient crlClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @Value("${" + ShellConst.SILENT_PROPERTY + "}")
    private boolean silent;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"import"},
            description = "Import CRLs from a file")
    public String importCRL(
            @Option(required = true)
                @OptionValues(provider = "fileCompletionProvider") File file,
            @Option(defaultValue = "true") boolean skipExpired)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(crlClient.importCRL(
                new FileSystemResource(ShellUtils.validateForReading(file)),
                skipExpired));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "all"}, description = "List all CRLs")
    public String getCRLs(
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(crlClient.getCRLs(firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "all", "count"}, description = "Get the number of CRLs")
    public Long getCRLsCount()
    {
        return crlClient.getCRLsCount();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get"}, description = "Get the CRL with the given thumbprint")
    public String getCRL(@Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(crlClient.getCRL(thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"delete"}, description = "Delete the CRL with the given thumbprint")
    public String deleteCRL(@Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(crlClient.deleteCRL(thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"export"}, description = "Export CRLs to a DER or PEM encoded file")
    public void exportCertificates(
            @Option(required = true, defaultValue = "pem", label = ShellConst.OBJECT_ENCODING_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") ObjectEncoding encoding,
            @Option(required = true) List<String> thumbprints,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        FileUtils.writeByteArrayToFile(outputFile, crlClient.exportCRLs(encoding, thumbprints));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"refresh"}, description = "Force the download of new CRLs")
    public void refreshCRLStore()
    {
        crlClient.refreshCRLStore();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"validate"}, description = "Validate the CRL")
    public String validateCRL(@Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(crlClient.validateCRL(thumbprint));
    }
}
