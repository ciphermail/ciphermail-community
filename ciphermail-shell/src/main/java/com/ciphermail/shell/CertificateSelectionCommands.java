/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.app.NamedCertificateCategory;
import com.ciphermail.rest.client.CertificateSelectionClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
@Command(command = {"certificate", "selection"}, group = "Certificate Selection")
@SuppressWarnings({"java:S6813"})
public class CertificateSelectionCommands
{
    @Autowired
    private CertificateSelectionClient certificateSelectionClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get" , "auto-selected"},
            description = "List auto selected encryption certificates for a user")
    public String getAutoSelectedEncryptionCertificatesForUser(@Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getAutoSelectedEncryptionCertificatesForUser(email));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "explicit"},
            description = "List manually selected certificates for a user")
    public String getExplicitCertificatesForUser(@Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getExplicitCertificatesForUser(email));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "set", "explicit"},
            description = "Set manually selected certificates for a user")
    public String setExplicitCertificatesForUser(
            @Option(required = true) String email,
            @Option(required = true) List<String> thumbprints)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setExplicitCertificatesForUser(email, thumbprints));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "reset", "explicit"},
            description = "Reset manually selected certificates for a user")
    public String resetExplicitCertificatesForUser(
            @Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setExplicitCertificatesForUser(email, Collections.emptyList()));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "inherited"},
            description = "List inherited certificates for a user")
    public String getInheritedCertificatesForUser(@Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getInheritedCertificatesForUser(email));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "encryption"},
            description = "List encryption certificates for a user")
    public String getEncryptionCertificatesForUser(@Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getEncryptionCertificatesForUser(email));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "named"},
            description = "List named certificates for a user")
    public String getNamedCertificatesForUser(
            @Option(required = true) String email,
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getNamedCertificatesForUser(email, category));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "set", "named"},
            description = "Set named certificates for a user")
    public String setNamedCertificatesForUser(
            @Option(required = true) String email,
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category,
            @Option(required = true) List<String> thumbprints)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setNamedCertificatesForUser(email, category, thumbprints));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "reset", "named"},
            description = "Reset named certificates for a user")
    public String resetNamedCertificatesForUser(
            @Option(required = true) String email,
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setNamedCertificatesForUser(email, category, Collections.emptyList()));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "named", "inherited"},
            description = "List inherited named certificates for a user")
    public String getInheritedNamedCertificatesForUser(
            @Option(required = true) String email,
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
            @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getInheritedNamedCertificatesForUser(email, category));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "explicit"},
            description = "List manually selected certificates for a domain")
    public String getExplicitCertificatesForDomain(@Option(required = true) String domain)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getExplicitCertificatesForDomain(domain));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "set", "explicit"},
            description = "Set manually selected certificates for a domain")
    public String setExplicitCertificatesForDomain(
            @Option(required = true) String domain,
            @Option(required = true) List<String> thumbprints)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setExplicitCertificatesForDomain(domain, thumbprints));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "reset", "explicit"},
            description = "Reset manually selected certificates for a domain")
    public String resetExplicitCertificatesForDomain(
            @Option(required = true) String domain)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setExplicitCertificatesForDomain(domain, Collections.emptyList()));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "inherited"},
            description = "List inherited certificates for a domain")
    public String getInheritedCertificatesForDomain(@Option(required = true) String domain)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getInheritedCertificatesForDomain(domain));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "named"},
            description = "List named certificates for a domain")
    public String getNamedCertificatesForDomain(
            @Option(required = true) String domain,
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getNamedCertificatesForDomain(domain, category));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "set", "named"},
            description = "Set named certificates for a domain")
    public String setNamedCertificatesForDomain(
            @Option(required = true) String domain,
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category,
            @Option(required = true) List<String> thumbprints)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setNamedCertificatesForDomain(domain, category, thumbprints));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "reset", "named"},
            description = "Reset named certificates for a domain")
    public String resetNamedCertificatesForDomain(
            @Option(required = true) String domain,
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setNamedCertificatesForDomain(domain, category, Collections.emptyList()));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "named", "inherited"},
            description = "List inherited named certificates for a domain")
    public String getInheritedNamedCertificatesForDomain(
            @Option(required = true) String domain,
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getInheritedNamedCertificatesForDomain(domain, category));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "get", "named"},
            description = "List named certificates for the global settings")
    public String getNamedCertificatesForGlobal(
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient.getNamedCertificatesForGlobal(category));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "set", "named"},
            description = "Set named certificates for the global settings")
    public String setNamedCertificatesForGlobal(
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category,
            @Option(required = true) List<String> thumbprints)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setNamedCertificatesForGlobal(category, thumbprints));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "reset", "named"},
            description = "Reset named certificates for the global settings")
    public String resetNamedCertificatesForGlobal(
            @Option(required = true, defaultValue = "additional", label = ShellConst.NAMED_CERTIFICATE_CATEGORY_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") NamedCertificateCategory category)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setNamedCertificatesForGlobal(category, Collections.emptyList()));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "signing"},
            description = "Get signing certificate for a user")
    public String getSigningCertificateForUser(@Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getSigningCertificateForUser(email));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "set", "signing"},
            description = "Set signing certificate for a user")
    public String setSigningCertificateForUser(
            @Option(required = true) String email,
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setSigningCertificateForUser(email, thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "reset", "signing"},
            description = "Reset signing certificate for a user")
    public String resetSigningCertificateForUser(
            @Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setSigningCertificateForUser(email, ""));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "signing"},
            description = "Get signing certificate for a domain")
    public String getSigningCertificateForDomain(@Option(required = true) String domain)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getSigningCertificateForDomain(domain));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "set", "signing"},
            description = "Set signing certificate for a domain")
    public String setSigningCertificateForDomain(
            @Option(required = true) String domain,
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setSigningCertificateForDomain(domain, thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "reset", "signing"},
            description = "Reset signing certificate for a domain")
    public String resetSigningCertificateForDomain(
            @Option(required = true) String domain)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setSigningCertificateForDomain(domain, ""));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "get", "signing"},
            description = "Get signing certificate for the global settings")
    public String getSigningCertificateForGlobal()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .getSigningCertificateForGlobal());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "set", "signing"},
            description = "Set signing certificate for the global settings")
    public String setSigningCertificateForGlobal(
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setSigningCertificateForGlobal(thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "reset", "signing"},
            description = "Reset signing certificate for the global settings")
    public String resetSigningCertificateForGlobal()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateSelectionClient
                .setSigningCertificateForGlobal(""));
    }
}
