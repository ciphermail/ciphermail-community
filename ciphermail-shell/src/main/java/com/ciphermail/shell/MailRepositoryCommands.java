/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.MailRepositoryClient;
import com.ciphermail.rest.core.MailDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Component
@Command(command = {"mpa", "repository"}, group = "MPA")
@SuppressWarnings({"java:S6813"})
public class MailRepositoryCommands
{
    @Autowired
    private MailRepositoryClient mailRepositoryClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @Value("${" + ShellConst.SILENT_PROPERTY + "}")
    private boolean silent;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"urls"}, description = "List all Mail repository URLs")
    public String getMailRepositoryUrls()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(mailRepositoryClient.getMailRepositoryUrls());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"keys"}, description = "List all Mail repository keys")
    public String getMailKeys(@Option(required = true) String mailRepositoryUrl)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(mailRepositoryClient.getMailKeys(mailRepositoryUrl));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"size"}, description = "Get Mail repository size")
    public Long getMailRepositorySize(@Option(required = true) String mailRepositoryUrl)
    {
        return mailRepositoryClient.getMailRepositorySize(mailRepositoryUrl);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get"}, description = "Get Mail repository item")
    public String getMail(
            @Option(required = true) String mailRepositoryUrl,
            @Option(required = true) String key,
            @Option(required = false, defaultValue = "false", longNames = "include-mime") boolean includeMIME,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File mimeOutputFile)
    throws IOException
    {
        MailDTO.MailDetailsWithMIME result = mailRepositoryClient.getMail(mailRepositoryUrl, key, includeMIME);

        if (mimeOutputFile != null && ShellUtils.validateForWriting(mimeOutputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(mimeOutputFile,false))
        {
            throw new CancelCommandException();
        }

        if (mimeOutputFile != null) {
            FileUtils.write(mimeOutputFile, result.mime(), StandardCharsets.UTF_8);
        }

        return jsonPrettyPrinter.prettyPrintJSON(result);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"delete"}, description = "Delete Mail repository item")
    public void deleteMail(
            @Option(required = true) String mailRepositoryUrl,
            @Option(required = true) String key)
    {
        mailRepositoryClient.deleteMail(mailRepositoryUrl, key);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"delete", "all"}, description = "Delete all Mail repository items")
    public void deleteAllMail(@Option(required = true) String mailRepositoryUrl)
    {
        mailRepositoryClient.deleteAllMail(mailRepositoryUrl);
    }
}
