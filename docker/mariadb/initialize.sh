#!/bin/bash

# Create databases and users. This step is performed as the MariaDB root user.
<< EOF mysql --user=root
CREATE USER "ciphermail" IDENTIFIED BY 'ciphermail';

CREATE DATABASE \`ciphermail\`;
GRANT ALL PRIVILEGES ON \`ciphermail\`.* TO "ciphermail";

CREATE DATABASE \`ciphermail-test\`;
GRANT ALL PRIVILEGES ON \`ciphermail-test\`.* TO "ciphermail";
EOF

# Initialize the 'ciphermail' database.
mysql --database=ciphermail --user=ciphermail --password=ciphermail < ./conf/sql/ciphermail.mariadb.10_5.sql

# Initialize the 'ciphermail-test' database.
mysql --database=ciphermail-test --user=ciphermail --password=ciphermail < ./conf/sql/ciphermail.mariadb.10_5.sql
