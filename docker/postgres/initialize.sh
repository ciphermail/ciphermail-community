#!/bin/bash

# Create databases and users. This step is performed as the PostgreSQL
# superuser.
<< EOF psql --username=postgres
CREATE USER "ciphermail" PASSWORD 'ciphermail';

CREATE DATABASE "ciphermail";
GRANT ALL PRIVILEGES ON DATABASE "ciphermail" TO "ciphermail";
ALTER DATABASE "ciphermail" OWNER TO "ciphermail";

CREATE DATABASE "ciphermail-test";
GRANT ALL PRIVILEGES ON DATABASE "ciphermail-test" TO "ciphermail";
ALTER DATABASE "ciphermail-test" OWNER TO "ciphermail";
EOF

# Initialize the 'ciphermail' database.
psql "postgresql:///ciphermail?user=ciphermail&password=ciphermail" < ./conf/sql/ciphermail.postgres.15.sql

# Initialize the 'ciphermail-test' database.
psql "postgresql:///ciphermail-test?user=ciphermail&password=ciphermail" < ./conf/sql/ciphermail.postgres.15.sql
